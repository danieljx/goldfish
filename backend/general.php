<?php
ini_set('session.gc_maxlifetime', 36000);
session_set_cookie_params(36000);
session_start(); 
function getServer() {
 $pageURL = 'http';
 if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
 $pageURL .= "://";
 if ($_SERVER["SERVER_PORT"] != "80" && $_SERVER["SERVER_PORT"] != "443") {
  $pageURL .= $_SERVER["HTTP_HOST"].":".$_SERVER["SERVER_PORT"]."/";
 } else {
  $pageURL .= $_SERVER["HTTP_HOST"]."/";
 }
 return $pageURL;
}

function isJson($string) {
    if(!is_array($string)) {
      json_decode($string);
      return (json_last_error() == JSON_ERROR_NONE);
    } else {
      return true;
    }
  }

$config = json_decode(file_get_contents(getServer().'config.json'), true);
$config['server'] = getServer();

define('PHYSDIR', $config['physDir']);

$config = json_encode($config);

function getDb() {
	return new PDO('mysql:host=127.0.0.1;dbname=goldfishv2;charset=utf8mb4', 'goldfish', 'fishGOLD');
}

function add_log($param) {
    $time = date('H:i:s');
    file_put_contents('log.txt', '[' . $time . '] ' . $param . PHP_EOL, FILE_APPEND);
}

?>