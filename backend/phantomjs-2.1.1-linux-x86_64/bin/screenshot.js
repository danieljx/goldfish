var page = require('webpage').create();
page.viewportSize = { width: 1280, height : 1200 };

function waitFor(testFx, onReady, timeOutMillis) {
  var maxtimeOutMillis = timeOutMillis ? timeOutMillis : 8000, //< Default Max Timout is 8s
      start = new Date().getTime(),
      condition = false,
      interval = setInterval(function() {
          if ( (new Date().getTime() - start < maxtimeOutMillis) && !condition ) {
              // If not time-out yet and condition not yet fulfilled
              condition = (typeof(testFx) === "string" ? eval(testFx) : testFx()); //< defensive code
              console.log(condition);
          } else {
              if(!condition) {
                  // If condition still not fulfilled (timeout but condition is 'false')
                  console.log("'waitFor()' timeout");
                  phantom.exit(1);
              } else {
                  // Condition fulfilled (timeout and/or condition is 'true')
                  console.log("'waitFor()' finished in " + (new Date().getTime() - start) + "ms.");
                  page.render('screenshot.png');
                  typeof(onReady) === "string" ? eval(onReady) : onReady(); //< Do what it's supposed to do once the condition is fulfilled
                  
                  clearInterval(interval); //< Stop this interval
              }
          }
      }, 2000); //< repeat check every 250ms
};


page.open('http://goldfish/users/1/projects/3/units/1', function(status) {
  if (status !== "success") {
      console.log("Unable to access network - status "+status);
  } else {
      // Wait for 'signin-dropdown' to be visible
      waitFor(function() {
          // Check in the page if a specific element is now visible
          return page.evaluate(function() {
              // console.log(document.getElementsByTagName("body")[0].classList);
              return document.getElementsByTagName("body")[0].classList.contains("screenshot-ready");
              // return document.getElementsByTagName("body")[0].classList.contains("grag");
          });
      }, function() {
        console.log("Timeout..");
        phantom.exit();
      });
  }
});