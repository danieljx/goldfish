<?php

class Log {

    public static function add($title, $str) {
    	$time = date('H:i:s');
    	return file_put_contents('log.txt', '[' . $time . '] '.$title.PHP_EOL . $str . PHP_EOL, FILE_APPEND);
    }
    
}
?>