<?php
class Page {
	private $type;
	private $index = false;
	private $name;
	private $body;

	public function __construct($type, $index, $name, $body) {
		$this->type = $type;
		$this->index = $index;
		$this->name = $name;
		$this->body = $body;
	}
}
?>