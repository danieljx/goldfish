<?php
class Db {
    public $host = "localhost";
    public $username = "catedra";
    public $pw = "catedra";
    public $dbname = "oa3";
    public $charset =" utf8";

    public function getPDO() {
    	$dbo = null;
    	try {
    		$dbo = new PDO('mysql:host='.$this->host.';dbname='.$this->dbname,$this->username,$this->pw);
    	} catch(PDOException  $e ){
			echo "Error: ".$e;
		}
    	
        return $dbo;
    }

    public function getId() {
    	return ($this->id);
    }

    public function setName($TName) {
        $this->name = $TName;
    }
}


?>