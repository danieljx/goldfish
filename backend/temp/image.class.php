<?php

class ImageTools {
	public static $imgCounter = 0;

	public static function getTagValue($xmlNode, $property) {
		$tmp = $xmlNode->getElementsByTagName($property);
		if ($tmp->length > 0) {
			return $tmp->item(0);
		} else {
			return false;
		}
	}

	public static function imageCount() {
		return self::$imgCounter++;

	}
}

class Image
{
    private $url;
	private $data;
	private $height;
	private $width;
	private $label;
	private $licensor;
	private $align;
	private $border;
	private $vspace;
	private $hspace;
	private $style;
	private $id;
	private $cssclass;
	private $finder;

    public function getFromXML($xmlNode) {
        $this->id = ImageTools::getTagValue("id");
        $this->url = ImageTools::getTagValue("url");
		$this->data = ImageTools::getTagValue("data");
		$this->height = ImageTools::getTagValue("height");
		$this->width = ImageTools::getTagValue("width");
		$this->label = ImageTools::getTagValue("label");
		$this->licensor = ImageTools::getTagValue("licensor");
		$this->align = ImageTools::getTagValue("align");
		$this->border = ImageTools::getTagValue("border");
		$this->vspace = ImageTools::getTagValue("vspace");
		$this->hspace = ImageTools::getTagValue("hspace");
		$this->style = ImageTools::getTagValue("style");
		$this->cssclass = ImageTools::getTagValue("cssclass");
		$this->finder = ImageTools::getTagValue("finder");
    }

    public function toHTML() {
    	$str = '<img ';
    	$str .= 'id="img'.ImageTools::imageCount().'" ';				
    	$str .= 'src="'.$this->src.'" ';
		$str .= 'width="'.$this->width.'" ';
		$str .= 'height="'.$this->height.'" ';
		$str .= 'align="'.$this->align.'" ';
		$str .= 'vspace="'.$this->vspace.'" ';
		$str .= 'style="'.$this->style.'" ';
		$str .= 'hspace="'.$this->hspace.'" ';					
		$str .= 'border="'.$this->border.'" ';	
		$str .= 'class="'.$this->cssclass.'" ';				
		$str .= 'alt="'.$this->label.'" ';
		$str .= 'title="'.$this->label.'" ';
		$str .= '/>';
    }
}
?>