<?php
class UnitTools {

    public static function getById($pdo, $id) {
    	$stmt = $pdo->prepare('SELECT name, body, usu_nombres, usu_apellidos FROM mdl_learningunit INNER JOIN au_usuario ON mdl_learningunit.author = au_usuario.usu_usuario where id = '.$id.' LIMIT 1');
    	$stmt->execute();
    	$row = $stmt->fetch();
        return $row;
    }

    public static function pageSplit($body) {
        Log::add("Body ", $body);
    	$pag = new DOMDocument;
    	$pag->loadXML($body);
    	$dom_xpath = new DOMXpath($pag);
    	$sections = $dom_xpath->query('/Document/Section');
    	$nodes = array();
    	foreach($sections as $section) {
    		
    		$subDom = new DomDocument;
			$subDom->appendChild($subDom->importNode($section, true));

    		$subdom_xpath = new DOMXpath($subDom);

    		$body = $subdom_xpath->query('body');
    		$body = $body->item(0);
    		$body = $body->nodeValue;



            // $pagina = new DOMDocument;
            // $pagina->loadHTML($body);
            // $pagina_xpath = new DOMXpath($pagina);

            $title = 'backend default';

            $title = $subdom_xpath->query('name');
            $title = $title->item(0);
            $title = $title->nodeValue;

            

    		$body = str_replace(array(']','[','*'), array('>','<','&'), $body);

    		$images = $subdom_xpath->query('Media/Image');
    		foreach ($images as $image) {
    			$img = new Image();
    			$img->getFromXML($image);
    			$body = str_replace('{'.$img->id.'/}', $img->toHTML(), $body);
    		}

      		$nodes[] = array("title" => $title, "body" => $body);
    	}
    	return $nodes;
    }

}

class Unit {
    private $pages = array();

    public function addPage($page) {
        $this->pages[] = $page;
    }

    public function getPage($idx) {
        if ($idx < count($this->pages)) {
            return $this->pages[$idx];
        }
    }

    public function readFromXML( $body ) {
        $pag = new DOMDocument;
        $pag->loadXML($body);
        $dom_xpath = new DOMXpath($pag);
        $sections = $dom_xpath->query('/Document/Section');
        $nodes = array();
        foreach($sections as $section) {

            $subDom = new DomDocument;
            $subDom->appendChild($subDom->importNode($section, true));

            $subdom_xpath = new DOMXpath($subDom);
            $name = $subdom_xpath->query('name')->item(0)->nodeValue; 
            $index = $subdom_xpath->query('index')->item(0)->nodeValue;
            $body = $subdom_xpath->query('body');
            $body = $body->item(0);
            $body = $body->nodeValue;

            $body = str_replace(array(']','[','*'), array('>','<','&'), $body);

            $images = $subdom_xpath->query('Media/Image');
            foreach ($images as $image) {
                $img = new Image();
                $img->getFromXML($image);
                $body = str_replace('{'.$img->id.'/}', $img->toHTML(), $body);
            }

            $nodes[] = $body;
        }
    }
}


?>