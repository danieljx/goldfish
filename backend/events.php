<?php
header("Content-Type: text/event-stream");
header("Cache-Control: no-cache, no-store");
header("Connection: keep-alive");

function add_log($param) {
    $time = date('H:i:s');
    file_put_contents('log.txt', '[' . $time . '] ' . $param . PHP_EOL, FILE_APPEND);
}

$lastId = $_SERVER["HTTP_LAST_EVENT_ID"];

$op = (isset($_GET['unit']) && isset($_GET['saved'])) ? "updateUnit" : "default";
$interval = 60;

if ($op == 'updateUnit') {
    $db = new PDO('mysql:host=127.0.0.1;dbname=goldfishv2;charset=utf8mb4', 'goldfish', 'fishGOLD');
    $unitId = $_GET['unit'];
    $saved = $_GET['saved'];
    $sql = "SELECT unit.saved FROM gf_units AS unit WHERE unit.id=:id and unit.saved > :saved";
    $interval = 1;
}

while (true) {
    if ($op == "updateUnit") {
        $sth = $db->prepare($sql);
        $sth->bindParam("id", $unitId);
        $sth->bindParam("saved", $saved);
        $sth->execute();
        $items = $sth->fetchALL(PDO::FETCH_ASSOC);
        if (count($items) > 0) {
            $saved = $items[0]['saved'];
            echo("id: ".$lastId.PHP_EOL);
            echo("data: ".$saved.PHP_EOL);  
            echo(PHP_EOL);
            ob_flush();
            flush();
            $lastId++;
        }
    } else {
        echo("id: ".$lastId.PHP_EOL);
        echo("data: ".time().PHP_EOL);  
        echo(PHP_EOL);
        ob_flush();
        flush();
        $lastId++;
    }
       // echo("Event: server-time\n");
       
    sleep($interval);
}
?>