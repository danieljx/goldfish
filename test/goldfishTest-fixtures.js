

var assert = chai.assert;
var expect = chai.expect;

describe('Goldfish', function() {

  var $$;
  var fixWin;
    before(function(done){
      fixtures.path = '/';
      fixtures.load('index.php', function() {
        done();
      });

        $$ = fixtures.window().jQuery; // access the jquery instance from within the fixtures context
        fixWin = fixtures.window();
    });

  it('Should be initiated', function() {
      expect(fixWin.goldfish).to.exist;  
    
  });

  it('Should have an App model instance', function() {
    expect(fixWin.goldfish.app).to.exist;
  });

  it('Should have an App View instance', function() {
    expect(fixWin.goldfish.appView).to.exist;
  });

  it('Should have a Project View', function() {
    expect(fixWin.goldfish.projectsView).to.exist;
  });

  it('Should have a Config Model', function() {
    expect(fixWin.goldfish.config).to.exist;
  });

  it('Should have a Config View', function() {
    expect(fixWin.goldfish.configView).to.exist;
  });

  it('Should have a Notifications View', function() {
    expect(fixWin.goldfish.notificationsView).to.exist;
  });

  it('Should have Router', function() {
    expect(fixWin.goldfish.router).to.exist;
  });

  after(function(){
        fixtures.cleanUp(); // cleans up the fixture for the next test
  });
  
});