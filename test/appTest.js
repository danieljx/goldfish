

var assert = chai.assert;
var expect = chai.expect;
var should = chai.should;
var activeUser = goldfish.app.attributes.activeUser

describe('App Model', function() {
  it('Should have a User Model that is an object', function() {
    // expect(activeUser).to.exist;
    expect(activeUser).to.be.a('object');
    // assert(goldfish.app.attributes.activeUser).not.equal(false);
  });

  it('Should have the property activeView set to "projects"', function() {
    // expect(activeUser).to.exist;
    expect(goldfish.app.attributes.activeSection).to.equal('projects');
    // assert(goldfish.app.attributes.activeUser).not.equal(false);
  });
  
});