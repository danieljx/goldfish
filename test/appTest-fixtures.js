
var assert = chai.assert;
var expect = chai.expect;
var should = chai.should;
// var activeUser = goldfish.app.attributes.activeUser

describe('App Model', function() {

  var $$;
  var fixWin;
  before(function(done){
    fixtures.path = '/';
    fixtures.load('index.php', function() {
      done();
    });

      $$ = fixtures.window().jQuery; // access the jquery instance from within the fixtures context
      fixWin = fixtures.window();
  });

  it('Should have a User Model that is an object', function() {
    // expect(activeUser).to.exist;
    expect(fixWin.goldfish.app.attributes.activeUser).to.be.a('object');
    // assert(goldfish.app.attributes.activeUser).not.equal(false);
  });

  it('Should have the property activeView set to "projects"', function() {
    // expect(activeUser).to.exist;
    expect(fixWin.goldfish.app.attributes.activeSection).to.equal('projects');
    // assert(goldfish.app.attributes.activeUser).not.equal(false);
  });

  after(function(){
        fixtures.cleanUp(); // cleans up the fixture for the next test
  });
  
});