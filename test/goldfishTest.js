

var assert = chai.assert;
var expect = chai.expect;

describe('Goldfish', function() {
  it('Should be initiated', function() {
    expect(goldfish).to.exist;
  });

  it('Should have an App model instance', function() {
    expect(goldfish.app).to.exist;
  });

  it('Should have an App View instance', function() {
    expect(goldfish.appView).to.exist;
  });

  it('Should have a Project View', function() {
    expect(goldfish.projectsView).to.exist;
  });

  it('Should have a Config Model', function() {
    expect(goldfish.config).to.exist;
  });

  it('Should have a Config View', function() {
    expect(goldfish.configView).to.exist;
  });

  it('Should have a Notifications View', function() {
    expect(goldfish.notificationsView).to.exist;
  });

  it('Should have Router', function() {
    expect(goldfish.router).to.exist;
  });
  
});