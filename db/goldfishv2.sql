-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: localhost    Database: goldfishv2
-- ------------------------------------------------------
-- Server version	5.7.21-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `gf_component_blueprints`
--

DROP TABLE IF EXISTS `gf_component_blueprints`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_component_blueprints` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `body` mediumtext,
  `title` varchar(255) DEFAULT NULL,
  `repeatable` varchar(45) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `sass` mediumtext,
  `version` bigint(20) unsigned DEFAULT NULL,
  `data` json DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_component_blueprints`
--

LOCK TABLES `gf_component_blueprints` WRITE;
/*!40000 ALTER TABLE `gf_component_blueprints` DISABLE KEYS */;
INSERT INTO `gf_component_blueprints` VALUES (1,'<blocks />','Main','',1,'','main','.main { \n	height: 100%; \n	margin: 0; \n	padding: 0; \n	position: relative; \n	background-color: transparent; \n	box-shadow: 0px 0px 4px rgba(0,0,0,0.5); \n}',3,NULL),(2,'<nav><ul><li><a href=\"\" contenteditable=\"true\">Uno</a></li><li><a href=\"\" contenteditable=\"true\">Dos</a></li><li><a href=\"\" contenteditable=\"true\">Tres</a></li></ul></nav>','Lista de navegación','li',1,'','lista','nav { background-color: $panelColor; }\n\nul { list-style: none; padding: 2vw 1vw;}\nli { list-style: none; }\nli a { color: $textColor; text-decoration: none; font-size: 1.5vw }\n\n@media (max-width: 800px) {\n	ul {\n	display:flex;flex-direction:row;flex-wrap:nowrap;justify-content:space-\nbetween;align-items:stretch;align-content:flex-start}\n	li a {\n		font-size: 14px;}	\n}',3,NULL),(3,'<h1 contenteditable=\"true\">They don\'t think it be like it is, but it do</h1>','Bloque título','',1,'','titulo','h1 { background-color: $backgroundColor; padding: 2vw 1em; color: $textColor; margin: 0; font-family: Roboto, sans-serif; font-weight: normal; font-size: 2vw; }\n\n@media (max-width: 800px) {\n	h1 { margin: 1em 0; font-size: 24px; }\n}',3,NULL),(4,'<div class=\"section-a\"><blocks /></div><div class=\"flex\"><div class=\"section-b\"><blocks /></div><div class=\"section-c\"><blocks /></div></div>','Nav-Side-Main','',1,'','nav-side-main','.section-a {min-height:5vw}\n.section-b {min-height:5vw}\n.section-c{min-height:5vw}\n.section-a{border:1px solid #f55;display:block}\n.flex{\n	padding:1vw 0;\n	display:flex;\n	flex-direction:row;\n	flex-wrap:nowrap;\n	justify-content:space-between;\n	align-items:stretch;\n	align-content:flex-start\n}\n.section-b{\n	border:1px solid #5f5;\n	flex-basis:35%\n}\n.section-c{\n	border:1px solid #55f;\n	flex-basis:60%\n}\n\n@media (max-width: 800px) {\n	.flex {\n		padding:1vw 0;\n		display:flex;\n		flex-direction:column;\n		flex-wrap:nowrap;\n		justify-content:space-between;\n		align-items:stretch;\n		align-content:flex-start\n	}\n}',3,NULL),(5,'<div class=\"ck\"><content /></div>','HTML','',1,'','html','',3,NULL),(6,'<div class=\"red ludica\"></div>','Lúdica','',2,'content/red/ludica','','',3,'{\"id\": 0, \"xml\": \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\"?><root><metadata><created></created><lastmodified></lastmodified>        <id></id>        <indicadores>        </indicadores>        <description></description>        <autor></autor>        <title></title>    </metadata>    <userdata>        <transition>horizontalflip</transition>        <avatar>            <src>files/avatar.png</src>        </avatar>        <logo><![CDATA[files/muy_importante.png]]></logo>        <basecolor>#f2f4f3</basecolor>        <instancecolor>#109eb5</instancecolor>        <ludicTitle><![CDATA[Muy <strong>importante</strong>]]></ludicTitle>        <content>            <![CDATA[            <p>                The width of the background image therefore depends on the size of its container. If our container width is 500px, our image is resized to 250×250. <strong>Using a percentage can be useful</strong> for responsive designs. Resize the demonstration page to discover how the dimensions change.</p>            <p>                So why not <strong>apply the same principle</strong> to an 8 hour work day? Because there are limits on human productivity. The brain is just like a muscle. Can you continuously run on a treadmill for eight hours? Like our muscles, the brain needs occasional rest. The limit is a bit different for each individual. Through trial and error, I have found that 4 hours is my max.            </p>                ]]>        </content>    </userdata></root>\", \"saves\": [], \"plugin_name\": \"ludica\"}'),(7,'<div class=\"image\"><img /></div>','Imagen','',3,'','img','.image {  > img {    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);    width: 100%;    height: auto;    border: 1px solid #ddd;    border-radius: 4px;   padding: 5px;        &:hover {      box-shadow: 0 0 2px 1px rgba(0, 140, 186, 0.5);    }  }}',2,NULL);
/*!40000 ALTER TABLE `gf_component_blueprints` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_component_relations`
--

DROP TABLE IF EXISTS `gf_component_relations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_component_relations` (
  `gf_descendant` bigint(10) unsigned NOT NULL,
  `depth` int(11) DEFAULT NULL,
  `gf_ascendant` bigint(10) unsigned NOT NULL,
  `position` int(11) DEFAULT NULL,
  `block_group` int(10) unsigned DEFAULT '1',
  KEY `fk_gf_component_relations_gf_components1_idx` (`gf_ascendant`),
  CONSTRAINT `fk_gf_component_relations_gf_components1` FOREIGN KEY (`gf_ascendant`) REFERENCES `gf_components` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_component_relations`
--

LOCK TABLES `gf_component_relations` WRITE;
/*!40000 ALTER TABLE `gf_component_relations` DISABLE KEYS */;
INSERT INTO `gf_component_relations` VALUES (1,0,1,0,0),(3,1,1,0,0),(3,0,3,0,0);
/*!40000 ALTER TABLE `gf_component_relations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_components`
--

DROP TABLE IF EXISTS `gf_components`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_components` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `saved` bigint(10) unsigned DEFAULT '0',
  `body` mediumtext,
  `title` varchar(255) DEFAULT NULL,
  `gf_component_blueprints_id` bigint(10) unsigned NOT NULL,
  `parent_id` bigint(10) unsigned NOT NULL,
  `repeatable` varchar(45) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `data` json DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gf_components_gf_component_blueprints1_idx` (`gf_component_blueprints_id`),
  CONSTRAINT `fk_gf_components_gf_component_blueprints1` FOREIGN KEY (`gf_component_blueprints_id`) REFERENCES `gf_component_blueprints` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_components`
--

LOCK TABLES `gf_components` WRITE;
/*!40000 ALTER TABLE `gf_components` DISABLE KEYS */;
INSERT INTO `gf_components` VALUES (1,20180209090252,'<blocks />',NULL,1,0,'',1,'','null'),(3,20180209090252,'<div class=\"image\" name=\"hd-1080p-backgrounds.jpg\" path_hash=\"80d9f81b4a4bc5cb38005d49c054361b\"><img src=\"http://goldfish/api/user/1/file/view/80d9f81b4a4bc5cb38005d49c054361b\" alt=\"hd-1080p-backgrounds.jpg\"></div>',NULL,7,1,'',3,'','null');
/*!40000 ALTER TABLE `gf_components` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_files`
--

DROP TABLE IF EXISTS `gf_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_files` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `storage_id` bigint(10) unsigned NOT NULL DEFAULT '0',
  `path` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `path_hash` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `parent` bigint(20) NOT NULL DEFAULT '0',
  `name` varchar(250) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `mimetype` bigint(10) unsigned NOT NULL DEFAULT '0',
  `mimepart` int(11) NOT NULL DEFAULT '0',
  `size` bigint(20) NOT NULL DEFAULT '0',
  `mtime` int(11) NOT NULL DEFAULT '0',
  `storage_mtime` int(11) NOT NULL DEFAULT '0',
  `encrypted` int(11) NOT NULL DEFAULT '0',
  `unencrypted_size` bigint(20) NOT NULL DEFAULT '0',
  `etag` varchar(40) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `permissions` int(11) DEFAULT '0',
  `checksum` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `fs_storage_path_hash` (`storage_id`,`path_hash`),
  KEY `fs_parent_name_hash` (`parent`,`name`),
  KEY `fs_storage_mimetype` (`storage_id`,`mimetype`),
  KEY `fs_storage_mimepart` (`storage_id`,`mimepart`),
  KEY `fs_storage_size` (`storage_id`,`size`,`id`),
  KEY `fk_files_storage_id_idx` (`storage_id`),
  KEY `fk_files_mimetype_idx` (`mimetype`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_files`
--

LOCK TABLES `gf_files` WRITE;
/*!40000 ALTER TABLE `gf_files` DISABLE KEYS */;
INSERT INTO `gf_files` VALUES (1,1,'','5fec80051203937854109294db2bc638',-1,'',2,1,5809166,1507234318,1507234313,0,0,'59d6920e6dabe',23,'','2017-12-01 17:07:06','2017-12-01 17:07:06'),(2,1,'cache','adacb950169892fd4717f8a910a99101',1,'cache',2,1,0,1507234312,1507234312,0,0,'59d69208d0bc3',31,'','2017-12-01 17:07:06','2017-12-01 17:07:06'),(3,1,'files','7afa92ca5527a22fbb49856e03be1555',1,'files',2,1,0,1507740358,1507740358,0,0,'59de4ac6d513c',31,NULL,'2017-12-01 17:07:06','2017-12-01 17:07:06'),(4,1,'files/Documents','fea84d082b595a13f1795593fe1ad48e',3,'Documents',2,0,0,1,1,0,0,NULL,65,NULL,'2017-11-30 16:59:35','2017-11-30 16:59:35'),(5,1,'files/Photos','47ac79fa5217a2d694e22fff6adf8e38',3,'Photo',2,0,0,1,1,0,0,NULL,65,NULL,'2017-11-30 17:00:03','2017-11-30 17:00:03'),(6,1,'files/pepepepe.pdf','b0d4bbecf27468ee2b029e61914c7418',3,'pepepepe.pdf',4,0,0,1,1,0,0,NULL,65,NULL,'2018-02-09 12:58:16','2017-12-01 17:07:06'),(7,1,'files/Documents/recetas.doc','0246da15081265c70d3cbb11b441e923',4,'recetas.doc',16,0,435712,1,1,0,0,NULL,65,NULL,'2017-11-30 17:01:49','2017-11-30 17:01:32'),(8,1,'files/Photos/backbone.png','b271093a4cd3b61d5807fe1c3f24edcd',5,'backbone.png',8,0,11985,1,1,0,0,NULL,65,NULL,'2017-11-30 17:05:30','2017-11-30 17:02:44'),(9,1,'files_trashbin','d5471bc74c06a4dd20b605f2f8ef2eb6',1,'files_trashbin',2,1,228761,1507740359,1507740358,0,0,'59de4ac7af04a',31,'','2017-12-01 17:07:06','2017-12-01 17:07:06'),(10,1,'files_trashbin/files','648f3de19aa6719064e82dd3daa1e0cb',9,'files',2,1,228761,1507740359,1507740359,0,0,'59de4ac7af04a',31,'','2017-12-01 17:07:06','2017-12-01 17:07:06'),(11,1,'files/Photos/hd-1080p-backgrounds.jpg','80d9f81b4a4bc5cb38005d49c054361b',5,'hd-1080p-backgrounds.jpg',7,0,408371,1,1,0,0,NULL,65,NULL,'2018-02-09 06:08:30','2018-02-09 06:08:30'),(12,1,'files/Photos/480px-Unofficial_JavaScript_logo_2.svg.png','193038e4f8815bff8e98afe78cfb9ebc',5,'480px-Unofficial_JavaScript_logo_2.svg.png',8,0,4995,1,1,0,0,NULL,65,NULL,'2018-02-09 06:09:21','2018-02-09 06:09:21'),(13,1,'files/Photos/dragon_ball_z_2-wallpaper-2560x1440.jpg','d4bf8d0ef6b123398acff9a1467a512a',5,'dragon_ball_z_2-wallpaper-2560x1440.jpg',7,0,99764,1,1,0,0,NULL,65,NULL,'2018-02-09 06:12:06','2018-02-09 06:12:06'),(14,1,'files/Photos/daniel-profile.jpg','007d53dc4fe01ae54d80402353f770f2',5,'daniel-profile.jpg',7,0,83211,1,1,0,0,NULL,65,NULL,'2018-02-09 06:13:39','2018-02-09 06:13:39'),(15,1,'files/Photos/Screenshot from 2018-02-04 15-16-49.png','587af559a2435945bc6e8e0ce111ffe4',5,'Screenshot from 2018-02-04 15-16-49.png',8,0,1041080,1,1,0,0,NULL,65,NULL,'2018-02-09 06:19:40','2018-02-09 06:19:40'),(16,1,'files/Video','d5ac98510f54228a82a4a02e4fa18e64',3,'Video',2,0,0,1,1,0,0,NULL,65,NULL,'2018-02-09 06:22:05','2018-02-09 06:22:05'),(17,1,'files/Video/Yo Necesito Amor Comprensión y Ternura.mp4','5d371c5272f9f883f7daed15fbe93ea9',16,'Yo Necesito Amor Comprensión y Ternura.mp4',23,0,860782,1,1,0,0,NULL,65,NULL,'2018-02-09 06:22:47','2018-02-09 06:22:47'),(18,1,'files/Video/copia222222.mp4','c4b5b081bf8cbb5dd96bc8e65dc2bfde',16,'copia222222.mp4',23,0,423063,1,1,0,0,NULL,65,NULL,'2018-02-09 09:09:26','2018-02-09 06:23:31'),(19,1,'files/Photos/Ejemplos10','1f576c060f9263001aa445ca121734fe',5,'Ejemplos10',2,0,0,1,1,0,0,NULL,65,NULL,'2018-02-09 06:56:10','2018-02-09 06:24:20'),(20,1,'files/Photos/Ejemplo2','9e7e79e534294b3533b110cf98253014',5,'Ejemplo2',2,0,0,1,1,0,0,NULL,65,NULL,'2018-02-09 06:26:44','2018-02-09 06:26:44'),(21,1,'files/Photos/Ejemplo3','dcecda1eea01ce7044d8590158cdf041',5,'Ejemplo3',2,0,0,1,1,0,0,NULL,65,NULL,'2018-02-09 06:28:36','2018-02-09 06:28:36'),(22,1,'files/Photos/Ejemplo4','a51ec877cc3398890700555b294de83e',5,'Ejemplo4',2,0,0,1,1,0,0,NULL,65,NULL,'2018-02-09 06:30:08','2018-02-09 06:30:08'),(23,1,'files/Video/space-collision,-arcing,-planets,-mountains,-sci-fi-206522.jpg','e330bad0d2033e14692c375d61c2f4fc',16,'space-collision,-arcing,-planets,-mountains,-sci-fi-206522.jpg',7,0,258053,1,1,0,0,NULL,65,NULL,'2018-02-09 07:44:39','2018-02-09 07:44:39'),(24,1,'files/Video/pomicons-master.zip','aa00f00ee4546fe32d3aeeacf7f9a0b1',16,'pomicons-master.zip',12,0,195954,1,1,0,0,NULL,65,NULL,'2018-02-09 07:44:55','2018-02-09 07:44:55'),(25,1,'files/Video/awesome-terminal-fonts-master.zip','d8a4e2e5f06b286e9054edd43164131e',16,'awesome-terminal-fonts-master.zip',12,0,1798407,1,1,0,0,NULL,65,NULL,'2018-02-09 07:44:58','2018-02-09 07:44:58'),(26,3,'','b3aef922e07a6b0cb444930566bb4a82',-1,'',2,0,0,1,1,0,0,NULL,65,NULL,'2018-02-09 08:09:10','2018-02-09 08:09:10'),(27,3,'cache','46f6e84325f97b159ad9a2deff717279',26,'cache',2,0,0,1,1,0,0,NULL,65,NULL,'2018-02-09 08:09:10','2018-02-09 08:09:10'),(28,3,'files','b53d797d0113d7e32e901eaf9c831377',26,'files',2,0,0,1,1,0,0,NULL,65,NULL,'2018-02-09 08:09:10','2018-02-09 08:09:10'),(29,3,'files/Documents','6cf49b1918766101a1475a1f88825c4d',28,'Documents',2,0,0,1,1,0,0,NULL,65,NULL,'2018-02-09 08:09:10','2018-02-09 08:09:10'),(30,3,'files/Documents/recetas.doc','5855977670a2c7afca3c272c72971371',29,'recetas.doc',16,0,435712,1,1,0,0,NULL,65,NULL,'2018-02-09 08:09:11','2018-02-09 08:09:11'),(31,3,'files/Manual-Backbone.pdf','5bffd0294851925e12f5b473f8aa520e',28,'Manual-Backbone.pdf',4,0,665654,1,1,0,0,NULL,65,NULL,'2018-02-09 08:09:11','2018-02-09 08:09:11'),(32,3,'files/Photos','cd7096ca2de7473d4015aa5b5a2c87b9',28,'Photos',2,0,0,1,1,0,0,NULL,65,NULL,'2018-02-09 08:09:11','2018-02-09 08:09:11'),(33,3,'files/Photos/backbone.png','de71397c77adef1bb6daa0d94f3da845',32,'backbone.png',8,0,37101,1,1,0,0,NULL,65,NULL,'2018-02-09 08:09:11','2018-02-09 08:09:11'),(34,3,'files_trashbin','3074ab1638c5c5f0cd1ddd0dad36892a',26,'files_trashbin',2,0,0,1,1,0,0,NULL,65,NULL,'2018-02-09 08:09:11','2018-02-09 08:09:11'),(35,3,'files_trashbin/files','7b58ca28dbc6e7951bae73b01e8d02a1',34,'files',2,0,0,1,1,0,0,NULL,65,NULL,'2018-02-09 08:09:11','2018-02-09 08:09:11'),(36,4,'','0a7c549f7e3c43745649625ce98b1721',-1,'',2,0,0,1,1,0,0,NULL,65,NULL,'2018-02-09 08:09:45','2018-02-09 08:09:45'),(37,4,'cache','84e9f6a407ba0e57118e2c9a63da339c',36,'cache',2,0,0,1,1,0,0,NULL,65,NULL,'2018-02-09 08:09:45','2018-02-09 08:09:45'),(38,4,'files','fdcb5ebcfc5ca7f71666694cd5aa3ff8',36,'files',2,0,0,1,1,0,0,NULL,65,NULL,'2018-02-09 08:09:45','2018-02-09 08:09:45'),(39,4,'files/Documents','8586c0fe18122f901a64874ef8a183d4',38,'Documents',2,0,0,1,1,0,0,NULL,65,NULL,'2018-02-09 08:09:45','2018-02-09 08:09:45'),(40,4,'files/Documents/recetas.doc','24caaac25ebdcd45573923e182125f0f',39,'recetas.doc',16,0,435712,1,1,0,0,NULL,65,NULL,'2018-02-09 08:09:45','2018-02-09 08:09:45'),(41,4,'files/Manual-Backbone.pdf','b67ac39a73dfa603783258b09f9f8258',38,'Manual-Backbone.pdf',4,0,665654,1,1,0,0,NULL,65,NULL,'2018-02-09 08:09:45','2018-02-09 08:09:45'),(42,4,'files/Photos','4cb6eba8285df4a9ad2b615aedff17d0',38,'Photos',2,0,0,1,1,0,0,NULL,65,NULL,'2018-02-09 08:09:45','2018-02-09 08:09:45'),(43,4,'files/Photos/backbone.png','389dfd3dfd3ec6122de2705ae6f5a4fc',42,'backbone.png',8,0,37101,1,1,0,0,NULL,65,NULL,'2018-02-09 08:09:46','2018-02-09 08:09:46'),(44,4,'files_trashbin','5e23aa95cab60da5a4990b758701d82a',36,'files_trashbin',2,0,0,1,1,0,0,NULL,65,NULL,'2018-02-09 08:09:46','2018-02-09 08:09:46'),(45,4,'files_trashbin/files','3cd651f866a06d301abb0c52bb9c9049',44,'files',2,0,0,1,1,0,0,NULL,65,NULL,'2018-02-09 08:09:46','2018-02-09 08:09:46'),(46,5,'','7e60b9bafd1edfbf0ec25c58063a4633',-1,'',2,0,0,1,1,0,0,NULL,65,NULL,'2018-02-09 08:10:15','2018-02-09 08:10:15'),(47,5,'cache','40bea297f4c297a4f076d153383ed9d8',46,'cache',2,0,0,1,1,0,0,NULL,65,NULL,'2018-02-09 08:10:15','2018-02-09 08:10:15'),(48,5,'files','5716ae8f6ffd13fe4d7180edf8668eef',46,'files',2,0,0,1,1,0,0,NULL,65,NULL,'2018-02-09 08:10:16','2018-02-09 08:10:16'),(49,5,'files/Documents','29bdc94e7bbe6b514be7280cbfdc7d52',48,'Documents',2,0,0,1,1,0,0,NULL,65,NULL,'2018-02-09 08:10:16','2018-02-09 08:10:16'),(50,5,'files/Documents/recetas.doc','cb4b561a5d8fc3b7abda2c857b54ac82',49,'recetas.doc',16,0,435712,1,1,0,0,NULL,65,NULL,'2018-02-09 08:10:16','2018-02-09 08:10:16'),(51,5,'files/Manual-Backbone.pdf','a6baa1f9544d8aac63f344bdac877607',48,'Manual-Backbone.pdf',4,0,665654,1,1,0,0,NULL,65,NULL,'2018-02-09 08:10:16','2018-02-09 08:10:16'),(52,5,'files/Photos','15e3d26e59c792a2971040257fe0c2a9',48,'Photos',2,0,0,1,1,0,0,NULL,65,NULL,'2018-02-09 08:10:16','2018-02-09 08:10:16'),(53,5,'files/Photos/backbone.png','cd6c361c789e3bf581f96129611b7042',52,'backbone.png',8,0,37101,1,1,0,0,NULL,65,NULL,'2018-02-09 08:10:16','2018-02-09 08:10:16'),(54,5,'files_trashbin','7c54c4b1cfc0186470804c99d938a872',46,'files_trashbin',2,0,0,1,1,0,0,NULL,65,NULL,'2018-02-09 08:10:16','2018-02-09 08:10:16'),(55,5,'files_trashbin/files','5d252b3c2f4195ff69c0b3f08adc45c0',54,'files',2,0,0,1,1,0,0,NULL,65,NULL,'2018-02-09 08:10:16','2018-02-09 08:10:16'),(56,1,'files/Documents/Yo Necesito Amor Comprensión y Ternura.mp4','ad1122abcabbd57e1817065df0fab069',4,'Yo Necesito Amor Comprensión y Ternura.mp4',23,0,860782,1,1,0,0,NULL,65,NULL,'2018-02-09 09:06:41','2018-02-09 09:06:41'),(57,1,'files/Documents/space-collision,-arcing,-planets,-mountains,-sci-fi-206522.jpg','17764840f80b8554055f914920492996',4,'space-collision,-arcing,-planets,-mountains,-sci-fi-206522.jpg',7,0,258053,1,1,0,0,NULL,65,NULL,'2018-02-09 09:06:41','2018-02-09 09:06:41'),(58,1,'files/hfgfgfgfd.jpg','e4813d7d88d47e9fe7cdc48b82ea4743',3,'hfgfgfgfd.jpg',7,0,258053,1,1,0,0,NULL,65,NULL,'2018-02-27 10:47:08','2018-02-09 12:54:29'),(59,1,'files/hashTags.pptx','d99218531e86247f049fda8804181712',3,'hashTags.pptx',3,0,563741,1,1,0,0,NULL,65,NULL,'2018-03-02 10:26:21','2018-03-02 10:26:21'),(60,1,'files/JWT.odt','70aff380224bff657920a0e48545c2f5',3,'JWT.odt',5,0,468017,1,1,0,0,NULL,65,NULL,'2018-03-02 10:26:28','2018-03-02 10:26:28');
/*!40000 ALTER TABLE `gf_files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_files_locks`
--

DROP TABLE IF EXISTS `gf_files_locks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_files_locks` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `lock` int(11) NOT NULL DEFAULT '0',
  `key` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `ttl` int(11) NOT NULL DEFAULT '-1',
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lock_key_UNIQUE` (`key`),
  KEY `lock_ttl_KEY` (`ttl`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_files_locks`
--

LOCK TABLES `gf_files_locks` WRITE;
/*!40000 ALTER TABLE `gf_files_locks` DISABLE KEYS */;
/*!40000 ALTER TABLE `gf_files_locks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_files_share`
--

DROP TABLE IF EXISTS `gf_files_share`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_files_share` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `file_id` bigint(10) unsigned NOT NULL,
  `owner_id` bigint(10) unsigned NOT NULL,
  `shared_id` bigint(10) unsigned NOT NULL,
  `initiator_id` bigint(10) unsigned NOT NULL,
  `share_expiration` datetime DEFAULT NULL,
  `share_type` smallint(6) NOT NULL DEFAULT '0',
  `share_edit` smallint(6) NOT NULL DEFAULT '1',
  `share_details` smallint(6) NOT NULL DEFAULT '1',
  `share_others` smallint(6) NOT NULL DEFAULT '0',
  `share_status` smallint(6) NOT NULL DEFAULT '1',
  `mail_send` smallint(6) NOT NULL DEFAULT '0',
  `share_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `share_hash` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_files_share_file_id_idx` (`file_id`),
  CONSTRAINT `fk_files_share_file_id` FOREIGN KEY (`file_id`) REFERENCES `gf_files` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_files_share`
--

LOCK TABLES `gf_files_share` WRITE;
/*!40000 ALTER TABLE `gf_files_share` DISABLE KEYS */;
INSERT INTO `gf_files_share` VALUES (1,56,1,2,1,NULL,1,1,0,0,1,1,'','02f88f09f2f0430a5a06eaa0b04c91e7','2018-02-09 11:07:49','2018-02-09 11:07:49'),(2,7,1,2,1,NULL,1,2,2,1,1,1,'','5bbbc5c6830c6817b21a4f415bd5d26c','2018-02-09 11:08:18','2018-02-09 11:08:18'),(3,15,1,2,1,NULL,1,1,0,0,1,1,'','c6a45ae9b27750f96dd14b7bdee3cf9c','2018-02-09 11:08:47','2018-02-09 11:08:47'),(4,58,1,2,1,NULL,1,2,2,1,1,1,'','91ae61dd3ed62d344d0792f516b6459e','2018-02-09 12:58:04','2018-02-09 12:58:04');
/*!40000 ALTER TABLE `gf_files_share` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_files_tags`
--

DROP TABLE IF EXISTS `gf_files_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_files_tags` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `file_id` bigint(10) unsigned NOT NULL,
  `tag_id` bigint(10) unsigned NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_file_id` (`file_id`),
  KEY `fk_tag_id` (`tag_id`),
  CONSTRAINT `fk_file_id` FOREIGN KEY (`file_id`) REFERENCES `gf_files` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tag_id` FOREIGN KEY (`tag_id`) REFERENCES `gf_tags` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_files_tags`
--

LOCK TABLES `gf_files_tags` WRITE;
/*!40000 ALTER TABLE `gf_files_tags` DISABLE KEYS */;
INSERT INTO `gf_files_tags` VALUES (1,58,1,'2018-03-02 17:46:37','2018-03-02 17:46:37'),(2,58,2,'2018-03-02 17:46:37','2018-03-02 17:46:37'),(3,58,3,'2018-03-02 17:46:37','2018-03-02 17:46:37'),(4,58,4,'2018-03-02 17:46:37','2018-03-02 17:46:37'),(5,58,5,'2018-03-02 17:46:37','2018-03-02 17:46:37'),(6,58,6,'2018-03-02 17:46:37','2018-03-02 17:46:37'),(7,6,7,'2018-03-02 17:46:37','2018-03-02 17:46:37'),(8,6,8,'2018-03-02 17:46:37','2018-03-02 17:46:37'),(9,6,9,'2018-03-02 17:46:37','2018-03-02 17:46:37'),(10,6,10,'2018-03-02 17:46:37','2018-03-02 17:46:37'),(11,6,11,'2018-03-02 17:46:37','2018-03-02 17:46:37');
/*!40000 ALTER TABLE `gf_files_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_files_trash`
--

DROP TABLE IF EXISTS `gf_files_trash`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_files_trash` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `file_id` bigint(10) unsigned NOT NULL,
  `name` varchar(250) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_id` bigint(10) unsigned NOT NULL,
  `timestamp` varchar(12) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `location` varchar(512) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `type` varchar(4) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `mime` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name_KEY` (`name`),
  KEY `timestamp_KEY` (`timestamp`),
  KEY `user_KEY` (`user_id`),
  KEY `fk_files_trash_file_id_idx` (`file_id`),
  CONSTRAINT `fk_files_trash_file_id` FOREIGN KEY (`file_id`) REFERENCES `gf_files` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_files_trash_user_id` FOREIGN KEY (`user_id`) REFERENCES `gf_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_files_trash`
--

LOCK TABLES `gf_files_trash` WRITE;
/*!40000 ALTER TABLE `gf_files_trash` DISABLE KEYS */;
/*!40000 ALTER TABLE `gf_files_trash` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_fonts`
--

DROP TABLE IF EXISTS `gf_fonts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_fonts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `file_name` varchar(45) DEFAULT NULL,
  `font_type` int(11) DEFAULT '1',
  `css_stack` varchar(255) DEFAULT NULL,
  `weight` int(11) DEFAULT '400',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_fonts`
--

LOCK TABLES `gf_fonts` WRITE;
/*!40000 ALTER TABLE `gf_fonts` DISABLE KEYS */;
/*!40000 ALTER TABLE `gf_fonts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_mediaqueries`
--

DROP TABLE IF EXISTS `gf_mediaqueries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_mediaqueries` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `media_query` varchar(255) DEFAULT '',
  `body_css` json DEFAULT NULL,
  `gf_components_id` bigint(10) unsigned NOT NULL,
  `gf_mediaquery_blueprints_id` bigint(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gf_mediaqueries_gf_components1_idx` (`gf_components_id`),
  KEY `fk_gf_mediaqueries_gf_mediaquery_blueprints1_idx` (`gf_mediaquery_blueprints_id`),
  CONSTRAINT `fk_gf_mediaqueries_gf_components1` FOREIGN KEY (`gf_components_id`) REFERENCES `gf_components` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_gf_mediaqueries_gf_mediaquery_blueprints1` FOREIGN KEY (`gf_mediaquery_blueprints_id`) REFERENCES `gf_mediaquery_blueprints` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_mediaqueries`
--

LOCK TABLES `gf_mediaqueries` WRITE;
/*!40000 ALTER TABLE `gf_mediaqueries` DISABLE KEYS */;
INSERT INTO `gf_mediaqueries` VALUES (16,'','[{\"selectors\": \".main\", \"properties\": []}]',1,16),(17,'','[{\"selectors\": \".image > img\", \"properties\": []}, {\"selectors\": \".image > img:hover\", \"properties\": []}]',3,48);
/*!40000 ALTER TABLE `gf_mediaqueries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_mediaquery_blueprints`
--

DROP TABLE IF EXISTS `gf_mediaquery_blueprints`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_mediaquery_blueprints` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `media_query` varchar(255) DEFAULT '',
  `body_css` json DEFAULT NULL,
  `gf_component_blueprints_id` bigint(10) unsigned NOT NULL,
  `gf_theme_blueprints_id` bigint(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gf_mediaqueries_gf_component_blueprints1_idx` (`gf_component_blueprints_id`),
  KEY `fk_gf_mediaquery_blueprints_gf_theme_blueprints1_idx` (`gf_theme_blueprints_id`),
  CONSTRAINT `fk_gf_mediaqueries_gf_component_blueprints1` FOREIGN KEY (`gf_component_blueprints_id`) REFERENCES `gf_component_blueprints` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_gf_mediaquery_blueprints_gf_theme_blueprints1` FOREIGN KEY (`gf_theme_blueprints_id`) REFERENCES `gf_theme_blueprints` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_mediaquery_blueprints`
--

LOCK TABLES `gf_mediaquery_blueprints` WRITE;
/*!40000 ALTER TABLE `gf_mediaquery_blueprints` DISABLE KEYS */;
INSERT INTO `gf_mediaquery_blueprints` VALUES (1,'','[{\"selectors\": [\".section-a\"], \"properties\": {\"min-height\": \"5vw\"}}, {\"selectors\": [\".section-b\"], \"properties\": {\"min-height\": \"5vw\"}}, {\"selectors\": [\".section-c\"], \"properties\": {\"min-height\": \"5vw\"}}, {\"selectors\": [\".section-a\"], \"properties\": {\"border\": \"1px solid #f55\", \"display\": \"block\"}}, {\"selectors\": [\".flex\"], \"properties\": {\"display\": \"flex\", \"padding\": \"1vw 0\", \"flex-wrap\": \"nowrap\", \"align-items\": \"stretch\", \"align-content\": \"flex-start\", \"flex-direction\": \"row\", \"justify-content\": \"space-between\"}}, {\"selectors\": [\".section-b\"], \"properties\": {\"border\": \"1px solid #5f5\", \"flex-basis\": \"35%\"}}, {\"selectors\": [\".section-c\"], \"properties\": {\"border\": \"1px solid #55f\", \"flex-basis\": \"60%\"}}]',4,1),(2,'@media (max-width:800px)','[{\"selectors\": [\".flex\"], \"properties\": {\"display\": \"flex\", \"padding\": \"1vw 0\", \"flex-wrap\": \"nowrap\", \"align-items\": \"stretch\", \"align-content\": \"flex-start\", \"flex-direction\": \"column\", \"justify-content\": \"space-between\"}}]',4,1),(3,'','[{\"selectors\": [\".section-a\"], \"properties\": {\"min-height\": \"5vw\"}}, {\"selectors\": [\".section-b\"], \"properties\": {\"min-height\": \"5vw\"}}, {\"selectors\": [\".section-c\"], \"properties\": {\"min-height\": \"5vw\"}}, {\"selectors\": [\".section-a\"], \"properties\": {\"border\": \"1px solid #f55\", \"display\": \"block\"}}, {\"selectors\": [\".flex\"], \"properties\": {\"display\": \"flex\", \"padding\": \"1vw 0\", \"flex-wrap\": \"nowrap\", \"align-items\": \"stretch\", \"align-content\": \"flex-start\", \"flex-direction\": \"row\", \"justify-content\": \"space-between\"}}, {\"selectors\": [\".section-b\"], \"properties\": {\"border\": \"1px solid #5f5\", \"flex-basis\": \"35%\"}}, {\"selectors\": [\".section-c\"], \"properties\": {\"border\": \"1px solid #55f\", \"flex-basis\": \"60%\"}}]',4,2),(4,'@media (max-width:800px)','[{\"selectors\": [\".flex\"], \"properties\": {\"display\": \"flex\", \"padding\": \"1vw 0\", \"flex-wrap\": \"nowrap\", \"align-items\": \"stretch\", \"align-content\": \"flex-start\", \"flex-direction\": \"column\", \"justify-content\": \"space-between\"}}]',4,2),(5,'','[{\"selectors\": [\".section-a\"], \"properties\": {\"min-height\": \"5vw\"}}, {\"selectors\": [\".section-b\"], \"properties\": {\"min-height\": \"5vw\"}}, {\"selectors\": [\".section-c\"], \"properties\": {\"min-height\": \"5vw\"}}, {\"selectors\": [\".section-a\"], \"properties\": {\"border\": \"1px solid #f55\", \"display\": \"block\"}}, {\"selectors\": [\".flex\"], \"properties\": {\"display\": \"flex\", \"padding\": \"1vw 0\", \"flex-wrap\": \"nowrap\", \"align-items\": \"stretch\", \"align-content\": \"flex-start\", \"flex-direction\": \"row\", \"justify-content\": \"space-between\"}}, {\"selectors\": [\".section-b\"], \"properties\": {\"border\": \"1px solid #5f5\", \"flex-basis\": \"35%\"}}, {\"selectors\": [\".section-c\"], \"properties\": {\"border\": \"1px solid #55f\", \"flex-basis\": \"60%\"}}]',4,3),(6,'@media (max-width:800px)','[{\"selectors\": [\".flex\"], \"properties\": {\"display\": \"flex\", \"padding\": \"1vw 0\", \"flex-wrap\": \"nowrap\", \"align-items\": \"stretch\", \"align-content\": \"flex-start\", \"flex-direction\": \"column\", \"justify-content\": \"space-between\"}}]',4,3),(7,'','[{\"selectors\": [\".section-a\"], \"properties\": {\"min-height\": \"5vw\"}}, {\"selectors\": [\".section-b\"], \"properties\": {\"min-height\": \"5vw\"}}, {\"selectors\": [\".section-c\"], \"properties\": {\"min-height\": \"5vw\"}}, {\"selectors\": [\".section-a\"], \"properties\": {\"border\": \"1px solid #f55\", \"display\": \"block\"}}, {\"selectors\": [\".flex\"], \"properties\": {\"display\": \"flex\", \"padding\": \"1vw 0\", \"flex-wrap\": \"nowrap\", \"align-items\": \"stretch\", \"align-content\": \"flex-start\", \"flex-direction\": \"row\", \"justify-content\": \"space-between\"}}, {\"selectors\": [\".section-b\"], \"properties\": {\"border\": \"1px solid #5f5\", \"flex-basis\": \"35%\"}}, {\"selectors\": [\".section-c\"], \"properties\": {\"border\": \"1px solid #55f\", \"flex-basis\": \"60%\"}}]',4,4),(8,'@media (max-width:800px)','[{\"selectors\": [\".flex\"], \"properties\": {\"display\": \"flex\", \"padding\": \"1vw 0\", \"flex-wrap\": \"nowrap\", \"align-items\": \"stretch\", \"align-content\": \"flex-start\", \"flex-direction\": \"column\", \"justify-content\": \"space-between\"}}]',4,4),(9,'','[{\"selectors\": [\".section-a\"], \"properties\": {\"min-height\": \"5vw\"}}, {\"selectors\": [\".section-b\"], \"properties\": {\"min-height\": \"5vw\"}}, {\"selectors\": [\".section-c\"], \"properties\": {\"min-height\": \"5vw\"}}, {\"selectors\": [\".section-a\"], \"properties\": {\"border\": \"1px solid #f55\", \"display\": \"block\"}}, {\"selectors\": [\".flex\"], \"properties\": {\"display\": \"flex\", \"padding\": \"1vw 0\", \"flex-wrap\": \"nowrap\", \"align-items\": \"stretch\", \"align-content\": \"flex-start\", \"flex-direction\": \"row\", \"justify-content\": \"space-between\"}}, {\"selectors\": [\".section-b\"], \"properties\": {\"border\": \"1px solid #5f5\", \"flex-basis\": \"35%\"}}, {\"selectors\": [\".section-c\"], \"properties\": {\"border\": \"1px solid #55f\", \"flex-basis\": \"60%\"}}]',4,5),(10,'@media (max-width:800px)','[{\"selectors\": [\".flex\"], \"properties\": {\"display\": \"flex\", \"padding\": \"1vw 0\", \"flex-wrap\": \"nowrap\", \"align-items\": \"stretch\", \"align-content\": \"flex-start\", \"flex-direction\": \"column\", \"justify-content\": \"space-between\"}}]',4,5),(11,'','[{\"selectors\": [], \"properties\": []}]',5,1),(12,'','[{\"selectors\": [], \"properties\": []}]',5,2),(13,'','[{\"selectors\": [], \"properties\": []}]',5,3),(14,'','[{\"selectors\": [], \"properties\": []}]',5,4),(15,'','[{\"selectors\": [], \"properties\": []}]',5,5),(16,'','[{\"selectors\": [\".main\"], \"properties\": {\"height\": \"100%\", \"margin\": \"0\", \"padding\": \"0\", \"position\": \"relative\", \"box-shadow\": \"0px 0px 4px rgba(0, 0, 0, 0.5)\", \"background-color\": \"transparent\"}}]',1,1),(17,'','[{\"selectors\": [\".main\"], \"properties\": {\"height\": \"100%\", \"margin\": \"0\", \"padding\": \"0\", \"position\": \"relative\", \"box-shadow\": \"0px 0px 4px rgba(0, 0, 0, 0.5)\", \"background-color\": \"transparent\"}}]',1,2),(18,'','[{\"selectors\": [\".main\"], \"properties\": {\"height\": \"100%\", \"margin\": \"0\", \"padding\": \"0\", \"position\": \"relative\", \"box-shadow\": \"0px 0px 4px rgba(0, 0, 0, 0.5)\", \"background-color\": \"transparent\"}}]',1,3),(19,'','[{\"selectors\": [\".main\"], \"properties\": {\"height\": \"100%\", \"margin\": \"0\", \"padding\": \"0\", \"position\": \"relative\", \"box-shadow\": \"0px 0px 4px rgba(0, 0, 0, 0.5)\", \"background-color\": \"transparent\"}}]',1,4),(20,'','[{\"selectors\": [\".main\"], \"properties\": {\"height\": \"100%\", \"margin\": \"0\", \"padding\": \"0\", \"position\": \"relative\", \"box-shadow\": \"0px 0px 4px rgba(0, 0, 0, 0.5)\", \"background-color\": \"transparent\"}}]',1,5),(21,'','[{\"selectors\": [\"nav\"], \"properties\": {\"background-color\": \"#86a873\"}}, {\"selectors\": [\"ul\"], \"properties\": {\"padding\": \"2vw 1vw\", \"list-style\": \"none\"}}, {\"selectors\": [\"li\"], \"properties\": {\"list-style\": \"none\"}}, {\"selectors\": [\"li a\"], \"properties\": {\"color\": \"#087f8c\", \"font-size\": \"1.5vw\", \"text-decoration\": \"none\"}}]',2,1),(22,'@media (max-width:800px)','[{\"selectors\": [\"ul\"], \"properties\": {\"display\": \"flex\", \"flex-wrap\": \"nowrap\", \"align-items\": \"stretch\", \"align-content\": \"flex-start\", \"flex-direction\": \"row\", \"justify-content\": \"space- between\"}}, {\"selectors\": [\"li a\"], \"properties\": {\"font-size\": \"14px\"}}]',2,1),(23,'','[{\"selectors\": [\"nav\"], \"properties\": {\"background-color\": \"#d8b4a0\"}}, {\"selectors\": [\"ul\"], \"properties\": {\"padding\": \"2vw 1vw\", \"list-style\": \"none\"}}, {\"selectors\": [\"li\"], \"properties\": {\"list-style\": \"none\"}}, {\"selectors\": [\"li a\"], \"properties\": {\"color\": \"#dbd3d8\", \"font-size\": \"1.5vw\", \"text-decoration\": \"none\"}}]',2,2),(24,'@media (max-width:800px)','[{\"selectors\": [\"ul\"], \"properties\": {\"display\": \"flex\", \"flex-wrap\": \"nowrap\", \"align-items\": \"stretch\", \"align-content\": \"flex-start\", \"flex-direction\": \"row\", \"justify-content\": \"space- between\"}}, {\"selectors\": [\"li a\"], \"properties\": {\"font-size\": \"14px\"}}]',2,2),(25,'','[{\"selectors\": [\"nav\"], \"properties\": {\"background-color\": null}}, {\"selectors\": [\"ul\"], \"properties\": {\"padding\": \"2vw 1vw\", \"list-style\": \"none\"}}, {\"selectors\": [\"li\"], \"properties\": {\"list-style\": \"none\"}}, {\"selectors\": [\"li a\"], \"properties\": {\"color\": \"#191516\", \"font-size\": \"1.5vw\", \"text-decoration\": \"none\"}}]',2,3),(26,'@media (max-width:800px)','[{\"selectors\": [\"ul\"], \"properties\": {\"display\": \"flex\", \"flex-wrap\": \"nowrap\", \"align-items\": \"stretch\", \"align-content\": \"flex-start\", \"flex-direction\": \"row\", \"justify-content\": \"space- between\"}}, {\"selectors\": [\"li a\"], \"properties\": {\"font-size\": \"14px\"}}]',2,3),(27,'','[{\"selectors\": [\"nav\"], \"properties\": {\"background-color\": \"#555\"}}, {\"selectors\": [\"ul\"], \"properties\": {\"padding\": \"2vw 1vw\", \"list-style\": \"none\"}}, {\"selectors\": [\"li\"], \"properties\": {\"list-style\": \"none\"}}, {\"selectors\": [\"li a\"], \"properties\": {\"color\": \"#111\", \"font-size\": \"1.5vw\", \"text-decoration\": \"none\"}}]',2,4),(28,'@media (max-width:800px)','[{\"selectors\": [\"ul\"], \"properties\": {\"display\": \"flex\", \"flex-wrap\": \"nowrap\", \"align-items\": \"stretch\", \"align-content\": \"flex-start\", \"flex-direction\": \"row\", \"justify-content\": \"space- between\"}}, {\"selectors\": [\"li a\"], \"properties\": {\"font-size\": \"14px\"}}]',2,4),(29,'','[{\"selectors\": [\"nav\"], \"properties\": {\"background-color\": \"#555\"}}, {\"selectors\": [\"ul\"], \"properties\": {\"padding\": \"2vw 1vw\", \"list-style\": \"none\"}}, {\"selectors\": [\"li\"], \"properties\": {\"list-style\": \"none\"}}, {\"selectors\": [\"li a\"], \"properties\": {\"color\": \"#fff\", \"font-size\": \"1.5vw\", \"text-decoration\": \"none\"}}]',2,5),(30,'@media (max-width:800px)','[{\"selectors\": [\"ul\"], \"properties\": {\"display\": \"flex\", \"flex-wrap\": \"nowrap\", \"align-items\": \"stretch\", \"align-content\": \"flex-start\", \"flex-direction\": \"row\", \"justify-content\": \"space- between\"}}, {\"selectors\": [\"li a\"], \"properties\": {\"font-size\": \"14px\"}}]',2,5),(31,'','[{\"selectors\": [], \"properties\": []}]',6,1),(32,'','[{\"selectors\": [], \"properties\": []}]',6,2),(33,'','[{\"selectors\": [], \"properties\": []}]',6,3),(34,'','[{\"selectors\": [], \"properties\": []}]',6,4),(35,'','[{\"selectors\": [], \"properties\": []}]',6,5),(36,'','[{\"selectors\": [\"h1\"], \"properties\": {\"color\": \"#087f8c\", \"margin\": \"0\", \"padding\": \"2vw 1em\", \"font-size\": \"2vw\", \"font-family\": \"Roboto, sans-serif\", \"font-weight\": \"normal\", \"background-color\": \"#095256\"}}]',3,1),(37,'@media (max-width:800px)','[{\"selectors\": [\"h1\"], \"properties\": {\"margin\": \"1em 0\", \"font-size\": \"24px\"}}]',3,1),(38,'','[{\"selectors\": [\"h1\"], \"properties\": {\"color\": \"#dbd3d8\", \"margin\": \"0\", \"padding\": \"2vw 1em\", \"font-size\": \"2vw\", \"font-family\": \"Roboto, sans-serif\", \"font-weight\": \"normal\", \"background-color\": \"#223843\"}}]',3,2),(39,'@media (max-width:800px)','[{\"selectors\": [\"h1\"], \"properties\": {\"margin\": \"1em 0\", \"font-size\": \"24px\"}}]',3,2),(40,'','[{\"selectors\": [\"h1\"], \"properties\": {\"color\": \"#191516\", \"margin\": \"0\", \"padding\": \"2vw 1em\", \"font-size\": \"2vw\", \"font-family\": \"Roboto, sans-serif\", \"font-weight\": \"normal\", \"background-color\": \"#00b295\"}}]',3,3),(41,'@media (max-width:800px)','[{\"selectors\": [\"h1\"], \"properties\": {\"margin\": \"1em 0\", \"font-size\": \"24px\"}}]',3,3),(42,'','[{\"selectors\": [\"h1\"], \"properties\": {\"color\": \"#111\", \"margin\": \"0\", \"padding\": \"2vw 1em\", \"font-size\": \"2vw\", \"font-family\": \"Roboto, sans-serif\", \"font-weight\": \"normal\", \"background-color\": \"#fff\"}}]',3,4),(43,'@media (max-width:800px)','[{\"selectors\": [\"h1\"], \"properties\": {\"margin\": \"1em 0\", \"font-size\": \"24px\"}}]',3,4),(44,'','[{\"selectors\": [\"h1\"], \"properties\": {\"color\": \"#fff\", \"margin\": \"0\", \"padding\": \"2vw 1em\", \"font-size\": \"2vw\", \"font-family\": \"Roboto, sans-serif\", \"font-weight\": \"normal\", \"background-color\": \"#000\"}}]',3,5),(45,'@media (max-width:800px)','[{\"selectors\": [\"h1\"], \"properties\": {\"margin\": \"1em 0\", \"font-size\": \"24px\"}}]',3,5),(46,'','[{\"selectors\": [\".image > img\"], \"properties\": {\"width\": \"100%\", \"border\": \"1px solid #ddd\", \"height\": \"auto\", \"padding\": \"5px\", \"box-shadow\": \"0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)\", \"border-radius\": \"4px\"}}, {\"selectors\": [\".image > img:hover\"], \"properties\": {\"box-shadow\": \"0 0 2px 1px rgba(0, 140, 186, 0.5)\"}}]',7,1),(47,'','[{\"selectors\": [\".image > img\"], \"properties\": {\"width\": \"100%\", \"border\": \"1px solid #ddd\", \"height\": \"auto\", \"padding\": \"5px\", \"box-shadow\": \"0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)\", \"border-radius\": \"4px\"}}, {\"selectors\": [\".image > img:hover\"], \"properties\": {\"box-shadow\": \"0 0 2px 1px rgba(0, 140, 186, 0.5)\"}}]',7,2),(48,'','[{\"selectors\": [\".image > img\"], \"properties\": {\"width\": \"100%\", \"border\": \"1px solid #ddd\", \"height\": \"auto\", \"padding\": \"5px\", \"box-shadow\": \"0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)\", \"border-radius\": \"4px\"}}, {\"selectors\": [\".image > img:hover\"], \"properties\": {\"box-shadow\": \"0 0 2px 1px rgba(0, 140, 186, 0.5)\"}}]',7,3),(49,'','[{\"selectors\": [\".image > img\"], \"properties\": {\"width\": \"100%\", \"border\": \"1px solid #ddd\", \"height\": \"auto\", \"padding\": \"5px\", \"box-shadow\": \"0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)\", \"border-radius\": \"4px\"}}, {\"selectors\": [\".image > img:hover\"], \"properties\": {\"box-shadow\": \"0 0 2px 1px rgba(0, 140, 186, 0.5)\"}}]',7,4),(50,'','[{\"selectors\": [\".image > img\"], \"properties\": {\"width\": \"100%\", \"border\": \"1px solid #ddd\", \"height\": \"auto\", \"padding\": \"5px\", \"box-shadow\": \"0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)\", \"border-radius\": \"4px\"}}, {\"selectors\": [\".image > img:hover\"], \"properties\": {\"box-shadow\": \"0 0 2px 1px rgba(0, 140, 186, 0.5)\"}}]',7,5);
/*!40000 ALTER TABLE `gf_mediaquery_blueprints` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_mimetypes`
--

DROP TABLE IF EXISTS `gf_mimetypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_mimetypes` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `mimetype` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `icon` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT 'file',
  `ext` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mimetype_id_UNIQUE` (`mimetype`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_mimetypes`
--

LOCK TABLES `gf_mimetypes` WRITE;
/*!40000 ALTER TABLE `gf_mimetypes` DISABLE KEYS */;
INSERT INTO `gf_mimetypes` VALUES (1,'httpd','file','','2018-02-01 12:03:16','2018-02-01 12:03:16'),(2,'httpd/unix-directory','folder',NULL,'2018-02-01 12:03:16','2018-02-01 12:03:16'),(3,'application','file',NULL,'2018-02-01 12:03:16','2018-02-01 12:03:16'),(4,'application/pdf','pdf','.pdf','2018-02-01 12:03:16','2018-02-01 12:03:16'),(5,'application/vnd.oasis.opendocument.text','file','.odt','2018-02-01 12:03:16','2018-02-01 12:03:16'),(6,'image','img','','2018-02-01 12:03:16','2018-02-01 12:03:16'),(7,'image/jpg','jpg','.jpg','2018-02-01 12:03:16','2018-02-01 12:03:16'),(8,'image/png','png','.png','2018-02-01 12:03:16','2018-02-01 12:03:16'),(9,'application/octet-stream','file',NULL,'2018-02-01 12:03:16','2018-02-01 12:03:16'),(10,'image/svg+xml','svg','.svg','2018-02-01 12:03:16','2018-02-01 12:03:16'),(11,'application/vnd.ms-powerpoint','ppt','.ppt','2018-02-01 12:03:16','2018-02-01 12:03:16'),(12,'application/zip','zip','.zip','2018-02-01 12:03:16','2018-02-01 12:03:16'),(13,'video/x-msvideo','avi','.avi','2018-02-01 12:03:16','2018-02-01 12:03:16'),(14,'text/css','css','.css','2018-02-01 12:03:16','2018-02-01 12:03:16'),(15,'text/csv','csv','.csv','2018-02-01 12:03:16','2018-02-01 12:03:16'),(16,'application/msword','doc','.doc','2018-02-01 12:03:16','2018-02-01 12:03:16'),(17,'application/x-shockwave-flash','swf','.swf','2018-02-01 12:03:16','2018-02-01 12:03:16'),(18,'text/html','html','.html','2018-02-01 12:03:16','2018-02-01 12:03:16'),(19,'text/htm','html','.htm','2018-02-01 12:03:16','2018-02-01 12:03:16'),(20,'application/javascript','js','.js','2018-02-01 12:03:16','2018-02-01 12:03:16'),(21,'application/json','json','.json','2018-02-01 12:03:16','2018-02-01 12:03:16'),(22,'audio/mp3','mp3','.mp3','2018-02-01 12:03:16','2018-02-01 12:03:16'),(23,'video/mp4','mp4','.mp4','2018-02-01 12:03:16','2018-02-01 12:03:16'),(24,'application/psd','psd','.psd','2018-02-01 12:03:16','2018-02-01 12:03:16'),(25,'text/richtext','rtf','.rtf','2018-02-01 12:03:16','2018-02-01 12:03:16'),(26,'text','txt','.txt','2018-02-01 12:03:16','2018-02-01 12:03:16'),(27,'application/xls','xls','.xls','2018-02-01 12:03:16','2018-02-01 12:03:16'),(28,'text/xml','xml','.xml','2018-02-01 12:03:16','2018-02-01 12:03:16'),(29,'application/x-directory','folder',NULL,'2018-02-01 12:03:16','2018-02-01 12:03:16'),(30,'image/jpeg','jpg','.jpeg','2018-02-01 12:03:16','2018-02-01 12:03:16');
/*!40000 ALTER TABLE `gf_mimetypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_mounts`
--

DROP TABLE IF EXISTS `gf_mounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_mounts` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `storage_id` bigint(10) unsigned NOT NULL,
  `root_id` bigint(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `mount_point` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mounts_user_root_UNIQUE` (`user_id`,`root_id`),
  KEY `mounts_user_UNIQUE` (`user_id`),
  KEY `mounts_storage_UNIQUE` (`storage_id`),
  KEY `mounts_root_UNIQUE` (`root_id`),
  KEY `fk_mounts_storage_id_idx` (`storage_id`),
  CONSTRAINT `fk_mounts_storage_id` FOREIGN KEY (`storage_id`) REFERENCES `gf_storages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_mounts`
--

LOCK TABLES `gf_mounts` WRITE;
/*!40000 ALTER TABLE `gf_mounts` DISABLE KEYS */;
INSERT INTO `gf_mounts` VALUES (1,1,1,1,'/matsfjellner@catedra.edu.co/','2018-02-01 12:03:16','2018-02-01 12:03:16'),(3,3,1,2,'/danielvillanueva@catedra.edu.co/','2018-02-09 08:09:10','2018-02-09 08:09:10'),(4,4,1,4,'/duvanvalencia@catedra.edu.co/','2018-02-09 08:09:45','2018-02-09 08:09:45'),(5,5,1,3,'/xiomigarzon@pegui.edu.co/','2018-02-09 08:10:15','2018-02-09 08:10:15');
/*!40000 ALTER TABLE `gf_mounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_output_formats`
--

DROP TABLE IF EXISTS `gf_output_formats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_output_formats` (
  `id` bigint(10) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `description` mediumtext,
  `default_format` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_output_formats`
--

LOCK TABLES `gf_output_formats` WRITE;
/*!40000 ALTER TABLE `gf_output_formats` DISABLE KEYS */;
INSERT INTO `gf_output_formats` VALUES (1,'SCORM 1.2','Standard e-learning format since 2001',1),(2,'xAPI','Future format',NULL);
/*!40000 ALTER TABLE `gf_output_formats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_page_relations`
--

DROP TABLE IF EXISTS `gf_page_relations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_page_relations` (
  `gf_descendant` bigint(10) NOT NULL,
  `depth` int(11) DEFAULT NULL,
  `gf_ascendant` bigint(10) unsigned NOT NULL,
  `position` int(11) DEFAULT NULL,
  `root` tinyint(1) DEFAULT '0',
  KEY `fk_gf_page_relations_gf_pages1_idx` (`gf_ascendant`),
  CONSTRAINT `fk_gf_page_relations_gf_pages1` FOREIGN KEY (`gf_ascendant`) REFERENCES `gf_pages` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_page_relations`
--

LOCK TABLES `gf_page_relations` WRITE;
/*!40000 ALTER TABLE `gf_page_relations` DISABLE KEYS */;
INSERT INTO `gf_page_relations` VALUES (8,0,8,0,1);
/*!40000 ALTER TABLE `gf_page_relations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_pages`
--

DROP TABLE IF EXISTS `gf_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_pages` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `gf_unit_id` bigint(10) unsigned NOT NULL,
  `gf_components_id` bigint(10) unsigned NOT NULL,
  `saved` bigint(10) DEFAULT NULL,
  `render_html` mediumtext,
  `render_css` mediumtext,
  PRIMARY KEY (`id`),
  KEY `fk_gf_pages_gf_unit1_idx` (`gf_unit_id`),
  CONSTRAINT `fk_gf_pages_gf_unit1` FOREIGN KEY (`gf_unit_id`) REFERENCES `gf_units` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_pages`
--

LOCK TABLES `gf_pages` WRITE;
/*!40000 ALTER TABLE `gf_pages` DISABLE KEYS */;
INSERT INTO `gf_pages` VALUES (8,'adadaadadad',1,1,20180209090252,NULL,NULL);
/*!40000 ALTER TABLE `gf_pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_project_has_components`
--

DROP TABLE IF EXISTS `gf_project_has_components`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_project_has_components` (
  `gf_projects_id` bigint(10) unsigned NOT NULL,
  `gf_component_blueprints_id` bigint(10) unsigned NOT NULL,
  KEY `fk_gf_project_has_components_gf_component_blueprints1_idx` (`gf_component_blueprints_id`),
  CONSTRAINT `fk_gf_project_has_components_gf_component_blueprints1` FOREIGN KEY (`gf_component_blueprints_id`) REFERENCES `gf_component_blueprints` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_project_has_components`
--

LOCK TABLES `gf_project_has_components` WRITE;
/*!40000 ALTER TABLE `gf_project_has_components` DISABLE KEYS */;
INSERT INTO `gf_project_has_components` VALUES (0,1),(0,2),(0,3),(0,4),(0,5),(0,6),(0,7);
/*!40000 ALTER TABLE `gf_project_has_components` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_project_has_themes`
--

DROP TABLE IF EXISTS `gf_project_has_themes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_project_has_themes` (
  `gf_projects_id` bigint(10) unsigned NOT NULL,
  `gf_theme_blueprints_id` bigint(10) unsigned NOT NULL,
  KEY `fk_gf_project_has_themes_gf_projects1_idx` (`gf_projects_id`),
  KEY `fk_gf_project_has_themes_gf_theme_blueprints1_idx` (`gf_theme_blueprints_id`),
  CONSTRAINT `fk_gf_project_has_themes_gf_projects1` FOREIGN KEY (`gf_projects_id`) REFERENCES `gf_projects` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_gf_project_has_themes_gf_theme_blueprints1` FOREIGN KEY (`gf_theme_blueprints_id`) REFERENCES `gf_theme_blueprints` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_project_has_themes`
--

LOCK TABLES `gf_project_has_themes` WRITE;
/*!40000 ALTER TABLE `gf_project_has_themes` DISABLE KEYS */;
INSERT INTO `gf_project_has_themes` VALUES (1,1),(1,2),(1,3),(1,4),(1,5),(3,1),(3,2),(3,3),(3,4),(3,5);
/*!40000 ALTER TABLE `gf_project_has_themes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_projects`
--

DROP TABLE IF EXISTS `gf_projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_projects` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `user_limit` int(4) DEFAULT '10',
  `remote_auth` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `gf_output_formats_id` bigint(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gf_projects_gf_output_formats1_idx` (`gf_output_formats_id`),
  CONSTRAINT `fk_gf_projects_gf_output_formats1` FOREIGN KEY (`gf_output_formats_id`) REFERENCES `gf_output_formats` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_projects`
--

LOCK TABLES `gf_projects` WRITE;
/*!40000 ALTER TABLE `gf_projects` DISABLE KEYS */;
INSERT INTO `gf_projects` VALUES (1,'Goldfish formación',10,'','Manuales de uso del editor',1),(2,'DIAN',10,'https://saga.pegui.edu.co/auth',NULL,1),(3,'Mintkula',10,'','Probando mas proyectos',2);
/*!40000 ALTER TABLE `gf_projects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_roles`
--

DROP TABLE IF EXISTS `gf_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_roles` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_roles`
--

LOCK TABLES `gf_roles` WRITE;
/*!40000 ALTER TABLE `gf_roles` DISABLE KEYS */;
INSERT INTO `gf_roles` VALUES (1,'Super'),(2,'Admin'),(3,'Editor'),(4,'Control de calidad'),(5,'Mantenimiento'),(6,'Diseño'),(7,'Animaciones'),(8,'Componentes'),(9,'Contacto cliente');
/*!40000 ALTER TABLE `gf_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_storages`
--

DROP TABLE IF EXISTS `gf_storages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_storages` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(10) unsigned NOT NULL,
  `user_path` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `available` int(11) NOT NULL DEFAULT '1',
  `last_checked` int(11) DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `storages_id_UNIQUE` (`id`),
  KEY `fk_storages_user_id` (`user_id`),
  CONSTRAINT `fk_storages_user_id` FOREIGN KEY (`user_id`) REFERENCES `gf_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_storages`
--

LOCK TABLES `gf_storages` WRITE;
/*!40000 ALTER TABLE `gf_storages` DISABLE KEYS */;
INSERT INTO `gf_storages` VALUES (1,1,'home::matsfjellner@catedra.edu.co',1,NULL,'2018-02-01 12:03:16','2018-02-01 12:03:16'),(3,2,'home::danielvillanueva@catedra.edu.co',1,1,'2018-02-09 08:09:10','2018-02-09 08:09:10'),(4,4,'home::duvanvalencia@catedra.edu.co',1,1,'2018-02-09 08:09:45','2018-02-09 08:09:45'),(5,3,'home::xiomigarzon@pegui.edu.co',1,1,'2018-02-09 08:10:15','2018-02-09 08:10:15');
/*!40000 ALTER TABLE `gf_storages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_tags`
--

DROP TABLE IF EXISTS `gf_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_tags` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` int(10) unsigned NOT NULL,
  `status` int(10) unsigned NOT NULL,
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `key` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tag_name_key` (`name`,`key`),
  KEY `tag_name_idx` (`name`),
  KEY `tag_key_idx` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_tags`
--

LOCK TABLES `gf_tags` WRITE;
/*!40000 ALTER TABLE `gf_tags` DISABLE KEYS */;
INSERT INTO `gf_tags` VALUES (1,1,1,'animal','animal','2018-03-01 17:56:41','2018-03-01 17:56:41'),(2,1,1,'imagen','image','2018-03-01 17:56:41','2018-03-01 17:56:41'),(3,1,1,'avatar','avatar','2018-03-01 17:56:41','2018-03-01 17:56:41'),(4,1,1,'icono','icon','2018-03-01 17:56:41','2018-03-01 17:56:41'),(5,1,1,'animacion','animation','2018-03-01 17:56:41','2018-03-01 17:56:41'),(6,1,1,'juego','game','2018-03-01 17:56:41','2018-03-01 17:56:41'),(7,1,1,'fondo','background','2018-03-01 17:56:41','2018-03-01 17:56:41'),(8,1,1,'botones','button','2018-03-01 17:56:41','2018-03-01 17:56:41'),(9,1,1,'multimedia','multimedia','2018-03-01 17:56:41','2018-03-01 17:56:41'),(10,1,1,'plantillas','template','2018-03-01 17:56:41','2018-03-01 17:56:41'),(11,1,1,'navegación','navigation','2018-03-01 17:56:41','2018-03-01 17:56:41');
/*!40000 ALTER TABLE `gf_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_tags_rel`
--

DROP TABLE IF EXISTS `gf_tags_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_tags_rel` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `tag_id` bigint(10) unsigned NOT NULL,
  `father_id` bigint(10) unsigned NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tag_father` (`tag_id`,`father_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_tags_rel`
--

LOCK TABLES `gf_tags_rel` WRITE;
/*!40000 ALTER TABLE `gf_tags_rel` DISABLE KEYS */;
/*!40000 ALTER TABLE `gf_tags_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_theme_blueprints`
--

DROP TABLE IF EXISTS `gf_theme_blueprints`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_theme_blueprints` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `folder` varchar(255) DEFAULT NULL,
  `description` mediumtext,
  `includes` mediumtext,
  `title` varchar(100) DEFAULT NULL,
  `version` bigint(10) unsigned DEFAULT NULL,
  `sass` mediumtext,
  `unitsass` mediumtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_theme_blueprints`
--

LOCK TABLES `gf_theme_blueprints` WRITE;
/*!40000 ALTER TABLE `gf_theme_blueprints` DISABLE KEYS */;
INSERT INTO `gf_theme_blueprints` VALUES (1,'goldfish','thumbnail.jpg','/themes/goldfish','The default Goldfish theme','[{\'css\',\'main.css\'},{\'js\',\'main.js\'}]','Goldfish',3,'$color1: rgba(9, 82, 86, 1);\n$color2: rgba(8, 127, 140, 1);\n$color3: rgba(90, 170, 149, 1);\n$color4: rgba(134, 168, 115, 1);\n$color5: rgba(187, 159, 6, 1);\n\n$textColor: $color2;\n$backgroundColor: $color1;\n$panelColor: $color4;\n$accentColor: $color5;',''),(2,'superpotatis','thumbnail.jpg','/themes/superpotatis','','','Superpotatis',3,'$color1: rgba(34, 56, 67, 1);\n$color2: rgba(239, 241, 243, 1);\n$color3: rgba(219, 211, 216, 1);\n$color4: rgba(216, 180, 160, 1);\n$color5: rgba(215, 122, 97, 1);\n\n$textColor: $color3;\n$backgroundColor: $color1;\n$panelColor: $color4;\n$accentColor: $color5;',''),(3,'linslus','thumbnail.jpg','/themes/linslus','','','Linslus',2,'$color1: rgba(201, 218, 234, 1);\n$color2: rgba(3, 247, 235, 1);\n$color3: rgba(0, 178, 149, 1);\n$color4: rgba(25, 21, 22, 1);\n$color5: rgba(171, 35, 70, 1);\n\n$textColor: $color4;\n$backgroundColor: $color3;\n$accentColor: $color5;',''),(4,'loparsko','thumbnail.jpg','/themes/loparsko','','','Löparsko',4,'$color1: rgba(255, 255, 255, 1);\n$color2: rgba(85, 85, 85, 1);\n$color3: rgba(17, 17, 17, 1);\n$color4: #f36b06;\n$color5: rgba(241, 211, 2, 1);\n\n$textColor: $color3;\n$backgroundColor: $color1;\n$panelColor: $color2;\n$accentColor: $color4;\n$titleColor: $color4;\n$mainFont: \'Raleway\';','body {\nmargin: 0;\nbackground-color: #2e4b49;\n\n\n&:after {\ncontent: \"\";\nbackground: url(#{$assetPath}wallhaven-558971.jpg) 50% 50% no-repeat;\nbackground-size: cover;\nbackground-attachment: fixed;\nposition:fixed;\n  top:0;\n  left:0;\n  right:0;\n  bottom:0;\n  opacity:0;\n  z-index:-1;\n  animation:bgFade 1s 1s forwards;\n}\n\n  > #main {\n    \n    > nav {\nz-index: 100;\n    background-color: rgba($panelColor, 0.8);\n    box-shadow: 0 0 0;\n    position: fixed;\nheight: 8vh;\ntop: 0;\nwidth: 100%;\ndisplay: flex;\n    flex-direction: row;\n    flex-wrap: wrap;\n    justify-content: flex-end;\n    align-items: stretch;\n  }\n\n  > nav a {\nposition: relative;\n    margin: 0 0;\n    padding: 1vw 2vw;\n    text-decoration: none;\n    color: $backgroundColor;\n    transition: background-color 0.3s;\n  }\n\n> nav a:before {\nposition: absolute;\n    content: \'\';\n    width: 1vw;\n    height: 1vw;\n    background-color: transparent;\n    left: 0.5vw;\n    top: 1.2vw;\ntransition: background-color 0.3s;\n}\n\n> nav a.sel, > nav a:hover {\ncolor: $backgroundColor;\nbackground-color: transparent;\n}\n\n  > nav a.sel:before, > nav a:hover:before {\n    background-color: $accentColor;\n    \n  }\n\n   #content {\nbackground-color: rgba($panelColor, 0.5);\npadding: 1vw;\n    flex-grow: 1;\nmargin: calc(8vh + 4vw) 3vw 3vw 3vw;\nopacity: 1;\n    transition: all ease-in-out 0.3s;\ntransform-style: preserve-3d;\nperspective: 2000px;\ntransform: rotateY(0deg);\n\n    &.out {\n      opacity: 0;\nmargin-left: 0;\ntransform: rotateY(-90deg);\n    }\n   \n\n> h2 {\n    font-size: 2vw;\n    position: absolute;\n    width: 100%;\n    text-align: center;\n    top: -4vw;\n    color: #fff;\n}\n   }\n\n  }\n  \n\n> h1 {\nposition: absolute;\nz-index: 200;\nmargin: 1.4vh 0 0 2vw;\nbox-sizing: border-box;\ntop: 0;\ncolor: $backgroundColor;\n    font-size: 2vw;\nborder-bottom: 0.3vh solid $accentColor;\n  }\n}\n\n@keyframes bgFade {\n    from {\n        opacity:0;\n    }\n    to {\n        opacity:1;\n    } \n}\n'),(5,'snorken','thumbnail.jpg','/themes/snorken','','','Snorken',5,'$color1: rgba(0, 0, 0, 1);\n$color2: rgba(85, 85, 85, 1);\n$color3: rgba(255, 255, 255, 1);\n$color4: #f36b06;\n$color5: rgba(241, 211, 2, 1);\n\n$textColor: $color3;\n$backgroundColor: $color1;\n$panelColor: $color2;\n$accentColor: $color4;\n$titleColor: $color4;\n$mainFont: \'Poiret one\';\n','body {\nmargin: 0;\nbackground-color: #e58735;\n--background-video: url(#{$assetPath}632325766.mp4);\n\n  > #main {\ndisplay: flex;\nflex-direction: row;\nflex-wrap: nowrap;\njustify-content: flex-start;\nalign-items: stretch;\nheight: 90vh;\nbox-sizing: border-box; \n    \n    > nav {\nz-index: 100;\nbackground-color: rgba($backgroundColor, 0.5);\n    box-shadow: 0 0 0;\nwidth: 20vw;\ndisplay: flex;\n    flex-direction: column;\n    flex-wrap: wrap;\n    justify-content: center;\n    align-items: stretch;\nalign-content: flex-start;\nbox-shadow: 0 0 5px rgba(255,215,0,0.3);\n  }\n\n  > nav a {\nposition: relative;\n    margin: 0 0;\n    padding: 1vw 2vw;\n    text-decoration: none;\n    color: $textColor;\n    transition: background-color 0.3s;\nbackground-color: rgba($backgroundColor,0);\ntransition: background-color 0.2s;\n  }\n\n> nav a.sel, > nav a:hover {\ncolor: $textColor;\nbackground-color: rgba($backgroundColor,0.5);\n}\n\n\n\n   #content {\nbackground-color: rgba($backgroundColor, 0.5);\npadding: 1vw;\n    flex-grow: 1;\nmargin: calc(8vh + 4vw) 3vw 3vw 3vw;\nbox-shadow: 0 0 5px rgba(255,215,0,0.3);\nopacity: 1;\n    transition: all ease-in-out 0.3s;\ntransform: scale(1);\n\n    &.out {\n      opacity: 0;\ntransform: scale(0);\n    }\n\n> h2 {\n    font-size: 2vw;\n    position: absolute;\n    width: 100%;\n    text-align: center;\n    top: -4vw;\n    color: #fff;\n}\n   }\n\n  }\n  \n\n> h1 {\nbackground-color: rgba($backgroundColor, 0.5);\nmargin: 0;\npadding: 0 0 0 2vw;\nbox-sizing: border-box;\nwidth: 20vw;\nheight: 10vh;\nline-height: 10vh;\ncolor: $textColor;\n    font-size: 4vw;\nbox-shadow: 0 0 5px rgba(255,215,0,0.3);\n  }\n}\n');
/*!40000 ALTER TABLE `gf_theme_blueprints` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_theme_instances`
--

DROP TABLE IF EXISTS `gf_theme_instances`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_theme_instances` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `changed` tinyint(1) DEFAULT '1',
  `body_css` mediumtext,
  `gf_theme_blueprints_id` bigint(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_theme_instance_gf_theme_blueprints1_idx` (`gf_theme_blueprints_id`),
  CONSTRAINT `fk_theme_instance_gf_theme_blueprints1` FOREIGN KEY (`gf_theme_blueprints_id`) REFERENCES `gf_theme_blueprints` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_theme_instances`
--

LOCK TABLES `gf_theme_instances` WRITE;
/*!40000 ALTER TABLE `gf_theme_instances` DISABLE KEYS */;
INSERT INTO `gf_theme_instances` VALUES (1,1,'',4),(2,1,'',1);
/*!40000 ALTER TABLE `gf_theme_instances` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_unit_has_users`
--

DROP TABLE IF EXISTS `gf_unit_has_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_unit_has_users` (
  `gf_unit_id` bigint(10) unsigned NOT NULL,
  `gf_users_id` bigint(10) unsigned NOT NULL,
  `gf_roles_id` bigint(10) unsigned NOT NULL,
  KEY `fk_gf_unit_has_gf_users_gf_users1_idx` (`gf_users_id`),
  KEY `fk_gf_unit_has_gf_users_gf_unit1_idx` (`gf_unit_id`),
  KEY `fk_gf_unit_has_gf_users_gf_roles1_idx` (`gf_roles_id`),
  CONSTRAINT `fk_gf_unit_has_gf_users_gf_roles1` FOREIGN KEY (`gf_roles_id`) REFERENCES `gf_roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_gf_unit_has_gf_users_gf_unit1` FOREIGN KEY (`gf_unit_id`) REFERENCES `gf_units` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_gf_unit_has_gf_users_gf_users1` FOREIGN KEY (`gf_users_id`) REFERENCES `gf_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_unit_has_users`
--

LOCK TABLES `gf_unit_has_users` WRITE;
/*!40000 ALTER TABLE `gf_unit_has_users` DISABLE KEYS */;
INSERT INTO `gf_unit_has_users` VALUES (1,1,3),(2,1,3);
/*!40000 ALTER TABLE `gf_unit_has_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_units`
--

DROP TABLE IF EXISTS `gf_units`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_units` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `saved` bigint(10) unsigned DEFAULT '0',
  `structure_json` mediumtext,
  `gf_theme_instance_id` bigint(10) unsigned NOT NULL,
  `gf_projects_id` bigint(10) unsigned NOT NULL,
  `gf_output_formats_id` bigint(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gf_units_theme_instance1_idx` (`gf_theme_instance_id`),
  KEY `fk_gf_units_gf_projects1_idx` (`gf_projects_id`),
  KEY `fk_gf_units_gf_output_formats1_idx` (`gf_output_formats_id`),
  CONSTRAINT `fk_gf_units_gf_output_formats1` FOREIGN KEY (`gf_output_formats_id`) REFERENCES `gf_output_formats` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_gf_units_gf_projects1` FOREIGN KEY (`gf_projects_id`) REFERENCES `gf_projects` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_gf_units_theme_instance1` FOREIGN KEY (`gf_theme_instance_id`) REFERENCES `gf_theme_instances` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_units`
--

LOCK TABLES `gf_units` WRITE;
/*!40000 ALTER TABLE `gf_units` DISABLE KEYS */;
INSERT INTO `gf_units` VALUES (1,'Ejemplo2',20180209090252,NULL,1,1,1),(2,'Ejemplo3',20180208153323,NULL,2,1,1);
/*!40000 ALTER TABLE `gf_units` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_user_configs`
--

DROP TABLE IF EXISTS `gf_user_configs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_user_configs` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `editor` varchar(50) DEFAULT NULL,
  `theme` varchar(50) DEFAULT NULL,
  `effects` tinyint(1) DEFAULT '0',
  `gf_users_id` bigint(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gf_user_config_gf_users1_idx` (`gf_users_id`),
  CONSTRAINT `fk_gf_user_config_gf_users1` FOREIGN KEY (`gf_users_id`) REFERENCES `gf_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_user_configs`
--

LOCK TABLES `gf_user_configs` WRITE;
/*!40000 ALTER TABLE `gf_user_configs` DISABLE KEYS */;
INSERT INTO `gf_user_configs` VALUES (2,'ck','dark',1,1);
/*!40000 ALTER TABLE `gf_user_configs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_user_has_projects_roles`
--

DROP TABLE IF EXISTS `gf_user_has_projects_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_user_has_projects_roles` (
  `gf_users_id` bigint(10) unsigned NOT NULL,
  `gf_projects_id` bigint(10) unsigned NOT NULL,
  `gf_roles_id` bigint(10) unsigned NOT NULL,
  KEY `fk_gf_users_has_gf_projects_gf_projects1_idx` (`gf_projects_id`),
  KEY `fk_gf_users_has_gf_projects_gf_users_idx` (`gf_users_id`),
  KEY `fk_gf_users_has_gf_projects_gf_roles1_idx` (`gf_roles_id`),
  CONSTRAINT `fk_gf_users_has_gf_projects_gf_projects1` FOREIGN KEY (`gf_projects_id`) REFERENCES `gf_projects` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_gf_users_has_gf_projects_gf_roles1` FOREIGN KEY (`gf_roles_id`) REFERENCES `gf_roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_gf_users_has_gf_projects_gf_users` FOREIGN KEY (`gf_users_id`) REFERENCES `gf_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_user_has_projects_roles`
--

LOCK TABLES `gf_user_has_projects_roles` WRITE;
/*!40000 ALTER TABLE `gf_user_has_projects_roles` DISABLE KEYS */;
INSERT INTO `gf_user_has_projects_roles` VALUES (1,1,2),(1,1,1),(1,2,2),(1,3,1),(1,3,2);
/*!40000 ALTER TABLE `gf_user_has_projects_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_users`
--

DROP TABLE IF EXISTS `gf_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_users` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `names` varchar(255) NOT NULL DEFAULT '',
  `surnames` varchar(255) NOT NULL DEFAULT '',
  `profile` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL DEFAULT '',
  `active` tinyint(1) DEFAULT '0',
  `password_salt` varchar(255) NOT NULL DEFAULT '',
  `token` char(16) DEFAULT NULL,
  `token_exp` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_users`
--

LOCK TABLES `gf_users` WRITE;
/*!40000 ALTER TABLE `gf_users` DISABLE KEYS */;
INSERT INTO `gf_users` VALUES (1,'Mats','Fjellner','1174915_10151710581102543_1404515987_n.jpg','matsfjellner@catedra.edu.co','12345',1,'foo',NULL,NULL),(2,'Daniel','Villanueva','daniel-profile.jpg','danielvillanueva@catedra.edu.co','123qwe',1,'dev',NULL,NULL),(3,'Xiomi','Garzon','','xiomigarzon@pegui.edu.co','12345',1,'pegui',NULL,NULL),(4,'Duvan','Valencia','','duvanvalencia@catedra.edu.co','12345',1,'dev',NULL,NULL);
/*!40000 ALTER TABLE `gf_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-06  9:44:15
