-- MySQL dump 10.15  Distrib 10.0.31-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: goldfishv2
-- ------------------------------------------------------
-- Server version	10.0.31-MariaDB-0ubuntu0.16.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `gf_component_blueprints`
--

DROP TABLE IF EXISTS `gf_component_blueprints`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_component_blueprints` (
  `id` bigint(10) unsigned NOT NULL,
  `body` mediumtext,
  `title` varchar(255) DEFAULT NULL,
  `repeatable` varchar(45) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `sass` mediumtext,
  `version` bigint(20) unsigned DEFAULT NULL,
  `data` mediumtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_component_blueprints`
--

LOCK TABLES `gf_component_blueprints` WRITE;
/*!40000 ALTER TABLE `gf_component_blueprints` DISABLE KEYS */;
INSERT INTO `gf_component_blueprints` VALUES (1,'<blocks />','Main','',1,'','main','.main { \n	height: 100%; \n	margin: 0; \n	padding: 0; \n	position: relative; \n	background-color: transparent; \n	box-shadow: 0px 0px 4px rgba(0,0,0,0.5); \n}',2,NULL),(2,'<nav><ul><li><a href=\"\" contenteditable=\"true\">Uno</a></li><li><a href=\"\" contenteditable=\"true\">Dos</a></li><li><a href=\"\" contenteditable=\"true\">Tres</a></li></ul></nav>','Lista de navegación','li',1,'','lista','nav { cat-editable-background-color: $panelColor; }\n\nul { list-style: none; padding: 2vw 1vw;}\nli { list-style: none; }\nli a { cat-editable-color: $textColor; text-decoration: none; cat-editable-font-size: 1.5vw }\n\n@media (max-width: 800px) {\n	ul {\n	display:flex;flex-direction:row;flex-wrap:nowrap;justify-content:space-\nbetween;align-items:stretch;align-content:flex-start}\n	li a {\n		font-size: 14px;}	\n}',2,NULL),(3,'<h1 contenteditable=\"true\">They don\'t think it be like it is, but it do</h1>','Bloque título','',1,'','titulo','h1 { background-color: $backgroundColor; padding: 2vw 1em; cat-editable-color: $textColor; margin: 0; font-family: Roboto, sans-serif; font-weight: normal; cat-editable-font-size: 2vw; }\n\n@media (max-width: 800px) {\n	h1 { margin: 1em 0; font-size: 24px; }\n}',2,NULL),(4,'<div class=\"section-a\"><blocks /></div><div class=\"flex\"><div class=\"section-b\"><blocks /></div><div class=\"section-c\"><blocks /></div></div>','Nav-Side-Main','',1,'','nav-side-main','.section-a {min-height:5vw}\n.section-b {min-height:5vw}\n.section-c{min-height:5vw}\n.section-a{border:1px solid #f55;display:block}\n.flex{\n	padding:1vw 0;\n	display:flex;\n	flex-direction:row;\n	flex-wrap:nowrap;\n	justify-content:space-between;\n	align-items:stretch;\n	align-content:flex-start\n}\n.section-b{\n	border:1px solid #5f5;\n	flex-basis:35%\n}\n.section-c{\n	border:1px solid #55f;\n	flex-basis:60%\n}\n\n@media (max-width: 800px) {\n	.flex {\n		padding:1vw 0;\n		display:flex;\n		flex-direction:column;\n		flex-wrap:nowrap;\n		justify-content:space-between;\n		align-items:stretch;\n		align-content:flex-start\n	}\n}',2,NULL),(5,'<div class=\"ck\"><content /></div>','HTML','',1,'','html','font-family: Roboto; font-size: 1rem;\n\n@media (max-width: 800px) {\n	font-size: 1.5rem;\n}',2,NULL),(6,'<div class=\"red ludica\"></div>','Lúdica','',2,'content/red/ludica','','',2,'{\n    \"id\": 0,\n    \"plugin_name\": \"ludica\",\n    \"xml\": \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\"?><root><metadata><created></created><lastmodified></lastmodified>        <id></id>        <indicadores>        </indicadores>        <description></description>        <autor></autor>        <title></title>    </metadata>    <userdata>        <transition>horizontalflip</transition>        <avatar>            <src>files/avatar.png</src>        </avatar>        <logo><![CDATA[files/muy_importante.png]]></logo>        <basecolor>#f2f4f3</basecolor>        <instancecolor>#109eb5</instancecolor>        <ludicTitle><![CDATA[Muy <strong>importante</strong>]]></ludicTitle>        <content>            <![CDATA[            <p>                The width of the background image therefore depends on the size of its container. If our container width is 500px, our image is resized to 250×250. <strong>Using a percentage can be useful</strong> for responsive designs. Resize the demonstration page to discover how the dimensions change.</p>            <p>                So why not <strong>apply the same principle</strong> to an 8 hour work day? Because there are limits on human productivity. The brain is just like a muscle. Can you continuously run on a treadmill for eight hours? Like our muscles, the brain needs occasional rest. The limit is a bit different for each individual. Through trial and error, I have found that 4 hours is my max.            </p>                ]]>        </content>    </userdata></root>\",\n    \"saves\": []\n}'),(7,'<div class=\"image\"></div>\n','Bloque\n','',1,'','block','',2,NULL);
/*!40000 ALTER TABLE `gf_component_blueprints` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_component_relations`
--

DROP TABLE IF EXISTS `gf_component_relations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_component_relations` (
  `gf_descendant` bigint(10) unsigned NOT NULL,
  `depth` int(11) DEFAULT NULL,
  `gf_ascendant` bigint(10) unsigned NOT NULL,
  `position` int(11) DEFAULT NULL,
  `block_group` int(10) unsigned DEFAULT '1',
  KEY `fk_gf_component_relations_gf_components1_idx` (`gf_ascendant`),
  CONSTRAINT `fk_gf_component_relations_gf_components1` FOREIGN KEY (`gf_ascendant`) REFERENCES `gf_components` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_component_relations`
--

LOCK TABLES `gf_component_relations` WRITE;
/*!40000 ALTER TABLE `gf_component_relations` DISABLE KEYS */;
INSERT INTO `gf_component_relations` VALUES (1,0,1,0,0),(2,1,1,0,0),(2,0,2,0,0),(3,0,3,0,0),(6,1,3,0,0),(6,0,6,0,0),(7,1,3,1,0),(7,0,7,1,0),(4,0,4,0,0),(5,0,5,0,0),(8,0,8,0,0),(9,0,9,0,0),(10,0,10,0,0),(11,0,11,0,0),(12,0,12,0,0),(13,0,13,0,0),(14,1,13,0,0),(14,0,14,0,0),(15,0,15,0,0),(16,0,16,0,0),(17,1,16,0,0),(17,0,17,0,0);
/*!40000 ALTER TABLE `gf_component_relations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_components`
--

DROP TABLE IF EXISTS `gf_components`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_components` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `saved` bigint(10) unsigned DEFAULT '0',
  `body` mediumtext,
  `title` varchar(255) DEFAULT NULL,
  `gf_component_blueprints_id` bigint(10) unsigned NOT NULL,
  `parent_id` bigint(10) unsigned NOT NULL,
  `repeatable` varchar(45) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `data` mediumtext,
  PRIMARY KEY (`id`),
  KEY `fk_gf_components_gf_component_blueprints1_idx` (`gf_component_blueprints_id`),
  CONSTRAINT `fk_gf_components_gf_component_blueprints1` FOREIGN KEY (`gf_component_blueprints_id`) REFERENCES `gf_component_blueprints` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_components`
--

LOCK TABLES `gf_components` WRITE;
/*!40000 ALTER TABLE `gf_components` DISABLE KEYS */;
INSERT INTO `gf_components` VALUES (1,20171213110806,'<blocks />',NULL,1,0,'',1,'',NULL),(2,20171213110806,'<h1 contenteditable=\"true\">They don\'t think it be like it is, but it do</h1>',NULL,3,1,'',1,'',NULL),(3,20171221165419,'<blocks />',NULL,1,0,'',1,'',NULL),(4,20171221165419,'<blocks />',NULL,1,0,'',1,'',NULL),(5,20171221165419,'<blocks />',NULL,1,0,'',1,'',NULL),(6,20171221165419,'<h1 contenteditable=\"true\">They don\'t think it be like it is, but it do</h1>',NULL,3,3,'',1,'',NULL),(7,20171221165419,'<nav><ul><li><a href=\"\" contenteditable=\"true\">Uno</a></li><li><a href=\"\" contenteditable=\"true\">Dos</a></li><li><a href=\"\" contenteditable=\"true\">Tres</a></li></ul></nav>',NULL,2,3,'li',1,'',NULL),(8,20171221165419,'<blocks />',NULL,1,0,'',1,'',NULL),(9,20171221165419,'<blocks />',NULL,1,0,'',1,'',NULL),(10,20171221165620,'<blocks />',NULL,1,0,'',1,'',NULL),(11,20171221165653,'<blocks />',NULL,1,0,'',1,'',NULL),(12,20171221171500,'<blocks />',NULL,1,0,'',1,'',NULL),(13,20180111151403,'<blocks />',NULL,1,0,'',1,'',NULL),(14,20180111151403,'<div class=\"red ludica\"></div>',NULL,6,13,'',2,'content/red/ludica','Array'),(15,20180111151729,'<blocks />',NULL,1,0,'',1,'',NULL),(16,20180111154902,'<blocks />',NULL,1,0,'',1,'',NULL),(17,20180111154902,'<div class=\"red ludica\"></div>',NULL,6,16,'',2,'content/red/ludica','Array');
/*!40000 ALTER TABLE `gf_components` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_files`
--

DROP TABLE IF EXISTS `gf_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_files` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `storage_id` bigint(10) unsigned NOT NULL DEFAULT '0',
  `path` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `path_hash` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `parent` bigint(20) NOT NULL DEFAULT '0',
  `name` varchar(250) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `mimetype` bigint(10) unsigned NOT NULL DEFAULT '0',
  `mimepart` int(11) NOT NULL DEFAULT '0',
  `size` bigint(20) NOT NULL DEFAULT '0',
  `mtime` int(11) NOT NULL DEFAULT '0',
  `storage_mtime` int(11) NOT NULL DEFAULT '0',
  `encrypted` int(11) NOT NULL DEFAULT '0',
  `unencrypted_size` bigint(20) NOT NULL DEFAULT '0',
  `etag` varchar(40) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `permissions` int(11) DEFAULT '0',
  `checksum` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `fs_storage_path_hash` (`storage_id`,`path_hash`),
  KEY `fs_parent_name_hash` (`parent`,`name`),
  KEY `fs_storage_mimetype` (`storage_id`,`mimetype`),
  KEY `fs_storage_mimepart` (`storage_id`,`mimepart`),
  KEY `fs_storage_size` (`storage_id`,`size`,`id`),
  KEY `fk_files_storage_id_idx` (`storage_id`),
  KEY `fk_files_mimetype_idx` (`mimetype`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_files`
--

LOCK TABLES `gf_files` WRITE;
/*!40000 ALTER TABLE `gf_files` DISABLE KEYS */;
INSERT INTO `gf_files` VALUES (1,1,'','5fec80051203937854109294db2bc638',-1,'',2,1,5809166,1507234318,1507234313,0,0,'59d6920e6dabe',23,'','2017-12-01 17:07:06','2017-12-01 17:07:06'),(2,1,'cache','adacb950169892fd4717f8a910a99101',1,'cache',2,1,0,1507234312,1507234312,0,0,'59d69208d0bc3',31,'','2017-12-01 17:07:06','2017-12-01 17:07:06'),(3,1,'files','7afa92ca5527a22fbb49856e03be1555',1,'files',2,1,0,1507740358,1507740358,0,0,'59de4ac6d513c',31,NULL,'2017-12-01 17:07:06','2017-12-01 17:07:06'),(4,1,'files/Documents','fea84d082b595a13f1795593fe1ad48e',3,'Documents',2,0,0,1,1,0,0,NULL,65,NULL,'2017-11-30 16:59:35','2017-11-30 16:59:35'),(5,1,'files/Photos','47ac79fa5217a2d694e22fff6adf8e38',3,'Photo',2,0,0,1,1,0,0,NULL,65,NULL,'2017-11-30 17:00:03','2017-11-30 17:00:03'),(6,1,'files/Manual-Backbone.pdf','300aecbffdc682cb5a6061cbaa7384fa',3,'Manual-Backbone.pdf',4,0,0,1,1,0,0,NULL,65,NULL,'2017-12-01 17:07:06','2017-12-01 17:07:06'),(7,1,'files/Documents/recetas.doc','0246da15081265c70d3cbb11b441e923',4,'recetas.doc',16,0,435712,1,1,0,0,NULL,65,NULL,'2017-11-30 17:01:49','2017-11-30 17:01:32'),(8,1,'files/Photos/backbone.png','b271093a4cd3b61d5807fe1c3f24edcd',5,'backbone.png',8,0,11985,1,1,0,0,NULL,65,NULL,'2017-11-30 17:05:30','2017-11-30 17:02:44'),(9,1,'files_trashbin','d5471bc74c06a4dd20b605f2f8ef2eb6',1,'files_trashbin',2,1,228761,1507740359,1507740358,0,0,'59de4ac7af04a',31,'','2017-12-01 17:07:06','2017-12-01 17:07:06'),(10,1,'files_trashbin/files','648f3de19aa6719064e82dd3daa1e0cb',9,'files',2,1,228761,1507740359,1507740359,0,0,'59de4ac7af04a',31,'','2017-12-01 17:07:06','2017-12-01 17:07:06'),(11,1,'files/js.zip','079e4012dd6fc2ef613cd2048570fb4e',3,'js.zip',12,0,51800,1,1,0,0,NULL,65,NULL,'2017-12-20 12:18:05','2017-12-20 12:18:05'),(12,1,'files/SCORM-Driver-with-sample-course-SCORM12.zip','c276acc5ee6bc12993621e39c5a4373e',3,'SCORM-Driver-with-sample-course-SCORM12.zip',12,0,115984,1,1,0,0,NULL,65,NULL,'2017-12-20 12:18:21','2017-12-20 12:18:21'),(13,1,'files/SCORM-Driver-with-sample-course-Tin-Can.zip','5b5241c951e04d25a85207737db86dbe',3,'SCORM-Driver-with-sample-course-Tin-Can.zip',12,0,324670,1,1,0,0,NULL,65,NULL,'2017-12-20 12:18:21','2017-12-20 12:18:21'),(14,1,'files/How-to-import-a-course-into-SCORM-Cloud.zip','6bcf137222497c1675ab1d99de39ae68',3,'How-to-import-a-course-into-SCORM-Cloud.zip',12,0,44309,1,1,0,0,NULL,65,NULL,'2017-12-20 12:18:21','2017-12-20 12:18:21');
/*!40000 ALTER TABLE `gf_files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_files_locks`
--

DROP TABLE IF EXISTS `gf_files_locks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_files_locks` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `lock` int(11) NOT NULL DEFAULT '0',
  `key` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `ttl` int(11) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `lock_key_UNIQUE` (`key`),
  KEY `lock_ttl_KEY` (`ttl`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_files_locks`
--

LOCK TABLES `gf_files_locks` WRITE;
/*!40000 ALTER TABLE `gf_files_locks` DISABLE KEYS */;
/*!40000 ALTER TABLE `gf_files_locks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_files_share`
--

DROP TABLE IF EXISTS `gf_files_share`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_files_share` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `file_id` bigint(10) unsigned NOT NULL,
  `owner_id` bigint(10) unsigned NOT NULL,
  `shared_id` bigint(10) unsigned NOT NULL,
  `initiator_id` bigint(10) unsigned NOT NULL,
  `share_expiration` datetime DEFAULT NULL,
  `share_type` smallint(6) NOT NULL DEFAULT '0',
  `share_edit` smallint(6) NOT NULL DEFAULT '1',
  `share_details` smallint(6) NOT NULL DEFAULT '1',
  `share_others` smallint(6) NOT NULL DEFAULT '0',
  `share_status` smallint(6) NOT NULL DEFAULT '1',
  `mail_send` smallint(6) NOT NULL DEFAULT '0',
  `share_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `share_hash` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_files_share_file_id_idx` (`file_id`),
  CONSTRAINT `fk_files_share_file_id` FOREIGN KEY (`file_id`) REFERENCES `gf_files` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_files_share`
--

LOCK TABLES `gf_files_share` WRITE;
/*!40000 ALTER TABLE `gf_files_share` DISABLE KEYS */;
/*!40000 ALTER TABLE `gf_files_share` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_files_trash`
--

DROP TABLE IF EXISTS `gf_files_trash`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_files_trash` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `file_id` bigint(10) unsigned NOT NULL,
  `name` varchar(250) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `user_id` bigint(10) unsigned NOT NULL,
  `timestamp` varchar(12) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `location` varchar(512) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `type` varchar(4) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `mime` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name_KEY` (`name`),
  KEY `timestamp_KEY` (`timestamp`),
  KEY `user_KEY` (`user_id`),
  KEY `fk_files_trash_file_id_idx` (`file_id`),
  CONSTRAINT `fk_files_trash_file_id` FOREIGN KEY (`file_id`) REFERENCES `gf_files` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_files_trash_user_id` FOREIGN KEY (`user_id`) REFERENCES `gf_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_files_trash`
--

LOCK TABLES `gf_files_trash` WRITE;
/*!40000 ALTER TABLE `gf_files_trash` DISABLE KEYS */;
/*!40000 ALTER TABLE `gf_files_trash` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_fonts`
--

DROP TABLE IF EXISTS `gf_fonts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_fonts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(45) DEFAULT NULL,
  `file_name` varchar(45) DEFAULT NULL,
  `font_type` int(11) DEFAULT '1',
  `css_stack` varchar(255) DEFAULT NULL,
  `weight` int(11) DEFAULT '400',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_fonts`
--

LOCK TABLES `gf_fonts` WRITE;
/*!40000 ALTER TABLE `gf_fonts` DISABLE KEYS */;
/*!40000 ALTER TABLE `gf_fonts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_mediaqueries`
--

DROP TABLE IF EXISTS `gf_mediaqueries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_mediaqueries` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `media_query` varchar(255) DEFAULT '',
  `body_css_custom` mediumtext,
  `body_css` mediumtext,
  `gf_components_id` bigint(10) unsigned NOT NULL,
  `gf_mediaquery_blueprints_id` bigint(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gf_mediaqueries_gf_components1_idx` (`gf_components_id`),
  KEY `fk_gf_mediaqueries_gf_mediaquery_blueprints1_idx` (`gf_mediaquery_blueprints_id`),
  CONSTRAINT `fk_gf_mediaqueries_gf_components1` FOREIGN KEY (`gf_components_id`) REFERENCES `gf_components` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_gf_mediaqueries_gf_mediaquery_blueprints1` FOREIGN KEY (`gf_mediaquery_blueprints_id`) REFERENCES `gf_mediaquery_blueprints` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_mediaqueries`
--

LOCK TABLES `gf_mediaqueries` WRITE;
/*!40000 ALTER TABLE `gf_mediaqueries` DISABLE KEYS */;
INSERT INTO `gf_mediaqueries` VALUES (4,'',NULL,'[{\"selector\":\".main\",\"editableCssAttributes\":[],\"parsed\":\" .main{height:100%;margin:0;padding:0;position:relative;background-color:transparent;box-shadow:0px 0px 4px rgba(0, 0, 0, 0.5);}\",\"cssAttributes\":\"\"}]',1,44),(5,'',NULL,'[{\"selector\":\"h1\",\"editableCssAttributes\":[\"color\",\"font-size\"],\"parsed\":\" h1{background-color:#095256;padding:2vw 1em;color:#087f8c;margin:0;font-family:Roboto, sans-serif;font-weight:normal;font-size:2vw;}\",\"cssAttributes\":\"\"}]',2,50),(6,'@media (max-width:800px)',NULL,'[{\"selector\":\"h1\",\"editableCssAttributes\":[],\"parsed\":\" h1{margin:1em 0;font-size:24px;}\",\"cssAttributes\":\"\"}]',2,51),(17,'',NULL,'[{\"selector\":\".main\",\"editableCssAttributes\":[],\"parsed\":\" .main{height:100%;margin:0;padding:0;position:relative;background-color:transparent;box-shadow:0px 0px 4px rgba(0, 0, 0, 0.5);}\",\"cssAttributes\":\"\"}]',3,1),(18,'',NULL,'[{\"selector\":\"h1\",\"editableCssAttributes\":[\"color\",\"font-size\"],\"parsed\":\" h1{background-color:#fff;padding:2vw 1em;color:#111;margin:0;font-family:Roboto, sans-serif;font-weight:normal;font-size:2vw;}\",\"cssAttributes\":\"\"}]',6,4),(19,'@media (max-width:800px)',NULL,'[{\"selector\":\"h1\",\"editableCssAttributes\":[],\"parsed\":\" h1{margin:1em 0;font-size:24px;}\",\"cssAttributes\":\"\"}]',6,5),(20,'',NULL,'[{\"selector\":\"nav\",\"editableCssAttributes\":[\"background-color\"],\"parsed\":\" nav{background-color:#555;}\",\"cssAttributes\":\"\"},{\"selector\":\"ul\",\"editableCssAttributes\":[],\"parsed\":\" ul{list-style:none;padding:2vw 1vw;}\",\"cssAttributes\":\"\"},{\"selector\":\"li\",\"editableCssAttributes\":[],\"parsed\":\" li{list-style:none;}\",\"cssAttributes\":\"\"},{\"selector\":\"li a\",\"editableCssAttributes\":[\"color\",\"font-size\"],\"parsed\":\" li a{color:#111;text-decoration:none;font-size:1.5vw;}\",\"cssAttributes\":\"\"}]',7,2),(21,'@media (max-width:800px)',NULL,'[{\"selector\":\"ul\",\"editableCssAttributes\":[],\"parsed\":\" ul{display:flex;flex-direction:row;flex-wrap:nowrap;justify-content:space- between;align-items:stretch;align-content:flex-start;}\",\"cssAttributes\":\"\"},{\"selector\":\"li a\",\"editableCssAttributes\":[],\"parsed\":\" li a{font-size:14px;}\",\"cssAttributes\":\"\"}]',7,3),(22,'',NULL,'[{\"selector\":\".main\",\"editableCssAttributes\":[],\"parsed\":\" .main{height:100%;margin:0;padding:0;position:relative;background-color:transparent;box-shadow:0px 0px 4px rgba(0, 0, 0, 0.5);}\",\"cssAttributes\":\"\"}]',4,1),(23,'',NULL,'[{\"selector\":\".main\",\"editableCssAttributes\":[],\"parsed\":\" .main{height:100%;margin:0;padding:0;position:relative;background-color:transparent;box-shadow:0px 0px 4px rgba(0, 0, 0, 0.5);}\",\"cssAttributes\":\"\"}]',5,1),(24,'',NULL,'[{\"selector\":\".main\",\"editableCssAttributes\":[],\"parsed\":\" .main{height:100%;margin:0;padding:0;position:relative;background-color:transparent;box-shadow:0px 0px 4px rgba(0, 0, 0, 0.5);}\",\"cssAttributes\":\"\"}]',8,1),(25,'',NULL,'[{\"selector\":\".main\",\"editableCssAttributes\":[],\"parsed\":\" .main{height:100%;margin:0;padding:0;position:relative;background-color:transparent;box-shadow:0px 0px 4px rgba(0, 0, 0, 0.5);}\",\"cssAttributes\":\"\"}]',9,1),(26,'',NULL,'[{\"selector\":\".main\",\"editableCssAttributes\":[],\"parsed\":\" .main{height:100%;margin:0;padding:0;position:relative;background-color:transparent;box-shadow:0px 0px 4px rgba(0, 0, 0, 0.5);}\",\"cssAttributes\":\"\"}]',10,1),(27,'',NULL,'[{\"selector\":\".main\",\"editableCssAttributes\":[],\"parsed\":\" .main{height:100%;margin:0;padding:0;position:relative;background-color:transparent;box-shadow:0px 0px 4px rgba(0, 0, 0, 0.5);}\",\"cssAttributes\":\"\"}]',11,1),(28,'',NULL,'[{\"selector\":\".main\",\"editableCssAttributes\":[],\"parsed\":\" .main{height:100%;margin:0;padding:0;position:relative;background-color:transparent;box-shadow:0px 0px 4px rgba(0, 0, 0, 0.5);}\",\"cssAttributes\":\"\"}]',12,1),(40,'',NULL,'[{\"selector\":\".main\",\"editableCssAttributes\":[],\"parsed\":\" .main{height:100%;margin:0;padding:0;position:relative;background-color:transparent;box-shadow:0px 0px 4px rgba(0, 0, 0, 0.5);}\",\"cssAttributes\":\"\"}]',13,44),(41,'',NULL,'[{\"selector\":\"\",\"editableCssAttributes\":[],\"parsed\":\" {}\",\"cssAttributes\":\"\"}]',14,41),(42,'',NULL,'[{\"selector\":\".main\",\"editableCssAttributes\":[],\"parsed\":\" .main{height:100%;margin:0;padding:0;position:relative;background-color:transparent;box-shadow:0px 0px 4px rgba(0, 0, 0, 0.5);}\",\"cssAttributes\":\"\"}]',15,44),(48,'',NULL,'[{\"selector\":\".main\",\"editableCssAttributes\":[],\"parsed\":\" .main{height:100%;margin:0;padding:0;position:relative;background-color:transparent;box-shadow:0px 0px 4px rgba(0, 0, 0, 0.5);}\",\"cssAttributes\":\"\"}]',16,44),(49,'',NULL,'[{\"selector\":\"\",\"editableCssAttributes\":[],\"parsed\":\" {}\",\"cssAttributes\":\"\"}]',17,41);
/*!40000 ALTER TABLE `gf_mediaqueries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_mediaquery_blueprints`
--

DROP TABLE IF EXISTS `gf_mediaquery_blueprints`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_mediaquery_blueprints` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `media_query` varchar(255) DEFAULT '',
  `body_css_custom` mediumtext,
  `body_css` mediumtext,
  `gf_component_blueprints_id` bigint(10) unsigned NOT NULL,
  `gf_theme_blueprints_id` bigint(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gf_mediaqueries_gf_component_blueprints1_idx` (`gf_component_blueprints_id`),
  KEY `fk_gf_mediaquery_blueprints_gf_theme_blueprints1_idx` (`gf_theme_blueprints_id`),
  CONSTRAINT `fk_gf_mediaqueries_gf_component_blueprints1` FOREIGN KEY (`gf_component_blueprints_id`) REFERENCES `gf_component_blueprints` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_gf_mediaquery_blueprints_gf_theme_blueprints1` FOREIGN KEY (`gf_theme_blueprints_id`) REFERENCES `gf_theme_blueprints` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_mediaquery_blueprints`
--

LOCK TABLES `gf_mediaquery_blueprints` WRITE;
/*!40000 ALTER TABLE `gf_mediaquery_blueprints` DISABLE KEYS */;
INSERT INTO `gf_mediaquery_blueprints` VALUES (1,'','\'.main{height:100%;margin:0;padding:0;position:relative;background-color:transparent;box-shadow:0px 0px 4px rgba(0, 0, 0, 0.5)}\'','\'.main{height:100%;margin:0;padding:0;position:relative;background-color:transparent;box-shadow:0px 0px 4px rgba(0, 0, 0, 0.5)}\'',1,4),(2,'','\'nav{cat-editable-background-color:#555}\',\'ul{list-style:none;padding:2vw 1vw}\',\'li{list-style:none}\',\'li a{cat-editable-color:#111;text-decoration:none;cat-editable-font-size:1.5vw}\'','\'nav{cat-editable-background-color:#555}\',\'ul{list-style:none;padding:2vw 1vw}\',\'li{list-style:none}\',\'li a{cat-editable-color:#111;text-decoration:none;cat-editable-font-size:1.5vw}\'',2,4),(3,'@media (max-width:800px)','\'ul{display:flex;flex-direction:row;flex-wrap:nowrap;justify-content:space- between;align-items:stretch;align-content:flex-start}\',\'li a{font-size:14px}\'','\'ul{display:flex;flex-direction:row;flex-wrap:nowrap;justify-content:space- between;align-items:stretch;align-content:flex-start}\',\'li a{font-size:14px}\'',2,4),(4,'','\'h1{background-color:#fff;padding:2vw 1em;cat-editable-color:#111;margin:0;font-family:Roboto, sans-serif;font-weight:normal;cat-editable-font-size:2vw}\'','\'h1{background-color:#fff;padding:2vw 1em;cat-editable-color:#111;margin:0;font-family:Roboto, sans-serif;font-weight:normal;cat-editable-font-size:2vw}\'',3,4),(5,'@media (max-width:800px)','\'h1{margin:1em 0;font-size:24px}\'','\'h1{margin:1em 0;font-size:24px}\'',3,4),(6,'','\'.section-a{min-height:5vw}\',\'.section-b{min-height:5vw}\',\'.section-c{min-height:5vw}\',\'.section-a{border:1px solid #f55;display:block}\',\'.flex{padding:1vw 0;display:flex;flex-direction:row;flex-wrap:nowrap;justify-content:space-between;align-items:stretch;align-content:flex-start}\',\'.section-b{border:1px solid #5f5;flex-basis:35%}\',\'.section-c{border:1px solid #55f;flex-basis:60%}\'','\'.section-a{min-height:5vw}\',\'.section-b{min-height:5vw}\',\'.section-c{min-height:5vw}\',\'.section-a{border:1px solid #f55;display:block}\',\'.flex{padding:1vw 0;display:flex;flex-direction:row;flex-wrap:nowrap;justify-content:space-between;align-items:stretch;align-content:flex-start}\',\'.section-b{border:1px solid #5f5;flex-basis:35%}\',\'.section-c{border:1px solid #55f;flex-basis:60%}\'',4,4),(7,'@media (max-width:800px)','\'.flex{padding:1vw 0;display:flex;flex-direction:column;flex-wrap:nowrap;justify-content:space-between;align-items:stretch;align-content:flex-start}\'','\'.flex{padding:1vw 0;display:flex;flex-direction:column;flex-wrap:nowrap;justify-content:space-between;align-items:stretch;align-content:flex-start}\'',4,4),(8,'','\'font-family:Roboto;{}\',\'font-size:1rem;{}\'','\'font-family:Roboto;{}\',\'font-size:1rem;{}\'',5,4),(9,'@media (max-width:800px)','\'font-size:1.5rem;{}\'','\'font-size:1.5rem;{}\'',5,4),(10,'','\'{}\'','\'{}\'',6,4),(11,'','\'{}\'','\'{}\'',7,4),(12,'','\'.main{height:100%;margin:0;padding:0;position:relative;background-color:transparent;box-shadow:0px 0px 4px rgba(0, 0, 0, 0.5)}\'','\'.main{height:100%;margin:0;padding:0;position:relative;background-color:transparent;box-shadow:0px 0px 4px rgba(0, 0, 0, 0.5)}\'',1,5),(13,'','\'nav{cat-editable-background-color:#555}\',\'ul{list-style:none;padding:2vw 1vw}\',\'li{list-style:none}\',\'li a{cat-editable-color:#fff;text-decoration:none;cat-editable-font-size:1.5vw}\'','\'nav{cat-editable-background-color:#555}\',\'ul{list-style:none;padding:2vw 1vw}\',\'li{list-style:none}\',\'li a{cat-editable-color:#fff;text-decoration:none;cat-editable-font-size:1.5vw}\'',2,5),(14,'@media (max-width:800px)','\'ul{display:flex;flex-direction:row;flex-wrap:nowrap;justify-content:space- between;align-items:stretch;align-content:flex-start}\',\'li a{font-size:14px}\'','\'ul{display:flex;flex-direction:row;flex-wrap:nowrap;justify-content:space- between;align-items:stretch;align-content:flex-start}\',\'li a{font-size:14px}\'',2,5),(15,'','\'h1{background-color:#000;padding:2vw 1em;cat-editable-color:#fff;margin:0;font-family:Roboto, sans-serif;font-weight:normal;cat-editable-font-size:2vw}\'','\'h1{background-color:#000;padding:2vw 1em;cat-editable-color:#fff;margin:0;font-family:Roboto, sans-serif;font-weight:normal;cat-editable-font-size:2vw}\'',3,5),(16,'@media (max-width:800px)','\'h1{margin:1em 0;font-size:24px}\'','\'h1{margin:1em 0;font-size:24px}\'',3,5),(17,'','\'.section-a{min-height:5vw}\',\'.section-b{min-height:5vw}\',\'.section-c{min-height:5vw}\',\'.section-a{border:1px solid #f55;display:block}\',\'.flex{padding:1vw 0;display:flex;flex-direction:row;flex-wrap:nowrap;justify-content:space-between;align-items:stretch;align-content:flex-start}\',\'.section-b{border:1px solid #5f5;flex-basis:35%}\',\'.section-c{border:1px solid #55f;flex-basis:60%}\'','\'.section-a{min-height:5vw}\',\'.section-b{min-height:5vw}\',\'.section-c{min-height:5vw}\',\'.section-a{border:1px solid #f55;display:block}\',\'.flex{padding:1vw 0;display:flex;flex-direction:row;flex-wrap:nowrap;justify-content:space-between;align-items:stretch;align-content:flex-start}\',\'.section-b{border:1px solid #5f5;flex-basis:35%}\',\'.section-c{border:1px solid #55f;flex-basis:60%}\'',4,5),(18,'@media (max-width:800px)','\'.flex{padding:1vw 0;display:flex;flex-direction:column;flex-wrap:nowrap;justify-content:space-between;align-items:stretch;align-content:flex-start}\'','\'.flex{padding:1vw 0;display:flex;flex-direction:column;flex-wrap:nowrap;justify-content:space-between;align-items:stretch;align-content:flex-start}\'',4,5),(19,'','\'font-family:Roboto;{}\',\'font-size:1rem;{}\'','\'font-family:Roboto;{}\',\'font-size:1rem;{}\'',5,5),(20,'@media (max-width:800px)','\'font-size:1.5rem;{}\'','\'font-size:1.5rem;{}\'',5,5),(21,'','\'{}\'','\'{}\'',6,5),(22,'','\'{}\'','\'{}\'',7,5),(23,'','\'.section-a{min-height:5vw}\',\'.section-b{min-height:5vw}\',\'.section-c{min-height:5vw}\',\'.section-a{border:1px solid #f55;display:block}\',\'.flex{padding:1vw 0;display:flex;flex-direction:row;flex-wrap:nowrap;justify-content:space-between;align-items:stretch;align-content:flex-start}\',\'.section-b{border:1px solid #5f5;flex-basis:35%}\',\'.section-c{border:1px solid #55f;flex-basis:60%}\'','\'.section-a{min-height:5vw}\',\'.section-b{min-height:5vw}\',\'.section-c{min-height:5vw}\',\'.section-a{border:1px solid #f55;display:block}\',\'.flex{padding:1vw 0;display:flex;flex-direction:row;flex-wrap:nowrap;justify-content:space-between;align-items:stretch;align-content:flex-start}\',\'.section-b{border:1px solid #5f5;flex-basis:35%}\',\'.section-c{border:1px solid #55f;flex-basis:60%}\'',4,1),(24,'@media (max-width:800px)','\'.flex{padding:1vw 0;display:flex;flex-direction:column;flex-wrap:nowrap;justify-content:space-between;align-items:stretch;align-content:flex-start}\'','\'.flex{padding:1vw 0;display:flex;flex-direction:column;flex-wrap:nowrap;justify-content:space-between;align-items:stretch;align-content:flex-start}\'',4,1),(25,'','\'.section-a{min-height:5vw}\',\'.section-b{min-height:5vw}\',\'.section-c{min-height:5vw}\',\'.section-a{border:1px solid #f55;display:block}\',\'.flex{padding:1vw 0;display:flex;flex-direction:row;flex-wrap:nowrap;justify-content:space-between;align-items:stretch;align-content:flex-start}\',\'.section-b{border:1px solid #5f5;flex-basis:35%}\',\'.section-c{border:1px solid #55f;flex-basis:60%}\'','\'.section-a{min-height:5vw}\',\'.section-b{min-height:5vw}\',\'.section-c{min-height:5vw}\',\'.section-a{border:1px solid #f55;display:block}\',\'.flex{padding:1vw 0;display:flex;flex-direction:row;flex-wrap:nowrap;justify-content:space-between;align-items:stretch;align-content:flex-start}\',\'.section-b{border:1px solid #5f5;flex-basis:35%}\',\'.section-c{border:1px solid #55f;flex-basis:60%}\'',4,2),(26,'@media (max-width:800px)','\'.flex{padding:1vw 0;display:flex;flex-direction:column;flex-wrap:nowrap;justify-content:space-between;align-items:stretch;align-content:flex-start}\'','\'.flex{padding:1vw 0;display:flex;flex-direction:column;flex-wrap:nowrap;justify-content:space-between;align-items:stretch;align-content:flex-start}\'',4,2),(27,'','\'.section-a{min-height:5vw}\',\'.section-b{min-height:5vw}\',\'.section-c{min-height:5vw}\',\'.section-a{border:1px solid #f55;display:block}\',\'.flex{padding:1vw 0;display:flex;flex-direction:row;flex-wrap:nowrap;justify-content:space-between;align-items:stretch;align-content:flex-start}\',\'.section-b{border:1px solid #5f5;flex-basis:35%}\',\'.section-c{border:1px solid #55f;flex-basis:60%}\'','\'.section-a{min-height:5vw}\',\'.section-b{min-height:5vw}\',\'.section-c{min-height:5vw}\',\'.section-a{border:1px solid #f55;display:block}\',\'.flex{padding:1vw 0;display:flex;flex-direction:row;flex-wrap:nowrap;justify-content:space-between;align-items:stretch;align-content:flex-start}\',\'.section-b{border:1px solid #5f5;flex-basis:35%}\',\'.section-c{border:1px solid #55f;flex-basis:60%}\'',4,3),(28,'@media (max-width:800px)','\'.flex{padding:1vw 0;display:flex;flex-direction:column;flex-wrap:nowrap;justify-content:space-between;align-items:stretch;align-content:flex-start}\'','\'.flex{padding:1vw 0;display:flex;flex-direction:column;flex-wrap:nowrap;justify-content:space-between;align-items:stretch;align-content:flex-start}\'',4,3),(29,'','\'nav{cat-editable-background-color:#86a873}\',\'ul{list-style:none;padding:2vw 1vw}\',\'li{list-style:none}\',\'li a{cat-editable-color:#087f8c;text-decoration:none;cat-editable-font-size:1.5vw}\'','\'nav{cat-editable-background-color:#86a873}\',\'ul{list-style:none;padding:2vw 1vw}\',\'li{list-style:none}\',\'li a{cat-editable-color:#087f8c;text-decoration:none;cat-editable-font-size:1.5vw}\'',2,1),(30,'@media (max-width:800px)','\'ul{display:flex;flex-direction:row;flex-wrap:nowrap;justify-content:space- between;align-items:stretch;align-content:flex-start}\',\'li a{font-size:14px}\'','\'ul{display:flex;flex-direction:row;flex-wrap:nowrap;justify-content:space- between;align-items:stretch;align-content:flex-start}\',\'li a{font-size:14px}\'',2,1),(31,'','\'nav{cat-editable-background-color:#d8b4a0}\',\'ul{list-style:none;padding:2vw 1vw}\',\'li{list-style:none}\',\'li a{cat-editable-color:#dbd3d8;text-decoration:none;cat-editable-font-size:1.5vw}\'','\'nav{cat-editable-background-color:#d8b4a0}\',\'ul{list-style:none;padding:2vw 1vw}\',\'li{list-style:none}\',\'li a{cat-editable-color:#dbd3d8;text-decoration:none;cat-editable-font-size:1.5vw}\'',2,2),(32,'@media (max-width:800px)','\'ul{display:flex;flex-direction:row;flex-wrap:nowrap;justify-content:space- between;align-items:stretch;align-content:flex-start}\',\'li a{font-size:14px}\'','\'ul{display:flex;flex-direction:row;flex-wrap:nowrap;justify-content:space- between;align-items:stretch;align-content:flex-start}\',\'li a{font-size:14px}\'',2,2),(33,'','\'nav{cat-editable-background-color:}\',\'ul{list-style:none;padding:2vw 1vw}\',\'li{list-style:none}\',\'li a{cat-editable-color:#191516;text-decoration:none;cat-editable-font-size:1.5vw}\'','\'nav{cat-editable-background-color:}\',\'ul{list-style:none;padding:2vw 1vw}\',\'li{list-style:none}\',\'li a{cat-editable-color:#191516;text-decoration:none;cat-editable-font-size:1.5vw}\'',2,3),(34,'@media (max-width:800px)','\'ul{display:flex;flex-direction:row;flex-wrap:nowrap;justify-content:space- between;align-items:stretch;align-content:flex-start}\',\'li a{font-size:14px}\'','\'ul{display:flex;flex-direction:row;flex-wrap:nowrap;justify-content:space- between;align-items:stretch;align-content:flex-start}\',\'li a{font-size:14px}\'',2,3),(35,'','\'font-family:Roboto;{}\',\'font-size:1rem;{}\'','\'font-family:Roboto;{}\',\'font-size:1rem;{}\'',5,1),(36,'@media (max-width:800px)','\'font-size:1.5rem;{}\'','\'font-size:1.5rem;{}\'',5,1),(37,'','\'font-family:Roboto;{}\',\'font-size:1rem;{}\'','\'font-family:Roboto;{}\',\'font-size:1rem;{}\'',5,2),(38,'@media (max-width:800px)','\'font-size:1.5rem;{}\'','\'font-size:1.5rem;{}\'',5,2),(39,'','\'font-family:Roboto;{}\',\'font-size:1rem;{}\'','\'font-family:Roboto;{}\',\'font-size:1rem;{}\'',5,3),(40,'@media (max-width:800px)','\'font-size:1.5rem;{}\'','\'font-size:1.5rem;{}\'',5,3),(41,'','\'{}\'','\'{}\'',6,1),(42,'','\'{}\'','\'{}\'',6,2),(43,'','\'{}\'','\'{}\'',6,3),(44,'','\'.main{height:100%;margin:0;padding:0;position:relative;background-color:transparent;box-shadow:0px 0px 4px rgba(0, 0, 0, 0.5)}\'','\'.main{height:100%;margin:0;padding:0;position:relative;background-color:transparent;box-shadow:0px 0px 4px rgba(0, 0, 0, 0.5)}\'',1,1),(45,'','\'.main{height:100%;margin:0;padding:0;position:relative;background-color:transparent;box-shadow:0px 0px 4px rgba(0, 0, 0, 0.5)}\'','\'.main{height:100%;margin:0;padding:0;position:relative;background-color:transparent;box-shadow:0px 0px 4px rgba(0, 0, 0, 0.5)}\'',1,2),(46,'','\'.main{height:100%;margin:0;padding:0;position:relative;background-color:transparent;box-shadow:0px 0px 4px rgba(0, 0, 0, 0.5)}\'','\'.main{height:100%;margin:0;padding:0;position:relative;background-color:transparent;box-shadow:0px 0px 4px rgba(0, 0, 0, 0.5)}\'',1,3),(47,'','\'{}\'','\'{}\'',7,1),(48,'','\'{}\'','\'{}\'',7,2),(49,'','\'{}\'','\'{}\'',7,3),(50,'','\'h1{background-color:#095256;padding:2vw 1em;cat-editable-color:#087f8c;margin:0;font-family:Roboto, sans-serif;font-weight:normal;cat-editable-font-size:2vw}\'','\'h1{background-color:#095256;padding:2vw 1em;cat-editable-color:#087f8c;margin:0;font-family:Roboto, sans-serif;font-weight:normal;cat-editable-font-size:2vw}\'',3,1),(51,'@media (max-width:800px)','\'h1{margin:1em 0;font-size:24px}\'','\'h1{margin:1em 0;font-size:24px}\'',3,1),(52,'','\'h1{background-color:#223843;padding:2vw 1em;cat-editable-color:#dbd3d8;margin:0;font-family:Roboto, sans-serif;font-weight:normal;cat-editable-font-size:2vw}\'','\'h1{background-color:#223843;padding:2vw 1em;cat-editable-color:#dbd3d8;margin:0;font-family:Roboto, sans-serif;font-weight:normal;cat-editable-font-size:2vw}\'',3,2),(53,'@media (max-width:800px)','\'h1{margin:1em 0;font-size:24px}\'','\'h1{margin:1em 0;font-size:24px}\'',3,2),(54,'','\'h1{background-color:#00b295;padding:2vw 1em;cat-editable-color:#191516;margin:0;font-family:Roboto, sans-serif;font-weight:normal;cat-editable-font-size:2vw}\'','\'h1{background-color:#00b295;padding:2vw 1em;cat-editable-color:#191516;margin:0;font-family:Roboto, sans-serif;font-weight:normal;cat-editable-font-size:2vw}\'',3,3),(55,'@media (max-width:800px)','\'h1{margin:1em 0;font-size:24px}\'','\'h1{margin:1em 0;font-size:24px}\'',3,3);
/*!40000 ALTER TABLE `gf_mediaquery_blueprints` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_mimetypes`
--

DROP TABLE IF EXISTS `gf_mimetypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_mimetypes` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `mimetype` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `icon` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT 'file',
  `ext` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mimetype_id_UNIQUE` (`mimetype`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_mimetypes`
--

LOCK TABLES `gf_mimetypes` WRITE;
/*!40000 ALTER TABLE `gf_mimetypes` DISABLE KEYS */;
INSERT INTO `gf_mimetypes` VALUES (1,'httpd','file',''),(2,'httpd/unix-directory','folder',NULL),(3,'application','file',NULL),(4,'application/pdf','pdf','.pdf'),(5,'application/vnd.oasis.opendocument.text','file','.odt'),(6,'image','img',''),(7,'image/jpg','jpg','.jpeg'),(8,'image/png','png','.png'),(9,'application/octet-stream','file',NULL),(10,'image/svg+xml','svg','.svg'),(11,'application/vnd.ms-powerpoint','ppt','.ppt'),(12,'application/zip','zip','.zip'),(13,'video/x-msvideo','avi','.avi'),(14,'text/css','css','.css'),(15,'text/csv','csv','.csv'),(16,'application/msword','doc','.doc'),(17,'application/x-shockwave-flash','swf','.swf'),(18,'text/html','html','.html'),(19,'text/htm','html','.htm'),(20,'application/javascript','js','.js'),(21,'application/json','json','.json'),(22,'audio/mp3','mp3','.mp3'),(23,'video/mp4','mp4','.mp4'),(24,'application/psd','psd','.psd'),(25,'text/richtext','rtf','.rtf'),(26,'text','txt','.txt'),(27,'application/xls','xls','.xls'),(28,'text/xml','xml','.xml'),(29,'application/x-directory','folder',NULL),(30,'image/jpeg','jpg','.jpeg');
/*!40000 ALTER TABLE `gf_mimetypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_mounts`
--

DROP TABLE IF EXISTS `gf_mounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_mounts` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `storage_id` bigint(10) unsigned NOT NULL,
  `root_id` bigint(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `mount_point` varchar(4000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mounts_user_root_UNIQUE` (`user_id`,`root_id`),
  KEY `mounts_user_UNIQUE` (`user_id`),
  KEY `mounts_storage_UNIQUE` (`storage_id`),
  KEY `mounts_root_UNIQUE` (`root_id`),
  KEY `fk_mounts_storage_id_idx` (`storage_id`),
  CONSTRAINT `fk_mounts_storage_id` FOREIGN KEY (`storage_id`) REFERENCES `gf_storages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_mounts`
--

LOCK TABLES `gf_mounts` WRITE;
/*!40000 ALTER TABLE `gf_mounts` DISABLE KEYS */;
INSERT INTO `gf_mounts` VALUES (1,1,1,1,'/matsfjellner@catedra.edu.co/');
/*!40000 ALTER TABLE `gf_mounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_output_formats`
--

DROP TABLE IF EXISTS `gf_output_formats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_output_formats` (
  `id` bigint(10) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `description` mediumtext,
  `default_format` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_output_formats`
--

LOCK TABLES `gf_output_formats` WRITE;
/*!40000 ALTER TABLE `gf_output_formats` DISABLE KEYS */;
INSERT INTO `gf_output_formats` VALUES (1,'SCORM 1.2','Standard e-learning format since 2001',1),(2,'xAPI','Future format',NULL);
/*!40000 ALTER TABLE `gf_output_formats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_page_relations`
--

DROP TABLE IF EXISTS `gf_page_relations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_page_relations` (
  `gf_descendant` bigint(10) NOT NULL,
  `depth` int(11) DEFAULT NULL,
  `gf_ascendant` bigint(10) unsigned NOT NULL,
  `position` int(11) DEFAULT NULL,
  `root` tinyint(1) DEFAULT '0',
  KEY `fk_gf_page_relations_gf_pages1_idx` (`gf_ascendant`),
  CONSTRAINT `fk_gf_page_relations_gf_pages1` FOREIGN KEY (`gf_ascendant`) REFERENCES `gf_pages` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_page_relations`
--

LOCK TABLES `gf_page_relations` WRITE;
/*!40000 ALTER TABLE `gf_page_relations` DISABLE KEYS */;
INSERT INTO `gf_page_relations` VALUES (16,0,16,0,1),(19,0,19,0,1);
/*!40000 ALTER TABLE `gf_page_relations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_pages`
--

DROP TABLE IF EXISTS `gf_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_pages` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `gf_unit_id` bigint(10) unsigned NOT NULL,
  `gf_components_id` bigint(10) unsigned NOT NULL,
  `saved` bigint(10) DEFAULT NULL,
  `render_html` mediumtext,
  `render_css` mediumtext,
  PRIMARY KEY (`id`),
  KEY `fk_gf_pages_gf_unit1_idx` (`gf_unit_id`),
  CONSTRAINT `fk_gf_pages_gf_unit1` FOREIGN KEY (`gf_unit_id`) REFERENCES `gf_units` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_pages`
--

LOCK TABLES `gf_pages` WRITE;
/*!40000 ALTER TABLE `gf_pages` DISABLE KEYS */;
INSERT INTO `gf_pages` VALUES (16,'1513893414023',1,12,20171221171501,NULL,NULL),(19,'1515701849288',5,16,20180111154902,NULL,NULL);
/*!40000 ALTER TABLE `gf_pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_project_has_components`
--

DROP TABLE IF EXISTS `gf_project_has_components`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_project_has_components` (
  `gf_projects_id` bigint(10) unsigned NOT NULL,
  `gf_component_blueprints_id` bigint(10) unsigned NOT NULL,
  KEY `fk_gf_project_has_components_gf_component_blueprints1_idx` (`gf_component_blueprints_id`),
  CONSTRAINT `fk_gf_project_has_components_gf_component_blueprints1` FOREIGN KEY (`gf_component_blueprints_id`) REFERENCES `gf_component_blueprints` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_project_has_components`
--

LOCK TABLES `gf_project_has_components` WRITE;
/*!40000 ALTER TABLE `gf_project_has_components` DISABLE KEYS */;
INSERT INTO `gf_project_has_components` VALUES (0,1),(0,2),(0,3),(0,4),(0,5),(0,6),(0,7);
/*!40000 ALTER TABLE `gf_project_has_components` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_project_has_themes`
--

DROP TABLE IF EXISTS `gf_project_has_themes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_project_has_themes` (
  `gf_projects_id` bigint(10) unsigned NOT NULL,
  `gf_theme_blueprints_id` bigint(10) unsigned NOT NULL,
  KEY `fk_gf_project_has_themes_gf_projects1_idx` (`gf_projects_id`),
  KEY `fk_gf_project_has_themes_gf_theme_blueprints1_idx` (`gf_theme_blueprints_id`),
  CONSTRAINT `fk_gf_project_has_themes_gf_projects1` FOREIGN KEY (`gf_projects_id`) REFERENCES `gf_projects` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_gf_project_has_themes_gf_theme_blueprints1` FOREIGN KEY (`gf_theme_blueprints_id`) REFERENCES `gf_theme_blueprints` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_project_has_themes`
--

LOCK TABLES `gf_project_has_themes` WRITE;
/*!40000 ALTER TABLE `gf_project_has_themes` DISABLE KEYS */;
INSERT INTO `gf_project_has_themes` VALUES (1,1),(1,2),(1,3),(1,4),(1,5),(3,1),(3,2),(3,3),(3,4),(3,5);
/*!40000 ALTER TABLE `gf_project_has_themes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_projects`
--

DROP TABLE IF EXISTS `gf_projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_projects` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `user_limit` int(4) DEFAULT '10',
  `remote_auth` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `gf_output_formats_id` bigint(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gf_projects_gf_output_formats1_idx` (`gf_output_formats_id`),
  CONSTRAINT `fk_gf_projects_gf_output_formats1` FOREIGN KEY (`gf_output_formats_id`) REFERENCES `gf_output_formats` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_projects`
--

LOCK TABLES `gf_projects` WRITE;
/*!40000 ALTER TABLE `gf_projects` DISABLE KEYS */;
INSERT INTO `gf_projects` VALUES (1,'Goldfish formación',10,'','Manuales de uso del editor',1),(2,'DIAN',10,'https://saga.pegui.edu.co/auth',NULL,1),(3,'Mintkula',10,'','Probando mas proyectos',2);
/*!40000 ALTER TABLE `gf_projects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_roles`
--

DROP TABLE IF EXISTS `gf_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_roles` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_roles`
--

LOCK TABLES `gf_roles` WRITE;
/*!40000 ALTER TABLE `gf_roles` DISABLE KEYS */;
INSERT INTO `gf_roles` VALUES (1,'Super'),(2,'Admin'),(3,'Editor'),(4,'Control de calidad'),(5,'Mantenimiento'),(6,'Diseño'),(7,'Animaciones'),(8,'Componentes'),(9,'Contacto cliente');
/*!40000 ALTER TABLE `gf_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_storages`
--

DROP TABLE IF EXISTS `gf_storages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_storages` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(10) unsigned NOT NULL,
  `user_path` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `available` int(11) NOT NULL DEFAULT '1',
  `last_checked` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `storages_id_UNIQUE` (`id`),
  KEY `fk_storages_trash_user_id` (`user_id`),
  CONSTRAINT `fk_storages_trash_user_id` FOREIGN KEY (`user_id`) REFERENCES `gf_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_storages`
--

LOCK TABLES `gf_storages` WRITE;
/*!40000 ALTER TABLE `gf_storages` DISABLE KEYS */;
INSERT INTO `gf_storages` VALUES (1,1,'home::matsfjellner@catedra.edu.co',1,NULL);
/*!40000 ALTER TABLE `gf_storages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_theme_blueprints`
--

DROP TABLE IF EXISTS `gf_theme_blueprints`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_theme_blueprints` (
  `id` bigint(10) unsigned NOT NULL,
  `name` varchar(100) NOT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `folder` varchar(255) DEFAULT NULL,
  `description` mediumtext,
  `includes` mediumtext,
  `title` varchar(100) DEFAULT NULL,
  `version` bigint(10) unsigned DEFAULT NULL,
  `sass` mediumtext,
  `unitsass` mediumtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_theme_blueprints`
--

LOCK TABLES `gf_theme_blueprints` WRITE;
/*!40000 ALTER TABLE `gf_theme_blueprints` DISABLE KEYS */;
INSERT INTO `gf_theme_blueprints` VALUES (1,'goldfish','thumbnail.jpg','/themes/goldfish','The default Goldfish theme','[{\'css\',\'main.css\'},{\'js\',\'main.js\'}]','Goldfish',3,'$color1: rgba(9, 82, 86, 1);\n$color2: rgba(8, 127, 140, 1);\n$color3: rgba(90, 170, 149, 1);\n$color4: rgba(134, 168, 115, 1);\n$color5: rgba(187, 159, 6, 1);\n\n$textColor: $color2;\n$backgroundColor: $color1;\n$panelColor: $color4;\n$accentColor: $color5;',''),(2,'superpotatis','thumbnail.jpg','/themes/superpotatis','','','Superpotatis',3,'$color1: rgba(34, 56, 67, 1);\n$color2: rgba(239, 241, 243, 1);\n$color3: rgba(219, 211, 216, 1);\n$color4: rgba(216, 180, 160, 1);\n$color5: rgba(215, 122, 97, 1);\n\n$textColor: $color3;\n$backgroundColor: $color1;\n$panelColor: $color4;\n$accentColor: $color5;',''),(3,'linslus','thumbnail.jpg','/themes/linslus','','','Linslus',2,'$color1: rgba(201, 218, 234, 1);\n$color2: rgba(3, 247, 235, 1);\n$color3: rgba(0, 178, 149, 1);\n$color4: rgba(25, 21, 22, 1);\n$color5: rgba(171, 35, 70, 1);\n\n$textColor: $color4;\n$backgroundColor: $color3;\n$accentColor: $color5;',''),(4,'loparsko','thumbnail.jpg','/themes/loparsko','','','Löparsko',4,'$color1: rgba(255, 255, 255, 1);\n$color2: rgba(85, 85, 85, 1);\n$color3: rgba(17, 17, 17, 1);\n$color4: #f36b06;\n$color5: rgba(241, 211, 2, 1);\n\n$textColor: $color3;\n$backgroundColor: $color1;\n$panelColor: $color2;\n$accentColor: $color4;\n$titleColor: $color4;\n$mainFont: \'Raleway\';','body {\nmargin: 0;\nbackground-color: #2e4b49;\n\n\n&:after {\ncontent: \"\";\nbackground: url(#{$assetPath}wallhaven-558971.jpg) 50% 50% no-repeat;\nbackground-size: cover;\nbackground-attachment: fixed;\nposition:fixed;\n  top:0;\n  left:0;\n  right:0;\n  bottom:0;\n  opacity:0;\n  z-index:-1;\n  animation:bgFade 1s 1s forwards;\n}\n\n  > #main {\n    \n    > nav {\nz-index: 100;\n    background-color: rgba($panelColor, 0.8);\n    box-shadow: 0 0 0;\n    position: fixed;\nheight: 8vh;\ntop: 0;\nwidth: 100%;\ndisplay: flex;\n    flex-direction: row;\n    flex-wrap: wrap;\n    justify-content: flex-end;\n    align-items: stretch;\n  }\n\n  > nav a {\nposition: relative;\n    margin: 0 0;\n    padding: 1vw 2vw;\n    text-decoration: none;\n    color: $backgroundColor;\n    transition: background-color 0.3s;\n  }\n\n> nav a:before {\nposition: absolute;\n    content: \'\';\n    width: 1vw;\n    height: 1vw;\n    background-color: transparent;\n    left: 0.5vw;\n    top: 1.2vw;\ntransition: background-color 0.3s;\n}\n\n> nav a.sel, > nav a:hover {\ncolor: $backgroundColor;\nbackground-color: transparent;\n}\n\n  > nav a.sel:before, > nav a:hover:before {\n    background-color: $accentColor;\n    \n  }\n\n   #content {\nbackground-color: rgba($panelColor, 0.5);\npadding: 1vw;\n    flex-grow: 1;\nmargin: calc(8vh + 4vw) 3vw 3vw 3vw;\nopacity: 1;\n    transition: all ease-in-out 0.3s;\ntransform-style: preserve-3d;\nperspective: 2000px;\ntransform: rotateY(0deg);\n\n    &.out {\n      opacity: 0;\nmargin-left: 0;\ntransform: rotateY(-90deg);\n    }\n   \n\n> h2 {\n    font-size: 2vw;\n    position: absolute;\n    width: 100%;\n    text-align: center;\n    top: -4vw;\n    color: #fff;\n}\n   }\n\n  }\n  \n\n> h1 {\nposition: absolute;\nz-index: 200;\nmargin: 1.4vh 0 0 2vw;\nbox-sizing: border-box;\ntop: 0;\ncolor: $backgroundColor;\n    font-size: 2vw;\nborder-bottom: 0.3vh solid $accentColor;\n  }\n}\n\n@keyframes bgFade {\n    from {\n        opacity:0;\n    }\n    to {\n        opacity:1;\n    } \n}'),(5,'snorken','thumbnail.jpg','/themes/snorken','','','Snorken',5,'$color1: rgba(0, 0, 0, 1);\n$color2: rgba(85, 85, 85, 1);\n$color3: rgba(255, 255, 255, 1);\n$color4: #f36b06;\n$color5: rgba(241, 211, 2, 1);\n\n$textColor: $color3;\n$backgroundColor: $color1;\n$panelColor: $color2;\n$accentColor: $color4;\n$titleColor: $color4;\n$mainFont: \'Poiret one\';\n','body {\nmargin: 0;\nbackground-color: #e58735;\n--background-video: url(#{$assetPath}632325766.mp4);\n\n  > #main {\ndisplay: flex;\nflex-direction: row;\nflex-wrap: nowrap;\njustify-content: flex-start;\nalign-items: stretch;\nheight: 90vh;\nbox-sizing: border-box; \n    \n    > nav {\nz-index: 100;\nbackground-color: rgba($backgroundColor, 0.5);\n    box-shadow: 0 0 0;\nwidth: 20vw;\ndisplay: flex;\n    flex-direction: column;\n    flex-wrap: wrap;\n    justify-content: center;\n    align-items: stretch;\nalign-content: flex-start;\nbox-shadow: 0 0 5px rgba(255,215,0,0.3);\n  }\n\n  > nav a {\nposition: relative;\n    margin: 0 0;\n    padding: 1vw 2vw;\n    text-decoration: none;\n    color: $textColor;\n    transition: background-color 0.3s;\nbackground-color: rgba($backgroundColor,0);\ntransition: background-color 0.2s;\n  }\n\n> nav a.sel, > nav a:hover {\ncolor: $textColor;\nbackground-color: rgba($backgroundColor,0.5);\n}\n\n\n\n   #content {\nbackground-color: rgba($backgroundColor, 0.5);\npadding: 1vw;\n    flex-grow: 1;\nmargin: calc(8vh + 4vw) 3vw 3vw 3vw;\nbox-shadow: 0 0 5px rgba(255,215,0,0.3);\nopacity: 1;\n    transition: all ease-in-out 0.3s;\ntransform: scale(1);\n\n    &.out {\n      opacity: 0;\ntransform: scale(0);\n    }\n\n> h2 {\n    font-size: 2vw;\n    position: absolute;\n    width: 100%;\n    text-align: center;\n    top: -4vw;\n    color: #fff;\n}\n   }\n\n  }\n  \n\n> h1 {\nbackground-color: rgba($backgroundColor, 0.5);\nmargin: 0;\npadding: 0 0 0 2vw;\nbox-sizing: border-box;\nwidth: 20vw;\nheight: 10vh;\nline-height: 10vh;\ncolor: $textColor;\n    font-size: 4vw;\nbox-shadow: 0 0 5px rgba(255,215,0,0.3);\n  }\n}\n');
/*!40000 ALTER TABLE `gf_theme_blueprints` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_theme_instances`
--

DROP TABLE IF EXISTS `gf_theme_instances`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_theme_instances` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `changed` tinyint(1) DEFAULT '1',
  `body_css` mediumtext,
  `gf_theme_blueprints_id` bigint(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_theme_instance_gf_theme_blueprints1_idx` (`gf_theme_blueprints_id`),
  CONSTRAINT `fk_theme_instance_gf_theme_blueprints1` FOREIGN KEY (`gf_theme_blueprints_id`) REFERENCES `gf_theme_blueprints` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_theme_instances`
--

LOCK TABLES `gf_theme_instances` WRITE;
/*!40000 ALTER TABLE `gf_theme_instances` DISABLE KEYS */;
INSERT INTO `gf_theme_instances` VALUES (1,1,'',4),(2,1,'',1),(3,1,'',1),(4,1,'',1),(5,1,'',1);
/*!40000 ALTER TABLE `gf_theme_instances` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_unit_has_users`
--

DROP TABLE IF EXISTS `gf_unit_has_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_unit_has_users` (
  `gf_unit_id` bigint(10) unsigned NOT NULL,
  `gf_users_id` bigint(10) unsigned NOT NULL,
  `gf_roles_id` bigint(10) unsigned NOT NULL,
  KEY `fk_gf_unit_has_gf_users_gf_users1_idx` (`gf_users_id`),
  KEY `fk_gf_unit_has_gf_users_gf_unit1_idx` (`gf_unit_id`),
  KEY `fk_gf_unit_has_gf_users_gf_roles1_idx` (`gf_roles_id`),
  CONSTRAINT `fk_gf_unit_has_gf_users_gf_roles1` FOREIGN KEY (`gf_roles_id`) REFERENCES `gf_roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_gf_unit_has_gf_users_gf_unit1` FOREIGN KEY (`gf_unit_id`) REFERENCES `gf_units` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_gf_unit_has_gf_users_gf_users1` FOREIGN KEY (`gf_users_id`) REFERENCES `gf_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_unit_has_users`
--

LOCK TABLES `gf_unit_has_users` WRITE;
/*!40000 ALTER TABLE `gf_unit_has_users` DISABLE KEYS */;
INSERT INTO `gf_unit_has_users` VALUES (1,1,3),(2,1,3),(3,1,3),(4,1,3),(5,1,3);
/*!40000 ALTER TABLE `gf_unit_has_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_units`
--

DROP TABLE IF EXISTS `gf_units`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_units` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `saved` bigint(10) unsigned DEFAULT '0',
  `structure_json` mediumtext,
  `gf_theme_instance_id` bigint(10) unsigned NOT NULL,
  `gf_projects_id` bigint(10) unsigned NOT NULL,
  `gf_output_formats_id` bigint(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gf_units_theme_instance1_idx` (`gf_theme_instance_id`),
  KEY `fk_gf_units_gf_projects1_idx` (`gf_projects_id`),
  KEY `fk_gf_units_gf_output_formats1_idx` (`gf_output_formats_id`),
  CONSTRAINT `fk_gf_units_gf_output_formats1` FOREIGN KEY (`gf_output_formats_id`) REFERENCES `gf_output_formats` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_gf_units_gf_projects1` FOREIGN KEY (`gf_projects_id`) REFERENCES `gf_projects` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_gf_units_theme_instance1` FOREIGN KEY (`gf_theme_instance_id`) REFERENCES `gf_theme_instances` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_units`
--

LOCK TABLES `gf_units` WRITE;
/*!40000 ALTER TABLE `gf_units` DISABLE KEYS */;
INSERT INTO `gf_units` VALUES (1,'Storken',20171221171500,NULL,1,3,2),(2,'Flerd',20171218100227,NULL,2,1,1),(3,'Sprit',20171220152556,NULL,3,3,2),(4,'Sprite',20171220152602,NULL,4,3,2),(5,'Brunch',20180111154902,NULL,5,3,2);
/*!40000 ALTER TABLE `gf_units` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_user_configs`
--

DROP TABLE IF EXISTS `gf_user_configs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_user_configs` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `editor` varchar(50) DEFAULT NULL,
  `theme` varchar(50) DEFAULT NULL,
  `effects` tinyint(1) DEFAULT '0',
  `gf_users_id` bigint(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gf_user_config_gf_users1_idx` (`gf_users_id`),
  CONSTRAINT `fk_gf_user_config_gf_users1` FOREIGN KEY (`gf_users_id`) REFERENCES `gf_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_user_configs`
--

LOCK TABLES `gf_user_configs` WRITE;
/*!40000 ALTER TABLE `gf_user_configs` DISABLE KEYS */;
INSERT INTO `gf_user_configs` VALUES (2,'ck','goldfish',1,1);
/*!40000 ALTER TABLE `gf_user_configs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_user_has_projects_roles`
--

DROP TABLE IF EXISTS `gf_user_has_projects_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_user_has_projects_roles` (
  `gf_users_id` bigint(10) unsigned NOT NULL,
  `gf_projects_id` bigint(10) unsigned NOT NULL,
  `gf_roles_id` bigint(10) unsigned NOT NULL,
  KEY `fk_gf_users_has_gf_projects_gf_projects1_idx` (`gf_projects_id`),
  KEY `fk_gf_users_has_gf_projects_gf_users_idx` (`gf_users_id`),
  KEY `fk_gf_users_has_gf_projects_gf_roles1_idx` (`gf_roles_id`),
  CONSTRAINT `fk_gf_users_has_gf_projects_gf_projects1` FOREIGN KEY (`gf_projects_id`) REFERENCES `gf_projects` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_gf_users_has_gf_projects_gf_roles1` FOREIGN KEY (`gf_roles_id`) REFERENCES `gf_roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_gf_users_has_gf_projects_gf_users` FOREIGN KEY (`gf_users_id`) REFERENCES `gf_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_user_has_projects_roles`
--

LOCK TABLES `gf_user_has_projects_roles` WRITE;
/*!40000 ALTER TABLE `gf_user_has_projects_roles` DISABLE KEYS */;
INSERT INTO `gf_user_has_projects_roles` VALUES (1,1,2),(1,1,1),(1,2,2),(1,3,1),(1,3,2);
/*!40000 ALTER TABLE `gf_user_has_projects_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gf_users`
--

DROP TABLE IF EXISTS `gf_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gf_users` (
  `id` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `names` varchar(255) NOT NULL DEFAULT '',
  `surnames` varchar(255) NOT NULL DEFAULT '',
  `profile` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL DEFAULT '',
  `active` tinyint(1) DEFAULT '0',
  `password_salt` varchar(255) NOT NULL DEFAULT '',
  `token` char(16) DEFAULT NULL,
  `token_exp` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gf_users`
--

LOCK TABLES `gf_users` WRITE;
/*!40000 ALTER TABLE `gf_users` DISABLE KEYS */;
INSERT INTO `gf_users` VALUES (1,'Mats','Fjellner','1174915_10151710581102543_1404515987_n.jpg','matsfjellner@catedra.edu.co','12345',1,'foo',NULL,NULL);
/*!40000 ALTER TABLE `gf_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mdl_learningunit_plugin_settings`
--

DROP TABLE IF EXISTS `mdl_learningunit_plugin_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mdl_learningunit_plugin_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `folder` varchar(150) NOT NULL,
  `data` text,
  `learningunit_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mdl_learningunit_plugin_settings`
--

LOCK TABLES `mdl_learningunit_plugin_settings` WRITE;
/*!40000 ALTER TABLE `mdl_learningunit_plugin_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `mdl_learningunit_plugin_settings` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-01-11 15:59:20
