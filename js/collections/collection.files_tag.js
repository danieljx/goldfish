var goldfish = goldfish || {};

goldfish.FilesTagCollection = Backbone.Collection.extend({
	log: false ? console.log.bind(window.console) : function() { },
	model: goldfish.FileTag,
    url: function() {
        return '/api/user/' + goldfish.app.getUser().get("id") + '/file/tag/';
    },
    //localStorage: new Backbone.LocalStorage(LOCALSTORAGE_NAME),
    initialize: function() {
        //this.on( "add", this.addShare, this);
        //this.on( "remove", this.delShare, this);
    },
    saveEach: function() {
        _(this.models).each( function(share) {
            share.save();
        });
    },
    saveAll: function(opt) {
        opt = opt || {};
        var saveAll     = $.Deferred();
        var formData    = [];
        var collection  = this;
        _(this.models).each(function(tag) {
						formData.push(tag.toJSON());
        });
        formData = JSON.stringify(formData);
        var fetch = collection.fetch({
            type: 'POST',
            reset: false,
            data: {
                tags: formData
            }
        });
        fetch.done(_.bind(function() {
            Backbone.Events.trigger('save:done');
            saveAll.resolveWith(this, [collection]);
        }, this));
        fetch.fail(function() {
            Backbone.Events.trigger('save:fail');
            saveAll.reject();
        });
    }
},{
    search: function(query, options) {
        console.log("File Tag search: ", query);
        var search = $.Deferred();
        options = options || {};
        var collection = new this([], options);
        collection.url = _.result(collection, 'url') + 'search/';
        var fetch = collection.fetch({
            data: {
                q: query,
                path_hash: options.path_hash || ''
            }
        });
        fetch.done(_.bind(function() {
            Backbone.Events.trigger('search:done');
            search.resolveWith(this, [collection]);
        }, this));
        fetch.fail(function() {
            Backbone.Events.trigger('search:fail');
            search.reject();
        });
        return search.promise();
    }
});
