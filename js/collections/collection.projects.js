var goldfish = goldfish || {};

goldfish.ProjectsCollection = goldfish.ProjectBaseCollection.extend({
	model: goldfish.Project,
    url: function() {
        return '/api/user/' + goldfish.app.getUser().get("id") + '/project/';
    },
	current: 0,
 	toJSON: function() {
 		goldfish.log("%cproject collection toJSON",'font-size: 20px');
 		var tmp = array();
 		this.each( function(item, i) {
 			tmp[i] = {
 				id: item.get("id"),
 				title: item.get("title")
 			};
 		});
 		return tmp;
 	}
});