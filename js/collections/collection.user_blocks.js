var goldfish = goldfish || {};

goldfish.UserBlocksCollection = Backbone.Collection.extend({
	log: true ? console.log.bind(window.console) : function() { },
	model: goldfish.UserBlock,
	url: '/goldfish/blocks/',
	current: 0,
	comparator: 'order',
	initialize: function(e) {
		this.on("add", function() { this.log("%cblocks added, now "+this.length,'color: purple; font-style: italic'); });
		this.on("change", function() { this.log("%cblocks changed, now "+this.length,'color: purple; font-style: italic'); });
		this.on("remove", function() { this.log("%cblocks removed, now "+this.length,'color: purple; font-style: italic'); });
	},
	save: function(callback, context){
		this.each( function(item, i) {
			item.save(); 
		});
		callback.apply();
		// Backbone.sync('create', this, { success: callback });
 	},
 	swapItems : function(index1, index2) {
 		this.log("moving "+index1+" to "+index2);
        this.models[index1] = this.models.splice(index2, 1, this.models[index1])[0];
        this.trigger("reorder");
    },
    remove: function(attrs, options) {
    	var me = this;
    	this.log(attrs.get("position"));
    	// var groupBlocks = this.where({group: attrs.get("group") });
    	this.each( function(el) {
    		if (el.get('group') == attrs.get("group") && el.get('position') > attrs.get("position")) {
    			el.set({position: el.get('position') - 1}, {silent: true});
    		}    		

		});
		// this.trigger("remove");
    	Backbone.Collection.prototype.remove.call(this, attrs, options);
    }
});