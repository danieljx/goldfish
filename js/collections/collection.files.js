var goldfish = goldfish || {};

goldfish.FilesCollection = Backbone.Collection.extend({
	log: false ? console.log.bind(window.console) : function() { },
	model: goldfish.File,
	current: 0,
    url: function() {
        return '/api/user/' + goldfish.app.getUser().get("id") + '/file/';
    },
    //localStorage: new Backbone.LocalStorage(LOCALSTORAGE_NAME),
    initialize: function() {
        this.on( "change:checked", this.changeChecked, this);
    },
    changeChecked: function(event) {
        var boKAll = true, boK = false;
            this.forEach(function(dataChild) {
                if(boKAll) {
                    boKAll = dataChild.get("checked");
                }
                if(!boK) {
                    boK = dataChild.get("checked");
                }
            });
            this.trigger('checkAll', {
                boK: boK,
                boKAll: boKAll
            });
    },
    setCheckedAll: function(boK) {
        this.forEach(function(dataChild) {
            dataChild.set("checked", boK);
        });
    },
	downloadFiles: function(opt) {
        this.forEach(function(dataChild) {
            if(dataChild.get("checked")) {
                dataChild.downloadFile();
            }
        });
	},
	downloadFilesZip: function(opt) {
        var arraHas = [];
        this.forEach(function(dataChild) {
            if(dataChild.get("checked")) {
                arraHas.push(dataChild.get("path_hash"));
            }
        });
        console.log(arraHas);
    },
    sortAttribute: null,
    sortOrder: 1,
    _searchFields: ['name'],
    changeSort: function (sortProperty) {
        this.comparator = this.strategies[sortProperty];
        if (this.sortOrder < 0) {
            this.comparator = reverseSortBy(this.comparator);
        }
    },
    changeSortOrder: function() {
        this.sortOrder = -1 * this.sortOrder;
    },
		/*
 	toJSON: function() {
 		goldfish.log("%cfile collection toJSON",'font-size: 20px');
 		var tmp = Array();
 		this.each( function(item, i) {
 			tmp[i] = {
 				id: item.get("id"),
 				name: item.get("name"),
 				path: item.get("path"),
 				path_hash: item.get("path_hash"),
 				children: item.get("children"),
 				shared: item.get("shared"),
 				breadcrumb: item.get("breadcrumb")
 			};
 		});
 		return {files: tmp[0]};
 	}*/
},{
    search: function(query, options) {
        console.log("File search: ", query);
        var search = $.Deferred();
        options = options || {};
        var collection = new this([], options);
        collection.url = _.result(collection, 'url') + 'search/';
        var fetch = collection.fetch({
            data: {
                q: query,
                path_hash: options.path_hash || ''
            }
        });
        fetch.done(_.bind(function() {
            Backbone.Events.trigger('search:done');
            search.resolveWith(this, [collection]);
        }, this));
        fetch.fail(function() {
            Backbone.Events.trigger('search:fail');
            search.reject();
        });
        return search.promise();
    }
});
