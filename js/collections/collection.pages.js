var goldfish = goldfish || {};

goldfish.PagesCollection = Backbone.Collection.extend({
	model: goldfish.Page,
	url: '/goldfish/pages/',
	// localStorage: new Backbone.LocalStorage("pages"),
	current: 0,
	initialize: function(e) {
		this.on("change", function() {
			// this.updated = true; 
			// goldfish.log("collection changed");
		});
		// this.on("add", function() { goldfish.log("%cpages added, now "+this.length,'color: purple; font-style: italic'); });
		// this.on("change", function() { goldfish.log("%cpages changed, now "+this.length,'color: purple; font-style: italic'); });
		// this.on("remove", function() { goldfish.log("%cpages removed, now "+this.length,'color: purple; font-style: italic'); });
		goldfish.log('loading pages');
	},
	save: function(callback, context){
		this.each( function(item, i) {
			item.save();
		});
		callback.apply();
		// Backbone.sync('create', this, { success: callback });
 	},
 	setCurrent: function(idx) {
 		this.current = idx;
 		return this.getCurrentIndex();
 	},
 	getCurrentIndex: function() {
 		return this.current;
 	},
 	getCurrent: function() {
 		return (this.current !== false ? this.at(this.current) : false);
 	},
 	reorder: function(new_index, original_index) {
	    // If nothing is being changed, don't bother
	    if (new_index === original_index) return this;
	    // Get the model being moved
	    var temp = this.at(original_index);
	    // Remove it
	    this.remove(temp, { silent: true });
	    // Add it back in at the new index
	    this.add(temp, { at: new_index, silent: true });
	    console.log(this);
	    return this;
	}
 	// toJSON: function() {
 	// 	var thisJSON = [];
 	// 	this.each( function(item, i) {
 	// 		thisJSON.push(item.get('id') || item.cid);
 	// 	});
 	// 	return thisJSON;
 	// }

});