var goldfish = goldfish || {};

goldfish.FilesShareCollection = Backbone.Collection.extend({
	log: false ? console.log.bind(window.console) : function() { },
	model: goldfish.FileShare,
    url: function() {
        return '/api/user/' + goldfish.app.getUser().get("id") + '/file/shared/';
    },
    //localStorage: new Backbone.LocalStorage(LOCALSTORAGE_NAME),
    initialize: function() {
        //this.on( "add", this.addShare, this);
        //this.on( "remove", this.delShare, this);
    },
 	addUser: function(file, user) {
         console.log(user);
        this.add({
            'file_id': file.get('id'),
            'shared_id': user.get('id'),
            'share_type': 1,
            'share_edit': 1,
            'share_details': false,
            'share_others': false,
            'share_status': 1,
            'share_name': '',
            'mail_send': true,
            'share_user': user,
			'share_action': 'add'
        });
    },
 	addProject: function(file, project) {
         console.log(project);
        this.add({
            'file_id': file.get('id'),
            'shared_id': project.get('id'),
            'share_type': 2,
            'share_edit': 1,
            'share_details': false,
            'share_others': false,
            'share_status': 1,
            'share_name': '',
            'mail_send': true,
            'share_project': project,
			'share_action': 'add'
        });
    },
    saveEach: function() {
        _(this.models).each( function(share) {
            share.save();
        });
    },
    saveAll: function(opt) {
        opt = opt || {};
        var saveAll     = $.Deferred();
        var formData    = [];
        var collection  = this;
        _(this.models).each(function(share) {
            if(share.get("share_action") == "add" || (share.get("share_action") == "up" && share.hasChanged("share_edit"))) {
                formData.push(share.toJSON());
            }
        });
        formData = JSON.stringify(formData);
        var fetch = collection.fetch({
            type: 'POST',
            reset: false,
            data: {
                shared: formData
            }
        });
        fetch.done(_.bind(function() {
            Backbone.Events.trigger('save:done');
            saveAll.resolveWith(this, [collection]);
        }, this));
        fetch.fail(function() {
            Backbone.Events.trigger('save:fail');
            saveAll.reject();
        });
    }
},{
    search: function(query, options) {
        console.log("File Share search: ", query);
        var search = $.Deferred();
        options = options || {};
        var collection = new this([], options);
        collection.url = _.result(collection, 'url') + 'search/';
        var fetch = collection.fetch({
            data: {
                q: query,
                path_hash: options.path_hash || ''
            }
        });
        fetch.done(_.bind(function() {
            Backbone.Events.trigger('search:done');
            search.resolveWith(this, [collection]);
        }, this));
        fetch.fail(function() {
            Backbone.Events.trigger('search:fail');
            search.reject();
        });
        return search.promise();
    }    
});