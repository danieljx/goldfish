var goldfish = goldfish || {};

goldfish.ProjectBaseCollection = Backbone.Collection.extend({
	model: goldfish.ProjectBase,
    url: function() {
        return '/api/user/' + goldfish.app.getUser().get("id") + '/project/';
    }
},{
	search: function(query, options) {
		console.log("Project search: ", query);
		var search = $.Deferred();
		options = options || {};
		var collection = new this([], options);
		collection.url = _.result(collection, 'url') + 'search/';
		var fetch = collection.fetch({
			data: {
				q: query
			}
		});
		fetch.done(_.bind(function() {
			Backbone.Events.trigger('search:done');
			search.resolveWith(this, [collection]);
		}, this));
		fetch.fail(function() {
			Backbone.Events.trigger('search:fail');
			search.reject();
		});
		return search.promise();
	}    
});