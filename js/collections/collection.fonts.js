var goldfish = goldfish || {};

goldfish.FontCollection = Backbone.Collection.extend({
	model: goldfish.Font,
	url: '/goldfish/cssattributes/'
});