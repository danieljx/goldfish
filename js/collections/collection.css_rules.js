var goldfish = goldfish || {};

goldfish.CssRulesCollection = Backbone.Collection.extend({
	model: goldfish.CssRule,
	url: '/goldfish/cssrules/',
	current: 0,
	save: function(callback, context){
		this.each( function(item, i) {
			item.save(); 
		});
		callback.apply();
		// Backbone.sync('create', this, { success: callback });
 	}
});