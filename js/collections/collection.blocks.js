var goldfish = goldfish || {};

goldfish.BlocksCollection = Backbone.Collection.extend({
	model: goldfish.Block,
	current: 0,
	save: function(callback, context){
		this.each( function(item, i) {
			item.save(); 
		});
		callback.apply();
		// Backbone.sync('create', this, { success: callback });
 	}
});