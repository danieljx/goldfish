var goldfish = goldfish || {};

goldfish.UserCollection = goldfish.UserBaseCollection.extend({
	log: false ? console.log.bind(window.console) : function() { },
	model: goldfish.User,
	url: '/api/user/',
    //localStorage: new Backbone.LocalStorage(LOCALSTORAGE_NAME),
	initialize: function(e) {
		this.on("change", function() { this.log("%cblocks changed, now "+this.length,'color: purple; font-style: italic'); });
	}
});