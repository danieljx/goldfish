var goldfish = goldfish || {};

goldfish.CssAttributesCollection = Backbone.Collection.extend({
	model: goldfish.CssAttribute,
	url: '/goldfish/cssattributes/',
	current: 0,
	save: function(callback, context){
		this.each( function(item, i) {
			item.save(); 
		});
		callback.apply();
		// Backbone.sync('create', this, { success: callback });
	 },
	 toJSON: function() {
		 var json = {};
		 this.each( function(item, i) {
			var modelJSON = item.toJSON();
			if (modelJSON.isCustom) {
				json[modelJSON.name] = modelJSON.customValue;
			}
			// if (!goldfish.Helpers.isEmpty(modelJSON)) {
			// 	for(var k in modelJSON) {
			// 		if (modelJSON.hasOwnProperty(k)) {
			// 			json[k]=modelJSON[k];
			// 		}
			// 	}
			// }
		 });
		//  console.log("COLLECTION");
		//  console.log(json);
		 return json;
	 }
});