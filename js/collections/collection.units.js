var goldfish = goldfish || {};

goldfish.UnitsCollection = Backbone.Collection.extend({
	model: goldfish.Unit,
	url: '/goldfish/units/',
	current: 0,
	// localStorage: new Backbone.LocalStorage("units"),
	save: function(callback, context){
		goldfish.log("units save");
		this.each( function(item, i) {
			item.save(); 
		});
		callback.apply();
		// Backbone.sync('create', this, { success: callback });
 	},
 	toJSON: function() {
 		goldfish.log("%cunit collection toJSON",'font-size: 20px');
 		var tmp = array();
 		this.each( function(item, i) {
 			tmp[i] = {
 				id: item.get("id"),
 				title: item.get("title")
 			};
 		});
 		return tmp;
 	}
});