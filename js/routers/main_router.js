var goldfish = goldfish || {};

goldfish.Router = Backbone.Router.extend({
		log: true ? console.log.bind(window.console) : function() { },
        routes: {
        	'auth/:auth': 'setAuth',
        	'*route/auth/:auth': 'setAuth',
        	'': 'goUser',
        	'user/:userId': 'goUser',
        	'project/:projectId': 'goProject',
        	'messages/:userId': 'goNotifications',
        	'project/:projectId/unit/' : 'addUnit',
            'project/:projectId/unit/:unitId' : 'goUnit',
            'tags/:tag' : 'goTags',
            'tags' : 'goTags',
            'tags/' : 'goTags',
            'files/:pathHash' : 'goFile',
            'files' : 'goFile',
			'files/': 'goFile',
			'files/shared': 'goSharedFile',
			'files/shared/': 'goSharedFile',
			'files/shared/:shareHash': 'goSharedFile',
			'files/public': 'goPublicFile',
			'files/public/': 'goPublicFile',
			'files/public/:publicHash': 'goPublicFile',
            'config' : 'goConfig',
            'logout': 'logout'
	    },
	    goUser: function(userId) {
	    	goldfish.log("%cRouter: goUser",'font-size: 30px');
	    	this.navigate('user/'+goldfish.app.getUser().get('id'));
	    	goldfish.app.setActive('projects');
	    },
	    setAuth: function() {
	    	var auth = arguments[arguments.length > 2 ? 1 : 0]; // 2 y no 1 porque el router pasa un argumento extra
	    	goldfish.app.getUser().set('auth',auth);
	    	if (arguments.length > 2) {
	    		this.navigate(arguments[0], {trigger: true});
	    	} else {
				this.navigate('', {trigger: true});
			}

	    },
	    goProject: function(projectId) {
	    	goldfish.log("%cRouter: goProject",'font-size: 30px');
	    	if (!goldfish.app.getUser().hasProject(projectId)) {
	    		goldfish.log("not user project");
	    		return false;
	    	} else {
	    		goldfish.app.loadProject(projectId, function(model, response, options) {
		    		if (goldfish.projectView) {
		    			goldfish.projectView.remove();
		    		}
		    		goldfish.projectView = new goldfish.ProjectView({model: goldfish.app.getProject() });
		    		$('#project').append(goldfish.projectView.render().el);
		    		goldfish.app.setActive('project');
	    		});
	    	}
	    	// this.navigate('project/')

	    },
	    goTags: function(tag) {
			goldfish.log("%cRouter: goTags",'font-size: 30px');
			goldfish.tagCollection	= new goldfish.TagCollection();
			if(tag) {
				goldfish.tagCollection.url	= _.result(goldfish.tagCollection, 'url') + tag;
			}
			goldfish.tagCollection.fetch({
				success: function(model, response, options) {
					if(goldfish.tagView) {
						goldfish.tagView.remove();
					}
					goldfish.tagView = new goldfish.TagView({
						collection: model
					});
					$('#tags').html(goldfish.tagView.render().el);
					goldfish.app.setActive('tags');
				},
				error: function(model, response, options) {
					console.log("%cSave error!",'color: red;');
					toastr.error(response.responseJSON.msg);
				}
			});
		},
	    goFile: function(pathHash) {
	    	goldfish.log("%cRouter: goFile",'font-size: 30px');
			goldfish.app.loadFile(pathHash, function(model, response, options) {
				if (goldfish.fileView) {
					goldfish.fileView.remove();
				}
				var t0 = performance.now();
				goldfish.fileView = new goldfish.UserFilesView({
					collection: goldfish.app.getFile(),
					sharedView: false,
					publicView: false
				});
				$('#files').html(goldfish.fileView.render().el);
				goldfish.app.setActive('files');
				var t1 = performance.now();
				console.log("Files La llamada a hacerAlgo tardó " + (t1 - t0) + " milisegundos.");
			});
	    },
	    goSharedFile: function(shareHash) {
		    	goldfish.log("%cRouter: goSharedFile",'font-size: 30px');
					goldfish.filesShareCollection = new goldfish.FilesShareCollection();
					goldfish.filesShareCollection.fetch({
						data: {
							share_hash: shareHash
						},
						processData: true,
						success: function(model, response, options) {
								if(goldfish.fileView) {
									goldfish.fileView.remove();
								}
								var t0 = performance.now();
								filesCollection 	= new goldfish.FilesCollection(response, { parse: true });
								goldfish.fileView = new goldfish.UserFilesView({
									collection: filesCollection,
									sharedView: true,
									publicView: false
								});
								$('#files').html(goldfish.fileView.render().el);
								goldfish.app.setActive('files');
								var t1 = performance.now();
								console.log("Shared La llamada a hacerAlgo tardó " + (t1 - t0) + " milisegundos.");
						},
						error: function(model, response, options) {
							console.log("%cSave error!",'color: red;');
							toastr.error(response.responseJSON.msg);
						}
					});
		},
		goPublicFile: function(publicHash) {
			goldfish.log("%cRouter: goPublicFile",'font-size: 30px');
			goldfish.filesCollection = new goldfish.FilesCollection();
			goldfish.filesCollection.fetch({
				url: '/api/user/' + goldfish.app.getUser().get("id") + '/file/public/',
				data: {
					public_hash: publicHash
				},
				processData: true,
				success: function(model, response, options) {
					if(goldfish.fileView) {
						goldfish.fileView.remove();
					}
					var t0 = performance.now();
					filesCollection		= new goldfish.FilesCollection(response, { parse: true });
					goldfish.fileView 	= new goldfish.UserFilesView({
						collection: filesCollection,
						sharedView: false,
						publicView: true
					});
					$('#files').html(goldfish.fileView.render().el);
					goldfish.app.setActive('files');
					var t1 = performance.now();
					console.log("Public La llamada a hacerAlgo tardó " + (t1 - t0) + " milisegundos.");
				},
				error: function(model, response, options) {
					console.log("%cSave error!",'color: red;');
					toastr.error(response.responseJSON.msg);
				}
			});
	    },
	    addUnit: function (projectId) {
	    	if (!goldfish.app.getUser().hasProject(projectId)) {
	    		return false;
	    	}
	    	var project = goldfish.app.getUser();

	    },
	    goUnit: function (projectId, unitId) {
	    	this.log("%cRouter: goUnit",'font-size: 30px');
	    	goldfish.log(unitId);
	    	if (!goldfish.app.getUser().hasProject(projectId)) {
	    		return false;
	    	} else {
				var currentProject = goldfish.app.getProject();
				console.log("current Project:");
				console.log(currentProject);
				console.log("project to load:");
				console.log(projectId);

	    		if (!currentProject || (currentProject.get("id") != projectId)) {
	    			console.log("other project");

	    			goldfish.app.loadProject(projectId, function(model, response, options) {
			    		if (goldfish.projectView) {
			    			goldfish.projectView.remove();
			    		}
			    		goldfish.projectView = new goldfish.ProjectView({model: goldfish.app.getProject() });
			    		$('#project').append(goldfish.projectView.render().el);
			    		goldfish.app.loadUnit(unitId, function(model, response, options) {
							this.log("loadUnit success");
							if (!model.get("page")) {
								model.set("page", new goldfish.Page({
									title: '(Untitled)',
									position: 0
								}));
							}
							if (goldfish.unitView) {
								goldfish.unitView.remove({empty: true});
							}
							goldfish.unitView = new goldfish.UnitView({model: model });
				    		$('#unit').replaceWith(goldfish.unitView.render().el);
				    		goldfish.appView.dashBoardToggle(false);
				    		goldfish.app.setActive('unit');
			    		}.bind(this));
		    		}.bind(this));
	    		} else {
					console.log("same project");

					if (!goldfish.app.get("activeUnit") || (goldfish.app.get("activeUnit").get("id") != unitId)) {
						console.log("no active unit, or unit is different");
						if (goldfish.unitView) {
							console.log(goldfish.unitView.$el.first());
							goldfish.unitView.remove({empty: true});
						}
						goldfish.app.loadUnit(unitId, function(model, response, options) {
							console.log("unit loaded");
							goldfish.unitView = new goldfish.UnitView({model: goldfish.app.getUnit() });
							$('#unit').replaceWith(goldfish.unitView.render().el);
							goldfish.appView.dashBoardToggle(false);
							goldfish.app.setActive('unit');
						});
					} else {
						console.log("same unit");
						goldfish.app.setActive('unit');
					}

	    		}

	    	}

	    },
	    goConfig: function() {
	    	goldfish.app.setActive('config');
	    },
	    goNotifications: function() {
	    	goldfish.app.setActive('notifications');
	    },
	    logout: function() {
	    	goldfish.log("%cRouter: logout",'font-size: 30px');
	    	$.ajax({
           type: "GET",
           url: "slim-api/public/auth/logout",

           success: function (msg) {
           	goldfish.log(msg);
               if (msg && msg.result === "ok") {
               	goldfish.log("logout redirect");
               	window.location.replace('/');
               } else {
                   goldfish.log("no response");
               }
           },


       });
	    }
    });
