var goldfish = goldfish || {};

goldfish.FileListViewSelect = Backbone.View.extend({
    template: _.template($('#tpl-files-modal-select-list').html()),
    tagName: 'div',
    className: '',
    events: {
        'click tr': 'goFileClick'
    },
    publicView: false,
    sharedView: false,
    selectedFile: function(file){},
    initialize : function(options) {
        this.close          = options.close || this.close;
        this.selectedFile   = options.selectedFile || this.selectedFile;
        this.listenTo(this.model, 'change', this.render);
    },
    render : function() {
        var me = this;
        this.collection = this.model.get("children");
        this.$el.html(this.template(this.model.toJSON()));
        this.breadcrumbView = new goldfish.FileBreadcrumbView({
            model: this.model,
            sharedView: this.sharedView,
            publicView: this.publicView,
            events: {
                'click div.breadcrumb > a': this.goFileBreadcrumb.bind(this)
            }
        });
        this.$('.breadcrumbs').html(this.breadcrumbView.render().el);
        this.$body  = this.$('.lists > table > tbody');
        this.$body.empty();
        this.collection.forEach(function(dataChild) {
            me.renderItem(dataChild);
        });
        this.$(".ui.search").search({
            type: "category",
            showNoResults: true,
            minCharacters: 3,
            onSelect: function (file, response) {
                var fileModel = new goldfish.File(file, { parse: true });
                if (fileModel.get('mimetype') == 2) {
                    me.goFile(fileModel.get('path_hash'), fileModel.get('mimetype'));
                } else {
                    me.selectedFile(fileModel);
                    me.close();
                }
                return false;
            },
            apiSettings: {
                url: "/api/user/" + goldfish.app.getUser().get("id") + "/file/search/{query}"
            }
        });
        return this;
    },
    renderItem: function(model) {
        var view = new goldfish.FileItemViewSelect({
            model: model,
            selectedFile: this.selectedFile,
            close: this.close
        });
        this.$body.append(view.render().el);
    },
    setSuccess: function(model, response, options) {
        //console.log(response);
    },
    goFile: function (hash, mimetype) {
        if(mimetype == 2) {
            var data = {}, url = '/api/user/' + goldfish.app.getUser().get("id") + '/file/';
            if(hash == 'public/' || this.publicView) {
                url += 'public/';
                if(hash == 'public/') {
                    hash = "";
                }
                this.publicView = true;
                this.sharedView = false;
                data = {
                    public_hash: hash
                };
            } else if (hash == 'shared/' || this.sharedView) {
                url += 'shared/';
                if(hash == 'shared/') {
                    hash = "";
                }
                this.publicView = false;
                this.sharedView = true;
                data = {
                    share_hash: hash
                };
            } else {
                this.publicView = false;
                this.sharedView = false;
                data = {
                    path_hash: hash
                };
            }
            this.model.fetch({
                url: url,
                data: data,
                processData: true,
                success: this.setSuccess
            });
        } else {
            console.log(hash);
        }
    },
    goFileClick: function (e) {
        e.preventDefault();
        e.stopPropagation();
        var $tr = $(e.currentTarget);
        var hash = $tr.attr('path_hash'), mimetype = $tr.attr('mimetype');
        this.goFile(hash, mimetype);
        return false;
    },
    goFileBreadcrumb: function (e) {
        e.preventDefault();
        e.stopPropagation();
        console.log(this);
        var $a = $(e.currentTarget);
        console.log($a);
        var href = $a.attr('href'), mimetype = 4;
        if (href == '#files/public/') {
            hash = 'public/';
            mimetype = 2;
            this.publicView = true;
            this.sharedView = false;
        } else if (href == '#files/shared/') {
            hash = 'shared/';
            mimetype = 2;
            this.publicView = false;
            this.sharedView = true;
        } else if (href == '#files/') {
            hash = '';
            mimetype = 2;
            this.publicView = false;
            this.sharedView = false;
        }
        console.log(hash);
        this.goFile(hash, mimetype);
        return false;
    }
});