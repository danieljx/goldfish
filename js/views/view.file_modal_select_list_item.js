var goldfish = goldfish || {};

goldfish.FileItemViewSelect = Backbone.View.extend({
    template: _.template($('#tpl-files-modal-select-list-item').html()),
    tagName: 'tr',
    events: {
        'click td.action > .buttons > button.select': 'selectFile'
    },
    close: function(){},
    selectedFile: function(file){},
    initialize: function(options) {
        this.close          = options.close || this.close;
        this.selectedFile   = options.selectedFile || this.selectedFile;
    },
    render: function () {
        this.$el.html(this.template(this.model.toJSON()));
        this.$el.attr({
            path_hash: this.model.get('path_hash'),
            mimetype: this.model.get('mimetype')
        });
        return this;
    },
    selectFile : function(e) {
        e.preventDefault();
        e.stopPropagation();
        this.selectedFile(this.model);
        this.close();
        return false;
    }
});