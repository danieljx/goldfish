var goldfish = goldfish || {};

goldfish.FileUpView = Backbone.View.extend({
    template: _.template($('#tpl-files-up').html()),
    tagName: 'form',
    className: 'form-file',
    files: [],
    filesSend: [],
    filesSendView: [],
    currentFile: 0,
    initialize : function(options) {
        this.collection = this.model.get('children');
        FileList.prototype.forEach = function(callback) {
            [].forEach.call(this, callback);
        };
    },
    events: {
        "submit": "submitFile",
        "change input#files[type=file]": "setFile",
        "dragstart #holder": "dragStarFile",
        "dragenter #holder": "dragEnterFile",
        "dragover #holder": "dragOverFile",
        "dragleave #holder": "dragLeaveFile",
        'dragend #holder': 'dragEndFile',
        'drop #holder': 'dropFile'
    },
    render : function() {
        this.$el.html(this.template(this.model.toJSON()));
        this.$el.attr({enctype:"multipart/form-data"});
        return this;
    },
    setFile: function(e) {
        e.preventDefault();
        this.$input = $(e.currentTarget);
        this.files  = this.$input.get(0).files;
        this.prepareFiles();
    },
    prepareFiles: function() {
        if(this.files.length > 0) {
            var me = this;
            this.files.forEach(function(file, index) {
                var itemFileUp = new goldfish.FileUp({
                        file: file
                    });
                    itemFileUp.set('path_hash', me.model.get("path_hash"));
                    itemFileUp.on('success', function(evt) {
                        me.currentFile++;
                        if(me.currentFile < me.files.length) {
                            me.submitFile(me.filesSend[me.currentFile]);
                        } else {
                            console.log("%csuccess All", 'color: green');
                            me.resetFiles();
                        }
                    });
                var itemFileUpView = new goldfish.FileUpItemView({
                        model: itemFileUp
                    });
                    me.filesSend.push(itemFileUp);
                    me.filesSendView.push(itemFileUpView);
                    me.$("div.upload-progress > ul.ul-up").append(itemFileUpView.render().el);
            });
            this.submitFiles();
        }
    },
    submitFiles: function() {
        if(this.filesSend.length > 0) {
            this.submitFile(this.filesSend[this.currentFile]);
        }
        return false;
    },
    submitFile: function(file) {
        var me = this;
        file.save(file.attributes,{
            success: function(model, response, options) {
                me.collection.add(me.parseResponse(response));
                toastr.success("Archivo: " + response.name + " Subido");
            },
            error: function(model, response, options) {
                console.log("%cSave error!",'color: red;');
                toastr.error(response.responseJSON.msg);
            }
        });
    },
    parseResponse: function(data) {
        return new goldfish.File(data, { parse: true });
    },
    resetFiles: function() {
        //this.$input.val("");
        this.files          = [];
        this.filesSend      = [];
        this.filesSendView  = [];
        this.currentFile    = 0;
    },
    dragStarFile: function(e, data, clone, element) {
        e.preventDefault();
        e.stopPropagation();
        return false;
    },
    dragEnterFile: function(e, clone, element) {
        e.preventDefault();
        e.stopPropagation();
        return false;
    },
    dragOverFile: function(e) {
        e.preventDefault();
        e.stopPropagation();
        return false;
    },
    dragLeaveFile: function(e, clone, element) {
        e.preventDefault();
        e.stopPropagation();
        this.$hover = $(e.currentTarget);
        this.$hover.removeClass('dragStart');
        return false;
    },
    dropFile: function (e, data, clone, element) {
        e.preventDefault();
        e.stopPropagation();
        this.$hover = $(e.currentTarget);
        this.files  = e.originalEvent.dataTransfer.files;
        this.prepareFiles();
        this.$hover.removeClass('dragStart');
        return false;
    }
});
