var goldfish = goldfish || {};

goldfish.UserBlockTools = Backbone.View.extend({
	tagName: 'div',
	className: 'tools cat-ui',
   template: _.template($('#tpl-block-tools').html()),
   log: true ? console.log.bind(window.console) : function() { },
   initialize: function() {
      this.listenTo(this.model, "remove", this.remove);
	},
	render: function() {
      var modelJSON = this.model.toJSON();
      modelJSON.config = this.model.hasConfig();
      modelJSON.debug = goldfish.debug;
      this.$el.html( this.template( modelJSON ) );
		return this;
      },
      blocksMode: function(on) {
            if (on) {
                  this.$el.addClass('block-mode');
            }
      },
	events: {
      'click .tool-css': function(e) {
         e.preventDefault();
         e.stopPropagation();
         this.model.toggleCSS();
      },
      'click .tool-config': function(e) {
            e.preventDefault();
            e.stopPropagation();
            this.model.toggleConfig();
         },
      'click .tool-info': function(e) {
         e.preventDefault();
         e.stopPropagation();
         goldfish.log(this.model.id);
         goldfish.log(this.model.toJSON());
      //    goldfish.log(this.model);
      },
      'click .tool-html': function(e) {
         e.preventDefault();
         e.stopPropagation();
         goldfish.appView.loadEditor(this.model);
      },
      'click .tool-files': function(e) {
            e.preventDefault();
            e.stopPropagation();
            var me = this;
            this.fileViewSelect = new goldfish.FileViewSelect({
                  modelListen: this.model,
                  selectedFile: function(file) {
                        console.log(me.model.toJSON());
                        $body = $(me.model.get('body'));
                        $body.attr({
                              name: file.get('name'),
                              path_hash: file.get('path_hash')
                        });
                        $body.html("<img src=\"http://goldfish/api/user/" + goldfish.app.getUser().get("id") + "/file/view/" + file.get('path_hash') + "\" alt=\"" + file.get('name') + "\">");
                        me.model.set('body', $body[0].outerHTML);
                  }
            });
            return false;
      },
      'click .tool-remove': function(e) {
         e.preventDefault();
         e.stopPropagation();
         this.log(this.model.collection.models);
         // this.model.trigger('destroy', this.model);
         this.model.collection.remove(this.model);
      }
  }


});
