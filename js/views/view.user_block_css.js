var goldfish = goldfish || {};

goldfish.UserBlockCSSView = Backbone.View.extend({
	log: true ? console.log.bind(window.console) : function() { },
	tagName: 'aside',
	className: 'css-edit',
	template: _.template($('#tpl-block-css').html()),
	// viewBody: '',
	css: '',

	initialize: function() {
		this.itemViews = {};
	},
	render: function() {
		var cssQueries = this.model.get('cssQueries');

		this.$el.html( this.template( this.model ) );
		var queryViews = $('<section></section>');
		cssQueries.each( function(el) {
			this.log("ADDING QUERY VIEW");
			this.itemViews[el.cid] =  new goldfish.UserBlockCSSQueryView ( {model: el });
			queryViews.append(this.itemViews[el.cid].render().attach());		
		}.bind(this));
		
		this.$el.append(queryViews);

		cssQueries.each( function(el) {
			this.itemViews[el.cid].postRender();
		}.bind(this));
		
	    return this;
	},
	postRender: function() {
		// this.log(this.$('.ui.checkbox.toggle').length);
		this.$('.ui.checkbox.toggle').checkbox();
		this.$('.ui.dropdown').dropdown();
		this.$('input.color-pick').spectrum({
			showButtons: false,
			showAlpha: true,
			showInput: true,
			move: function(aColor) {
				$(this).val(aColor.toRgbString());
				$(this).trigger("input");
			}
		});
		// this.delegateEvents();
	},
	_isAttached: function() {
		return (this.$el.parent().length > 0);
	},
	attach: function() {
		return this.el;
	},
	detach: function() {
		this.viewState = 'view';
	},
	updateModel: function(aTarget) {
		var aCid = aTarget.attr('data-cid');
		var rule = this.model.getCSSRule(aCid);
		if (aTarget[0].tagName.toLowerCase() === 'input') { 
			goldfish.log(aTarget.val());
			rule.setAttribute(aTarget.prevAll('span').html(), aTarget.val());
		} else {
			rule.setAttribute(aTarget.prevAll('span').html(), aTarget.html());
		}
		
		
		
		// this.model.set('body',this.$el.find('.block').first().html(),{silent: true});
		return true;
	}
});