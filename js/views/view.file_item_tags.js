var goldfish = goldfish || {};

goldfish.FileItemTagsView = Backbone.View.extend({
    template: _.template($('#tpl-files-item-tags').html()),
    tagName: 'div',
    className: 'ui modal modalTags',
    addCount: 0,
    dataTags: [],
    events: {
        'click a.addTag': 'addTagButton',
        'click .button.save': 'save'
    },
    initialize: function(opt) {
        this.sharedView = opt.sharedView;
        this.publicView = opt.publicView;
        this.collection = this.model.get('tags');
        this.listenTo(this.collection, 'add', this.addTag);
        this.listenTo(this.collection, 'reset', this.resetTag);
        this.listenTo(this.collection, 'add remove change', this.changeTags);
        this.listenTo(this.collection, 'change:tag_action', this.changeAction);
    },
    render : function() {
      var me = this;
      var data            = this.model.toJSON();
          data.sharedView = this.sharedView;
          data.publicView = this.publicView;
          data.shared     = data.shared.toJSON();
          this.$el.attr({
              id: 'modalTags_' + this.model.get("id")
          });
          this.$el.html(this.template(data));
          this.$tagsSel = this.$('.ui.dropdown').dropdown({
            allowAdditions: true,
            glyphWidth: '1em',
            minCharacters: 2,
            regExp : {
              escape   : /[-[\]{}()*+?.,\\^$|#\s]/g,
              //escape   : /[-[\]{}()*+?.,!°¬@·~½¬"%&/=¡\\^$|#\s]/g,
              //escape   : /[^\w]/g,
            },
            keys: {
              delimiter: 32
            },
            apiSettings: {
                url: '/api/user/' + goldfish.app.getUser().get("id") + '/tag/search/{query}',
                onResponse: function(response) {
                    console.log(response);
                    return response;
                }
            },
            onAdd: function (id, name, $addedChoice) {
                me.dataTags = me.dataTags.filter(function (item) {
                    return item.id != id;
                });
                me.dataTags.push({
                    id: id,
                    name: name
                });
            },
            onRemove: function (id, name, $removedChoice) {
                me.dataTags = me.dataTags.filter(function(item) {
                    return item.id != id;
                });
            }
          });
          this.$tags = this.$('div.segment.tags');
          this.collection.forEach(function(dataChild) {
              me.renderTag(dataChild);
          });
          return this;
    },
    addTagButton: function(e) {
        e.preventDefault();
        e.stopPropagation();
        var me = this;
        if(this.dataTags.length) {
            this.dataTags.forEach(function(item, i) {
                me.collection.add({
                    id: item.id,
                    file_id: me.model.get('id'),
                    name: item.name,
                    key: item.name
                });
            });
        }
        console.log(this.collection);
        console.log(this.collection.getIds());
        this.$tagsSel.dropdown('restore defaults');
        return false;
    },
    changeAction: function(model, value, options) {
        if(value == 'del' && model.previous("tag_action") == 'add') {
          this.delTag(model);
        }
    },
    changeTags: function(dataChild) {
        this.$el.find('.button.save').toggleClass('disabled', false);
    },
    resetTag: function(models) {
      var me = this;
      this.$tags.empty();
      this.collection.forEach(function(dataChild) {
          me.renderTag(dataChild);
      });
    },
    addTag: function(dataChild) {
        this.renderTag(dataChild);
    },
    delTag: function(dataChild) {
        this.collection.remove(dataChild);
    },
    renderTag : function(tag) {
      this.tags  = new goldfish.FileItemTagView({
          model: tag,
          sharedView: this.sharedView,
          publicView: this.publicView,
          file: this.model
      });
      this.$tags.prepend(this.tags.render().el);
    },
    save: function(e) {
        e.preventDefault();
        e.stopPropagation();
        var me = this;
        //console.log(this.model.get('tags').toJSON());
        this.model.save(this.model.attributes, {
            silent: true,
            type: 'POST',
            url: '/api/user/' + goldfish.app.getUser().get("id") + '/file/tags/',
            success: function(model, response, options) {
                console.log("%cSuccess!","color: green");
                console.log(response.tags);
                me.collection.reset(response.tags);
                me.$el.find('.button.save').toggleClass('disabled', true);
            },
            error: function(model, response, options) {
                console.log("%cSave error!",'color: red;');
            }
        });
        //this.$el.modal('hide');
        return false;
    }
});
