var goldfish = goldfish || {};

goldfish.UserConfigView = Backbone.View.extend({
	template: _.template($('#tpl-config').html()),
	initialize: function() {
		this.render();
	},
	render: function() {
		this.$el.html( this.template(this.model.toJSON()) );
	},

	events: {
		'change .theme-pick': function(e) {
			this.model.set('theme',$(e.target).val());
			goldfish.appView.setTheme( this.model.get('theme') );
		},
		'change .effects': function(e) {
			this.model.set('effects',$(e.target).prop('checked') ? 1 : 0);
			goldfish.appView.setEffects( this.model.get('effects') );
		},
		'change' : function(e) {
			this.model.save();
		}
	}
});