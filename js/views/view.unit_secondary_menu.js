var goldfish = goldfish || {};

goldfish.UnitSecondaryMenuView = Backbone.View.extend({
   template: _.template($('#tpl-secondary-menu').html()),
   initialize: function() {
      me = this;
      this.render();
      this.$styles = this.$('.secondary-styles');
      this.$blocks = this.$('.secondary-blocks');
	},
  render: function() {
    this.$el.html( this.template(this.model.toJSON()) );
  },
	events: {
      'click > ul > li > a': function(e) {
         e.preventDefault();
         // goldfish.log("optionswitch");
         this.optionSwitch(e);
      },
      'click .secondary-menu-toggle': function(e) {
        e.preventDefault();
        console.log("secondary menu click (unit)");
        this.model.secondaryMenuViewToggle();
        // this.$el.toggleClass('on');
      }
  },
   toggle: function(on) {
    this.$el.toggleClass('on', on);
   },
   optionSwitch: function(e) {
    // goldfish.log(e);
    var li = $(e.target).closest('li');
    li.siblings().removeClass('on');
    li.toggleClass('on');
  },
   loadComponents: function(blockViews) {
    console.log("loading components..");
    console.log(blockViews);
    console.log(this.$blocks);
    // TODO: cambiar esto a ser su propio view
    var componentTypes = {1: 'Bloques HTML', 2: 'Recursos complejos', 3: 'Recursos Imagen', 4: 'Cuestionarios'};
    var tmp = '';


    // goldfish.log(blockViews);

    for (var type in componentTypes) {
      if (componentTypes.hasOwnProperty(type)) {
        tmp += '<ul class="component-type-'+type+'"><h4>'+componentTypes[type]+'</h4>';

        for (var key in blockViews) {

          if (blockViews.hasOwnProperty(key) && (blockViews[key].model.get('type') == type) && !blockViews[key].model.get('editOnly') && blockViews[key].model.get('selectable')) {
            console.log("ok");
            tmp += '<li><svg viewBox="0 0 32 32" width="0" height="0"><use xlink:href="img/icons/template.svg#icon-1"></use></svg>'+blockViews[key].preview()+'</li>';
          }
        }
        tmp += '</ul>';
      }

    }

      this.$blocks.find('div').html(tmp);
     this.activateDraggables();
   },

   activateDraggables: function() {
    me = this;
    this.$blocks.find('li').each( function() {
      if ($(this).data('draggable')) {
        $(this).draggable('destroy');
      }
        $(this).draggable({
          opacity: 0.7,
          start: function() { goldfish.app.setDragging(true); },
          stop: function() { goldfish.app.setDragging(false); } ,
          // helper: "clone",
          revert: true,
          // containment: '#unit section.page',

          cursor: 'move'
        });
    });
   },
   loadCSS: function(aBlock) {
    var cssView = new goldfish.UserBlockCSSView({model: aBlock});
    this.$styles.find('.block').remove();
    this.$styles.append(cssView.render().attach());
    this.$styles.toggleClass('on');
   }
});
