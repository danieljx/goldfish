var goldfish = goldfish || {};

goldfish.FileModalShareListItemView = Backbone.View.extend({
    template: _.template($('#tpl-modal-share-list-item-files').html()),
    tagName: 'li',
    className: 'shared-option',
    events: { },
    initialize: function(opt) {
        this.listenTo(this.model, 'remove', this.delShare);
        this.listenTo(this.model, 'change:share_action', this.changeAction);
        this.file       = opt.file;
        this.shared_user= this.model.toJSON();
        this.user       = this.model.get('share_user');
        this.project    = this.model.get('share_project');
        this.data = {
            shared_user: this.shared_user,
            user: (this.user?this.user.toJSON():{}),
            project: (this.project?this.project.toJSON():{})
        };
        $('input[name=share_others]').on('change', function() {
          this.$shareOthers   = $(this).val();
          console.log(this.$shareOthers);
        });
        $('input[name=share_details]').on('change', function() {
          this.$shareDetails   = $(this).val();
          console.log(this.$shareDetails);
        });
    },
    render: function() {
        var me = this;
        this.$el.html(this.template(this.data));
        this.$('.action.ui.dropdown').dropdown({
          action: 'activate',
          onChange: function(value, text, $selectedItem) {
            me.selectAction(value);
          }
        });
        this.$shareOthers   = $('input[name=share_others]').val();
        this.$shareDetails  = $('input[name=share_details]').val();
        return this;
    },
    changeAction: function(data) {
        console.log("changeAction");
        console.log(data);
        if(data.get('share_action') == 'del') {
          this.delShare();
        }
    },
    delShare: function() {
        console.log("delShare");
        this.undelegateEvents();
        this.$el.removeData().unbind();
        this.remove();
        Backbone.View.prototype.remove.call(this);
        return false;
    },
    selectAction: function(val) {
      console.log(val);
      if(val != "del") {
        this.model.set('share_edit', val);
        if(val == 2) {
          this.model.set('share_others', 1);
          this.model.set('share_details', 2);
        } else {
          this.model.set('share_others', this.file.get('share_others'));
          this.model.set('share_details', this.file.get('share_details'));
        }
      } else {
        this.model.set('share_action', val);
      }
    }
});
