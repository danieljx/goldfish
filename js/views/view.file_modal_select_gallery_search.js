var goldfish = goldfish || {};

goldfish.FileGallerySearchViewSelect = Backbone.View.extend({
    template: _.template($('#tpl-files-modal-select-gallery-search').html()),
    tagName: 'div',
    className: 'ui fluid icon input',
    events: {
        'keyup > input': 'keyAction'
        //'click td.action > .buttons > button.select': 'selectFile'
    },
    close: function(){},
    selectedFile: function(file){},
    initialize: function(options) {
        this.close          = options.close || this.close;
        this.selectedFile   = options.selectedFile || this.selectedFile;
    },
    render: function () {
        this.$el.html(this.template(this.collection.toJSON()));
        return this;
    },
    keyAction: function (e) {
        var $i = $(e.currentTarget);
        var char = String.fromCharCode(e.which);
        //if (!char.match(/[A-Za-z0-9+\s]/)) {
        //    return false;
        //}
        var word = $i.val();
        console.log(word);
        if (word.length > 2 || word.length < 1) {
            this.updateSearch(word);
        }
    },
    updateSearch: function (word) {
        var me = this;
        console.log(word);
        this.collection.fetch({
            url: '/api/user/' + goldfish.app.getUser().get("id") + '/file/gallery/' + word,
            add: true,
            remove: true,
            beforeSend: function () {
                //me.$loadGallery.addClass("active");
            },
            success: function (model, response, options) {
                //console.log(response);
                //console.log(model);
                //me.$loadGallery.removeClass("active");
            },
            error: function (model, response, options) {
                console.log("%cSave error!", 'color: red;');
                toastr.error(response.responseJSON.msg);
                //me.$loadGallery.removeClass("active");
            }
        });
    }
});