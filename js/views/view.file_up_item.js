var goldfish = goldfish || {};

goldfish.FileUpItemView = Backbone.View.extend({
    template: _.template($('#tpl-files-up-item').html()),
    tagName: 'li',
    className: 'li-up warning',
    newFile: {},
    controls: false,
    success: false,
    cancel: false,
    events: {
        //"click .controls > .controls-pause": "setPause",
        "click .controls > .controls-cancel": "setAbort"
    },
    initialize : function(options) {
        var me = this;
        this.newFile = {
            name: this.model.get('file').name,
            size: this.totalSize(this.model.get('file').size)
        };
        this.model.on('progress', function(evt) {
            var cant = me.progressRender(evt);
            console.log("%cprogress " + cant + "% => " + me.newFile.name, 'color: yellow');
            me.setProgress();
        });
        this.model.on('success', function(evt) {
            me.setSuccess();
        });
        this.model.on('abort', function(evt) {
            me.removeItem();
        });
    },
    render : function() {
        this.$el.html(this.template(this.newFile));
        return this;
    },
    totalSize: function(bytes, decimals) {
        if(bytes == 0) return '0 Bytes';
        var k = 1024,
            dm = decimals || 2,
            sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
            i = Math.floor(Math.log(bytes) / Math.log(k));
        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
    },
    progressRender: function(num) {
        return parseInt(num*100);
    },
    setSuccess: function() {
        var me = this;
        this.$el.removeClass('progress').removeClass('warning');
        this.$el.addClass('succes');
        this.controls   = false;
        this.success    = true;
        setTimeout(function () {
            me.removeItem();
        }, 3000);
    },
    setProgress: function() {
        this.$el.removeClass('succes').removeClass('warning');
        if(!this.$el.hasClass('progress')) {
            this.controls  = true;
            this.$el.addClass('progress');
        }
    },
    setAbort: function() {
        if(this.controls && !this.success) {
            this.model.abort();
        } else if(this.success) {
            this.removeItem();
        }
    },
    removeItem: function() {
        this.undelegateEvents();
        this.$el.removeData().unbind(); 
        this.remove();  
        Backbone.View.prototype.remove.call(this);
    }
});