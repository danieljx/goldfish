var goldfish = goldfish || {};

/** User page component view
 * @author Mats Fjellner
 * @augments Backbone.View
 * @constructor
 * @name UserBlockView
 */
goldfish.UserBlockView = goldfishView.extend( /** @lends UserBlockView */ {
	tagName: 'div',
	className: 'block',
	repeatTool: '<div class="repeat-tool cat-ui"><svg viewBox="0 0 32 32" width="0" height="0" class="repeat-add"><use xlink:href="img/icons/plus.svg#icon-1"></use></svg><svg viewBox="0 0 32 32" width="0" height="0" class="repeat-remove"><use xlink:href="img/icons/minus.svg#icon-1"></use></svg></div>',
	log: true ? console.log.bind(window.console) : function() { },
	// repeatTool: '<div class="repeat-tool cat-ui"><svg viewBox="0 0 32 32" width="0" height="0" class="repeat-add"><use xlink:href="img/icons/plus.svg#icon-1"></use></svg><svg viewBox="0 0 32 32" width="0" height="0" class="repeat-remove"><use xlink:href="img/icons/minus.svg#icon-1"></use></svg></div>',
	debug: true,
	viewName: 'UserBlockView',
	initialize: function (options) {
		this.setLog();
		this.log("Hola");
		this.log("init user block view " + this.cid + "(" + this.model.get("name") + ")", "font-size: 30px; color: green");
		if (this.model.get('repeatable') !== '') {
			this.repeatTool = $('<div class="repeat-tool cat-ui"><svg viewBox="0 0 32 32" width="0" height="0" class="repeat-add"><use xlink:href="img/icons/plus.svg#icon-1"></use></svg><svg viewBox="0 0 32 32" width="0" height="0" class="repeat-remove"><use xlink:href="img/icons/minus.svg#icon-1"></use></svg></div>');
		}
		this.subViews = {};
		this.showCss = false;
		this.showConfig = false;
		this.css = '';
		this.page = false;
		this.toolbar = new goldfish.UserBlockTools({
			model: this.model
		});
		this.pluginFunctions = (this.model.get("data") && this.model.get("data").functions) ? this.model.get("data").functions : false;
		this.model.on('attachView', this.attach);
		this.listenTo(this.model.get("blocks"), "remove", this.childRemove);
		this.listenTo(this.model, "change", this.render);

		this.listenTo(this.model, "toggle-css", this.toggleCSS);

		this.listenTo(this.model, "toggle-config", this.toggleConfig);

		this.listenTo(this.model, "css-change", function () {
			this.log("CSS Change");
			this.updateCSS();
		});

		this.listenTo(this.model, "remove", function () {

			this.log("%cComponent view remove " + this.cid, "font-size:30px; color: #fa0;");
			this.page.unregisterUserBlockView(this.cid);
			goldfish.appView.toggleExtraCSS((this.viewState === 'edit' ? 'block-' : 'previewblock-') + this.model.cid, this.css);
			this.remove();
		});
		this.page = goldfish.app.getUnit().getCurrentPage();
		this.page.registerUserBlockView(this.cid, this);
	},
	toggleCSS: function (force) {

		if (this.showConfig) {
			this.toggleConfig(false);
		}
		var prev = this.showCss;
		this.showCss = (typeof force !== 'undefined') ? force : !this.showCss;
		if (prev === this.showCss) {
			return false;
		}
		if (this.showCss) {
			// var t1 = performance.now();
			this.cssView = new goldfish.UserBlockCSSView({
				model: this.model
			});
			this.$el.append(this.cssView.render().attach());
			this.cssView.postRender();
			// var t2 = performance.now();
			// this.log("%cCSS VIEW RENDER " + (t2 - t1) + " ms", "background-color: black; color: #f50; padding: 4px;");
		} else {
			this.cssView.remove();
		}

		console.log("SHOW CSS");
	},
	toggleConfig: function (force) {

		if (this.showCss) {
			this.toggleCSS(false);
		}
		var prev = this.showConfig;
		this.showConfig = (typeof force !== 'undefined') ? force : !this.showConfig;
		if (prev === this.showConfig) {
			return false;
		}
		if (this.showConfig) {
			this.configView = new goldfish.UserBlockConfigView({
				model: this.model
			});
			this.$el.append(this.configView.render().attach());
			this.configView.postRender();
		} else {
			this.configView.remove();
		}

		console.log("SHOW CONFIG");
	},
	childRemove: function (options) {
		this.log("Child change in " + this.cid, "color: #0ff; font-size: 30px;");
		this.log(options);
		delete this.subViews[options];
	},
	/** Renders the current component, along with nested components interspersed with drop zones and toolbar
	 */
	render: function () {
		this.log("USER BLOCK RENDER " + this.model.get("name") + " " + this.cid, "color: #0ff; font-size: 30px;");
		var tmp, tmp2;
		var cssQueries = this.model.get('cssQueries');
		var blocks = this.model.getBlocks(); // Subblocks
		// this.log("%crendering user block "+this.cid+" with "+blocks.length+" blocks.","color: green");

		this.$el.html('');
		console.log( this.model.attributes );
		
		var q = 23;
		this.pluginFunction('render', q);
		
		this.$el.html( _.template( this.model.get("body") )( this.model.toJSON() ) );
		this.$el.find('[gf-editable]').attr('contenteditable', 'true');

		this.$el.find('[data-placeholder]').each( function() {
			console.log( $(this).attr('data-placeholder'));
			$(this).prepend( '<div class="placeholder">['+$(this).attr('data-placeholder')+']</div>');
		});
		// this.log(this.$el.html());
		this.log(blocks);
		this.log(this.subViews);
		// this.log("%cIterate blocks",'background-color: black; color: green; font-size: 14px;');
		this.$el.find('blocks').each(function (i, el) {
			this.log("found block in " + this.model.get("body") + ", i:" + i + ", blocks.length: " + blocks.length);
			var groupBlocks = this.model.get("blocks").where({
				group: i
			});
			var newEl = this._dropZone(groupBlocks.length === 0, i, 0);

			for (var j = 0; j < groupBlocks.length; j++) {
				// me.log("adding blockview at "+i+","+j);
				// me.log(groupBlocks[j]);
				if (!this.subViews[groupBlocks[j].cid]) {
					this.subViews[groupBlocks[j].cid] = new goldfish.UserBlockView({
						model: groupBlocks[j]
					});
				}

				newEl = newEl.add(this.subViews[groupBlocks[j].cid].render().attach());
				newEl = newEl.add(this._dropZone(false, i, j + 1));
			}
			$(el).replaceWith(newEl);
		}.bind(this));

		if (!this.model.get('main')) {
			this.$el.append(this.toolbar.render().$el);
			this.log(this.subViews);
			this.log(this.model.attributes);
			this.log(Object.keys(this.subViews).length);
			if (Object.keys(this.subViews).length < 1) {
				this.log("editable");
				// this.$el[0].contentEditable = "true";	

			} else {
				this.log("not editable");
				this.toolbar.blocksMode(true);
			}

		} else {
			this.$el.addClass('main');
		}

		this.toolbar.delegateEvents();

		this.$el.attr('id', 'block-' + this.model.cid);
		this.$el.attr('data-group', this.model.get('group'));
		this.$el.attr('data-position', this.model.get('position'));
		this.$el.attr('data-cid', this.cid);

		this.delegateEvents();
		this.log("USER BLOCK RENDER FINISH " + this.model.get("name") + " " + this.cid, "color: #0ff; font-size: 30px;");
		this.postRender();
		this.updateCSS();

		return this;
	},
	pluginFunction: function(fName, args) {
		if (this.pluginFunctions && this.pluginFunctions[fName]) {
			var a = new Function(this.pluginFunctions[fName]);
			return a.call(this, args);
		} else {
			return false;
		}
	},
	updateCSS: function () {
		var css = '';
		this.model.get('cssQueries').each(function (el) {
			this.log("GETTING BLOCK CSS QUERIES FOR VIEW " + this.cid, "font-size: 30px; color: #aaf");
			css += el.getCSSwithId(this.model.cid);
			// this.log( el.getCSSwithId(this.model.cid) );
		}.bind(this));

		goldfish.appView.toggleExtraCSS('block-' + this.model.cid, css);
	},
	_dropZone: function (unique, group, idx) {
		return $('<p class="cat-dropzone' + (unique ? ' unique' : '') + '" id="drop-' + this.cid + '-' + group + '-' + idx + '"></p>');
	},
	postRender: function () {
		var me = this;
		// this.log("%cpostRender block "+this.cid,"font-size: 20px");
		// this.log(this.model.get('body'));
		// this.log( this.model.droppables() );

		// this.model.showCSS();

		var context = this;

		this.$('.ui.checkbox.toggle').checkbox();
		console.log("%c(postRender) Droppables:  "+this.model.droppables());
		if (this.model.droppables() > 0) {
			// this.log("%c"+this.model.get('name')+" has Droppables","font-size: 20px");
			var i = 0;
			this.$el.find('.cat-dropzone').each(function (idx, el) {
				if (context.cid === $(el).closest('.block').attr('data-cid')) {
					// me.log("%cAdding dropzone for "+context.cid,"font-size: 20px");
					context._addDroppable(i, el, context);
					i++;
				}

			});
		}
		this._addDraggable(this);


		// this.$el.find('[contenteditable="true"]')
		//        .on('input', function() { 
		//        	this.log(this);
		//        	this.log("INPUT");
		//           this.updateModel();
		//        }.bind(this));


	},
	_addDroppable: function (index, el, context) {
		if ($(el).data('droppable')) {
			$(el).droppable("destroy");
		}
		$(el).droppable({
			hoverClass: 'ui-state-highlight',
			activate: function (event, ui) {},
			over: function (event, ui) {},
			drop: function (event, ui, index) {
				var idx = context._findIndex.apply(this, [context]);
				context._dropCallback(
					context,
					event,
					ui,
					idx[0],
					idx[1],
					idx[2]
				);
			}
		});
	},
	_dropCallback: function (context, event, ui, group, position, index) {
		this.log("%cDropcallback", "font-size: 20px; color: blue");
		this.log(arguments);
		if ($(ui.draggable[0]).hasClass('block')) { // Rearrange
			var dragged = $(ui.draggable[0]);

			var elView = goldfish.unitView.currentPageView.getUserBlockViewByCid(dragged.data('originEl'));
			var parentView = goldfish.unitView.currentPageView.getUserBlockViewByCid(dragged.data('originParent'));

			this.log(context);
			this.log(parentView);
			this.log(elView);
			this.log("index destino: " + index);

			if (parentView.cid === context.cid) { // Rearrange in same parent
				var startIndex = context.model.attributes.blocks.indexOf(elView.model) * 1;
				this.log("index inicial:" + startIndex);
				if (startIndex < index) {
					this.log("moving down");
					index--;
					this.log("index destino: " + index);
				}
				context.model.moveBlock(elView.model, group, position, index);
			} else {
				var el = parentView.model.removeBlock(elView.model);
				context.model.addBlock(el, group, position, index);
			}


			// el.view = new goldfish.UserBlockView({model: el.model});



			dragged.remove();
			context.render();
		} else { // Add dragged from secondary menu
			this.log("%cAdding to " + context.cid + " at " + group + "," + position + "," + index, "font-size: 30px; background: white; color: red;");

			var bluePrint = goldfish.app.getUnit().getBlocks().get($(ui.draggable).find('> div').data('modelid'));
			this.log(bluePrint);
			// this.log(bluePrint.copy());
			this.log(bluePrint.toJSON());
			// this.log(bluePrint.parse( bluePrint.attributes ));

			var userBlock = new goldfish.UserBlock(bluePrint.toJSON());
			// userBlock.set( userBlock.parse( bluePrint.attributes ) );
			this.log("%cFinished copy", "font-size: 30px; background: white; color: red;");
			this.log(userBlock);
			// if (userBlock.get("data")) {
			// 	userBlock.set("data", JSON.parse(userBlock.get("data")));
			// }
			this.log("data set");
			this.log(userBlock.toJSON());
			userBlock.set({
				position: position,
				group: group
			}, {
				silent: true
			});
			this.log("position, group set");
			context.model.addBlock(userBlock, group, position, index);
			this.log("block added");
		}
	},
	_addDraggable: function (context) {
		if (context.$el.data('draggable')) {
			context.$el.draggable("destroy");
		}

		context.$el.draggable({
			// opacity: 0.7, 
			// helper: "clone",
			revert: 'invalid',
			//  grid: [40,40], // Número sin significado especial para mejorar rendimiento, se puede ajustar

			// containment: '#unit section.page',
			cursor: 'move',
			handle: context.$el.find('.tool-drag'),
			start: function (event, ui) {
				context.log("dragstart");
				goldfish.app.setDragging(true);
				$(this).prev('.cat-dropzone').addClass('adjacent');
				$(this).next('.cat-dropzone').addClass('adjacent');
				$(this).data('originEl', context.cid);
				$(this).data('originParent', $(this).parent().closest('.block').attr('data-cid'));
			},
			stop: function (event, ui) {
				context.log("dragstop");
				goldfish.app.setDragging(false);
				$(this).prev('.cat-dropzone').removeClass('adjacent');
				$(this).next('.cat-dropzone').removeClass('adjacent');
			}
		});
		// .click(function() { $(this).draggable({ disabled: false }); })
		// .dblclick(function() { $(this).draggable({ disabled: true }); }); 
	},
	events: function () {
		var _events = {
			'input [gf-editable]': function(e) {
				e.stopPropagation();
				console.log($(e.currentTarget).html());
				this.updateModel();
			},
			'mouseover': function (e) {
				// if (!goldfish.app.isDragging() && this.model.droppables() < 1) {
				if (!goldfish.app.isDragging()) {
					e.stopPropagation();
					this.$el.addClass('on');
				}
			},
			'mouseout': function (e) {
				// if (!goldfish.app.isDragging() && this.model.droppables() < 1) {
				if (!goldfish.app.isDragging()) {
					e.stopPropagation();
					this.$el.removeClass('on');
				}
			},
			'click .repeat-tool .repeat-add': function (e) {
				e.stopPropagation();

				var el = this.$('[data-repeater]');
				el.removeAttr('data-repeater');
				var repeaters = this.$(this.model.get('repeatable'));

				// var body = $("<div>"+this.model.get("body")+"</div>");

				// var bodyRepeaters = body.find(this.model.get('repeatable'));
				// var bodyActive = bodyRepeaters.eq( repeaters.index(el) );
				// bodyActive.after( bodyActive[0].outerHTML );

				// this.model.set({
				// 		body: goldfish.Helpers.replaceAll(body.html(), '<blocks></blocks>', '<blocks />')
				// 	}, {
				// 		silent: true
				// 	});
				console.log(this.model.get("blocks"));
				this.pluginFunction('addAt', repeaters.index(el)+1, el);
				console.log(this.model.get("blocks"));

				this.render();
				this.postRender();

			},
			'click .repeat-tool .repeat-remove': function (e) {
				e.stopPropagation();

				var el = this.$('[data-repeater]');
				el.removeAttr('data-repeater');
				var repeaters = this.$(this.model.get('repeatable'));

				// var selector = this.model.get("repeatable").substr(this.model.get("repeatable").lastIndexOf(">") +1 );
				// var el = $(e.currentTarget).closest(selector, this.$el);

				
				this.pluginFunction('removeAt', repeaters.index(el), el);
				this.repeatTool.detach();
				// el.remove();
				
				this.render();
				this.postRender();
			},
			// Funcionalidades particulares para componentes específicos, mientras tanto
			// Debería migrarse a un sistema de plugins, lo mas sencillo tal vez que los componentes definan un arreglo de event handlers en data.events,
			// Y/o subvistas por componente.
			'click .red': function (e) {
				e.stopPropagation();
				goldfish.appView.screenView.setBlock(true);
				goldfish.appView.screenView.openConfig(this.model.get('data').plugin_name, this);
			},
			// 'change .options .option .checkbox [type="checkbox"]': function(e) {
			// 	e.stopPropagation();
			// 	console.log("Checkbox change");
			// 	var useCustom = $(e.target).prop('checked');
			// 	console.log($(e.target).prop('checked'));
			// 	// this.model.set("isCustom", useCustom);
			// } 
		};
		if (this.model.get("eventHandlers")) {
			var specificHandlers = this.model.get("eventHandlers");
			for (var i = 0; i < specificHandlers.length; specificHandlers++) {
				_events[specificHandlers[i].event] = new Function(specificHandlers[i].body);
			}
		}
		if (this.model.get('repeatable') !== '') {
			_events['mouseenter ' + this.model.get('repeatable')] = function (e) {
				e.stopPropagation();

				this.$('.repeat-tool').detach();
				var el = $(e.currentTarget);
				el.attr('data-repeater', 'fnord');

				if (el.find('.repeat-tool').length < 1) {
					var repeatTool = $(this.repeatTool);
					var pos = el.position(); // Falta calcular para el caso de que el EL mismo tenga position absolute o relative
					this.repeatTool.css({
						top: '0px',
						left: '-3vw',
					});
					el.append(this.repeatTool);
					// console.log(el.outerHeight(true));
				}


			};
			_events['mouseleave ' + this.model.get('repeatable')] = function (e) {
				// e.stopPropagation();
				var el = $(e.target);
				this.repeatTool.detach();
				el.find('.repeat-tool').remove();
				this.$('[data-repeater]').removeAttr('data-repeater');
			};
		}

		return _events;
	},
	_isAttached: function () {
		return (this.$el.parent().length > 0);
	},
	preview: function () {
		var tmp = $('<div class="block"></div>');

		tmp.attr('id', 'block-' + this.model.cid);
		tmp.attr('data-cid', this.model.cid);
		tmp.append(this.$el.html());
		tmp.wrap('<div></div>');
		// this.log(tmp);
		// this.log(tmp.html());
		return tmp.parent().html();
	},
	attach: function () {
		// this.render();
		return this.el;
	},
	detach: function () {
		// this.viewState = 'view';
		var blockViews = this.model.get('blockViews');
		this.log(blockViews);
		for (var blockView in blockViews) {
			if (blockViews.hasOwnProperty(blockView)) {
				this.log(blockViews[blockView]);
				blockViews[blockView].detach();
			}
		}
		// this.model.get('blockViews').each( function(el) {
		// 	el.detach();
		// });
		this.remove();
	},
	updateModel: function () {
		var el = $('<div>'+this.$el.html()+'</div>');

		el.find('.cat-ui').remove();
		el.find('.cat-dropzone').remove();
		el.find('.repeat-tool').remove();

		this.model.set('body', el.html(), {
			silent: true
		});
		return true;
	},
	_findIndex: function (context) {
		var level;
		if ($(this).hasClass('cat-dropzone')) {
			context.log("finding dropzone index");
			var tmp = $(this).attr('id').split('-');
			level = $(this).parents('.block').length;
			index = $(this).closest('.block').find('.cat-dropzone').filter(
				function () {
					return $(this).parents('.block').length === level;
				}).index($(this));
			return [tmp[2] * 1, tmp[3] * 1, index];
		} else {
			// TODO: group + position
			context.log("finding block index");
			level = $(this).parents('.block').length;

			return $(this).parent().closest('.block').find('.block').filter(
				function () {
					return $(this).parents('.block').length === level;
				}).index($(this));
		}
	},
	remove: function () {
		goldfish.appView.toggleExtraCSS((this.viewState === 'edit' ? 'block-' : 'previewblock-') + this.model.cid, '');
		this.page.unregisterUserBlockView(this.cid);
		// recurse
		Backbone.View.prototype.remove.call(this);
	}
});