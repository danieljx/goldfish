var goldfish = goldfish || {};

goldfish.BlockView = Backbone.View.extend({
	tagName: 'section',
	className: 'block',
	viewState: 'view',
	initialize: function() {
		this.model.on('attachView', this.attach);
	},
	render: function() {
		if (this.viewState == 'view') {
			this.$el.html(this.model.get('body')); 
		} else {
			this.$el.html('<textarea class="editor">'+this.model.get('body')+'</textarea>');
		}
	    return this;
	},
	attach: function() {
		this.render();
		return this.$el;
	},
	preview: function() {
		// return this.model.preview();
		return '<div data-modelId="'+this.model.id+'"><h3>'+this.model.attributes.title+'</h3></div>';
	},
	detach: function() {
		this.viewState = 'view';
	},
	edit: function() {
		// goldfish.log("edit");
		// goldfish.log(this.viewState);
		if (this.viewState !== 'edit') {
			// goldfish.log("in");
			this.viewState = 'edit';
			this.render();
			return true;
		} else {
			return false;
		}
	},
	view: function() {

		// goldfish.log("view");
		// goldfish.log(this.viewState);
		if (this.viewState !== 'view') {
			// goldfish.log("in");
			this.viewState = 'view';	
			this.render();
			return true;
		} else {
			return false;
		}
	},
	events: {
      'click #menu-toggle': function(e) {
         e.preventDefault();
         this.menuViewToggle();
      }
  }
});