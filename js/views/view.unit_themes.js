var goldfish = goldfish || {};

goldfish.UnitThemesView = Backbone.View.extend({
	template: _.template($('#tpl-unit-themes').html()),
	initialize: function() {
		// this.render();
	},
	render: function() {
		goldfish.log(this.model.toJSON());
		this.$el.html( this.template(this.model.toJSON()) );
		return this;
	},
	events: {
		'click li > a': function(e) {
         e.preventDefault();
         $(e.target).closest('#themes').find('li').removeClass('selected');
         $(e.target).closest('li').addClass('selected');
         this.model.set('theme', $(e.target).closest('a').attr('href').split('/')[1]);
      }
	}
});