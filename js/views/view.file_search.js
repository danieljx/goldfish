var goldfish = goldfish || {};

goldfish.FileSearchView = Backbone.View.extend({
    template: _.template($('#tpl-files-search').html()),
    tagName: 'div',
    className: 'file-search',
    events: {
        //'keypress input#search_text': 'keypressSearch',
        'click div.menuAction div.upFile': 'upFile',
        'click div.menuAction div.newFile': 'newFile',
        'click div.menuAction div.trashFile': 'trashFile'
    },
    initialize : function(opt) {
      this.sharedView = opt.sharedView;
      this.publicView = opt.publicView;
    },
    render : function() {
      var me = this;
      var data            = this.model.toJSON();
          data.sharedView = this.sharedView;
          data.publicView = this.publicView;
          data.shared     = data.shared.toJSON();
      this.$el.html(this.template(data));
      this.$('.menuAction.dropdown').dropdown({
        direction: 'menuAction'
      });
      this.FileNotificationView = new goldfish.FileNotificationView({
          model: this.model
      });
      this.$('.menuNotificationItem').html(this.FileNotificationView.render().$el);
      this.$('.menuNotification').popup({
        popup : this.$('.menuNotificationItem'),
        on    : 'click',
        position: 'bottom center'
      });
      this.$(".ui.search.search_text").search({
        type: "category",
        minCharacters: 3,
        //maxResults: 500,
        //source: this.model.get('children').toJSON(),
        apiSettings: {
          url: "/api/user/" + goldfish.app.getUser().get("id") + "/file/search/{query}",
        },
        onSelect: function (file, response) {
          me.goFile(file);
          return false;
        },
        settings: {
          templates : {
            escape: function (string) {
              console.log(string);
            },
            message: function (message, type) {
              console.log(message, type);
            },
            category: function (response) {
              console.log(response);
            },
            standard: function (response) {
              console.log(response);
            }
          }
        }
      });
      return this;
    },
    goFile: function(file) {
      if(file.mimetype == 2) {
        goldfish.router.navigate('files/' + (file.sharedView ? 'shared/' : '') + (file.publicView ? 'public/' : '') + file.path_hash, { trigger: true, replace: true });
      } else {
        console.log(file);
      }
    },
    keypressSearch: function(e){
      var char = String.fromCharCode(e.which);
      if (e.keyCode == 13 || e.which == 13) {
        this.submit();
      } else if (!char.match(/[A-Za-z0-9+#.]/)) {
        return false;
      }
    },
    showMenu: function(e) {
      e.preventDefault();
      e.stopPropagation();
      return false;
    },
    upFile: function(e) {
      e.preventDefault();
      e.stopPropagation();
      $(".filesUp > form.form-file > input#files[type=file]").trigger("click");
      return false;
    },
    newFile: function(e) {
      e.preventDefault();
      e.stopPropagation();
      goldfish.appView.screenView.setBlock(true);
      this.FolderModalView = new goldfish.FileFolderModalView({
          model: this.model
      });
      goldfish.appView.screenView.$el.prepend( this.FolderModalView.render().$el );
      this.FolderModalView.delegateEvents();
      this.FolderModalView.focus();
      return false;
    },
    trashFile: function(e) {
      e.preventDefault();
      e.stopPropagation();
      console.log("trashFile");
      return false;
    }
});
