var goldfish = goldfish || {};

goldfish.FileGalleryListViewSelect = Backbone.View.extend({
    template: _.template($('#tpl-files-modal-select-gallery-list').html()),
    tagName: 'div',
    className: 'ui four cards',
    events: {
        //'click tr': 'goFileClick'
    },
    selectedFile: function(file){},
    initialize : function(options) {
        this.close          = options.close || this.close;
        this.selectedFile = options.selectedFile || this.selectedFile;
        this.listenTo(this.collection, 'change', this.renderItemChange);
        this.listenTo(this.collection, 'add', this.renderItem);
        this.listenTo(this.collection, 'reset', this.renderItemReset);
    },
    render : function() {
        var me = this;
        this.$el.html(this.template(this.collection.toJSON()));
        this.$loadGallery = this.$(".loadGallery");
        this.collection.fetch({
            url: '/api/user/' + goldfish.app.getUser().get("id") + '/file/gallery/',
            beforeSend: function () {
                me.$loadGallery.addClass("active");
            },
            success: function (model, response, options) {
                me.$loadGallery.removeClass("active");
            },
            error: function (model, response, options) {
                console.log("%cSave error!", 'color: red;');
                toastr.error(response.responseJSON.msg);
                me.$loadGallery.removeClass("active");
            }
        });
        return this;
    },
    renderItem: function (model) {
        var view = new goldfish.FileGalleryListViewSelectItem({
            model: model,
            selectedFile: this.selectedFile,
            close: this.close
        });
        this.$el.prepend(view.render().el);
    }
});