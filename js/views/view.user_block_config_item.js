var goldfish = goldfish || {};

goldfish.UserBlockConfigItemView = Backbone.View.extend({
	log: true ? console.log.bind(window.console) : function() { },
	tagName: 'aside',
	className: 'config-edit',
	template: _.template($('#tpl-data-config').html()),
	// viewBody: '',

	initialize: function() {
		this.itemViews = {};
	},
	render: function() {
        var data = this.model.get("data");
		this.$el.html( this.template( data ) );
	    return this;
	},
	postRender: function() {
		// this.log(this.$('.ui.checkbox.toggle').length);
		this.$('.ui.checkbox.toggle').checkbox();
		this.$('.ui.dropdown').dropdown();
		// this.delegateEvents();
	},
	_isAttached: function() {
		return (this.$el.parent().length > 0);
	},
	attach: function() {
		return this.el;
	},
	detach: function() {
		this.viewState = 'view';
	},
	updateModel: function(aTarget) {
		var aCid = aTarget.attr('data-cid');
		var rule = this.model.getCSSRule(aCid);
		if (aTarget[0].tagName.toLowerCase() === 'input') { 
			goldfish.log(aTarget.val());
			rule.setAttribute(aTarget.prevAll('span').html(), aTarget.val());
		} else {
			rule.setAttribute(aTarget.prevAll('span').html(), aTarget.html());
		}
		
		
		
		// this.model.set('body',this.$el.find('.block').first().html(),{silent: true});
		return true;
	}
});