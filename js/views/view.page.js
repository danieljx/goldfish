var goldfish = goldfish || {};

/** The application view
* @author Mats Fjellner
* @augments Backbone.View
* @constructor
* @name PageView
*/
goldfish.PageView = Backbone.View.extend(/** @lends PageView */{
	log: true ? console.log.bind(window.console) : function() { },
	tagName: 'div',
	className: 'page',
	initialize: function(options) {
		this.userBlockViews = [];
		this.model.on('attachView', this.attach);
		// this.model.on('change', function(e) {
		// 	goldfish.log("model changed!");
		// 	this.render();
		// }, this);
		this.listenTo(goldfish.app, "dragchange", function() {
			this.$el.toggleClass("dragging",goldfish.app.isDragging());
		});	
		this.listenTo(this.model, "blockRegister", this.registerUserBlockView);
		this.listenTo(this.model, "blockUnregister", this.unregisterUserBlockView);
		this.blockView = new goldfish.UserBlockView({model: this.model.get('block')});
	},
	events: {
		'input > h2': function(e) {
			e.preventDefault();
			this.model.set({title: $(e.target).html()});
		}
	},
	render: function() {
		this.log("%crender page view",'color: green');
		var startTime = window.performance.now();
		var me = this;

		this.$el.html('<h2 contenteditable="true">'+this.model.get('title')+'</h2>');

		this.$el.append(this.blockView.render().attach());
		// var replaceHTML = [' contenteditable="true"','ui-draggable','ui-droppable','cat-dropzone'];
		// var renderedHTML = this.$el.html();

		// for (var i = 0; i < replaceHTML.length; i++) {
		// 	renderedHTML = renderedHTML.split(replaceHTML[i]).join('');
		// }

		// this.model.set('rendered_css', this.blockView.model.get('rendered_css'));
		// this.model.set('rendered_html', renderedHTML);

		if (this._isAttached()) {
			this.postRender(this);
		}
		var endTime = window.performance.now();
		goldfish.log("%cRender time: "+(endTime - startTime)+"ms","color: blue");
	    return this;
	},
	preview: function() {
		// var myBlocks = this.model.getBlocks();
		var me = this;
		var preview = '<div class="page">';

		if (this.model.attributes.blockView) {
			preview += this.model.attributes.blockView.preview();
		}
		return preview;
	},
	_isAttached: function() {
		return (this.$el.parent().length > 0);
	},
	postRender: function(context) {
		this.delegateEvents();
	},
	// _findBlockView: function(aCid) {
	// 	return this.getBlockViewByCid(aCid);
	// },     
	// getBlockViewByCid: function(aCid) {
	// 	return this.attributes.blockViews[aCid];
	// },
	/** Registers user component view
	* @param {integer} id - View id
	* @param {Object} View - Component View
   */
	registerUserBlockView: function(aCid, aView) {
		this.userBlockViews[aCid] = aView;
	},
	/** Unregisters user component view
	* @param {integer} id - View id
   */
	unregisterUserBlockView: function(aCid) {
		delete this.userBlockViews[aCid];
	},
	/** Gets user component view 
	* @param {integer} id - View id
	* @return {Object} View - Component View
   */
	getUserBlockViewByCid: function(aCid) {
		goldfish.log(this.userBlockViews);
		return this.userBlockViews[aCid];
	},
	attach: function() {
		return this.$el;
	},
	detach: function() {
		goldfish.log("%cDetaching view "+this.cid+" with el","color: red; font-size: 20px");
		goldfish.log(this.$el);

		this.blockView.detach();
		this.remove();
	},
	config: function(state) {
		console.log("page config");
		goldfish.appView.secondaryMenuViewToggle();		
   },
 //   remove: function() {
	// 		this.$el.empty().off(); 
	// 		this.stopListening();
	// }
});