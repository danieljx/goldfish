var goldfish = goldfish || {};

goldfish.UserFilesView = Backbone.View.extend({
    template: _.template($('#tpl-user-files').html()),
    tagName: "div",
    sharedView: false,
    publicView: false,
    attributes: {
        draggable: true
    },
    initialize: function(opt) {
        this.sharedView = opt.sharedView;
        this.publicView = opt.publicView;
    },
    events: {
        'click #tableFixed > thead > tr > th.checkAll > div > div.md-checkbox > label': 'selectFileAll',
        "dragenter": "dragEnterFile"
    },
    render: function() {
        var me = this;
        if(this.collection.models.length > 0) {
            this.model = this.collection.models[0];
        }
        this.$el.html(this.template(this.model.toJSON()));
        this.fileView       = new goldfish.FileView({
            model: this.model,
            sharedView: this.sharedView,
            publicView: this.publicView
        });
        this.searchView     = new goldfish.FileSearchView({
            model: this.model,
            collection: this.collection,
            sharedView: this.sharedView,
            publicView: this.publicView
        });
        this.breadcrumbView = new goldfish.FileBreadcrumbView({
            model: this.model,
            sharedView: this.sharedView,
            publicView: this.publicView
        });
        this.fileUpView     = new goldfish.FileUpView({
            model: this.model,
            sharedView: this.sharedView,
            publicView: this.publicView
        });
        this.fileMenuRightView = new goldfish.FileMenuRightView({
            model: this.model,
            sharedView: this.sharedView,
            publicView: this.publicView
        });

        this.$('.searchs').html(this.searchView.render().el);
        this.$('.breadcrumbs').html(this.breadcrumbView.render().el);
        this.$('.files').html(this.fileView.render().el);
        this.$('.filesUp').html(this.fileUpView.render().el);
        this.$el.append(this.fileMenuRightView.render().el);

        return this;
    },
    dragEnterFile: function(e, data, clone, element) {
        e.preventDefault();
        e.stopPropagation();
        this.fileUpView.$('#holder').addClass('dragStart');
        return false;
    },
    selectFileAll: function(e) {
        e.preventDefault();
        e.stopPropagation();
        this.$checkTool = this.$('#table > thead > tr > th.checkAll > div > div.md-checkbox > input[name=checkToolAll]');
        this.$checkTool.trigger('click');
        return false;
    },
    toggleVisibility: function(visible) {
        if (visible) {
            this.$el.css('display','block');
        } else {
            this.$el.css('display','none');
        }
    }
});
