var goldfish = goldfish || {};

goldfish.FileModalShareListView = Backbone.View.extend({
    template: _.template($('#tpl-modal-share-list-files').html()),
    tagName: 'div',
    className: 'user-shared',
    events: { },
    initialize: function(opt) {
        this.shared_user = opt.shared_user;
        this.listenTo(this.shared_user, 'add', this.addShare);
    },
    render: function() {
        var me = this;
        this.$el.html(this.template(this.model.toJSON()));
        this.$ulShared = this.$('ul.ul-shared');
        this.shared_user.forEach(function(dataChild) {
            me.renderItemShared(dataChild);
        });
        return this;
    },
    addShare: function(data) {
        console.log("addShare");
        console.log(data);
        this.renderItemShared(data);
        return false;
    },
    renderItemShared: function(share) {
        this.fileModalShareListItemView = new goldfish.FileModalShareListItemView({
            model: share,
            file: this.model
        });
        this.$ulShared.append(this.fileModalShareListItemView.render().el);
    }
});
