var goldfish = goldfish || {};
/** The application view
* @author Mats Fjellner
* @augments Backbone.View
* @constructor
* @name AppView
*/
goldfish._AppView = Backbone.View.extend(/** @lends AppView */
{
   el: 'body',
   /** On init, creates subviews dashBoardView, SecondayMenuView. Also creates screen.
   */
   initialize: function() {
      me = this;
      this.editorInstance = false;

    console.log(this.model);

      this.screenView = new goldfish.ScreenView({model: this.model, el: this.$('.screen') });
      this.dashBoardView = new goldfish.DashBoardView({model: this.model, el: $('#dashboard') });

      if (this.model.get("showDebug")) {
        this.debugView = new goldfish.DebugView({model: this.model, el: $('#debug-info') });
      }
      // this.secondaryMenuView = new goldfish._AppSecondaryMenuView({ model: this.model, el: $('#secondary-menu') });

      this.listenTo(this.model, "switch", function() {
        goldfish.log("switching app view...");
        this.setBusy();
        if (this.model.get('activeSectionPrev')) {
          this.getSectionView(this.model.get('activeSectionPrev')).css('display','none');
        }
        this.getSectionView(this.model.get('activeSection')).css('display',this.model.get('activeSection') == 'unit' ? 'flex' : 'block');
        this.setReady();
        this.editingBlock = false;
      });
      this.dashBoardToggle();
	},
  /** Returns section view corresponding to alias
 * @param {string} aStr - Section alias
 * @return {Object|boolean} The view, or false if not found.
   */
  getSectionView: function(aStr) {
    switch (aStr) {
      case 'unit':
        return $('#unit');
      case 'config':
        return $('#config');
      case 'projects':
        return $('#projects');
      case 'files':
        return $('#files');
      case 'tags':
        return $('#tags');
      case 'project':
        return $('#project');
      case 'notifications':
        return $('#notifications');
    }
    return false;
  },
  /** Toggles effects
 * @param {boolean} state - Explicitly sets state
   */
  setEffects: function(state) {
    this.toggleExtraCSS('effects', state ? '' : 'body * { transition: none !important; }');
  },
  /** Sets app ready for interaction.
   */
	setReady: function() { // Mejor cambiar por transition + .one()
    this.screenView.setReady();
	},
  /** Sets app busy (blocks interaction).
   */
	setBusy: function() { // Mejor cambiar por transition + .one()
    this.screenView.setBusy();
	},
  /** Shows message to User
  * @param {string} msg - Message
   */
	showMsg: function(msg) {
    this.screenView.showMsg(msg);
	},
/** Map of event handlers
    * @type Object
   */
	events: {
    /** Listens for clicks on .dashboard-toggle, toggles dashboard.
     * @lends AppView
   */
      'click > aside > .dashboard-toggle': function(e)/** @lends AppView */ {
         e.preventDefault();
         this.dashBoardToggle();
      },
     /** Listens for clicks on .editor-toggle, toggles editor for components.
     * @lends AppView
   */
      'click .editor-toggle': function(e) {
         if (this.editorInstance && this.editingBlock) {
            this.editingBlock.set('body', this.editorInstance.getData());
            this.editorInstance.destroy();
            this.editorInstance = false;
            this.setReady();
         }
      },
      'keydown' : 'keydownHandler'
  },
  /** Listens for keyboard clicks, ESC closes preview OR closes editor, dashboard and secondary menu
  * @lends AppView
   */
  keydownHandler : function (e) {

    switch (e.which) {
      // esc
      case 27 :
        if (this.model.attributes.showPreview) {
          goldfish.log("preview on");
          this.showPreview(false);
        } else {
          if (this.editorInstance && this.editingBlock) {
            this.editorInstance.destroy();
            this.editorInstance = false;
            this.setReady();
         } else {
          if (this.model.screen) {
            this.screenView.setBlock(false);
          } else {
            if (this.model.attributes.showMenu) {
              this.dashBoardToggle();
            }
            // this.secondaryMenuView.toggle(false);
          }

         }

        }

      // do things...
      break;
    }
    if (!e.target.isContentEditable && !$(e.target).is('input')) {
      switch (e.which) {
        case 65:
          this.dashBoardToggle();
          break;
        case 68:
          // this.secondaryMenuView.toggle();
          break;
        case 87:
          if (this.model.get("activeUnit")) {
            goldfish.unitView.infoToggle();
          }
          break;
      }
    }
  },
  /** Sets theme
  * @param {string} aTheme
   */
  setTheme: function(aTheme) {
    this.setBusy();

    // var themeHref = $('#css-theme').attr('href');
    // themeHref = themeHref.split('/');
    // themeHref[themeHref.length - 1] = aTheme+".css";
    // themeHref = themeHref.join('/');
    var themeHref = 'dist/css/'+aTheme+'.css';
    $.ajax({
      url: themeHref,
      // dataType: "script",
      success: function(css) {
        console.log("got CSS");
        $('#css-theme').text(css);
        console.log("set CSS");
        $('#css-void').remove();
        console.log("removed void CSS");
        this.setReady();
      }.bind(this)
    });
    
    // $('#css-theme').attr('href', themeHref);
    // this.setReady();
  },
  /** Toggles unit preview
  * @param {boolean} toggle - On/Off
   */
  showPreview: function(toggle) {
    // this.model.attributes.showPreview = html && (html !== '');
    this.model.attributes.showPreview = toggle;

    if (this.model.attributes.showPreview) {
      // this.toggleExtraCSS('preview-css',css);
      // $('#preview').html( html );
      // $('#preview').toggleClass('on');
    } else {
      // $('#preview').html( );
      // $('#preview').toggleClass('on');
      // this.toggleExtraCSS('preview-css',false);
      this.unitView.previewPage(false);
    }


  },
  /** Toggles dashboard
  * @param {boolean} state - On/Off
   */
  dashBoardToggle: function(state) {
      this.dashBoardView.toggle(state);
   },
   /** Toggle secondary menu
  * @param {boolean=} state - On/Off
   */
  //  secondaryMenuViewToggle: function(state) {
  //    console.log("SECONDARY TOGGLE");
  //     this.model.attributes.showSecondaryMenu = (state !== undefined) ? state : !this.model.attributes.showSecondaryMenu;
  //     if (this.model.attributes.showSecondaryMenu) {
  //       // this.dashBoardToggle(false);
  //       this.secondaryMenuView.loadComponents(goldfish.unitView.getComponentViews());
  //       goldfish.unitView.shrink();
  //       this.secondaryMenuView.toggle(true);
  //     } else {
  //       goldfish.unitView.expand();
  //       this.secondaryMenuView.toggle(false);
  //     }
  //  },
   /** Adds UnitView
  * @param {Object} aUnitView
   */
   setUnitView: function(aUnitView) {
      this.unitView = aUnitView;
      this.model.setUnit(aUnitView.model);
      this.setReady();
   },
   /** Toggles custom CSS, on if cssStr != ''
  * @param {string} cssId - DOM ID
  * @param {string=} cssStr - CSS String
   */
   toggleExtraCSS: function(cssId, cssStr) {
    if ($('head #'+cssId).length === 0) {
      $('head').append('<style id="'+cssId+'">'+cssStr+'</style>');
    } else {
      if (cssStr) {
        $('head #'+cssId).html(cssStr);
      } else {
        $('head #'+cssId).remove();
      }

    }
   },
   /** Loads component CSS in secondary menu and shows the menu
  * @param {Object} aBlock - Component
   */
  //  secondaryMenuLoadCSS: function(aBlock) {
  //   this.secondaryMenuView.loadCSS(aBlock);
  //   this.secondaryMenuViewToggle(true);
  //  },
   /** Loads component HTML editor in secondary menu and hides the menu
  * @param {Object} aBlock - Component
   */
   loadEditor: function(aBlock) {
      this.editingBlock = aBlock;
      goldfish.unitView.expand();
      this.$('#secondary-menu').removeClass("on");
      this.screenView.editorView();
      this.editorInstance = CKEDITOR.appendTo( $('#content-holder')[0], {}, aBlock.get('body') );
   }
});
