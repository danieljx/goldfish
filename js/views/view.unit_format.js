var goldfish = goldfish || {};

goldfish.UnitFormatView = Backbone.View.extend({
	template: _.template($('#tpl-unit-format').html()),
	initialize: function() {
		// this.render();
	},
	render: function() {
		goldfish.log(this.model.toJSON());
		this.$el.html( this.template(this.model.toJSON()) );
		return this;
	},
	events: {
	}
});