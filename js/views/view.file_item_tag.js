var goldfish = goldfish || {};

goldfish.FileItemTagView = Backbone.View.extend({
    template: _.template($('#tpl-files-item-tag').html()),
    tagName: 'a',
    className: 'ui tag label',
    events: {
      'click i.delete.icon': 'delTagx'
    },
    initialize: function(opt) {
        this.listenTo(this.model, 'remove', this.delTag);
        this.sharedView = opt.sharedView;
        this.publicView = opt.publicView;
        this.file       = opt.file;
    },
    render: function() {
        var data            = this.model.toJSON();
            data.sharedView = this.sharedView;
            data.publicView = this.publicView;
            data.shared     = this.file.get('shared').toJSON();
        this.$el.html(this.template(data));
        this.$el.attr({
            id: 'tag_' + this.model.get("id")
        }).addClass(this.model.get("tag_action"));
        return this;
    },
    delTag: function() {
        console.log("delTag");
        this.undelegateEvents();
        this.$el.removeData().unbind();
        this.remove();
        Backbone.View.prototype.remove.call(this);
        return false;
    },
    delTagx : function(e) {
        e.preventDefault();
        e.stopPropagation();
        if(this.model.get('tag_action') == 'up') {
          this.delTag();
        }
        this.model.set('tag_action', 'del');
        return false;
    }
});
