var goldfish = goldfish || {};

goldfish.FileMenuRightView = Backbone.View.extend({
    template: _.template($('#tpl-file-menu-right').html()),
    tagName: 'aside',
    className: 'secondary-menu files',
    actionModel: [
        {
            name:'Nueva Carpeta',
            icon:'new-folder',
            viewBox:'-0 -0 65 65',
            class: 'newFolder on'
        },
        {
            name:'Archivos Eliminados',
            icon:'trash',
            viewBox:'0 0 600 600',
            class: 'trash on'
        },
        {
            name:'Descargar',
            icon:'download',
            viewBox:'-20 -20 500 500',
            class: 'download one'
        },
        {
            name:'Cambiar Nombre',
            icon:'cursor',
            viewBox:'10 0 230 230',
            class: 'cursor one'
        },
        {
            name:'Mover',
            icon:'move',
            viewBox:'0 0 60 60',
            class: 'move one multiple'
        },
        {
            name:'Copiar',
            icon:'copy',
            viewBox:'0 0 600 600',
            class: 'copy one multiple'
        },
        {
            name:'Eliminar',
            icon:'trash',
            viewBox:'0 0 600 600',
            class: 'delete one multiple'
        }
    ],
    display: false,
    countFile : 0,
    currentFile : 0,
    collection: [],
    events: {
        'click .secondary-menu-files-toggle': 'toggle',
        'click .menu-secondary-sidebar > .content-button-action-menu > button.upFile': 'upFile',
        'click .menu-secondary-sidebar > .content-button-action-menu > button.downFile': 'downFile',
        'click .menu-secondary-sidebar > .content-button-action-menu > button.shareFile': 'shareFile',
        'click .menu-secondary-sidebar > ul > li.newFolder > a': 'newFolder',
        'click .menu-secondary-sidebar > ul > li.trash > a': 'trashFile',
        'click .menu-secondary-sidebar > ul > li.download > a': 'downFile',
        'click .menu-secondary-sidebar > ul > li.cursor > a': 'changeName',
        'click .menu-secondary-sidebar > ul > li.move > a': 'moveFile',
        'click .menu-secondary-sidebar > ul > li.copy > a': 'copyFile',
        'click .menu-secondary-sidebar > ul > li.delete > a': 'dropFiles'
    },
    initialize: function(opt) {
        this.sharedView = opt.sharedView;
        this.publicView = opt.publicView;
        var me = this;
        var collectionTemp = this.model.get("children");
            collectionTemp.forEach(function(dataChild) {
                me.listenTo(dataChild,"change:checked",me.getChecked);
            });
        this.$el.addClass("files");
        this.render();
    },
    render: function() {
      var me    = this;
          this.data  = this.model.toJSON();
          this.data.sharedView   = this.sharedView;
          this.data.publicView   = this.publicView;
          this.data.shared       = this.data.shared.toJSON();
          this.data.modelAction  = this.actionModel.filter(function(dataChild) {
              return (!me.publicView && !me.sharedView) || (dataChild.icon != 'new-folder' && dataChild.icon != 'cursor') || ((dataChild.icon == 'new-folder' || dataChild.icon == 'cursor') && me.data.shared.share_edit == 1);
          });
          this.data.modelAction = this.data.modelAction.filter(function(dataChild) {
              return (!me.publicView && !me.sharedView) || (dataChild.icon != 'move' && dataChild.icon != 'copy' && dataChild.icon != 'trash' && dataChild.icon != 'download') || ((dataChild.icon == 'move' || dataChild.icon == 'copy' || dataChild.icon == 'trash' || dataChild.icon == 'download') && (!me.publicView && !me.sharedView));
          });
        this.$el.html(this.template(this.data));
        return this;
    },
    toggle: function(e) {
        e.preventDefault();
        this.$el.toggleClass('on');
        $('.upload-progress').toggleClass('on');
        this.display = !this.display;
    },
    getChecked: function(model) {
        if(model.get("checked")) {
            this.countFile++;
            this.collection.push(model);
        } else {
            this.countFile--;
            this.collection.pop(model);
        }
        this.chengeCount();
    },
    chengeCount: function() {
        this.$("button.button-action-menu").removeClass("upFile").removeClass("downFile").removeClass("shareFile");
        if(this.countFile == 1 && !this.sharedView) {
            this.$el.removeClass("multiple").addClass("one");
            this.$("button.button-action-menu > span").html("Compartir");
            this.$("button.button-action-menu").addClass("shareFile").show();
        } else if((this.countFile > 1 && !this.sharedView) || (this.countFile > 0 && this.sharedView)) {
            if(this.countFile == 1 && this.data.shared.share_edit == 1) {
                this.$el.removeClass("multiple").addClass("one");
            } else {
              this.$el.removeClass("one").addClass("multiple");
            }
            this.$("button.button-action-menu > span").html("Descargar");
            this.$("button.button-action-menu").addClass("downFile").show();
        } else if((this.countFile == 0 && this.data.shared.share_edit == 1) || !this.sharedView) {
            this.$el.removeClass("one").removeClass("multiple");
            this.$("button.button-action-menu > span").html("Cargar Archivos");
            this.$("button.button-action-menu").addClass("upFile").show();
        } else {
            this.$("button.button-action-menu").hide();
        }
        this.$("span.selection-header-text > strong").html(this.countFile);
    },
    upFile: function(e) {
        e.preventDefault();
        e.stopPropagation();
        $(".filesUp > form.form-file > input#files[type=file]").trigger("click");
        console.log("upFile");
        return false;
    },
    shareFile: function(e) {
        e.preventDefault();
        e.stopPropagation();
        $a = $(e.currentTarget);
        goldfish.appView.screenView.setBlock(true);
        this.FileModalShare = new goldfish.FileModalShareView({
            model: this.model
        });
        goldfish.appView.screenView.$el.prepend( this.FileModalShare.render().$el );
        this.FileModalShare.delegateEvents();
        return false;
    },
    newFolder: function(e) {
        e.preventDefault();
        e.stopPropagation();
        $a = $(e.currentTarget);
        goldfish.appView.screenView.setBlock(true);
        this.FolderModalView = new goldfish.FileFolderModalView({
            model: this.model
        });
        goldfish.appView.screenView.$el.prepend( this.FolderModalView.render().$el );
        this.FolderModalView.delegateEvents();
        this.FolderModalView.focus();
        return false;
    },
    trashFile: function(e) {
        e.preventDefault();
        e.stopPropagation();
        $a = $(e.currentTarget);
        console.log("trashFile");
        return false;
    },
    downFile : function(e) {
        e.preventDefault();
        e.stopPropagation();
        $a = $(e.currentTarget);
        var collectionTemp = this.model.get("children");
            //collectionTemp.downloadFiles();
        console.log("downloadFile");
        console.log(this.collectionTemp);
        return false;
    },
    changeName : function(e) {
        e.preventDefault();
        e.stopPropagation();
        $a = $(e.currentTarget);
        this.collection[this.currentFile].set("edit", true);
        //this.model.set("edit", true);
        return false;
    },
    moveFile : function(e) {
        e.preventDefault();
        e.stopPropagation();
        $a = $(e.currentTarget);
        console.log("moveFile");
        return false;
    },
    copyFile : function(e) {
        e.preventDefault();
        e.stopPropagation();
        $a = $(e.currentTarget);
        console.log("copyFile");
        return false;
    },
    dropFiles : function(e) {
        e.preventDefault();
        e.stopPropagation();
        console.log("dropFile");
        //console.log(this.collection);
        //this.dropFile(this.collection[this.currentFile]);
        return false;
    },
    dropFile : function(modelDrop) {
    var me = this;
        modelDrop.destroy({
            wait: true,
            data: {
                path_hash: modelDrop.get("path_hash")
            },
            processData: true,
            success: function(model, response, options) {
                console.log("%csuccess => " + modelDrop.get("name"), 'color: green');
                me.nextFile();
            },
            error: function(model, response, options) {
                goldfish.log("%cerror! => " + modelDrop.get("name"),'color: red;');
                me.nextFile();
            }
        });
    },
    nextFile: function() {
        this.currentFile++;
        if(this.currentFile < this.collection.length) {
            this.dropFile(this.collection[this.currentFile]);
        } else {
            console.log("%cFinish All", 'color: green');
            this.resetFiles();
        }
        this.chengeCount();
    },
    resetFiles: function() {
        this.countFile      = 0;
        this.currentFile    = 0;
        this.collection     = [];
    }
});
