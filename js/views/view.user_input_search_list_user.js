var goldfish = goldfish || {};

goldfish.UserInputSearchListUserView = Backbone.View.extend({
    template: _.template($('#tpl-user-input-search-list-user').html()),
    tagName: 'li',
    className: 'user-option',
    initialize: function(opt) {
        this.shared_user = opt.shared_user;
        this.file   = opt.file;
    },
    events: {
        'click': 'addUser'
    },
    render: function() {
        this.$el.html(this.template(this.model.toJSON()));
        return this;
    },
    addUser: function(e) {
        e.preventDefault();
        e.stopPropagation();
        var $a = $(e.currentTarget);
        this.shared_user.addUser(this.file, this.model);
        $('.search-users-list').hide();
        return false;
    }
});
