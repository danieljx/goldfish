var goldfish = goldfish || {};

goldfish.FileItemView = Backbone.View.extend({
    template: _.template($('#tpl-files-item').html()),
    tagName: 'tr',
    events: {
        'click': 'goFile',
        'click td.action div.tools': 'showMenu',
        'click td.action div.view': 'goFileEye',
        'click td.icon > div > div.md-checkbox > label': 'selectFile',
        'click td.icon > div > div.md-checkbox > input[name=checkTool]': 'checkTool',
        'input td.name > span.input-name > input': 'changeName',
        'focusin td.name > span.input-name > input': 'inInput',
        'focusout td.name > span.input-name > input': 'outInput',
        'keypress td.name > span.input-name > input': function(e) {
            var char = String.fromCharCode(e.which);
            if (e.keyCode == 13 || e.which == 13) {
                console.log(e.keyCode);
            } else if (!char.match(/[A-Za-z0-9+#.]/)) {
                return false;
            }
        }
    },
    checked: false,
    edit: false,
    path: "",
    name: "",
    initialize: function(opt) {
        this.sharedView = opt.sharedView;
        this.publicView = opt.publicView;
        this.path = this.model.get("path");
        this.name = this.model.get("name");
        this.listenTo(this.model,"remove",this.removeItem);
        this.listenTo(this.model,"change:checked",this.setChecked);
        this.listenTo(this.model,"change:edit",this.setEdit);
        this.listenTo(this.model,"change:name",this.setName);
        this.listenTo(this.model,"change:icon",this.setIcon);
    },
    render : function() {
        this.$el.attr({
            id: 'file_' + this.model.get("id"),
            path: this.model.get("path"),
            pathHash: this.model.get("path_hash")
        }).data({
            id:this.model.get("id"),
            type: this.model.get("mimetype")
        });
        this.$el.html(this.template(this.model.toJSON()));
        this.$checkTool = this.$('td.icon > div > div.md-checkbox > input[name=checkTool]');
        this.$action    = this.$('td.action > div.tools').attr({
                              id: 'menuNotification_' + this.model.get("id")
                          });
        this.menu       = new goldfish.FileItemMenuView({
            model: this.model,
            sharedView: this.sharedView,
            publicView: this.publicView
        });
        this.$action.append(this.menu.render().el);
        this.$action.dropdown({
          direction: 'tools'
        });
        this.$tdName = this.$("td.name");
        return this;
    },
    setChecked: function(data) {
        this.checked = data.get("checked");
        this.$checkTool.trigger('click').prop('checked', this.checked);
    },
    selectFile : function(e) {
        e.preventDefault();
        e.stopPropagation();
        this.model.set("checked", !this.checked);
        return false;
    },
    showMenu : function(e) {
        e.preventDefault();
        e.stopPropagation();
        return false;
    },
    checkTool : function(e) {
        e.preventDefault();
        e.stopPropagation();
        $('.popover').hide();
        if(this.$checkTool.prop('checked')) {
            $("td.action > div",this.$el).addClass('active');
            $("td.action > div.tools",this.$el).hide();
            $("td.action > div.view",this.$el).show();
        } else {
            $("td.action > div",this.$el).removeClass('active');
            $("td.action > div.view",this.$el).hide();
            $("td.action > div.tools",this.$el).show();
        }
        return false;
    },
    goFile : function(e) {
        e.preventDefault();
        e.stopPropagation();
        if(this.model.get("mimetype") == 2 && !this.model.get("edit") && !this.model.get("checked")) {
            goldfish.router.navigate('files/' + (this.sharedView ? 'shared/' : '') + (this.publicView ? 'public/' : '') + this.model.get("path_hash"), {trigger: true, replace: true});
        } else {
            console.log(this.model.toJSON());
        }
    },
    goFileEye : function(e) {
        e.preventDefault();
        e.stopPropagation();
        if(this.model.get("mimetype") == 2 && (this.model.get("checked") || this.model.get("path") == "shared")) {
            goldfish.router.navigate("files/" + (this.sharedView ? 'shared/' : '') + (this.publicView ? 'public/' : '') + this.model.get("path_hash"), {trigger: true, replace: true});
        } else {
            console.log(this.model.toJSON());
        }
    },
    removeItem: function() {
        this.undelegateEvents();
        this.$el.removeData().unbind();
        this.remove();
        Backbone.View.prototype.remove.call(this);
    },
    setEdit: function(data) {
        this.edit = data.get("edit");
        //this.hideMenu();
        if(this.edit) {
            this.$tdName.find("span.text-name").hide();
            this.$tdName.find("span.input-name").show();
            this.$tdName.find("span.input-name > input").focus();
        } else {
            this.$tdName.find("span.text-name").show();
            this.$tdName.find("span.input-name").hide();
        }
    },
    inInput: function(e) {
        e.stopPropagation();
        var $input = $(e.currentTarget);
        var start = 0, end = $input.val().indexOf(".");
        $input.get(0).setSelectionRange(start,end);
    },
    outInput: function(e) {
        e.stopPropagation();
        var $input = $(e.currentTarget);
        this.model.set("edit", false);
        if(this.name != this.model.get("name")) {
            var me = this;
            this.$tdName.find("span.text-name").html("Cambiando Nombre......");
            this.model.save(this.model.attributes, {
                success: function(model, response, options) {
                    goldfish.log("%cSuccess!","color: green");
                    toastr.success("Nombre Cambiado: " + me.name + " -> " + me.model.get("name"));
                    me.$tdName.find("span.text-name").html(me.model.get("name"));
                    me.name = me.model.get("name");
                    me.path = me.model.get("path");
                    me.model.set('path_hash', response.path_hash);
                    me.model.set('icon', response.icon);
                },
                error: function(model, response, options) {
                    goldfish.log("%cSave error!",'color: red;');
                    toastr.error(response.responseJSON.msg);
                    me.$tdName.find("span.input-name > input").val(me.name);
                    me.model.set('name', me.name);
                    me.model.set('path', me.path);
                }
            });
        }
    },
    changeName: function(e) {
        e.stopPropagation();
        var $input  = $(e.currentTarget);
        var str     = this.path;
        var res     = str.replace(this.name, "");
        var path    = res + $input.val();
        this.model.set('name', $input.val());
        this.model.set('path', path);
    },
    setName: function(data) {
        this.$tdName.find("span.text-name").html(data.get("name"));
    },
    setIcon: function(data) {
        this.$svg = this.$('td.icon > div > span > svg');
        this.$svg.attr({"class" : data.get("icon")});
        $("use",this.$svg).attr({"xlink:href": "img/icons/file/" + data.get("icon") + ".svg#icon-1"});
    }
});
