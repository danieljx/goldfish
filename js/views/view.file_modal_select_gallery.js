var goldfish = goldfish || {};

goldfish.FileGalleryViewSelect = Backbone.View.extend({
    template: _.template($('#tpl-files-modal-select-gallery').html()),
    tagName: 'div',
    className: '',
    events: {},
    selectedFile: function(file){},
    initialize : function(options) {
        this.close          = options.close || this.close;
        this.selectedFile   = options.selectedFile || this.selectedFile;
        this.collection     = new goldfish.FilesCollection({});
    },
    render : function() {
        var me = this;
        this.$el.html(this.template(this.collection.toJSON()));
        this.gallerySearchView = new goldfish.FileGallerySearchViewSelect({
            collection: this.collection
        });
        this.$(".searchGallery").append(this.gallerySearchView.render().el);
        this.galleryListView = new goldfish.FileGalleryListViewSelect({
            collection: this.collection,
            selectedFile: this.selectedFile,
            close: this.close
        });
        this.$(".listGallery").append(this.galleryListView.render().el);
        return this;
    }
});