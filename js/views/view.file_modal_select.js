var goldfish = goldfish || {};

goldfish.FileViewSelect = Backbone.View.extend({
    template: _.template($('#tpl-modal-files-select').html()),
    tagName: 'div',
    className: 'ui modal scrolling modalFileSelect',
    selectedFile: function(file){},
    events: {  },
    initialize: function(options) {
        var me = this;
        this.selectedFile   = options.selectedFile || this.selectedFile;
        this.collection     = new goldfish.FilesCollection({});
        //this.model          = new goldfish.Files({});
       this.render();
    },
    render: function() {
        var me = this;
        this.$el.html(this.template(this.model.toJSON()));
        this.$el.modal({
            onShow: function () {
                $('body').addClass('scrolling');
            },
            onHidden: function () {
                $('body').removeClass('scrolling');
                me.close();
            }
        });
        this.show();
        this.$('.filesTab .item').tab();
        this.$loadMyFile = this.$(".loadMyFile");
        this.collection.fetch({
            data: {
                path_hash: ''
            },
            processData: true,
            success: function (model, response, options) {
                me.model = me.collection.models[0];
                me.fileListView = new goldfish.FileListViewSelect({
                    model: me.model,
                    selectedFile: me.selectedFile,
                    close: function () {
                        me.hide();
                    }
                });
                me.$(".myfilesTab").append(me.fileListView.render().el);
                me.$loadMyFile.removeClass("active");
            },
            error: function (model, response, options) {
                console.log("%cSave error!", 'color: red;');
                toastr.error(response.responseJSON.msg);
                me.$loadMyFile.removeClass("active");
            }
        });
        this.galleryView = new goldfish.FileGalleryViewSelect({
            selectedFile: this.selectedFile,
            close: function () {
                me.hide();
            }
        });
        this.$(".galleryTab").append(this.galleryView.render().el);
        return this;
    },
    show: function() {
        this.$el.modal('show');
    },
    hide: function () {
        this.$el.modal('hide');
    },
    close: function() {
        this.undelegateEvents();
        this.$el.removeData().unbind();
        this.remove();
        Backbone.View.prototype.remove.call(this);
    }
});