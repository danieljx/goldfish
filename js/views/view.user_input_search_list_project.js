var goldfish = goldfish || {};

goldfish.UserInputSearchListProjectView = Backbone.View.extend({
    template: _.template($('#tpl-user-input-search-list-project').html()),
    tagName: 'li',
    className: 'project-option',
    initialize: function(opt) {
        this.shared_user = opt.shared_user;
        this.file   = opt.file;
    },
    events: {
        'click': 'addProject'
    },
    render: function() {
        this.$el.html(this.template(this.model.toJSON()));
        return this;
    },
    addProject: function(e) {
        e.preventDefault();
        e.stopPropagation();
        var $a = $(e.currentTarget);
        this.shared_user.addProject(this.file, this.model);
        $('.search-users-list').hide();
        return false;
    }
});
