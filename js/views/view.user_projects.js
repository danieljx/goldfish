var goldfish = goldfish || {};

goldfish.UserProjectsView = Backbone.View.extend({
   template: _.template($('#tpl-user-projects').html()),
   // current: 0,
   
   initialize: function() {
      this.render();
   },
   events: {
      'click .project > a': function(e) {
         if ($(e.target).closest('.project').hasClass('locked')) {
            // Procedimiento login hacia aplicación remota (LMS/R&C)
            e.preventDefault();   
         }
      }
   },
   render: function() {
      goldfish.log(this.model.toJSON());
      this.$el.html( this.template(this.model.toJSON()) );
      return this;
   },
   toggleVisibility: function(visible) {
      if (visible) {
         this.$el.css('display','block');
      } else {
         this.$el.css('display','none');
      }
   },
});