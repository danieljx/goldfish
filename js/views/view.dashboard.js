var goldfish = goldfish || {};

goldfish.DashBoardView = Backbone.View.extend({
   template: _.template($('#tpl-dashboard').html()),
   log: false ? console.log.bind(window.console) : function() { },
   initialize: function() {
      me = this;
      this.render();
      this.listenTo(this.model, 'change', function() {
        this.render();
      });
	},
  render: function() {
    this.log(this.model.toJSON());
    this.$el.html( this.template(this.model.toJSON()) );
    this.delegateEvents();
  },
  events: {
    'resize ul': function(e) {
      e.preventDefault();
      goldfish.log("resize event");
      goldfish.log(e);
    }
  },
  toggle: function(state) {
    this.model.attributes.showMenu = (state !== undefined) ? state : !this.model.attributes.showMenu;
      if (this.model.attributes.showMenu) {
        $('body').toggleClass("full", false);
        this.$el.toggleClass("on", true);
      } else {
        $('body').toggleClass("full", true);
        this.$el.toggleClass("on", false);
      }  
  }
  
});