var goldfish = goldfish || {};

goldfish.UserInputSearchListView = Backbone.View.extend({
    template: _.template($('#tpl-user-input-search-list').html()),
    tagName: 'div',
    className: 'user-selector project-selector',
    initialize: function(opt) {
        this.shared_user = opt.shared_user;
    },
    events: {
        'click ul.ul-users > li.user-option': 'addUser',
        'click ul.ul-projects > li.project-option': 'addProject'
    },
    render: function() {
        this.$el.html(this.template(this.model.toJSON()));
        return this;
    },
    addUser: function(e) {
        e.preventDefault();
        e.stopPropagation();
        var $a = $(e.currentTarget);
        console.log($a);
        return false;
    },
    addProject: function(e) {
        e.preventDefault();
        e.stopPropagation();
        var $a = $(e.currentTarget);
        console.log($a);
        return false;
    },
    setUsers: function(users) {
      var me = this, count = 0;
        this.$users     = this.$('ul.ul-users');
        this.$('ul.ul-users > li.user-option').not('.not').remove();
        this.$userEmpy  = this.$('ul.ul-users > li.user-option.not');
        this.$userEmpy.hide();
        users.forEach(function(dataChild) {
            me.renderItemUser(dataChild);
            count++;
        });
        if(count < 1) {
          this.$userEmpy.show();
        }
    },
    renderItemUser: function(user) {
        this.userInputSearchListUserView = new goldfish.UserInputSearchListUserView({
            model: user,
            shared_user: this.shared_user,
            file: this.model
        });
        this.$users.append(this.userInputSearchListUserView.render().el);
    },
    setProjects: function(projects) {
      var me = this, count = 0;
        this.$projects     = this.$('ul.ul-projects');
        this.$('ul.ul-projects > li.user-option').not('.not').remove();
        this.$projectEmpy  = this.$('ul.ul-projects > li.project-option.not');
        this.$projectEmpy.hide();
        projects.forEach(function(dataChild) {
            me.renderItemProject(dataChild);
            count++;
        });
        if(count < 1) {
          this.$projectEmpy.show();
        }
    },
    renderItemProject: function(project) {
        this.userInputSearchListProjectView = new goldfish.UserInputSearchListProjectView({
            model: project,
            shared_user: this.shared_user,
            file: this.model
        });
        this.$projects.append(this.userInputSearchListProjectView.render().el);
    }
});
