var goldfish = goldfish || {};

goldfish.FileModalShareView = Backbone.View.extend({
    template: _.template($('#tpl-modal-share-files').html()),
    tagName: 'div',
    className: 'dialog share',
    path: '',
    newFile: {},
    collection: {},
    events: {
        'click .cancel': 'cancel',
        'click .ok': 'save',
        'change input[name=share_others]': 'shareOthers',
        'change input[name=share_details]': 'shareDetails'
    },
    initialize: function(opt) {
        var me = this;
        this.shared_user = this.model.get('shared_user');
        this.shared_user.forEach(function(dataChild) {
          if(dataChild.get('share_edit') == 1) {
            me.model.set('share_others', dataChild.get('share_others'));
            me.model.set('share_details', dataChild.get('share_details'));
          }
        });
        this.listenTo(this.shared_user, 'add remove change', this.changeShared);
        this.delegateEvents();
        this.$el.css({
            width: "100%",
            height: "100%"
        });
    },
    render: function() {
        var me = this;
        this.$el.html(this.template(this.model.toJSON()));
        this.userInputSearchView = new goldfish.UserInputSearchView({
            model: this.model,
            shared_user: this.shared_user
        });
        this.$('.material-input.search > .input-wrapper').html(this.userInputSearchView.render().el);

        this.fileModalShareListView = new goldfish.FileModalShareListView({
            model: this.model,
            shared_user: this.shared_user
        });
        this.$('.material-input.list > .input-wrapper').html(this.fileModalShareListView.render().el);
        if(this.checkShared()) {
            this.$el.find('.ok').toggleClass('disabled', false);
        }
        this.$('.footer.ui').popup({
          popup : this.$('.footerPupup.popup'),
          on    : 'click',
          position   : 'top left',
          delay: {
            show: 300,
            hide: 800
          }
        });
        return this;
    },
    shareOthers: function(e) {
        e.preventDefault();
        e.stopPropagation();
        var $input = $(e.currentTarget);
        this.model.set('share_others', $input.val());
        this.shared_user.forEach(function(dataChild) {
          if(dataChild.get('share_edit') == 1) {
            dataChild.set('share_others', $input.val());
          }
        });
        return false;
    },
    shareDetails: function(e) {
        e.preventDefault();
        e.stopPropagation();
        var $input = $(e.currentTarget);
        this.model.set('share_details', $input.val());
        this.shared_user.forEach(function(dataChild) {
          if(dataChild.get('share_edit') == 1) {
            dataChild.set('share_details', $input.val());
          }
        });
        return false;
    },
    changeShared: function(data) {
        console.log('changeShared', data);
        this.$el.find('.ok').toggleClass('disabled', false);
    },
    checkShared: function() {
        var count = 0;
        this.shared_user.forEach(function(dataChild) {
            if(dataChild.get('share_action') == 'add') {
                count++;
            }
        });
        return (count>0);
    },
    cancel: function(e) {
        e.preventDefault();
        e.stopPropagation();
        var me = this;
        goldfish.appView.screenView.setBlock(false);
        this.$el.animate({opacity: 0}, 300, function() {
            me.remove();
        });
        return false;
    },
    save: function(e) {
        e.preventDefault();
        e.stopPropagation();
        var me = this;
        this.model.save(this.model.attributes, {
            silent: true,
            type: 'POST',
            url: '/api/user/' + goldfish.app.getUser().get("id") + '/file/shared/',
            success: function(model, response, options) {
                console.log("%cSuccess!","color: green");
                me.shared_user.reset(response.shared_user);
                me.$el.find('.ok').toggleClass('disabled', true);
            },
            error: function(model, response, options) {
                console.log("%cSave error!",'color: red;');
            }
        });
        return false;
    }
});
