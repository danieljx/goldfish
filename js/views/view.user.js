var goldfish = goldfish || {};

goldfish.UserView = Backbone.View.extend({
   template: _.template($('#tpl-user').html()),
   initialize: function() {
      me = this;
   		
   		goldfish.log("Userview init");
      this.render();
      this.listenTo(this.model, 'change', function() {
        this.render();
      });
	},
  render: function() {
goldfish.log("render user");
    goldfish.log(this.model.toJSON());
    
      this.$el.html( this.template(this.model.toJSON()) );
  },
	events: {
      'click > ul > li > a': function(e) {
        // e.preventDefault();
        // goldfish.log("user menu level 1");
        // // var dub = $(e.target).attr('id').split('menu-')[1];
        // // this.menuViewToggle();
        // // this.model.setActive(dub);
        // var dub = $(e.target).attr('id').split('menu-')[1];
        // this.menuViewToggle();
        // this.model.setActive(dub);
      },
      // 'click ul.user-units > li > a': function(e) {
        // e.preventDefault();
        // goldfish.log( $(e.target).attr('data-id'));
        // var dub = $(e.target).attr('id').split('menu-')[1];
        // this.menuViewToggle();
        // this.model.setActive(dub);
      // }
  }
});