var goldfish = goldfish || {};

goldfish.FileFolderModalView = Backbone.View.extend({
    template: _.template($('#tpl-dialog-folder').html()),
    tagName: 'div',
    className: 'dialog',
    path: '',
    newFile: {},
    collection: {},
    initialize: function(options) {
        this.collection = this.model.get("children");
        this.path = this.model.get("path") + "/";
        this.newFile = new goldfish.File({
            path: this.path,
            parent: this.model.get("id"),
            storage: this.model.get("storage"),
            icon: 'folder',
            mimetype:2
        });
    },
    render: function() {
        this.$el.html(this.template(this.model.toJSON()));
        return this;
    },
    events: {
        'click .ok': 'submit',
        'click .cancel': 'cancel',
        'keypress .folder-name-create': function(e){
            var char = String.fromCharCode(e.which);
            if (e.keyCode == 13 || e.which == 13) {
                this.submit();
            } else if (!char.match(/[A-Za-z0-9+#.]/)) {
                return false;
            }
        },
        'input .folder-name-create': function(e) {
            e.stopPropagation();
            this.updateModel($(e.currentTarget));
        }
    },
    focus: function() {
        this.$el.find('.folder-name-create').focus();
    },
    cancel: function() {
        var me = this;
        goldfish.appView.screenView.setBlock(false);
        this.$el.animate({opacity: 0}, 300, function() {
            me.remove();
        });
    },
    submit: function(e) {
        e.preventDefault();
        e.stopPropagation();
        goldfish.log("submit..");
        var me          = this;
        if (this.newFile.isValid()) {
            this.$el.find('.ok').toggleClass('disabled', true);
            goldfish.log("%cSaving..","color: green");
            this.newFile.save(this.newFile.attributes, {
                success: function(model, response, options) {
                    goldfish.log("%cSuccess!","color: green");
                    me.collection.add(me.parseResponse(response));
                    toastr.success("Carpeta: " + response.name + " creada");
                    me.cancel();
                },
                error: function(model, response, options) {
                    goldfish.log("%cSave error!",'color: red;');
                    toastr.error(response.responseJSON.msg);
                    me.cancel();
                }
            });
        }
    },
    updateModel: function($input) {
        var path = this.path + $input.val();
        this.newFile.set('name', $input.val());
        this.newFile.set('path', path);
        var validated = this.newFile.validateNameFolder(this.newFile.attributes, {}) || null;
        if (!validated) {
            this.$el.find('.ok').toggleClass('disabled', false);
            this.$el.find('.validation').html('');
        } else {
            this.$el.find('.ok').toggleClass('disabled', true);
            this.$el.find('.validation').html(validated);
        }
    },
    parseResponse: function(data) {
        return new goldfish.File(data, { parse: true });
    }
});
