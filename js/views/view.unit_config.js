var goldfish = goldfish || {};

goldfish.UnitConfigView = Backbone.View.extend({
    template: _.template($('#tpl-unit-config').html()),
    className: 'data-config',
	initialize: function() {
		// this.render();
		this.itemViews = {};
	},
	render: function() {
		console.log("%cRENDER UNIT CONFIG","color: green; font-size: 20px;");
		console.log(this.model);
		var data = this.model.get("data");
		console.log(data);
        this.$el.html( this.template( {} ) );

        var itemViews = $(document.createDocumentFragment());
        for(var name in data.config) {
            if(data.config.hasOwnProperty(name)) {
                var tmp = {
                    name: name,
                    value: data.config[name].value,
                    title: goldfish.i18n.t(name)
                };
                switch (data.config[name].type) {
                    case 'transition':
                        tmp.transitions = ['scale','zoom','fade','fade up','fade down','fade left','fade right','horizontal flip','vertical flip','drop','fly left','fly right','fly up','fly down','swing left','swing right','swing up','swing down','slide down','slide up','slide left','slide right'];
                        break;
                    case 'select':
                        tmp.allValues = data.config[name].allValues;
                        break;
                }
                itemViews.append( _.template( $('#tpl-data-config-'+data.config[name].type).html())(tmp) );		
            }
        }
        // data.config.each( function(aConfig) {
        // // for (var i = 0; i < data.config.length; i++) {
        //     data.config[i].title = goldfish.i18n.t(data.config[i].name);
		// 	itemViews.append( _.template( $('#tpl-block-config-'+data.config[i].type).html())(data.config[i]) );		
        // });

        this.$('.items').append(itemViews);
	    return this;
    },
    postRender: function() {
		// this.log(this.$('.ui.checkbox.toggle').length);
		this.$('.ui.checkbox.toggle').checkbox();
		this.$('.ui.dropdown').dropdown();
		// this.delegateEvents();
    },
    updateConfig: function(configProperty, val) {
        var data = this.model.get("data");
        console.log(data);
        console.log(configProperty);
        data.config[configProperty].value = val;
        this.model.set({ data: data }, { silent: true });
    },
	events: {
		'change .transition': function(e) {
			e.stopPropagation();
			// this.log("TRANSITION CHANGE");
			// this.log($(e.target).val());
			this.updateConfig($(e.target).closest('.config-item').attr('data-name'), $(e.target).val());
			
			// this.updateModel( "'"+$(e.target).val()+"'", $(e.target).closest('.contains').hasClass('custom-value'));
		}
	}
});