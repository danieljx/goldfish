var goldfish = goldfish || {};

/** Screen view to cover the app while performing isolated functions
* @author Mats Fjellner
* @augments Backbone.View
* @constructor
* @name ScreenView
*/
goldfish.ScreenView = Backbone.View.extend(/** @lends ScreenView */{
   initialize: function() {
      $(this.$screen).height($(document).height());
      this.contentHolder = this.$el.find('.content-holder');
	},
  events: {
    'click .red-config > p .ok': function(e) {
      e.preventDefault();
      goldfish.log("ok button");
      var me = this;


      var iframeWindow = $(e.target).closest('.red-config').find('iframe')[0].contentWindow;
      var blockCid = $(e.target).closest('.red-config').data('cid');
      var redName = $(e.target).closest('.red-config').data('red-name');
      // var currentView = goldfish.app.getUnit().

      var blockView = goldfish.unitView.getCurrentPageView().getUserBlockViewByCid( blockCid );
      console.log(blockView);

      console.log( iframeWindow.getIncludes() );

      var data = {
        id: 0,
        plugin_name: redName,
        xml: iframeWindow.getXml(),
        saves: iframeWindow.getIncludes(),
        output: '<div class="red '+redName+'"><iframe id="{{id}}" class="catflexheight" src="{{url}}" /></div>'
      };

      blockView.model.set('data', data);
      $(this.el).find('.red-config').remove();
      this.setBlock(false);

      // $.ajax({
      //   type: "POST",
      //   url: "content/red/save_content.php",
      //   data: {
      //     plugin_name: 'ludica',
      //     xml: iframeWindow.getXml(),
      //     includes: iframeWindow.getIncludes(),
      //     src: iframeWindow.getSrc(),
      //     async: false
      //   },
      //   dataType: 'json',
      //   success: function(result) {
      //     goldfish.log("save plugin successful..");
      //   /*  
      //     realNode.setAttribute('src', result.src);
      //     realNode.setAttribute('id', plugin+"-" + result.id);
      //     realNode.setAttribute('class', 'catplugin catflexheight');
      //     realNode.setAttribute('scrolling', 'no');
      //     realNode.setAttribute('style', 'border: none 0; width: '+result.w+'; height: 600px;');
      //     var newFakeNode = editor.createFakeElement(realNode, 'cke_catplugin', 'catplugin', true);
      //     newFakeNode.replace(fakeNode);
      //     goldfish.log("hide dialog");
      //     */
      //     $(this.$el).find('.red-config').remove();
      //     this.setBlock(false);
      //   }.bind(this),
      //   error: function(jqsumthn, status, error) {
      //     goldfish.log("save content ajax error");
      //   }
      // });
    },
    'click .red-config > p .cancel': function(e) {
      e.preventDefault();
      var me = this;
      this.$el.find('.red-config iframe').fadeOut(300, 
        function(f) {
          $(this).closest('.red-config').remove();
          me.setBlock(false);
        }
      );
    }
  },
  /** Blocks / unblocks main interface
  * @param {boolean} on
  */
  setBlock: function(on) {
    goldfish.log("setBlock "+(on ? "on" : "off"));
    if (on) {
      this.model.screen = true;
      this.$el.height($(document).height())
        .addClass("no-wait")
        .css('display','flex')
        .animate({opacity: 1}, 300);
    } else {
      this.model.screen = false;
      this.$el.animate({opacity: 0}, 300, function() {
        $(this).css('display','none');
        $(this).find('p').html('');
        $(this).removeClass("no-wait");  
      });
    }
  },
  /** Sets app ready for interaction.
   */
  setReady: function() { // Mejor cambiar por transition + .one()
    this.$el.animate({opacity: 0}, 300, function() {
      $(this).css('display','none');
      $(this).find('p').html('');
      $(this).removeClass("no-wait");
    });
  },
  /** Sets app busy (blocks interaction).
   */
  setBusy: function() { // Mejor cambiar por transition + .one()
    this.$el.height($(document).height());
    this.$el.removeClass("no-wait");
    this.$el.css('display','flex');
    this.$el.animate({opacity: 1}, 300, function() {
    });  
  },
  /** Sets screen for editor view
   */
  editorView: function() {
    this.setBlock(true);
    this.$el.append('<svg viewBox="0 0 29.978 29.978" class="editor-toggle" width="0" height="0"><use xlink:href="img/icons/save.svg#icon-1"></use></svg>');
  },
  addDialog: function(htmlStr) {
    goldfish.log("adding dialog");
    this.contentHolder.html(htmlStr);
  },
  /** Open RED config
  * @param {string} url
  */
  openConfig: function(red, blockView) {
    var redUrl = 'content/red/'+red;
    var configUrl = redUrl;
    configUrl = configUrl.split('/');
    var redName = configUrl[configUrl.length - 1];
    configUrl.splice(configUrl.length - 1, 0, '_config');
    
    configUrl = configUrl.join('/');
    var config = $('<div class="red-config"><iframe></iframe><p><svg viewBox="0 0 32 32" class="cancel"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/icons/cross.svg#icon-1"></use></svg><svg viewBox="0 0 32 32" class="ok"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/icons/check.svg#icon-1"></use></svg></p></div>');
    
    config.data('red-name', redName);
    config.data('cid', blockView.cid);
    config.find('iframe').on("load", function() {
      var data = blockView.model.get('data');
      if (!data) {
        data = {
          id: 0,
          plugin_name: '',
          xml: '',
          saves: [],
          output: '<div class="red '+redName+'"><iframe id="{{id}}" class="catflexheight" src="{{url}}" /></div>'
        };
      }
      this.initConfig(data, redUrl);
    }.bind(this));
    config.find('iframe').attr({
      src: configUrl
    });
    config.appendTo(this.$el);
  },
  initConfig: function(data, redUrl) {
    console.log("%cinitConfig", "color: green;");
    var config = $('.red-config').first();
    if (typeof config.find('iframe')[0].contentWindow == 'undefined') {
      setTimeout( this.initConfig(data, redUrl).bind(this), 500);
    } else {
      config.find('iframe')[0].contentWindow.init(data, redUrl, goldfish.generalConfig.toJSON());
    }
  },
  /** Show message to user
  * @param {string} msg
  */
  showMsg: function(msg) {
    this.$el.find("p").html(msg);
  }
});