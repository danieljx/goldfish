var goldfish = goldfish || {};

goldfish.UserInputSearchView = Backbone.View.extend({
    template: _.template($('#tpl-user-input-search').html()),
    tagName: "div",
    className: 'search-users',
    initialize: function(opt) {
        this.shared_user = opt.shared_user;
        this.userInputSearchListView = new goldfish.UserInputSearchListView({
            model: this.model,
            shared_user: this.shared_user
        });
    },
    events: {
        'keypress input#user-search': 'keypressSearch',
        'focusin input#user-search': 'inSearch',
        'focusout input#user-search': 'outSearch',
        'input input#user-search': 'chageSearch'
    },
    render: function() {
        this.$el.html(this.template(this.model.toJSON()));
        this.$('.search-users-list').html(this.userInputSearchListView.render().el);
        return this;
    },
    keypressSearch: function(e) {
        var char = String.fromCharCode(e.which);
        if (e.keyCode == 13 || e.which == 13) {
            this.submit();
        } else if ((char.length === 0 || !char.trim()) && !char.match(/[A-Za-z0-9+#.]/)) {
            return false;
        }
    },
    inSearch: function(e) {
        e.preventDefault();
        e.stopPropagation();
        var me = this;
        var $i = $(e.currentTarget);
        var q  = $i.val();
        if(!(q.length === 0 || !q.trim())) {
            this.$('.search-users-list').show();
        }
    },
    outSearch: function(e) {
        e.preventDefault();
        e.stopPropagation();
        var me = this;
        var $i = $(e.currentTarget);
        var q  = $i.val();
            //this.$('.search-users-list').hide();
    },
    chageSearch: function(e) {
        e.preventDefault();
        e.stopPropagation();
        var $i = $(e.currentTarget);
        var q  = $i.val();
        if(!(q.length === 0 || !q.trim())) {
            console.log(q);
            this.$('.search-users-list').show();
            this.userSearch(q);
            this.projectSearch(q);
        } else {
            this.$('.search-users-list').hide();
        }
        return false;
    },
    userSearch: function(q) {
        var me = this;
        var findUsers = goldfish.UserBaseCollection.search(q);
            findUsers.done(function(users) {
                me.userInputSearchListView.setUsers(users);
            });
    },
    projectSearch: function(q) {
        var me = this;
        var findProjects = goldfish.ProjectBaseCollection.search(q);
            findProjects.done(function(projects) {
                me.userInputSearchListView.setProjects(projects);
            });
    }
});
