var goldfish = goldfish || {};

goldfish.TagView = Backbone.View.extend({
    template: _.template($('#tpl-tags').html()),
    tagName: "div",
    initialize: function(opt) {
      this.collection = opt.collection;
    },
    events: { },
    render: function() {
        var data = { modelTags : this.collection.toJSON() };
            console.log(data);
        this.$el.html(this.template(data));
        return this;
    }
});
