var goldfish = goldfish || {};

goldfish.UserBlockCSSAttributeView = Backbone.View.extend({
	log: true ? console.log.bind(window.console) : function() { },
	tagName: 'div',
	className: 'css-attribute',
	template: _.template($('#tpl-block-css-attribute').html()),

	initialize: function() {
		this.types = this.model.getTypes();

		this.subTemplates = {};
		for (var i = 0; i < this.types.length; i++) {
			this.subTemplates[this.types[i]] = _.template($('#tpl-block-css-attribute-'+this.types[i]).html());
		}

		this.listenTo(this.model, "render", this.render);

		this.listenTo(this.model, "remove", function() { 

			this.log("%cAttribute view remove "+this.cid,"font-size:30px; color: #fa0;");
			this.remove();
		});
	},
	_colorValue: function(aColor) {
		if (aColor.length == '4') {
			var fullColor = '#';
			for (var i = 1; i <= 3; i++) {
				fullColor += aColor.charAt(i)+aColor.charAt(i);
			}
			return fullColor;
		} else {
			return aColor;
		}
	},
	render: function() {
		this.$el.html( this.template( this.model.toJSON() ) );

		this.$('[class*="attr-type"]').each( function(idx, el) {
			var classNames = el.className.split(' ');
			for (var i = 0; i < classNames.length; i++) {
				if ((classNames[i].indexOf('attr-type') === 0)) {
					var postfix = classNames[i].substr(10);
					var custom = classNames.includes('custom-value');
					var enabled = custom ? this.model.get("isCustom") : !this.model.get("isCustom");
					var templateName = postfix !== '' ? classNames[i].substr(10) : 'general';
					var templateJSON = { "value" : $(el).html(), "enabled": enabled };
					if (postfix == 'font') {
						templateJSON.fonts = goldfish.fonts.toJSON();
					}
					$(el).html(  this.subTemplates[postfix || 'general']( templateJSON ) );
					
				}
			}
			
		}.bind(this));

		return this;
	},
	attach: function() {
		return this.el;
	},
	postRender: function() {
		this.log("%cPostrender attribute "+this.cid,"font-size: 30px; color: green");
		// this.delegateEvents();
		// this.$('.ui.checkbox.toggle').checkbox();
		// this.$('.ui.dropdown').dropdown();
		
	},
	updateModel: function(value, custom) {
		this.model.setValue(value, custom);
		return true;
	},
	events: {
		'input [type="text"]:not(.color-pick)': function(e) {
			e.stopPropagation();
			this.log("Attribute input");
			this.updateModel( $(e.target).val(), $(e.target).closest('.contains').hasClass('custom-value'));
		},
		'input .color-pick': function(e) {
			this.log($(e.target).val());
			e.stopPropagation();
			this.updateModel( $(e.target).val(), $(e.target).closest('.contains').hasClass('custom-value'));
		},
		'change .font-pick': function(e) {
			this.log("FONT PICK");
			this.log($(e.target).val());
			this.updateModel( "'"+$(e.target).val()+"'", $(e.target).closest('.contains').hasClass('custom-value'));
		},
		'click .remove-attribute': function(e) {
			this.log("REMOVE ATTRIBUTE");
			this.model.collection.remove(this.model);
		},
	     'change [type="text"]': function(e) {
	     	// this.updateModel(e.target);
	      	// this.updateModel($(e.target));
		  },
		  'change [type="checkbox"]': function(e) {
			  this.log("Checkbox change");
			  var useCustom = $(e.target).prop('checked');
			  this.$('.default-value input').attr('disabled', useCustom);
			  this.$('.custom-value input').attr('disabled', !useCustom);
			  this.log($(e.target).prop('checked'));
			  this.model.set("isCustom", useCustom);
		  } 
      }
});