var goldfish = goldfish || {};

goldfish.ProjectView = Backbone.View.extend({
   template: _.template($('#tpl-project').html()),
   tagName: 'div',
   className: 'project-full',
   
   
   initialize: function() {
      
      this.listenTo(this.model, "change", function() { 
         goldfish.log("%cProject model changed!","font-size: 20px");
         this.render(); });

   },
   render: function() {
      // goldfish.log("Project view render");
      // goldfish.log(this.model.toJSON());
      this.$el.html( this.template(this.model.toJSON()) );
      return this;
   },
   events: {
      'click .unit-add': function(e) {
         e.preventDefault();
         this.addUnitDialog();
      }
   },
   
   addUnitDialog: function() {
      goldfish.appView.screenView.setBlock(true);
      // goldfish.log("%cScreen blocked",'color: green');
      this.createUnit = new goldfish.Unit({});
      // goldfish.log("%cUnit created",'color: green');
      // goldfish.log(this.createUnit);
     // goldfish.app.setUnit(this.createUnit);
      this.createUnitView = new goldfish.UnitCreateView( { model: this.createUnit } ); 
      // goldfish.log("%cUnit view created",'color: green');
      goldfish.appView.screenView.$el.prepend( this.createUnitView.render().$el );
      this.createUnitView.delegateEvents();
      this.createUnitView.focus();
      // goldfish.log("%cUnit created",'color: green');
         // addDialog( _.template($('#tpl-dialog-unit').html(), {}) );
   },
   toggleVisibility: function(visible) {
      if (visible) {
         this.$el.css('display','block');
      } else {
         this.$el.css('display','none');
      }
   },
});