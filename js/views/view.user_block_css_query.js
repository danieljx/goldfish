var goldfish = goldfish || {};

goldfish.UserBlockCSSQueryView = Backbone.View.extend({
	log: false ? console.log.bind(window.console) : function() { },
	tagName: 'div',
	className: 'css-query',
	template: _.template($('#tpl-block-css-query').html()),

	initialize: function() {
        this.itemViews = {};
		// this.model.on('attachView', this.attach);
		// this.listenTo(this.model, "change", this.render);
	},
	render: function() {
		this.log("render css query view");
		var rules = this.model.get('rules');
		this.$el.html( this.template( this.model.toJSON() ) );

		var ruleViews = $('<section></section>');
		rules.each( function(el) {
			this.log("ADDING RULE VIEW");
			this.itemViews[el.cid] =  new goldfish.UserBlockCSSRuleView ( {model: el });
			ruleViews.append(this.itemViews[el.cid].render().attach());		
		}.bind(this));
		this.log(ruleViews);

		this.$el.append(ruleViews);
	
	
		// this.$el.html(body); 
		// this.$el.attr('id', 'block-'+this.model.cid);
		// this.$el.attr('data-cid',this.model.cid);
		
	    return this;
	},
	postRender: function() {
		// this.model.get('rules').each( function(el) {
		// 	this.itemViews[el.cid].postRender();
		// }.bind(this) );
		//  this.delegateEvents();

	},
	events: {
  	},
	_isAttached: function() {
		return (this.$el.parent().length > 0);
	},
	attach: function() {
		return this.el;
	},
	detach: function() {
		this.viewState = 'view';
	},
	updateModel: function(aTarget) {
		var aCid = aTarget.attr('data-cid');
		var rule = this.model.getCSSRule(aCid);
		if (aTarget[0].tagName.toLowerCase() === 'input') { 
			goldfish.log(aTarget.val());
			rule.setAttribute(aTarget.prevAll('span').html(), aTarget.val());
		} else {
			rule.setAttribute(aTarget.prevAll('span').html(), aTarget.html());
		}
		
		
		
		// this.model.set('body',this.$el.find('.block').first().html(),{silent: true});
		return true;
	}
});