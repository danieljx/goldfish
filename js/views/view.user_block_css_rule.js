var goldfish = goldfish || {};

goldfish.UserBlockCSSRuleView = Backbone.View.extend({
	log: true ? console.log.bind(window.console) : function() { },
	tagName: 'div',
	className: 'css-rule',
	template: _.template($('#tpl-block-css-rule').html()),

	initialize: function() {
		this.itemViews = {};
		this.listenTo(this.model, "add-attribute", this._onAddAttribute);
		// this.model.on('attachView', this.attach);
		// this.listenTo(this.model, "change", this.render);
	},
	render: function() {
		var cssAttributes = this.model.get('cssAttributes');
		
        var json = this.model.toJSON();
		json.attrList = this.model.availableAttributes();
		
        this.$el.html( this.template( json ) );
        
		var attributeViews = $('<section></section>');
		// var attributeViews = $(document.createDocumentFragment());

		cssAttributes.each( function(el) {
			this.itemViews[el.cid] =  new goldfish.UserBlockCSSAttributeView ( {model: el });
			attributeViews.append(this.itemViews[el.cid].render().attach());		
			
		}.bind(this));

		this.$('.attributes').append(attributeViews);
	    return this;
	},
	_onAddAttribute: function(el) {
		var newItemView =  new goldfish.UserBlockCSSAttributeView ( {model: el });
		this.$('.attributes').append(newItemView.render().attach());
		newItemView.postRender();

		// this.itemViews[el.cid] =  new goldfish.UserBlockCSSAttributeView ( {model: el });
		// attributeViews.append(this.itemViews[el.cid].render().attach());		
	},
	postRender: function() {
		// this.$('.ui.checkbox.toggle').checkbox();
        //  this.delegateEvents();
	},
	events: {
		'click .add-attribute:not(".disabled")': function(e) {
			this.model.addAttribute( this.$('.attribute-pick').dropdown('get value') );
		},
		'change .attribute-pick': function(e) {
			this.log("Change");
			if ( $(e.target).val() !== '' ) {
				this.$('.add-attribute').removeClass("disabled");
			} else {
				this.$('.add-attribute').addClass("disabled");
			}
			// this.model.changeType( $(e.target).val(), true);
		},
  	},
	_isAttached: function() {
		return (this.$el.parent().length > 0);
	},
	attach: function() {
		return this.el;
	},
	detach: function() {
		this.viewState = 'view';
	},
	updateModel: function(aTarget) {
		var aCid = aTarget.attr('data-cid');
		var rule = this.model.getCSSRule(aCid);
		if (aTarget[0].tagName.toLowerCase() === 'input') { 
			goldfish.log(aTarget.val());
			rule.setAttribute(aTarget.prevAll('span').html(), aTarget.val());
		} else {
			rule.setAttribute(aTarget.prevAll('span').html(), aTarget.html());
		}
		
		// this.model.set('body',this.$el.find('.block').first().html(),{silent: true});
		return true;
	}
});