var goldfishView = Backbone.View.extend({
    setLog: function() {
		if (this.debug) {

            // console.log(this.viewName+" "+this.cid);
            // var output = (typeof arguments[0] === 'string' && arguments[0].indexOf("%c") == 0) ? arguments[0].substr(2) : arguments[0];
            // var color = this.viewName ? this.strToColor(this.viewName) : '#ffffff';
            // goldfish.log.call(console, "%c"+this.viewName+" "+this.cid, "color: "+color);
            // goldfish.log.call(console, output);
            // goldfish.log.apply(goldfish.log, [output]);
            // return Function.prototype.bind.call(console.log, console);
            // this.log = console.log.bind( _.partial(_,'background-color: #0f5; color: #000').bind(console.log), ''+this.viewName+' '+this.cid+': %s' );
            // this.log = _.partial(
            //     Function.prototype.bind.call(console.log, console), _, 'background-color: #0f5; color: #000'
            // ).bind(this);
            this.log = goldfish.log.bind( goldfish.log, "%c"+this.viewName+" "+this.cid+": %s", 'color: '+this.strToColor(this.viewName));
            // this.log = function (a) { console.log.apply(console, arguments); };
            // this.log = function(a,b) { return console.log.bind(console)(a,b); }.bind(this);
            // this.log = function() { return function() {}; };
		}
    },
    logParameters: function(args) {
    },
    strToColor: function(str) {
        var hash = 0;
        for (var i = 0; i < str.length; i++) {
            hash = str.charCodeAt(i) + ((hash << 5) - hash);
        }
        var color = '#';
        for (var j = 0; j < 3; j++) {
            var value = (hash >> (j * 8)) & 0xFF;
            color += ('00' + value.toString(16)).substr(-2);
        }
        return color;
    },
    hashCode: function(str) { // java String#hashCode
        var hash = 0;
        for (var i = 0; i < str.length; i++) {
           hash = str.charCodeAt(i) + ((hash << 5) - hash);
        }
        return hash;
    },
    intToRGB: function(i){
        var c = (i & 0x00FFFFFF)
            .toString(16)
            .toUpperCase();
    
        return "00000".substring(0, 6 - c.length) + c;
    }
});