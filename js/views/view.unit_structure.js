var goldfish = goldfish || {};

goldfish.UnitStructureView = Backbone.View.extend({
    log: true ? console.log.bind(window.console) : function() {},
    template: _.template($('#tpl-unit-structure').html()),
    initialize: function() {
        this.treeG = false;
        this.getData();
        console.log(this.data);
    },
    getData: function() {
        this.log("%c> getData",'background-color: #0ff; color: #022;');
        var JSONmodel = this.model.get("page").toJSON();
        this.data = JSON.parse(JSON.stringify(JSONmodel), function(k, v) { // Cambiando propiedad "pages" de modelo a "children" para D3
            if (k === "pages") {
                this.children = v;
            } else {
                if (k === "block") {
                    delete this.block;
                } else {
                    return (k === "position") ? v*1 : v;
                }
            }
        });
        this.newData = true;
    },
    render: function() {
        var html = this.template(this.model.toJSON());
        this.setElement(html);
        return this;
    },
    loadPage: function(aCid) {
        this.model.setCurrentPage(aCid);
    },
    events: {
    },
    returnSanitized: function(node) {
        var sanNode = {};
        sanNode.title = node.data.title;
        sanNode.position = node.data.position;
        sanNode.pages = [];
        if (node.children) {
            node.children.forEach(function(child, index, array) {
                sanNode.pages.push(this.returnSanitized(child));
            }.bind(this));
        }
        return sanNode;
    },
    initTree: function() {
        this.log("%c> initTree",'background-color: #0ff; color: #022;');
        this.treeG = d3.select('#structure').append("svg").append("g");
        this.root = d3.hierarchy(this.data, function(d) {
            return d.children;
        });
        this.treemap = d3.tree();
        this.tooltip = d3.select('#structure')
            .append("div")
            .classed("tooltip", true);
        this.selectedNode = null;
        this.draggingNode = null;
        this.dragStarted = false;
        this.selected = null;
        this.duration = 300;

        window.addEventListener("resize", function() {
            this.update(this.root);
        }.bind(this));
    },
    update: function() {
        this.log("%c> update",'background-color: #0ff; color: #022;');
        var me = this;
        if (!this.treeG) {
            this.initTree();
        } 
        if (this.newData) {
            this.root = d3.hierarchy(this.data, function(d) {
                return d.children;
            });
            this.log(this.data);
            this.newData = false;
        }

        this.root.sort(function(a, b) {
            return b.data.position*1 < a.data.position*1 ? 1 : -1;
        });

        var i = 0;

        this.margin = this.$el.height() * 0.05;
        this.height = this.$el.height() - 2 * this.margin;
        this.width = this.$el.width() - 2 * this.margin;
        this.maxDim = Math.max(this.width, this.height);
        

        this.log("%cw/h: "+this.width+"/"+this.height,'background-color: #0ff; color: #022;');

        this.root.x0 = this.width / 2;
        this.root.y0 = this.height / 2;

        

        this.treemap.size([this.width, this.height]);

        this.treeG.attr("transform", "translate(" + this.margin + "," + this.margin + ")");

        this.maxDepth = 1;
        this.treeData = this.treemap(this.root.sort());

        this.nodes = this.treeData.descendants();

        this.links = this.treeData.descendants().slice(1);
        this.nPerDepth = {};
        this.nodes.forEach(function(d) {
            this.maxDepth = Math.max(this.maxDepth, d.depth);
             if (d.depth in this.nPerDepth) {
                this.nPerDepth[d.depth] += 1; 
            } else {
                this.nPerDepth[d.depth] = 1;
            }
        }.bind(this));
        console.log("%cnPerDepth:","font-size: 30px; color: red;");
        console.log(Math.max.apply(null, _.values(this.nPerDepth)));

        console.log(this.nodes);

        this.circleR = this.maxDim / (this.maxDepth * 10);
        this.circleR = Math.min(this.circleR, this.maxDim / 40);

        this.nodes.forEach(function(d) {
            d.y = d.depth * this.height / this.maxDepth;
            if (this.selected && (d.data.cid == this.selectedCid)) {
                this.selected = d; // Actualizar cual es el nodo seleccionado, cambian ids después al actualizar datos
            }
        }.bind(this));

        this.link = this.treeG.selectAll('path.link').
        data(this.links, function(d) {
            return d.id;
        });

        this.linkEnter = this.link.enter().insert("path", "g")
            .attr("class", "link")
            .attr("d", function(d) {
                return "M" + d.x + "," + d.y +
                    "C" + d.x + "," + (d.y + d.parent.y) / 2 +
                    " " + (d.x + d.parent.x) / 2 + "," + d.parent.y +
                    " " + d.parent.x + "," + d.parent.y;
            });

        this.linkUpdate = this.linkEnter.merge(this.link);

        this.linkUpdate.transition().
        duration(this.duration).
        attr("d", function(d) {
            return "M" + d.x + "," + d.y +
                "C" + d.x + "," + (d.y + d.parent.y) / 2 +
                " " + (d.x + d.parent.x) / 2 + "," + d.parent.y +
                " " + d.parent.x + "," + d.parent.y;
        });

        this.linkExit = this.link.exit().remove();

        this.node = this.treeG.selectAll('g.node')
            .data(this.nodes, function(d) {
                return d.id || (d.id = ++i);
            });

        this.nodeEnter = this.node.enter().
        append('g').
        attr('class', 'node').
        attr('id', function(d) { return 'node-' + d.id; }).
        attr("transform", function(d) {
            return "translate(" + this.root.x0 + "," + this.root.y0 + ")";
        }.bind(this)).
        on('click', click).
        on('dblclick', dblClick);

        this.nodeEnter.on("mouseover", function(d) {
            if (this.dragStarted) { overCircle(d); }
            me.log(d.data.title);
                this.tooltip.
                text(d.data.title).
                style("top", d.y + this.margin + "px").
                style("left", d.x + this.margin + this.circleR * 2 + "px");

                return this.tooltip.style("opacity", 1);
            }.bind(this))
            .on("mouseout", function(d) { if (this.dragStarted) { outCircle(d); } return this.tooltip.style("opacity", 0); }.bind(this));

        this.nodeEnter.append('circle').attr('r', this.circleR);

        this.nodeEnter.append("circle")
            .attr('class', 'ghostCircle')
            .attr("r", this.circleR * 2)
            .attr("opacity", 0.2); // change this to zero to hide the target area

        this.nodeUpdate = this.nodeEnter.merge(this.node);

        this.nodeUpdate
            .transition()
            .duration(this.duration)
            .attr("transform", function(d) {
                return "translate(" + d.x + "," + d.y + ")";
            });

        this.nodeUpdate.select('circle').
        attr('r', this.circleR).
        attr('cursor', 'pointer');

        this.nodeExit = this.node.exit().
        transition().
        duration(this.duration).
        attr("transform", function(d) {
            return "translate(" + this.root.y + "," + this.root.x + ")";
        }.bind(this)).
        remove();

        this.nodeExit.select('circle').remove();

        this.nodes.forEach(function(d) {
            d.x0 = d.x;
            d.y0 = d.y;
        });

        function click(d) {
            me.selected = d;
            me.log(d.data.title);
            me.selectedCid = d.data.cid;
            d3.selectAll('.node:not(#' + this.id + ')').classed('selected', false);
            d3.select(this).classed('selected', true);
            document.getElementById('add-child').disabled = false;
            document.getElementById('remove').disabled = false;
            // me.update(d);
        }

        function dblClick(d) {
            me.loadPage(d.data.cid);
        }

        function setNodeDepth(d) {
            d.depth = d == me.root ? 0 : d.parent.depth + 1;
            if (d.children) {
                d.children.forEach(setNodeDepth);
            } else if (d._children) {
                d._children.forEach(setNodeDepth);
            }
        }

        function initiateDrag(d, domNode, context) {
            context.log("initiateDrag");
            context.log(d);
            context.draggingNode = d;
            d3.select(domNode).select('.ghostCircle').attr('pointer-events', 'none');
            d3.selectAll('.ghostCircle').attr('class', 'ghostCircle show');
            d3.select(domNode).attr('class', 'node activeDrag');

            // if nodes has children, remove the links and nodes
            if (dragNodes.length > 0) {

                // remove link paths
                links = dragNodes.slice(1);
                nodePaths = context.treeG.selectAll("path.link")
                    .data(links, function(d) {
                        return d.id;
                    }).remove();
                // remove child nodes
                nodesExit = context.treeG.selectAll("g.node")
                    .data(dragNodes, function(d) {
                        return d.id;
                    })
                    .filter(function(d, i) {
                        if (d.id == context.draggingNode.id) {
                            return false;
                        }
                        return true;
                    })
                    .remove();
            }

            parentLink = context.treemap(context.draggingNode.parent).descendants().slice(1);
            context.treeG.selectAll('path.link').filter(function(d, i) {
                if (d.id == context.draggingNode.id) {
                    return true;
                }
                return false;
            }).remove();

            dragStarted = null;
        }


        // Define the drag listeners for drag/drop behaviour of nodes.
        dragListener = d3.drag()
            .on("start", function(d) {
                me.log("dragstart");
                if (d == me.root) {
                    return;
                }
                me.dragStarted = true;
                dragNodes = me.treemap(d).descendants();
                d3.event.sourceEvent.stopPropagation();
                // it's important that we suppress the mouseover event on the node being dragged. Otherwise it will absorb the mouseover event and the underlying node will not detect it d3.select(this).attr('pointer-events', 'none');
            })
            .on("drag", function(d) {
                me.log("drag");
                if (d == me.root) {
                    return;
                }
                if (me.dragStarted) {
                    domNode = this;
                    initiateDrag(d, domNode, me);
                }

                d.x0 += d3.event.dx;
                d.y0 += d3.event.dy;
                var node = d3.select(this);
                // node.attr("transform", "translate(" + d.x0 + "," + d.y0 + ")");
                updateTempConnector();
            }).on("end", function(d) {
                if (d == me.root) {
                    return;
                }
                domNode = this;
                me.log(me.selectedNode);
                if (me.selectedNode) {
                    // selectedNode is destiny
                    // draggingNode is origin
                    // remove draggingNode from draggingNode.parent
                    // add draggingNode to selectedNode.children at last position
                    // update

                    var detachedChild = goldfish.app.getUnit().findPage(me.draggingNode.parent.data.cid).removeChild(me.draggingNode.data.cid, {silent: true});
                    me.log(typeof detachedChild);
                    me.log(detachedChild instanceof Backbone.Model);
                    me.log(detachedChild);
                    goldfish.app.getUnit().findPage(me.selectedNode.data.cid).addChild(detachedChild);

                    endDrag();
                } else {
                    endDrag();
                }
            });

        dragListener(d3.selectAll('.node'));

        function endDrag() {
            me.selectedNode = null;
            d3.selectAll('.ghostCircle').attr('class', 'ghostCircle');
            d3.select(domNode).attr('class', 'node');
            // now restore the mouseover event or we won't be able to drag a 2nd time
            d3.select(domNode).select('.ghostCircle').attr('pointer-events', '');
            me.dragStarted = false;

            updateTempConnector();
            if (me.draggingNode !== null) {
                // me.update(me.root);
                // centerNode(draggingNode);
                me.getData();
                me.update();
                me.draggingNode = null;

            }
            
        }

        var updateTempConnector = function() {
            var data = [];
            if (this.draggingNode !== null && this.selectedNode !== null) {
                // have to flip the source coordinates since we did this for the existing connectors on the original tree
                // data = [{
                //     source: {
                //         x: selectedNode.x0,
                //         y: selectedNode.y0
                //     },
                //     target: {
                //         x: draggingNode.x0,
                //         y: draggingNode.y0
                //     }
                // }];
            }
            //  var link = svg.selectAll(".templink").data(data);

            //  link.enter().append("path")
            //      .attr("class", "templink")
            //      .attr("d", function(d) {
            //        this.log(d);
            // return "M" + d.source.x + "," + d.source.y
            //   + "C" + (d.source.x + d.target.x) / 2 + "," + d.source.y
            //   + " " + (d.source.x + d.target.x) / 2 + "," + d.target.y
            //   + " " + d.target.x + "," + d.target.y;
            // })
            //      .attr('pointer-events', 'none');

            //  link.attr("d", function(d) {
            // return "M" + d.source.x + "," + d.source.y
            //   + "C" + (d.source.x + d.target.x) / 2 + "," + d.source.y
            //   + " " + (d.source.x + d.target.x) / 2 + "," + d.target.y
            //   + " " + d.target.x + "," + d.target.y;
            // });

            //  link.exit().remove();
        }.bind(this);

        var overCircle = function(d) {
            this.selectedNode = d;
            this.log("%cnew selected node: "+d.data.title,"background-color: #0ff; color: #022");
            
            updateTempConnector();
        }.bind(this);
        var outCircle = function(d) {
            this.selectedNode = null;
            updateTempConnector();
        }.bind(this);

        function collapse(d) {
            if (d.children) {
                d._children = d.children;
                d._children.forEach(collapse);
                d.children = null;
            }
        }

        function expand(d) {
            if (d._children) {
                d.children = d._children;
                d.children.forEach(expand);
                d._children = null;
            }
        }

        function sortTree() {
            tree.sort(function(a, b) {
                return b.data.position < a.data.position ? 1 : -1;
            });
        }

        document.getElementById('add-child').onclick = function() {
            var selectedModel = goldfish.app.getUnit().findPage(this.selected.data.cid);
            selectedModel.addChild({
                title: new Date().getTime()
            });
            this.getData();
            this.update(this.root);
        }.bind(this);

        document.getElementById('remove').onclick = function() {
            var parentModel = goldfish.app.getUnit().findPage(this.selected.parent.data.cid);
            parentModel.removeChild(this.selected.data.cid);
            this.getData();
            this.update(this.root);
        }.bind(this);

        document.getElementById('move-left').onclick = function() {
            this.log(this.selected);
            var children = [];
            var parent = this.selected.parent;
            var leftSibling = false;
            this.selected.parent.children.forEach(function(child) {
                if (leftSibling && (child.id == this.selected.id)) {
                    leftOrder = leftSibling.data.position;
                    goldfish.app.getUnit().findPage(leftSibling.data.cid).set({'position': child.data.position}, {silent: true});
                    goldfish.app.getUnit().findPage(child.data.cid).set({'position': leftOrder}, {silent: true});
                    d3.select('#node-'+child.id).classed('selected', false);
                    d3.select('#node-'+leftSibling.id).classed('selected', true);
                 //   this.selected = leftSibling;
                }
                leftSibling = child;
            }.bind(this));
            this.getData();
            this.update(this.root);
        }.bind(this);

        document.getElementById('move-right').onclick = function() {
            this.log(this.selected);
            var children = [];
            var parent = this.selected.parent;
            var selectedChild = false;
            this.selected.parent.children.forEach(function(child) {
                if (selectedChild) {
                    leftOrder = selectedChild.data.position;
                    goldfish.app.getUnit().findPage(selectedChild.data.cid).set({'position': child.data.position}, {silent: true});
                    goldfish.app.getUnit().findPage(child.data.cid).set({'position': leftOrder}, {silent: true});
                    d3.select('#node-'+selectedChild.id).classed('selected', false);
                    d3.select('#node-'+child.id).classed('selected', true);
                    selectedChild = false;
                   // this.selected = child;
                }
                if (child.id == this.selected.id) {
                    selectedChild = child;
                }
            }.bind(this));
            this.getData();
            this.update(this.root);
        }.bind(this);

        document.getElementById('save-tree').onclick = function() {
            this.log(JSON.stringify(this.returnSanitized(this.root)));
        }.bind(this);

        document.getElementById('go-unit').onclick = function() {
            if (this.selected) {
                this.loadPage(this.selected.data.cid);
            }
        }.bind(this);
    }
});