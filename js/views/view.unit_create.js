var goldfish = goldfish || {};

goldfish.UnitCreateView = Backbone.View.extend({
   template: _.template($('#tpl-dialog-unit').html()),
   tagName: 'div',
   className: 'dialog',
   initialize: function() {
      goldfish.log(this.model);
   },
   render: function() {
      this.$el.html( this.template( this.model ) );
      return this;
   },
   events: {
      'click .ok': function(e) {
         e.preventDefault();
         this.submit();
         
      },
      'click .cancel': function(e) {
         e.preventDefault();
         this.cancel();
      },
      'keypress .unit-name-create': function(e){
          var char = String.fromCharCode(e.which);
          if (e.keyCode == 13 || e.which == 13) {
              this.submit();
          }
          else if (char.match(/[\\\t/<>%$]/)) { // No permitir slashes, tab.. a completar con mas carácteres problemáticos
              return false;
            }
        },
      'input .unit-name-create': function(e) {
         e.stopPropagation();
         this.updateModel();

         
      },
   },
   focus: function() {
    this.$el.find('.unit-name-create').focus();
   },
   cancel: function() {
      var me = this;
      goldfish.appView.screenView.setBlock(false);
      this.$el.animate({opacity: 0}, 300, function() {
         // me.model.destroy();
         me.remove();
      });
   },
   submit: function() {
       goldfish.log("submit..");
       if (this.model.isValid()) {
           this.$el.find('.ok').toggleClass('disabled', true);
           goldfish.log("%cSaving..","color: green");
           this.model.save(this.model.attributes, {
               success: function(model, response, options) {
                   goldfish.log("%cSuccess!","color: green");
                   goldfish.app.getProject().fetch();
                   this.cancel();
                }.bind(this),
                error: function(model, response, options) {
                    goldfish.log("%cSave error!",'color: red;');
                }
            });
         }
   },
   updateModel: function() {
      this.model.set('title', this.$el.find('.unit-name-create').val());
      var validated = this.model.validate(this.model.attributes, {}) || null;
      
      if (!validated) {
         this.$el.find('.ok').toggleClass('disabled', false);
         this.$el.find('.validation').html('');   
      } else {
         this.$el.find('.ok').toggleClass('disabled', true);
         this.$el.find('.validation').html(validated);   
      }
      
   }
   
   
});