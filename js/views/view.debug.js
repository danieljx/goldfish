var goldfish = goldfish || {};

goldfish.DebugView = Backbone.View.extend({
   initialize: function() {
      this.render();
      this.listenTo(this.model, 'change', function() {
        this.render();
      });
	},
  render: function() {
    this.$el.html( '<p class="'+this.model.get("debugClass")+'">'+this.model.get("debugInfo") + '</p>' );
  },
  events: {
  }
  
});