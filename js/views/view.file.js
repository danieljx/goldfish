var goldfish = goldfish || {};

goldfish.FileView = Backbone.View.extend({
    template: _.template($('#tpl-files').html()),
    tagName: 'div',
    className: 'table-responsive-vertical',
    checkedAll: false,
    initialize : function(opt) {
        this.sharedView = opt.sharedView;
        this.publicView = opt.publicView;
        this.collection = this.model.get("children");
        this.listenTo(this.collection, 'add', this.addFile);
        this.listenTo(this.collection, 'remove', this.delFile);
        this.listenTo(this.collection, 'reset', this.render);
        this.listenTo(this.collection, 'checkAll', this.onCheckAll);
    },
    events: {
        'click thead > tr > th.checkAll > div > div.md-checkbox > label': 'selectFileAll',
        'click thead > tr > th.checkAll > div > div.md-checkbox > input[name=checkToolAll]': 'setCheckAll',
    },
    render : function() {
        var me = this;
        this.$el.html(this.template(this.model.toJSON()));
        this.$checkTool = this.$('thead > tr > th.checkAll > div > div.md-checkbox > input[name=checkToolAll]');
        this.$body  = this.$('table#table > tbody');
        this.$body.empty();
        this.collection.forEach(function(dataChild) {
            me.renderItem(dataChild);
        });
        return this;
    },
    renderItem: function(model) {
        var view = new goldfish.FileItemView({
            model: model,
            sharedView: this.sharedView,
            publicView: this.publicView
        });
        this.$body.append(view.render().el);
    },
    addFile: function(dataChild) {
        this.renderItem(dataChild);
    },
    delFile: function(dataChild) {
        this.collection.remove(dataChild);
    },
    selectFileAll: function(e) {
        e.preventDefault();
        e.stopPropagation();
        this.$checkTool.trigger('click');
        return false;
    },
    setCheckAll: function(e) {
        e.preventDefault();
        e.stopPropagation();
        this.$checkToolFixed = $('#tableFixed > thead > tr > th.checkAll > div > div.md-checkbox > input[name=checkToolAll]');
        this.checkedAll = !this.checkedAll;
        this.$checkTool.prop('checked', this.checkedAll);
        this.$checkToolFixed.prop('checked', this.checkedAll);
        this.collection.setCheckedAll(this.$checkTool.prop('checked'));
        return false;
    },
    onCheckAll: function(data) {
        if(data.boK) {
            $('input[name=checkTool]').parent().addClass('active');
            $('input[name=checkToolAll]').parent().addClass('active');
        } else {
            $('input[name=checkTool]').parent().removeClass('active');
            $('input[name=checkToolAll]').parent().removeClass('active');
        }
        $('input[name=checkToolAll]').prop('checked', data.boKAll);
    }
});
