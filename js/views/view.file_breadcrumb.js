var goldfish = goldfish || {};

goldfish.FileBreadcrumbView = Backbone.View.extend({
  template: _.template($('#tpl-files-breadcrumb').html()),
  tagName: 'div',
  className: 'file-breadcrumb',
  initialize: function (opt) {
    this.sharedView = opt.sharedView;
    this.publicView = opt.publicView;
    console.log('publicView', this.publicView);
  },
  render : function() {
    var data = this.model.toJSON();
        data.sharedView = this.sharedView;
        data.publicView = this.publicView;
    this.$el.html(this.template(data));
    return this;
  }
});
