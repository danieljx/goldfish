var goldfish = goldfish || {};

goldfish.FileItemMenuView = Backbone.View.extend({
    template: _.template($('#tpl-files-item-menu').html()),
    tagName: 'div',
    className: 'menu',
    events: {
        'click div.share': 'shareFile',
        'click div.download': 'downloadFile',
        'click div.changeName': 'changeName',
        'click div.tags': 'tagsFile',
        'click div.move': 'moveFile',
        'click div.copy': 'copyFile',
        'click div.drop': 'dropFile'
    },
    initialize: function(opt) {
        this.sharedView = opt.sharedView;
        this.publicView = opt.publicView;
        this.tags       = new goldfish.FileItemTagsView({
            model: this.model,
            sharedView: this.sharedView,
            publicView: this.publicView
        });
        this.$tagsView = this.tags.render().$el;
        this.$tagsView.modal();
    },
    render : function() {
      var data            = this.model.toJSON();
          data.sharedView = this.sharedView;
          data.publicView = this.publicView;
          data.shared     = data.shared.toJSON();
        this.$el.attr({
            id: 'menuNotificationItem_' + this.model.get("id")
        });
        this.$el.html(this.template(data));
        return this;
    },
    shareFile : function(e) {
        e.preventDefault();
        e.stopPropagation();
        goldfish.appView.screenView.setBlock(true);
        this.FileModalShare = new goldfish.FileModalShareView({
            model: this.model,
            sharedView: this.sharedView,
            publicView: this.publicView
        });
        goldfish.appView.screenView.$el.prepend( this.FileModalShare.render().$el );
        this.FileModalShare.delegateEvents();
        return false;
    },
    downloadFile : function(e) {
        e.preventDefault();
        e.stopPropagation();
        this.model.downloadFile({
            sharedView: this.sharedView,
            publicView: this.publicView,
            success: function(model, response, options) {
                console.log("%cSuccess Download!","color: green");
                console.log(response);
            },
            error: function(model, response, options) {
                console.log(model);
                console.log(response);
                console.log(options);
                console.log("%cDownload error!",'color: red;');
            }
        });
        return false;
    },
    changeName : function(e) {
        e.preventDefault();
        e.stopPropagation();
        $a = $(e.currentTarget);
        this.model.set("edit", true);
        return false;
    },
    tagsFile : function(e) {
        e.preventDefault();
        e.stopPropagation();
        this.$tagsView.modal('show');
        return false;
    },
    moveFile : function(e) {
        e.preventDefault();
        e.stopPropagation();
        $a = $(e.currentTarget);
        console.log("moveFile");
        console.log(this.model.get("name"));
        console.log($a);
        return false;
    },
    copyFile : function(e) {
        e.preventDefault();
        e.stopPropagation();
        $a = $(e.currentTarget);
        console.log("copyFile");
        console.log(this.model.get("name"));
        console.log($a);
        return false;
    },
    dropFile : function(e) {
        e.preventDefault();
        e.stopPropagation();
        $a = $(e.currentTarget);
        var me = this;
        this.model.destroy({
            wait: true,
            data: {
                path_hash: me.model.get("path_hash")
            },
            processData: true,
            success: function(model, response, options) {
                goldfish.log("%cSuccess!","color: green");
                console.log("%cSuccess!","color: green");
            },
            error: function(model, response, options) {
                goldfish.log("%cSave error!",'color: red;');
            }
        });
        return false;
    }
});
