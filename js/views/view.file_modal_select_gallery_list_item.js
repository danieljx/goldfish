var goldfish = goldfish || {};

goldfish.FileGalleryListViewSelectItem = Backbone.View.extend({
    template: _.template($('#tpl-files-modal-select-gallery-list-item').html()),
    tagName: 'a',
    className: 'card',
    events: {
        'click': 'selectFile'
    },
    close: function(){},
    selectedFile: function(file){},
    initialize: function(options) {
        this.close          = options.close || this.close;
        this.selectedFile = options.selectedFile || this.selectedFile;
        this.listenTo(this.model, 'remove', this.removeItem);
    },
    render: function () {
        var me = this;
        this.$el.html(this.template(this.model.toJSON()));
        this.$el.addClass(this.model.get('color'));
        this.$loadImg = this.$(".loadImg");
        this.img = this.$('img')[0];
        var newImg = new Image;
            newImg.onload = function () {
                me.img.src = this.src;
                me.$loadImg.removeClass("active");
            };
        newImg.src = "http://goldfish/api/user/2/file/" + (this.model.get('view') == '' ? '' : this.model.get('view') + "/") + "view/" + this.model.get('path_hash');
        return this;
    },
    selectFile : function(e) {
        e.preventDefault();
        e.stopPropagation();
        this.selectedFile(this.model);
        this.close();
        return false;
    },
    removeItem: function () {
        console.log("delFile Gallery");
        this.undelegateEvents();
        this.$el.removeData().unbind();
        this.remove();
        Backbone.View.prototype.remove.call(this);
        return false;
    },
});