var goldfish = goldfish || {};

goldfish.UnitView = Backbone.View.extend({
   template: _.template($('#tpl-unit').html()),
   // el: $('#unit'),
   // tagName: 'div',
   // className: 'unit',
   pageViews: [],
   currentEditor: false,
   currentPageView: false,
   componentViews: [],
   themesView: false,
   log: true ? console.log.bind(window.console) : function() { },
   // current: 0,
   initialize: function() {
      console.log("%cinit view","color: red; font-size: 40px;");
      var components = this.model.get("blocks");
      this.componentViews = [];
      components.each( function(el) {
         if (el.get("id") != 1) {
            this.componentViews.push( new goldfish.BlockView({model: el}) );
         }
      }.bind(this));
      this.screenView = new goldfish.ScreenView({model: this.model, el: this.$('.screen') });
      this.listenTo(this.model, "change:currentPage", function() { this.goToPage(this.model.get("currentPage")); }.bind(this) );
      this.listenTo(this.model, "change:showSecondaryMenu", function() { this.log("!!!"); this.secondaryMenuViewToggle(); }.bind(this) );
      this.listenTo(this.model, "change", function() { this.log("%cUnit changed!", "color: #0ff; font-size: 30px;"); });
      
      // this.render();
   },
   events: {
      'click #save-unit:not(.saving)': function(e) {

         e.preventDefault();
         this.$el.find('#save-unit').addClass("saving");
         goldfish.appView.setBusy();
         
         this.log("%csaving unit","color: green");
         console.log(this.model);
         this.log(this.model.toJSON());

         this.model.save(false, {wait: true, success: function() { 
            this.log("%cunit saved","color: green");
            this.log(this.model);
            $('#save-unit').removeClass('saving');
            goldfish.appView.showMsg("Unit saved.");
            goldfish.appView.setReady();
         }.bind(this)});
            // $('#save-unit').removeClass('saving');
            // goldfish.appView.showMsg("Unit saved.");
            // goldfish.appView.setReady();

      },
      'click #show-page:not(.active)': function(e) {
         e.preventDefault();
         this.showPage();
      },
      'click #export-unit:not(.active)': function(e) {
            e.preventDefault();
            goldfish.appView.setBusy();
            this.export();
            goldfish.appView.setReady();
         },
      'click #show-themes:not(.active)': function(e) {
         e.preventDefault();
         this.showThemes();
      },
      'click #show-config:not(.active)': function(e) {
            e.preventDefault();
            this.showConfig();
         },
      'click #show-structure:not(.active)': function(e) {
         e.preventDefault();
         this.showStructure();
      },
   	'click .page-list a:not(.toggle)': function(e) {
         e.preventDefault();
         var clickIndex = this.$pageList.find('a').index(e.target.closest('a')); // closest por si el click es en un hijo span
         this.showPage();
         this.goToPage(clickIndex);
         // if (goldfish.appView.showSecondaryMenu) {
         //    goldfish.appView.secondaryMenuView.activateDraggables();
         // }
   	},
      'click #preview-toggle': function(e) {
         e.preventDefault();
         $(e.target).closest('aside').find('button').removeClass('active');
         this.showPreview();
         // $(e.target).closest('button').addClass('active');
         // this.$el.find('.sections').removeClass("show-themes");

      },
      'click .unit-info > .info-toggle': function(e) {
         e.preventDefault();
         this.infoToggle();
      },
      'click #template-list a.toggle': function(e) {
         e.preventDefault();
         this.templateViewToggle();
      },
      'click #template-list li:not(.current)': function(e) {
         e.preventDefault();
         $(e.target).closest('ul').find('li').removeClass('current');
         $(e.target).closest('li').addClass('current');
      },
      'click #template-list li.current div': function(e) {
         e.preventDefault();
         $(e.target).closest('li').find('div').removeClass('current');
         $(e.target).closest('div').addClass('current');
      },
      'click #page > aside > .menu-toggle': function(e) {
         e.preventDefault();
         if (this.currentPageView) {
            this.currentPageView.config();   
         }
         
         // this.pageViews[this.model.getPages().getCurrentIndex()].config();

      },
      // 'click #page > aside > .preview-toggle': function(e) {
      //    e.preventDefault();
      //    this.previewPage(true);
      //    // this.pageViews[this.model.getPages().getCurrentIndex()].config();
      // },
      'click #add-page': function(e) {
         e.preventDefault();
         goldfish.appView.setBusy();
         this.addOne();
         goldfish.appView.setReady();
      },
      'input .unit-info-title > h2': function(e) {
         e.preventDefault();
         this.model.set('title', $(e.target).html(), {silent: true});
      },
      'click .page-list li .delete': function(e) {
         e.preventDefault();
         e.stopPropagation();
         var clickIndex = this.$pageList.find('a').index(e.target.closest('a'));

         this.model.getPages().remove( this.model.getPages().at( clickIndex ) );
         // this.model.getPages().each( function(item,i) {
         //    this.renderListItem(item, i);
         // }, this);
         // this.$pageList.find('li').last().remove();

         if ( this.model.getPages().getCurrentIndex() == clickIndex && this.model.getPages().length > 0 ) { // Borrando la página actualmente abierta
            this.goToPage( this.model.getPages().getCurrentIndex() >= this.model.getPages().length ? this.model.getPages().length - 1 : this.model.getPages().getCurrentIndex() );
         }

         this.render();
      }
   },
   switchToView: function(aSel, after) {
      if (this.$('.sections > section:visible').length > 0) {
            this.$('.sections > section:visible:not('+aSel+')').transition( {
                  animation: 'scale',
                  onComplete: function(e) {
                        this.$(aSel).transition( {
                              animation: 'scale',
                              onComplete: typeof after !== 'undefined' ? after : function() {}
                        });
                  }.bind(this)
            });
      } else {
            this.$(aSel).transition( {
                  animation: 'scale',
                  onComplete: typeof after !== 'undefined' ? after : function() {}
            });
      }
   },
   showStructure: function() {
      console.log("%cshowStructure","color: #ffaa00; background-color: #000; font-size: 20px; padding: 5px;");
      this.$el.find('.unit-info-title aside button').removeClass('active');
      
      if (!this.structureView) {
         this.structureView = new goldfish.UnitStructureView({model: this.model });      
         this.$el.find('#structure').replaceWith( this.structureView.render().el );
      }

      // this.structureView.getData();
      // this.structureView.update();

      this.switchToView('#structure', this.structureView.update.bind(this.structureView));
   },
   showPage: function() {
      console.log("%cshowPage","color: #ffaa00; background-color: #000; font-size: 20px; padding: 5px;");
      this.$el.find('.unit-info-title aside button').removeClass('active');
      this.switchToView('#page');
   },
   showThemes: function() {
      console.log("%cshowThemes","color: #ffaa00; background-color: #000; font-size: 20px; padding: 5px;");
      this.$el.find('.unit-info-title aside button').removeClass('active');

      if (this.themesView) {
            this.switchToView('#themes');
      } else {
         this.model.getProjectThemes( {
            success: function(themes) {
               this.model.set("themes", themes.clone());
               this.themesView = new goldfish.UnitThemesView({model: this.model });      
               this.$el.find('#themes').append( this.themesView.render().el );
               this.switchToView('#themes');
         }.bind(this) });   
      }
   },
   showConfig: function() {
      console.log("%cshowConfig","color: #ffaa00; background-color: #000; font-size: 20px; padding: 5px;");
      this.$el.find('.unit-info-title aside button').removeClass('active');

      if (this.configView) {
            this.switchToView('#config');
      } else {
            this.configView = new goldfish.UnitConfigView({model: this.model });      
            this.$el.find('#config').append( this.configView.render().el );
            this.configView.postRender();
            this.switchToView('#config');
      }
   },
   showPreview: function() {
      goldfish.log("open preview");
      var previewUrl = '/users/'+goldfish.app.getUser().get('id')+'/projects/'+goldfish.app.getProject().get('id');
      previewUrl += '/units/'+this.model.get('id');
      window.open(previewUrl,'_blank');
   },
   previewPage: function(on) {

      // this.preview = on;

      if (on) {
         goldfish.log("showing preview");
         goldfish.appView.showPreview(on);
         // this.preview = new goldfish.PageView({model: this.model.getPages().getCurrent() });

         // this.preview.view().render();

         // this.pageViews[ this.model.getPages().getCurrent().cid ];
         // goldfish.appView.showPreview(this.pageViews[ this.model.getPages().getCurrentIndex()].preview() );
         this.preview = new goldfish.PageView({el: $('#preview'), model: this.model.getPages().getCurrent(), viewState: 'view' });
         this.preview.render().attach();

      } else {
         goldfish.log("removing preview");
         goldfish.log(this.preview);
         this.preview.remove();
      }
      $('#preview').toggleClass('on');


      
   },
   export: function() {
      this.log("Exporting unit..");
      window.location.assign(this.model.url()+'/export');
   },
   getComponentViews: function() {
      return this.componentViews;
   },
   render: function() {
      // this.log("%crender unitView "+this.cid,'font-size: 20px');

      if (!this.model.get("page")) {
            // this.attributes.page = new goldfish.Page();
      } else {
            // this.log("page:");
            // this.log(this.model.get("page"));
      }
      // goldfish.log(this.model.toJSON());
      // this.$el.html( this.template(this.model.toJSON()) );
      this.setElement( this.template(this.model.toJSON()) );

      

      this.secondaryMenuView = new goldfish.UnitSecondaryMenuView({ model: this.model, el:  this.$('.secondary-menu') });
      this.$pageList = this.$('.page-list ul');
      goldfish.log("%cCOLLECTION", this.model.attributes.pages instanceof Backbone.Collection ? "color: green" : "color: red");

      this.model.on('change', function() {
         this.$('.unit-info h2').html(this.model.get('title'));
      }.bind(this));

      this.model.on('shrink', this.shrink, this);
      this.model.on('expand', this.expand, this);

      if (this.model.get("page")) {
            this.model.setCurrentPage(this.model.get("page").cid);
      }


      this.$('.sections > section:not(#page)').transition( 'hide' );
      this.$('.sections > section#page').transition( 'show' );


      return this;
   },
   toggleVisibility: function(visible) {
      if (visible) {
         this.$el.css('display','block');
      } else {
         this.$el.css('display','none');
      }
   },
   getCurrentPageView: function() {
         return this.currentPageView;
   },
   attachPageView: function(aCid) {
      // this.model.setCurrentPage(aCid);
      // this.$pageList.find('a').eq(index).addClass("current");
      this.$('#page').append('<div class="page"></div>');
      // this.log("%cGetting page model:","color: green");
      var aModel = this.model.getCurrentPage();
      // this.log("%cCreating view for model:","color: green");
      this.currentPageView = new goldfish.PageView({el: this.$('#page > .page'), model: aModel });
      // this.log("%cRendering page view","color: green");
      this.currentPageView.render().attach();
   },
   detachPageView: function() {
      // this.log("%cChecking detach","color: green");
      if (this.currentPageView) {
      //    this.log("%cAbout to detach","color: green");
         this.currentPageView.detach();
      //    this.log("%cDetach","color: green");
      }
      // this.log("%cSetting currentPageView = false","color: green");
      this.currentPageView = false;
   },
   goToPage: function(aCid) {
         console.log("%cGoToPage "+aCid, "font-size: 30px; color: red;");
      // console.log(aCid);
      if (aCid) {
         goldfish.appView.setBusy();
         this.detachPageView();
         this.attachPageView(aCid);
      //    this.log("%cShow page","color: red");
         this.showPage();
         
         goldfish.appView.setReady();   
      }
      
   },
   infoToggle: function(state) {
      this.model.attributes.showInfo = !this.model.attributes.showInfo;
      if (this.model.attributes.showInfo) {
         this.$('.unit-info').removeClass('collapsed');
      } else {
         this.$('.unit-info').addClass('collapsed');
      }
   },
   templateViewToggle: function(state) {
      this.model.attributes.showTemplates = !this.model.attributes.showTemplates;
      if (this.model.attributes.showTemplates) {
         this.$('#template-list').removeClass('collapsed');
      } else {
         this.$('#template-list').addClass('collapsed');
      }
   },
   addOne: function() {
      var itemModel = new goldfish.Page({
         title: "Sin nombre"
      });
      this.model.getPages().add(itemModel);
      this.renderListItem(itemModel, this.model.getPages().length - 1);
   },
      /** Toggle secondary menu
  * @param {boolean=} state - On/Off
   */
  secondaryMenuViewToggle: function() {
       if (this.model.get("showSecondaryMenu")) {
            console.log("open");
            this.shrink();
            this.secondaryMenuView.toggle(true);
         this.secondaryMenuView.loadComponents(this.getComponentViews());
         
         
       } else {
            console.log("close");
         this.expand();
         this.secondaryMenuView.toggle(false);
       }
    },
   updateList: function() {
      this.model.getPages().each( function(item, i) {
         if ( item.get('title') !== this.$pageList.find('a').eq(i).find('span').html() ) {
            this.$pageList.find('a').eq(i).find('span.title').html(item.get('title'));
         }
         if (item.isUpdated()) {
            this.$pageList.find('a').eq(i).addClass("changed");
         } else {
            this.$pageList.find('a').eq(i).removeClass("changed");
         }
      }, this);
   },
   renderListItem: function(item,i) {
   	var link = '<a href=""><span class="delete">x</span>'+(i+1)+'<span class="title">'+item.get('title')+'</span></a>';
		if (this.$pageList.find('li').length > i) {
      	this.$pageList.find('li').eq(i).html(link);
   	} else {
   		this.$pageList.append('<li>'+link+'</li>');
   	}
   },
   movePage: function(e) {
      console.log(e.detail);
      this.model.getPages().reorder(e.detail.index, e.detail.oldindex);
   },
   shrink: function() {
      this.$el.addClass("shrunk");
   },
   expand: function() {
      this.$el.removeClass("shrunk");
   }
});