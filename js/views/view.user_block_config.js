var goldfish = goldfish || {};

goldfish.UserBlockConfigView = Backbone.View.extend({
	log: true ? console.log.bind(window.console) : function() { },
	tagName: 'aside',
	className: 'data-config',
	template: _.template($('#tpl-data-config').html()),
	// viewBody: '',

	initialize: function() {
		this.itemViews = {};
	},
	render: function() {
        var data = this.model.get("data");
        this.$el.html( this.template( {} ) );

        var itemViews = $(document.createDocumentFragment());
        for(var name in data.config) {
            if(data.config.hasOwnProperty(name)) {
                var tmp = {
                    name: name,
                    value: data.config[name].value,
                    title: goldfish.i18n.t(name)
				};
				if (data.config[name].type == "transition") {
					tmp.transitions = ['scale','zoom','fade','fade up','fade down','fade left','fade right','horizontal flip','vertical flip','drop','fly left','fly right','fly up','fly down','swing left','swing right','swing up','swing down','slide down','slide up','slide left','slide right'];
				}
                itemViews.append( _.template( $('#tpl-data-config-'+data.config[name].type).html())(tmp) );		
            }
        }
        // data.config.each( function(aConfig) {
        // // for (var i = 0; i < data.config.length; i++) {
        //     data.config[i].title = goldfish.i18n.t(data.config[i].name);
		// 	itemViews.append( _.template( $('#tpl-block-config-'+data.config[i].type).html())(data.config[i]) );		
        // });

        this.$('.items').append(itemViews);
	    return this;
    },
    events: {
		'input .type-int [type="text"]': function(e) {
			e.stopPropagation();
            this.log("Attribute input");
            this.updateConfig($(e.target).closest('.config-item').attr('data-name'), parseInt($(e.target).val()));
            
			// this.updateModel( $(e.target).val(), $(e.target).closest('.contains').hasClass('custom-value'));
		},
		'change .type-toggle [type="checkbox"]': function(e) {
            e.stopPropagation();
            this.log("Checkbox change");
            this.updateConfig($(e.target).closest('.config-item').attr('data-name'), $(e.target).prop('checked'));
			// var useCustom = $(e.target).prop('checked');
			// this.$('.default-value input').attr('disabled', useCustom);
			// this.$('.custom-value input').attr('disabled', !useCustom);
			// this.log($(e.target).prop('checked'));
			// this.model.set("isCustom", useCustom);
		},
		'change .transition': function(e) {
			e.stopPropagation();
			this.log("TRANSITION CHANGE");
			this.log($(e.target).val());
			this.updateConfig($(e.target).closest('.config-item').attr('data-name'), $(e.target).val());
			
			// this.updateModel( "'"+$(e.target).val()+"'", $(e.target).closest('.contains').hasClass('custom-value'));
		}
	},
    updateConfig: function(configProperty, val) {
        var data = this.model.get("data");
        console.log(data);
        console.log(configProperty);
        data.config[configProperty].value = val;
        this.model.set({ data: data }, { silent: true });
    },
	postRender: function() {
		// this.log(this.$('.ui.checkbox.toggle').length);
		this.$('.ui.checkbox.toggle').checkbox();
		this.$('.ui.dropdown').dropdown();
		// this.delegateEvents();
	},
	_isAttached: function() {
		return (this.$el.parent().length > 0);
	},
	attach: function() {
		return this.el;
	},
	detach: function() {
		this.viewState = 'view';
	},
	updateModel: function(aTarget) {
		var aCid = aTarget.attr('data-cid');
		var rule = this.model.getCSSRule(aCid);
		if (aTarget[0].tagName.toLowerCase() === 'input') { 
			goldfish.log(aTarget.val());
			rule.setAttribute(aTarget.prevAll('span').html(), aTarget.val());
		} else {
			rule.setAttribute(aTarget.prevAll('span').html(), aTarget.html());
		}
		// this.model.set('body',this.$el.find('.block').first().html(),{silent: true});
		return true;
	}
});