var goldfish = goldfish || {};

goldfish.FileNotificationView = Backbone.View.extend({
    template: _.template($('#tpl-files-notification').html()),
    tagName: 'div',
    className: 'file-notification',
    events: {},
    initialize : function(opt) { },
    render : function() {
      this.$el.html(this.template(this.model.toJSON()));
      return this;
    }
});
