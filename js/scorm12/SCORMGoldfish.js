var fishunit = fishunit || {};

/** Scorm Representation
* @author Daniel Villanueva @danieljx
* @augments Backbone.Model
* @constructor
* @name Scorm 1.2
*/
fishunit.Scorm = Backbone.Model.extend({
    log: true ? console.log.bind(window.console) : function () { },
    defaults: {
        scorm: false,
        status: '',
        location: '',
        listenLocation: Backbone.Model.extend({}),
        fnLocation: false,
        keyLocation: '',
        attrLocation: ''
    },
    initialize: function (opt) {
        this.listenTo(this, "change:scorm", this.setScorm);
        this.set('scorm', doLMSInitialize());
        if (this.get('scorm')) {
            this.log("%cSCORM INITIALIZE ", "color: green;");
            this.set('status', doLMSGetValue("cmi.core.lesson_status"));
            this.set('listenLocation', (opt.listenLocation || Backbone.Model.extend({})));
            this.set('keyLocation', (opt.keyLocation || ''));
            this.set('fnLocation', (opt.fnLocation || false));
            this.set('attrLocation', (opt.attrLocation || ''));
            this.listenTo(this.get('listenLocation'), "change:" + this.get('keyLocation'), this.changeLocation);
            this.listenTo(this, "change:location", this.setLocation);
            this.set('location', doLMSGetValue("cmi.core.lesson_location"));
        }
    },
    setScorm: function (model) {
        if (model.get('scorm') == "false") {
            this.log("%cSCORM NOT INITIALIZE ", "color: red;");
            this.set('scorm', false);
        }
    },
    setLocation: function (model) {
        if (model.get('location') && model.get('location') != "") {
            if (this.get('listenLocation')[this.get('fnLocation')]) {
                this.get('listenLocation')[this.get('fnLocation')](this.get('location'));
            } else {
                this.log("%cSCORM fnLocation :" + this.get('fnLocation') + " not found", "color: red;");
            }
        }
    },
    changeLocation: function (model) {
        if (model.get(this.get('keyLocation'))) {
            if (model.get(this.get('keyLocation'))[this.get('attrLocation')]) {
                var page = model.get(this.get('keyLocation'))[this.get('attrLocation')];
                if (page && page != "") {
                    this.log("%cSCORM changeLocation " + page, "color: green;");
                    doLMSSetValue("cmi.core.lesson_location", page);
                    doLMSCommit();
                } else {
                    this.log("%cSCORM not location ", "color: red;");
                }
            } else {
                this.log("%cSCORM attrLocation :" + this.get('attrLocation') + " not found", "color: red;");
            }
        } else {
            this.log("%cSCORM keyLocation :" + this.get('keyLocation') + " not found", "color: red;");
        }
    }
});