var goldfish = goldfish || {};

/** Represents an instantiated component
* @author Mats Fjellner
* @augments Backbone.Model
* @constructor
* @name Component
*/
goldfish.UserBlock = Backbone.Model.extend(/** @lends Component */ {
	log: false ? console.log.bind(window.console) : function() { },
	defaults: {
		title: 'Untitled',
		body: "default block",
		data: '',
		instanceOf: false, // Id of component blueprint this instances
		css: [],
		cssQueries: false,
		repeatable: '',
		position: 0,
		group: 0,
		blocks: false,
		dropZone: false,
		dropZones: [],
		main: false,
		type: 1,
		eventHandlers: []
	},
	/** Creates collection of child components on initialization, creates CSS query class
	* @construct
	*/
	initialize: function(e) {
		this.log("%cUSER BLOCK MODEL INITIALIZE "+this.cid,"font-size: 30px;");
		this.log(this.attributes);

		if (!this.attributes.blocks) {
			this.attributes.blocks = new goldfish.UserBlocksCollection();	
		}
		// this.log(this.attributes);
		if (!this.get('main')) {
			this.set('main', (this.get('instanceOf') == 1));	
		}

		if (this.get("data")) {
			if (this.get("data").eventHandlers) {
				this.set("eventHandlers", this.get("data").eventHandlers);
			}
		}
		this.log("%cChecking cssQueries","font-size: 30px;");
		if (!this.attributes.cssQueries) {
			this.log("%cNo cssQueries","font-size: 30px;");
			// this.log(this.attributes.css);
			// if (!(this.attributes.css instanceof Array) ) {
			// 	this.attributes.css = Object.values(this.attributes.css);
			// }
			this.log(this.attributes.css);
			this.attributes.cssQueries = new goldfish.CssQueriesCollection();
			// Create CssQuery objects corresponding to CSS from API

			// for (var css in this.attributes.css) {
			// 	if (this.attributes.css.hasOwnProperty(css)) {
			// 		this.log("%cAdding css query","font-size: 30px;");
		    //     	this.attributes.cssQueries.add(
		    //     		new goldfish.CssQuery( {
		    //     			mediaQuery: this.attributes.css[css].media_query, 
			// 				body_css: this.attributes.css[css].body_css, // Base CSS
			// 				body_css_custom: this.attributes.css[css].body_css_custom, // Real CSS
		    //     			instanceOf: this.attributes.css[css].instanceOf // Mediaquery blueprint ID
		    //     		} ) );
			// 	}
			// }

			for (i=0; i < this.attributes.css.length; i++) {
				this.log("%cAdding css query","font-size: 30px;");
		        	this.attributes.cssQueries.add(
		        		new goldfish.CssQuery( {
		        			mediaQuery: this.attributes.css[i].media_query, 
							body_css: this.attributes.css[i].body_css, // Base CSS
							body_css_custom: this.attributes.css[i].body_css_custom, // Real CSS
		        			instanceOf: this.attributes.css[i].id // Mediaquery blueprint ID
		        		} ) );
		    }	
		}
		
	    this.listenTo(this.attributes.cssQueries, "change", function() {
			this.trigger("change");
		}); 

		this.listenTo(this.attributes.cssQueries, "css-change", function() {
			this.trigger("css-change");
		}); 

		this.listenTo(this.attributes.blocks, "add change remove", function() {
			this.log("Model: blocks change");
			this.trigger("change");
		}); 

		this.on("change", function() {
			this.log("%cuserblock "+this.cid+" model changed","color: red; font-size: 20px");
			this.log(this.attributes);
		});

	},
	/** Returns the number of drop zones in the component
	* @return {Boolean} 
	*/
	droppables: function() {
		var droppables = goldfish.Helpers.stringN(this.get('body'), '<blocks />');
		return droppables;
	},
	hasConfig: function() {
		return (this.get("data") && this.get("data").config);
	},
	addBlock: function(aBlock, group, position, index) {
		var me = this;
		var userBlock;
		this.log("adding user block at "+group+","+position+","+index);
		this.log(aBlock);
		this.attributes.blocks.each( function(block) {
			var idx = block.get('position');
			me.log(block);
			me.log(index);

			me.log( block.get('group') );
			me.log( group );
			me.log( idx );
			me.log( position );
			
			if (block.get('group')*1 == group*1 && idx*1 >= position*1) {
				me.log("moving element down");
				block.set({position: idx+1}, {silent: true}); // Move existing elemenents in this zone below this spot one step down
			}
		});
		this.log("setting new block group and position");
		aBlock.set({group: group, position: position},{silent: true});
		this.log("adding block to parent");
		this.attributes.blocks.add(aBlock, {at: index});
		this.log("%cBLOCKS",'font-size: 30px;');
		this.log(this.attributes.blocks.models);
	},
	removeBlock: function(el) {
		// var view = this.attributes.blockViews[el.cid];
		var index = this.attributes.blocks.indexOf(el);

		// goldfish.app.unregisterUserBlockView(el.cid);

		// delete this.attributes.blockViews[el.cid];
		this.attributes.blocks.remove(el);

		if (this.droppables() > 0) {
			var tmp = this.attributes.body.split('<block />');
			tmp.splice(index, 1);
			tmp = tmp.join('<block />');
			this.set('body', tmp, {silent: true});
		}

		return el;
	},
	getBlocks: function() {
		return this.attributes.blocks;
	},
	toggleCSS: function() {
		this.trigger("toggle-css");
		this.log("Toggle CSS");
	},
	toggleConfig: function() {
		this.trigger("toggle-config");
		this.log("Toggle Config");
	},
	// getBlockView: function(aBlock) {
	// 	return this.getBlockViewByCid(aBlock.cid);
	// },
	getCSSRule: function(aCid) {
		var rule = false;
		this.attributes.cssQueries.each( function(el) {
			var rules = el.get('rules');
			rules.each( function(el2) {
				if (el2.cid == aCid) {
					rule = el2;
				}
			});
			
		});
		return rule;
	},
	// getBlockViewByCid: function(aCid) {
	// 	// this.log("getBlockViewByCid "+aCid);
	// 	var tmp = this.attributes.blockViews[aCid];

	// 	if (typeof tmp === 'object') {
	// 		return tmp;
	// 	} else {
	// 		for (var attr in this.attributes.blockViews) {
	//         	if (this.attributes.blockViews.hasOwnProperty(attr)) {
	//         		console.trace();
	//         		tmp = this.attributes.blockViews[attr].getBlockViewByCid(aCid);
	//         		if (tmp) {
	//         			return tmp;
	//         		}
	//         	}
	//     	}
	//     	return false;
	// 	}
		
	// },
	parse: function(response, xhr) {
		this.log("%cUSER BLOCK MODEL PARSE"+this.cid,"font-size: 30px;");
		this.log(response);
		if (response.blocks && response.blocks.length > 0) {
			this.attributes.blocks = this.attributes.blocks ? this.attributes.blocks : new goldfish.UserBlocksCollection();	
			this.attributes.blocks.set(response.blocks, {parse: true});
		}

		this.log(this.attributes.cssQueries);
		if (response.type !== 2 && response.cssQueries && response.cssQueries.length > 0) {
			this.attributes.cssQueries = this.attributes.cssQueries ? this.attributes.cssQueries : new goldfish.CssQueriesCollection();
			this.attributes.cssQueries.set(response.cssQueries, {parse: true});
		}
		this.log(this.attributes.cssQueries);

		// var blocks = new goldfish.UserBlocksCollection();
		// var cssQueries = new goldfish.CssQueriesCollection();

		return {
			body: response.body,
			data: response.data,
			title: response.title,
			instanceOf: response.instanceOf*1, // Ensure integer - multiplication faster than parseInt
			position: response.position*1, 
			css: response.css,
			group: response.group*1, 
			repeatable: response.repeatable,
			blocks: this.attributes.blocks,
			cssQueries: this.attributes.cssQueries,
			id: response.id,
			type: response.type,
			url: response.url,
			main: response.instanceOf*1 == 1
		};
	},
	toJSON: function() {
		this.log("%cUSER BLOCK MODEL TOJSON "+this.cid,"font-size: 30px;");
		var json = {
			instanceOf: this.get('instanceOf'),
			body: this.get('body'),
			data: this.get('data'),
			title: this.get('title'),
			repeatable: this.get('repeatable'),
			blocks: this.get('blocks').toJSON(),
			cssQueries: this.get('cssQueries').toJSON(),
			cid: this.cid,
			position: this.get('position'),
			group: this.get('group'),
			type: this.get('type'),
			url: this.get('url'),
			viewMode: false
		};
		if (this.get('id')) {
			json.id = this.get('id');
		}
		this.log("%cBlock JSON:","color: green");
		this.log(json);
		return json;
	}
});