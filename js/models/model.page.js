var goldfish = goldfish || {};

/** Represents a learning unit page
* @author Mats Fjellner
* @augments Backbone.Model
* @constructor
* @name Page
*/
goldfish.Page = Backbone.Model.extend( /** @lends Page */ {
	log: true ? console.log.bind(window.console) : function() { },
	defaults: {
		title: '',
		body: "<html><head></head><body>default model page</html>",
		updated: false,
		showMenu: false,
		block: false,
		pages: false,
		render_html: '',
		render_css: ''
		// blockView: false,
		// blocks: false,
		// blockViews: {}
	},
	/** Creates collection of available projects on initialization, creates default user component
	* @construct
	*/
	initialize: function(e) {
		this.log("%cPAGE INITIALIZE "+this.cid,"font-size: 30px;");
		this.on("change", function() {
			this.log("%cPage change!","color: green; font-size: 30px;");
			this.updated = true; 
		});

		if (!this.attributes.pages) {
			this.attributes.pages = new goldfish.PagesCollection();	
		}

		// this.log("init Page model");

		if (!this.attributes.block) { // Set bloque base con vista
			this.log("%cpage has no block!",'color: red');
			var defaultBlock = goldfish.app.getUnit().getDefaultBlock();
			this.log(defaultBlock);
			// var dropBlock = goldfish.app.getDropBlock();
			this.attributes.block = new goldfish.UserBlock( defaultBlock.toJSON() );
			this.log(this.attributes.block);
			// this.attributes.block.addBlock( dropBlock, 0 );
			// this.attributes.blockView = new goldfish.UserBlockView( {model: this.attributes.block } );
		} else {
			this.log("page has block(s)");
		}

		// this.log("init Page model end");
	},
	isUpdated: function(e) {
		return this.updated;
	},
	// addBlock: function(aBlock, index) {
		// var userBlock = new goldfish.UserBlock(aBlock.copy());

		// var userBlockView = new goldfish.UserBlockView({
	 //   		model: userBlock
	 //   	});
		// this.attributes.blocks.add(userBlock,{at: index});
		// this.attributes.blockViews[userBlock.cid] = userBlockView;
		// this.trigger("change");
	// },
	// getBlocks: function() {
	// 	return this.attributes.blocks;
	// },
	// getBlockView: function(aBlock) {
	// 	return this.getBlockViewByCid(aBlock.cid);
	// },
	
	// save: function(e) {
	// 	if (this.updated) {
	// 		this.log(this.get('title')+" updated");
	// 		this.updated = false;
	// 	}
	// }
	save: function (attrs, options) { 
		this.log("saving page ");
	    attrs = attrs || this.toJSON();
	    this.log("page: got JSON");
	    options = options || {};
	    
	    // If model defines serverAttrs, replace attrs with trimmed version
	    if (this.serverAttrs) attrs = _.pick(attrs, this.serverAttrs);
	    // options.error = function(model, response, options) { 
     //        this.log("%cpage: save error!",'color: red'); 
     //        this.log(response);
     //     };
     //    options.success = function(model, response, options) { 
     //        this.log("%cpage: save success!",'color: green');             
     //        this.log(response);
     //     };
	    // Move attrs to options
	    options.attrs = attrs;
	    this.log(options.attrs);
	    // Call super with attrs moved to options
	    Backbone.Model.prototype.save.call(this, attrs, options);
	},
	/** Registers user component view
	* @param {integer} id - View id
	* @param {Object} View - Component View
   */
	registerUserBlockView: function(aCid, aView) {
		this.trigger("blockRegister",aCid,aView);
	},
	/** Unregisters user component view
	* @param {integer} id - View id
   */
	unregisterUserBlockView: function(aCid) {
		this.trigger("blockUnregister",aCid);
	},
	/** Returns page within this and child pages
	* @param {integer} cid - View cid
	* @return {Object} Attributes
	*/
	findPage: function(aCid) {
		if (this.cid == aCid) {
			this.log("found "+aCid);
			return this;
		} else {
			if (this.get("pages").length > 0) {
				var subPageFind = false;
				this.get("pages").each(function(model){
					if (!subPageFind) {
						subPageFind = model.findPage(aCid);
					}
				});	
				return subPageFind;
				
			} else {
				return false;
			}
		}
	},
	/** Adds child page, new last position
	* @param {Object} attrs - Child page attributes OR a model
	 */
	addChild: function(attrs) {
		var isModel = attrs instanceof Backbone.Model;
		var itemModel;
		var position = this.get("pages").length; // Agregar al final
		if (isModel) {
			this.log("model add");
			itemModel = attrs;
			this.log(itemModel);
		} else {
			this.log("model create");
			itemModel = new goldfish.Page(attrs);
		}
		itemModel.set('position', position);
		this.get("pages").add(itemModel);

	},
	/** Removes child page, rearranges child positions
	* @param {integer} aCid - Child page cid
	 */
	removeChild: function(aCid, options) {
		options = options || {};
		var child = this.findPage(aCid);
		var childPos = child.get("position");
		var childModel = this.get("pages").remove(child, options);
		this.get("pages").forEach( function(aPage) {
			var pos = aPage.get("position");
			if (pos > childPos) {
				aPage.set("position", pos - 1);
			}
		});
		return childModel;

	},
	/** Parses server response
	* @return {Object} Attributes
	*/
	parse: function(response, xhr) {
		this.log("%cPage parse!", "color: #0ff; font-size: 30px;");
		this.log(response);
		var block;
		if (!this.attributes.block) {
			this.log("blockless page, adding");
			this.attributes.block = new goldfish.UserBlock({main: true});
		} else {
			this.log("modifying existing block");
		}
		
		if (response.block) {
			this.log("setting block");
			this.attributes.block.set(this.attributes.block.parse(response.block));
		}

		if (!this.attributes.pages) {
			this.attributes.pages = new goldfish.PagesCollection();
		}
		if (response.pages && response.pages.length > 0) {
			this.attributes.pages.set(response.pages, {parse: true});
		}

		// this.log(block);
		return {
			title: response.title,
			id: response.id,
			cid: response.cid,
			position: response.position,
			block: this.attributes.block,
			pages: this.attributes.pages
		};
	},
	/** JSON representation including base component
	* @return {JSON} JSON
	*/
	toJSON: function() {
		var json = {
			title: this.get('title'),
			// render_html: this.get('render_html'),
			block: this.get('block').toJSON(),
			cid: this.cid,
			position: this.get("position"),
			pages: this.get('pages').toJSON()
		};

		if (this.get('id')) {
			json.id = this.get('id');
		}
		return json;
	}
});