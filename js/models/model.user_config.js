var goldfish = goldfish || {};

goldfish.UserConfig = Backbone.Model.extend( {
	urlRoot: function() {
		return '/api/user/'+goldfish.app.getUser().get("id")+'/config/';
	},
	defaults: {
		theme: 'dark',
		editor: 'ck',
		effects: 1
	},
	initialize: function() {
		
	}
} );