var goldfish = goldfish || {};

/** Represents a user
* @author Mats Fjellner
* @augments Backbone.Model
* @constructor
* @name User
*/
goldfish.User = goldfish.UserBase.extend( /** @lends User */ {
	urlRoot: './slim-api/public/user',
	defaults: {
		names: 'Nombre',
		surnames: 'Apellidos',
		profile: false,
		auth: '',
		activeProject: '',
		activeFile: ''
	},
	/** Creates collection of available projects on initialization
	* @construct
	*/
	initialize: function(initialModels) {
		this.attributes.projects = new goldfish.ProjectsCollection(initialModels.projects);
		this.attributes.files 	 = new goldfish.FilesCollection(initialModels.files);
		this.on("change", function() {
			goldfish.log("%cUser changed",'font-size: 20px; color: green');
			goldfish.log(this.attributes);
		});
	},

	hasProject: function(projectId) { // TODO
		return true;
	},
	/** Gets Project by Id
	* @param {integer} id
	* @return {Object} Project
	*/
	getProject: function(projectId) { // TODO
		return this.attributes.projects.findWhere({id: projectId});
	},
	/** Gets File by path hash md5 name file or folder
	* @param {string} pathHash
	* @return {Object} Files
	*/
	getFile: function(pathHash) {
		return this.attributes.files.findWhere({path_hash: pathHash});
	},
	/** JSON representation including basic attributes of each project
	* @return {JSON} JSON
	*/
	toJSON: function() {
		var projects = [];
		var files 	 = [];
		// HACK ALERT! el each siguiente es temporal
		// goldfish.log("%cPROJECTS "+this.attributes.projects.length,'font-size: 40px');
		this.attributes.projects.each( function(item, i) {
			// goldfish.log("%cCounted project",'font-size: 20px');
			projects[i] = {
				id: item.get("id"),
				title: item.get("title"),
				description: item.get("description"),
				remote_auth: item.get("remote_auth"),
				roles: item.get("roles"),
			};
			// goldfish.log(projects);
		});
		// var units = this.attributes.units.toJSON(); // .toJSON();
		// goldfish.log(projects);
		// goldfish.log(pages);

		this.attributes.files.each( function(item, i) {
			files[i] = {
				id: item.get("id"),
				path: item.get("path"),
				path_hash: item.get("path_hash"),
				name: item.get("name"),
				children: item.get("children")
			};
		});
		return {
			names: this.get('names'),
			surnames: this.get('surnames'),
			profile: this.get('profile'),
			id: this.get('id') || this.cid,
			activeProject: this.get('activeProject') !== '' ? {
				id: this.get('activeProject').get('id'),
				title: this.get('activeProject').get('title')
			} : '',		

			activeFile: this.get('activeFile') !== '' ? {
				id: this.get('activeFile').get('id'),
				path: this.get('activeFile').get('path'),
				path_hash: this.get('activeFile').get('path_hash'),
				name: this.get('activeFile').get('name')
				} : '',

			projects: projects,
			files: files[0]
		};	
	}
	// parse: function(response) {
	// 	goldfish.log("User parse");
	// 	goldfish.log(response);
	// }
} );