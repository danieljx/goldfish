var goldfish = goldfish || {};

goldfish.Block = Backbone.Model.extend({
	defaults: {
		title: 'Untitled',
		body: "<html><head></head><body>default block</html>",
		css: [],
		hollow: false,
		editOnly: false,
		selectable: true,
		dropZone: false,
		type: 1,
		url: false
	},
	initialize: function (e) {
		// console.log("%cGot block with body "+this.attributes.body,"color: green");
		// console.log(this.attributes);
		this.on("change", function () {
			// goldfish.log("model changed");
			this.updated = true;
		});
		this.attributes.body = this.attributes.body;

		if (this.attributes.title === '') {
			this.attributes.title = Date.now();
		}
	},
	copy: function (obj) {
		if (obj === undefined) {
			obj = this.attributes;
		}
		var tmp = {};
		for (var attr in obj) {
			if (obj.hasOwnProperty(attr)) {
				if (attr == 'id') {
					tmp.instanceOf = obj[attr];
				} else {
					var value = obj[attr];
					if (typeof value == "object") {
						value = this.copy(value);
					}
					tmp[attr] = value;
				}
			}
		}
		return tmp;

	},
	isUpdated: function (e) {
		return this.updated;
	},
	save: function (e) {
		if (this.updated) {
			// goldfish.log(this.get('title')+" updated");
			this.updated = false;
		}
	},
	toJSON: function (e) {
		// console.log("%cBLOCK MODEL TOJSON "+this.cid,"font-size: 30px;");
		var json = {
			instanceOf: this.get('id'),
			body: this.get('body'),
			data: this.get('data'),
			repeatable: this.get('repeatable'),
			blocks: this.get('blocks'),
			css: this.get('css'),
			cid: this.cid,
			type: this.get('type'),
			url: this.get('url')
		};

		// console.log(json);

		return json;
	}
});