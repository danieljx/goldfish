var goldfish = goldfish || {};

/** File Representation
* @author Daniel Villanueva @danieljx
* @augments Backbone.Model
* @constructor
* @name Files
*/
goldfish.File = goldfish.FileBase.extend({
	parse: function(response, options) {
		this.attributes.children = new goldfish.FilesCollection();
		this.attributes.children.set(response.children, {parse: true});
		this.attributes.shared_user = new goldfish.FilesShareCollection();
		this.attributes.shared_user.set(response.shared_user, {parse: true});
		this.attributes.shared = new goldfish.FileShare();
		this.attributes.shared.set(response.shared, {parse: true});
		this.attributes.shared_user.set(response.shared_user, {parse: true});
		this.attributes.tags = new goldfish.FilesTagCollection();
		this.attributes.tags.set(response.tags, {parse: true});
		return {
			'id': response.id,
			'path': response.path,
			'path_hash': response.path_hash,
			'name': response.name,
			'parent': response.parent,
			'size': response.size,
			'mtime': response.mtime,
			'encrypted': response.encrypted,
			'storage': response.storage,
			'storage_mtime': response.storage_mtime,
			'permissions': response.permissions,
			'mimetype':response.mimetype,
			'icon': response.icon,
			'children':  this.attributes.children,
			'shared_user':  this.attributes.shared_user,
			'shared':  this.attributes.shared,
			'tags':  this.attributes.tags,
			'breadcrumb': response.breadcrumb,
			'view': response.view,
			'color': response.color,
			'edit': false,
			'checked': false,
			'fileBlob': null
		};
	},
    validateNameFolder: function(attrs, options) {
		var ascii = /^[ -~]+$/;
		if (attrs.path && !ascii.test(attrs.path)) {
			return "Sin carácteres raros";
		}

		if (!attrs.path || (attrs.path < 5)) {
			return "Nombre demasiado corto";
		}
	},
	downloadFile: function(opt) {
		var link 		= document.createElement('a');
		link.href 		= '/api/user/' + goldfish.app.getUser().get("id") + '/file/' + (opt.sharedView ? 'shared/' : '') + (opt.publicView ? 'public/' : '') + 'download/' + this.get('path_hash');
		link.download 	= this.get("name");
		link.style 		= "display: none";
		document.body.appendChild(link);
		link.click();
		document.body.removeChild(link);
	},
	downloadBlobFile: function(opt) {
		if(this.get("fileBlob")) {
			this.saveBlobFile();
		} else {
			var me = this;
			$.ajax({
				url: '/api/user/' + goldfish.app.getUser().get("id") + '/file/' + (opt.sharedView ? 'shared/' : '') + (opt.publicView ? 'public/' : '') + 'download/' + this.get('path_hash'),
				async: true,
				contentType: false,
				processData: false,
				xhrFields: {
					responseType: 'blob'
				},
				success: function(fileBlob, status, xhr) {
					me.set("fileBlob", fileBlob);
					me.saveBlobFile();
					if(opt.success) {
						opt.success(me, fileBlob, me.options);
					}
				},
				error: function(response, status, xhr) {
					if(opt.error) {
						opt.error(me, response, me.options);
					}
				}
			});
		}
	},
	saveBlobFile: function() {
		if (typeof window.navigator.msSaveBlob === 'function') {
			window.navigator.msSaveBlob(this.get("fileBlob"), this.get("name"));
		} else {
			var link			= document.createElement('a');
				link.href		= this.get("fileBlob");
				link.download 	= this.get("name");
				link.style 		= "display: none";
				document.body.appendChild(link);
				link.click();
				window.URL.revokeObjectURL(link.href);
		}
	}
});
