var goldfish = goldfish || {};

/** File Representation
* @author Daniel Villanueva @danieljx
* @augments Backbone.Model
* @constructor
* @name FileTag
*/
goldfish.FileTag = Backbone.Model.extend( {
	urlRoot: function() {
		return '/api/user/'+goldfish.app.getUser().get("id")+'/file/tags/';
	},
  defaults: {
      'id': undefined,
      'file_id': undefined,
      'tag_id': undefined,
      'type': 1,
      'status': 1,
      'name': undefined,
      'key': undefined,
			'tag_action': 'add'
  },
	initialize: function(initialModels) {

	},
	parse: function(response, options) {
		return {
			'id': response.id,
			'file_id': response.file_id,
			'tag_id': response.tag_id,
			'type': response.type,
			'status': response.status,
			'name': response.name,
			'key': response.key,
			'tag_action': 'up'
		};
	},
  fetch: function(options) {
        return Backbone.Model.prototype.fetch.call(this, options);
	}
});
