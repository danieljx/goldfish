var goldfish = goldfish || {};

goldfish.UnitFormat = Backbone.Model.extend( {
	urlRoot: function() {
		return '/api/formats/';
	},
	defaults: {
		title: '',
		id: 0,
		name: '',
		description: '',

	},
	initialize: function() {
		
	}
} );