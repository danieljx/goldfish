var goldfish = goldfish || {};

goldfish.CssRule = Backbone.Model.extend({
	log: false ? console.log.bind(window.console) : function() { },
	defaults: function(obj) {
		return {
		selectors: '',
		body_css: [],
		body_css_custom: [],
		custom_css: '',
		parsed: '',
		attrList: [],
		usedAttrs: []
		};
	},
	initialize: function(e) {
		this.log("%cINIT CSS RULE","font-size: 30px;"); 

		this.listenTo(this, "change:body_css", function() {
			this.log("%ccss rule change!","font-size: 30px; background-color: white; color: black;");
			this.updateTheme();
		});

		this.listenTo(this, "remove", function() {
			this.updated = true;
			this.log("%ccss rule remove!","font-size: 30px; background-color: white; color: black;");
		});

		this.attributes.cssAttributes = new goldfish.CssAttributesCollection();

		this.listenTo(this.get("cssAttributes"),"change remove", function() {
			this.log("%ccssAttributes change!","font-size: 30px; background-color: white; color: black;");
			this.trigger("change");
		});

		this.log("%cNEW CSS RULE", 'font-size: 30px; color: green');
		this.createAttributes();
		
		
		return this;
	},
	updateTheme: function() {
		for (var cssAttribute in this.get("body_css")) {
			if (this.get("body_css").hasOwnProperty(cssAttribute) && (cssAttribute.substr('0','1') !== '_') ) {
				var newAttrs = {
					defaultValue: this.attributes.body_css[cssAttribute]
				};
				
				var attribute = this.get("cssAttributes").findWhere({name: cssAttribute});
				if (!attribute.get("isCustom")) {
					newAttrs.customValue = newAttrs.defaultValue;
					attribute.set(newAttrs);
				} else {
					attribute.set(newAttrs, { silent: true });
				}
			}
		}

	},
	createAttributes: function() {
		var cssAttributes = {};
		this.log("%cCSS RULE UPDATE","font-size: 30px; color: yellow");
		for (var cssAttribute in this.attributes.body_css) {
			if (this.attributes.body_css.hasOwnProperty(cssAttribute) && (cssAttribute.substr('0','1') !== '_') ) {
				var value = this.attributes.body_css[cssAttribute];
				var custom = false;

				if (this.attributes.body_css_custom.hasOwnProperty(cssAttribute) && (cssAttribute.substr('0','1') !== '_')) {
					custom = true;
					value = this.attributes.body_css_custom[cssAttribute];
				}

				if (this.get("usedAttrs").indexOf(cssAttribute) == -1) {
					this.log("%cPushing "+this.cid+" "+this.get("selectors")+": "+cssAttribute,"color: #5f0;");
					this.get("usedAttrs").push(cssAttribute);
				}
				
				this.attributes.cssAttributes.add( 
					new goldfish.CssAttribute( {
						name: cssAttribute,
						defaultValue: this.attributes.body_css[cssAttribute],
						customValue: value,
						isCustom: custom
					} ) 
				);
			}
		}

		for (cssAttribute in this.attributes.body_css_custom) {
			if (this.attributes.body_css_custom.hasOwnProperty(cssAttribute) && (cssAttribute.substr('0','1') !== '_') && !this.attributes.body_css.hasOwnProperty(cssAttribute)) {


				if (this.get("usedAttrs").indexOf(cssAttribute) == -1) {
					this.log("%cPushing custom "+this.cid+" "+this.get("selectors")+": "+cssAttribute,"color: #5f0;");
					this.get("usedAttrs").push(cssAttribute);
				}
					
				this.attributes.cssAttributes.add( 
					new goldfish.CssAttribute( {
						name: cssAttribute,
						defaultValue: false,
						customValue: this.attributes.body_css_custom[cssAttribute],
						isCustom: true
					} ) 
				);
				
			}
		}

		cssAttributes = this.get('cssAttributes');
		if (cssAttributes.length > 0) {
			this.set("attrList", cssAttributes.at(0).getAttrList());
		} else {
			var tmp = new goldfish.CssAttribute();
			this.set("attrList", tmp.getAttrList());
			tmp.trigger('destroy', tmp);
		}
	},
	addAttribute: function(name) {
		this.log("Adding attribute");
		var newModel = this.get("cssAttributes").add(
			new goldfish.CssAttribute( {
				name: name,
				isCustom: true
			}, { silent: true } )
			
		);
		
		this.log("Model add");
		this.trigger("add-attribute", newModel);
	},
	availableAttributes: function() {
		
		var tmp = this.get("attrList").slice();
		this.log("%cAvailable attributes "+this.cid,"color: #5f0;");
		this.log(tmp);
		this.log(this.get("usedAttrs"));
		for (var i = 0; i < this.get("usedAttrs").length; i++) {
			tmp.splice(tmp.indexOf(this.get("usedAttrs")[i]), 1);
		}
		this.log(tmp);
		return tmp;
	},
	setAttribute: function(attr,val) {
		this.attributes.cssAttributes[attr] = val;
		this.attributes.parsed = this.toStr();
		this.trigger("change");		
	},
	_splitTrim: function(aStr,aSplitter) {
		var a = aStr.split(aSplitter);
		for (var j = 0; j < a.length; j++) {
			a[j] = a[j].trim();
		}
		return a;
	},
	toStr: function() {
		var tmp = ' '+this.attributes.selectors+'{';
		for (var attr in this.attributes.cssAttributes) {
	        if (this.attributes.cssAttributes.hasOwnProperty(attr)) {
	        	tmp += attr + ':' + this.attributes.cssAttributes[attr]+';';
	        }
	    }
	    tmp += '}';
	    return tmp;
	},
	isUpdated: function(e) {
		return this.updated;
	},
	save: function(e) {
		if (this.updated) {
			this.updated = false;
		}
	},
	getCSSwithId: function(aCid) {
		// this.log("%cCSS RULE GET", "font-size: 30px; color: #f0f;");
		// this.log(this.attributes);
		// this.log(this.toJSON());
		var str = ' '+this.get('selectors') + ' { ';
		this.get('cssAttributes').each( function(attr) {
			str += attr.getCSSwithId(aCid);
		});
		str += '} ';

		return str;
	},
	parse: function(response,xhr) {
		// this.log("%cCSS RULE PARSE", "font-size: 30px; color: #f0f;");
		return {
			cssAttributes: response.cssAttributes ? response.cssAttributes : this.attributes.cssAttributes,
			// cssAttributes: this.attributes.cssAttributes ? this.attributes.cssAttributes : response.cssAttributes,
			parsed: response.parsed.trim(),
			selectors: response.selectors
		};
	},
	toJSON: function() {
		// this.log("%cCSS RULE TOJSON", "font-size: 30px; color: #f0f;");
		var json = { 
			selectors: this.get('selectors'),
			properties: this.get('cssAttributes').toJSON()
		};
		if (this.get('id')) {
			json.id = this.get('id');
		}
		return json;
	}
	
});