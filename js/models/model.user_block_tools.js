var goldfish = goldfish || {};

goldfish.UserBlockTools = Backbone.Model.extend( {
	defaults: {
		cssEdit: false,
		htmlEdit: false,
		draggable: false,
		comments: false

	},
	initialize: function() {
	}
	
} ); 