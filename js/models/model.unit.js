var goldfish = goldfish || {};

/** Represents a learning unit
* @author Mats Fjellner
* @augments Backbone.Model
* @constructor
* @name Unit
*/
goldfish.Unit = Backbone.Model.extend( /** @lends Unit */
 {
 	/** Sets API base URL based on App.getUser() and App.getProject
	* @return {string} URL
	*/
	log: true ? console.log.bind(window.console) : function() { },
	urlRoot: function() {
		return '/api/user/'+goldfish.app.getUser().get("id")+'/project/'+goldfish.app.getProject().get("id")+'/unit';
	},
	defaults: {
		showInfo: true,
		showTemplates: true,
		editState: 0,
		page: false,
		saved: '',
		author: '', 
		title: '',
		theme: 0,
		blocks: false,
		themes: false,
		currentPage: false,
		showSecondaryMenu: false,
		data: {
			config: {
				outputNav: {
					type: 'select',
					value: 'd3',
					allValues: ['d3', 'accordeon']
				},
				transitionIn: {
					type: 'transition',
					value: 'scale'
				},
				transitionOut: {
					type: 'transition',
					value: 'scale'
				}
			}
		}
	},
	/** Creates collection of available pages on initialization
	* @construct
	*/
	initialize: function(initialModels) {
		this.log("%cunit initialize","color: green");
		this.log("%cUNIT INITIALIZE "+this.cid,"font-size: 30px;");
		// this.attributes.pages = new goldfish.PagesCollection(initialModels.pages);


		if (!this.attributes.blocks) {
			this.attributes.blocks = new goldfish.BlocksCollection();
			
			this.listenTo(this.attributes.blocks, "add remove", function() { 
				goldfish.log("block change");
			});
			
		}
		
		// if (!this.attributes.page) {
		// 	this.attributes.page = new goldfish.Page();
		// }
		// if (!this.attributes.pages) {
		// 	goldfish.log("unit:pages init");
		// 	this.attributes.pages = new goldfish.PagesCollection(initialModels.pages);
		// }
 
		// if (this.attributes.saved === '') {
		// 	this.setSaved();
		// }

		// this.on("change", this.setSaved() );

		// this.attributes.pages.on("sync", function() {
  //     	});
      	this.listenTo(this, 'change', function() {
			this.log("%cUnit model changed!",'color: blue');
			this.log(this);
		}.bind(this));

		this.listenTo(this, 'change:themes', function() {
			this.log("%cThemes changed!",'color: blue');
			this.log(this.get("themes"));
		}.bind(this));

      	this.listenTo(this, 'change:theme', function() {
      		
      	});
      	
      	// goldfish.log("unit model ok");
      	
	},
	// fetch: function(options) {
	// 	return Backbone.Model.prototype.fetch.call(this, options);  

	// },
	fetchComponents: function(options) {
		// this.get("blocks").url = this.urlRoot()+this.get('id')+'/components';
		this.get("blocks").url = '/api/user/'+goldfish.app.getUser().get("id")+'/project/'+goldfish.app.getProject().get("id")+'/theme/'+this.get('theme')+'/components';
		this.get("blocks").fetch(options);
	},
	/** Gets all available components
	* @return {Collection} Components
   */
	getBlocks: function() {
		return this.attributes.blocks;
	},
	/** Gets all available component views
	* @return {Collection} ComponentViews
   */
	getBlockViews: function() {
		return this.attributes.blockViews;
	},
	/** Gets all available themes <em style="color: red;">Should this be here?</em>
	* @return {Collection} Components
   */
	getProjectThemes: function(options) {
		var themes = goldfish.app.getProject().getThemes(options);
		// goldfish.log(themes);
		// this.attributes.themes = new goldfish.ThemesCollection();
		// this.attributes.themes.set(themes.clone());
		// goldfish.log("Got themes:");
		// goldfish.log(this.get("themes"));
		// goldfish.log(this.get("themes").toJSON());
		// return this.get("themes");
	},

	/** Returns default base component
	* @return {Object} Component

   */
	getDefaultBlock: function() {
		return this.attributes.blocks.findWhere({name: "main"});
	},
	/** Returns drop block component
	* @return {Object} Component
   */
	getDropBlock: function() {
		// return this.attributes.dropBlock;
		return this.attributes.blocks.findWhere({name: "drop"});
	},
	/** Gets component view
	* @param {integer} id - View id
	* @return {Object} View - Component View
   */
	getBlockViewByCid: function(aCid) {
		return this.attributes.blockViews[aCid];
	},
	/** Toggles secondary view
	* @param {booleab} state - off/on
   */
	secondaryMenuViewToggle: function(state) {
		var newState;
		if (typeof(state) !== "undefined") {
			newState = state;
		} else {
			newState = !this.get("showSecondaryMenu");
		}
		this.set("showSecondaryMenu", newState);
   },

	/** Custom checks
	* @param {array} attrs
	* @param {Object} options
	* @return {string} Error message, if any
	*/
	validate: function(attrs, options) {
		var badChars = /[\\/\t<>%$]/; 

		if (!attrs.title || (attrs.title.length < 5)) {
			return "Título demasiado corto";
		}
		
		if (attrs.title && badChars.test(attrs.title)) {
			return "Sin carácteres raros";
		}

		
	},
	/** Sets saved date <em style="color: red">Should get from server instead</em>
	*/
	setSaved: function() {
		aDate = new Date();
		this.set('saved', aDate.getDate() + "/" + (aDate.getMonth()+1)  + "/" + aDate.getFullYear() + " @ " + aDate.getHours() + ":" + aDate.getMinutes() + ":" + aDate.getSeconds());
	},
	/** Gets pages in unit
	* @return {Collection} Pages
	*/
	getPages: function() {
		return this.attributes.page;
	},
	/** Gets pages in unit
	* @return {Object} Page
	*/
	setCurrentPage: function(aCid) {
		this.log("setCurrentPage "+aCid);
		var aPage = this.findPage(aCid);
		if (aPage) {
			this.set('currentPage',aPage);	
		}
		
	},
	/** Gets pages in unit
	* @return {Collection} Pages
	*/
	getCurrentPage: function() {
		return this.get('currentPage');
	},
	/** Gets page by index
	* @param {integer} index
	* @return {Object} Page
	*/
	getPageByIndex: function(index) {
		return this.attributes.pages.at(index);
	},
	findPage: function(aCid) {		
		return this.get("page").findPage(aCid);
	},
	/** Saves to server API with complete JSON representation of unit + pages and components
	*/
	save: function (attrs, options) { 
		this.log("%csaving unit","color: green");
	    attrs = attrs || this.toJSON();
	    options = options || {};

	    // If model defines serverAttrs, replace attrs with trimmed version
	    if (this.serverAttrs) attrs = _.pick(attrs, this.serverAttrs);
	    options.attrs = attrs;
	    Backbone.Model.prototype.save.call(this, attrs, options);
	},
	/** Parses server response and sets pages
	* @return {Object} Attributes + pages
	*/
	parse: function(response, xhr) {
		// if (response.pages.constructor === Array) {
		// 	goldfish.log("%cpages array","font-weight: bold");
		// }
		this.log("%cUnit parse!", "color: #0ff; font-size: 30px;");
		this.log(this.isNew());
		this.log(response);
		this.log(this.isNew());
		this.log(response.pages);
		if (response.pages) {
			this.log("has pages");
			this.attributes.page = this.attributes.page ? this.attributes.page : new goldfish.Page({root: true});
			this.attributes.page.set(this.attributes.page.parse(response.pages));
		} else {
			this.log("no pages");
			if (!this.isNew()) {
				this.attributes.page = new goldfish.Page({root: true, title: new Date().getTime(), position: 0});
			}
		}

		var parseObj = {
			title: response.title,
			id: response.id,
			saved: response.saved,
			author: response.author,
			data: response.data || this.defaults.data,
			// data: this.defaults.data,
			cid: response.cid,
			theme: response.theme
		};

		if (response.themes) {
			parseObj.themes = new goldfish.ThemesCollection( response.themes );
		}
		
		return parseObj;
	},
	/** JSON representation including id + title of units
	* @return {JSON} JSON
	*/
	toJSON: function() {
		this.log("%cunit toJSON","color: green");
		var json = {
			title: this.get('title'),
			saved: this.get('saved'),
			author: this.get('author'),
			data: this.get('data'),
			cid: this.cid,
			theme: this.get('theme'),
			pages: this.get('page') ? this.get("page").toJSON() : ''
		};
		if (this.get('id')) {
			json.id = this.get('id');
		}
		if (this.get('themes')) {
			json.themes = this.attributes.themes.toJSON();
		}
		return json;
	},
	pagesJSON: function() {

	}
} );


