var goldfish = goldfish || {};

/** File Representation
* @author Daniel Villanueva @danieljx
* @augments Backbone.Model
* @constructor
* @name FilesBase
*/
goldfish.FileBase = Backbone.Model.extend({
	urlRoot: function() {
		return '/api/user/'+goldfish.app.getUser().get("id")+'/file/';
	},
    defaults: {
        'id': undefined,
        'path': undefined,
        'path_hash': undefined,
        'parent': undefined,
        'size': 0,
        'mtime': undefined,
        'encrypted': false,
        'storage': undefined,
        'storage_mtime': undefined,
        'permissions': undefined,
        'mimetype': undefined,
        'icon': 'file',
        'children': {},
        'shared': {},
        'breadcrumb': {},
				'edit': false,
				'checked': false,
				'fileBlob': null,
				'share_others' : 1,
				'share_details' : 1
    }
});
