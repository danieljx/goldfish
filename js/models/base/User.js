var goldfish = goldfish || {};

/** File Representation
* @author Daniel Villanueva @danieljx
* @augments Backbone.Model
* @constructor
* @name UserBase
*/
goldfish.UserBase = Backbone.Model.extend({
	urlRoot: function() {
		return '/api/user';
	},
	defaults: {
		id: undefined,
		names: 'Nombre',
		surnames: 'Apellidos',
		profile: false,
		active: '',
		email: ''
    },
	parse: function(response, options) {
		return {
			'id': response.id,
			'names': response.names,
			'surnames': response.surnames,
			'profile': response.profile,
			'active': response.active,
			'email': response.email
		};
	}
});