var goldfish = goldfish || {};

/** File Representation
* @author Daniel Villanueva @danieljx
* @augments Backbone.Model
* @constructor
* @name ProjectBase
*/
goldfish.ProjectBase = Backbone.Model.extend({
	urlRoot: function() {
		return '/api/user/'+goldfish.app.getUser().get("id")+'/project/';
	},
	defaults: {
		id: undefined,
		title: '',
		description: '',
		admin: false,
		remote_auth: false
	},
	parse: function(response, options) {
		return {
			'id': response.id,
			'title': response.title,
			'description': response.description,
			'admin': response.admin,
			'remote_auth': response.remote_auth
		};
	}
});