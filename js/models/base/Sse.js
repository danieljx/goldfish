var goldfish = goldfish || {};

/** File Representation
* @author Daniel Villanueva @danieljx
* @augments Backbone.Model
* @constructor
* @name SseBase
*/
goldfish.SseBase = Backbone.Model.extend({
  constructor: function(attributes, options) {
  //  _.bindAll(this, '_receiveMessage', '_receiveError', '_receiveOpen');
    url = _.result(this, 'url');
    if (this._eventSource) {
      this.stop();
    }
    this._eventSource = new window.EventSource(url);
    this._eventSource.addEventListener('message', _.bind(this._receiveMessage, this), false);
    Backbone.Model.prototype.constructor.apply(this, arguments);
  },
  /*
  start: function(url) {
    url = url || _.result(this, 'url');
    if (this._eventSource) {
      this.stop();
    };
    this._eventSource = new window.EventSource(url);
    this._eventSource.addEventListener('message', _.bind(this._receiveMessage, this), false);
    //this._eventSource.addEventListener('error', _.bind(this._receiveError, this), false);
    //this._eventSource.addEventListener('open', _.bind(this._receiveOpen, this), false);
  },
  */
  stop: function() {
    if(this._eventSource) {
      this._eventSource.close();
      this._eventSource.removeEventListener('message', this._receiveMessage);
      this._eventSource.removeEventListener('error',   this._receiveError);
      this._eventSource.removeEventListener('open',    this._receiveOpen);
      this._eventSource = null;
    }
  },
  restart: function() {
    this.stop();
    this.start();
  },
  _receiveMessage: function(e) {
    console.log('_receiveMessage');
    console.log(e.lengthComputable);
    console.log(e.data);
      this.trigger('message', e.data);
		if (e.lengthComputable && e.data) {
      //this.trigger('message', data);
		}
	},
  _receiveError: function(e) {
    console.log('_receiveError');
    console.log(e);
		if (e.lengthComputable) {
      this.trigger('error', e);
    }
  },
  _receiveOpen: function(e) {
    console.log('_receiveOpen');
    console.log(e);
		if (e.lengthComputable) {
      this.trigger('error', this);
    }
  },
  noop : function(key) {
    return function() {
      throw key + ' is not available in Backbone.SSE';
    };
  },
  sync: function(key) {
    return this.noop('sync');
  },
  fetch: function(key) {
    return this.noop('fetch');
  },
  destroy: function(key) {
    return this.noop('destroy');
  },
  save: function(key) {
    return this.noop('save');
  }
});
