var goldfish = goldfish || {};

/** File Representation
* @author Daniel Villanueva @danieljx
* @augments Backbone.Model
* @constructor
* @name FileShare
*/
goldfish.FileShare = Backbone.Model.extend( {
	urlRoot: function() {
		return '/api/user/'+goldfish.app.getUser().get("id")+'/file/shared/';
	},
    defaults: {
        'id': undefined,
        'file_id': undefined,
        'owner_id': undefined,
        'shared_id': undefined,
        'initiator_id': undefined,
        'share_expiration': undefined,
        'share_type': undefined,
        'share_edit': undefined,
        'share_details': undefined,
        'share_others': undefined,
        'share_status': undefined,
        'share_name': undefined,
        'share_hash': undefined,
        'mail_send': true,
        'share_user': undefined,
        'share_project': undefined,
				'share_action': 'up'
    },
	initialize: function(initialModels) {

	},
	parse: function(response, options) {
		this.attributes.share_user = new goldfish.UserBase();
		this.attributes.share_user.set(response.share_user, {parse: true});
		this.attributes.share_project = new goldfish.ProjectBase();
		this.attributes.share_project.set(response.share_project, {parse: true});
		this.attributes.share_owner = new goldfish.UserBase();
		this.attributes.share_owner.set(response.share_owner, {parse: true});
		this.attributes.share_initiator = new goldfish.UserBase();
		this.attributes.share_initiator.set(response.share_initiator, {parse: true});
		return {
			'id': response.id,
			'file_id': response.file_id,
			'owner_id': response.owner_id,
			'shared_id': response.shared_id,
			'initiator_id': response.initiator_id,
			'share_expiration': response.share_expiration,
			'share_type': response.share_type,
			'share_edit': response.share_edit,
			'share_details': response.share_details,
			'share_others': response.share_others,
			'share_status': response.share_status,
			'share_name': response.share_name,
			'share_hash': response.share_hash,
			'mail_send': true,
			'share_user': this.attributes.share_user,
			'share_project': this.attributes.share_project,
			'share_owner': this.attributes.share_user,
			'share_initiator': this.attributes.share_project,
			'share_action': 'up'
		};
	},
    fetch: function(options) {
        return Backbone.Model.prototype.fetch.call(this, options);
	}
});
