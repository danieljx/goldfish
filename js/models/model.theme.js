var goldfish = goldfish || {};

goldfish.Theme = Backbone.Model.extend( {
	urlRoot: function() {
		return '/api/projects/'+goldfish.app.getProject().get("id")+'/themes/';
	},
	defaults: {
		title: 'Gaelic',
		id: 0,
		name: '',
		description: 'blablabla..',

	},
	initialize: function() {
		
	}
} );