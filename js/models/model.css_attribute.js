var goldfish = goldfish || {};

goldfish.CssAttribute = Backbone.Model.extend({
	log: false ? console.log.bind(window.console) : function() { },
	defaults: {
        type: '',
        name: '',
		defaultValue: false,
        customValue: '',
		isCustom: false,
		types: {
			length: ["margin","padding","top","left","bottom","right","height","width","font-size","border-width"],
			color: ["color","background-color","border-color"],
			font: ['font-family'],
			general: ["font-weight","box-shadow","position","display","opacity","border"]
		}
	},
	initialize: function(e) {
		// this.log("%cINIT CSS ATTRIBUTE", "color: green; font-size: 30px;");
		// this.log(this.attributes);

		this.listenTo(this, 'change', function() {
			// this.log("ATTRIBUTE CHANGED");
		}.bind(this));

		this.setType();

		return this;
	},
	getAttrList: function() {
		var attrList = [];
		for(var type in this.get("types")) {
			if (this.get("types").hasOwnProperty(type) ) {
				attrList = attrList.concat(this.get("types")[type]);
			}
		}
		return attrList;
	},
	setType: function() {
		for(var type in this.get("types")) {
			if (this.get("types").hasOwnProperty(type) && $.inArray( this.get("name"), this.get("types")[type] ) > -1 ) {
				this.set("type", type);
			}
		}
		if (this.get("type") === "") {
			this.log("%cNo type?","font-size: 30px; color: red");
			this.log(this.attributes);
		}
	},
	setValue: function(value,custom) {
		// this.log("Setting "+(custom ? "custom" : "default")+" to "+value);
        if (custom) {
            this.set({"customValue": value, "isCustom": true});
        } else {
            this.set({"isCustom": false});
        }
	},
	changeType: function(newType) {
		this.set("name", newType);
		this.setType();
		this.trigger("render");
	},
	getTypes: function() {
		return Object.keys( this.get("types") );
	},
	toStr: function() {
	    return this.get("name")+':'+(this.get("isCustom") ? this.get("customValue") : this.get("defaultValue")) + ";";
	},
	getCSSwithId: function(aCid) {
		// this.log("%cGetting CSS for attribute","color: #f00");
		// this.log(this.get("name")+':'+(this.get("isCustom") ? this.get("customValue") : this.get("defaultValue")) + ";");
		return this.get("name")+':'+(this.get("isCustom") ? this.get("customValue") : this.get("defaultValue")) + ";";
	},
	parse: function(response,xhr) { // No está en uso actualmente
		// this.log("%cCSS ATTRIBUTE PARSE", "font-size: 30px; color: #ff0;");
		return {
			editableCssAttributes: response.editableCssAttributes,
			cssAttributes: response.cssAttributes,
			parsed: response.parsed.trim(),
			selector: response.selector
		};
	},
	toJSON: function() {
		// this.log("%cCSS ATTRIBUTE TO JSON", "font-size: 30px; color: #ff0;");
		var json = {
			type: this.get('type'),
        	name: this.get('name'),
			defaultValue: this.get('defaultValue'),
        	customValue: this.get('customValue'),
			isCustom: this.get('isCustom')
		};
		// if (this.get("isCustom")) {
		// 	json[this.get('name')] = this.get('customValue');
		// }
		return json;
	}
	
});