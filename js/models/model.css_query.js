var goldfish = goldfish || {};

/** Instancia de un mediaquery CSS
* @author Mats Fjellner
* @augments Backbone.Model
* @constructor
* @name CssQuery
*/
goldfish.CssQuery = Backbone.Model.extend({/** @lends CssQuery */
	log: false ? console.log.bind(window.console) : function() { },
	defaults: {
		mediaQuery: '',
		rules: false,
		instanceOf: false,
		list: [],
		body_css: [],
		body_css_custom: []

	},
	/** Creates collection of available components on initialization
	* @construct
	*/
	initialize: function(e) {
		this.log("%cINIT CSS QUERY","font-size: 30px;"); 
		this.log(this.attributes);

		this.createRules();

		this.listenTo(this,"change:body_css", function() {
			this.log("%cCSS THEME CHANGE","font-size: 30px;"); 
			this.updateTheme();
		});

		// this.listenTo(this.get('body_css_custom'),"change", function() {
		// 	this.log("%cCSS QUERY CHANGE","font-size: 30px;"); 
		// 	this.updateRules();
		// 	this.updated = true;
		// });
		// goldfish.log("New CSS Query: "+this.attributes.rules.length+" rules");
		// goldfish.log(this.attributes.list);
		// this.attributes.parsed = this.toStr();
		return this;
	},
	updateTheme: function() {
		this.log("%cUPDATE THEME", "font-size: 30px; color: red;");
		var ruleSelectors = {};
		if (this.get("body_css")) {
			for (var i = 0; i < this.get("body_css").length; i++) { // Repasamos todas las reglas CSS base
				var selConcat = this.attributes.body_css[i].selectors.join(',');
				this.log(selConcat);
				if (ruleSelectors.hasOwnProperty( selConcat )) {
					ruleSelectors[selConcat].body_css = goldfish.Helpers.objMerge(ruleSelectors[selConcat].body_css, this.attributes.body_css[i].properties);
				} else {
					ruleSelectors[selConcat] = {
						'body_css': this.attributes.body_css[i].properties,
						'body_css_custom': {}
					};
				}
			}	
		}
		

		for (var selectors in ruleSelectors) {
			if (ruleSelectors.hasOwnProperty(selectors)) {
				console.log("Look for "+selectors+" in rules");
				console.log(this.get("rules"));
				var rule = this.get("rules").findWhere({ selectors: selectors });
				console.log(rule);
				rule.set({ body_css: ruleSelectors[selectors].body_css });
			}
		 }

	},
	createRules: function() {
		this.log("%cCSS QUERY: Updating rules","font-size: 30px;"); 
		this.log(this.attributes);
		var ruleSelectors = {};
		var selConcat;

			for (var i = 0; i < this.attributes.body_css.length; i++) { // Repasamos todas las reglas CSS base

				selConcat = this.attributes.body_css[i].selectors;

				if (ruleSelectors.hasOwnProperty( selConcat )) {
					this.log(this.attributes.body_css[i].properties);
					ruleSelectors[selConcat].body_css = goldfish.Helpers.objMerge(ruleSelectors[selConcat].body_css, this.attributes.body_css[i].properties);
				} else {
					ruleSelectors[selConcat] = {
						'body_css': this.attributes.body_css[i].properties,
						'body_css_custom': {}
					};
				}
			}

			for (i = 0; i < this.attributes.body_css_custom.length; i++) { // Repasamos todas las reglas CSS personalizadas

				selConcat = this.attributes.body_css_custom[i].selectors;

				if (ruleSelectors.hasOwnProperty( selConcat )) {
					ruleSelectors[selConcat].body_css_custom = goldfish.Helpers.objMerge(ruleSelectors[selConcat].body_css_custom, this.attributes.body_css_custom[i].properties);
				} else {
					ruleSelectors[selConcat] = {
						'body_css': {},
						'body_css_custom': this.attributes.body_css_custom[i].properties
					};
				}
			}

			this.log(ruleSelectors);

			if (!this.attributes.rules) {
				this.log("%cHAS NO CSS RULES","font-size: 30px; color: red;"); 
				this.attributes.rules = new goldfish.CssRulesCollection();
				this.listenTo(this.get("rules"),"change", function() {
					this.log("%cCSS RULE CHANGE","font-size: 30px;"); 
					this.trigger("css-change");
				});
				
				
			} else {
				this.log("%cHAS RULES","font-size: 30px; color: green;"); 
				
				
			}
			// this.attributes.rules.set(ruleSelectors);

			this.log(this.attributes.rules);

			
			

			for (var selectors in ruleSelectors) {
				if (ruleSelectors.hasOwnProperty(selectors)) {
					this.attributes.rules.add( new goldfish.CssRule( {
						selectors: selectors,
						body_css: ruleSelectors[selectors].body_css,
						body_css_custom: ruleSelectors[selectors].body_css_custom 
					} ) );
				}
			 }

		
	},
	isUpdated: function(e) {
		return this.updated;
	},
	save: function(e) {
		if (this.updated) {
			this.updated = false;
		}
	},
	getCSSwithId: function(aCid) {
		var str = '';
		this.log("*** CSSQUERY getCSSwithId");
		if (this.get('mediaQuery') !== '') {
			str += this.get('mediaQuery')+'{';
		}
		this.get('rules').each( function(el2) {
			str += ' .page #block-'+aCid+':not(.tools) '+el2.getCSSwithId(aCid);
		}.bind(this));

		if (this.get('mediaQuery') !== '') {
			str += '}';
		}
		return str;
	},
	parse: function(response,xhr) {
		this.log("%cCSS QUERY PARSE","font-size: 30px; color: #0ff");
		// var rules = new goldfish.CssRulesCollection();
		
		// if (response.rules) {
		// 	rules.add(response.rules, {parse: true, merge: false});
		// }
		// if (response.blocks && response.blocks.length > 0) {
		// 	this.attributes.blocks = this.attributes.blocks ? this.attributes.blocks : new goldfish.UserBlocksCollection();	
		// 	this.attributes.blocks.set(response.blocks, {parse: true});
		// }

		// if (response.cssQueries && response.cssQueries.length > 0) {
		// 	this.attributes.cssQueries = this.attributes.cssQueries ? this.attributes.cssQueries : new goldfish.CssQueriesCollection();
		// 	this.attributes.cssQueries.set(response.cssQueries, {parse: true});
		// }

		return {
			mediaQuery: response.mediaQuery,
			id: response.id,
			instanceOf: response.instanceOf,
			body_css: response.body_css,
			body_css_custom: response.body_css_custom
		};
	},
	toJSON: function() {
		var json = {
			mediaQuery: this.get('mediaQuery'),
			instanceOf: this.get('instanceOf'),
			cid: this.cid,
			body_css_custom: this.get('rules').toJSON()
		};
		if (this.get('id')) {
			json.id = this.get('id');
		}
		return json;
	}
	
});