var goldfish = goldfish || {};

/** File Representation
* @author Daniel Villanueva @danieljx
* @augments Backbone.Model
* @constructor
* @name FilesNotification
*/
goldfish.FilesNotification = goldfish.SseBase.extend({
	initialize: function() {
		console.log("Files Initialize is being called.");
	},
	url: function() {
		return '/api/user/'+goldfish.app.getUser().get("id")+'/file/notification/';
	}
});
