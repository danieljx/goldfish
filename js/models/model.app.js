var goldfish = goldfish || {};

/** The application
* @author Mats Fjellner
* @augments Backbone.Model
* @constructor
* @name App
*/
goldfish.App = Backbone.Model.extend(/** @lends App */
 {
 	log: false ? console.log.bind(window.console) : function() { },
	defaults: {
		activeUnit: false,
		activeProject: false,
		activeFile: false,
		activeUser: false,
		activeSectionPrev: false,
		activeSection: false,
		thinking: true,
		showMenu: false,
		showSecondaryMenu: false,
		showPreview: false,
		currentUnit: false,
		editState: 0,
		units: false,
		dragging: false,
		screen: false,
		sse: false,
		showDebug: true,
		debugClass: '',
		debugInfo: '',
		sseMessages: []
	},
	/** Creates collection of available components on initialization
	* @construct
	*/
	initialize: function() {
		var me = this;
		

		if (this.get("showDebug")) {
			if (window.performance.memory) {
				this.updateDebugInfo();

			} else {
				this.set("showDebug", false);
				this.set("debugInfo", "Debug no disponible en este navegador");
			}

		}

		if (!this.attributes.formats) {
			this.attributes.formats = new goldfish.UnitFormatsCollection();
		}
		// if (!this.attributes.blocks) {
		// 	this.attributes.blocks = new goldfish.BlocksCollection();
		// }

	},
	initSSE: function() {
		this.sse = new EventSource('/backend/events.php');
		this.sse.onmessage = this.onSSE.bind(this);
		this.sse.addEventListener("open", function(e) {
		}, false);
		this.sse.addEventListener("error", function(e) {
    		goldfish.log("Error SSE.");
    		goldfish.log(e);
		}, false);
	},
	updateDebugInfo: function() {
		var mem = window.performance.memory;
		this.set("debugInfo", "Memory: "+Math.round(mem.usedJSHeapSize/1000)+"kb" );
		this.set("debugClass", mem.usedJSHeapSize > 30000000 ? "warning" : "");
		window.setTimeout( function() {this.updateDebugInfo(); }.bind(this), 1000);
	},
	onSSE: function(e) {
		this.get("sseMessages").push(e.data);
		this.trigger("change");
	},
	/** Sets app user
	* @param {Object} aUser - User Model
   */
	setUser: function(aUser) {
		this.set("activeUser", aUser);
	},
	/** Gets app user
	* @return {Object} User Model
   */
	getUser: function() {
		return this.get("activeUser");
	},
	/** Loads project and sets as active
	* @param {integer} projectId - Id
	* @param {function} aCallback - Callback on success
	* @return {Object} Project Model
   */
	loadProject: function(projectId, aCallback) {
		if (this.set("activeProject", this.get("activeUser").get("projects").findWhere({id: projectId*1}) )) {
			this.get("activeProject").fetch({ success: aCallback });
		} else {
			return false;
		}

		return this.get("activeProject");

	},
	/** Gets active project
	* @return {Object} Project Model
   */
	getProject: function() {
		return this.get("activeProject");
	},
	/** Loads Files
	* @param {integer} pathHash - md5 hash name file or folder
	* @param {function} aCallback - Callback on success
	* @return {Object} File Model
    */
	loadFile: function(pathHash, aCallback) {
		this.get("activeUser").get("files").fetch({ data: {path_hash: pathHash}, processData: true, success: aCallback });
		if(this.set("activeFile", this.get("activeUser").get("files"))) {
			return this.get("activeFile");
		} else {
			return false;
		}
	},
	/** Gets active file
	 * @return {Object} File Model
     */
	getFile: function() {
		return this.get("activeFile");
	},
	/** Loads unit and sets as active
	* @param {integer} unitId - Id
	* @param {function} aCallback - Callback on success
	* @return {Object} Unit Model
   */
	loadUnit: function(unitId, aCallback) {
		if (this.set("activeUnit", this.get("activeProject").get("units").findWhere({id: unitId}) )) {
			this.get("activeUnit").fetchComponents({ success: function() {
				console.log("%cUnit components OK","color: green; font-size: 30px");
				this.get("activeUnit").fetch({ success: aCallback });
			}.bind(this), error: function() { console.log("components error"); }, parse: true });
		} else {
			return false;
		}

		return this.get("activeUnit");

	},
	/** Gets active unit
	* @return {Object} Unit Model
   */
	getUnit: function() {
		return this.get("activeUnit");
	},
	/** Sets active unit
	* @param {Object} Unit Model
	* @return {Object} Unit Model
   */
	setUnit: function(aUnit) {
		this.attributes.currentUnit = aUnit;
		return this.attributes.currentUnit;
	},
	/** Sets unit formats list
	* @param {array} Format models
	* @return {arrat} Format models
   */
  	setFormats: function(formats) {
		this.attributes.formats.set(formats, {parse: true});
		return this.attributes.formats;
	},
	/** Gets unit format
	* @param {integer} Format id
	* @return {Object} Format model
   */
	getFormat: function(formatId) {
		return this.attributes.formats.findWhere( {id: formatId} );
	},
	/** Sets active section and triggers event 'switch'
	* @param {string} aSection - Section alias
   */
	setActive: function(aSection) {
		this.attributes.activeSectionPrev = this.attributes.activeSection;
		this.attributes.activeSection = aSection;
		this.trigger("switch");
	},
	/** Toggles 'dragging' state
	* @param {boolean} dragging - On/Off
   */
	setDragging: function(dragging) {
		this.attributes.dragging = dragging;
		this.trigger("dragchange");

	},
	/** Returns 'dragging' state
	* @return {boolean} On/Off
   */
	isDragging: function() {
		return this.attributes.dragging;
	},
	/** Adds units <em style="color: red;">Move to Project</em>
	* @param {Collection} units
   */
	addUnits: function(units) {
		for(var i=0; i<units.length;i++) {
			this.attributes.units.add(units[i]);
		}

	},
	localFetch: function() {
		this.attributes.units.fetch({
      		success: function(model, response, options) {
      			goldfish.log("%clocalfetch ok",'font-weight: bold; color: green');
      			goldfish.log(response);
      			goldfish.log(response[0]);
      			if (response[0].pages && response[0].pages.constructor === Array) {
					goldfish.log("%cpages array","font-weight: bold");

				}
      			// goldfish.log("fetch OK");
      			// goldfish.log(model);
      			// goldfish.log(response);
      			// goldfish.log(response[0].title);
      			// for (var i=0; i<model.models.length; i++) {
      			// 	// goldfish.log(model.models[i]);
      			// 	// var unit = goldfish.app.attributes.units.findWhere({id: model.models[i].id});
      			// 	// goldfish.log(unit);
      			// 	delete response[i].pages;
      			// 	goldfish.log(response[i]);
      			// 	goldfish.log( model.models[i] );
      			// 	// model.models[i].set( response[i] );
      			// 	goldfish.log( model.models[i] );
      			// 	// unit.set(model.models[i].attributes);
      			// }

      		},
      		error: function(model, response, options) {
      			goldfish.log("fetch error");
      			goldfish.log(response);
      		}
      	});
	},
	/** JSON representation
	* @returns {JSON} JSON
   */
	toJSON: function() {

		return {
			user: this.get("activeUser").toJSON(),
			sseMessages: this.get("sseMessages"),
			activeProject: this.get("activeProject") ? this.get("activeProject").toJSON() : '',
			showDebug: this.get("showDebug"),
			debugInfo: this.get("debugInfo"),
			debugClass: this.get("debugClass")
			// pages: pages
		};
	}
} );
