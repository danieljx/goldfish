var goldfish = goldfish || {};

/** Represents a user project
* @author Mats Fjellner
* @augments Backbone.Model
* @constructor
* @name Project
*/
goldfish.Project = goldfish.ProjectBase.extend( /** @lends Project */
{
	urlRoot: function() {
		return '/api/user/'+goldfish.app.getUser().get("id")+'/project/';
	},
	defaults: {
		title: '',
		description: '',
		// units: false,
		admin: false,
		control_calidad: false,
		remote_auth: false,
		format_id: false,
		roles: false,
		blocks: false,
		themes: false,
	},
	/** Creates collection of available units on initialization
	* @construct
	*/
	initialize: function(initialModels) {
		// goldfish.log("%cNew project initialized!",'font-size: 40px;');
		// goldfish.log(this.attributes);
		this.attributes.units = new goldfish.UnitsCollection(initialModels.units);
		this.attributes.themes = new goldfish.ThemesCollection();
		this.listenTo(this.attributes.units, "add remove", function() { 
			this.trigger("change");
		});

		/*if (!this.attributes.blocks) {
			this.attributes.blocks = new goldfish.BlocksCollection();
			
			this.listenTo(this.attributes.blocks, "add remove", function() { 
				goldfish.log("block change");
			});
			
		}*/

		// this.units = new goldfish.UnitsCollection();
	},
	fetch: function(options) {
/*		this.get("blocks").url = this.urlRoot()+this.get('id')+'/components';
		this.get("blocks").fetch();
		goldfish.log("will call model fetch");*/
		return Backbone.Model.prototype.fetch.call(this, options);  

	},

	getThemes: function(options) {
		this.get("themes").url = this.urlRoot()+this.get('id')+'/themes';
		this.get("themes").fetch( options );
		goldfish.log(this.get("themes").length);
		return this.get("themes");  

	},
	
	/** Parses server response and sets units
	* @return {Object} Attributes + units
	*/
	parse: function(response, xhr) {
		// goldfish.log("%cparse!",'font-weight: bold;');
		// goldfish.log(response);
		if (response.units) {
			this.attributes.units.set(response.units);
		}
		return {
			title: response.title,
			remote_auth: response.remote_auth,
			description: response.description,
			format_id: response.format_id,
			roles: response.roles,
			units: this.attributes.units
		};
	},
	/** JSON representation including id + title of units
	* @return {JSON} JSON
	*/
	toJSON: function() {
		// console.log(this.attributes);
		// var pages = this.attributes.units; // .toJSON();
		// goldfish.log(pages);

		var units = [];

			this.attributes.units.each( function(item, i) {
			units[i] = {
				id: item.get("id"),
				title: item.get("title")
			};
		});	
				

		
		return {
			title: this.get('title'),
			remote_auth: this.get('remote_auth'),
			description: this.get('description'),
			format_id: this.get('format_id'),
			format_name: this.get('format_id') ? goldfish.app.getFormat( this.get('format_id') ).get("name") : '',
			roles: this.get('roles'),
			units: units,
			id: this.get('id') || this.cid
			// pages: pages
		};
	},
} );


