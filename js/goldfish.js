var goldfish = goldfish || {};

CKEDITOR.disableAutoInline = true;

function convertTimestamp(timestamp) {
  var d = new Date(timestamp),	// Convert the passed timestamp to milliseconds
		yyyy = d.getFullYear(),
		mm = ('0' + (d.getMonth() + 1)).slice(-2),	// Months are zero based. Add leading 0.
		dd = ('0' + d.getDate()).slice(-2),			// Add leading 0.
		hh = ('0' + d.getHours()).slice(-2),
		min = ('0' + d.getMinutes()).slice(-2),		// Add leading 0.
		time;

	time = yyyy + '-' + mm + '-' + dd + ', ' + hh + ':' + min + ' ';


	return timestamp;
}

goldfish.start = function(initialModels) {
	this.debug = true;
	if (this.debug) {
		this.log = console.log.bind(window.console);
	} else {
		this.log = function() {};
	}

	i18next.init({
		lng: 'es',
		debug: true,
		resources: {
		  es: {
			translation: initialModels.i18n
		  },
		}
	  }, function(err, t) {
		// init set content
		
	  });

	this.i18n = i18next;
	this.app = new this.App();
	this.app.setUser( new this.User(initialModels.user) );
	this.app.setFormats( initialModels.formats );
	this.appView = new this._AppView( {model: this.app } );
	this.projectsView = new this.UserProjectsView({model: this.app.getUser(), el: $('#projects') });
	this.config = new this.UserConfig( initialModels.user.config );
	this.configView = new this.UserConfigView( {model: this.config, el: $('#config') } );
	$('#config .ui.checkbox.toggle').checkbox();
	$('#config .ui.dropdown').dropdown();
	this.fonts = new this.FontCollection( initialModels.fonts);
	// console.log($('#config .ui.checkbox'));

	this.generalConfig = new this.Config( initialModels.config );
	
	this.appView.setTheme( this.config.get('theme') );
	this.appView.setEffects( this.config.get('effects') );
	
	this.router = new this.Router();
	Backbone.history.start();
	
	// this.app.initSSE();
	// this.notificationsView = new this.UserNotificationsView( {model: this.app, el: $('#notifications') } );

	// goldfish.log("fetching locally..");
	// this.app.localFetch();
};
