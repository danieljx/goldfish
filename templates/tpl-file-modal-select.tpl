<script type="text/template" id="tpl-modal-files-select">
    <div class="header">
        <label>Buscar Archivos</label>
    </div>
    <div class="content">
        <div class="filesTab ui top attached tabular menu">
            <a class="item active" data-tab="myfiles">Mis Archivos</a>
            <a class="item" data-tab="gallery">Galeria Imagenes</a>
        </div>
		<div class="myfilesTab ui bottom attached tab segment active" data-tab="myfiles">
			<div class="ui active dimmer loadMyFile">
				<div class="ui loader"></div>
			</div>
		</div>
		<div class="galleryTab ui bottom attached tab segment" data-tab="gallery"></div>
    </div>
    <div class="actions">
        <div class="ui button cancel">Cerrar</div>
    </div>
</script>