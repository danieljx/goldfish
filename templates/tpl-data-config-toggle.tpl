<script type="text/template" id="tpl-data-config-toggle">
<div class="config-item type-toggle" data-name="<%= name %>">
    <div class="input">
        <div class="ui toggle checkbox">
            <input type="checkbox" <% if (value) { %>checked="true"<% } %> />
        </div>
    </div>
    <div class="name">
        <p><%= title %></p>
    </div>
</div>
</script>