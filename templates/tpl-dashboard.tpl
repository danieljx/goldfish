<script type="text/template" id="tpl-dashboard">
  <!-- Template content goes here -->

  <svg viewBox="0 0 284.936 284.936" class="dashboard-toggle">
    <use xlink:href="img/open-horizontal.svg#icon-1"></use>
  </svg>
  <a href="#user/<%=user.id%>">
    <% if (user.profile) { %>
      <img class="user-profile-pic" src="user/profile/<%=user.id%>/<%=user.profile%>" />
      <% } else { %>
        <svg viewBox="0 0 32 32" class="user-profile-pic default">
          <use xlink:href="user/profile/0/default.svg#icon-1"></use>
        </svg>
        <% } %>
  </a>
  <h4>
    <%=user.names%>
      <%=user.surnames%>
  </h4>

  <ul>
    <li>
      <a href="messages/<%=user.id%>" data-tooltip="Notificaciones" data-position="right center">
        <svg viewBox="0 0 32 32">
          <use xlink:href="img/icons/bell.svg#icon-1"></use>
        </svg>
        <span>Notificaciones
          <sup>
            <%=sseMessages.length%>
          </sup>
        </span>
      </a>
    </li>

    <li>
      <a href="#user/<%=user.id%>" data-tooltip="Proyectos" data-position="right center">
        <svg viewBox="0 0 32 32">
          <use xlink:href="img/icons/unit.svg#icon-1"></use>
        </svg>
        <span>Proyectos
          <sup>
            <%=user.projects.length%>
          </sup>
        </span>
      </a>
      <ul>
        <% if (activeProject) { %>
          <li class="active project">
            <a href="#project/<%=activeProject.id%>">
              <svg viewBox="0 0 32 32">
                <use xlink:href="img/icons/project.svg#icon-1"></use>
              </svg>
              <span>
                <%=activeProject.title%>
              </span>
            </a>
            <% if (activeProject.units) { %>
              <ul class="user-units">
                <% _.each(activeProject.units, function(aUnit) {  %>
                  <li>
                    <a href="#project/<%=activeProject.id%>/unit/<%=aUnit.id%>">
                      <svg viewBox="0 0 32 32">
                        <use xlink:href="img/icons/page.svg#icon-1"></use>
                      </svg>
                      <span>
                        <%=aUnit.title%>
                      </span>
                    </a>
                  </li>
                  <% }); %>
              </ul>
              <% } %>
          </li>
          <% } %>
      </ul>
    </li>

    <li>
      <a href="#files/" data-tooltip="Archivos" data-position="right center">
        <svg viewBox="0 0 32 32">
          <use xlink:href="img/icons/storage.svg#icon-1"></use>
        </svg>
        <span>Archivos</span>
      </a>
    </li>

    <li>
      <a href="#config" data-tooltip="Preferencias" data-position="right center">
        <svg viewBox="0 0 32 32">
          <use xlink:href="img/icons/settings.svg#icon-1"></use>
        </svg>
        <span>Preferencias</span>
      </a>
    </li>

    <li>
      <a href="#logout" data-tooltip="Desconectar" data-position="right center">
        <svg viewBox="0 0 32 32">
          <use xlink:href="img/icons/logout.svg#icon-1"></use>
        </svg>
        <span>Desconectar</span>
      </a>
    </li>
  </ul>

</script>