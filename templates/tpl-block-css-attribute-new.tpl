<script type="text/template" id="tpl-block-css-attribute-new">
<select class="ui dropdown attribute-pick">	
<% _.each(attrList, function(attribute) { %>  
    <option><%= attribute %></option>
<% }); %>
</select>
<div class="contains default-value"></div>
<div class="switch">
<div class="ui toggle checkbox">
      <input type="checkbox" class="is-custom" name="is-custom" <% if (isCustom == 1) { %>checked="true"<% } %>>
</div>
</div>
<div class="contains custom-value"></div>
</script>