<script type="text/template" id="tpl-files-up">
  <div id="holder" draggable=”true”>
    <h2>Drop Files</h2>
  </div>
  <input type="file" id="files" name="files[]" multiple />
  <div class="upload-progress">
    <ul class="ul-up"></ul>
  </div>
</script>