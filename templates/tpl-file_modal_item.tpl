<script type="text/template" id="tpl-files-modal-list-item">
    <td class="icon">
        <div>
            <span>
                <svg width="40" height="40" viewBox="0 0 60 60" class="<%=icon%>"><use xlink:href="img/icons/file/<%=icon%>.svg#icon-1"></use></svg>
            </span>
        </div> 
    </td>
    <td class="name">
        <span class="text-name"><%=name%></span>
    </td>
    <td class="date">10/4/2017 16:09</td>
    <td class="action">
        <% if (mimetype != 2) { %>
        <button class="button-select">
            <span class="button-text">Seleccionar</span>
        </button>
        <% } %>
    </td>
</script>