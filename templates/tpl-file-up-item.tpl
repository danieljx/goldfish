<script type="text/template" id="tpl-files-up-item">
  <div class="status">
    <span class="load">
      <div class="lds-css">
        <div class="lds-ring" style="width:100%;height:100%"><div></div><div></div><div></div><div></div></div>
      </div>
    </span>
    <span class="done">
      <svg class="checked" x="0px" y="0px" width="30" height="30" viewBox="0 0 286.054 286.054"><use xlink:href="img/icons/controls/checked.svg#icon-1"></use></svg>
    </span>
    <span class="warning">
      <svg class="info" x="0px" y="0px" width="30" height="30" viewBox="0 0 286.054 286.054"><use xlink:href="img/icons/controls/info.svg#icon-1"></use></svg>
    </span>
  </div>
  <div class="file-info">
    <span class="file">
      <%=name%>
    </span>
    <span class="size">
      <i><%=size%></i>
    </span>
  </div>
  <div class="controls">
    <!--
    <div class="controls-pause">
      <svg class="pause" x="0px" y="0px" width="30" height="30" viewBox="0 0 286.054 286.054"><use xlink:href="img/icons/controls/pause.svg#icon-1"></use></svg>
    </div>
    -->
    <div class="controls-cancel">
      <svg class="cancel" x="0px" y="0px" width="30" height="30" viewBox="0 0 286.054 286.054"><use xlink:href="img/icons/controls/cancel.svg#icon-1"></use></svg>
    </div>
  </div>
</script>