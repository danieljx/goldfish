<script type="text/template" id="tpl-files-modal-select-list">
    <div class="ui column doubling stackable grid">
        <div class="eight wide column breadcrumbs"></div>
        <div class="eight wide column searchs">
            <div class="ui fluid category search">
                <div class="ui fluid icon input">
                    <input class="prompt" type="text" placeholder="Search Files...">
                    <i class="search icon"></i>
                </div>
                <div class="results"></div>
            </div>
        </div>
        <div class="sixteen wide column lists">
            <table class="ui selectable inverted compact striped definition celled table">
                <thead class="full-width">
                    <tr>
                        <th></th>
                        <th>Nombre</th>
                        <th>Modificado</th>
                        <th>Miembros</th>
                        <th>Acción</th>
                    </tr>
                </thead>
                <tbody></tbody>
                <tfoot class="full-width">
                    <tr>
                        <th colspan="5"></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</script>