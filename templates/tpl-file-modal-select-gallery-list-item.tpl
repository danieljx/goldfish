<script type="text/template" id="tpl-files-modal-select-gallery-list-item">
    <div class="ui active dimmer loadImg">
        <div class="ui loader"></div>
    </div>
    <div class="ui fluid image blurring dimmable">
        <div class="ui <%=color%> bottom attached tiny label">
            <i class="hotel icon"></i> <%=view%>
        </div>
        <div class="ui dimmer">
            <div class="content">
                <div class="center">
                    <div class="ui inverted button">Add Friend</div>
                </div>
            </div>
        </div>
        <img>
    </div>
</script>