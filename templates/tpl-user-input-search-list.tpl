<script type="text/template" id="tpl-user-input-search-list">
    <span class="user-header">
        <h3>Usuarios</h3>
    </span>
    <ul class="ul-users">
        <li class="user-option not">
            <div class="user-option-flag">
                <div class="user-option-flag-name">
                    <span>Not Users</span>
                </div>
            </div>
        </li>
    </ul>
    <span class="project-header">
        <h3>Proyectos</h3>
    </span>
    <ul class="ul-projects">
        <li class="project-option not">
            <div class="project-option-flag">
                <div class="project-option-flag-name">
                    <span>Not Projects</span>
                </div>
            </div>
        </li>
    </ul>
</script>