<script type="text/template" id="tpl-files-item-tags">
  <div class="header">
    <span class="icon">
      <svg width="40" height="40" viewBox="0 0 60 60" class="<%=icon%>">
        <use xlink:href="img/icons/file/<%=icon%>.svg#icon-1"></use>
      </svg>
    </span>
    <span class="text"><%=name%></span>
  </div>
  <div class="content">
    <% if((!publicView && !sharedView) || shared.share_edit == 1) { %>
    <div class="ui menu">
      <div class="item icon left button">
        <i class="tag icon"></i>
      </div>
      <div class="ui item fluid multiple search selection dropdown">
        <input name="tags" type="hidden">
        <i class="search icon"></i>
        <div class="default text">Tags</div>
        <div class="menu"></div>
      </div>
      <a class="ui item button right addTag">
        Agregar
      </a>
    </div>
    <% } %>
    <div class="ui tags segment"></div>
  </div>
  <div class="actions">
    <div class="ui button cancel">Cancelar</div>
    <% if((!publicView && !sharedView)  || shared.share_edit == 1) { %>
    <div class="ui green button save disabled">Guardar</div>
    <% } %>
  </div>
</script>
