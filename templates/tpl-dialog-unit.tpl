<script type="text/template" id="tpl-dialog-unit">
	<h4>Crear nueva unidad</h4>
	<input type="text" placeholder="Introducir nombre" class="unit-name-create" />
	<p class="validation"></p>
	<p>
		<svg viewBox="0 0 32 32" class="ok disabled"><use xlink:href="img/icons/check.svg#icon-1"></use></svg>
		<svg viewBox="0 0 32 32" class="cancel"><use xlink:href="img/icons/cross.svg#icon-1"></use></svg>
	</p>

</script>