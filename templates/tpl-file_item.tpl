<script type="text/template" id="tpl-files-item">
    <td class="icon">
        <div>
        <div class="md-checkbox">
        <% if(icon != 'folder-shared' && icon != 'folder-public') { %>
            <input id="i<%=id%>" name="checkTool" type="checkbox">
            <label for="i<%=id%>"></label>
        <% } %>
        </div>
        <span>
            <% if (icon == 'folder') { %>
                <svg width="40" height="40" viewBox="-5 -5 65 65" class="<%=icon%>"><use xlink:href="img/icons/file/<%=icon%>.svg#icon-1"></use></svg>
            <% } else if (icon == 'folder-shared') { %>
                <svg width="40" height="40" viewBox="-5 -5 46 46" class="<%=icon%>"><use xlink:href="img/icons/file/<%=icon%>.svg#icon-1"></use></svg>
            <% } else if (icon == 'folder-public') { %>
                <svg width="40" height="40" viewBox="-5 -5 46 46" class="<%=icon%>"><use xlink:href="img/icons/file/<%=icon%>.svg#icon-1"></use></svg>
            <% } else { %>
                <svg width="40" height="40" viewBox="0 0 60 60" class="<%=icon%>"><use xlink:href="img/icons/file/<%=icon%>.svg#icon-1"></use></svg>
            <% } %>
        </span>
        </div>
    </td>
    <td class="name">
        <span class="text-name"><%=name%></span>
        <span class="input-name">
            <input type="text" class="name-file" name="name-file" value="<%=name%>" />
        </span>
    </td>
    <td class="date">10/4/2017 16:09</td>
    <td class="users">Solo Tú</td>
    <td class="action">
        <% if(icon != 'folder-shared' && icon != 'folder-public') { %>
        <div class="tools ui icon top right pointing dropdown button">
            <i class="ellipsis horizontal icon"></i>
        </div>
        <div class="view ui icon top right pointing dropdown button hidden">
            <i class="eye icon"></i>
        </div>
        <% } %>
    </td>
</script>
