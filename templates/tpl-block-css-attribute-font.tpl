<script type="text/template" id="tpl-block-css-attribute-font">
<select class="ui dropdown font-pick">	
<% _.each(fonts, function(font) { 
    var isSelected = (font.title == value) ? 'selected="selected"': 'false';%>  
    <option <%=isSelected%>><%=font.title%></option>
<% }); %>
</select>
</script>