<script type="text/template" id="tpl-unit-structure">
<section id="structure">
    <h3 id="<%=theme%>">Structure</h3>
    <nav>
        <button id="add-child" disabled="disabled">+</button>
        <button id="remove" disabled="disabled">-</button>
        <button id="save-tree">S</button>
        <button id="move-left">&lt;</button>
        <button id="move-right">&gt;</button>
        <button id="go-unit">@</button>
    </nav>
</section>
</script>