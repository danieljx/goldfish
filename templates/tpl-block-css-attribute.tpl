<script type="text/template" id="tpl-block-css-attribute">

<div class="name"><%=name%></div>
<% if (type == "color") { %>
<div class="contains default-value attr-type-<%=type%>"><%=defaultValue%></div>
<% } else { %>
<div class="contains default-value"><%=defaultValue%></div>
<% } %> 

<div class="switch">
<% if (defaultValue) { %>
<div class="ui toggle checkbox">
      <input type="checkbox" class="is-custom" name="is-custom" <% if (isCustom == 1) { %>checked="true"<% } %>>
</div>
<% } else { %>
<% } %> 
</div>
<div class="contains custom-value attr-type-<%=type%>"><%=customValue%></div>
<% if (!defaultValue) { %>
<button class="remove-attribute"><svg viewBox="0 0 32 32"><use xlink:href="img/icons/cross.svg#icon-1"></use></svg></button>
<% } %>

</script>