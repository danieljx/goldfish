<script type="text/template" id="tpl-user-input-search-list-user">
    <div class="user-option-flag">
        <div class="user-option-flag-icon">
            <div class="user-avatar">
                <% if (profile) { %>
                    <img alt="Foto de la cuenta" class="mc-avatar-image" src="user/profile/<%=id%>/<%=profile%>">
                <% } else { %>
                    <svg viewBox="-4 -2 40 40" width="0" height="0"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/icons/user.svg#icon-1"></use></svg>
                <% } %>
            </div>
        </div>
        <div class="user-option-flag-name">
            <span><%=names%></span>
            <span><%=surnames%></span>
        </div>
    </div>
</script>