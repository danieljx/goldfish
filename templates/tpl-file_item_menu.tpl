<script type="text/template" id="tpl-files-item-menu">
  <% if(!publicView && !sharedView) { %>
    <div class="item share">
      <i class="share alternate icon"></i>
      Compartir
    </div>
  <% } %>
    <div class="item download">
      <i class="download icon"></i>
      Descargar
    </div>
  <% if (mimetype != 2) { %>
    <div class="item tags">
      <i class="tag icon"></i>
      Tags
    </div>
  <% } %>
  <% if((!publicView && !sharedView) || shared.share_edit == 1) { %>
    <div class="item changeName">
      <i class="i cursor icon"></i>
      Cambiar nombre
    </div>
  <% } %>
  <% if(!publicView && !sharedView) { %>
    <div class="item move">
      <i class="share icon"></i>
      Mover
    </div>
    <div class="item copy">
      <i class="copy icon"></i>
      Copiar
    </div>
    <div class="item drop">
      <i class="eraser icon"></i>
      Eliminar
    </div>
  <% } %>
</script>
