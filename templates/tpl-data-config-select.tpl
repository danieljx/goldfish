<script type="text/template" id="tpl-data-config-select">
<div class="config-item type-toggle" data-name="<%= name %>">
    <div class="input">
        <select class="ui dropdown transition">	
            <% _.each(allValues, function(item) { 
                var isSelected = (item == value) ? 'selected="selected"': 'false';%>  
                <option <%= isSelected %> value="<%= item %>"><%= goldfish.i18n.t(item) %></option>
            <% }); %>
            </select>
    </div>
    <div class="name">
        <p><%= title %></p>
    </div>
</div>
</script>