<script type="text/template" id="tpl-config">
<!-- Template content goes here -->
  <h2><svg viewBox="0 0 32 32"><use xlink:href="img/icons/settings.svg#icon-1"></use></svg>Preferencias</h2>
  <div class="config">
  	
    <p><span>Theme:</span>
    <select class="ui dropdown theme-pick">
    <% _.each(themes, function(aTheme) {  
    var isSelected = (aTheme == theme) ? 'selected="selected"': 'false';
    %>
    	<option <%=isSelected%>><%=aTheme%></option>
    <% }); %>
	</select>
    </p>
    <p><span>Editor:</span> <%=editor%></p>
    <div class="ui toggle checkbox">
      <label>Efectos</label>
      <input type="checkbox" class="effects" name="effects" <% if (effects == 1) { %>checked="true"<% } %>>
      
    </div>
    
  </div>

</script>