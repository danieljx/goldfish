<script type="text/template" id="tpl-file-menu-right">
	<svg viewBox="0 0 284.936 284.936" class="secondary-menu-files-toggle" width="0" height="0">
		<use xlink:href="img/open-horizontal.svg#icon-1"></use>
	</svg>
	<div class="menu-secondary-sidebar" tabindex="6">
		<span class="selection-header-text"><strong>0</strong>&nbsp;elemento seleccionado</span>
		<div class="content-button-action-menu">
				<button class="button-action-menu upFile" style="<%=(!sharedView || shared.share_edit == 1 ? '' : 'display:none;')%>">
					<span class="button-text">Cargar Archivos</span>
				</button>
		</div>
		<ul>
			<% _.each(modelAction, function(action) {  %>
			<li class="<%=action.class%>">
				<a href="#">
					<span class="icon">
						<svg class="secondary-menu-files-toggle" x="0px" y="0px" width="20" height="20" viewBox="<%=action.viewBox%>" xml:space="preserve">
							<use xlink:href="img/icons/<%=action.icon%>.svg#icon-1"></use>
						</svg>
					</span>
					<span class="text"><%=action.name%></span>
				</a>
			</li>
			<% }); %>
		</ul>
	</div>
</script>
