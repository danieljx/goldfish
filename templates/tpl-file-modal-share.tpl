<script type="text/template" id="tpl-modal-share-files">
	<div class="container">
		<span class="close">
			<svg viewBox="0 0 32 32" class="cancel">
				<use xlink:href="img/icons/cross.svg#icon-1"></use>
			</svg>
		</span>
		<header>
			<div id="material-head">
				<span class="icon">
					<svg width="40" height="40" viewBox="0 0 60 60" class="<%=icon%>">
						<use xlink:href="img/icons/file/<%=icon%>.svg#icon-1"></use>
					</svg>
				</span>
				<span class="text"><%=name%></span>
			</div>
		</header>
		<div class="body-content">
			<div class="material-input search">
				<div class="input-wrapper"></div>
			</div>
			<div class="material-input list">
				<div class="input-wrapper"></div>
			</div>
		</div>
		<footer>
			<div class="left">
				<div class="footer ui button">
					<i class="setting icon"></i>
					Configuración de <%= (mimetype==2?'carpeta':'archivo') %>
				</div>
				<div class="footerPupup ui flowing popup top left transition hidden">
					<div class="ui form">
						<h1 class="ui sub header">Configuración de <%=name%></h1>
					  <div class="grouped fields">
					    <label>¿Quiénes pueden administrar el acceso a la carpeta?</label>
					    <div class="field">
					      <div class="ui radio checkbox">
					        <input type="radio" name="share_others" value="1" <%=(share_others == 1?'checked':'')%> >
					        <label>Solo los propietarios</label>
					      </div>
					    </div>
					    <div class="field">
					      <div class="ui radio checkbox">
					        <input type="radio" name="share_others" value="2" <%=(share_others == 2?'checked':'')%>>
					        <label>Las personas con permiso para editar</label>
					      </div>
					    </div>
					  </div>
						<div class="ui horizontal divider"></div>
					  <div class="grouped fields">
							<% if(mimetype == 2) { %>
					    	<label>¿Mostrar quiénes vieron los archivos de la carpeta?</label>
							<% } else { %>
						    <label>¿Mostrar quiénes vieron el archivo?</label>
							<% } %>
					    <div class="field">
					      <div class="ui radio checkbox">
					        <input type="radio" name="share_details" value="1" <%=(share_details == 1?'checked':'')%> >
					        <label>Mostrar la información sobre lectores a las personas con permiso de edición</label>
					      </div>
					    </div>
					    <div class="field">
					      <div class="ui radio checkbox">
					        <input type="radio" name="share_details" value="2" <%=(share_details == 2?'checked':'')%> >
					        <label>No mostrar la información sobre lectores</label>
					      </div>
					    </div>
					  </div>
					</div>
				</div>
			</div>
			<div class="right">
				<svg viewBox="0 0 32 32" class="cancel"><use xlink:href="img/icons/cross.svg#icon-1"></use></svg>
				<svg viewBox="0 0 32 32" class="ok disabled"><use xlink:href="img/icons/check.svg#icon-1"></use></svg>
			</div>
		</footer>
	</div>
</script>
