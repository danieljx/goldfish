<script type="text/template" id="tpl-user">
<!-- Template content goes here -->
  
  <svg viewBox="0 0 284.936 284.936" class="menu-toggle" width="0" height="0"><use xlink:href="img/open-horizontal.svg#icon-1"></use></svg>
    <a href="#user/<%=user.id%>">
    <% if (profile) { %>
    <img class="user-profile-pic" src="user/profile/<%=user.id%>/<%=user.profile%>" />
    <% } else { %>
    <svg viewBox="0 0 32 32" class="user-profile-pic default" width="0" height="0"><use xlink:href="user/profile/0/default.svg#icon-1"></use></svg>
    <% } %>
    </a>
    <h4><%=names%> <%=surnames%></h4>
    <ul>
      <li><a href="#user/<%=id%>">
        <svg viewBox="0 0 32 32" width="0" height="0"><use xlink:href="img/icons/project.svg#icon-1"></use></svg>
        <span>Proyectos <sup><%=projects.length%></sup></span>
      </a></li>
      <% if (activeProject) { %>
      <li class="active project">
        <a href="#project/<%=activeProject.id%>">
          <span><%=activeProject.title%></span>
      </a></li>
      <li>
      <!-- <a href="" id="menu-unit">
        <svg viewBox="0 0 32 32" width="0" height="0"><use xlink:href="img/icons/unit.svg#icon-1"></use></svg>
        <span>Unidades <sup><%=units.length%></sup></span>
      </a> -->
      <!-- <ul class="user-units">
        <% _.each(units, function(aUnit) {  %>
        <li>

        <a href="#unit/<%=aUnit.id%>">
          <svg viewBox="0 0 32 32" width="0" height="0"><use xlink:href="img/icons/page.svg#icon-1"></use></svg>
          <span><%=aUnit.title%></span></a></li>
        <% }); %>

      </ul> -->
      </li>
      <% } %>
      
      <li><a href="" id="menu-config">
        <svg viewBox="0 0 32 32" width="0" height="0"><use xlink:href="img/icons/settings.svg#icon-1"></use></svg>
        <span>Config</span>
      </a></li>
      <li><a href="">
        <svg viewBox="0 0 32 32" width="0" height="0"><use xlink:href="img/icons/user.svg#icon-1"></use></svg>
        <span>Perfil</span>
      </a></li>
      <li><a href="#logout">
        <svg viewBox="0 0 32 32" width="0" height="0"><use xlink:href="img/icons/logout.svg#icon-1"></use></svg>
        <span>Desconectar</span>
      </a></li>
      <!-- <li><a href="">
        Blocks
      </a></li> -->
    </ul>

</script>