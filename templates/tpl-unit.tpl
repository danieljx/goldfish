<script type="text/template" id="tpl-unit">
<section id="unit">
  <div class="unit-main">
    <div class="unit-info">
    <svg viewBox="0 0 284.936 284.936" class="info-toggle" height="0" width="0"><use xlink:href="img/open-horizontal.svg#icon-1"></use></svg>
    <div class="unit-info-title">
    <h2 contenteditable="true"><%=title%></h2>
    <aside>

      <button id="show-structure" data-tooltip="Estructura" data-position="bottom center"><svg viewBox="0 0 94 94"><use xlink:href="img/icons/structure.svg#icon-1"></use></svg><span>Estructura</span></button>

    <button id="show-page" class="active" data-tooltip="Página" data-position="bottom center"><svg viewBox="0 0 32 32"><use xlink:href="img/icons/page.svg#icon-1"></use></svg><span>Página</span></button>

    <button id="save-unit" data-tooltip="Guardar" data-position="bottom center"><svg viewBox="0 0 32 32"><use xlink:href="img/icons/save.svg#icon-1"></use></svg><span>Guardar</span></button>

      <button id="show-themes" data-tooltip="Plantillas" data-position="bottom center"><svg viewBox="0 0 32 32"><use xlink:href="img/icons/theme.svg#icon-1"></use></svg><span>Plantillas</span></button>

      <button id="show-config" data-tooltip="Configuración" data-position="bottom center"><svg viewBox="0 0 32 32"><use xlink:href="img/icons/config.svg#icon-1"></use></svg><span>Configuración</span></button>

      <button id="preview-toggle" data-tooltip="Vista previa" data-position="bottom center"><svg viewBox="0 0 54.308 54.308"><use xlink:href="img/icons/preview.svg#icon-1"></use></svg><span>Vista previa</span></button>

      <button id="export-unit" data-tooltip="Exportar" data-position="bottom center"><svg viewBox="0 0 29.978 29.978"><use xlink:href="img/icons/export.svg#icon-1"></use></svg><span>Exportar</span></button>


    </aside>
    </div>

    </div>

    <div class="sections">
      <section id="structure">
      </section>

    <section id="themes">
      
    </section>    

    <section id="config">
      
    </section>

    <section id="page">

    </section>
  </div>

  

  </div>

  <aside class="secondary-menu">
  </aside>
</section>
</script>