<script type="text/template" id="tpl-files-modal-select-list-item">
    <td class="icon">
        <div>
            <span>
                <% if (icon == 'folder') { %>
                    <svg width="40" height="40" viewBox="-5 -5 65 65" class="<%=icon%>"><use xlink:href="img/icons/file/<%=icon%>.svg#icon-1"></use></svg>
                <% } else if (icon == 'folder-shared') { %>
                    <svg width="40" height="40" viewBox="-5 -5 46 46" class="<%=icon%>"><use xlink:href="img/icons/file/<%=icon%>.svg#icon-1"></use></svg>
                <% } else if (icon == 'folder-public') { %>
                    <svg width="40" height="40" viewBox="-5 -5 46 46" class="<%=icon%>"><use xlink:href="img/icons/file/<%=icon%>.svg#icon-1"></use></svg>
                <% } else { %>
                    <svg width="40" height="40" viewBox="0 0 60 60" class="<%=icon%>"><use xlink:href="img/icons/file/<%=icon%>.svg#icon-1"></use></svg>
                <% } %>
            </span>
        </div>
    </td>
    <td class="name">
        <span class="text-name"><%=name%></span>
    </td>
    <td class="date">10/4/2017 16:09</td>
    <td class="users">Solo Tú</td>
    <td class="action">
        <% if (mimetype != 2) { %>
        <div class="ui right floated icon yellow buttons">
            <button class="select ui button" data-tooltip="Seleccionar" data-position="bottom center" data-inverted="">
                <i class="check icon"></i>
            </button>
        </div>
        <% } %>
    </td>
</script>