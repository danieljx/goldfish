<script type="text/template" id="tpl-block-tools">
<!-- Template content goes here -->
<% if (debug) { %>
<svg viewBox="0 0 32 32" class="tool-info">
  <use xlink:href="img/icons/info.svg#icon-1"></use>
</svg>
<% } %>
<% if (config) { %>
  <svg viewBox="0 0 32 32" class="tool-config">
    <use xlink:href="img/icons/settings.svg#icon-1"></use>
  </svg>
  <% } %>
<svg viewBox="0 0 502.664 502.664" class="tool-html">
  <use xlink:href="img/html.svg#icon-1"></use>
</svg>
<% //if (name == 'block') { %>
<svg viewBox="0 0 58 58" class="tool-files">
  <use xlink:href="img/icons/files.svg#icon-1"></use>
</svg>
<% //} %>
<svg viewBox="0 0 58 58" class="tool-msg">
  <use xlink:href="img/msg.svg#icon-1"></use>
</svg>
<svg viewBox="0 0 439.875 439.875" class="tool-css">
  <use xlink:href="img/css.svg#icon-1"></use>
</svg>
<svg viewBox="0 0 489.776 489.776" class="tool-drag">
  <use xlink:href="img/drag.svg#icon-1"></use>
</svg>
<svg viewBox="0 0 32 32" class="tool-remove">
  <use xlink:href="img/icons/cross.svg#icon-1"></use>
</svg>
</script>