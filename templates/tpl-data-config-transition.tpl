<script type="text/template" id="tpl-data-config-transition">
<div class="config-item type-toggle" data-name="<%= name %>">
    <div class="input">
        <select class="ui dropdown transition">	
            <% _.each(transitions, function(transition) { 
                var isSelected = (transition == value) ? 'selected="selected"': 'false';%>  
                <option <%=isSelected%>><%=transition%></option>
            <% }); %>
            </select>
    </div>
    <div class="name">
        <p><%= title %></p>
    </div>
</div>
</script>