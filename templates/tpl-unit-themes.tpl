<script type="text/template" id="tpl-unit-themes">
<h3 id="<%=theme%>">Themes</h3>

<ul class="themes">
<% _.each(themes, function(aTheme) {  %>
<% if (theme == aTheme.id) { %>
  <li class="selected">
<% } else { %>
  <li>
<% } %>
        <img src="img/dummy/theme2.png" />
        <h4><%=aTheme.title%></h4>
        <a href="setTheme/<%=aTheme.id%>"><span>Details</span></a>
</li>
<% }); %>
</ul>
</script>