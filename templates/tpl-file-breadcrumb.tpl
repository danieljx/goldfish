<script type="text/template" id="tpl-files-breadcrumb">
  <div class="ui large breadcrumb">
    <% _.each(breadcrumb, function(aBreadcrumb, i) {  %>
      <% if((i+1) < breadcrumb.length) { %>
        <% if(aBreadcrumb.name == 'Home') { %>
          <a class="section home" pathHash="<%=aBreadcrumb.path_hash%>" href="#files/<%=aBreadcrumb.path_hash%>"><i class="home icon"></i></a>
          <i class="right angle icon divider"></i>
        <% } else { %>
          <a class="section" pathHash="<%=aBreadcrumb.path_hash%>" href="#files/<%=(sharedView?'shared/':'')%><%=(publicView?'public/':'')%><%=aBreadcrumb.path_hash%>"><%=aBreadcrumb.name%></a>
          <i class="right angle icon divider"></i>
        <% } %>
      <% } else { %>
        <% if(aBreadcrumb.name == 'Home') { %>
          <div class="active section home"><i class="home icon"></i></div>
        <% } else { %>
          <div class="active section"><%=aBreadcrumb.name%></div>
        <% } %>
      <% } %>
    <% }); %>
  </div>
</script>
