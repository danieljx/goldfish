<script type="text/template" id="tpl-data-config-int">
<div class="config-item type-int" data-name="<%= name %>">
    <div class="input">
        <div class="ui fluid input">
            <input type="text" value="<%=value%>" />
        </div>
    </div>
    <div class="name">
        <p><%= title %></p>
    </div>
</div>
</script>