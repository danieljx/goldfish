<script type="text/template" id="tpl-user-input-search-list-project">
    <div class="project-option-flag">
        <div class="project-option-flag-icon">
            <div class="project-avatar">
                <svg viewBox="-4 -2 40 40" width="0" height="0"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/icons/unit.svg#icon-1"></use></svg>
            </div>
        </div>
        <div class="project-option-flag-name">
            <span><%=title%></span>
        </div>
    </div>
</script>