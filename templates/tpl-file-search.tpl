<script type="text/template" id="tpl-files-search">
    <div class="ui icon menu">
        <div class="menuNotification item ui icon top left pointing button" data-variation="very wide">
            <i class="alarm icon"></i>
            <div class="floating ui red label right">22</div>
        </div>
        <div class="menuNotificationItem ui flowing popup transition hidden"></div>
        <div class="search_text ui fluid category search item">
            <div class="ui transparent icon input">
                <input class="prompt" placeholder="Search..." id="search_text" name="search_text" type="text">
                <i class="search link icon"></i>
            </div>
            <div class="results"></div>
        </div>
        <div class="menuAction item ui icon top right pointing dropdown button">
            <i class="sidebar icon"></i>
            <div class="menu">
                <% if((!publicView && !sharedView) || shared.share_edit == 1) { %>
                <div class="item upFile">
                    <i class="upload icon"></i>
                    Cargar archivos
                </div>
                <div class="item newFile">
                    <i class="plus icon"></i>
                    Nueva carpeta
                </div>
                <% } %>
                <% if(!publicView && !sharedView && false) { %>
                <div class="item trashFile">
                    <i class="trash icon"></i>
                    Archivos eliminados
                </div>
                <% } %>
            </div>
        </div>
    </div>
</script>
