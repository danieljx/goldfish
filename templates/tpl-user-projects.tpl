<script type="text/template" id="tpl-user-projects">
<!-- Template content goes here -->
  
  <div class="projects" >
  	<h2><svg viewBox="0 0 32 32"><use xlink:href="img/icons/unit.svg#icon-1"></use></svg>Proyectos</h2>
    <% _.each(projects, function(aProject) {  %>
    <% if (aProject.remote_auth) { %>
    <div class="project locked">
    <% } else { %>
    <div class="project">
    <% } %>
      <div class="project-info">
        <h4><%=aProject.title%> <% if (aProject.remote_auth) { %><svg viewBox="0 0 32 32" class="info-toggle" height="0" width="0"><use xlink:href="img/icons/locked.svg#icon-1"></use></svg><% } %></h4>
        <p><%=aProject.description%></p>
      </div>
      <div class="roles">
        <% if (_.indexOf(aProject.roles, '1') > -1) { %>
        <p class="role"><svg viewBox="0 0 32 32" class="info-toggle" height="0" width="0"><use xlink:href="img/icons/check.svg#icon-1"></use></svg>Admin</p>
        <% } %>
        <% if (_.indexOf(aProject.roles, '2') > -1) { %>
        <p class="role"><svg viewBox="0 0 32 32" class="info-toggle" height="0" width="0"><use xlink:href="img/icons/check.svg#icon-1"></use></svg>Editor</p>
        <% } %>
        <% if (_.indexOf(aProject.roles, '3') > -1) { %>
        <p class="role"><svg viewBox="0 0 32 32" class="info-toggle" height="0" width="0"><use xlink:href="img/icons/check.svg#icon-1"></use></svg>Control de calidad</p>
        <% } %>
      </div>
      <a href="#project/<%=aProject.id%>"></a>
    </div>
    <% }); %>
  </div>

</script>