<script type="text/template" id="tpl-files-notification">
<div class="ui feed">
  <div class="event">
    <div class="label">
      <img src="user/profile/1/1174915_10151710581102543_1404515987_n.jpg">
    </div>
    <div class="content">
      <div class="summary">
        <a class="user">
          Mats Fjellner
        </a> added you as a friend
        <div class="date">
          1 Hour Ago
        </div>
      </div>
      <!--
      <div class="meta">
        <a class="like">
          <i class="like icon"></i> 4 Likes
        </a>
      </div>
      -->
    </div>
  </div>
  <div class="event">
    <div class="label">
      <i class="user icon"></i>
    </div>
    <div class="content">
      <div class="summary">
        <a>Xiomi Garzon</a> added <a>2 share files</a>
        <div class="date">
          4 days ago
        </div>
      </div>
      <div class="extra images">
        <a><img src="/images/wireframe/image.png"></a>
        <a><img src="/images/wireframe/image.png"></a>
      </div>
      <!--
      <div class="meta">
        <a class="like">
          <i class="like icon"></i> 1 Like
        </a>
      </div>
    -->
    </div>
  </div>
  <div class="event">
    <div class="label">
      <img src="user/profile/1/1174915_10151710581102543_1404515987_n.jpg">
    </div>
    <div class="content">
      <div class="summary">
        <a class="user">
          Mats Fjellner
        </a> added you as a friend
        <div class="date">
          2 Days Ago
        </div>
      </div>
      <!--
      <div class="meta">
        <a class="like">
          <i class="like icon"></i> 8 Likes
        </a>
      </div>
      -->
    </div>
  </div>
</script>
