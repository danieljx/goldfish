<script type="text/template" id="tpl-block-css-rule">

<h3>[<%=selectors%>]</h3>
<div class="css-attribute header">
<p class="name">Atributo</p>
<p class="default-value">Theme</p>
<div class="switch">
</div>
<p class="custom-value">Personalizado</p>
</div>
<section class="attributes"></section>
<section class="new-attributes"></section>
<nav>
<select class="ui dropdown attribute-pick">	
    <option value="" selected="selected">Añadir</option>
<% _.each(attrList, function(attribute) { %>  
    <option><%= attribute %></option>
<% }); %>
</select>
<button class="add-attribute disabled"><svg viewBox="0 0 29.978 29.978"><use xlink:href="img/icons/plus.svg#icon-1"></use></svg></button>
</nav>
</script>