<script type="text/template" id="tpl-modal-share-list-item-files">
	<div class="shared-option-flag">
		<div class="shared-option-flag-icon">
			<div class="shared-avatar">
				<% if (shared_user.share_type == 1) { %>
                    <% if (user.profile) { %>
                        <img alt="Foto de la cuenta" class="mc-avatar-image" src="user/profile/<%=user.id%>/<%=user.profile%>">
                    <% } else { %>
                        <svg viewBox="-4 -2 40 40" width="0" height="0"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/icons/user.svg#icon-1"></use></svg>
                    <% } %>
                <% } else { %>
                    <svg viewBox="-4 -2 40 40" width="0" height="0"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/icons/unit.svg#icon-1"></use></svg>
                <% } %>
			</div>
		</div>
		<div class="shared-option-flag-name">
			<% if (shared_user.share_type == 1) { %>
				<span><%=user.names%></span>
				<span><%=user.surnames%></span>
			<% } else { %>
				<span><%=project.title%></span>
			<% } %>
		</div>
		<div class="shared-option-flag-action">
			<div class="action ui floating labeled icon dropdown button">
			  <i class="edit icon"></i>
			  <span class="text"><%=(shared_user.share_edit == 1?'Puede(n) editar':'Puede(n) ver')%></span>
			  <div class="menu">
			    <div class="header">
			      Acciones Usuario(s)
			    </div>
			    <div class="divider"></div>
			    <div class="item <%=(shared_user.share_edit == 1?'active selected':'')%>" data-value="1">
						<i class="checkmark icon"></i>
			      <span class="text">Puede(n) editar</span>
				    <span class="description"><p>Las personas pueden editar, eliminar y agregar el archivo a sus storage</p></span>
			    </div>
			    <div class="item <%=(shared_user.share_edit == 2?'active selected':'')%>" data-value="2">
						<i class="checkmark icon"></i>
			      <span class="text">Puede(n) ver</span>
				    <span class="description"><p>Las personas pueden ver, descargar</p></span>
			    </div>
			    <div class="item delete" data-value="del">
						<span class="text">Quitar</span>
				    <span class="description"></span>
			    </div>
			  </div>
			</div>
		</div>
	</div>
</script>
