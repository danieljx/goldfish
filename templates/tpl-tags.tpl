<script type="text/template" id="tpl-tags">
  <div class="ui divided selection list">
  <% _.each(modelTags, function(tag) {  %>
    <div class="item">
      <a class="ui circular label">#<%=tag.countFile%></a>
      <a class="ui tag label"><%=tag.name%></a>
    </div>
  <% }); %>
  </div>
</script>
