<script type="text/template" id="tpl-project">
<!-- Template content goes here -->
  <h2><svg viewBox="0 0 32 32"><use xlink:href="img/icons/unit.svg#icon-1"></use></svg>Proyectos</h2>
  <div>
  	
  	<div class="project-info">
        <h3><%=title%> <% if (remote_auth) { %><svg viewBox="0 0 32 32" class="info-toggle" height="0" width="0"><use xlink:href="img/icons/locked.svg#icon-1"></use></svg><% } %></h3>
        <p><span class="unit-format"><%=format_name%></span> <%=description%></p>

        <% _.each(units, function(aUnit) {  %>
    		<% if (aUnit.title) { %>
    		<a href="#project/<%=id%>/unit/<%=aUnit.id%>" class="unit-link">
    			<svg viewBox="0 0 32 32" width="0" height="0"><use xlink:href="img/icons/page.svg#icon-1"></use></svg>
    			<%=aUnit.title%>
    			</a>
    		<% } %>
        
    	<% }); %>
      <% if (_.intersection(roles, ['1','2','3']).length > 0) { %>
        <a href="#project/<%=id%>/unit/" class="unit-add">
        <svg viewBox="0 0 32 32" width="0" height="0"><use xlink:href="img/icons/plus.svg#icon-1"></use></svg>
        </a>
        <% } %>
      </div>
      <div class="roles">
        <% if (_.indexOf(roles, '1') > -1) { %>
        <p class="role"><svg viewBox="0 0 32 32" height="0" width="0"><use xlink:href="img/icons/check.svg#icon-1"></use></svg>Superadmin</p>
        <% } %>
        <% if (_.indexOf(roles, '2') > -1) { %>
        <p class="role"><svg viewBox="0 0 32 32" height="0" width="0"><use xlink:href="img/icons/check.svg#icon-1"></use></svg>Admin</p>
        <% } %>
        <% if (_.indexOf(roles, '3') > -1) { %>
        <p class="role"><svg viewBox="0 0 32 32" height="0" width="0"><use xlink:href="img/icons/check.svg#icon-1"></use></svg>Editor</p>
        <% } %>
      </div>
  </div>

</script>