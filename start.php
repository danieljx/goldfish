<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>Goldfish</title>
  <!-- <link href="https://fonts.googleapis.com/css?family=Oswald:300,400,700" rel="stylesheet"> -->
  <link rel="stylesheet" id="css-theme" href="dist/css/start.css">
</head>
<body>
<svg viewBox="0 0 765 990" class="waiting"><use xlink:href="img/coi.svg#coi"></use></svg>
  <form action="" method="post">  
    <input id="username" name="username" autocomplete="username" autofocus="true" placeholder="Usuario" value="matsfjellner@catedra.edu.co">
    <input type="password" id="password" name="password" autocomplete="current-password" placeholder="Contraseña">
    <button>Iniciar</button>
  </form>
  <script src="dist/lib/jquery.min.js"></script>
  <script>
  $('button').click( function(e) {
    e.preventDefault();
    var username = $('#username').val();
    var password = $('#password').val();
    console.log("Trying login "+username+":"+password);
    $.ajax({
           type: "GET",
           url: "/slim-api/public/auth/"+username+'/'+password,
           success: function (msg) {
               if (msg) {
                   if (msg.result*1 > 0) {
                    var url = window.location.href;
                    url = url + ((url.indexOf('#') > -1) && (url.slice(-1) !== '/') ? '/' : '#') +'auth/'+msg.auth;
                    location.assign(url);
                    location.reload(true);
                   } else {
                    $('body').addClass("wrong");
                    $('button').attr('disabled',true);
                   }
               } else {
                   console.log("no response");
               }
           },

           
       });
  });

  $('input').keyup( function(e) {
    enableAction();
  });

  function enableAction() {
    if ($('#username').val().length > 5 && $('#password').val().length > 4) {
      $('body').addClass('enabled').removeClass('wrong');
      $('button').attr('disabled',false);
    } else {
      $('button').attr('disabled',true);
      $('body').removeClass('enabled')
    }
  }



  enableAction();
  </script>
</body>
</html>