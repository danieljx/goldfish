<script type="text/template" id="tpl-components">
<!-- Template content goes here -->

    <h2>Components</h2>
    <a class="save-all" href="#" data-tooltip="Guardar todos" data-position="bottom center">
        <svg viewBox="0 0 32 32"><use xlink:href="/img/icons/save.svg#icon-1"></use></svg>
    </a>

    <div class="container">
    <% _.each(components, function(aComponent) {  %>
    <div class="component">
    <h4><%=aComponent.title%></h4>
    <p><%=aComponent.type%></p>
    <nav>
    <a data-tooltip="Editar" data-position="bottom center" class="edit" href="component/<% if (aComponent.id) { %><%= aComponent.id %><% } else { %><%= 'new/'+aComponent.cid %><% } %>">
        <svg viewBox="0 0 27 32"><use xlink:href="/img/icons/edit.svg#icon-1"></use></svg>
    </a>
    <a data-tooltip="Guardar" data-position="bottom center" class="save<% if (!aComponent.id) { %><%= ' disabled' %><% } %>" href="#">
        <svg viewBox="0 0 32 32"><use xlink:href="/img/icons/save.svg#icon-1"></use></svg>
    </a>
    </nav>
    </div>
    <% }); %>
    <button class="add">
        <svg viewBox="0 0 32 32"><use xlink:href="/img/icons/plus.svg#icon-1"></use></svg>
    </button>
    </div>

</script>
