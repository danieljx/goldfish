<script type="text/template" id="tpl-component-tools">
<!-- Template content goes here -->

    <a class="html<% if (!valid.html) { %> disabled<% } %>" href="#" data-tooltip="HTML5" data-position="bottom center">
        <svg viewBox="0 0 32 32"><use xlink:href="/img/icons/html5.svg#icon-1"></use></svg>
    </a>
    <a class="scss<% if (!valid.scss) { %> disabled<% } %>" href="#" data-tooltip="SCSS" data-position="bottom center">
        <svg viewBox="0 0 32 32"><use xlink:href="/img/icons/sass.svg#icon-1"></use></svg>
    </a>
    <a class="json<% if (!valid.json) { %> disabled<% } %>" href="#" data-tooltip="JSON" data-position="bottom center">
        <svg viewBox="0 0 32 32"><use xlink:href="/img/icons/json.svg#icon-1"></use></svg>
    </a>
    <a class="save" href="#" data-tooltip="Save" data-position="bottom center">
        <svg viewBox="0 0 32 32"><use xlink:href="/img/icons/save.svg#icon-1"></use></svg>
    </a>

</script>
