<script type="text/template" id="tpl-users-pag">
    <% if(pages > 1) { %>
    <a class="next icon item  <%= (page==1?'disabled':'') %>">
        <i class="step backward icon"></i>
    </a>
    <a  class="previous icon item <%= (!prev?'disabled':'') %>">
        <i class="left chevron icon"></i>
    </a>
    <a class="item <%= (page==1?'active':'') %>"><%= range[0] %></a>
    <div class="disabled item">...</div>
    <% _.each(_.range(pages), function(i) {  %>
        <a class="item <%= (page==1?'active':'') %>"><%= i %></a>
    <% }); %>
    <a class="item <%= (page==total?'active':'') %>"><%= total %></a>
    <a class="next icon item  <%= (!next?'disabled':'') %>">
        <i class="right chevron icon"></i>
    </a>
    <a class="next icon item  <%= (page==total?'disabled':'') %>">
        <i class="step forward icon"></i>
    </a>
    <% } %>
</script>
