<script type="text/template" id="tpl-component">
<!-- Template content goes here -->

    <h2>Components</h2>

    <div class="container one">
        <div class="component">
            <h4 contenteditable="true"><%= title %></h4>
            <section>
                <div class="ui input">
                    <input class="repeatable" value="<%= repeatable %>" placeholder="Repetible: > div > p > a" />
                </div>

                <select class="ui dropdown component-type">
                    <% _.chain(types)
                    .filter(function(aType, i) { return i > 0})
                    .each(function(aType, i) {  %>
                    <option value="<%= i + 1 %>"<% if (i + 1 == type) { %> selected="selected"<% } %>><%= aType %></option>
                    <% }); %>
                </select>

            </section>
            <nav class="tools">
            </nav>
        </div>
        <div class="editor">
            <section class="html">
                <textarea><%= body %></textarea>
            </section>
            <section class="scss">
                <textarea><%= sass %></textarea>
            </section>
            <section class="json">
                <textarea><%= data %></textarea>
            </section>
        </div>
    </div>


</script>
