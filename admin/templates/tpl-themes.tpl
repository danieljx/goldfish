<script type="text/template" id="tpl-themes">
<!-- Template content goes here -->

    <h2>Themes</h2>
    <a class="save-all" href="#" data-tooltip="Guardar todos" data-position="bottom center">
        <svg viewBox="0 0 32 32" width="0" height="0"><use xlink:href="/img/icons/save.svg#icon-1"></use></svg>
    </a>

    <div class="container">
    <% _.each(themes, function(aTheme) {  %>
    <div class="theme">
        <h4><%=aTheme.title%></h4>
        <nav>
            <a data-tooltip="Editar" data-position="bottom center" class="edit" href="theme/<% if (aTheme.id) { %><%= aTheme.id %><% } else { %><%= 'new' %><% } %>">
                <svg viewBox="0 0 27 32"><use xlink:href="/img/icons/edit.svg#icon-1"></use></svg>
            </a>
            <a data-tooltip="Guardar" data-position="bottom center" class="save<% if (!aTheme.id) { %><%= ' disabled' %><% } %>" href="#">
                <svg viewBox="0 0 32 32" width="0" height="0"><use xlink:href="/img/icons/save.svg#icon-1"></use></svg>
            </a>
        </nav>
    </div>
    <% }); %>
    </div>



</script>
