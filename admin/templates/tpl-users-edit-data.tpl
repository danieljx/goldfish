<script type="text/template" id="tpl-users-edit-data">
    <div class="ui form">
        <input name="action" type="hidden" value="<%=user.action%>">
        <input name="id" type="hidden" value="<%=user.id%>">
        <div class="ui stackable two column divided grid container">
            <div class="row">
                <div class="large screen six wide column">
                    <div class="ui segment">
                        <div class="ui inverted dimmer loadPhoto">
                            <div class="ui small text loader">Loading</div>
                        </div>
                        <div class="field">
                            <div class="ui special cards">
                                <div class="card">
                                    <div class="blurring dimmable image">
                                        <div class="ui dimmer transition hidden">
                                            <div class="content">
                                                <div class="center">
                                                    <div class="ui yellow button userPhotoAdd">Add Photo</div>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="file" class="userPhotoUp transition hidden" name="profile" accept="image/x-png,image/gif,image/jpeg"/>
                                        <% if (user.id == 1) { %>
                                            <img alt="<%=user.names%> <%=user.surnames%>" class="ui image" src="../img/coi.svg#coi">
                                        <% } else if (user.profile) { %>
                                            <img alt="<%=user.names%> <%=user.surnames%>" class="ui image" src="../user/profile/<%=user.id%>/<%=user.profile%>">
                                        <% } else { %>
                                            <img alt="<%=user.names%> <%=user.surnames%>" class="ui image" src="../img/icons/user.svg#icon-1">
                                        <% } %>
                                    </div>
                                    <div class="content">
                                        <a class="header"><%=user.names%> <%=user.surnames%></a>
                                        <div class="meta">
                                            <span class="date">Created in Sep 2014</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="large screen ten wide column">
                    <div class="ui segment">
                        <h4 class="ui dividing header">Datos</h4>
                        <div class="field">
                            <div class="two fields">
                                <div class="field">
                                    <label>Primer Nombre</label>
                                    <input name="names" placeholder="Primer Nombre" type="text" value="<%=user.names%>">
                                </div>
                                <div class="field">
                                    <label>Segundo Nombre</label>
                                    <input name="surnames" placeholder="Segundo Nombre" type="text" value="<%=user.surnames%>">
                                </div>
                            </div>
                            <div class="field">
                                <div class="two fields">
                                    <div class="field">
                                        <label>E-mail</label>
                                        <input name="email" placeholder="example@example.com" type="email" value="<%=user.email%>">
                                    </div>
                                    <div class="field">
                                        <label>Password</label>
                                        <input type="password" name="password" placeholder="Password">
                                    </div>
                                </div>
                            </div>
                            <div class="ui segment">
                                <div class="field">
                                    <label>Estatus</label>
                                    <div class="statusActive ui fitted toggle checkbox">
                                        <input name="active" type="checkbox" <%=(user.active?'checked':'')%>>
                                        <label></label>
                                    </div>
                                </div>
                            </div>
                            <h4 class="ui dividing header">Preferencias</h4>
                            <div class="field">
                                <input name="config_id" type="text" class="transition hidden" value="<%=user.config.id%>">
                                <div class="two fields">
                                    <div class="field">
                                        <label>Theme</label>
                                        <div class="ui selection dropdown theme">
                                            <input name="theme" type="hidden" value="<%=user.config.theme%>">
                                            <i class="dropdown icon"></i>
                                            <div class="default text"><%=user.config.theme%></div>
                                            <div class="menu">
                                                <div class="item" data-value="dark">dark</div>
                                                <div class="item" data-value="goldfish">goldfish</div>
                                                <div class="item" data-value="light">light</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="field">
                                        <label>Editor</label>
                                        <div class="ui selection dropdown editor">
                                            <input name="editor" type="hidden" value="<%=user.config.editor%>">
                                            <i class="dropdown icon"></i>
                                            <div class="default text"><%=user.config.editor%></div>
                                            <div class="menu">
                                                <div class="item" data-value="ck">ck</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="ui segment">
                                <div class="field">
                                    <label>Efectos</label>
                                    <div class="statusEffects ui fitted toggle checkbox">
                                        <input name="effects" type="checkbox" <%=(user.config.effects?'checked':'')%>>
                                        <label></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>