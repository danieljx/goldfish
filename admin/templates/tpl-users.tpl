<script type="text/template" id="tpl-users">
    <h2>Users</h2>
    <div class="ui icon yellow buttons">
        <button class="addUser ui button" data-tooltip="Crear Usuario" data-position="bottom center" data-inverted="">
            <i class="user plus icon"></i>
        </button>
        <button class="ui button">
            <i class="filter icon"></i>
        </button>
    </div>
    <table class="ui inverted compact striped definition celled table">
        <thead class="full-width">
            <tr>
                <th>Usuario</th>
                <th>Email</th>
                <th>Rol</th>
                <th>Estatus</th>
                <th>Acción</th>
            </tr>
        </thead>
        <tbody></tbody>
        <tfoot class="full-width">
            <tr>
                <th colspan="5"></th>
            </tr>
        </tfoot>
    </table>
</script>
