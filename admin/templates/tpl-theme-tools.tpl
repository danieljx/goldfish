<script type="text/template" id="tpl-theme-tools">
<!-- Template content goes here -->

    <a class="scss-var<% if (!valid.scssVar) { %> disabled<% } %>" href="#" data-tooltip="Variables SCSS" data-position="bottom center">
        <svg viewBox="0 0 32 32"><use xlink:href="/img/icons/sass.svg#icon-1"></use></svg>
    </a>
    <a class="scss-structure<% if (!valid.scssStructure) { %> disabled<% } %>" href="#" data-tooltip="Estructura SCSS" data-position="bottom center">
        <svg viewBox="0 0 32 32"><use xlink:href="/img/icons/css3.svg#icon-1"></use></svg>
    </a>
    <a class="save<% if (!true) { %> disabled<% } %>" href="#" data-tooltip="Save" data-position="bottom center">
        <svg viewBox="0 0 32 32"><use xlink:href="/img/icons/save.svg#icon-1"></use></svg>
    </a>

</script>
