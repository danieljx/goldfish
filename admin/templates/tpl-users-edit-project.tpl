<script type="text/template" id="tpl-users-edit-project"> 
    <h4 class="ui dividing header">Projectos</h4>
    <div class="ui vertical inverted accordion menu">
        <div class="item">
            <a class="title">
                <i class="dropdown icon"></i>
                Goldfish formación
            </a>
            <div class="content">
                <div class="ui form">
                    <div class="grouped fields">
                        <div class="field">
                            <div class="ui checkbox">
                                <input name="small" type="checkbox">
                                <label>Super</label>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui checkbox">
                                <input name="medium" type="checkbox">
                                <label>Admin</label>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui checkbox">
                                <input name="large" type="checkbox">
                                <label>Editor</label>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui checkbox">
                                <input name="x-large" type="checkbox">
                                <label>Control de calidad</label>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui checkbox">
                                <input name="small" type="checkbox">
                                <label>Mantenimiento</label>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui checkbox">
                                <input name="medium" type="checkbox">
                                <label>Diseño</label>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui checkbox">
                                <input name="large" type="checkbox">
                                <label>Animaciones</label>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui checkbox">
                                <input name="x-large" type="checkbox">
                                <label>Componentes</label>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui checkbox">
                                <input name="x-large" type="checkbox">
                                <label>Contacto cliente</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="item">
            <a class="title">
                <i class="dropdown icon"></i>
                Mintkula 
            </a>
            <div class="content">
                <div class="ui form">
                    <div class="grouped fields">
                        <div class="field">
                            <div class="ui checkbox">
                                <input name="small" type="checkbox">
                                <label>Super</label>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui checkbox">
                                <input name="medium" type="checkbox">
                                <label>Admin</label>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui checkbox">
                                <input name="large" type="checkbox">
                                <label>Editor</label>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui checkbox">
                                <input name="x-large" type="checkbox">
                                <label>Control de calidad</label>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui checkbox">
                                <input name="small" type="checkbox">
                                <label>Mantenimiento</label>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui checkbox">
                                <input name="medium" type="checkbox">
                                <label>Diseño</label>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui checkbox">
                                <input name="large" type="checkbox">
                                <label>Animaciones</label>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui checkbox">
                                <input name="x-large" type="checkbox">
                                <label>Componentes</label>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui checkbox">
                                <input name="x-large" type="checkbox">
                                <label>Contacto cliente</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>