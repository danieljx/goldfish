<script type="text/template" id="tpl-theme">
<!-- Template content goes here -->

    <h2>Themes</h2>

    <div class="container one">
        <div class="theme">
            <h4><%= title %></h4>
            <p>
            </p>
            <nav class="tools">
            </nav>
        </div>
        <div class="editor">
            <section class="scss-structure">
                <textarea><%= unitsass %></textarea>
            </section>
            <section class="scss-var">
                <textarea><%= sass %></textarea>
            </section>
        </div>
    </div>


</script>
