<script type="text/template" id="tpl-fonts">
<!-- Template content goes here -->

    <h2>Fonts</h2>

    <div class="container">
    <% _.each(fonts, function(aFont) {  %>
        <div class="font">
            <h4 style="font-family: <%=aFont.title%>"><%=aFont.title%></h4>
            <% if (aFont.id) { %>
                <p class="ok">
                    <svg viewBox="0 0 32 32"><use xlink:href="/img/icons/check.svg#icon-1"></use></svg>
                </p>
            <% } else { %>
                <a href="font/new/<%= aFont.cid %>"></a>
            <% } %>
            <div class="text" style="font-family: <%=aFont.title%>">
                Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            </div>
        </div>
    <% }); %>
    </div>

</script>
