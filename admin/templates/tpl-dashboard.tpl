<script type="text/template" id="tpl-dashboard">
<!-- Template content goes here -->

    <ul>
      <li><a href="/" id="goldfish">
        <svg viewBox="0 0 765 990" class="waiting"><use xlink:href="/img/coi.svg#coi"></use></svg>
        <span>Componentes</span>
      </a></li>

      <li><a href="components" data-tooltip="Components" data-position="right center">
        <svg viewBox="0 0 32 32"><use xlink:href="/img/icons/template.svg#icon-1"></use></svg>
        <span>Componentes</span>
      </a></li>

      <li><a href="themes" data-tooltip="Themes" data-position="right center">
        <svg viewBox="0 0 32 32"><use xlink:href="/img/icons/theme.svg#icon-1"></use></svg>
        <span>Plantillas</span>
      </a></li>

      <li><a href="fonts" data-tooltip="Fonts" data-position="right center">
        <svg viewBox="0 0 30 32"><use xlink:href="/img/icons/font.svg#icon-1"></use></svg>
        <span>Fonts</span>
      </a></li>

      <li><a href="users" data-tooltip="Users" data-position="right center">
        <svg viewBox="0 0 34 32"><use xlink:href="/img/icons/users.svg#icon-1"></use></svg>
        <span>Users</span>
      </a></li>

      <li><a href="/logout" data-tooltip="Desconectar" data-position="right center">
        <svg viewBox="0 0 32 32"><use xlink:href="/img/icons/logout.svg#icon-1"></use></svg>
        <span>Desconectar</span>
      </a></li>
    </ul>

</script>
