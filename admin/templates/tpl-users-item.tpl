<script type="text/template" id="tpl-users-item">
    <td class="name">
        <h4 class="ui image header">
            <% if (user.id == 1) { %>
                <img alt="<%=user.names%> <%=user.surnames%>" class="ui mini rounded image" src="../img/coi.svg#coi">
            <% } else if (user.profile) { %>
                <img alt="<%=user.names%> <%=user.surnames%>" class="ui mini rounded image" src="../user/profile/<%=user.id%>/<%=user.profile%>">
            <% } else { %>
                <img alt="<%=user.names%> <%=user.surnames%>" class="ui mini rounded image" src="../img/icons/user.svg#icon-1">
            <% } %>
            <div class="content">
                <%=user.names%>
                <div class="sub header"><%=user.surnames%></div>
            </div>
        </h4>
    </td>
    <td class="email"><%=user.email%></td>
    <td class="profile">Admin</td>
    <td class="status <%=(user.active?'positive':'error')%>">
        <div class="ui fitted toggle checkbox statusActive">
            <input name="active" type="checkbox" <%=(user.active?'checked':'')%>>
            <label></label>
        </div>
        <span>
            <i class="icon <%=(user.active?'checkmark':'times')%>"></i> 
            <span class="txt"><%=(user.active?'Activo':'Inactivo')%></span>
        </span>
    </td>
    <td class="action">
        <div class="ui right floated icon yellow buttons">
            <button class="edit ui button" data-tooltip="Editar Usuario" data-position="bottom center" data-inverted="">
                <i class="edit icon"></i>
            </button>
            <button class="del ui button" data-tooltip="Borrar Usuario" data-position="bottom center" data-inverted="">
                <i class="trash icon"></i>
            </button>
            <div class="ui small basic modal deleUser">
                <div class="ui icon header">
                    <i class="user times icon"></i>
                    <%=user.names%> <%=user.surnames%>
                </div>
                <div class="content">
                    <p>confirma que desea borrar al usuario?</p>
                </div>
                <div class="actions">
                    <div class="ui red basic cancel inverted button">
                        <i class="remove icon"></i>
                        No
                    </div>
                    <div class="ui green ok inverted button">
                        <i class="checkmark icon"></i>
                        Yes
                    </div>
                </div>
            </div>
        </div>
    </td>
</script>