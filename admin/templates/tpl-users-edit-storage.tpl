<script type="text/template" id="tpl-users-edit-storage">
    <div class="ui stackable two column divided grid container">
        <div class="row">
            <div class="large screen eight wide column">
                <div class="ui segment">
                    <div class="ui middle aligned list">
                        <div class="item">
                            <div class="right floated content">
                                <span class="ui label">547.5</span>
                            </div>
                            <i class="cloud icon"></i>
                            <div class="content">
                                <div class="header">Mi Storage</div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="right floated content">
                                <span class="ui label">547.5</span>
                            </div>
                            <i class="share alternate icon"></i>
                            <div class="content">
                                <div class="header">Archivos compartidos</div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="right floated content">
                                <span class="ui label">547.5</span>
                            </div>
                            <i class="recycle icon"></i>
                            <div class="content">
                                <div class="header">Papelera</div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="right floated content">
                                <span class="ui label">547.5</span>
                            </div>
                            <i class="life ring outline icon"></i>
                            <div class="content">
                                <div class="header">Disponible</div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="content">
                                <div class="ui active yellow small progress">
                                    <div class="bar" style="transition-duration: 300ms; width: 57%;">
                                        <div class="progress">57%</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="large screen eight wide column">
                <div class="ui segment">
                    <h4 class="ui dividing header">Configuración</h4>
                    <div class="field">
                        <svg class="ui image circle storage" width="200" height="200">
                            <circle cx="100" cy="100" r="80" class="loader"></circle>
                            <text class="cloud icon" x=42 y=58></text>
                        </svg>
                    </div>
                    <div class="field">
                        <input type="range" min="0" max="1" value="0" step="0.01" name="storage">
                        <!--<div class="ui range"></div>-->
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="large screen sixteen wide column">
                <div class="ui tertiary segment">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sed diam eget risus varius blandit sit amet non magna. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Cras mattis consectetur purus sit amet fermentum. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Aenean lacinia bibendum nulla sed consectetur.</p>
                </div>
            </div>
        </div>
    </div>
</script>