<script type="text/template" id="tpl-users-edit"> 
    <div class="ui inverted dimmer loadUser">
        <div class="ui small text loader">Loading</div>
    </div>
    <div class="header">
        <label>
            <span class="names"><%=user.names%> </span>
            <span class="surnames"><%=user.surnames%></span>
        </label>
    </div>
    <div class="content">
        <div class="userTab ui top attached tabular menu">
            <a class="item active" data-tab="user">Datos</a>
            <a class="item" data-tab="project">Projectos</a>
            <a class="item" data-tab="storage">Storage</a>
        </div>
        <form class="formUserEdit">
            <div class="dataTab ui bottom attached tab segment active" data-tab="user"></div>
            <div class="projectTab ui bottom attached tab segment" data-tab="project"></div>
            <div class="storageTab ui bottom attached tab segment" data-tab="storage"></div>
        </form>
    </div>
    <div class="actions">
        <div class="ui button cancel">Cancelar</div>
        <div class="ui green button save">Guardar</div>
    </div>
</script>