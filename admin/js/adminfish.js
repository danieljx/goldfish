var adminfish = adminfish || {};

adminfish.start = function(initialModels) {
	this.debug = true;
	if (this.debug) {
		this.log = console.log.bind(window.console);
	} else {
		this.log = function() {};
	}
	this.app = new this.App();
	this.app.setUser( new this.User(initialModels.user) );
	this.themes = new adminfish.ThemesCollection( initialModels.themes );
	this.app.setThemes( this.themes );
	this.app.setComponents( new adminfish.ComponentsCollection( initialModels.components ) );
	this.app.setFonts(new adminfish.FontsCollection(initialModels.fonts));
	this.app.setUsers(new adminfish.UsersCollection(initialModels.users, { parse: true }));
	this.appView = new this._AppView( {model: this.app } );	
	
	// this.config = new this.UserConfig( initialModels.user.config );
	// this.configView = new this.UserConfigView( {model: this.config, el: $('#config') } );
	
	this.router = new this.Router();
	this.log("Starting history..");
	Backbone.history.start({
		pushState: true,
		root: "/admin/"
	});

	// adminfish.log("fetching locally..");
	// this.app.localFetch();
};
    