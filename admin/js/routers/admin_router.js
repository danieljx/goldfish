var adminfish = adminfish || {};

adminfish.Router = Backbone.Router.extend({
		log: true ? console.log.bind(window.console) : function() { },
        routes: {
        	'auth/:auth': 'setAuth',
        	//'*route/auth/:auth': 'setAuth',
			'': 'goComponents',
			'components': 'goComponents',
			'themes': 'goThemes',
			'fonts': 'goFonts',
			'component(/:new)/:componentId': 'goComponent',
			'theme/:themeId': 'goTheme',
			'users(/:page)(/:perPAge)': 'goUsers'
	    },
	    goComponents: function(userId) {
	    	adminfish.log("%cRouter: goComponents",'font-size: 30px');
	    	this.navigate('components');
	    	adminfish.app.setActive('components');
		},
		goThemes: function(userId) {
	    	adminfish.log("%cRouter: goThemes",'font-size: 30px');
	    	this.navigate('themes');
	    	adminfish.app.setActive('themes');
		},
		goFonts: function(userId) {
	    	adminfish.log("%cRouter: goFonts",'font-size: 30px');
	    	this.navigate('fonts');
	    	adminfish.app.setActive('fonts');
		},
		goUsers: function (page, perPAge) {
			adminfish.log("%cRouter: goUsers", 'font-size: 30px');
			if (page) {
				page 	= parseInt(page);
				perPAge = parseInt(perPAge);
				var url = '/api/user/' + adminfish.app.getUser().get("id") + '/users/list/';
				url += page;
				if (perPAge) {
					url += '/' + perPAge;
				}
				adminfish.usersCollection = new adminfish.UsersCollection();
				adminfish.usersCollection.fetch({
					url: url,
					success: function (model, response, options) {
						var t0 = performance.now();
						adminfish.app.setUsers(adminfish.usersCollection);
						adminfish.appView.addSectionView(
							'users',
							new adminfish.UsersView({ collection: adminfish.app.getUsers() }),
							this.$('#users')
						);
						adminfish.app.setActive('users');
						var t1 = performance.now();
						console.log("Public La llamada a hacerAlgo tardó " + (t1 - t0) + " milisegundos.");
					},
					error: function (model, response, options) {
						console.log("%cSave error!", 'color: red;');
						toastr.error(response.responseJSON.msg);
					}
				});
			} else {
				adminfish.app.setActive('users');
			}
		},
		goComponent: function(a,b) {

			adminfish.log("%cRouter: goComponent",'font-size: 30px');
			componentId = (a !== 'new') ? b : false;

			var model = componentId ? 
			adminfish.app.getComponents().get(parseInt(componentId)) :
			adminfish.app.getComponents().get({cid: b });

			adminfish.appView.addSectionView(
				'component', 
				new adminfish.ComponentView({
					model: model
				}),
				adminfish.appView.$('#component') 
			);
			adminfish.app.setActive('component');
		},
		goTheme: function(themeId) {
			adminfish.log("%cRouter: goTheme",'font-size: 30px');
			adminfish.log( adminfish.themes.findWhere({id: parseInt(themeId) }) );
			adminfish.appView.addSectionView(
				'theme', 
				new adminfish.ThemeView({
					model: adminfish.themes.findWhere({id: parseInt(themeId) })				
				}),
				adminfish.appView.$('#theme') 
			);
			adminfish.app.setActive('theme');
		},
	    setAuth: function() {
	    	var auth = arguments[arguments.length > 2 ? 1 : 0]; // 2 y no 1 porque el router pasa un argumento extra
	    	adminfish.app.getUser().set('auth',auth);
	    	if (arguments.length > 2) {
	    		this.navigate(arguments[0], {trigger: true});
	    	}

	    },
	    goProject: function(projectId) {
	    	adminfish.log("%cRouter: goProject",'font-size: 30px');
	    	if (!adminfish.app.getUser().hasProject(projectId)) {
	    		adminfish.log("not user project");
	    		return false;
	    	} else {
	    		adminfish.app.loadProject(projectId, function(model, response, options) {
		    		if (adminfish.projectView) {
		    			adminfish.projectView.remove();
		    		}
		    		adminfish.projectView = new adminfish.ProjectView({model: adminfish.app.getProject() });
		    		$('#project').append(adminfish.projectView.render().el);
		    		adminfish.app.setActive('project');
	    		});
	    	}
	    	// this.navigate('project/')

	    },
	    goFile: function(pathHash) {
	    	adminfish.log("%cRouter: goFile",'font-size: 30px');
			adminfish.app.loadFile(pathHash, function(model, response, options) {
				if (adminfish.fileView) {
					adminfish.fileView.remove();
				}
				var t0 = performance.now();
				adminfish.fileView = new adminfish.UserFilesView({
					collection: adminfish.app.getFile(),
					sharedView: false
				});
				$('#files').html(adminfish.fileView.render().el);
				adminfish.app.setActive('files');
				var t1 = performance.now();
				console.log("Files La llamada a hacerAlgo tardó " + (t1 - t0) + " milisegundos.");
			});
	    },
	    logout: function() {
	    	adminfish.log("%cRouter: logout",'font-size: 30px');
	    	$.ajax({
           type: "GET",
           url: "slim-api/public/auth/logout",

           success: function (msg) {
           	adminfish.log(msg);
               if (msg && msg.result === "ok") {
               	adminfish.log("logout redirect");
               	window.location.replace('/');
               } else {
                   adminfish.log("no response");
               }
           },


       });
	    }
    });
