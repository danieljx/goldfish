var adminfish = adminfish || {};

/** The application
* @author Mats Fjellner
* @augments Backbone.Model
* @constructor
* @name App
*/
adminfish.App = Backbone.Model.extend(/** @lends App */
 {
 	log: false ? console.log.bind(window.console) : function() { },
	defaults: {
		activeFile: false,
		activeUser: false,
		activeUsers: false,
		activeSectionPrev: false,
		activeSection: false,
		thinking: true,
		screen: false,
	},
	/** Creates collection of available components on initialization
	* @construct
	*/
	initialize: function() {

	},
	updateDebugInfo: function() {
		var mem = window.performance.memory;
		this.set("debugInfo", "Memory: "+Math.round(mem.usedJSHeapSize/1000)+"kb" );
		this.set("debugClass", mem.usedJSHeapSize > 30000000 ? "warning" : "");
		window.setTimeout( function() {this.updateDebugInfo(); }.bind(this), 1000);
	},
	/** Sets app user
	* @param {Object} aUser - User Model
   */
	setUser: function(aUser) {
		this.set("activeUser", aUser);
	},
	/** Gets app user
	* @return {Object} User Model
   */
	getUser: function() {
		return this.get("activeUser");
	},
	/** Sets components
	* @param {ComponentCollection} components
   */
  	setComponents: function(components) {
		this.set("components", components);
	},
	/** Gets components
	* @return {Object} Component collection
	*/
	getComponents: function() {
		return this.get("components");
	},
	/** Sets themes
	* @param {ThemeCollection} components
   */
	setThemes: function(themes) {
		this.set("themes", themes);
	},
	/** Gets themes
	* @return {Object} Theme collection
	*/
	getThemes: function() {
		return this.get("themes");
	},
	/** Sets themes
	* @param {FontsCollection} fonts
	*/
	setFonts: function(fonts) {
		this.set("fonts", fonts);
	},
	/** Gets themes
	* @return {Object} Font collection
	*/
	getFonts: function () {
		return this.get("fonts");
	},
	/** Sets themes
	* @param {UsersCollection} users
	*/
	setUsers: function (users) {
		this.set("users", users);
	},
	/** Gets themes
	* @return {Object} User collection
	*/
	getUsers: function () {
		return this.get("users");
	},
	/** Sets active section and triggers event 'switch'
	* @param {string} aSection - Section alias
   */
	setActive: function(aSection) {
		this.attributes.activeSectionPrev = this.attributes.activeSection;
		this.attributes.activeSection = aSection;
		this.trigger("switch");
	},
	routeLink: function(event, link) {
		console.log("Routing link");
		console.log(event);
		console.log(link);
		var href = $(link).attr('href');
		var protocol = link.protocol + '//';
		
		if (href.slice(protocol.length) !== protocol) {
			event.preventDefault();
			adminfish.router.navigate(href, true);
		}
	},
	/** JSON representation
	* @returns {JSON} JSON
   */
	toJSON: function() {
		var json = {
			components: this.get("components"),
			themes: this.get("themes")
			// pages: pages
		};
		console.log(json);
		return json;
	}
} );
