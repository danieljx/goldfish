var adminfish = adminfish || {};

/** Represents a user_config
* @author Daniel Villanueva
* @augments Backbone.Model
* @constructor
* @name UserConfig
*/
adminfish.UserConfig = Backbone.Model.extend({
	urlRoot: function () {
		return '/api/user/' + adminfish.app.getUser().get("id") + '/config/';
	},
	defaults: {
		id: null,
		editor: 'ck',
		theme: 'goldfish',
		effects: 1
	},
	initialize: function(initialModels) {
	}
});