var adminfish = adminfish || {};

/** Represents a user
* @author Daniel Villanueva
* @augments Backbone.Model
* @constructor
* @name User
*/
adminfish.User = adminfish.UserBase.extend({
	urlRoot: function () {
		return '/api/user/' + adminfish.app.getUser().get("id") + '/users/';
	},
	defaults: {
		id: null,
		names: 'Nombre',
		surnames: 'Apellidos',
		email: '',
		profile: false,
		profileBlob: false,
		active: 0,
		action: 'add',
		config: {}
	},
	initialize: function(initialModels) {
	},
	parse: function (response, options) {
		this.attributes.config = new adminfish.UserConfig();
		this.attributes.config.set(response.config);
		return {
			'id': response.id,
			'names': response.names,
			'surnames': response.surnames,
			'profile': response.profile,
			'profileBlob': false,
			'active': response.active,
			'email': response.email,
			'action': 'up',
			'config': this.attributes.config
		};
	}
} );