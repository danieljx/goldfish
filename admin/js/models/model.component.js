var adminfish = adminfish || {};

adminfish.Component = Backbone.Model.extend({
	log: false ? console.log.bind(window.console) : function() { },
	urlRoot: function() {
		return '/api/user/'+adminfish.app.getUser().get("id")+'/component/';
	},
	defaults: {
		title: '[Potatis]',
		types: ['-','Normal', 'Multimedia XML', 'Juego XML', 'Cuestionarios'],
		valid: {
			html: true,
			scss: true,
			json: true,
			all: true
		},
		body: '',
		sass: '',
		data: '{}',
		type: 1,
		repeatable: ''

	},
	initialize: function(e) {
		if (!this.get('data')) {
			this.set('data', '{}');
		}
		return this;
	},
	save: function (attrs, options) { 
	    attrs = attrs || this.toJSON();
	    options = options || {};

	    options.attrs = attrs;
	    Backbone.Model.prototype.save.call(this, attrs, options);
	},
	getTypes: function() {
		return this.get("types");
	},
	validate: function(attrs, options) {
		if (!this.get("valid")) {
			return "invalid";
		} else {
			if (typeof this.get("valid") === 'object') {
				_.each(this.get("valid"), function(a, b) {
					if (!a) {
						return "invalid";
					}
				});

			}
		}
	},
	setValid: function (type, isValid) {
		console.log("setValid("+type+","+isValid+")");
		var validArr = this.get("valid");
		if (validArr[type] !== isValid) {
			console.log("%cvalidation change","color: #a0f");
			validArr[type] = isValid;
			validArr.all = (validArr.html && validArr.scss & validArr.json);
			this.set({ valid: validArr }, {silent: true});
			this.trigger("validityChange");
		}	
	}
	
});