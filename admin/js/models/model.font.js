var adminfish = adminfish || {};

adminfish.Font = Backbone.Model.extend({
	log: false ? console.log.bind(window.console) : function() { },
	urlRoot: function() {
		return '/api/user/'+adminfish.app.getUser().get("id")+'/font/';
	},
	defaults: {
		fontDir: '../dist/css/fonts/',
		weight: 400,
		font_type: 1
	},
	initialize: function(e) {
		return this;
	},
	getCSS: function() {
		var css = "@font-face {\nfont-family: '"+this.get("title")+"';\nfont-style: normal;\nfont-weight: "+this.get("weight")+";\nsrc: local('"+this.get("title")+"'), local('"+this.get("title")+"'), url({fontPath}"+this.get("file_name")+") format('woff');\n}\n";
		return css.replace(new RegExp('{fontPath}', 'g'), this.get("fontDir"));
	},
	save: function (attrs, options) { 
	    attrs = attrs || this.toJSON();
	    options = options || {};

	    options.attrs = attrs;
	    Backbone.Model.prototype.save.call(this, attrs, options);
	},
	toJSON: function() {
		return {
			title: this.get("title"),
			css_stack: '"'+this.get("title")+'", sans-serif',
			weight: this.get("weight"),
			font_type: this.get("font_type"),
			file_name: this.get("file_name")
		};
	}
});