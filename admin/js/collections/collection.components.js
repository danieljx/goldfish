var adminfish = adminfish || {};

adminfish.ComponentsCollection = Backbone.Collection.extend({
	model: adminfish.Component,
	url: '/adminfish/cssattributes/',
	toJSON: function() {
		var tmp = [];
		var types = false;
		this.each( function(item, i) {
			if (!types) {
				types = item.getTypes();
			}
			tmp[i] = {
				id: item.get("id"),
				title: item.get("title"),
				type: types[item.get("type")],
				cid: item.cid
			};
		});
		return tmp;
	}
});