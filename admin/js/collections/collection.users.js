var adminfish = adminfish || {};

adminfish.UsersCollection = Backbone.Collection.extend({
	log: false ? console.log.bind(window.console) : function () { },
	model: adminfish.User,
	url: function () {
		return '/api/user/' + adminfish.app.getUser().get("id") + '/users/list/';
	},
	initialize: function (e) {
		this.page = 1;
		this.perPage = 5;
		//_.bindAll(this, 'parse', 'url', 'pageInfo', 'nextPage', 'previousPage');
		//this.on("change", function () { this.log("%cblocks changed, now " + this.length, 'color: purple; font-style: italic'); });
	},
	fetch: function (options) {
		options = options || {};
		this.trigger("fetching");
		var self = this;
		var success = options.success;
		options.success = function (resp) {
			self.trigger("fetched");
			if (success) { success(self, resp); }
		};
		Backbone.Collection.prototype.fetch.call(this, options);
	},
	parse: function (resp) {
		this.page 		= resp.page;
		this.perPage 	= resp.perPage;
		this.total 		= resp.total;
		return resp.models;
	},
	//url: function () {
	//	return '/api/user/list/' + $.param({ page: this.page });
	//},
	pageInfo: function () {
		var info = {
			total: this.total,
			page: this.page,
			perPage: this.perPage,
			pages: Math.ceil(this.total / this.perPage),
			prev: false,
			next: false
		};

		var max = Math.min(this.total, this.page * this.perPage);

		if (this.total == this.pages * this.perPage) {
			max = this.total;
		}

		info.range = [(this.page - 1) * this.perPage + 1, max];

		if (this.page > 1) {
			info.prev = this.page - 1;
		}

		if (this.page < info.pages) {
			info.next = this.page + 1;
		}
		console.log(info);
		return info;
	},
	nextPage: function () {
		this.page = this.page + 1;
		this.fetch();
	},
	previousPage: function () {
		this.page = this.page - 1;
		this.fetch();
	}
});