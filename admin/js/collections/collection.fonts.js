var adminfish = adminfish || {};

adminfish.FontsCollection = Backbone.Collection.extend({
	model: adminfish.Font,
	url: '/adminfish/cssattributes/',
	comparator: 'title',
	allCSS: function() {
		var tmp = "";
		this.each( function(item, i) {
			tmp += item.getCSS();
		});
		return tmp;
	},
	toJSON: function() {
		var tmp = [];
		this.each( function(item, i) {
			tmp[i] = {
				id: item.get("id"),
				title: item.get("title"),
				cid: item.cid
			};
		});
		return tmp;
	}
});