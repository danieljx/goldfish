var adminfish = adminfish || {};

adminfish.ThemesCollection = Backbone.Collection.extend({
	model: adminfish.Theme,
	url: '/adminfish/pages/',
	// localStorage: new Backbone.LocalStorage("pages"),
	current: 0,
	initialize: function(e) {
		this.on("change", function() {
			// this.updated = true; 
			// adminfish.log("collection changed");
		});
		// this.on("add", function() { adminfish.log("%cpages added, now "+this.length,'color: purple; font-style: italic'); });
		// this.on("change", function() { adminfish.log("%cpages changed, now "+this.length,'color: purple; font-style: italic'); });
		// this.on("remove", function() { adminfish.log("%cpages removed, now "+this.length,'color: purple; font-style: italic'); });
		adminfish.log('loading pages');
	},
	toJSON: function() {
		var tmp = [];
		this.each( function(item, i) {
			tmp[i] = {
				id: item.get("id"),
				title: item.get("title")
			};
		});
		console.log(tmp);
		return tmp;
	}

});