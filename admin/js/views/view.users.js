var adminfish = adminfish || {};

adminfish.UsersView = Backbone.View.extend({
      template: _.template($('#tpl-users').html()),
      tagName: 'div',
      className: 'users-full',
      events: {
            'click .buttons > button.addUser': 'addUser'
      },
      initialize: function () {
            this.listenTo(this.collection, "add", this.renderItem);
      },
      render: function() {
            var me = this;
            this.$el.html(this.template({ users: this.collection.toJSON() }));
            this.$body        = this.$('table.ui.table > tbody');
            this.$tfootPag    = this.$('table.ui.table > tfoot > tr > th');
            this.collection.forEach(function (dataChild) {
                  me.renderItem(dataChild);
            });
            this.pag = new adminfish.UsersPagView({
                  collection: this.collection
            });
            this.$tfootPag.append(this.pag.render().el);
            this.delegateEvents();
            return this;
      },
      postRender: function () {},
      renderItem: function (model) {
            var view = new adminfish.UsersItemView({
                  model: model,
                  collection: this.collection
            });
            this.$body.append(view.render().el);
      },
      addUser: function (e) {
            e.preventDefault();
            e.stopPropagation();
            var dataUser = new adminfish.User();
            dataUser.attributes.config = new adminfish.UserConfig();
            this.edit = new adminfish.UsersEditView({
                  model: dataUser,
                  collection: this.collection
            });
            this.$edit = this.edit.render().$el;
            this.$edit.modal('show');
            return false;
      }
});