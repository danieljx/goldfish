var adminfish = adminfish || {};

adminfish.ComponentsView = Backbone.View.extend({
      template: _.template($('#tpl-components').html()),
      tagName: 'div',
      className: 'components-full',

      initialize: function () {
            this.runningOperations = 0;

            this.listenTo(this.collection, "change add remove", function () {
                  adminfish.log("%cComponent collection changed!", "font-size: 20px");
                  this.render();
            });
      },
      render: function () {

            adminfish.log(this.collection.toJSON());
            this.$el.html(this.template({
                  components: this.collection.toJSON()
            }));
            this.delegateEvents();
            return this;
      },
      postRender: function() {

      },
      events: {
            'click .add': function (e) {
                  console.log("add");
                  e.preventDefault();
                  var newComponent = new adminfish.Component();
                  this.collection.add( newComponent );
                  console.log(this.collection);
                  console.log(adminfish.app.getComponents());
            },
            'click .edit': function (e) {
                  adminfish.app.routeLink(e, e.currentTarget);
            },
            'click .save:not(.disabled)': function (e) {
                  e.preventDefault();
                  console.log("SAVE");
            },
            'click .save-all': function (e) {
                  e.preventDefault();
                  adminfish.appView.setBusy();

                  this.collection.each( function(aComponent) {
                        this.runningOperations++;
                        aComponent.save(false, {wait: true, success: function() { 
                              this.runningOperations--;
                              if (this.runningOperations < 1) {
                                    adminfish.appView.setReady();
                              }
                              
                        }.bind(this)});
                  }.bind(this));
            }
      },

      addUnitDialog: function () {
            adminfish.appView.screenView.setBlock(true);
            // adminfish.log("%cScreen blocked",'color: green');
            this.createUnit = new adminfish.Unit({});
            // adminfish.log("%cUnit created",'color: green');
            // adminfish.log(this.createUnit);
            // adminfish.app.setUnit(this.createUnit);
            this.createUnitView = new adminfish.UnitCreateView({
                  model: this.createUnit
            });
            // adminfish.log("%cUnit view created",'color: green');
            adminfish.appView.screenView.$el.prepend(this.createUnitView.render().$el);
            this.createUnitView.delegateEvents();
            this.createUnitView.focus();
            // adminfish.log("%cUnit created",'color: green');
            // addDialog( _.template($('#tpl-dialog-unit').html(), {}) );
      },
      toggleVisibility: function (visible) {
            if (visible) {
                  this.$el.css('display', 'block');
            } else {
                  this.$el.css('display', 'none');
            }
      },
});