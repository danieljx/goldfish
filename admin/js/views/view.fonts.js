var adminfish = adminfish || {};

adminfish.FontsView = Backbone.View.extend({
   template: _.template($('#tpl-fonts').html()),
   tagName: 'div',
   className: 'fonts-full',
   
   
   initialize: function() {
      
      this.listenTo(this.model, "change", function() { 
         adminfish.log("%cProject model changed!","font-size: 20px");
         this.render(); });

   },
   render: function() {
      // adminfish.log("Project view render");
      // adminfish.log(this.model.toJSON());
      // this.$el.html( this.template(this.model.toJSON()) );
      // return this;

    adminfish.log(this.collection.toJSON());
    this.$el.html( this.template({ fonts: this.collection.toJSON() }) );
    this.delegateEvents();
    return this;
   },
   postRender: function() {

   },
   events: {
      'click a': function(e) {
            e.preventDefault();
            var url = e.target.href.split('/');

            adminfish.appView.setBusy();
            this.collection.get({cid: url[url.length - 1] }).save(false, {wait: true, success: function() { 
                        console.log(this.collection);
                        this.render();
                        adminfish.appView.setReady();
                  }.bind(this)
            });
      }
   },
   
   addUnitDialog: function() {
      adminfish.appView.screenView.setBlock(true);
      // adminfish.log("%cScreen blocked",'color: green');
      this.createUnit = new adminfish.Unit({});
      // adminfish.log("%cUnit created",'color: green');
      // adminfish.log(this.createUnit);
     // adminfish.app.setUnit(this.createUnit);
      this.createUnitView = new adminfish.UnitCreateView( { model: this.createUnit } ); 
      // adminfish.log("%cUnit view created",'color: green');
      adminfish.appView.screenView.$el.prepend( this.createUnitView.render().$el );
      this.createUnitView.delegateEvents();
      this.createUnitView.focus();
      // adminfish.log("%cUnit created",'color: green');
         // addDialog( _.template($('#tpl-dialog-unit').html(), {}) );
   },
   toggleVisibility: function(visible) {
      if (visible) {
         this.$el.css('display','block');
      } else {
         this.$el.css('display','none');
      }
   },
});