var adminfish = adminfish || {};

adminfish.DashBoardView = Backbone.View.extend({
   template: _.template($('#tpl-dashboard').html()),
   log: false ? console.log.bind(window.console) : function() { },
   initialize: function() {
      this.render();
      this.listenTo(this.model, 'change', function() {
        this.render();
      });
	},
  render: function() {
    this.log(this.model.toJSON());
    this.$el.html( this.template(this.model.toJSON()) );
    this.delegateEvents();
    return this;
  },
  events: {
    'click a': function(e) {
      adminfish.app.routeLink(e, e.currentTarget);
    }
  },
  toggle: function(state) {
    this.model.attributes.showMenu = (state !== undefined) ? state : !this.model.attributes.showMenu;
      if (this.model.attributes.showMenu) {
        $('body').toggleClass("full", false);
        this.$el.toggleClass("on", true);
      } else {
        $('body').toggleClass("full", true);
        this.$el.toggleClass("on", false);
      }  
  }
  
});