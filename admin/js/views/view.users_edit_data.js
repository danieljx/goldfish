var adminfish = adminfish || {};

adminfish.UsersEditDataView = Backbone.View.extend({
      template: _.template($('#tpl-users-edit-data').html()),
      tagName: 'div',
      className: 'ui form',
      events: {
            'click .button.userPhotoAdd': 'upFile',
            'change input.userPhotoUp[type=file]': 'upFileChange'
      },
      initialize: function () {
            this.modelConfig = this.model.get('config');
            this.listenTo(this.model, "change:active", this.setActive);
            this.listenTo(this.model, "change:id", this.setId);
            this.listenTo(this.modelConfig, "change:id", this.setConfigId);
      },
      render: function() {
            var me = this, data = this.model.toJSON();
            data.config = this.modelConfig.toJSON();
            this.$el.html(this.template({ user: data }));
            this.$('.special.cards .image').dimmer({
                  on: 'hover'
            });
            this.$fileImg     = this.$("input.userPhotoUp[type=file]");
            this.$userPhoto   = this.$("img.ui.image");
            this.$loadPhoto   = this.$(".loadPhoto");
            this.$status      = this.$('.statusActive.toggle.checkbox');
            this.$status.checkbox();
            this.$('.ui.dropdown').dropdown();
            return this;
      },
      setId: function (model) {
            this.$("input[name=id]").val(model.get('id'));
      },
      setConfigId: function (model) {
            this.modelConfig = model.get('config');
            this.$("input[name=config_id]").val(this.modelConfig.get('id'));
      },
      setActive: function(model) {
            if(model.get('active')) {
                  this.$status.checkbox('check');
            } else {
                  this.$status.checkbox('uncheck');
            }
      },
      upFile: function (e) {
            e.preventDefault();
            e.stopPropagation();
            this.$fileImg.trigger("click");
            return false;
      },
      upFileChange: function (e) {
            e.preventDefault();
            e.stopPropagation();
            var me = this, files = $(e.currentTarget);
            if (FileReader && files && files.length) {
                  this.$loadPhoto.addClass("active");
                  var imgObj  = files[0].files[0];
                  var ext = imgObj.name.substring(imgObj.name.lastIndexOf('.') + 1).toLowerCase();
                  if (ext == "gif" || ext == "png" || ext == "bmp" || ext == "jpeg" || ext == "jpg") {
                        var fr = new FileReader();
                        fr.onload = function () {
                              me.$userPhoto[0].src = fr.result;
                              me.model.set({ 'profileBlob': files[0].files });
                              me.$loadPhoto.removeClass("active");
                        };
                        fr.readAsDataURL(imgObj);
                  } else {
                        toastr.warning("Photo only allows file types of GIF, PNG, JPG, JPEG and BMP. ");
                        this.$loadPhoto.removeClass("active");
                  }
            }
            return false;
      }
});