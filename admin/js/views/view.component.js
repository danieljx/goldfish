var adminfish = adminfish || {};

adminfish.ComponentView = Backbone.View.extend({
      template: _.template($('#tpl-component').html()),
      tagName: 'div',
      className: 'component-edit',

      initialize: function () {
            
            this.cmTheme = 'monokai';
            this.editMode = false;
            this.typeMode = false;
            this.editAttribute = false;
            this.editorTransition = 'fade';
            this.toolView = false;
            this.isNew = this.model.isNew();
      },
      render: function () {
            this.codeMirror = false;
            var JSON = this.model.toJSON();
            JSON.types = this.model.getTypes();
            JSON.body = JSON.body.replace(/</g,'&lt;').replace(/>/g,'&gt;');
            console.log(JSON);
            this.$el.html( this.template( JSON ));
            this.toolView = new adminfish.ComponentToolsView({ model: this.model, el: this.$('.tools') });
            this.toolView.render();
            // this.$('.tools').html( this.toolView.render().el );
            
            this.delegateEvents();

            return this;
      },
      postRender: function() {
            this.$('.ui.dropdown').dropdown();
            // this.switchEditor('.editor .html','htmlmixed');
      },
      events: {
            'input h4': function(e) {
                  e.preventDefault();
                  this.model.set('title', this.$('h4').first().html());
            },
            'change .repeatable': function(e) {
                  e.preventDefault();
                  this.model.set('repeatable', this.$('.repeatable').first().val());
            },
            'change .component-type': function(e) {
                  e.preventDefault();
                  this.model.set('type', $(e.target).val());
            },
            'click nav .html': function (e) {
                  e.preventDefault();
                  this.switchEditor('.editor .html','html');
                  this.editingAttribute = 'body';
            },
            'click nav .scss': function (e) {
                  e.preventDefault();
                  this.switchEditor('.editor .scss','scss');
                  this.editingAttribute = 'sass';
            },
            'click nav .json': function (e) {
                  e.preventDefault();
                  this.switchEditor('.editor .json', 'json');
                  this.editingAttribute = 'data';
            },
            'click .save:not(.disabled)': function (e) {
                  e.preventDefault();
                  this.save();
            },
            'click .save.disabled': function (e) {
                  e.preventDefault();
            },
            'keydown': function(e) {
                  if ((e.ctrlKey || e.metaKey) && e.which == 83) {
                        this.save();
                        e.preventDefault();
                  }
            }
      },
      save: function() {
            adminfish.appView.setBusy();
                  
            if (this.codeMirror) {
               this.saveEditor();
            }

            this.model.save(false, {
                  wait: true, 
                  success: function() { 

                        if (this.isNew) {
                              this.isNew = false;
                              console.log(this.model);
                              adminfish.router.navigate('component/'+this.model.id, true);
                        }
                        
                        adminfish.appView.setReady();

                  }.bind(this)
            });
      },
      getLintUpdate: function (cm, updateLinting, options) {

            var errors = false;
            switch (this.editMode) {
                  case 'scss':
                        console.log("lint css");
                        errors = CodeMirror.lint.scss(cm);
                        break;
                  case 'html':
                        console.log("lint html");
                        errors = CodeMirror.lint.html(cm);
                        break;
                  case 'json':
                        console.log("lint json");
                        errors = CodeMirror.lint.json(cm);
                        break;
            }

            var foundError = false;
            for (var i = 0; i < errors.length; i++) {
                  if (errors[i].severity == "error") {
                        foundError = true;
                        break;
                  }
            }
            this.model.setValid(this.editMode, !foundError);
            updateLinting(errors);
      },
      showEditor: function(selector, type) {
            var typeModes = {
                  html: 'htmlmixed',
                  scss: 'text/x-scss',
                  json: 'application/json'
            };

            this.editMode = type;
            this.typeMode = typeModes[type];

            this.codeMirror = CodeMirror.fromTextArea( 
                  this.$(selector+' textarea')[0],
                  {
                        mode: this.typeMode,
                        theme: this.cmTheme,
                        lineWrapping: true,
                        lineNumbers: true,
                        gutters: ["CodeMirror-lint-markers"],
                        // lint: true
                        lint: {
                              "getAnnotations": this.getLintUpdate.bind(this),
                              "async": true,
                              "options": {
                                    "rules": {
                                        "no-empty-rulesets": 1
                                    }
                                }
                          }
                  }
            );
            
            this.$(selector).transition({
                  animation: this.editorTransition
            });
            this.codeMirror.refresh();
            this.codeMirror.autoFormatRange({line:0, ch:0}, {line:this.codeMirror.lineCount() });
            this.codeMirror.setCursor({line:0, ch:0}); // la llamada anterior selecciona todo el código, esto lo deselecciona
      },
      saveEditor: function() {
            var val = this.codeMirror.getValue().replace(/\n/g,'');
            $(this.codeMirror.getTextArea()).text( val );
            this.model.set(this.editingAttribute, val);
      },
      switchEditor: function(selector, type) {
            if (this.codeMirror) {
                  this.saveEditor();
                  this.$('.editor > section.visible').transition({
                        animation: this.editorTransition,
                        queue: false,
                        onComplete: function() {
                              var textArea = this.codeMirror.getTextArea();
                              this.codeMirror.toTextArea();
                              this.showEditor(selector, type);
                        }.bind(this)
                  });      
            } else {
                  this.showEditor(selector, type);
            }
      },
      toggleVisibility: function (visible) {
            if (visible) {
                  this.$el.css('display', 'block');
            } else {
                  this.$el.css('display', 'none');
            }
      },
});