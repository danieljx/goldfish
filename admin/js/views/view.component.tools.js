var adminfish = adminfish || {};

adminfish.ComponentToolsView = Backbone.View.extend({
      template: _.template($('#tpl-component-tools').html()),
      tagName: 'div',
      className: 'component-edit',

      initialize: function () {
            this.listenTo(this.model, 'validityChange', function() {
                  this.setButtonStates();
            });            
      },
      render: function () {
            console.log("%c-> render tools", "color: #ff0;");
            console.log(this.model.toJSON().valid);

            this.$el.html( this.template( this.model.toJSON() ));
            this.delegateEvents();
            return this;
      },
      setButtonStates: function() {
            this.render();
      },
      postRender: function() {
      },
      events: {
            // Funcionalidad en vista padre ComponentView para menor complejidad
      }


});