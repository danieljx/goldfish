var adminfish = adminfish || {};

adminfish.UsersEditProjectView = Backbone.View.extend({
      template: _.template($('#tpl-users-edit-project').html()),
      tagName: 'div',
      className: 'ui form',
      events: { },
      initialize: function() { },
      render: function() {
            var data = this.model.toJSON();
            data.config = this.model.get('config').toJSON();
            this.$el.html(this.template({ user: data }));
            this.$('.menu.accordion').accordion();
            return this;
      }
});