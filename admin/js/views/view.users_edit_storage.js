var adminfish = adminfish || {};

adminfish.UsersEditStorageView = Backbone.View.extend({
      template: _.template($('#tpl-users-edit-storage').html()),
      tagName: 'div',
      className: 'ui form',
      radius: 80,
      percentage: 0,
      events: {
            'change input[name=storage]': 'changeStorage'
      },
      initialize: function() { },
      render: function() {
            var data = this.model.toJSON();
            data.config = this.model.get('config').toJSON();
            this.$el.html(this.template({ user: data }));
            this.$circle = this.$('circle.loader');
            /*
            this.$('.ui.range').range({
                  min: 0,
                  max: 10,
                  start: 5,
                  onChange: function (value) {
                        console.log(value);
                  }
            });
            */
            return this;
      },
      changeStorage: function(e) {
            e.preventDefault();
            e.stopPropagation();
            var $input = $(e.currentTarget);
            this.percentage = $input.val();
            var dasharray = (Number(this.percentage) * 2 * Number((Number(this.radius) * Math.PI))) + ", " + ((1 - Number(this.percentage)) * 2 * Number((Number(this.radius) * Math.PI)));
            console.log(parseInt(Number(this.percentage)*100) + '%');
            this.$circle[0].style.strokeDasharray = dasharray;
            return false;
      }
});