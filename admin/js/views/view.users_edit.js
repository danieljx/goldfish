var adminfish = adminfish || {};

adminfish.UsersEditView = Backbone.View.extend({
      template: _.template($('#tpl-users-edit').html()),
      tagName: 'div',
      className: 'modalUser ui modal scrolling',
      events: {
            'click .button.save': 'save'
      },
      initialize: function () {
            this.data = new adminfish.UsersEditDataView({
                  model: this.model
            });
            this.project = new adminfish.UsersEditProjectView({
                  model: this.model
            });
            this.storage = new adminfish.UsersEditStorageView({
                  model: this.model
            });
      },
      render: function() {
            var data = this.model.toJSON();
            data.config = this.model.get('config').toJSON();
            this.$el.html(this.template({ user: data }));
            this.$form = this.$('.content form.formUserEdit').attr({
                  id: 'formUserEdit_' + this.model.get("id")
            });
            this.$('.content .dataTab').html(this.data.render().$el);
            this.$('.content .projectTab').html(this.project.render().$el);
            this.$('.content .storageTab').html(this.storage.render().$el);
            this.$('.content .menu .item').tab();
            this.$loadUser = this.$(".loadUser");
            return this;
      },
      getFormData: function () {
            var me = this, dataTemp = {}, dataForm = this.$form.serializeArray();
            var formData = new FormData();
            $.map(dataForm, function (n, i) {
                  if (n.name != 'active' && n.name != 'effects') {
                        formData.append(n.name, n.value);
                  }
            });
            formData.append('active', (this.$form.find('.statusActive > input[name=active]').prop('checked') ? 1 : 0));
            formData.append('effects', (this.$form.find('.statusEffects > input[name=effects]').prop('checked') ? 1 : 0));
            if(this.model.get('profileBlob') instanceof FileList || this.model.get('profileBlob') instanceof Array) {
                  _.each(this.model.get('profileBlob'), function (file) {
                        formData.append('file', file);
                  });
            }
            return formData;
      },
      save: function (e) {
            e.preventDefault();
            e.stopPropagation();
            var me = this;
            this.$el.find('.button.save').toggleClass('disabled', true);
            this.$loadUser.addClass("active");
            console.log(me.model.get('action'));
            var acction = me.model.get('action');
            this.saveData(this.getFormData(), function (response) {
                  console.log("%cSuccess!", "color: green");
                  toastr.success("Datos guardados");
                  response.config = new adminfish.UserConfig(response.config);
                  console.log(me.model.get('action'));
                  me.model.set(response);
                  if (acction == 'add') {
                        me.collection.add(me.model);
                  }
                  me.$el.find('.button.save').toggleClass('disabled', false);
                  me.$loadUser.removeClass("active");
            }, function (response) {
                  console.log("%cSave error!", 'color: red;');
                  toastr.error(response.responseJSON.msg);
                  me.$el.find('.button.save').toggleClass('disabled', false);
                  me.$loadUser.removeClass("active");
            });
            return false;
      },
      saveData: function (data, success, error) {
            console.log(data);
            this.model.save(data, {
                  type: 'POST',
                  data: data,
                  //silent: true,
                  processData: false,
                  contentType: false,
                  success: function (model, response, options) {
                        if (success) {
                              success(response);
                        }
                  },
                  error: function (model, response, options) {
                        if (error) {
                              error(response);
                        }
                  }
            });
      }
});