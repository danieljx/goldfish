var adminfish = adminfish || {};
/** The application view
* @author Mats Fjellner
* @augments Backbone.View
* @constructor
* @name AppView
*/
adminfish._AppView = Backbone.View.extend(/** @lends AppView */
{
   el: 'body',
   /** On init, creates subviews dashBoardView, SecondayMenuView. Also creates screen.
   */
   initialize: function() {
      this.editorInstance = false;
      this.sectionViews = {};

    console.log(this.model);

      this.screenView = new adminfish.ScreenView({model: this.model, el: this.$('.screen') });
      this.dashBoardView = new adminfish.DashBoardView({model: this.model, el: $('#dashboard') });

      this.addSectionView(
        'themes', 
        new adminfish.ThemesView({collection: adminfish.app.getThemes() }),
        this.$('#themes')
      );
      this.addSectionView(
        'components', 
        new adminfish.ComponentsView({collection: adminfish.app.getComponents() }),
        this.$('#components')
      );
      this.addSectionView(
        'fonts', 
        new adminfish.FontsView({collection: adminfish.app.getFonts() }),
        this.$('#fonts')
      );
      this.addSectionView(
        'users',
        new adminfish.UsersView({ collection: adminfish.app.getUsers() }),
        this.$('#users')
      );
      $('head').append('<style id="font-css">'+adminfish.app.getFonts().allCSS()+'</style>');

      // this.secondaryMenuView = new adminfish._AppSecondaryMenuView({ model: this.model, el: $('#secondary-menu') });
   		
      this.listenTo(this.model, "switch", function() { 
        adminfish.log("switching app view...");
        // this.setBusy();
        var newView = this.getSectionView(this.model.get('activeSection'));
        newView.el.append( newView.view.render().el );
        newView.view.postRender();
        if (this.model.get('activeSectionPrev')) {
          var oldView = this.getSectionView(this.model.get('activeSectionPrev'));
          oldView.view.undelegateEvents();
          oldView.el.transition({
            animation: 'scale',
            onComplete: function() {
              newView.el.transition({
                animation: 'scale'
              });    
            }});
        } else {
          newView.el.transition({
            animation: 'scale'
          });
        }
        
        
        // this.setReady();
        this.editingBlock = false;
      });
      
      this.dashBoardToggle();
      this.screenView.setReady();
	},
  /** Returns section view corresponding to alias
 * @param {string} aStr - Section alias
 * @return {Object|boolean} The view, or false if not found.
   */
  getSectionView: function(viewName) {
    return _.find(this.sectionViews, function(val, key) {
      return (key == viewName);
    });
  },
  addSectionView: function(name, view, el) {
    var oldView = this.getSectionView(name);
    if (oldView) {
      adminfish.log("Removing old view on "+name);
      adminfish.log(oldView);
      oldView.view.remove();
    }

    this.sectionViews[name] = { 
      view: view,
      el: el
    };
  },
  /** Toggles effects
 * @param {boolean} state - Explicitly sets state
   */
  setEffects: function(state) {
    this.toggleExtraCSS('effects', state ? '' : 'body * { transition: none !important; }');
  },
  /** Sets app ready for interaction.
   */
	setReady: function() { // Mejor cambiar por transition + .one()
    this.screenView.setReady();
	},
  /** Sets app busy (blocks interaction).
   */
  setBusy: function() { // Mejor cambiar por transition + .one()
    this.screenView.setBusy();
	},
  /** Shows message to User
  * @param {string} msg - Message
   */
	showMsg: function(msg) {
    this.screenView.showMsg(msg);
	},
/** Map of event handlers
    * @type Object
   */
	events: {
    /** Listens for clicks on .dashboard-toggle, toggles dashboard.
     * @lends AppView
   */
      'click > aside > .dashboard-toggle': function(e)/** @lends AppView */ {
         e.preventDefault();
         this.dashBoardToggle();
      },
     /** Listens for clicks on .editor-toggle, toggles editor for components.
     * @lends AppView
   */ 
      'click .editor-toggle': function(e) {
         if (this.editorInstance && this.editingBlock) {
            this.editingBlock.set('body', this.editorInstance.getData());
            this.editorInstance.destroy();
            this.editorInstance = false;
            this.setReady();
         }
      },
      'keydown' : 'keydownHandler'
  },
  /** Listens for keyboard clicks, ESC closes preview OR closes editor, dashboard and secondary menu
  * @lends AppView
   */
  keydownHandler : function (e) {
  },
  /** Sets theme
  * @param {string} aTheme
   */
  setTheme: function(aTheme) {
    this.setBusy();
    var themeHref = $('#css-theme').attr('href');
    themeHref = themeHref.split('/');
    themeHref[themeHref.length - 1] = aTheme+".css";
    themeHref = themeHref.join('/');
    $('#css-theme').attr('href', themeHref);
    this.setReady();
  },
  /** Toggles unit preview
  * @param {boolean} toggle - On/Off
   */
  showPreview: function(toggle) {
    // this.model.attributes.showPreview = html && (html !== '');
    this.model.attributes.showPreview = toggle;

    if (this.model.attributes.showPreview) {
      // this.toggleExtraCSS('preview-css',css);
      // $('#preview').html( html );
      // $('#preview').toggleClass('on');
    } else {
      // $('#preview').html( );
      // $('#preview').toggleClass('on');
      // this.toggleExtraCSS('preview-css',false);
      this.unitView.previewPage(false);
    }
    

  },
  /** Toggles dashboard
  * @param {boolean} state - On/Off
   */
  dashBoardToggle: function(state) {
      this.dashBoardView.toggle(state);
   },
   /** Toggle secondary menu
  * @param {boolean=} state - On/Off
   */
  //  secondaryMenuViewToggle: function(state) {
  //    console.log("SECONDARY TOGGLE");
  //     this.model.attributes.showSecondaryMenu = (state !== undefined) ? state : !this.model.attributes.showSecondaryMenu;
  //     if (this.model.attributes.showSecondaryMenu) {
  //       // this.dashBoardToggle(false);
  //       this.secondaryMenuView.loadComponents(adminfish.unitView.getComponentViews());
  //       adminfish.unitView.shrink();
  //       this.secondaryMenuView.toggle(true);
  //     } else {
  //       adminfish.unitView.expand();
  //       this.secondaryMenuView.toggle(false);
  //     }
  //  },
   /** Adds UnitView
  * @param {Object} aUnitView
   */
   setUnitView: function(aUnitView) {
      this.unitView = aUnitView;
      this.model.setUnit(aUnitView.model);
      this.setReady();
   },
   /** Toggles custom CSS, on if cssStr != ''
  * @param {string} cssId - DOM ID
  * @param {string=} cssStr - CSS String
   */
   toggleExtraCSS: function(cssId, cssStr) {
    if ($('head #'+cssId).length === 0) {
      $('head').append('<style id="'+cssId+'">'+cssStr+'</style>');
    } else {
      if (cssStr) {
        $('head #'+cssId).html(cssStr);
      } else {
        $('head #'+cssId).remove();  
      }
      
    }
   },
   /** Loads component CSS in secondary menu and shows the menu
  * @param {Object} aBlock - Component
   */
  //  secondaryMenuLoadCSS: function(aBlock) {
  //   this.secondaryMenuView.loadCSS(aBlock);
  //   this.secondaryMenuViewToggle(true);
  //  },
   /** Loads component HTML editor in secondary menu and hides the menu
  * @param {Object} aBlock - Component
   */
   loadEditor: function(aBlock) {
      this.editingBlock = aBlock;
      adminfish.unitView.expand();
      this.$('#secondary-menu').removeClass("on");
      this.screenView.editorView();
      this.editorInstance = CKEDITOR.appendTo( $('#content-holder')[0], {}, aBlock.get('body') );
   }
});