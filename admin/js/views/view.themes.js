var adminfish = adminfish || {};

adminfish.ThemesView = Backbone.View.extend({
   template: _.template($('#tpl-themes').html()),
   tagName: 'div',
   className: 'themes-full',
   
   initialize: function() {
      this.runningOperations = 0;
   },
   render: function() {
      adminfish.log(this.collection.toJSON());
      this.$el.html( this.template({ themes: this.collection.toJSON() }) );
      this.delegateEvents();
      return this;
   },
   postRender: function() {

   },
   events: {
      'click .unit-add': function(e) {
         e.preventDefault();
         this.addUnitDialog();
      },
      'click .edit': function (e) {
            adminfish.app.routeLink(e, e.currentTarget);
      },
      'click .save-all': function (e) {
            e.preventDefault();
            adminfish.appView.setBusy();

            this.collection.each( function(aTheme) {
                  this.runningOperations++;
                  aTheme.save(false, {wait: true, success: function() { 
                        this.runningOperations--;
                        if (this.runningOperations < 1) {
                              adminfish.appView.setReady();
                        }
                  }.bind(this)});
            }.bind(this));
      }
   }
});