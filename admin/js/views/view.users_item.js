var adminfish = adminfish || {};

adminfish.UsersItemView = Backbone.View.extend({
      template: _.template($('#tpl-users-item').html()),
      tagName: 'tr',
      className: 'users-full-item',
      events: {
            'click td.action > div > button.edit': 'editUser',
            'click td.action > div > button.del': 'delUserConfirm'
      },
      initialize: function () {
            this.listenTo(this.model, "change:profile", this.setProfile);
            this.listenTo(this.model, "change:names", this.setNames);
            this.listenTo(this.model, "change:surnames", this.setNames);
            this.listenTo(this.model, "change:email", this.setEmail);
            this.listenTo(this.model, "change:active", this.setActive);
            this.listenTo(this.model, "remove", this.removeItem);
            this.edit = new adminfish.UsersEditView({
                  model: this.model,
                  collection: this.collection
            });
            this.$edit = this.edit.render().$el;
      },
      render: function() {
            var me = this;
            this.$el.html(this.template({ user: this.model.toJSON() }));
            this.$tdName      = this.$('td.name');
            this.$tdEmail     = this.$('td.email');
            this.$tdProfile   = this.$('td.rol');
            this.$tdStatus    = this.$('td.status');
            this.$tdAction    = this.$('td.action');
            this.$('.statusActive.checkbox').checkbox({
                  onChecked: function () {
                        me.model.set({ 'active': 1 });
                        me.model.save(me.model.attributes, {
                              success: function (model, response, options) {
                                    console.log("%cSuccess!", "color: green");
                                    toastr.success("Datos guardados");
                              },
                              error: function (model, response, options) {
                                    console.log("%cSave error!", 'color: red;');
                                    toastr.error(response.responseJSON.msg);
                              }
                        });
                  },
                  onUnchecked: function () {
                        me.model.set({ 'active': 0 });
                        me.model.save(me.model.attributes, {
                              success: function (model, response, options) {
                                    console.log("%cSuccess!", "color: green");
                                    toastr.success("Datos guardados");
                              },
                              error: function (model, response, options) {
                                    console.log("%cSave error!", 'color: red;');
                                    toastr.error(response.responseJSON.msg);
                              }
                        });
                  }
            });
            this.$del = this.$('.ui.modal.deleUser');
            this.$del.modal({
                  onApprove: function () {
                        me.delUser();
                  }
            });
            return this;
      },
      removeItem: function () {
            this.undelegateEvents();
            this.$el.removeData().unbind();
            this.remove();
            Backbone.View.prototype.remove.call(this);
      },
      delUser: function () {
            var me = this;
            this.model.set('action', 'del');
            this.model.destroy({
                  wait: true,
                  processData: true,
                  success: function (model, response, options) {
                        console.log("%cSuccess!", "color: green");
                        toastr.success("Usuario " + me.model.get('names') + " " + me.model.get('surnames') + " Borrado");
                  },
                  error: function (model, response, options) {
                        console.log("%cSave error!", 'color: red;');
                        toastr.error(response.responseJSON.msg);
                  }
            });
      },
      delUserConfirm: function (e) {
            e.preventDefault();
            e.stopPropagation();
            this.$del.modal('show');
            return false;
      },
      editUser: function (e) {
            e.preventDefault();
            e.stopPropagation();
            this.$edit.modal('show');
            return false;
      },
      setProfile: function (model) {
            var imgSrc = '../img/icons/user.svg#icon-1';
            if (model.get('id') == 1) {
                  imgSrc = '../img/coi.svg#coi';
            } else if (model.get('profile')) {
                  imgSrc = '../user/profile/' + model.get('id') + '/' + model.get('profile');
            }
            this.$tdName.find('h4.ui.image.header > img').attr({ src: imgSrc });
      },
      setNames: function (model) {
            this.$tdName.find('div.content').html(model.get('names') + '<div class="sub header">' + model.get('surnames') + '</div>');
      },
      setEmail: function (model) {
            this.$tdEmail.html(model.get('email'));
      },
      setActive: function (model) {
            if (model.get('active')) {
                  this.$tdStatus.removeClass('error').addClass('positive');
                  this.$tdStatus.find('i.icon').removeClass('times').addClass('checkmark');
                  this.$tdStatus.find('span.txt').html('activo');
                  this.$tdStatus.find('.statusActive.toggle.checkbox').checkbox('set checked');
            } else {
                  this.$tdStatus.removeClass('positive').addClass('error');
                  this.$tdStatus.find('i.icon').removeClass('checkmark').addClass('times');
                  this.$tdStatus.find('span.txt').html('Inactivo');
                  this.$tdStatus.find('.statusActive.toggle.checkbox').checkbox('set unchecked');
            }
      }
});