var adminfish = adminfish || {};

adminfish.UsersPagView = Backbone.View.extend({
      template: _.template($('#tpl-users-pag').html()),
      tagName: 'div',
      className: 'ui right floated pagination menu',
      events: {
            'click a.prev': 'previous',
            'click a.next': 'next'
      },
      initialize: function () {
            this.listenTo(this.collection, "refresh", this.render);
      },
      render: function() {
            var me = this;
            this.$el.html(this.template(this.collection.pageInfo()));
            return this;
      },
      previous: function () {
            this.collection.previousPage();
            return false;
      },
      next: function () {
            this.collection.nextPage();
            return false;
      }
});