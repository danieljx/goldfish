<?php
require('../backend/general.php');
// Importante: build.php es la fuente de index.php, que se genera desde el anterior a traves de gulp
if (!isset($_SESSION['user_id'])) {
  include('../start.php');
  die();
}

$user_id = $_SESSION['user_id'];
$user = file_get_contents(getServer().'api/user/'.$user_id);
$components = file_get_contents(getServer().'api/user/'.$user_id.'/components/');
$themes = file_get_contents(getServer().'api/user/'.$user_id.'/themes/');
$fonts = file_get_contents(getServer().'api/user/'.$user_id.'/fonts/');
$users = file_get_contents(getServer().'api/user/'.$user_id.'/users/list/');

if (!isJson($components) || !isJson($themes) || !isJson($user) || !isJson($config) || !isJson($fonts)) {
  die("Error getting initial models.");
}
?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>Goldfish Admin</title>
  <base href="/admin/">
  <link rel="stylesheet" id="css-theme" href="/dist/css/admin.css">
	<link rel="stylesheet" href="/dist/lib/semantic/dist/semantic.min.css">
	<link rel="stylesheet" href="/dist/lib/codemirror/lib/codemirror.css">
	<link rel="stylesheet" href="/dist/lib/codemirror/addon/lint/lint.css">
	<link rel="stylesheet" href="/dist/lib/codemirror/theme/twilight.css">
	<link rel="stylesheet" href="/dist/lib/codemirror/theme/dracula.css">
	<link rel="stylesheet" href="/dist/lib/codemirror/theme/monokai.css">
	<link rel="stylesheet" href="/dist/lib/codemirror/theme/material.css">
	<link rel="stylesheet" href="/dist/lib/codemirror/theme/seti.css">
	<link rel="stylesheet" href="/dist/lib/codemirror/theme/hopscotch.css">
  	<link rel="stylesheet" href="../node_modules/toastr/build/toastr.min.css">
</head>
<body class="admin">
	<!-- <h2>Admin</h2> -->
	
	<section id="components">
	</section>

	<section id="component">
	</section>
	
	<section id="themes">
	</section>

	<section id="theme">
	</section>

	<section id="fonts">
	</section>

	<section id="users">
	</section>

	<aside id="dashboard">		
	</aside>

	<div class="screen">
	<svg viewBox="0 0 32 32" class="waiting"><use xlink:href="/img/icons/spinner2.svg#icon-1"></use></svg>
	</div>

	<?php include('../dist/admin-templates.html'); ?>
	<script type="text/javascript" charset="utf-8" src="/dist/js/bundle.admin-lib.js"></script>
	<script type="text/javascript" charset="utf-8" src="/dist/js/cthulhu.min.js"></script>
  	<script src="../node_modules/toastr/build/toastr.min.js"></script>
	<script>
	adminfish.start({
		user: <?php echo($user); ?>, 
		config: <?php echo($config); ?>, 
		components: <?php echo($components); ?>, 
		themes: <?php echo($themes); ?>, 
		fonts: <?php echo($fonts); ?>,
		users: <?php echo($users); ?>
	});
  </script>
</body>
</html>