<?php
error_reporting(E_ALL);
require('../../vendor/autoload.php');
require('../../backend/general.php');

$pageSass = file_get_contents(PHYSDIR.'/admin/sample/demo.scss');
$scss = new scssc();
$varSass = '$textColor: #fff; $backgroundColor: #333; $panelColor: #aaa; $accentColor: #555;';

if (isset($_REQUEST["sass"])) {
	$varSass = $_REQUEST["sass"];
}

$scss->setFormatter('scss_formatter_compressed');
$css = $scss->compile($varSass.' '.$pageSass);
?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <style>
  <?php echo($css); ?>
  </style>
  <title>Theme demo</title>
</head>
<body>
	<nav>
		<a href="">Developers</a>
		<a href="">Developers</a>
		<a href="">Developers</a>
		<a href="">Developers</a>
	</nav>
	<header>
		<h1>Schrödinger's Cat</h1>
	</header>
	<section>
		<p>
		Lorem ipsum dolizzle yippiyo amizzle, shit adipiscing elit. Pimpin' sapizzle velizzle, black volutpizzle, cool quis, gravida vel, arcu. Pellentesque bling bling tortor. Sizzle erizzle. Cool izzle dolor dapibizzle turpizzle break it down shut the shizzle up. Maurizzle check out this nibh izzle funky fresh. Vestibulum izzle dizzle. Pellentesque eleifend yo sheezy. In shizznit habitasse platea dictumst. Brizzle dapibizzle. Crackalackin tellizzle boom shackalack, shizznit eu, mattizzle ac, eleifend yo, fo shizzle. Check it out suscipizzle. Integizzle sempizzle velit crazy purus.
		</p>
		<p>
		Nulla eu its fo rizzle. For sure porta commodo tellus. Aenean gizzle, shiz at fo shizzle hendrerizzle, libero dizzle hendrerit leo, non condimentum nunc sapien at nizzle. Donizzle eu dolizzle. Vestibulum quizzle felizzle. Sed check out this faucibizzle eros. Pimpin' fizzle its fo rizzle, crunk izzle, bling bling egizzle, auctor eget, phat. Sizzle izzle. Nunc nisi. Bling bling sit own yo' gangster nizzle ante blandit dignissim. Quisque laoreet tellizzle sheezy we gonna chung enim. Aenean sure dignissizzle shiznit.
		</p>
	<section>
</body>