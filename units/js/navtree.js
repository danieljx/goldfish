function treeNav(aSel) {
	  		this.selector = aSel;
	  		this.el = $(this.selector);
	  		
	  		this.data = false;
	  		this.root = false;

	  		this.loadData = function(data) {
	  			this.data = data;
	  		};

	  		this.render = function() {
	  			this.root = d3.hierarchy(this.data, function(d) {
	            	return d.children;
	        	});
	        	this.update();
	  		};

	  		this.update = function() {
	  			var me = this;

    	if (!this.treeG) {
    		this.treeG = d3.select(this.selector).append("svg").append("g");	
    		this.treemap = d3.tree();
    		this.tooltip = d3.select(this.selector)
            .append("div")
            .classed("tooltip", true);
            this.selectedNode = null;
            this.draggingNode = null;
            this.selected = null;
            this.root = d3.hierarchy(this.data, function(d) {
            	return d.children;
        	});
    	}
    	
        // Set the dimensions and margins of the diagram
        var margin,
            width,
            height,
            maxDim,
            circleR,
            panBoundary = 20;

        var diagonal = d3.linkHorizontal()
            .x(function(d) { return d.y; })
            .y(function(d) { return d.x; });

        var i = 0,
            duration = 300;

        window.addEventListener("resize", function() { this.update(this.root); }.bind(this));

            margin = this.el.height() * 0.05;
            height = this.el.height() - 2 * margin;
            width = this.el.width() - 2 * margin;
            maxDim = Math.max(width, height);
            circleR = height / 40;

            this.root.sort(function(a, b) {
                return b.data.position < a.data.position ? 1 : -1;
            });

            this.root.x0 = circleR;
            this.root.y0 = width / 2;

            this.treemap.size([width, height]);

            this.treeG.attr("transform", "translate(" + margin + "," + margin + ")");

            var maxDepth = 0;
            this.treeData = this.treemap(this.root.sort());

            this.nodes = this.treeData.descendants();
			this.links = this.treeData.descendants().slice(1);

            this.nodes.forEach(function(d) {
                maxDepth = Math.max(maxDepth, d.depth);
            });

            this.nodes.forEach(function(d) {
                d.y = d.depth * height / maxDepth;
            });

            this.link = this.treeG.selectAll('path.link').
            data(this.links, function(d) {
                return d.id;
            });

            this.linkEnter = this.link.enter().insert("path", "g")
                .attr("class", "link")
                .attr("d", function(d) {
                    return "L" + d.x + "," + d.y;
                });

            this.linkUpdate = this.linkEnter.merge(this.link);

            this.linkUpdate.transition().
            duration(duration).
            attr("d", function(d) {
                return "L" + d.x + "," + d.y;
            });

            this.linkExit = this.link.exit().remove();

            this.node = this.treeG.selectAll('g.node')
                .data(this.nodes, function(d) {
                    return d.id || (d.id = ++i);
                });

            this.nodeEnter = this.node.enter().
            append('g').
            attr('class', function(d) { console.log(d.data); return d.data.visited ? 'node visited' : 'node'; }).
            attr('id', function(d) { return 'node-' + d.id }).
            attr("transform", function(d) {
                return "translate(" + this.root.x0 + "," + this.root.y0 + ")";
            }.bind(this)).
            on('click', click);

            this.nodeEnter.on("mouseover", function(d) {
                    this.tooltip.
                    text(d.data.title).
                    style("top", d.y + margin + "px").
                    style("left", d.x + margin + circleR * 2 + "px");

                    return this.tooltip.style("opacity", 1);
                }.bind(this))
                .on("mouseout", function() { return this.tooltip.style("opacity", 0); }.bind(this))

            this.nodeEnter.append('circle').attr('r', circleR);

            this.nodeEnter.append("circle")
                .attr('class', 'ghostCircle')
                .attr("r", circleR * 2)
                .attr("opacity", 0.2) // change this to zero to hide the target area
                .attr('pointer-events', 'mouseover')
                .on("mouseover", function(node) {
                    overCircle(node);
                })
                .on("mouseout", function(node) {
                    outCircle(node);
                });

            this.nodeUpdate = this.nodeEnter.merge(this.node);

            this.nodeUpdate
                .attr('class', function(d) { console.log(d.data); return d.data.visited ? 'node visited' : 'node'; })
                .transition()
                .duration(duration)
                .attr("transform", function(d) {
                    return "translate(" + d.x + "," + d.y + ")";
                });

            this.nodeUpdate.select('circle.node').
            attr('r', circleR).
            attr('cursor', 'pointer');

            this.nodeExit = this.node.exit().
            transition().
            duration(duration).
            attr("transform", function(d) {
                return "translate(" + this.root.y + "," + this.root.x + ")";
            }.bind(this)).
            remove();

            this.nodeExit.select('circle').attr('r', 0);

            this.nodes.forEach(function(d) {
                d.x0 = d.x;
                d.y0 = d.y;
            });

            function click(d) {
                me.selected = d;
                d3.selectAll('.node:not(#' + this.id + ')').classed('selected', false);
                d3.select(this).classed('selected', true);
                d.data.visited = true;
                me.update(d);
                console.log(d.data.i);
                
                showPage(d.data.i);
            }

            function setNodeDepth(d) {
                d.depth = d == me.root ? 0 : d.parent.depth + 1;
                if (d.children) {
                    d.children.forEach(setNodeDepth);
                } else if (d._children) {
                    d._children.forEach(setNodeDepth);
                }
            }

	  		};
	  		
	  	}