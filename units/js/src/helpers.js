var fishunit = fishunit || {};

function helpersClass () {
    
    /** Function that count occurrences of a substring in a string;
     * @param {String} string               The string
     * @param {String} subString            The sub string to search for
     * @param {Boolean} [allowOverlapping]  Optional. (Default:false)
     *
     * @author Vitim.us https://gist.github.com/victornpb/7736865/edit
     * @see Unit Test https://jsfiddle.net/Victornpb/5axuh96u/
     * @see http://stackoverflow.com/questions/4009756/how-to-count-string-occurrence-in-string/7924240#7924240
     */
    this.stringN = function (string, subString) {
        // Modificado, no necesitamos contar con strings solapados
        string += "";
        subString += "";
        if (subString.length <= 0) return (string.length + 1);

        var n = 0,
            pos = 0,
            step = subString.length; 

        while (true) {
            pos = string.indexOf(subString, pos);
            if (pos >= 0) {
                ++n;
                pos += step;
            } else break;
        }
        return n;
    };

    this.isEmpty = function(obj) {
        for(var key in obj) {
            if(obj.hasOwnProperty(key))
                return false;
        }
        return true;
    };

    this.objMerge = function(obj1, obj2) {
        var obj3 = {};
        for(var key in obj1) {
            if(obj1.hasOwnProperty(key))
                obj3[key] = obj1[key];
        }
        for(var key2 in obj2) {
            if(obj2.hasOwnProperty(key2))
                obj3[key2] = obj2[key2];
        }
        return obj3;
    };

    this.replaceAll = function(haystack, needle, replace) {
        return haystack.replace(new RegExp(needle,'g'), replace);
    };
}



fishunit.Helpers = new helpersClass();

document.addEventListener("contextmenu", function(e) {
  e.preventDefault();
});