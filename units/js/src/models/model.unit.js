var fishunit = fishunit || {};

/** Represents a learning unit
* @author Mats Fjellner
* @augments Backbone.Model
* @constructor
* @name Unit
*/
fishunit.Unit = Backbone.Model.extend( /** @lends Unit */
 {
 	/** Sets API base URL based on App.getUser() and App.getProject
	* @return {string} URL
	*/
	log: true ? console.log.bind(window.console) : function() { },
	defaults: {
		currentPage: false,
		showIntro: false,
		showNav: false,
		page: false,
		data: {
			config: {
				transitionIn: {
					type: 'transition',
					value: 'scale'
				},
				transitionOut: {
					type: 'transition',
					value: 'scale'
				}
			}
		}
	},
	/** Creates collection of available pages on initialization
	* @construct
	*/
	initialize: function(initialModels) {
		this.log("%cUNIT INITIALIZE "+this.cid,"color: green;");
		console.log(this.attributes);
	},
	/** Initializes backend change listener through SSE (https://developer.mozilla.org/en-US/docs/Web/API/Server-sent_events)
	*/
	initSSE: function() {
		this.sse = new EventSource('/backend/events.php?unit='+this.get('id')+'&saved='+this.get('saved'));
		this.sse.onmessage = this.onSSE.bind(this);
		this.sse.addEventListener("open", function(e) {
		}, false);
		this.sse.addEventListener("error", function(e) {
    		goldfish.log("Error SSE.");
    		goldfish.log(e);
		}, false);
	},
	/** SSE event handler
	* @param {MessageEvent} e - MessageEvent object (https://developer.mozilla.org/en-US/docs/Web/API/MessageEvent)
	*/
	onSSE: function(e) {
		if (e.data > this.get("saved")) {
			window.location.reload(true);
		}
	},
	/** Toggles secondary view
	* @param {boolean} state - off/on
   */
	navToggle: function(state) {
		newState = (typeof(state) !== "undefined") ? state : !this.get("showSecondaryMenu");
		this.set("showNav", newState);
   },
	/** Gets pages in unit
	* @return {Collection} Pages
	*/
	getPages: function() {
		return this.attributes.page;
	},
	/** Gets pages in unit
	* @return {Object} Page
	*/
	setCurrentPage: function(aCid) {
		this.log("setCurrentPage "+aCid);
		var aPage = this.findPage(aCid);
		if (aPage) {
			aPage.seen();
			this.set('currentPage',aPage);	
		}
		
	},
	/** Gets pages in unit
	* @return {Collection} Pages
	*/
	getCurrentPage: function() {
		return this.get('currentPage');
	},
	/** Gets page by index
	* @param {integer} index
	* @return {Object} Page
	*/
	getPageByIndex: function(index) {
		return this.attributes.pages.at(index);
	},
	findPage: function(aCid) {		
		return this.get("page").findPage(aCid);
	},
	/** Parses server response and sets pages
	* @return {Object} Attributes + pages
	*/
	parse: function(response, xhr) {
		// if (response.pages.constructor === Array) {
		// 	fishunit.log("%cpages array","font-weight: bold");
		// }
		this.log("%cUnit parse!", "color: #0ff; font-size: 30px;");
		if (response.pages) {
			this.log("has pages");
			this.attributes.page = this.attributes.page ? this.attributes.page : new fishunit.Page({root: true});
			this.attributes.page.set(this.attributes.page.parse(response.pages));
			this.log("resulting page");
			this.log(this.get("page"));
		} else {
			this.log("no pages");
			if (!this.isNew()) {
				this.attributes.page = new fishunit.Page({root: true, title: new Date().getTime(), position: 0});
			}
			
		}

		var parseObj = {
			title: response.title,
			id: response.id,
			saved: response.saved,
			author: response.author,
			data: response.data || this.defaults.data,
			cid: response.cid,
			theme: response.theme,
			page: this.get("page")
		};

		if (response.themes) {
			parseObj.themes = new fishunit.ThemesCollection( response.themes );
		}
		
		return parseObj;
	},
	/** JSON representation including id + title of units
	* @return {JSON} JSON
	*/
	toJSON: function() {
		this.log("%cunit toJSON","color: green");
		var json = {
			title: this.get('title'),
			saved: this.get('saved'),
			author: this.get('author'),
			data: this.get('data'),
			cid: this.cid,
			theme: this.get('theme'),
			pages: this.get('page') ? this.get("page").toJSON() : '',
			currentPage: this.get('currentPage')
		};
		if (this.get('id')) {
			json.id = this.get('id');
		}
		if (this.get('themes')) {
			json.themes = this.attributes.themes.toJSON();
		}
		return json;
	},
	pagesJSON: function() {

	}
} );


