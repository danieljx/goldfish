var fishunit = fishunit || {};

/** Represents a learning unit page
* @author Mats Fjellner
* @augments Backbone.Model
* @constructor
* @name Page
*/
fishunit.Page = Backbone.Model.extend( /** @lends Page */ {
	log: true ? console.log.bind(window.console) : function() { },
	defaults: {
		title: '',
		body: "<html><head></head><body>default model page</html>",
		updated: false,
		showMenu: false,
		block: false,
		pages: false,
		render_html: '',
		render_css: '',
		visited: false
		// blockView: false,
		// blocks: false,
		// blockViews: {}
	},
	/** Creates collection of available projects on initialization, creates default user component
	* @construct
	*/
	initialize: function(initialModels) {
		this.log("%cPAGE INITIALIZE "+this.cid,"font-size: 30px;");
		this.on("change", function() {
			this.log("%cPage change!","color: green; font-size: 30px;");
			this.updated = true; 
		});

		

		// this.attributes.pages = new fishunit.PagesCollection();	

		// if (initialModels.pages) {
		// 	this.set("pages", new fishunit.Page(initialModels.pages));
		// }

		// if (initialModels.block) {
		// 	this.set("block", new fishunit.UserBlock( initialModels.block ));
		// }

	},
	/** Registers user component view
	* @param {integer} id - View id
	* @param {Object} View - Component View
   */
  	registerUserBlockView: function(aCid, aView) {
		this.trigger("blockRegister",aCid,aView);
	},
	/** Unregisters user component view
	* @param {integer} id - View id
	*/
	unregisterUserBlockView: function(aCid) {
		this.trigger("blockUnregister",aCid);
	},
	/** Sets this page as "seen"
	*/
	seen: function() {
		this.set("visited",true);
	},
	/** Returns page within this and child pages
	* @param {integer} cid - View cid
	* @return {Object} Attributes
	*/
	findPage: function(aCid) {
		if (this.cid == aCid) {
			this.log("found "+aCid);
			return this;
		} else {
			if (this.get("pages").length > 0) {
				var subPageFind = false;
				this.get("pages").each(function(model){
					if (!subPageFind) {
						subPageFind = model.findPage(aCid);
					}
				});	
				return subPageFind;
				
			} else {
				return false;
			}
		}
	},
	/** Adds child page, new last position
	* @param {Object} attrs - Child page attributes OR a model
	 */
	addChild: function(attrs) {
		var isModel = attrs instanceof Backbone.Model;
		var itemModel;
		var position = this.get("pages").length; // Agregar al final
		if (isModel) {
			this.log("model add");
			itemModel = attrs;
			this.log(itemModel);
		} else {
			this.log("model create");
			itemModel = new fishunit.Page(attrs);
		}
		itemModel.set('position', position);
		this.get("pages").add(itemModel);

	},
	/** Removes child page, rearranges child positions
	* @param {integer} aCid - Child page cid
	 */
	removeChild: function(aCid, options) {
		options = options || {};
		var child = this.findPage(aCid);
		var childPos = child.get("position");
		var childModel = this.get("pages").remove(child, options);
		this.get("pages").forEach( function(aPage) {
			var pos = aPage.get("position");
			if (pos > childPos) {
				aPage.set("position", pos - 1);
			}
		});
		return childModel;

	},
	/** Parses server response
	* @return {Object} Attributes
	*/
	parse: function(response, xhr) {
		this.log("%cPage parse!", "color: #0ff; font-size: 30px;");
		this.log(response);
		var block;
		if (!this.attributes.block) {
			this.log("blockless page, adding");
			this.attributes.block = new fishunit.UserBlock({main: true});
		} else {
			this.log("modifying existing block");
		}
		
		if (response.block) {
			this.log("setting block");
			this.attributes.block.set(this.attributes.block.parse(response.block));
		}

		if (!this.attributes.pages) {
			this.attributes.pages = new fishunit.PagesCollection();
		}
		if (response.pages && response.pages.length > 0) {
			this.attributes.pages.set(response.pages, {parse: true});
		}

		// this.log(block);
		return {
			title: response.title,
			id: response.id,
			cid: response.cid,
			position: response.position,
			block: this.attributes.block,
			pages: this.attributes.pages
		};
	},
	/** JSON representation including base component
	* @return {JSON} JSON
	*/
	toJSON: function() {
		var json = {
			title: this.get('title'),
			// render_html: this.get('render_html'),
			block: this.get('block').toJSON(),
			cid: this.cid,
			position: this.get("position"),
			pages: this.get('pages').toJSON(),
			visited: this.get('visited')
		};

		if (this.get('id')) {
			json.id = this.get('id');
		}
		return json;
	}
});