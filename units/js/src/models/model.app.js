var fishunit = fishunit || {};

/** The application
* @author Mats Fjellner
* @augments Backbone.Model
* @constructor
* @name App
*/
fishunit.App = Backbone.Model.extend(/** @lends App */
 {
 	log: false ? console.log.bind(window.console) : function() { },
	defaults: {
		thinking: true,
		dragging: false,
		screen: false
	},
	/** Creates collection of available components on initialization
	* @construct
	*/
	initialize: function() {
	},
	/** Toggles 'dragging' state
	* @param {boolean} dragging - On/Off
   */
	setDragging: function(dragging) {
		this.attributes.dragging = dragging;
		this.trigger("dragchange");

	},
	/** Returns 'dragging' state
	* @return {boolean} On/Off
   */
	isDragging: function() {
		return this.attributes.dragging;
	},
	/** JSON representation
	* @returns {JSON} JSON
   */
	toJSON: function() {

		return {
			user: this.get("activeUser").toJSON(),
			sseMessages: this.get("sseMessages"),
			activeProject: this.get("activeProject") ? this.get("activeProject").toJSON() : '',
			showDebug: this.get("showDebug"),
			debugInfo: this.get("debugInfo"),
			debugClass: this.get("debugClass")
			// pages: pages
		};
	}
} );
