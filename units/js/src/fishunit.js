var fishunit = fishunit || {};

fishunit.start = function(initialModels, sseUpdate) {

	this.debug = true;
	if (this.debug) {
		this.log = console.log.bind(window.console);
	} else {
		this.log = function() {};
	}

	i18next.init({
		lng: 'es',
		debug: true,
		resources: {
			es: {
				translation: initialModels.i18n
			},
		}
	}, 
	function(err, t) {
		// init set content
	});
	
	this.i18n = i18next;
	this.app = new this.App();
	this.unit = new this.Unit(initialModels.unit, {parse: true});
	if (sseUpdate) {
		this.unit.initSSE();
	}
	this.appView = new this.AppView( {model: this.app } );
	this.unitView = new this.UnitView({model: this.unit, el: $('body') });
	
	$('#config .ui.checkbox.toggle').checkbox();
	$('#config .ui.dropdown').dropdown();
	// this.router = new this.Router();
	// Backbone.history.start();
	
	// this.app.initSSE();
};
    