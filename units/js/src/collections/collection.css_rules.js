var fishunit = fishunit || {};

fishunit.CssRulesCollection = Backbone.Collection.extend({
	model: fishunit.CssRule,
	url: '/fishunit/cssrules/',
	current: 0,
	save: function(callback, context){
		this.each( function(item, i) {
			item.save(); 
		});
		callback.apply();
		// Backbone.sync('create', this, { success: callback });
 	}
});