var fishunit = fishunit || {};

fishunit.CssAttributesCollection = Backbone.Collection.extend({
	model: fishunit.CssAttribute,
	url: '/fishunit/cssattributes/',
	current: 0,
	save: function(callback, context){
		this.each( function(item, i) {
			item.save(); 
		});
		callback.apply();
		// Backbone.sync('create', this, { success: callback });
	 },
	 toJSON: function() {
		 var json = {};
		 this.each( function(item, i) {
			var modelJSON = item.toJSON();
			if (modelJSON.isCustom) {
				json[modelJSON.name] = modelJSON.customValue;
			}
			// if (!fishunit.Helpers.isEmpty(modelJSON)) {
			// 	for(var k in modelJSON) {
			// 		if (modelJSON.hasOwnProperty(k)) {
			// 			json[k]=modelJSON[k];
			// 		}
			// 	}
			// }
		 });
		//  console.log("COLLECTION");
		//  console.log(json);
		 return json;
	 }
});