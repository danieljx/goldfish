var fishunit = fishunit || {};

fishunit.CssQueriesCollection = Backbone.Collection.extend({
	model: fishunit.CssQuery,
	url: '/fishunit/cssqueries/',
	current: 0,
	initialize: function() {
		this.on('change', function() {
		});
		this.on('add', function(query) {
		});
	},
	save: function(callback, context){
		this.each( function(item, i) {
			item.save(); 
		});
		callback.apply();

		// Backbone.sync('create', this, { success: callback });
 	}
});