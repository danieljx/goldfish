var fishunit = fishunit || {};

fishunit.StructureViewAccordeon = Backbone.View.extend({
    log: true ? console.log.bind(window.console) : function() {},
    initialize: function() {
        this.getData();
    },
    getData: function() {
        this.log("%c> getData",'background-color: #0ff; color: #022;');
        var JSONmodel = this.model.get("page").toJSON();
        this.currentPage = this.model.get("currentPage");
        
        this.data = JSON.parse(JSON.stringify(JSONmodel), function(k, v) { // Cambiando propiedad "pages" de modelo a "children" para D3
            if (k === "pages") {
                this.children = v;
            } else {
                if (k === "block") {
                    delete this.block;
                } else {
                    return (k === "position") ? v*1 : v;
                }
            }
        });
        this.log(this.data);
        this.newData = true;
    },
    renderChildren: function(subEl) {
        var html = '<li';
        console.log(subEl.cid);
        console.log(this.model.get('currentPage'));

        if (subEl.cid == this.model.get('currentPage').cid || (!this.model.get('currentPage') && subEl.cid == this.data.cid)) {
            html+= ' class="current"';
        }
        html += '><a href="#'+subEl.cid+'"><span><span></span></span>'+subEl.title+'</a>';
        if (subEl.children.length > 0) {
            html += '<ul>';
            for (var i = 0; i < subEl.children.length; i++) {
                html = html + this.renderChildren(subEl.children[i]);
            }
            html += '</ul>';
        }
        html += '</li>';
        return html;
    },
    render: function() {
        this.$el.html('');
        this.$el.addClass('accordeon');
        this.$el.append($('<ul>'+this.renderChildren(this.data)+'</ul>'));
        return this;
    },
    events: {
        'click a': function(e) {
            e.preventDefault();
            var cid = e.currentTarget.href.split('#')[1];
            console.log(cid);
            fishunit.unit.setCurrentPage(cid);
            this.render();
        }
    },
    update: function() {
        this.log("%c> update",'background-color: #0ff; color: #022;');
    }
});