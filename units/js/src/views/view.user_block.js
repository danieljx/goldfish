var fishunit = fishunit || {};

/** User page component view
 * @author Mats Fjellner
 * @augments Backbone.View
 * @constructor
 * @name UserBlockView
 */
fishunit.UserBlockView = Backbone.View.extend( /** @lends UserBlockView */ {
	tagName: 'div',
	className: 'block',
	// repeatTool: '<div class="repeat-tool cat-ui"><svg viewBox="0 0 32 32" width="0" height="0" class="repeat-add"><use xlink:href="img/icons/plus.svg#icon-1"></use></svg><svg viewBox="0 0 32 32" width="0" height="0" class="repeat-remove"><use xlink:href="img/icons/minus.svg#icon-1"></use></svg></div>',
	debug: true,
	viewName: 'UserBlockView',
	log: true ? console.log.bind(window.console) : function() { },
	initialize: function (options) {
		// console.log(this.log);
		this.log('%c%s', 'background-color: red; color: black', "whateva man");
		// this.setLog();
		this.log("Hola");
		this.log("%cinit user block view " + this.cid + "(" + this.model.get("name") + ")", "font-size: 30px; color: green");
		
		this.subViews = {};
		this.showCss = false;
		this.showConfig = false;
		this.css = '';
		this.page = false;
		this.pluginFunctions = this.model.get("pluginFunctions");
		this.model.on('attachView', this.attach);
		this.listenTo(this.model, "change", this.render);

		this.listenTo(this.model, "css-change", function () {
			this.log("CSS Change");
			this.updateCSS();
		});

		this.listenTo(this.model, "remove", function () {
			this.log("%cComponent view remove " + this.cid, "font-size:30px; color: #fa0;");
			this.page.unregisterUserBlockView(this.cid);
			fishunit.appView.toggleExtraCSS((this.viewState === 'edit' ? 'block-' : 'previewblock-') + this.model.cid, this.css);
			this.remove();
		});
		this.page = fishunit.unit.getCurrentPage();
		this.page.registerUserBlockView(this.cid, this);
	},
	childRemove: function (options) {
		this.log("%cChild change in " + this.cid, "color: #0ff; font-size: 30px;");
		this.log(options);
		delete this.subViews[options];
	},
	/** Renders the current component, along with nested components interspersed with drop zones and toolbar
	 */
	render: function () {
		this.log("%cUSER BLOCK RENDER " + this.model.get("name") + " " + this.cid, "color: #0ff; font-size: 30px;");
		var me = this; // Alias to use when this refers to another context
		var tmp, tmp2;
		var cssQueries = this.model.get('cssQueries');
		var blocks = this.model.getBlocks(); // Subblocks
		// this.log("%crendering user block "+this.cid+" with "+blocks.length+" blocks.","color: green");
		me.css = '';

		this.$el.html('');
		this.log( this.model.attributes );
		
		var q = 23;
		this.pluginFunction('render', q);

		var JSON = this.model.toJSON();
		JSON.viewMode = true;
		this.log(JSON);
		this.log(this.model.get("body"));
		var body = _.template( this.model.get("body") )( JSON );
		this.log(body);

		switch (this.model.get("instanceOf")) {
			case 8: // Cuestionario
				// body = $('<div>'+body+'</div>').find("> section").css("display","block");
				body = $('<div>'+body+'</div>');
				body.find("> section:not(:first-child)").css("display", "none");
				body = body.html();

				break;
			case 9: // Pregunta
				break;
			default:
				break;
		}
		
		this.$el.html( body );

		this.log(blocks);
		this.log(this.subViews);

		this.$el.find('blocks').each(function (i, el) {
			this.log("found block in " + this.model.get("body") + ", i:" + i + ", blocks.length: " + blocks.length);
			var groupBlocks = this.model.get("blocks").where({
				group: i
			});
			
			var newEl = $(document.createDocumentFragment());

			for (var j = 0; j < groupBlocks.length; j++) {
				if (!this.subViews[groupBlocks[j].cid]) {
					this.subViews[groupBlocks[j].cid] = new fishunit.UserBlockView({
						model: groupBlocks[j]
					});
					if ((this.model.get("instanceOf") == 8) && (i == 1)) {
						if (!this.questions) {
							this.questions = [];
						}
						this.questions.push(this.subViews[groupBlocks[j].cid]);
					}
				}

				newEl = newEl.append(this.subViews[groupBlocks[j].cid].render().attach());
				// newEl = newEl.add(this._dropZone(false, i, j + 1));
			}

			switch (this.model.get("instanceOf")) {
				case 8: // Cuestionario
					if (i == 1) {
						this.log(newEl);
						newEl.find('> .block').not(':eq(0)').css('display', 'none');
					}
					break;
				case 9: // Pregunta
					break;
				default:
					break;
			}

			$(el).replaceWith(newEl);
		}.bind(this));

		if (!this.model.get('main')) {

		} else {
			this.$el.addClass('main');
		}

		this.$el.attr('id', 'block-' + this.model.cid);
		this.$el.attr('data-group', this.model.get('group'));
		this.$el.attr('data-position', this.model.get('position'));
		this.$el.attr('data-cid', this.cid);

		this.delegateEvents();
		this.log("%cUSER BLOCK RENDER FINISH " + this.model.get("name") + " " + this.cid, "color: #0ff; font-size: 30px;");
		this.postRender();
		this.updateCSS();


		// fishunit.appView.toggleExtraCSS((this.viewState === 'edit' ? 'block-' : 'previewblock-')+this.model.cid, this.css);
		// me.model('rendered_css',me.model('rendered_css')+this.css);

		return this;
	},
	pluginFunction: function(fName, args) {
		if (this.pluginFunctions && this.pluginFunctions[fName]) {
			var a = new Function(this.pluginFunctions[fName]);
			return a.call(this, args);
		} else {
			return false;
		}
	},
	updateCSS: function () {
		var css = '';
		this.model.get('cssQueries').each(function (el) {
			this.log("%cGETTING BLOCK CSS QUERIES FOR VIEW " + this.cid, "font-size: 30px; color: #aaf");
			css += el.getCSSwithId(this.model.cid);
			// this.log( el.getCSSwithId(this.model.cid) );
		}.bind(this));

		fishunit.appView.toggleExtraCSS('block-' + this.model.cid, css);
	},
	postRender: function () {
		this.$('.ui.checkbox.toggle').checkbox();
	},


	events: function () {
		var _events = {
		};
		if (this.model.get("eventHandlers")) {
			
			var specificHandlers = this.model.get("eventHandlers");
			if (this.model.get("instanceOf") == 8) {
				this.log("Fixing "+specificHandlers.length+"eventHandlers ");
			}
			for (var i = 0; i < specificHandlers.length; i++) {
				this.log(specificHandlers[i]);
				_events[specificHandlers[i].event] = new Function(specificHandlers[i].body);
			}
		}


		return _events;
	},
	_isAttached: function () {
		return (this.$el.parent().length > 0);
	},
	attach: function () {
		// this.render();
		return this.el;
	},
	detach: function () {
		// this.viewState = 'view';
		var blockViews = this.model.get('blockViews');
		this.log(blockViews);
		for (var blockView in blockViews) {
			if (blockViews.hasOwnProperty(blockView)) {
				this.log(blockViews[blockView]);
				blockViews[blockView].detach();
			}
		}
		// this.model.get('blockViews').each( function(el) {
		// 	el.detach();
		// });
		this.remove();
	},
	updateModel: function () {
		var el = $('<div>'+this.$el.html()+'</div>');

		el.find('.cat-ui').remove();
		el.find('.cat-dropzone').remove();
		el.find('.repeat-tool').remove();

		this.model.set('body', el.html(), {
			silent: true
		});
		return true;
	},
	_findIndex: function (context) {
		var level;
		if ($(this).hasClass('cat-dropzone')) {
			context.log("finding dropzone index");
			var tmp = $(this).attr('id').split('-');
			level = $(this).parents('.block').length;
			index = $(this).closest('.block').find('.cat-dropzone').filter(
				function () {
					return $(this).parents('.block').length === level;
				}).index($(this));
			return [tmp[2] * 1, tmp[3] * 1, index];
		} else {
			// TODO: group + position
			context.log("finding block index");
			level = $(this).parents('.block').length;

			return $(this).parent().closest('.block').find('.block').filter(
				function () {
					return $(this).parents('.block').length === level;
				}).index($(this));
		}
	},
	remove: function () {
		fishunit.appView.toggleExtraCSS((this.viewState === 'edit' ? 'block-' : 'previewblock-') + this.model.cid, '');
		this.page.unregisterUserBlockView(this.cid);
		// recurse
		Backbone.View.prototype.remove.call(this);
	}
});