var fishunit = fishunit || {};
/** The application view
* @author Mats Fjellner
* @augments Backbone.View
* @constructor
* @name AppView
*/
fishunit.AppView = Backbone.View.extend(/** @lends AppView */
{
   el: 'body',
   /** On init, creates subviews dashBoardView, SecondayMenuView. Also creates screen.
   */
   initialize: function() {

	},
  /** Sets app ready for interaction.
   */
	setReady: function() { // Mejor cambiar por transition + .one()
    // this.screenView.setReady();
	},
  /** Sets app busy (blocks interaction).
   */
	setBusy: function() { // Mejor cambiar por transition + .one()
    // this.screenView.setBusy();
	},
  /** Shows message to User
  * @param {string} msg - Message
   */
	showMsg: function(msg) {
    this.screenView.showMsg(msg);
	},
/** Map of event handlers
    * @type Object
   */
	events: {
      'keydown' : 'keydownHandler'
  },
  /** Listens for keyboard clicks, ESC closes preview OR closes editor, dashboard and secondary menu
  * @lends AppView
   */
  keydownHandler : function (e) {

    switch (e.which) {
      // esc
      case 27 :
      break;
    }

  },
  /** Toggles dashboard
  * @param {boolean} state - On/Off
   */
  dashBoardToggle: function(state) {
      this.dashBoardView.toggle(state);
   },
   /** Toggle secondary menu
  * @param {boolean=} state - On/Off
   */
  //  secondaryMenuViewToggle: function(state) {
  //    console.log("SECONDARY TOGGLE");
  //     this.model.attributes.showSecondaryMenu = (state !== undefined) ? state : !this.model.attributes.showSecondaryMenu;
  //     if (this.model.attributes.showSecondaryMenu) {
  //       // this.dashBoardToggle(false);
  //       this.secondaryMenuView.loadComponents(fishunit.unitView.getComponentViews());
  //       fishunit.unitView.shrink();
  //       this.secondaryMenuView.toggle(true);
  //     } else {
  //       fishunit.unitView.expand();
  //       this.secondaryMenuView.toggle(false);
  //     }
  //  },
   /** Toggles custom CSS, on if cssStr != ''
  * @param {string} cssId - DOM ID
  * @param {string=} cssStr - CSS String
   */
   toggleExtraCSS: function(cssId, cssStr) {
    if ($('head #'+cssId).length === 0) {
      $('head').append('<style id="'+cssId+'">'+cssStr+'</style>');
    } else {
      if (cssStr) {
        $('head #'+cssId).html(cssStr);
      } else {
        $('head #'+cssId).remove();  
      }
      
    }
   }
});