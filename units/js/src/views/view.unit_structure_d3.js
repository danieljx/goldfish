var fishunit = fishunit || {};

fishunit.StructureViewD3 = Backbone.View.extend({
    template: _.template($('#tpl-nav-d3').html()),
    log: true ? console.log.bind(window.console) : function() {},
    initialize: function() {
        this.treeG = false;
        this.on = false;
        this.getData();
        console.log(this.data);
        this.listenTo(this.model, "change:currentPage", function() { this.setVisited(this.model.get("currentPage").get("id")); }.bind(this) );
    },
    getData: function() {
        this.log("%c> getData",'background-color: #0ff; color: #022;');
        var JSONmodel = this.model.get("page").toJSON();
        this.currentPage = this.model.get("currentPage");
        console.log(JSONmodel);
        this.data = JSON.parse(JSON.stringify(JSONmodel), function(k, v) { // Cambiando propiedad "pages" de modelo a "children" para D3
            if (k === "pages") {
                this.children = v;
            } else {
                if (k === "block") {
                    delete this.block;
                } else {
                    return (k === "position") ? v*1 : v;
                }
            }
        });
        this.newData = true;
        return this;
    },
    render: function() {
        this.$el.addClass('d3');
        this.$el.html( this.template(this.model.toJSON()) );
        // this.setElement('<section id="structure"></section>');
        // alert(this.$el.width());
        return this;
    },
    loadPage: function(aCid) {
        this.model.setCurrentPage(aCid);
    },
    events: {
        'click .nav-open': function(e) {
            e.preventDefault();
            this.toggle();
            
        }
    },
    toggle: function(force) {
        this.on = (force !== 'undefined') ? force : !this.on;
        $('body').toggleClass('blur', this.on);
        this.$el.toggleClass('on', this.on);
    },
    returnSanitized: function(node) {
        var sanNode = {};
        sanNode.title = node.data.title;
        sanNode.position = node.data.position;
        sanNode.pages = [];
        if (node.children) {
            node.children.forEach(function(child, index, array) {
                sanNode.pages.push(this.returnSanitized(child));
            }.bind(this));
        }
        return sanNode;
    },
    setVisited: function(id) {
        // d3.select('.node#node-' + id).classed('visited', true);
        this.getData().update();

    },
    initTree: function() {
        this.log("%c> initTree",'background-color: #0ff; color: #022;');
        this.treeG = d3.select(this.el).select(".main svg").append("g");
        this.root = d3.hierarchy(this.data, function(d) {
            return d.children;
        });
        this.treemap = d3.tree();
        this.tooltip = d3.select('#structure')
            .append("div")
            .classed("tooltip", true);
        this.selectedNode = null;
        this.draggingNode = null;
        this.dragStarted = false;
        this.selected = null;
        this.duration = 300;

        window.addEventListener("resize", function() {
            this.update(this.root);
        }.bind(this));
    },
    update: function() {
        this.log("%c> update",'background-color: #0ff; color: #022;');
        var me = this;
        if (!this.treeG) {
            this.initTree();
        } 
        if (this.newData) {
            this.root = d3.hierarchy(this.data, function(d) {
                return d.children;
            });
            this.log(this.data);
            this.newData = false;
        }

        this.root.sort(function(a, b) {
            return b.data.position*1 < a.data.position*1 ? 1 : -1;
        });

        var i = 0;

        this.margin = this.$el.height() * 0.05;
        this.height = this.$el.height() - 2 * this.margin;
        this.width = this.$el.width() - 2 * this.margin;
        this.maxDim = Math.max(this.width, this.height);
        

        this.log("%cw/h: "+this.width+"/"+this.height,'background-color: #0ff; color: #022;');

        this.root.x0 = this.width / 2;
        this.root.y0 = this.height / 2;

        

        this.treemap.size([this.width, this.height]);

        this.treeG.attr("transform", "translate(" + this.margin + "," + this.margin + ")");

        this.maxDepth = 1;
        this.treeData = this.treemap(this.root.sort());

        this.nodes = this.treeData.descendants();

        this.links = this.treeData.descendants().slice(1);
        this.nPerDepth = {};
        this.nodes.forEach(function(d) {
            this.maxDepth = Math.max(this.maxDepth, d.depth);
             if (d.depth in this.nPerDepth) {
                this.nPerDepth[d.depth] += 1; 
            } else {
                this.nPerDepth[d.depth] = 1;
            }
        }.bind(this));
        console.log("%cnPerDepth:","font-size: 30px; color: red;");
        console.log(Math.max.apply(null, _.values(this.nPerDepth)));

        console.log(this.nodes);

        this.circleR = this.maxDim / (this.maxDepth * 10);
        this.circleR = Math.min(this.circleR, this.maxDim / 40);

        this.nodes.forEach(function(d) {
            d.y = d.depth * this.height / this.maxDepth;
            if (this.selected && (d.data.cid == this.selectedCid)) {
                this.selected = d; // Actualizar cual es el nodo seleccionado, cambian ids después al actualizar datos
            }
        }.bind(this));

        this.link = this.treeG.selectAll('path.link').
        data(this.links, function(d) {
            return d.id;
        });

        this.linkEnter = this.link.enter().insert("path", "g")
            .attr("class", "link")
            .attr("d", function(d) {
                return "M" + d.x + "," + d.y +
                    "C" + d.x + "," + (d.y + d.parent.y) / 2 +
                    " " + (d.x + d.parent.x) / 2 + "," + d.parent.y +
                    " " + d.parent.x + "," + d.parent.y;
            });

        this.linkUpdate = this.linkEnter.merge(this.link);

        this.linkUpdate.transition().
        duration(this.duration).
        attr("d", function(d) {
            return "M" + d.x + "," + d.y +
                "C" + d.x + "," + (d.y + d.parent.y) / 2 +
                " " + (d.x + d.parent.x) / 2 + "," + d.parent.y +
                " " + d.parent.x + "," + d.parent.y;
        });

        this.linkExit = this.link.exit().remove();

        this.node = this.treeG.selectAll('g.node')
            .data(this.nodes, function(d) {
                return d.id || (d.id = ++i);
            });

        this.nodeEnter = this.node.enter().
        append('g').
        attr('class', 'node').
        attr('data-content', function(d) { return d.data.title; }).
        attr('id', function(d) { return 'node-' + d.id; }).
        attr("transform", function(d) {
            return "translate(" + this.root.x0 + "," + this.root.y0 + ")";
        }.bind(this)).
        on('click', click);
        
        this.nodeEnter.append('circle').attr('r', this.circleR);

        this.nodeEnter.append("circle")
            .attr('class', 'inner')
            .attr("r", this.circleR * 0.8);

        this.nodeUpdate = this.nodeEnter.merge(this.node);

        this.nodeUpdate
            .attr('class', function(d) { console.log(d.data); return d.data.visited ? 'node visited' : 'node'; })
            .transition()
            .duration(this.duration)
            .attr("transform", function(d) {
                return "translate(" + d.x + "," + d.y + ")";
            });

        this.nodeUpdate.select('circle').
        attr('r', this.circleR).
        attr('cursor', 'pointer');

        this.nodeExit = this.node.exit().
        transition().
        duration(this.duration).
        attr("transform", function(d) {
            return "translate(" + this.root.y + "," + this.root.x + ")";
        }.bind(this)).
        remove();

        this.nodeExit.select('circle').remove();

        this.nodes.forEach(function(d) {
            d.x0 = d.x;
            d.y0 = d.y;
        });

        function click(d) {
            me.selected = d;
            me.log(d.data.title);
            me.selectedCid = d.data.cid;
            d3.selectAll('.node:not(#' + this.id + ')').classed('selected', false);
            d3.select(this).classed('selected', true);
            me.toggle(false);
            fishunit.unit.setCurrentPage(d.data.cid);
            // me.update(d);
        }

    }
});