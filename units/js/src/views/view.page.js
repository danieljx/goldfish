var fishunit = fishunit || {};

/** The application view
* @author Mats Fjellner
* @augments Backbone.View
* @constructor
* @name PageView
*/
fishunit.PageView = Backbone.View.extend(/** @lends PageView */{
	log: true ? console.log.bind(window.console) : function() { },
	tagName: 'div',
	className: 'page',
	initialize: function(options) {
		this.userBlockViews = [];

		if (!this.model.get('data')) {
			this.model.set('data', fishunit.unit.get('data'));
		}
		this.model.on('attachView', this.attach);
		// this.model.on('change', function(e) {
		// 	fishunit.log("model changed!");
		// 	this.render();
		// }, this);
		this.listenTo(this.model, "blockRegister", this.registerUserBlockView);
		this.listenTo(this.model, "blockUnregister", this.unregisterUserBlockView);
		this.blockView = new fishunit.UserBlockView({model: this.model.get('block')});
	},
	events: {
	},
	render: function() {
		this.log("%crender page view",'color: green');
		var startTime = window.performance.now();

		this.$el.html('<h2>'+this.model.get('title')+'</h2>');

		this.$el.append(this.blockView.render().attach());
		this.$el.transition({animation: this.model.get("data").config.transitionIn.value });

		if (this._isAttached()) {
			this.postRender(this);
		}
		var endTime = window.performance.now();
		fishunit.log("%cRender time: "+(endTime - startTime)+"ms","color: blue");
	    return this;
	},
	_isAttached: function() {
		return (this.$el.parent().length > 0);
	},
	postRender: function(context) {
		this.delegateEvents();
	},
	// _findBlockView: function(aCid) {
	// 	return this.getBlockViewByCid(aCid);
	// },     
	// getBlockViewByCid: function(aCid) {
	// 	return this.attributes.blockViews[aCid];
	// },
	/** Registers user component view
	* @param {integer} id - View id
	* @param {Object} View - Component View
   */
	registerUserBlockView: function(aCid, aView) {
		this.userBlockViews[aCid] = aView;
	},
	/** Unregisters user component view
	* @param {integer} id - View id
   */
	unregisterUserBlockView: function(aCid) {
		delete this.userBlockViews[aCid];
	},
	/** Gets user component view 
	* @param {integer} id - View id
	* @return {Object} View - Component View
   */
	getUserBlockViewByCid: function(aCid) {
		fishunit.log(this.userBlockViews);
		return this.userBlockViews[aCid];
	},
	attach: function() {
		return this.$el;
	},
	detach: function(aCallback, args, context) {
		fishunit.log("%cDetaching view "+this.cid+" with el","color: red; font-size: 20px");
		fishunit.log(this.$el);

		this.blockView.detach();
		this.$el.transition( {animation: this.model.get("data").config.transitionOut.value, onComplete: function() { this.remove(); aCallback(); }.bind(this) } );
		// this.remove();
	},
	config: function(state) {
		console.log("page config");
		fishunit.appView.secondaryMenuViewToggle();		
   },
 //   remove: function() {
	// 		this.$el.empty().off(); 
	// 		this.stopListening();
	// }
});