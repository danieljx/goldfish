var fishunit = fishunit || {};

fishunit.UnitView = Backbone.View.extend({
   template: _.template($('#tpl-unit').html()),
   pageViews: [],
   currentEditor: false,
   currentPageView: false,
   componentViews: [],
   themesView: false,
   log: true ? console.log.bind(window.console) : function() { },
   // current: 0,
   initialize: function() {
      this.log("%cinit view","color: red; font-size: 40px;");
      this.listenTo(this.model, "change:currentPage", function() { this.log("%cPage change!", "color: green"); this.goToPage(this.model.get("currentPage").cid); }.bind(this) );
      this.listenTo(this.model, "change:showSecondaryMenu", function() { this.log("!!!"); this.secondaryMenuViewToggle(); }.bind(this) );
      this.log(this.model);
      this.render();
   },
   events: {
      'click #show-page:not(.active)': function(e) {
         e.preventDefault();
         this.showPage();
      },
   	'click .page-list a:not(.toggle)': function(e) {
         e.preventDefault();
         var clickIndex = this.$pageList.find('a').index(e.target.closest('a')); // closest por si el click es en un hijo span
         this.showPage();
         this.goToPage(clickIndex);
         // if (fishunit.appView.showSecondaryMenu) {
         //    fishunit.appView.secondaryMenuView.activateDraggables();
         // }
   	},
      'click #preview-toggle': function(e) {
         e.preventDefault();
         $(e.target).closest('aside').find('button').removeClass('active');
         this.showPreview();
         // $(e.target).closest('button').addClass('active');
         // this.$el.find('.sections').removeClass("show-themes");

      }

   },
   render: function() {
      // this.log("%crender unitView "+this.cid,'font-size: 20px');
      var videoBg = false;

      if (!this.model.get("page")) {
            // this.attributes.page = new fishunit.Page();
      } else {
            // this.log("page:");
            // this.log(this.model.get("page"));
      }
      // fishunit.log(this.model.toJSON());
      this.$el.html( this.template(this.model.toJSON()) );

      if ( getComputedStyle(document.body).getPropertyValue('--background-video') ) {
            videoBg = getComputedStyle(document.body).getPropertyValue('--background-video').trim().split('\\').join('').split('url(').join('').split(')').join('');
            $('body').prepend($('<div class="video-module"><div class="video-container"><div class="filter"></div><video autoplay loop class="fillWidth"><source src="' + videoBg + '" type="video/mp4" /></video><div class="poster hidden"><img  alt=""></div></div></div>'));
      }

      
      switch (this.model.get("data").config.outputNav.value) {
            case 'accordeon':
                  this.structureView = new fishunit.StructureViewAccordeon({ model: this.model, el: this.$('#main > nav')});
                  break;
            case 'd3':
                  this.structureView = new fishunit.StructureViewD3({ model: this.model, el: this.$('#main > nav')});
                  break;
            default:
                  this.structureView = false;
      }
      
      this.structureView.render().update();
      
      $('.node').popup({position: 'right center'});

      this.model.on('shrink', this.shrink, this);
      this.model.on('expand', this.expand, this);

      


      this.$('.sections > section:not(#page)').transition( 'hide' );
      this.$('.sections > section#page').transition( 'show' );

      if (this.model.get("page")) {
            console.log("Setting current page");
            this.model.setCurrentPage(this.model.get("page").cid);
      } else {
            console.log("No page!");
            console.log(this.model.attributes);
      }


      return this;
   },
   showPage: function() {
      console.log("%cshowPage","color: #ffaa00; background-color: #000; font-size: 20px; padding: 5px;");
      // this.$el.find('.unit-info-title aside button').removeClass('active');
      // this.switchToView('#page');
   },
   toggleVisibility: function(visible) {
      if (visible) {
         this.$el.css('display','block');
      } else {
         this.$el.css('display','none');
      }
   },
   getCurrentPageView: function() {
         return this.currentPageView;
   },
   attachPageView: function(aCid) {
         this.log("%cAttaching page "+aCid, "color: green");
      // this.model.setCurrentPage(aCid);
      // this.$pageList.find('a').eq(index).addClass("current");
      this.$('#content').append('<div class="page"></div>');
      // this.log("%cGetting page model:","color: green");
      var aModel = this.model.getCurrentPage();
      // this.log("%cCreating view for model:","color: green");
      this.currentPageView = new fishunit.PageView({el: this.$('#content > .page'), model: aModel });
      // this.log("%cRendering page view","color: green");
      this.currentPageView.render();
   },
   detachPageView: function(aCallback) {
      this.log("%cChecking detach","color: green");
      if (this.currentPageView) {
            this.log("%cHas page","color: green");
            this.currentPageView.detach(function() { this.currentPageView = false; aCallback(); } );
      } else {
            this.log("%cNo page","color: green");
            this.currentPageView = false;
            aCallback();
      }
      // this.log("%cSetting currentPageView = false","color: green");
      
   },
   goToPage: function(aCid) {
         console.log("%cGoToPage "+aCid, "font-size: 30px; color: red;");
      // console.log(aCid);
      
      if (aCid) {
         fishunit.appView.setBusy();
         this.detachPageView(function () { this.attachPageView(aCid); }.bind(this));
         
      //    this.log("%cShow page","color: red");
         this.showPage();
         
         fishunit.appView.setReady();   
      }
      
   },
   addOne: function() {
      var itemModel = new fishunit.Page({
         title: "Sin nombre"
      });
      this.model.getPages().add(itemModel);
      this.renderListItem(itemModel, this.model.getPages().length - 1);
   },
      /** Toggle secondary menu
  * @param {boolean=} state - On/Off
   */
  secondaryMenuViewToggle: function() {
       if (this.model.get("showSecondaryMenu")) {
            console.log("open");
            this.shrink();
            this.secondaryMenuView.toggle(true);
         this.secondaryMenuView.loadComponents(this.getComponentViews());
         
         
       } else {
            console.log("close");
         this.expand();
         this.secondaryMenuView.toggle(false);
       }
    },
   shrink: function() {
      this.$el.addClass("shrunk");
   },
   expand: function() {
      this.$el.removeClass("shrunk");
   }
});