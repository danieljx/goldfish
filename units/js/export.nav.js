var videoBg = false;
if ( getComputedStyle(document.body).getPropertyValue('--background-video') !== '' ) {
    videoBg = getComputedStyle(document.body).getPropertyValue('--background-video').trim().split('\\').join('').split('url(').join('').split(')').join('');

    $('body').prepend($('<div class="video-module"><div class="video-container"><div class="filter"></div><video autoplay loop class="fillWidth"><source src="' + videoBg + '" type="video/mp4" /></video><div class="poster hidden"><img  alt=""></div></div></div>'));
}
  
  var pageDir = 'content';
  var pagesN = i;
  var nav = navJSON;
  console.log("Nav:");
  console.log(nav);
  console.log(pagesN);
  var pages = new Array();

  for (var i = 0; i < pagesN; i++) {
      console.log("creating page obj "+i);
      var pageN = findPage(nav,i);
      pages[i] = new contentPage(pageN.id);
      pages[i].title = pageN.title;
  }

  navTree = new treeNav('#main > nav');
  navTree.loadData( nav );
  navTree.render();

  function contentPage(id) {
    this.loaded = false;
    this.id = id;
    this.html = "";
    this.css = "";
}

  function findPage(navObj, i) {
      console.log("find "+i+" in:");
      console.log(navObj);
      if (nav.i == i) {
          return navObj;
      } else {
          if (navObj.children.length > 0) {
              var childFind = false;
              for (j = 0; j < navObj.children.length; j++) {
                  if (!childFind) {
                      childFind = findPage(navObj.children[j], i);
                  }
              }
              return childFind;
          } else {
              return false;
          }
      }
  }

  function getPage(i) {
      
    //   $.get({
    //       url: pageDir+'/page_'+i+'.css', 
    //       async: false,
    //       success: function(data) {
    //         pages[i].css = data;
    //         $('head').append('<style>'+pages[i].css+'</style>');
    //       }
    //   });
      $.get({
          url: pageDir+'/page_'+i+'.htm', 
          async: false,
          success: function(data) {
            pages[i].html = data;  			
            pages[i].loaded = true;	
          }
      });
  }

  function showPage(i) {

      if (!$.inArray(i, pages) || !pages[i].loaded) {
          getPage(i);
      }

      $('#content').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(e) {
        $('#content').html(pages[i].html);

        $('#content iframe.catflexheight').on("load", function(e) {
            console.log("THIS");
            console.log($(this));
          $(this)[0].contentWindow.postMessage("I am your father,"+$(this).attr('id'),'*'); 
          });

        $('#content').removeClass('out');
    });

    $('#content').addClass('out');
  }

  window.addEventListener("message", receiveIframeMsg, false);

  function receiveIframeMsg(event)
  {  
      console.log(event);
    var data = JSON.parse(event.data);
    if (data.msg) {
      switch (data.msg) {
          case 'growFrameH':
              console.log("growFrameH - recive",data.height);
              $('#'+data.id).height(data.height); 
              break;
          case 'showImgFull':
              var img = $('<img />');
              img.attr('src', data.src);
              $('#cover').append(img);
              img.on("click", function(e) {
                  $('#cover').removeClass('on');    
                  $('#cover').find('img').remove();
              });
              $('#cover').addClass('on');
              break;
          case 'waitForHeightHtml5C':
              waitForHeightHtml5C(event);
              break;
      }
    }
  }
  var iframeX;
  var docX;
  var segInterval = 0;
  var minHeight 	= 0;
  var growFrameHtml5C = function(newH) {
      $(iframeX).height(newH);
  };
  var waitForHeightHtml5C = function(e) {
      var bodyX = e.target.document.body;
      var h = (segInterval == 1?bodyX.scrollHeight:$(bodyX).outerHeight(true));
          if(segInterval == 1 || h < minHeight) {
              minHeight = h;
              $(bodyX).css({"max-height":minHeight});
          }
          growFrameHtml5C(h);
        };

  showPage(nav.i);

