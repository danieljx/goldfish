<?php
error_reporting(E_ALL);
require('../backend/general.php');
require('../vendor/leafo/scssphp/scss.inc.php');
require('../slim-api/src/Dependencies/helpers.php');
header('Access-Control-Allow-Origin: *');
$userId = $_GET['user'];
$projectId = $_GET['project'];
$unitId = $_GET['unit'];
$unitJSON = file_get_contents(getServer()."api/user/".$userId."/project/".$projectId."/unit/".$unitId);
$unit = json_decode($unitJSON, true);
$theme = json_decode(file_get_contents(getServer()."api/usertheme/".$unit["user_theme"]), true);
$config = json_decode($config, true);
$tempDir = '/preview/'.$userId.'-'.$projectId.'-'.$unitId;

$tempDirFull = $config['baseDir'].'/'.$config['dataFolder'].$tempDir;

$i = 0;
$eventSource = '/backend/events.php?unit='.$unitId.'&saved='.$unit['saved'];

if (!file_exists($tempDirFull) && !mkdir($tempDirFull)) {
	echo("Couldn't create directory ".$tempDirFull);
	die();
}

/*
* processPage
* Escribe contenidos HTML y CSS procesados a archivos, y procesa páginas posteriores (subpáginas)
*/
function processPage($page, &$contents, &$i) {
	$body = '<h2>'.$page['title'].'</h2>';
	$blocks = ['html'=>'', 'css'=>''];
	$navObj = array(
		'title' => $page['title'],
		'id' => $page['id'],
		'i' => $i,
		'visited' => false,
		'position' => $page['position'],
		'children' => []
	);

	if (count($page['block']['blocks']) > 0) {
		$blocks = Helpers::prepareBlock($page['block'],[$i+1,1]);
	}

	$contents[$i] = array(
		'css' => $blocks['css'],
		'html' => $body.$blocks['html']
	);
	// file_put_contents($tempDirFull.'/page_'.$i.'.css', $blocks['css']);
	// file_put_contents($tempDirFull.'/page_'.$i.'.htm', $body.$blocks['html']);



								//die(var_dump($tempDirFull.'/page_'.$i.'.htm'));
	$i++;
	if (array_key_exists('pages', $page)) {
		foreach ($page['pages'] as $subpage) {
			$navObj['children'][] = processPage($subpage, $contents, $i);
		}
	}

	// file_put_contents($tempDirFull.'/page_'.$i.'.css', $blocks['css']);

	return $navObj;
}

$contents = [];

$navJson = json_encode( processPage($unit['pages'], $contents, $i) );


$scss = new scssc();
$scss->addImportPath('../components/_common');
$scss->setFormatter('scss_formatter_compressed');



$css = $scss->compile('$assetPath: \'/components/themes/'.$theme['name'].'/assets/\'; '.$theme['sass'].' $fontPath: \'/dist/css/fonts/\';'.' @import "basics.scss";');
if ($theme['unitsass']) {
	$css .= $scss->compile('$assetPath: \'/components/themes/'.$theme['name'].'/assets/\'; '.$theme['sass'].$theme['unitsass']);
} ?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Goldfish :: <?php echo($unit["title"]); ?></title>
	<!-- <link href="https://fonts.googleapis.com/css?family=Oswald:300,400,700" rel="stylesheet"> -->
	<style id="theme-styles"><?php echo($css) ?>
  	body {  }

  	#main > nav {
  		padding-top: 12vh;
  		position: relative;
  	}

	#main .red > iframe {
		width: 100%;
		border: 0 none;
	}

	#main > #content > .page {
		display: none;
	}
	</style>
	<link rel="stylesheet" href="../../../../../dist/lib/semantic/dist/semantic.min.css">
	<link rel="stylesheet" id="css-theme" href="../../../../../units/css/navtree.css" />
	<script type="text/javascript" src="../../../../../dist/lib/jquery.min.js"></script>
	<script type="text/javascript" src="../../../../../dist/lib/underscore.min.js"></script>
	<script type="text/javascript" src="../../../../../dist/lib/i18next.min.js"></script>
	<script type="text/javascript" src="../../../../../dist/lib/semantic/dist/semantic.min.js"></script>
	<script type="text/javascript" src="../../../../../dist/lib/backbone.js"></script>
	
  	<script type="text/javascript" src="../../../../../node_modules/d3/build/d3.min.js"></script>
  	<!-- <script type="text/javascript" src="../../../../../units/js/navtree.js"></script> -->
</head>
<body class="grag">

<?php include('../dist/templates.html'); ?>
<script type="text/javascript" src="../../../../../dist/js/fishunit.min.js"></script>
<script type="text/javascript">

var unit = <?php echo($unitJSON); ?>;
var contents = <?php echo(json_encode($contents)); ?>;

console.log("%cRaw SLIM:","color: pink");
console.log(unit);
console.log("%cProcessed:","color: pink");
console.log(contents);

var i18n = {
      "sequence": "Mostrar en secuencia",
      "blockNavDuring": "Bloquear navegación por la duración del cuestionario",
      "blockNav": "Bloquear navegación hasta aprobar",
      "timeLimit": "Límite de tiempo (0 = sin límite)"
	};
	
fishunit.start({unit: unit, i18n: i18n });

</script>
</body>
</html>
