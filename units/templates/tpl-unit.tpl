<script type="text/template" id="tpl-unit">
  <h1><%= title %></h1>
  <section id="main">
    <nav></nav>
    <section id="content"></section>
  </section>
</script>