(function (factory) {
  /* global define */
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['jquery'], factory);
  } else {
    // Browser globals: jQuery
    factory(window.jQuery);
  }
}(function ($) {
  // template
  var tmpl = $.summernote.renderer.getTemplate();

  /**
   * @class plugin.colorPicker
   *
   * colorPicker Plugin
   * 
   * ### load script 
   * 
   * ```
   * < script src="plugin/summernote-ext-colorPicker.js"></script >
   * ``` 
   * 
   * ### use a plugin in toolbar
   * ```
   *    $("#editor").summernote({
   *    ...
   *    toolbar : [
   *        ['group', [ 'fontcolor', 'strikethrough', 'superscript', 'subscript' ]]
   *    ]
   *    ...    
   *    }); 
   * ```
   * 
   * ### provided features
   * 
   * * strikethrough 
   * * superscript
   * * subscript 
   * * fontcolor  ('8', '9', '10', '11', '12', '14', '18', '24', '36')
   */
  $.summernote.addPlugin({
    /** @property {String} name name of plugin */
    name: 'colorPicker', // name of plugin
    /**
     * @property {Object} buttons 
     * @property {Function} buttons.strikethrough  
     * @property {Function} buttons.superscript   
     * @property {Function} buttons.subscript   
     * @property {Function} buttons.fontcolor   dropdown button
     */
    buttons: { // buttons
      colorPicker: function (lang) {
        return tmpl.iconButton('fa fa-strikethrough', {
          event: 'strikethrough',
          title: lang.colorPicker.strikethrough
        });
      },
      superscript: function (lang) {
        return tmpl.iconButton('fa fa-superscript', {
          event: 'superscript',
          title: lang.colorPicker.superscript
        });
      },
      subscript: function (lang) {
        return tmpl.iconButton('fa fa-subscript', {
          event: 'subscript',
          title: lang.colorPicker.subscript
        });
      },
      colorPicker: function (lang, options) {
        var colorButtonLabel = '<i class="' + options.iconPrefix + 'font" style="color:black;background-color:yellow;"></i>';
        var colorButton = tmpl.button(colorButtonLabel, {
          className: 'colorPicker',
          title: lang.color.recent,
          event: 'colorPicker',
          value: ''
        });

        var dropdown = '<span class="dropdown-menu keep-open">Esto es una prueba de funcionamiento</span>';

        var moreButton = tmpl.button('', {
          title: lang.color.more,
          dropdown: dropdown
        });

        return '<div class="dropdown keep-open"><!-- Dropdown Button --><button id="dLabel" role="button" href="#" data-toggle="dropdown" data-target="#" class="btn btn-primary">Dropdown <span class="caret"></span></button><!-- Dropdown Menu --><ul class="dropdown-menu" role="menu" aria-labelledby="dLabel"><li><a href="#">Action</a></li><li><a href="#">Another action</a></li><li><a href="#">Something else here</a></li><li class="divider"></li><li><a href="#">Separated link</a></li></ul></div>';
      }
    },

    /**
     * @property {Object} events
     * @property {Function} events.strikethrough  apply strikethrough  style to selected range
     * @property {Function} events.superscript apply superscript to selected range
     * @property {Function} events.subscript apply subscript to selected range
     * @property {Function} events.fontcolor apply font color to selected range
     */
    events: { // events
      strikethrough: function (event, editor, layoutInfo) {
        editor.strikethrough(layoutInfo.editable());
      },
      superscript: function (event, editor, layoutInfo) {
        editor.superscript(layoutInfo.editable());
      },
      subscript: function (event, editor, layoutInfo) {
        editor.subscript(layoutInfo.editable());
      },
      fontcolor: function (event, editor, layoutInfo, value) {
        editor.fontcolor(layoutInfo.editable(), value);
      }
    },

    options: {
      fontcolors: ['8', '9', '10', '11', '12', '14', '18', '24', '36']
    },

    langs: {
      'en-US': {
        colorPicker: {
          strikethrough: 'Strikethrough',
          subscript: 'Subscript',
          superscript: 'Superscript',
          color: 'Font color'
        }
      },
      'ar-AR': {
        colorPicker: {
          strikethrough: 'فى وسطه خط',
          color: 'الحجم'
        }
      },
      'cs-CZ': {
        colorPicker: {
          strikethrough: 'Přeškrtnuté',
          color: 'Velikost písma'
        }
      },
      'ca-ES': {
        colorPicker: {
          strikethrough: 'Ratllat',
          color: 'Mida de lletra'
        }
      },
      'da-DK': {
        colorPicker: {
          strikethrough: 'Gennemstreget',
          subscript: 'Sænket skrift',
          superscript: 'Hævet skrift',
          color: 'Skriftstørrelse'
        }
      },
      'de-DE': {
        colorPicker: {
          strikethrough: 'Durchgestrichen',
          color: 'Schriftgröße'
        }
      },
      'es-ES': {
        colorPicker: {
          strikethrough: 'Tachado',
          superscript: 'Superíndice',
          subscript: 'Subíndice',
          color: 'Tamaño de la fuente'
        }
      },
      'es-EU': {
        colorPicker: {
          strikethrough: 'Marratua',
          color: 'Letren neurria'
        }
      },
      'fa-IR': {
        colorPicker: {
          strikethrough: 'Strike',
          color: 'اندازه ی فونت'
        }
      },
      'fi-FI': {
        colorPicker: {
          strikethrough: 'Yliviivaus',
          color: 'Kirjasinkoko'
        }
      },
      'fr-FR': {
        colorPicker: {
          strikethrough: 'Barré',
          superscript: 'Exposant',
          subscript: 'Indicé',
          color: 'Taille de police'
        }
      },
      'he-IL': {
        colorPicker: {
          strikethrough: 'קו חוצה',
          subscript: 'כתב תחתי',
          superscript: 'כתב עילי',
          color: 'גודל גופן'
        }
      },
      'hu-HU': {
        colorPicker: {
          strikethrough: 'Áthúzott',
          color: 'Betűméret'
        }
      },
      'id-ID': {
        colorPicker: {
          strikethrough: 'Coret',
          color: 'Ukuran font'
        }
      },
      'it-IT': {
        colorPicker: {
          strikethrough: 'Testo barrato',
          color: 'Dimensione del carattere'
        }
      },
      'jp-JP': {
        colorPicker: {
          strikethrough: '取り消し線',
          color: '大きさ'
        }
      },
      'ko-KR': {
        colorPicker: {
          superscript: '위 첨자',
          subscript: '아래 첨자',
          strikethrough: '취소선',
          color: '글자 크기'
        }
      },
      'nb-NO': {
        colorPicker: {
          strikethrough: 'Gjennomstrek',
          color: 'Skriftstørrelse'
        }
      },
      'nl-NL': {
        colorPicker: {
          strikethrough: 'Doorhalen',
          color: 'Tekstgrootte'
        }
      },
      'pl-PL': {
        colorPicker: {
          strikethrough: 'Przekreślenie',
          color: 'Rozmiar'
        }
      },
      'pt-BR': {
        colorPicker: {
          strikethrough: 'Riscado',
          color: 'Tamanho da fonte'
        }
      },
      'ro-RO': {
        colorPicker: {
          strikethrough: 'Tăiat',
          color: 'Dimensiune font'
        }
      },
      'ru-RU': {
        colorPicker: {
          strikethrough: 'Зачёркнутый',
          subscript: 'Нижний индекс',
          superscript: 'Верхний индекс',
          color: 'Размер шрифта'
        }
      },
      'sk-SK': {
        colorPicker: {
          strikethrough: 'Preškrtnuté',
          color: 'Veľkosť písma'
        }
      },
      'sl-SI': {
        colorPicker: {
          strikethrough: 'Prečrtano',
          subscript: 'Podpisano',
          superscript: 'Nadpisano',
          color: 'Velikost pisave'
        }
      },
      'sr-RS': {
        colorPicker: {
          strikethrough: 'Прецртано',
          color: 'Величина фонта'
        }
      },
      'sr-RS-Latin': {
        colorPicker: {
          strikethrough: 'Precrtano',
          color: 'Veličina fonta'
        }
      },
      'sv-SE': {
        colorPicker: {
          strikethrough: 'Genomstruken',
          color: 'Teckenstorlek'
        }
      },
      'th-TH': {
        colorPicker: {
          strikethrough: 'ขีดฆ่า',
          subscript: 'ตัวห้อย',
          superscript: 'ตัวยก',
          color: 'ขนาดตัวอักษร'
        }
      },
      'tr-TR': {
        colorPicker: {
          strikethrough: 'Üstü çizili',
          subscript: 'Subscript',
          superscript: 'Superscript',
          color: 'Yazı tipi boyutu'
        }
      },
      'uk-UA': {
        colorPicker: {
          strikethrough: 'Закреслений',
          subscript: 'Нижній індекс',
          superscript: 'Верхній індекс',
          color: 'Розмір шрифту'
        }
      },
      'vi-VN': {
        colorPicker: {
          strikethrough: 'Gạch Ngang',
          color: 'Cỡ Chữ'
        }
      },
      'zh-CN': {
        colorPicker: {
          strikethrough: '删除线',
          color: '字号'
        }
      },
      'zh-TW': {
        colorPicker: {
          strikethrough: '刪除線',
          color: '字體大小'
        }
      }
    }
  });
}));
$('.dropdown-menu.keep-open').on({
    "shown.bs.dropdown": function() { this.closable = false; },
    "click":             function() { this.closable = false; },
    "hide.bs.dropdown":  function() { return this.closable; }
});
