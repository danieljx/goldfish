
jQuery.fn.summernoteConfig = function(additional,id) {
	var toolbar = [
	    ['style', ['style']], // no style button
	    ['style', ['bold', 'italic', 'underline', 'clear']],
	    ['fontsize', ['fontsize']],
	    ['fontname',['fontname']],
	    ['color', ['color']],
	    ['para', ['ul', 'ol', 'paragraph']],
	    ['height', ['height']],
	    ['insert', ['link', 'hr']],
	    ['font', ['superscript', 'subscript']],
	    ['code', ['codeview']],
	  ];
	if(additional){
		toolbar.push(['perzonalized',[additional]])
	}
    return {
	  toolbar: toolbar,
	  disableDragAndDrop: true,
	    // onpaste: function(e) {
	    //         var thisNote = $(this);
	    //         var updatePastedText = function(someNote){
	    //             var original = someNote.code();
	    //             var cleaned = $(original).text(); //this is where to call whatever clean function you want. I have mine in a different file, called CleanPastedHTML.
	    //             someNote.code('').html(cleaned); //this sets the displayed content editor to the cleaned pasted code.
	    //         };
	    //         setTimeout(function () {
	    //             //this kinda sucks, but if you don't do a setTimeout, 
	    //             //the function is called before the text is really pasted.
	    //             updatePastedText(thisNote);
	    //         }, 10);
	    //     },
	    onImageUpload: function(files, editor, $editable){
	        sendFile(files[0],editor,$editable,id);
	    }
	};
	
};