<?php
	if(isset($_REQUEST['action']) && isset($_REQUEST['file'])){
		$doc_root = $_SERVER['DOCUMENT_ROOT'];//realpath(dirname(__DIR__));
		$requestFile = explode("/",$_REQUEST['file']);
		foreach (array_keys($requestFile, '..') as $key) {
		    unset($requestFile[$key]);
		}
		$requestFile = "/".implode("/", $requestFile);
		$file = $doc_root.urldecode($requestFile);
		header("Cache-Control: public");
	    header("Content-Description: File Transfer");
	    header("Content-Disposition: attachment; filename=".basename($file)."");
	    header("Content-Transfer-Encoding: binary");
	    header("Content-Type: binary/octet-stream");
	    readfile($file);exit();
	}
	if(isset($_FILES) && isset($_REQUEST['uploaddir'])){
		$error = false;
		$files = array();
		$uploaddir = $_REQUEST['uploaddir'];

		foreach ($_FILES as $file) {
			$filename = preg_replace("/[^a-zA-Z0-9.]/", "", $file['name']);
			try{
				if(!is_dir($uploaddir)){
					throw new Exception($uploaddir." is not a directory");
				}else{
					if(!file_exists($file['tmp_name'])){
						throw new Exception($file['tmp_name']." file doesn't exists");
					}else{
						if(!move_uploaded_file($file['tmp_name'], $uploaddir.$filename)){
							throw new Exception("Could not move file");
						}else{
							$basedir = realpath(dirname(__FILE__));
							$basedir = explode('/', $basedir);
							array_pop($basedir);
							array_pop($basedir);
							array_pop($basedir);
							$base = preg_replace('/\\.[^.\\s]{3,4}$/', '', $filename);
							$basedir = implode('/', $basedir);
							$output = shell_exec($basedir.'/filters'.DIRECTORY_SEPARATOR.'ffmpeg -i "' .$uploaddir.$filename. '" -ab 64k "' .$uploaddir.$base. '.ogg" 2>&1'. " && " . $basedir.'/filters'.DIRECTORY_SEPARATOR.'ffmpeg -i "' .$uploaddir.$filename. '" -ab 64k "' .$uploaddir.$base. '.mp3" 2>&1'. PHP_EOL);
						}
					}
					
				}
				
				$files[] = $base = preg_replace('/\\.[^.\\s]{3,4}$/', '', $uploaddir.$filename);
			}catch(Exception $e){
				die('File did not upload : '. $e->getMessage());
			}
		}
		$data = ($error)?array('error' => 'There was an error uploading your files'):array('files' => $files );
	}else{
		$data = array('success' => 'Form was submited','FormData' => $_POST);
	}
	echo json_encode($data);
?>