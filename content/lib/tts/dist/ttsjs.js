
    
    function tts(ttsFile,dataFolder,defaultExtension){
        this.ttsFile = ttsFile || "../../../tts/index.php";
        this.dataFolder = dataFolder || "../../../../oa3data/tts/";
        this.data = {};
        this.rootPath="";
        this.sounds = {};
        this.ttsFileId = "";
        this.uniqueID="";
        this.pathUploadFile = "../../../lib/tts/dist/upload.php";
        this.allowedExtensions = ['mp3','aac','wma','rec','cda','wav','ogg'];
        this.defaultExtension = defaultExtension || '.mp3';
        this.loading = '<svg width="100%" height="100%" xmlns="http://www.w3.org/2000/svg" '+
                       'viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="uil-ripple">'+
                       '<rect x="0" y="0" width="100" height="100" fill="none" class="bk"></rect><g>'+
                       '<animate attributeName="opacity" dur="2s" repeatCount="indefinite" begin="0s"'+
                       'keyTimes="0;0.33;1" values="1;1;0"></animate><circle cx="50" cy="50" r="40"'+
                       'stroke="#afafb7" fill="none" stroke-width="6" stroke-linecap="round">'+
                       '<animate attributeName="r" dur="2s" repeatCount="indefinite" begin="0s"'+ 
                       'keyTimes="0;0.33;1" values="0;22;44"></animate></circle></g><g>'+
                       '<animate attributeName="opacity" dur="2s" repeatCount="indefinite" '+
                       'begin="1s" keyTimes="0;0.33;1" values="1;1;0"></animate><circle cx="50"'+
                       'cy="50" r="40" stroke="#708c85" fill="none" stroke-width="6" stroke-linecap="round">'+
                       '<animate attributeName="r" dur="2s" repeatCount="indefinite" begin="1s" keyTimes="0;0.33;1"'+
                       'values="0;22;44"></animate></circle></g></svg>';
        this.markup = '<div class="popover-markup"><div class="head hide">Text to speech</div><div class="content hide container">'+
                          '<div class="row">'+
                            '<div class="container-body col-md-12 col-sm-12">'+
                              '<div class="tabs-left">'+
                                '<ul class="nav nav-tabs">'+
                                  '<li class="active"><a href="#a" data-toggle="tab"><span class="glyphicon glyphicon-equalizer"></span></a></li>'+
                                  '<li><a href="#b" data-toggle="tab"><span class="glyphicon glyphicon-star"></span></a></li>'+
                                '</ul>'+
                                '<div class="tab-content">'+
                                  '<div class="tab-pane active" id="a">'+
                                    '<div class="form-group">'+
                                        '<label for="speed">Speed</label>'+
                                        ' <div class="input-group">'+
                                              '<input type="range" value="170" min="80" max="450" step="1" class="form-control" id="speed" placeholder="Amount">'+
                                              '<div class="input-group-addon">170</div>'+
                                        '</div>'+
                                        '<label for="tone">Tone</label>'+
                                         '<div class="input-group">'+
                                              '<input type="range" value="10" min="10" max="20" step="1" class="form-control" id="tone" placeholder="Amount">'+
                                              '<div class="input-group-addon">10</div>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="form-group">'+
                                        '<label for="genre">Genre</label>'+
                                       '<select class="form-control" id="genre">'+
                                            '<option value="0">Male</option>'+
                                            '<option value="1">Female</option>'+
                                       '</select>'+
                                    '</div>'+
                                    '<div class="form-group">'+
                                        '<label for="lang">Language</label>'+
                                       '<select class="form-control" id="lang">'+
                                            '<option value="es">Spanish</option>'+
                                            '<option value="en">English</option>'+
                                       '</select>'+
                                   '</div>'+
                                   '<div class="progressbar"><article>Please wait</article><article>'+this.loading+'</article>'+                             
                                    '</div>'+
                                    '<button type="submit" class="generate-voice btn btn-default btn-block">'+
                                        'Generate voice'+
                                   ' </button>'+
                                  '</div>'+
                                  '<div class="tab-pane" id="b">'+
                                      '<div tabindex="500" class="btn btn-primary btn-file"><i class="glyphicon glyphicon-folder-open"></i>&nbsp; <span class="hidden-xs">Browse …</span></div><input name="audio-file-upload" id="audio-file-upload" type="file">'+
                                      '<div class="progress"><div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 0%"><span class="sr-only">20% Complete</span></div></div>'+
                                  '</div>'+
                                '</div><!-- /tab-content -->'+
                                '<div class="popover-footer"><div class=audiotts><audio class=audiotts controls>'+
                                  '<source id="ogg" src="#" type="audio/ogg">'+
                                  '<source id="mp3" src="#" type="audio/mp3">'+
                                  'Your browser does not support the audio element.'+
                                  '</audio></div><i class="download-voice glyphicon glyphicon-cloud-download"></i></div>'+
                              '</div><!-- /tabbable -->'+
                            '</div><!-- /col -->'+
                          '</div><!-- /row -->'+
                        '</div><!-- /container --><div>';
    }
    tts.prototype.init = function(){
        $('body').append(this.markup);
        this.getPath('ttsjs');
        this.prepareEvents();
    };
    tts.prototype.prepareEvents = function() {
        var that = this;
        $(document).on('change ready','input[type=range]',function(e){
            var element = $(e.target);
            element.parent().find('.input-group-addon').text(element.val());
        });
        $(document).on('click','.tts',function(e){
            e.preventDefault();
            try{
                that.ttsButton = $(this);
                that.uniqueID = $(this).data('uniqueid');
                $('body').find('.content .audiotts,.content .download-voice').css({display:'none'});
                if($(this).data('file')){
                    $('body').find('.content .audiotts audio source#ogg').attr('src',$(this).data('file')+".ogg");
                    $('body').find('.content .audiotts audio source#mp3').attr('src',$(this).data('file')+".mp3");
                    $('body').find('.content .download-voice').attr('data-file',$(this).data('file')+".mp3");
                    $('body').find('.content .audiotts audio').load();
                    $('body').find('.content .audiotts,.content .download-voice').css({display:'inline-block'});
                    that.addSound($(this).data('file'));
                }
                if($(document).find('.popover.in').length>0){
                    $('.popover.in').popover('hide');
                }
                $(this).popover({
                    html: true,
                    title: function () {
                        return $('body').find('.head').html();
                    },
                    content: function () {
                        return $('body').find('.content').html();
                    }
                });
                
                $(this).popover('show');

                
            }catch(error){
                console.log('error '+error);
            }
        });
        $(document).on('click','.download-voice',function(e){
          e.preventDefault();
          that.downloadFile($(this).data('file'));
        });
        $(document).on('click','.btn-file',function(e){
            e.preventDefault();
            var button = $(this);
            $(this).parent().find('#audio-file-upload').click().change(function(e){
              e.preventDefault();
              var filename = $(this).val().replace(/^.*[\\\/]/, ''),
                  extension = filename.replace(/^.*\./, ''),
                  file = $(this)[0].files[0];
              if (that.validateExtension(extension)) {
                button.find('span.hidden-xs').text(filename);
                that.upload(file,$(this));
              }else{
                $(this).val("");
                console.log('Archivo no permitido '+ filename );
              }
            });
        });
        $(document).on('click','.generate-voice',function(e){
            e.preventDefault();
            var progressbar = $(this).parent().find('.progressbar');
            that.data.speed = $(this).parent().find('#speed').val();
            that.data.tone  = $(this).parent().find('#tone').val();
            that.data.genre = $(this).parent().find('#genre').val();
            that.data.lang  = $(this).parent().find('#lang').val();
            $(this).parents().each(function(i){
                var element = $(this).find('.voice-code');
                if(element.length){
                    that.data.text = that.clean(element.code());
                    console.log(element.code());
                    console.log(that.data.text);
                    return false;
                }
            });

            

            that.data.string = 'idi='+ that.data.lang +'&ora='+ that.data.genre +
                               '&vel='+ that.data.speed + '&ton=' + that.data.tone +
                               '&txt=' + that.data.text;

            console.log(that.data.text);
           
            that.generateVoice(progressbar);
            
        });
    };

    tts.prototype.upload = function(files,e){      
      var data = new FormData(),
          loadBar = e.parent().parent().find('.progress'),
          that = this;
      console.log(loadBar);
      data.append('file',files);
      data.append('uploaddir',this.dataFolder);
      loadBar.css({display:'block'});
      $.ajax({
        url:this.pathUploadFile,
        type:'POST',
        cache:false,
        data:data,
        processData:false,
        contentType:false,
        success: function(data,textStatus,jqXHR){
          data = JSON.parse(data);
          if(typeof data.error === 'undefined'){
            $.each(data.files,function(key,value){
               that.addSound(String(value+that.defaultExtension));
               e.parents('.popover').find('.audiotts audio source#ogg').attr('src',String(value+'.ogg'));
               e.parents('.popover').find('.audiotts audio source#mp3').attr('src',String(value+'.mp3'));
               e.parents('.popover').find('.audiotts,.download-voice').css({display:'inline-block'}).load();
               that.ttsButton.attr('data-file',String(value+that.defaultExtension));
            });
          }else{
            console.log('Error: '+data.error);
          }
        },
        xhr: function(){
          var xhr = new window.XMLHttpRequest();
          //Upload progress
          xhr.upload.addEventListener("progress", function(evt){
            if (evt.lengthComputable) {
              var percentComplete = evt.loaded / evt.total;
              loadBar.find('.progress-bar').css({width:(percentComplete*100)+"%"});
              if (percentComplete === 1) {
                loadBar.hide(1000);
              }
              //Do something with upload progress
            }
          }, false);
          //Download progress
          xhr.addEventListener("progress", function(evt){
            if (evt.lengthComputable) {
              var percentComplete = evt.loaded / evt.total;
              //Do something with download progress
              console.log(percentComplete);
            }
          }, false);
          return xhr;
        },
        error: function(jqXHR,textStatus,errorThrown){
          console.log('Errors: '+ textStatus);
        }
      });
    };
    tts.prototype.validateExtension = function(ext){
      if($.inArray(ext,this.allowedExtensions) > -1){
        return true;
      }
      return false;
    };
    tts.prototype.generateVoice = function(progressbar){
        var that = this;
        $.ajax({
            url:this.ttsFile,
            type:'GET',
            data:this.data.string,
            beforeSend: function(){
                progressbar.show();
            }
        }).always(function(data){
            progressbar.hide();
            that.renewLinks(data);
            data = that.dataFolder+data;
            that.addSound(data);
            progressbar.parents('.popover').find('.audiotts audio source#ogg').attr('src',data+".ogg");
            progressbar.parents('.popover').find('.audiotts audio source#mp3').attr('src',data+".mp3");
            progressbar.parents('.popover').find('.download-voice').attr('data-file',data);
            progressbar.parents('.popover').find('.audiotts,.download-voice').css({display:'inline-block'});
            progressbar.parents('.popover').find('.audiotts audio').load();
            that.ttsButton.attr('data-file',data);
        }).fail(function(e){
        });
    };
    tts.prototype.downloadFile = function(path){
      that = this;
      $.ajax({
        url:this.pathUploadFile,
        type:'GET',
        data:{action:'download',file:path}
      }).always(function(data){
        window.location = that.pathUploadFile+"?action=download&file="+path;
      });
    };
    tts.prototype.renewLinks = function(fileName){
        $(document).find('#'+this.uniqueID+' audio').each(function(){
            console.log($(this));
        });
    };
    tts.prototype.clean = function(text){
      text = text.replace(/<(?:.|\n)*?>/gm, '');
      text = text.replace(/&nbsp;/g, " ").trim();
      
      //DUVAN 02033017
      //return escape(text.replace(/&nbsp;/g, " ").replace(/[^\w\s]/gi, '').trim());
      return text;
    };
    tts.prototype.getSounds = function(){
        return this.sounds;
    };
    tts.prototype.getPath = function(str){
      var that = this;
      $(document).find('script').each(function(){
        var src = String($(this).attr('src'));
        if(src.indexOf(str) > -1){
          src = src.split("/");
          src.pop();
          src = src.join("/");
          that.rootPath = src;
        }
      });
    };
    tts.prototype.addSound = function(name){
        this.sounds[this.uniqueID] = name;
    };