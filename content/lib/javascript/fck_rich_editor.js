//-------------- EDITOR

function FCKeditor_OnComplete( editorInstance )
{
	editorInstance.Events.AttachEvent( 'OnBlur'	, FCKeditor_OnBlur ) ;
	editorInstance.Events.AttachEvent( 'OnFocus', FCKeditor_OnFocus ) ;
}

function FCKeditor_OnBlur( editorInstance )
{
	editorInstance.ToolbarSet.Collapse() ;
}

function FCKeditor_OnFocus( editorInstance )
{
	editorInstance.ToolbarSet.Expand() ;
}

function getEditorContent(id){
        
	//Tiny MCE
	//return tinyMCE.getContent(id);	
	//FCK
	var oEditor = FCKeditorAPI.GetInstance(id) ;
        //console.log(oEditor.GetXHTML(true));
	//console.log('XXXXXXXXXXXXXXXXXXXXXX');
	//console.log(oEditor);
	return oEditor.GetXHTML(true);
}
var styles='<style>H4{color:#FF0000 !important;margin:4px 4px 2px 0px;text-decoration:underline;font-weight:normal !important;} H2{color: #DD2A2A !important; text-transform: uppercase;border-bottom:1px solid #E8F1F6;font-size:12px;margin-bottom:4px;} H3{color: #4D88C3 !important;text-transform:uppercase;font-size:12px;border-bottom:1px dashed #E8F1F6;display:block !important;} H5{color:#0173AA !important;margin:2px 4px 2px 4px;font-size:12px;text-decoration: underline;font-weight:normal !important;} H6{color:#0183BA !important;margin:2px 4px 2px 4px;font-size:12px;font-weight:normal !important;}</style>';
function html_editor_active(id){	
	//Tiny MCE
	//tinyMCE.execCommand('mceAddControl', true, id);
	//FCK
	document.getElementById(id).className="richEditor";
	document.getElementById(id).value=styles+document.getElementById(id).value;
	var oFCKeditor = new FCKeditor( id ) ;
	oFCKeditor.Config['ToolbarStartExpanded'] = false ;
	oFCKeditor.BasePath = sysroot+'/editor/' ;
	oFCKeditor.ToolbarSet	= 'Basic' ;
	oFCKeditor.Height=document.getElementById(id).rows*16;
	oFCKeditor.Config["EnterMode"]='br';
	oFCKeditor.ReplaceTextarea() ;	
}

var editors='';

function Tiny_MCE_richEditor(id){	
	tinyMCE.execCommand('mceAddControl', true, id);
}

function richEditor(id){	
    
	//alert("RE"+document.getElementById(id).rows);
	var str=document.getElementById(id).value;
	if(document.getElementById(id).rows<8)
		document.getElementById(id).rows=16;
	var oFCKeditor = new FCKeditor( id ) ;
	oFCKeditor.Config['ToolbarStartExpanded'] = false ;
	oFCKeditor.BasePath = sysroot+'/editor/' ;
	oFCKeditor.ToolbarSet	= 'Basic' ;
	str=str.replace(new RegExp('\\n','g'),'<br/>');
	document.getElementById(id).value=str;
	//alert(document.getElementById(id).value);	
	oFCKeditor.Height=document.getElementById(id).rows*16;//parseInt(document.getElementById(id).rows);
	oFCKeditor.Config["EnterMode"]='br';
	oFCKeditor.ReplaceTextarea();	
	document.getElementById("active_"+id).className='link_html_disable';
	//document.getElementById("active_"+id).innerHTML='Desactivar HTML';
	document.getElementById("active_"+id).href='javascript:rich_editor_disable(\''+id+'\'); void(0);';	
	document.getElementById(id).className='rich_text';
}
/*
function rich_editor_toggle(id) {
	var textArea = document.getElementById(id);
	if (textArea.style.display == 'none') {
		var oEditor = FCKeditorAPI.GetInstance(id);
		var editor = document.getElementById(id.concat('___Frame'));		
		//switch to original
		textArea.style.display = 'inline';
		editor.style.display = 'none';
		textArea.value = oEditor.GetData();
		document.getElementById("active_"+id).innerHTML='Activar HTML';
	} else {
		//switch to editor
		var oEditor = FCKeditorAPI.GetInstance(id);
		var editor = document.getElementById(id.concat('___Frame'));			
		textArea.style.display = 'none';
		editor.style.display = '';
		oEditor.SetHTML(textArea.value);
		document.getElementById("active_"+id).innerHTML='Desactivar HTML';
	}
}
*/
function rich_editor_toggle(id){
	richEditor(id);
}

function rich_editor_get_content(id){
	var oEditor = FCKeditorAPI.GetInstance(id) ;
	return oEditor.GetXHTML(true);
}

function rich_editor_disable(id){
	//document.getElementById(id).rows=3;	
	//alert(document.getElementById(id).rows);

	document.getElementById(id).value=rich_editor_get_content(id);
	data = document.getElementById(id).value;	
	data = data.replace(/(<br[^>]*>)/gi, "\n");
	data = data.replace(/(<[^>]*>)/gi, "");
	document.getElementById(id).value=data;
	
	document.getElementById(id).style.display='block';
	document.getElementById(id).className='plain_text';
	document.getElementById(id.concat('___Frame')).style.display = 'none';
	document.getElementById("active_"+id).className='link_html_enable';
	//document.getElementById("active_"+id).innerHTML='Activar HTML';
	document.getElementById("active_"+id).href='javascript:rich_editor_enable(\''+id+'\'); void(0);';		
}

function rich_editor_enable(id){
	//alert(document.getElementById(id).rows);
	if(document.getElementById(id).rows<8)
		document.getElementById(id).rows=16;
	
	document.getElementById(id).style.display='none';
	document.getElementById(id).className='rich_text';
	//alert(document.getElementById(id).className);
	var oEditor = FCKeditorAPI.GetInstance(id) ;
	//alert(document.getElementById(id).value);
	oEditor.SetHTML(document.getElementById(id).value);	
	document.getElementById(id.concat('___Frame')).style.display = 'block';
	document.getElementById("active_"+id).className='link_html_disable';
	//document.getElementById("active_"+id).innerHTML='Desactivar HTML';
	document.getElementById("active_"+id).href='javascript:rich_editor_disable(\''+id+'\'); void(0);';
}
