// JavaScript Document
function viewer_slide_view(param){
	parent.frames[frames["core"]].document.core.slide_view(param);
}

inLink = viewer_slide_view;

function showInternal(param){
        //Freddy Rojas 12.02.2014
        //Ver archivo viewer.as
        console.debug("Show internal: "+param);
	parent.frames[frames["core"]].document.core.slide_view(param);
}


//Mats Fjellner 05.05.2014
//Freddy Rojas 05.05.2014
//Función de prueba para controlar comportamiento de los enlaces
function manejadorInterno(e) {
    e.preventDefault();
    console.log("*****Target: "+e.target);
    var id;
    var enlace = $(this).attr('href');
    console.info("Href: "+$(this).attr('href'));
    // Freddy Rojas 05.05.2014
    
    console.info("EXPRESIÓN REGULAR: "+enlace.match(/\d{1,}/));
    
    /* Hace la búsqueda del número del slide para
       poder utilizar la función showInternal */
    id = enlace.match(/\d{1,}/);
    
    /* id es un arreglo que contiene los datos de la consulta hecha con match
       y el primer elemento es el número de la página enlazada */
    
    var numPag = parseInt(id[0])
    console.info("Tipo de dato: "+numPag);
    
    showInternal(numPag, '1');
}



function viewer_game_wordsearch_view(){
	parent.frames[frames["core"]].document.core.game_wordsearch_view();
}

function viewer_game_memory_view(){
	parent.frames[frames["core"]].document.core.game_memory_view();
}

function viewer_game_hangman_view(){
	parent.frames[frames["core"]].document.core.game_hangman_view();
}

function viewer_game_drag_view(){
	parent.frames[frames["core"]].document.core.game_drag_view();
}

function viewer_game_aplasta_view(){
	parent.frames[frames["core"]].document.core.game_aplasta_view();
}

function viewer_game_paint_view(){
	parent.frames[frames["core"]].document.core.game_paint_view();
}

function viewer_game_phrases_view(){
	parent.frames[frames["core"]].document.core.game_phrases_view();
}

function viewer_game_arrange_view(){
	parent.frames[frames["core"]].document.core.game_arrange_view();
}

function viewer_game_truefalse_view(){
	parent.frames[frames["core"]].document.core.game_truefalse_view();
}

function viewer_game_catch_view(){
	parent.frames[frames["core"]].document.core.game_catch_view();
}

function viewer_slide_next(param){
	parent.frames[frames["core"]].document.core.slide_next(param);
}

function viewer_slide_previous(param){
	parent.frames[frames["core"]].document.core.slide_previous(param);
}

function _viewer_next_slide(){
	var current;
	var total;
	current=parseInt(parent.frames[frames["workspace"]].document.getElementById('slide_control').value);
	total=parseInt(parent.frames[frames["workspace"]].document.getElementById('slide_total').value);
	if(current<total){
		parent.frames[frames["workspace"]].document.getElementById('slide_'+current).style.display='none';
		current=current+1;
		parent.frames[frames["workspace"]].document.getElementById('slide_'+current).style.display='block';
	}
	parent.frames[frames["workspace"]].document.getElementById('slide_control').value=current;
	parent.frames[frames["workspace"]].document.getElementById('display_current_slide').innerHTML=current;
}

function viewer_previous_slide(){
	var current;
	var total;
	current=parseInt(parent.frames[frames["workspace"]].document.getElementById('slide_control').value);
	total=parseInt(parent.frames[frames["workspace"]].document.getElementById('slide_total').value);
	if(current>1){
		parent.frames[frames["workspace"]].document.getElementById('slide_'+current).style.display='none';
		current-=1;
		parent.frames[frames["workspace"]].document.getElementById('slide_'+current).style.display='block';
	}
	parent.frames[frames["workspace"]].document.getElementById('slide_control').value=current;
	parent.frames[frames["workspace"]].document.getElementById('display_current_slide').innerHTML=current;
}


function viewer_quiz_attempt_review(id){
	parent.frames[frames["core"]].document.core.quiz_attempt_review(id);
}

function viewer_quiz_attempt_continue(id){
	parent.frames[frames["core"]].document.core.quiz_attempt_continue(id);
}

function viewer_quiz_attempt_save(id_form){
	//alert(forms_form_quiz_attempt_send(id_form));
	parent.frames[frames["core"]].document.core.quiz_attempt_save(forms_form_quiz_attempt_send(id_form));
}

function viewer_quiz_attempt_save_and_finish(id_form,msg){
	if(msg!=''){
		if(confirm(msg))
			parent.frames[frames["core"]].document.core.quiz_attempt_save_and_finish(forms_form_quiz_attempt_send(id_form));
	}else
		parent.frames[frames["core"]].document.core.quiz_attempt_save_and_finish(forms_form_quiz_attempt_send(id_form));
}

function viewer_quiz_attempt_add(){
	parent.frames[frames["core"]].document.core.quiz_attempt_add();
}

function viewer_internal_goto(tab,anchor){
	parent.frames[frames["core"]].document.core.internal_goto(tab,anchor);
}

function viewer_assignment_submission_disable_edit(id){
	parent.frames[frames["core"]].document.core.assignment_submission_disable_edit(id);
}

function viewer_assignment_submission_edit(id){
	parent.frames[frames["core"]].document.core.assignment_submission_edit(id);
}

function viewer_assignment_submission_save(id_form){
	var form_string=forms_form_send(id_form);
	parent.frames[frames["core"]].document.core.assignment_submission_save(form_string);
}

function viewer_assignment_submission_add(id){
	parent.frames[frames["core"]].document.core.assignment_submission_add(id);
}

function viewer_assignment_submission_remove(id){
//	var form_string=forms_form_send(id_form);
	util_set_opacity("submission_"+id+"_box",5,"");
	parent.frames[frames["core"]].document.core.assignment_submission_remove(id);
}

function viewer_assignment_submission_remove_response(id,status){
	switch(status){
		case 'SUCESS':
			util_drop_inner_html("submission_"+id+"_box","workspace");
		break;
		default:
			util_set_opacity("submission_"+id+"_box",10);
		break;
	}
}

function viewer_quiz_attempt_finish_and_save(id_form){
	alert(forms_form_standar_send(id_form));
	parent.frames[frames["core"]].document.core.quiz_attempt_finish_and_save(forms_form_standar_send(id_form));
}

function viewer_quiz_attempt_add(){
	parent.frames[frames["core"]].document.core.quiz_attempt_add();
}

function viewer_quiz_attempt_review(param){
	parent.frames[frames["core"]].document.core.quiz_attempt_review(param);
}

function viewer_quiz_attempt_show_new(param){
	document.getElementById('quiz_info').style.display='none';
	document.getElementById('quiz_display').innerHTML=param;
	if (!isIE)
		com_amath_render('quiz_display');
}

function viewer_quiz_attempt_show(param){
	document.getElementById('quiz_display').innerHTML=param;
}

function viewer_quiz_attempts_summary_show(param){
	document.getElementById('quiz_info').style.display='block';
	document.getElementById('quiz_info').innerHTML=param;
}

function viewer_log_view(){
	parent.frames[frames["core"]].document.core.show_log('');
}

function viewer_change_game(quantity){
	var gl = $('.games_list');
	var id = $(gl).not('.hidden').addClass('hidden').attr('id');
	var num = Number(id.split('_').pop())+quantity;
	if(num < 0){
		num = 0;
	}
	if(num > gl.length-1){
		num = gl.length-1;
	}
	if(num == 0){
		$('#lu_games_list_prev').attr('disabled','disabled');
		$('#lu_games_list_next').removeAttr('disabled');
	}else if(num == gl.length-1){
		$('#lu_games_list_prev').removeAttr('disabled');
		$('#lu_games_list_next').attr('disabled','disabled');
	}else{
		$('#lu_games_list_prev').removeAttr('disabled');
		$('#lu_games_list_next').removeAttr('disabled');
	}
	$('#li_games_list_' +num).removeClass('hidden');
}

function show_category_hide_others(i,t,e){
	for(x=0; x<t; x++){
		o1 = document.getElementById('bot' +x).style;
		o2 = document.getElementById('cat' +x).style;
		if(x != i){
			for(y=0; y<e.length; y+=2){
				o1.removeProperty(e[y]);
			}
			o2.display = 'none';
		}else{
			for(y=0; y<e.length; y+=2){
				o1.setProperty(e[y], '#'+e[y+1], null);
			}
			o2.display = 'block';
		}
	}
}

function lu_settings_init() {

    $('#fondoss .viewer_theme img').each( function(e) {
        
        if (!$(this).hasClass("loaded")) {
            $(this).attr('src', $(this).attr('data-ruta_thumb'));
            $(this).addClass("loaded");
        }
    });
}

//Santiago Peñuela A - 30/01/2015
function indicator_added()
{    
    $('.editor_skill_indicator_box').each(function()
    {
       if($(this).find('.editor_input_skill_indicator_desc').val() == "")
       {
           $(this).find('.editor_input_skill_indicator_desc').removeAttr('disabled');
       }
       if($(this).find('.editor_input_skill_indicator_code').val() == "")
       {
           $(this).find('.editor_input_skill_indicator_code').removeAttr('disabled');
       }
    });
}

function skill_added()
{
    $('.editor_skill_box').each(function()
    {
       if($(this).find('.editor_input_skill_desc').val() == "")
       {
           $(this).find('.editor_input_skill_desc').removeAttr('disabled');
       }
       if($(this).find('.editor_input_skill_code').val() == "")
       {
           $(this).find('.editor_input_skill_code').removeAttr('disabled');
       }
    });
}

// Mats Fjellner 2014.11.13
// Se llama cada vez que se añade una actividad nueva

function activity_added() {
    console.log("added activity..");
    lu_activities_edit();
}

// Mats Fjellner 2014.11.13
// Se llama cada vez que se entra en la sección "Actividades"

function lu_activities_edit() {
    
    var textareas = $("#task_activities_display > div textarea:not(.rich_text)");
    
    textareas.each( function() {
            rich_editor_toggle( $(this).attr("id") );
    } );
    
    var canAdd = canAddIndicators;
    
    $('input.activity_skills').each( function(e) {
        if (!($(this).prev("ul").hasClass("token-input-list") ) ) {
            var ids = $(this).attr('data-ids').split(',');
            var names = $(this).attr('data-names').split(',');

            var prePopObj = [];
            
            if (($(this).attr('data-ids') != "") && (ids.length === names.length) && ids.length > 0) {

                for (var i = 0; i < ids.length; i++) {
                    prePopObj.push({
                        id: ids[i],
                        name: unescape(names[i])
                    });
                }
            }

            $(this).tokenInput("../app/ajax/getIndicators.php?id="+id_unidad, {hintText: "", searchingText: "......", noResultsText: "", minChars: 2, permitCustom: canAdd, prePopulate: prePopObj});
            if (!canAdd) {
                $(this).next(".add-indicator").addClass("disabled");
            }
//                $('body').on('click', 'a.add-indicator', function(e) {
//                indicatorAdmin($(this));
//            }

            $('#lu_activities .link_save').click( function(e) {
    //            e.preventDefault();

                var form = $(this).closest('.options_bar').next('.display').find('form');
                var skills = form.find('.activity_skills').val().split(',');
                for (var i = 0; i < skills.length; i++) {
                    var input = $('<input type="hidden" name="activity_skill" value="'+ skills[i] +'" />');
                    if (form.find('input[name="activity_skill"][value="'+skills[i]+'"]').length < 1) {
                        form.append(input);
                    }
                }

            });
        }
    });
}

// Mats Fjellner 2014.11.14
// Se llama cada vez que se añade una pregunta nueva

function question_added() {
    
    lu_question_skill_edit();
}

function lu_question_skill_edit() {

    $('input.question_skill_text').each(function (e) {

        var canAdd = canAddIndicators;
        var ids;
        var names;

        var linkRef = $('[id^=options_question_] .link_save').attr('href');
        console.log("Enlace: " + linkRef);


        ids = $(this).attr('data-ids').split(',');
        names = $(this).attr('data-names').split(',');

        var prePopObj = [];

        if (($(this).attr('data-ids') != "") && (ids.length === names.length) && ids.length > 0) {
            for (var i = 0; i < ids.length; i++) {
                prePopObj.push({
                    id: ids[i],
                    name: unescape(names[i])
                });
            }
        }



        $(this).tokenInput("../app/ajax/getIndicators.php", {
            hintText: "",
            searchingText: "......",
            noResultsText: "",
            minChars: 2,
            permitCustom: canAdd,
            prePopulate: prePopObj,
            onAdd: function () {
                saveLinkEnable($('[id^=options_question_] .link_save'), linkRef);
            },
            
            onDelete: function () {
                saveLinkEnable($('[id^=options_question_] .link_save'), linkRef);
            }
            
        });
        
        if (!canAdd) {
                $(this).next(".add-indicator").addClass("disabled");
            }

        $('[id^=options_question_] .link_save').click(function (e) {

                // var skills = form.find('.activity_skills').val().split(',');
                // for (var i = 0; i < skills.length; i++) {
                //     var input = $('<input type="hidden" name="activity_skill" value="'+ skills[i] +'" />');
                //     form.append(input);
                // }

        var form = $(this).closest('.options_bar').next('.display').find('form');
        var skills = form.find('.question_skill_text').val().split(','); // ?? class

        console.log(skills);

        for (var i = 0; i < skills.length; i++) {
            var input = $('<input type="hidden" name="question_skill" value="' + skills[i] + '" />');
            // if (skills[i])
            if (form.find('input[name="question_skill"][value="'+skills[i]+'"]').length < 1) {
                form.append(input);
            }
        }
        // saveLinkDisable($(this), $(this).attr('href'));
    });

        /*$('[id^=options_question_] .link_save').click(function (e) {

            var form = $(this).closest('.options_bar').next('.display').find('form');
            var skills = form.find('.question_skill_text').val().split(',');

            for (var i = 0; i < skills.length; i++) {
                var input = $('<input id="question_skill_' + skills[i] + '"' + ' type="hidden" name="question_skill" value="' + skills[i] + '" />');
                console.log('<input id="question_skill_' + skills[i] + '"' + ' type="hidden" name="question_skill" value="' + skills[i] + '" />');
                if (skills[i])
                    form.append(input);
            }

            console.info("deshabilitando link...");
            saveLinkDisable(this, linkRef);
        });*/
    });

}

function saveLinkDisable(selectedObj, scriptFunction) {
    console.info("saveDisabled");
    eval(unescape(scriptFunction));
    $(selectedObj).off('click');
    $(selectedObj).removeAttr('href');
}

function saveLinkEnable(selectedObj, scriptFunction) {
    console.info("saveEnabled");    
    $(selectedObj).on('click');
    $(selectedObj).attr('href',scriptFunction);
    saveLinkProcess($(selectedObj));
}

function saveLinkProcess(selectedObj) {      
   
    
}