var nelement = 0;
function addActivity(parent) {
   var seciones='';
   container = $(parent);
   div = document.createElement('DIV');
   id='n'+(++nelement);
   div.id = 'boxactivity_'+id; 
   //div.className = 'edit_boxactivity';
   for(i=0;i<Sections.length;i++){
	   seciones+='<option value="'+i+'">'+Sections[i]+'</option>';
   }   
   
   div.innerHTML = '<div class="edit_boxassignment_frame"><div class="edit_boxassignment"><div><input type="hidden" value="'+id+'" name="form_uac_activity_id"/><div class="edit_inputinline activity_line">Seccion: <select name="form_uac_activity_section">'+seciones+'<option value="-1" selected="selected">--General--</option></select></div><div class="edit_inputinline activity_line">Nombre: <input type="text" value="" name="form_uac_activity_name" size="84"/><a class="link_more" onclick="javascript:showHidde(\'activity_'+id+'\',\'slide\'); return false;" href="#">&nbsp;&nbsp;</a><a class="link_delete" href="javascript:if(confirm(\'Esta seguro que desea borrar esta Actividad?\')){getElementById(\''+div.id+'\').parentNode.removeChild(getElementById(\''+div.id+'\'));} void(0);">&nbsp;&nbsp;</a></div><div style="" id="activity_'+id+'"><div class="edit_inputinline activity_line" style="">Competencias: <input type="checkbox" value="PRO" name="form_uac_activity_competition_PRO"/>Propositiva<input type="checkbox" value="INT" name="form_uac_activity_competition_INT" />Interpretativa<input type="checkbox" value="ARG" name="form_uac_activity_competition_ARG" />Argumentativa</div><div class="edit_boxtextarea comment_activity">Descripcion<textarea class="richEditor" style="width: 630px;" rows="10" name="form_uac_activity_body" id="form_uac_activity_body'+div.id+'"></textarea></div><div class="edit_boxtextarea comment_activity">Comentarios<textarea class="richEditor" style="width: 630px;" rows="8" name="form_uac_activity_comment" id="form_uac_activity_comment'+div.id+'"></textarea></div><div class="edit_inputinline activity_line">Tiempo estimado de elaboracion: <input type="text" value="" name="form_uac_activity_time" size="2" style="text-align: right;"/> minutos.   Tipo: <input type="checkbox" value="EVAL" name="form_uac_activity_type" checked="checked"/>Evaluable</div></div></div></div></div>';    
   container.appendChild(div);
   richEditor('form_uac_activity_body'+div.id);
   richEditor('form_uac_activity_comment'+div.id);   
}
addGlossary = function (parent) {
   container = $(parent);
   div =document.createElement('DIV');
   id='n'+(++nelement);
   div.id = 'boxglosary_'+id;  
   div.className = 'box';
   div.innerHTML = '<div class="edit_boxglossary"><div class="edit_inputinline glossary_line"><input type="text" size="16" name="form_uac_glossary_word" value="" /><strong>:</strong><input type="text" size="68" name="form_uac_glossary_define" value="" /><a href="javascript:void(0);" onClick="javascript:if(confirm(\'Esta seguro que desea borrar esta entrada en el glosario?\')){getElementById(\''+div.id+'\').parentNode.removeChild(getElementById(\''+div.id+'\'));}" class="link_delete" >&nbsp;&nbsp;</a></div></div>';     
   container.appendChild(div);
}

addResource = function (parent) {
   container = $(parent);
   div =document.createElement('DIV');
   id='n'+(++nelement);
   div.id = 'boxresource_'+id;  
   div.className = 'box';
   div.innerHTML = '<div class="edit_boxresource"><div class="box_title"><div class="box_title_control edit_box_title_control"><a href="#" onclick="javascript:showHidde(\'resource_comment'+div.id+'\',\'slide\');  return false;" class="link_comment">&nbsp;</a><a href="javascript:void(0);" onClick="javascript:if(confirm(\'Esta seguro que desea borrar este Recurso asociado?\')){getElementById(\''+div.id+'\').parentNode.removeChild(getElementById(\''+div.id+'\'));}" class="link_delete" >&nbsp;</a></div></div><div class="field_line"><span>Url:</span><input type="text" name="form_uac_resource_url" value="" /><div id="resource_comment'+div.id+'" class="field_line" style="display:none"><span>Descripci&oacute;n:</span><input type="text" name="form_uac_resource_comment"/></div></div></div>';     
   container.appendChild(div);
}


addSource = function (parent) {
   container = $(parent);
   div =document.createElement('DIV');
   id='n'+(++nelement);
   div.id = 'boxsource_'+id;  
   div.className = 'box';
   div.innerHTML = '<div class="edit_boxsource"><div class="box_title"><div class="box_title_control edit_box_title_control"><a href="#" onclick="javascript:showHidde(\'source_comment'+div.id+'\',\'slide\');  return false;" class="link_comment">&nbsp;</a><a href="javascript:void(0);" onClick="javascript:if(confirm(\'Esta seguro que desea borrar este Recurso asociado?\')){getElementById(\''+div.id+'\').parentNode.removeChild(getElementById(\''+div.id+'\'));}" class="link_delete" >&nbsp;</a></div></div><div class="field_line"><span>Url:</span><input type="text" name="form_uac_source_url" value="" /><div id="source_comment'+div.id+'" class="field_line" style="display:none"><span>Descripci&oacute;n:</span><input type="text" name="form_uac_source_comment"/></div></div></div>';     
   container.appendChild(div);
}

addGoal = function (parent) {
   container = $(parent);
   div =document.createElement('DIV');
   id='n'+(++nelement);
   div.id = 'boxgoal'+id;  
   div.className = 'box';
   div.innerHTML = '<div class="edit_boxgoal"><div class="edit_inputinline goal_line"><input id="input_'+div.id+'" type="text" size="84" name="form_uac_goal" value="" /><a href="javascript:void(0);" onClick="javascript:if($(\'input_'+div.id+'\').value!=\'\'){if(confirm(\'Esta seguro que desea borrar este Logro?\')){getElementById(\''+div.id+'\').parentNode.removeChild(document.getElementById(\''+div.id+'\'));}}else{document.getElementById(\''+div.id+'\').parentNode.removeChild(document.getElementById(\''+div.id+'\'));}" class="link_delete" >&nbsp;</a></div></div>';     
   container.appendChild(div);
}

function changeAnswerState(flag, id){
	if(flag==true){
		document.getElementById(id).className='edit_answer_correct';
	}else{
		document.getElementById(id).className='edit_answer_incorrect';	
	}	
}

function addAnswerOptions(idAnswer){
	var strXHML='';
	strXHML+='<div  class="edit_answer_options">';
	strXHML+='<a class="link_comment" href="javascript:showHidde(\'comment_'+idAnswer+'\',\'slide\'); void(0);">&nbsp;&nbsp;</a>';	
	strXHML+='<a href="javascript:if(confirm(\'Esta seguro que desea borrar esta respuesta?\')){getElementById(\''+idAnswer+'\').parentNode.removeChild(getElementById(\''+idAnswer+'\')); } void(0);" class="link_delete" >&nbsp;&nbsp;</a>';
	strXHML+='</div>';
	return strXHML;
}


function addAnswerComment(idAnswer){
	var strXHML='';
	strXHML+='<div class="edit_answer_boxcomment" id="comment_'+idAnswer+'" style="display:none">';
	strXHML+='Comentario: <input style="width:100%"  type="text" name="form_uac_answer_feedback" value="" />';
	strXHML+='</div>';
	return strXHML;
}

function addAnswerText(idAnswer){
	var strXHML='';
	strXHML+='<div class="edit_answer_boxcomment">';	
	strXHML+='<input style="width:100%" type="text" name="form_uac_answer_name" id="form_uac_answer_name_'+idAnswer+'"/>';
	strXHML+='</div>';
	return strXHML;
}

function addAnswerHTML(idAnswer){
	var strXHML='';
	strXHML+='<div class="edit_boxtextarea comment_activity">';
	strXHML+='<textarea class="richEditor" style="width: 630px;" rows="8" name="form_uac_answer_name" id="form_uac_answer_name_'+idAnswer+'"></textarea>';
	strXHML+='</div>';
	return strXHML;
}

function addAnswerOpen(idAnswer){
	var strXHML='';
	strXHML+='<div class="edit_boxanswer">';
	strXHML+='<div id="flag_'+idAnswer+'" class="edit_answer">';
	return strXHML;
}

function addAnswerClose(){
	var strXHML='';
	strXHML+='</div>';
	strXHML+='</div>';	
	return strXHML;
}

function addAnswerShortQuestion(parent,idQuestion) {
	question = $(parent);
	answer = document.createElement('DIV');
	id='n'+(++nelement);
	answer.id = 'form_uac_answerquestion_'+id; 
	answer.className = 'edit_boxanswer_frame';
	
	var strXHML='';
	strXHML+=addAnswerOpen(answer.id);
	strXHML+='<input type="hidden" name="form_uac_quiz_question_answer" value="'+idQuestion+'" />';	
	strXHML+='<input type="hidden" name="form_uac_answer_emperejar"/>';
	strXHML+='<input type="hidden" name="form_uac_answer_grade" value="1" />';
	strXHML+='<div class="edit_answer_bar">';
	strXHML+='<div class="edit_answer_label">Respuesta</div>';
	strXHML+=addAnswerOptions(answer.id);
	strXHML+='</div>';	

	strXHML+=addAnswerText(answer.id);
	strXHML+=addAnswerComment(answer.id);
	
	strXHML+=addAnswerClose();
	
	answer.innerHTML = strXHML;    
	question.appendChild(answer);
	$('flag_'+answer.id).className="edit_answer_correct";
}

function addAnswerQuestion(parent,idQuestion) {
	question = $(parent);
	answer = document.createElement('DIV');
	id='n'+(++nelement);
	answer.id = 'form_uac_answerquestion_'+id; 
	answer.className = 'edit_boxanswer_frame';
	
	var strXHML='';
	strXHML+=addAnswerOpen(answer.id);
	strXHML+='<input type="hidden" name="form_uac_quiz_question_answer" value="'+idQuestion+'" />';	
	strXHML+='<input type="hidden" name="form_uac_answer_emperejar"/>';
	strXHML+='<div class="edit_answer_bar">';
	strXHML+='<div class="edit_answer_label">';
	strXHML+='<input onChange="changeAnswerState(this.checked,\'flag_'+answer.id+'\');" type="checkbox" name="form_uac_answer_grade" value="1" />Es correcta';
	strXHML+='</div>';
	strXHML+=addAnswerOptions(answer.id);
	strXHML+='</div>';	

	strXHML+=addAnswerHTML(answer.id);
	strXHML+=addAnswerComment(answer.id);
	
	strXHML+=addAnswerClose();
	
	answer.innerHTML = strXHML;    
	question.appendChild(answer);
	richEditor('form_uac_answer_name_'+answer.id);
}

function addAnswerMatchQuestion(parent,idQuestion) {
	question = $(parent);
	answer = document.createElement('DIV');
	id='n'+(++nelement);
	answer.id = 'form_uac_answerquestion_'+id; 
	answer.className = 'edit_boxanswer_frame';
	
	var strXHML='';
	strXHML+=addAnswerOpen(answer.id);
	strXHML+='<input type="hidden" name="form_uac_quiz_question_answer" value="'+idQuestion+'" />';	
	strXHML+='<input type="hidden" name="form_uac_answer_grade" value="1" />';
	strXHML+='<div class="edit_answer_bar">';
	strXHML+='<div class="edit_answer_label">';
	strXHML+='Respuestas';
	strXHML+='</div>';
	strXHML+=addAnswerOptions(answer.id);
	strXHML+='</div>';	

	strXHML+=addAnswerHTML(answer.id);
	
	strXHML+='<div class="edit_answer_boxcomment">';	
	strXHML+='Emparejar con:<input style="width:100%" type="text" name="form_uac_answer_emperejar"/>';
	strXHML+='</div>';

	strXHML+=addAnswerComment(answer.id);
	
	strXHML+=addAnswerClose();
	
	answer.innerHTML = strXHML;    
	question.appendChild(answer);
	richEditor('form_uac_answer_name_'+answer.id);
	$('flag_'+answer.id).className="edit_answer_correct";
}

function showHidde(id, effect){
	if($(id).style.display=='none'){
		$(id).style.display='block';	
	}else
		$(id).style.display='none';	
	return false;
}


function addQuestionHeader(idQuestion,type){
	var strXHML='';
	strXHML+='<div class="edit_question_header"> ';   
	strXHML+='<div class="edit_question_summary"> ';   
	strXHML+='<strong>'+type+'</strong>'; 		 
	strXHML+='<span id="question_summary_form_uac_quiz_question_body'+idQuestion+'" style="display:none">&nbsp;</span>'; 
	strXHML+='<span id="form_uac_quiz_question_grade_box'+idQuestion+'">&nbsp;&nbsp;Puntos: <input type="text" size="2" name="form_uac_quiz_question_grade" id="form_uac_quiz_question_grade'+idQuestion+'" value="1" maxlength="3" />';
	strXHML+='</div> ';  
	
	strXHML+='<div class="edit_question_icons"> ';   
	strXHML+='<a href="javascript:showHidde(\'question_'+idQuestion+'\',\'slide\'); questionSummary(\''+idQuestion+'\'); void(0);" class="link_more">&nbsp;&nbsp;</a>';
	strXHML+='<a class="link_delete" href="javascript:if(confirm(\'Esta seguro que desea borrar esta pregunta?\')){document.getElementById(\''+idQuestion+'\').parentNode.removeChild(document.getElementById(\''+idQuestion+'\'));} void(0);">&nbsp;&nbsp;</a>';   
	strXHML+='</div> '; 	
	strXHML+='</div> '; 
	return strXHML;
}

function addQuestionBody(idQuestion){
	var strXHML='';
	strXHML+='<div class="edit_boxtextarea comment_activity">'; 
		strXHML+='Descripcion: <textarea class="richEditor" style="width: 630px;" mce_editable="true" rows="12" name="form_uac_quiz_question_body" id="form_uac_quiz_question_body'+idQuestion+'"></textarea>'; 
	strXHML+='</div>'; 
	return strXHML;
}

function addQuestionOpen(idQuestion){
	var strXHML='';
	strXHML+='<div class="edit_boxquestion">';
	strXHML+='<div class="edit_question">';
	strXHML+='<input type="hidden" value="'+idQuestion+'" name="form_uac_quiz_question_id"/>';	
	return strXHML;
}

function addQuestionClose(){
	var strXHML='';
	strXHML+='</div>';
	strXHML+='</div>';
	return strXHML;
}

function addQuestionAnswerSpace(idQuestion){
	var strXHML='';
	strXHML+='<div class="edit_boxanswerlist" id="answers_'+idQuestion+'">';
	strXHML+='</div>';
	return strXHML;
}

function questionSummary(id){
	if(document.getElementById('question_summary_form_uac_quiz_question_body'+id).style.display!='none'){
		document.getElementById('question_summary_form_uac_quiz_question_body'+id).style.display='none';
		document.getElementById('question_summary_form_uac_quiz_question_body'+id).innerHTM='&nbsp;';
		document.getElementById('form_uac_quiz_question_grade_box'+id).style.display='inline';
	}else{
		var output=getEditorContent('form_uac_quiz_question_body'+id);	
		output = output.replace(/(<style[^>]*>[^<]*<\/style[^>]*>)/g, "");
		output = output.replace(/(<[^>]*>)/g, "");	
		output='('+document.getElementById('form_uac_quiz_question_grade'+id).value+') '+output.substr(0,56)+'...';
		document.getElementById('question_summary_form_uac_quiz_question_body'+id).style.display='inline';
		document.getElementById('question_summary_form_uac_quiz_question_body'+id).innerHTML=output;
		document.getElementById('form_uac_quiz_question_grade_box'+id).style.display='none';		
	}
}

function addShortQuestion(parent) {
	container = $(parent);
	question = document.createElement('DIV');
	id=time()+(++nelement);
	question.id = '2'+''+id; 
	question.className = 'edit_boxquestion_frame';
	var strXHML='';
	strXHML+='<input type="hidden" name="form_uac_quiz_question_type" value="RC" />';
	strXHML+=addQuestionOpen(question.id);	
	//PREGUNTA
	strXHML+=addQuestionHeader(question.id,'RC'); 	
	//INFORMACION EXTENDIDA	
	strXHML+='<div id="question_'+question.id+'">';	
	strXHML+=addQuestionBody(question.id);
	//RESPUESTAS
	strXHML+=addQuestionAnswerSpace(question.id);
	strXHML+='<a class="edit_link_add" href="javascript:addAnswerShortQuestion(\'answers_'+question.id+'\',\''+question.id+'\')">A&ntilde;adir una respuesta</a>';	
	strXHML+='</div>'; 	
	strXHML+=addQuestionClose();	
	question.innerHTML = strXHML;    
	container.appendChild(question);
	richEditor('form_uac_quiz_question_body'+question.id);
}

function addMultipleQuestion(parent) {
	quiz = $(parent);
	question = document.createElement('DIV');
	id=time()+(++nelement);
	question.id = '2'+''+id; 
	question.className = 'box';
	var strXHML='';
	strXHML+=addQuestionOpen(question.id);
	strXHML+='<input type="hidden" name="form_uac_quiz_question_type" value="MC" />';
	//PREGUNTA
	strXHML+=addQuestionHeader(question.id,'OM'); 	
	//INFORMACION EXTENDIDA	
	strXHML+='<div id="question_'+question.id+'">';	
	strXHML+=addQuestionBody(question.id);
	//RESPUESTAS
	strXHML+=addQuestionAnswerSpace(question.id);
	strXHML+='<a class="edit_link_add" href="javascript:addAnswerQuestion(\'answers_'+question.id+'\',\''+question.id+'\')">A&ntilde;adir una respuesta</a>';	
	strXHML+='</div>'; 	
	strXHML+=addQuestionClose();	
	question.innerHTML = strXHML;    
	quiz.appendChild(question);
	richEditor('form_uac_quiz_question_body'+question.id);
}

function addTrueFalseQuestion(parent) {
	quiz = $(parent);
	question = document.createElement('DIV');
	id=time()+(++nelement);
	question.id = '2'+''+id; 
	question.className = 'edit_boxquestion_frame';
	var strXHML='';
	strXHML+=addQuestionOpen(question.id);
	strXHML+='<input type="hidden" name="form_uac_quiz_question_type" value="VF" />';
	//PREGUNTA
	strXHML+=addQuestionHeader(question.id,'VF'); 	
	//INFORMACION EXTENDIDA	
	strXHML+='<div id="question_'+question.id+'">';	
	strXHML+=addQuestionBody(question.id);
	//RESPUESTAS
	
	var idQuestion=question.id;
	strXHML+='<div class="edit_boxanswerlist" id="answers_'+idQuestion+'">';
	
	//VERDADERO	
	id='n'+(++nelement);
	answer_id = 'form_uac_answerquestion_'+id; 
	strXHML+='<div id="'+answer_id+'" class="edit_boxanswer_frame">';
	strXHML+=addAnswerOpen(answer_id);
	strXHML+='<input type="hidden" name="form_uac_quiz_question_answer" value="'+idQuestion+'" />';	
	strXHML+='<input type="hidden" name="form_uac_answer_emperejar"/>';
	strXHML+='<div class="edit_answer_bar">';
	strXHML+='<div class="edit_answer_label">';
	strXHML+='<input onChange="changeAnswerState(this.checked,\'flag_'+answer_id+'\');" type="checkbox" name="form_uac_answer_grade" value="1" />Es correcta &nbsp;&nbsp;<strong>Verdadero</strong>';
	strXHML+='</div>';
	strXHML+=addAnswerOptions(answer_id);
	strXHML+='</div>';	

	strXHML+='<input type="hidden" name="form_uac_answer_name" id="form_uac_answer_name_'+answer_id+'" value="Verdadero"/>';	
	
	strXHML+=addAnswerComment(answer_id);
	strXHML+=addAnswerClose();
	strXHML+='</div>';


	//FALSO
	id='n'+(++nelement);
	answer_id = 'form_uac_answerquestion_'+id; 
	strXHML+='<div id="'+answer_id+'" class="edit_boxanswer_frame">';
	strXHML+=addAnswerOpen(answer_id);
	strXHML+='<input type="hidden" name="form_uac_quiz_question_answer" value="'+idQuestion+'" />';	
	strXHML+='<input type="hidden" name="form_uac_answer_emperejar"/>';
	strXHML+='<div class="edit_answer_bar">';
	strXHML+='<div class="edit_answer_label">';
	strXHML+='<input onChange="changeAnswerState(this.checked,\'flag_'+answer_id+'\');" type="checkbox" name="form_uac_answer_grade" value="1" />Es correcta &nbsp;&nbsp;<strong>Falso</strong>';
	strXHML+='</div>';
	strXHML+=addAnswerOptions(answer_id);
	strXHML+='</div>';	

	strXHML+='<input type="hidden" name="form_uac_answer_name" id="form_uac_answer_name_'+answer_id+'" value="Falso"/>';

	strXHML+=addAnswerComment(answer_id);
	strXHML+=addAnswerClose();
	strXHML+='</div>';



	strXHML+='</div>';
		
	strXHML+=addQuestionAnswerSpace(question.id);
	strXHML+='</div>'; 	
	strXHML+=addQuestionClose();	
	question.innerHTML = strXHML;    
	quiz.appendChild(question);
	richEditor('form_uac_quiz_question_body'+question.id);
}

function addMatchingQuestion(parent) {
	quiz = $(parent);
	question = document.createElement('DIV');
	id=time()+(++nelement);
	question.id = '2'+''+id; 
	question.className = 'edit_boxquestion_frame';
	var strXHML='';
	strXHML+=addQuestionOpen(question.id);
	strXHML+='<input type="hidden" name="form_uac_quiz_question_type" value="MT" />';
	//PREGUNTA
	strXHML+=addQuestionHeader(question.id,'EM'); 	
	//INFORMACION EXTENDIDA	
	strXHML+='<div id="question_'+question.id+'">';	
	strXHML+=addQuestionBody(question.id);
	//RESPUESTAS
	strXHML+=addQuestionAnswerSpace(question.id);
	strXHML+='<a class="edit_link_add" href="javascript:addAnswerMatchQuestion(\'answers_'+question.id+'\',\''+question.id+'\')">A&ntilde;adir una respuesta</a>';	
	strXHML+='</div>'; 	
	strXHML+=addQuestionClose();	
	question.innerHTML = strXHML;    
	quiz.appendChild(question);
	richEditor('form_uac_quiz_question_body'+question.id);
}