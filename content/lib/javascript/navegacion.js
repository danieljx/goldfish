/* Variable que contiene la lista de slides
* que se encuentran actualmente en el còdigo HTML */
var arregloSlides = new Array();

/******************************************************************************/

/******* VALORES DE SLIDES *******/

/* Corresponde al nombre del id del slide anterior al mostrado actualmente.
* Si es el primer slide, no tiene anterior. */
var slideAnterior = null;

/* Corresponde al nombre del id del slide que se muestra actualmente. */
var slideActual;

/******************************************************************************/
/******* CONDICIONES INICIALES *******/
/******************************************************************************/
$(document).ready(
	function(){
		/* Se cambia la condición inicial del primer slide para mostrarlo.
		* Los demás slides están ocultos de forma predeterminada. */
		$('[id^=slide_]').hide();
		
		/* Tener un arreglo de todas las diapositivas visibles para construir
		el índice de acuerdo a sus valores. */
		arregloSlides = generarArreglo();

		/* Freddy Rojas 04.03.2014
		* Se muestra la primera diapositiva agregada al índice, no necesariamente
		* la primera que se agrega dentro del editor. */
		$(arregloSlides[0]).show();
	
		//Freddy Rojas 11.03.2014
		window.slideActual = setSlideActual();

		/* Se genera la barra de navegación de acuerdo al slide presentado. */
		crearMenuNav(slideActual);
		
		/* Se establece el comportamiento de los enlaces cuando son seleccionados. */
		capturarEnlace();
		abrirEnlaceInterno();

		/* Se despliega el menú de navegación */
		menuDiapositivas(arregloSlides);
		cambiarTamSlide();

		/* Freddy Rojas 14.03.2014
		* Detecta cambios en el tamaño del área visible de la página
		* (ventana del navegador) e invoca la función de recálculo
		* de ser necesario. */
		$(window).on("resize", cambiarTamSlide);
	});

/******************************************************************************/
/******* FUNCIONES DE NAVEGACIÓN *******/
/******************************************************************************/
/* Freddy Rojas 05.03.2014
* Pasa a un arreglo los id de cada uno de los slides que deben ir en el índice. */
function generarArreglo(){
	var arreglo = new Array();

	//Versión original de desarrollo comentada
	//$('.diap_principal').each(
	
	$('.pag_principal').each(
		function(){
				//Freddy Rojas 10.03.2014
				arreglo.push("#"+$(this).attr('id'));
			});
	return arreglo;
}

/* Freddy Rojas 05.03.2014
* Crear barra de navegación según diapositiva actual. */
function crearMenuNav(diapositivaActual){	
	$('#barra').show();
	setEnlacesNav(diapositivaActual);
}

/* Freddy Rojas 06.03.2014
* Crea el conjunto de enlaces "anterior" y "siguiente" según la posición
* del slide que se muestra actualmente. */
function setEnlacesNav(diapositivaActual){
	var posicionSlide = arregloSlides.indexOf(diapositivaActual);
	var diapositivaAnterior = arregloSlides[posicionSlide-1];
	var diapositivaSiguiente = arregloSlides[posicionSlide+1];

	if(posicionSlide==0){
		$("#capAnterior").hide();
	}

	if(posicionSlide>0){
		setEnlace(diapositivaAnterior,'anterior');
	}

	if(posicionSlide<arregloSlides.length){
		if(posicionSlide==arregloSlides.length-1){
			$("#capSiguiente").hide();
		}
		else{
			setEnlace(diapositivaSiguiente,'siguiente');
		}
	}
}

/* Freddy Rojas 06.03.2014
* Permite mostrar los enlaces "anterior o "siguiente" y asignar su dirección de destino.
* Se utiliza el id del div indicado en el parámetro divEnlace para asignar el destino del
* enlace especificado en posicionEnlace. */
function setEnlace(divEnlace,posicionEnlace){
	var enlace;
	var idEnlace;

	switch(posicionEnlace){
		case 'anterior':
			idEnlace = '#capAnterior';
			break;
		case 'siguiente':
			idEnlace = '#capSiguiente';
			break;
	}
	$(idEnlace).show();
	//Freddy Rojas 27.03.2014
	//Versión anterior
	//enlace = getAnclaSlide(divEnlace);
	//$(idEnlace).attr('href',"#"+enlace);
	
	$(idEnlace).attr('href',divEnlace);
}

/* Freddy Rojas 06.03.2014
* Permite obtener el id del ancla que se va a asignar al enlace
* que se está creando. */
function getAnclaSlide(nombreDiapositiva){
	var idAncla;
	
	if(nombreDiapositiva){
		
	
		//Freddy Rojas 27.03.2014
		//Versión anterior
		idAncla = "#"+ $(nombreDiapositiva + ' a').first().attr('id');
		
		return idAncla;
	}
}

/* Freddy Rojas 13.03.2014
* Procesa la acción de los enlaces "anterior" y "siguiente" para mostrar las diapositivas. */
function capturarEnlace(){
	$('#barra a, a.enlacePrincipal').click(
		function(event){
			//Restringe el comportamiento predeterminado del evento onclick
			event.preventDefault();
			var diapositivaActual = getSlideActual();

			/*Freddy Rojas 12.03.2014
			* Versión optimizada, no es necesario especificar si se escoge
			* el enlace de "Cap. Anterior" o "Cap. Siguiente". */
			ocultarSlideActual(diapositivaActual);
			slideEnlace = $(this).attr('href');
			mostrarSlide(slideEnlace);
			setSlideActual();

			/* Se verifica la diapositiva que se muestra actualmente y lueg
			* se genera el conjunto de enlaces para saltar entre capítulos. */
			diapositivaActual = getSlideActual();
			setEnlacesNav(diapositivaActual);
			cambiarTamSlide();
		});
}

/* Freddy Rojas 12.03.2014
* Muestra en pantalla el menú de navegación de acuerdo a los id de cada una
* de las diapositivas. */
function menuDiapositivas(arregloDiapositivas){
	var i=0;
	while(i<arregloDiapositivas.length){
		console.info("Arreglo: "+arregloDiapositivas[i]);
		$("#menuNav").append("<option>"+getTituloDiapositivas(arregloDiapositivas[i])+"</option>");
		i++;
	}
}

/* Freddy Rojas 12.03.2014
* Obtiene los títulos de las diapositivas para mostrarlos en el menú de navegación. */
function getTituloDiapositivas(nombreId){
	return $(nombreId+" h2").text();
}

/******************************************************************************/
/******* FUNCIONES SOBRE COMPORTAMIENTO DE SLIDES *******/
/******************************************************************************/
/* Freddy Rojas 11.03.2014
* Entrega el id de la diapositiva que se está mostrando actualmente */
function getSlideActual(){
	return window.slideActual;
}

/*Freddy Rojas 11.03.2014
* Establece el id de la diapositiva que se muestra actualmente
* en el navegador. */ 
function setSlideActual(){
	window.slideActual = "#"+$("[id^=slide_]:visible").attr("id");
	console.info('setSlideActual: '+window.slideActual);
	return window.slideActual;
}

/* Freddy Rojas 11.03.2014
* Oculta la diapositiva indicada de acuerdo al id del elemento
* que la contiene. */
function ocultarSlideActual(diapositivaActual){
	$(diapositivaActual).hide();
}

/* Freddy Rojas 12.03.2014
* Muestra la diapositiva indicada de acuerdo al id del elemento
* que la contiene. */
function mostrarSlide(diapositivaSeleccionada){
	
	$(diapositivaSeleccionada).show();
	//Freddy Rojas 27.03.2014
	//Versión anterior
	//$(diapositivaSeleccionada).parent().show();
}

/* Freddy Rojas 12.03.2014
/* Controla el comportamiento de los enlaces internos
* (los que no se incluyen en el índice). */
function abrirEnlaceInterno(){
	$('.enlaceInt').click(
		function(event){
			//Restringe el comportamiento predeterminado del evento onclick
			event.preventDefault();

			var diapositivaActual;
			var slideEnlace;
			/* Se modifica el comportamiento del slide actual para ocultarlo
			* y se muestra el div que contiene el enlace seleccionado y su
			* respectivo slide. */
			diapositivaActual = getSlideActual();
			$(diapositivaActual).hide();
			slideEnlace = $(this).attr('href');
			$(slideEnlace).parent().show();
			setSlideActual();
		});
}

/* Freddy Rojas 12.03.2014
* Calcula el espacio máximo disponible en el área cliente del navegador para
* mostrar el slide. Si el contenido del mismo queda por fuera del área
* visible al usuario, se recalcula su altura en proporción al espacio máximo. */
function cambiarTamSlide(){
	var alturaTotalBarra = $('#barra').outerHeight();
	var alturaTotalSlideActual = $(getSlideActual()).outerHeight();
	var alturaTotalMenuSuperior = $('.scorm_top_menu').outerHeight();
	
	//Freddy Rojas 20.03.2014
	console.info("Altura total del menú superior: "+alturaTotalMenuSuperior);
		
	var alturaInternaSlideActual = $(getSlideActual()).height();
	var espacioAdicionalSlideActual = alturaTotalSlideActual - alturaInternaSlideActual;
	var alturaDocumento = $(window).height();
	var diferencia = alturaTotalSlideActual - alturaTotalBarra;
	var espDisponible = alturaDocumento - alturaTotalBarra;

	/* Se toman en cuenta los espacios padding ocupados por el slide
	* para recalcular la altura del mismo y su distancia de la barra
	* de navegación */
	var alturaFinal = espDisponible - alturaTotalMenuSuperior - (4*espacioAdicionalSlideActual);

	$(getSlideActual()).css("height",alturaFinal+"px");
}