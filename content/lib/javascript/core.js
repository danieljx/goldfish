// JavaScript Document
function core_system_busy(param){
	if(confirm(param))
		parent.frames[frames["core"]].document.core.system_busy_cancel();
}

function core_update_title(param){
	parent.document.title=''+param+'';
}


function core_continue_export(data){
	parent.frames[frames["workspace"]].forms_export_file(sysroot+'/export.php',data);
}

function core_download_file(param){
	util_download_file(param);
}


function core_alert(msg){
	alert(msg);
}

function core_quiz_attempt_autosend(id, msg){
	parent.frames[frames["workspace"]].viewer_quiz_attempt_save_and_finish('form_quiz_attempt_'+id,'');
	alert(msg);
}


function core_goto(option, alloptions, anchor, frame){
	core_show_hidden(option, alloptions, frame);
	//alert('#'+anchor+'/'+frame);
	//window.location='#'+anchor;
	//parent.frames[frames[frame]].window.location='#'+anchor;

	parent.frames[frames[frame]].util_goto('#'+anchor);
	//window.location='#45_8';
	void(0);
}

function core_remove_result(result,id,msg,frame){
	//alert(result+'/'+id+'/'+msg+'/'+frame);
	if(msg!="")
		alert(msg);
	switch(result){
		case 'DONE':
			core_drop_inner_html(id,frame);
		break;
		case 'FAIL':
			util_set_opacity(id,10,frame);
		break;

	}
}
function core_assignment_submission_remove_response(id,status){
	parent.frames[frames["workspace"]].viewer_assignment_submission_remove_response(id,status);
}
function core_editor_synchronize_javascript_vars(vars){
	parent.frames[frames["workspace"]].editor_synchronize_javascript_vars(vars);
}

function core_search_show_results(param){
	parent.frames[frames["workspace"]].search_show_results(param);
}

function core_goto_instance(param){
	parent.location.href=wwwroot+'/mod/uac/view.php?id='+param;
}

function core_update_uac_save_changes(){
	parent.frames[frames["workspace"]].update_uac_save_changes();
}

function core_uac_display_update(html){
	//alert('core_uac_display_update');
	parent.frames[frames["workspace"]].document.getElementById('content').innerHTML=html;
	setTimeout("core_uac_render()", 800 );
}

function core_goto_course(param){
	parent.location.href=wwwroot+'/course/view.php?id='+param;
}

function core_share_uac_save_changes(){
	parent.frames[frames["workspace"]].share_uac_save_changes();
}

function core_evaluator_synchronize_javascript_vars(vars){
	parent.frames[frames["workspace"]].evaluator_synchronize_javascript_vars(vars);
}

function core_evaluator_quiz_attempt_display(html){
	parent.frames[frames["workspace"]].evaluator_quiz_attempt_display(html);
}

function core_evaluator_assignment_submission_display(html){
	parent.frames[frames["workspace"]].evaluator_assignment_submission_display(html);
}

function core_uac_save_changes(){
	parent.frames[frames["workspace"]].editor_uac_save_changes();
}

function core_assignment_reload_sections(){
	parent.frames[frames["workspace"]].editor_assignment_reload_sections();
}

function core_bgcolor_change(color){
	parent.frames[frames["workspace"]].com_workspace_bgcolor_change(color);
	document.getElementById('core_body').style.backgroundColor='#'+color;
}

function core_uac_display(html,theme){
	//	util_change_bgcolor(util_get_cookie('lu_bgcolor'));}
	html = html.split('&cl;').join('[');
	html = html.split('&cr;').join(']');

	parent.frames[frames["workspace"]].document.getElementById('content').innerHTML=html;

	if (typeof parent.frames[frames["workspace"]].init_slide_view == "function") {
		parent.frames[frames["workspace"]].init_slide_view();
	}
	parent.frames[frames["workspace"]].executeQueue();

	setTimeout("core_change_theme(\'"+theme+"\')", 800 );
}

function core_change_theme(theme){
	if(theme.indexOf('.jpg') >= 0){
		f = ['core_menu', 'workspace'];
		ai = theme.lastIndexOf('&');
		if(ai >= 0){
			bg = theme.substring(ai+1, theme.length);
			theme = theme.substring(0, ai);
		}else{
			bg = 'white';
		}
		for(i=0; i<f.length; i++){
			o = parent.frames[frames[f[i]]].document.getElementById(f[i]+ "_body");
			os = o.style;
			os.backgroundColor = '#' +bg;
			if(f[i] == 'workspace'){
				o.className = 'theme';
				os.backgroundImage = 'url(../wallpapers/' +theme+ ')';
			}
		}
	}else{
		parent.frames[frames["core_menu"]].document.getElementById("core_menu_body").className = theme+'_top';
		parent.frames[frames["workspace"]].document.getElementById("workspace_body").className = theme;
	}
}

function core_display(html){
	parent.frames[frames["workspace"]].document.getElementById('content').innerHTML=html;
}

var workspaceInnerHTML='';
function core_uac_display_edit(html){
	workspaceInnerHTML=html;
	setTimeout("timeout_core_uac_display_edit()", 2000 );
}

function timeout_core_uac_display_edit(){
	parent.frames[frames["workspace"]].document.getElementById('content').innerHTML=workspaceInnerHTML;
	setTimeout("core_active_rich_editor(\'alltext\')", 800 );
}

function core_uac_new(html){
	workspaceInnerHTML=html;
	setTimeout("timeout_core_uac_new()", 2000 );
}

function timeout_core_uac_new(){
	parent.frames[frames["workspace"]].document.getElementById('content').innerHTML=workspaceInnerHTML;
	setTimeout("core_active_rich_editor(\'alltext\')", 800 );
}

function core_active_rich_editor(param){
	parent.frames[frames["workspace"]].html_editor_active(param);
}

function core_uac_render(){
	//if (!isIE)
	//parent.frames[frames["workspace"]].com_amath_render('content');
	//alert(util_get_cookie('lu_theme'));
	if(util_get_cookie('lu_theme')!=""){
	//if(util_get_cookie('lu_theme')!=null&&util_get_cookie('lu_theme')!=""){
		parent.frames[frames["core_menu"]].document.getElementById("core_menu_body").className = util_get_cookie('lu_theme')+'_top';
		parent.frames[frames["workspace"]].document.getElementById("workspace_body").className = util_get_cookie('lu_theme');
	}else{
		parent.frames[frames["core_menu"]].document.getElementById("core_menu_body").className = 'apparance_7_top';
		parent.frames[frames["workspace"]].document.getElementById("workspace_body").className = 'apparance_7';
	}
}

function core_html_render(id,frame){
	if (!isIE){
		try{
			parent.frames[frames[frame]].com_amath_render(id);
			parent.frames[frames[frame]].com_rich_text_render(id);
		}catch(e){}
	}
	var wsWindow = parent.frames[frames["workspace"]];
	//console.log(wsWindow.$(wsWindow));
	wsWindow.$(wsWindow).resize();
}

function core_put_inner_html(id, html, frame, init){
	if (frame == "") {
		if(document.getElementById(id)){
			document.getElementById(id).innerHTML=html;
		}
	} else {
		html = html.split('&cl;').join('[');
		html = html.split('&cr;').join(']');
		parent.frames[frames[frame]].document.getElementById(id).innerHTML=html;
		
		if (id == "content_box" && (typeof parent.frames[frames[frame]].init_slide_view == "function")) {
			parent.frames[frames[frame]].init_slide_view();
		}
		parent.frames[frames[frame]].executeQueue();
	}
        if (typeof parent.frames[frames[frame]][init]) {
            parent.frames[frames[frame]][init]();
        }     
	setTimeout("core_html_render('"+id+"','"+frame+"')", 200 );
        
}

function core_show_menu(){
	parent.frames[frames['core_menu']].document.getElementById('Menu').style.display = 'block';
}

function core_rich_editor_toggle(id)
{
	parent.frames[frames["workspace"]].rich_editor_toggle(id);
}

function core_activity_added()
{
	parent.frames[frames["workspace"]].activity_added();
}

//Santiago Peñuela A. - 30/01/2015
function core_skill_indicator_added()
{
    parent.frames[frames["workspace"]].indicator_added();
}

function core_skill_added()
{
    parent.frames[frames["workspace"]].skill_added();
}

function core_question_added()
{
    parent.frames[frames["workspace"]].question_added();
}


function core_replace_inner_html(id, html, frame){
    div =document.createElement('DIV');
    alert(html);
	div.innerHTML = html;
	if(frame==""){
		if(document.getElementById(id)){
		document.getElementById(id).innerHTML=div.innerHTML;
		}
	}else{
		parent.frames[frames[frame]].document.getElementById(id).innerHTML=div.innerHTML;
	}
}

function core_drop_inner_html(id, frame){
	if(frame==""){
		if(document.getElementById(id)){
		document.getElementById(id).parentNode.removeChild(document.getElementById(id));
		}
	}else{
		parent.frames[frames[frame]].document.getElementById(id).parentNode.removeChild(parent.frames[frames[frame]].document.getElementById(id));;
	}
}

function core_add_inner_html(id, html, frame){
	//alert(id+"/"+html+"/"+frame);
    div =document.createElement('DIV');
    div.innerHTML = html;
	if(frame==""){
		if(document.getElementById(id)){
		document.getElementById(id).appendChild(div.firstChild);
		}
	}else{
		parent.frames[frames[frame]].document.getElementById(id).appendChild(div.firstChild);
	}
}

function core_swap_container(option, alloptions){
	var brokenstring=alloptions.split(",");
	var i;
	var height=0;
	for(i=0;i<brokenstring.length;i++){
		document.getElementById(brokenstring[i]).style.zIndex=i+1;
	}
	document.getElementById(option).style.zIndex=i+1;
	window.scroll(0,0);
}

function core_show_help(option, alloptions, frame, lang){

	var brokenstring=alloptions.split(",");
	var i;
	for(i=0;i<brokenstring.length;i++){
		if(parent.frames[frames[frame]].document.getElementById(brokenstring[i]))
			parent.frames[frames[frame]].document.getElementById(brokenstring[i]).style.display='none';
	}
	parent.frames[frames[frame]].document.getElementById(option).style.display='block';
	parent.frames[frames[frame]].scroll(0,0);
	data = core_ajax_load(sysroot+'/help/'+lang+'/index.html');
	data = data.replace(/images\//g, sysroot+'/help/'+lang+'/images/');
	parent.frames[frames[frame]].document.getElementById('help_display').innerHTML=data;
}

function core_ajax_load(url,id)
{
	if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	  xmlhttp.open("GET",url,false);
	  xmlhttp.send(null);
	  }
	else
	  {// code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  xmlhttp.open("GET",url,false);
	  // Do not send null for ActiveX
	  xmlhttp.send();
	  }
	  return xmlhttp.responseText;
}


function core_show_hidden(option, alloptions, frame){
	var brokenstring=alloptions.split(",");
	var i;
	for(i=0;i<brokenstring.length;i++){
		if(parent.frames[frames[frame]].document.getElementById(brokenstring[i]))
			parent.frames[frames[frame]].document.getElementById(brokenstring[i]).style.display='none';
	}
	parent.frames[frames[frame]].document.getElementById(option).style.display='block';
	parent.frames[frames[frame]].scroll(0,0);
        
        var initFunction = parent.frames[frames[frame]][option+"_init"];
        if (typeof initFunction === "function") {
            initFunction();
        }
}

function core_quiz_attempt_show_new(param){
	parent.frames[frames["workspace"]].viewer_quiz_attempt_show_new(param);
}

function core_quiz_attempt_show(param){
	parent.frames[frames["workspace"]].viewer_quiz_attempt_show(param);
}

function core_quiz_attempts_summary_show(param){
	parent.frames[frames["workspace"]].viewer_quiz_attempts_summary_show(param);
}

function core_quiz_attempt_finish_and_save(){
	parent.frames[frames["workspace"]].viewer_quiz_attempt_finish_and_save("form_quiz");
	alert("Tiempo agotado, el cuestionario ha sido enviado.");
}

function core_log(param){
	var caller = 'N/A';
	if(arguments && arguments.callee && arguments.callee.caller){
		var wholeFunction = arguments.callee.caller.toString();
		caller = wholeFunction.substring(wholeFunction.indexOf(' ')+1, wholeFunction.indexOf('('));
	}
	console.log('Caller: "' +caller+ '" :: Param: "' +param+ '"');
	//console.log(arguments.callee.caller.toString());
	//newwindow=window.open('','name','height=480,width=640,scrollbars=yes');
	//if (window.focus) {newwindow.focus()}
	//newwindow.document.write(param);
	//return false;
}