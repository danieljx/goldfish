function scorm_float_round(param){
	return Math.round(param*10)/10;
}

function scorm_get_question_points(id){
	return document.getElementById("points_"+id).value;
}

function scorm_clear_answers(id){
	if(document.getElementById('check_'+id)){
		document.getElementById('check_'+id).style.display='inline';
		document.getElementById('clear_'+id).style.display='none';	
	}
}

function scorm_disable_question(id){
	if(document.getElementById('check_'+id)){
		document.getElementById('check_'+id).style.display='none';
		document.getElementById('clear_'+id).style.display='inline';	
	}
}

function scorm_check_answers_multichoice(id){

	// contenedor de la pregunta
	var form=document.getElementById(id);
	var points=0;
	var _points=0;
	var correct=0;

	// hidden que contiene el peso de la pregunta
	var question_points=document.getElementById("points_"+id).value;
	var counter=0;
	var result=0;
	for (var i=0; i<form.elements.length;i++) {

		// recorremos las opciones de respuesta
		if(form.elements[i].type=="radio"||form.elements[i].type=="checkbox"){	
			counter++;
			if(form.elements[i].value!="0")
				correct++;				
			if(form.elements[i].checked===true){
				// value="0" -> falso, value="1" -> correcto
				if(form.elements[i].value!="0"){
					points++;
					document.getElementById('frame_'+form.elements[i].id).className='viewer_option_correct';
				}else{
					_points++;
					document.getElementById('frame_'+form.elements[i].id).className='viewer_option_incorrect';
				}
			}				
		}
	}
	total_points=parseInt(points)-parseInt(_points);
	total_points=total_points<0?0:total_points;
	result=scorm_float_round(question_points*(total_points/correct));	
	document.getElementById('score_'+id).innerHTML='Score: '+result+'/'+question_points;

	scorm_disable_question(id);
	var score=result;	
	// Acá faltaría hacer el reporte SCORM
	return score;	
}

function scorm_clear_answers_multichoice(id){
	var form=document.getElementById(id);
	for (var i=0; i<form.elements.length;i++) {
		if(form.elements[i].type=="radio"||form.elements[i].type=="checkbox"){	
			document.getElementById('frame_'+form.elements[i].id).className='viewer_option_answer';						
		}
	}
	document.getElementById('score_'+id).innerHTML='&nbsp;';
	form.reset();
	scorm_clear_answers(id);
}

function stringsEqual(str1, str2) {
	str1 = String(str1).toLowerCase();
	str2 = String(str2).toLowerCase();
	return ( str1 === str2 );
}

function scorm_check_answer_shortanswer(id){
	var form=document.getElementById(id);
	var question_points=document.getElementById("points_"+id).value;
	var id_element="";
	var j=0;
	var flag=false;
	var points=0;
	for (var i=0; i<form.elements.length;i++) {
		if(form.elements[i].type=="text") {	
			id_element=form.elements[i].id;
			for (j=0; j<form.elements.length;j++) {
				
				if ((form.elements[j].name.indexOf("scorm_answer_option")!=-1) && stringsEqual(form.elements[i].value, form.elements[j].value))	{
					flag=true;
					points++;
				}
			}
		}
	}

	if (flag) {
		document.getElementById('frame_'+id_element).className='viewer_option_correct';
	} else {
		document.getElementById('frame_'+id_element).className='viewer_option_incorrect';	
	}
	result=scorm_float_round(question_points*points);
	document.getElementById('score_'+id).innerHTML='Score: '+result+'/'+question_points;
	//array_quiz_result[id] = result+':'+question_points;
	//scorm_save_result_quiz(id);
	scorm_disable_question(id);
	var score=result;	
	return score;	
}

function scorm_clear_answer_shortanswer(id){
	var form=document.getElementById(id);
	var id_element="";
	for (var i=0; i<form.elements.length;i++) {
		if(form.elements[i].type=="text"){	
			id_element=form.elements[i].id;
		}
	}	
	document.getElementById('frame_'+id_element).className='viewer_option_answer';	
	document.getElementById('score_'+id).innerHTML='&nbsp;';
	scorm_clear_answers(id);
	form.reset();
}

function scorm_check_answers_matching(id){
	var form=document.getElementById(id);
	var question_points=document.getElementById("points_"+id).value;
	var points=0;
	var counter=0;
	var result=0;
	for (var i=0; i<form.elements.length;i++) {
		if(form.elements[i].type=="select-one"){	
			form.elements[i].disabled=true;
			counter++;
			if(document.getElementById('match_'+form.elements[i].id).value==form.elements[i].value){
				points++;
				document.getElementById('frame_'+form.elements[i].id).className='viewer_option_correct';
			}else{
				document.getElementById('frame_'+form.elements[i].id).className='viewer_option_incorrect';
			}		
		}
	}
	result=scorm_float_round(question_points*(points/counter));	
	document.getElementById('score_'+id).innerHTML='Score: '+result+'/'+question_points;
	//array_quiz_result[id] = result+':'+question_points;
	//scorm_save_result_quiz(id);
	scorm_disable_question(id);
	var score=result;	
	return score;
}

function scorm_clear_answers_matching(id){
	var form=document.getElementById(id);
	for (var i=0; i<form.elements.length;i++) 
		if(form.elements[i].type=="select-one"){
			form.elements[i].selectedIndex=0;
			form.elements[i].disabled=false;
			document.getElementById('frame_'+form.elements[i].id).className='viewer_option_answer_select';
		}
	document.getElementById('score_'+id).innerHTML='&nbsp;';
	scorm_clear_answers(id);
	form.reset();
}