var ajax_file_upload_element=0;
function form_ajax_file_upload_add(parent,file_list){
   container = document.getElementById(parent);
   div =document.createElement('DIV');
   div.id='ajax_file_upload_'+(++ajax_file_upload_element);
   div.className = 'viewer_box_file_upload';
   div.innerHTML = '<div id="upload_feedback_'+div.id+'" class="viewer_file_upload"><span id="upload_display_form_'+div.id+'"><form method="POST" enctype="multipart/form-data"><input id="file_'+div.id+'" type="file" size="45" name="file2upload" class="input" onChange="document.getElementById(\'upload_feedback_'+div.id+'\').className=\'viewer_file_uploading\'; return form_file_upload(\''+div.id+'\',\''+file_list+'\');"><a href="javascript:void(0);" onClick="javascript:if(confirm(\'Esta seguro que desea borrar este archivo adjunto?\')){getElementById(\''+div.id+'\').parentNode.removeChild(getElementById(\''+div.id+'\'));}" class="link_delete" >&nbsp;Eliminar</a></form></span></div>';     
   container.appendChild(div);
}

function form_file_upload(id,file_list)
{
	//alert(id);
	$("#upload_feedback_"+id)
	.ajaxStart(function(){
		//start actions
	})
	.ajaxComplete(function(){
		//complete actions
	});

	$.ajaxFileUpload
	(
		{
			url:sysroot+'/upload/doajaxfileupload.php',
			secureuri:false,
			fileElementId:'file_'+id,
			dataType: 'json',
			success: function (data, status)
			{
				if(typeof(data.error) != 'undefined')
				{
					if(data.error != '')
					{
						//alert(data.error);
					}else
					{
						document.getElementById(file_list).value+=document.getElementById('file_'+id).value+'FILEtoOPERATIONaddOPERATIONtoNAME'+data.filename+'FILEtoFILE';
						document.getElementById("upload_feedback_"+id).className="viewer_file_uploaded";
						document.getElementById("upload_display_form_"+id).innerHTML='<a target="_blank" href="download.php?name='+escape(data.filename)+'">'+document.getElementById('file_'+id).value+'</a><a href="javascript:void(0);" onClick="javascript:if(confirm(\'Esta seguro que desea borrar este archivo adjunto?\')){document.getElementById(\''+file_list+'\').value+=\''+document.getElementById('file_'+id).value+'FILEtoOPERATIONremoveOPERATIONtoNAME'+data.filename+'FILEtoFILE\'; getElementById(\''+id+'\').parentNode.removeChild(getElementById(\''+id+'\'));}" class="link_delete" >&nbsp;Eliminar</a>';
					}
				}
			},
			error: function (data, status, e)
			{
				//alert(e);
			}
		}
	)	
	return false;
}

function formPluginFileUpload(id)
{
	
	$("#upload_feedback_"+id)
	.ajaxStart(function(){
		//start actions
	})
	.ajaxComplete(function(){
		//complete actions
	});

	$.ajaxFileUpload
	(
		{
			url:sysroot+'/upload/doajaxfileupload.php',
			secureuri:false,
			fileElementId:'file_'+id,
			dataType: 'json',
			success: function (data, status)
			{
				if(typeof(data.error) != 'undefined')
				{
					if(data.error != '')
					{
						//alert(data.error);
					}else
					{
						//document.getElementById(file_list).value+=document.getElementById('file_'+id).value+'FILEtoOPERATIONaddOPERATIONtoNAME'+data.filename+'FILEtoFILE';
						//document.getElementById("upload_feedback_"+id).className="viewer_file_uploaded";
						//document.getElementById("upload_display_form_"+id).innerHTML='<a target="_blank" href="download.php?name='+escape(data.filename)+'">'+document.getElementById('file_'+id).value+'</a><a href="javascript:void(0);" onClick="javascript:if(confirm(\'Esta seguro que desea borrar este archivo adjunto?\')){document.getElementById(\''+file_list+'\').value+=\''+document.getElementById('file_'+id).value+'FILEtoOPERATIONremoveOPERATIONtoNAME'+data.filename+'FILEtoFILE\'; getElementById(\''+id+'\').parentNode.removeChild(getElementById(\''+id+'\'));}" class="link_delete" >&nbsp;Eliminar</a>';
						console.log('FILE NAME:'+data.filename);
					}
				}
			},
			error: function (data, status, e)
			{
				//alert(e);
			}
		}
	)	
	return false;
}

function forms_cancel_file_import(id){
	document.getElementById('import_button').disabled=true;
	document.getElementById('import_button').className='editor_disabled_import';
	document.getElementById('import_button').value='Import';
	document.getElementById('import_content').disabled=true;
	document.getElementById('import_task').disabled=true;
	document.getElementById('import_quiz').disabled=true;
	document.getElementById('import_games').disabled=true;	
	document.getElementById('import_file').value='';
	document.getElementById("upload_display_form_"+id).innerHTML='<input type="file" onchange="document.getElementById(\'upload_feedback_ajax_file_upload_import\').className=\'viewer_file_uploading\'; return forms_import_from_file(\'ajax_file_upload_import\');" class="input" name="file2upload" size="45" id="file_ajax_file_upload_import"/>';	
}

function forms_import_from_file(id)
{
	document.getElementById('import_button').value='Loading...';
	document.getElementById('import_button').className='editor_loading_import';
	$("#upload_feedback_"+id)
	.ajaxStart(function(){
		//start actions
	})
	.ajaxComplete(function(){
		//complete actions
	});
        
	$.ajaxFileUpload
	(
		{
			url:sysroot+'/upload/doajaxfileupload.php',
			secureuri:false,
			fileElementId:'file_'+id,
			dataType: 'json',
			success: function (data, status)
			{
				if(typeof(data.error) != 'undefined')
				{
					if(data.error != '')
					{
						//alert(data.error);
					}else
					{
						document.getElementById('import_file').value=document.getElementById('file_'+id).value+'FILEtoOPERATIONaddOPERATIONtoNAME'+data.filename+'FILEtoFILE';
						document.getElementById('import_button').disabled=false;
						document.getElementById('import_button').className='editor_enable_import';
						document.getElementById('import_button').value='Import';
						document.getElementById('import_content').disabled=false;
						document.getElementById('import_task').disabled=false;
						document.getElementById('import_quiz').disabled=false;
						document.getElementById('import_games').disabled=false;
						document.getElementById("upload_feedback_"+id).className="viewer_file_uploaded";								
						
						document.getElementById("upload_display_form_"+id).innerHTML='<a target="_blank" href="download.php?name='+escape(data.filename)+'">'+document.getElementById('file_'+id).value+'</a><a href="javascript:void(0);" onClick="javascript:forms_cancel_file_import(\''+id+'\');" class="link_delete" >&nbsp;Eliminar</a>';
					}
				}
			},
			error: function (data, status, e)
			{
				alert(e);
			}
		}
	)	
	return false;
}	


function forms_calendar_enable(id, param){
	var val=param==true?false:true;
	document.getElementById(id+'_day').disabled=val;
	document.getElementById(id+'_month').disabled=val;
	document.getElementById(id+'_year').disabled=val;
	document.getElementById(id+'_hour').disabled=val;
	document.getElementById(id+'_min').disabled=val;
}

function form_elements_disable_and_clear(formid,key,state){
	var Formulario = document.getElementById(formid);
	for (var i=0; i <= Formulario.elements.length-1;i++) {
		if(Formulario.elements[i].id.indexOf(key)!=-1){
		   Formulario.elements[i].disabled=state==true?false:true;
		   switch(Formulario.elements[i].type){
			 case 'radio':
			 	Formulario.elements[i].checked=false;
			 break;
			 default:
			 break;
			}		   
		}		 
	}
}


function form_disable(formid){
	var Formulario = document.getElementById(formid);
	for (var i=0; i <= Formulario.elements.length-1;i++) {
		Formulario.elements[i].disabled=true;
	}
}

function forms_form_quiz_attempt_send(formid){
	var Formulario = document.getElementById(formid);
	var cadenaFormulario = "";
	var textarea="";
	cadenaFormulario = "vars=true"
	for (var i=0; i <= Formulario.elements.length-1;i++) {
		if(Formulario.elements[i].type=="radio"){
			if(Formulario.elements[i].checked==true)
				cadenaFormulario += '&answer_option_response='+util_str_escape(Formulario.elements[i].value);
			else
				cadenaFormulario += '&answer_option_response=0';
		}
		else if(Formulario.elements[i].type=="checkbox"){
			if(Formulario.elements[i].checked==true)
				cadenaFormulario += '&answer_option_response='+util_str_escape(Formulario.elements[i].value);
			else
				cadenaFormulario += '&answer_option_response=0';
		}
		//TEXTAREA
		else if(Formulario.elements[i].type=="textarea"){
                    
			////alert(Formulario.elements[i].className);
			if(Formulario.elements[i].className=="rich_text"){
                            
				textarea=getEditorContent(Formulario.elements[i].id);
				////alert(util_valid_XHTML(textarea));
				textarea=util_str_escape(util_valid_XHTML(textarea));
				cadenaFormulario += '&'+Formulario.elements[i].name+'='+textarea;
				cadenaFormulario += '&'+Formulario.elements[i].name+'_type=rich_text';
			}else{			
			 	textarea=Formulario.elements[i].value;				
				textarea=util_str_escape(util_valid_TEXT(textarea));
				cadenaFormulario += '&'+Formulario.elements[i].name+'='+textarea;	
				cadenaFormulario += '&'+Formulario.elements[i].name+'_type=plain_text';	
			}
		}
		//END TEXTAREA
		//TEXT
		else if(Formulario.elements[i].type=="text"){
			cadenaFormulario += '&answer_option_response='+util_str_escape(Formulario.elements[i].value);		
		}
		//SELECT
		else if(Formulario.elements[i].type=="select-one"){
			cadenaFormulario += '&answer_option_response='+util_str_escape(Formulario.elements[i].value);		
		}		
		else{
			////alert(Formulario.elements[i].type);
			cadenaFormulario += '&'+Formulario.elements[i].name+'='+util_str_escape(Formulario.elements[i].value);
		}
	}
	//alert(cadenaFormulario);
	return cadenaFormulario;
}

function forms_form_send(formid){
	var Formulario = document.getElementById(formid);
	var cadenaFormulario = "";
	var textarea="";
	cadenaFormulario = "vars=true";
        dato2=Formulario.elements.length;
        
	for (var i=0; i <= Formulario.elements.length-1;i++) {
		if(Formulario.elements[i].type=="radio"){
			if(Formulario.elements[i].checked==true)
				cadenaFormulario += '&'+Formulario.elements[i].id+'='+util_str_escape(Formulario.elements[i].value);
			else
				cadenaFormulario += '&'+Formulario.elements[i].id+'=0';
		}
		else if(Formulario.elements[i].type=="checkbox"){
			if(Formulario.elements[i].checked==true)
				cadenaFormulario += '&'+Formulario.elements[i].name+'='+util_str_escape(Formulario.elements[i].value);
		}
		//TEXTAREA
		else if(Formulario.elements[i].type=="textarea"){
			if(Formulario.elements[i].className=="rich_text"){
				textarea=getEditorContent(Formulario.elements[i].id);
				o = document.createElement('div');
				o.innerHTML = textarea.trim();
				cnt = o.innerText?o.innerText:o.textContent;
				cnt = cnt.trim();
				if(cnt!='' || textarea.length>7){
					textarea = util_valid_XHTML(textarea);
					textarea = util_str_escape(textarea);
				}else{
					textarea = '';
				}
				cadenaFormulario += '&'+Formulario.elements[i].name+'='+textarea;
				cadenaFormulario += '&'+Formulario.elements[i].name+'_type=rich_text';
			}else{
                            
//                                if(formid.indexOf('game')=="-1")
//                                textarea=getEditorContent(Formulario.elements[i].id);
//                                else
//			 	textarea=Formulario.elements[i].value;	
//				
//                                textarea=util_str_escape(util_valid_TEXT(textarea));
//				cadenaFormulario += '&'+Formulario.elements[i].name+'='+textarea;	
//				cadenaFormulario += '&'+Formulario.elements[i].name+'_type=plain_text';	
                                
                                if(formid.indexOf('game')=="-1") {
                                    textarea=getEditorContent(Formulario.elements[i].id);
                                    o = document.createElement('div');
                                    o.innerHTML = textarea.trim();
                                    cnt = o.innerText?o.innerText:o.textContent;
                                    cnt = cnt.trim();
                                    if(cnt!='' || textarea.length>7){
                                            textarea = util_valid_XHTML(textarea);
                                            textarea = util_str_escape(textarea);
                                    }else{
                                            textarea = '';
                                    }
                                }
                                
                                else
                                    textarea=Formulario.elements[i].value;	
				
                                // textarea=util_str_escape(util_valid_TEXT(textarea));
				cadenaFormulario += '&'+Formulario.elements[i].name+'='+textarea;	
				cadenaFormulario += '&'+Formulario.elements[i].name+'_type=rich_text';	
                                
			}
		}
		//END TEXTAREA
		else{
			cadenaFormulario += '&'+Formulario.elements[i].name+'='+util_str_escape(Formulario.elements[i].value);
                }
	}
	console.log(cadenaFormulario);
        return cadenaFormulario;
}


function forms_form_complete_send(formid){
	var Formulario = document.getElementById(formid);
	var cadenaFormulario = "";
	var textarea="";
	cadenaFormulario = "vars=true";
	for (var i=0; i <= Formulario.elements.length-1;i++) {
	
		if(Formulario.elements[i].type=="radio"){
			if(Formulario.elements[i].checked==true)
				cadenaFormulario += '&'+Formulario.elements[i].id+'='+util_str_escape(Formulario.elements[i].value);
			else
				cadenaFormulario += '&'+Formulario.elements[i].id+'=0';
		}
		else if(Formulario.elements[i].type=="checkbox"){
			if(Formulario.elements[i].checked==true)
				cadenaFormulario += '&'+Formulario.elements[i].name+'='+util_str_escape(Formulario.elements[i].value);
			else
				cadenaFormulario += '&'+Formulario.elements[i].name+'=0';
		}
		//TEXTAREA
		else if(Formulario.elements[i].type=="textarea"){
			////alert(Formulario.elements[i].className);
			if(Formulario.elements[i].className=="rich_text"){
				textarea=getEditorContent(Formulario.elements[i].id);
				////alert(util_valid_XHTML(textarea));
				textarea=util_str_escape(util_valid_XHTML(textarea));
				cadenaFormulario += '&'+Formulario.elements[i].name+'='+textarea;
				cadenaFormulario += '&'+Formulario.elements[i].name+'_type=rich_text';
			}else{			
			 	textarea=Formulario.elements[i].value;				
				textarea=util_str_escape(util_valid_TEXT(textarea));
				cadenaFormulario += '&'+Formulario.elements[i].name+'='+textarea;	
				cadenaFormulario += '&'+Formulario.elements[i].name+'_type=plain_text';	
			}
		}
		//TEXT
		else if(Formulario.elements[i].type=="text"){
			cadenaFormulario += '&'+Formulario.elements[i].name+'='+util_str_escape(Formulario.elements[i].value);
			//----------------
			
			
		}		
		else{
			cadenaFormulario += '&'+Formulario.elements[i].name+'='+util_str_escape(Formulario.elements[i].value);
			
		}
	}
	return cadenaFormulario;
}

function forms_form_standar_send(formid){
	var Formulario = document.getElementById(formid);
	var cadenaFormulario = "";
	var textarea="";
	cadenaFormulario = "vars=true"
	for (var i=0; i <= Formulario.elements.length-1;i++) {
		if(Formulario.elements[i].type=="radio"){
			if(Formulario.elements[i].checked==true)
				cadenaFormulario += '&'+Formulario.elements[i].name+'='+util_str_escape(Formulario.elements[i].value);
		}
		else if(Formulario.elements[i].type=="checkbox"){
			if(Formulario.elements[i].checked==true)
				cadenaFormulario += '&'+Formulario.elements[i].name+'='+util_str_escape(Formulario.elements[i].value);
		}
		else if(Formulario.elements[i].type=="textarea" && Formulario.elements[i].className=="richEditor"){
			textarea=getEditorContent(Formulario.elements[i].id);
			//textarea=setLocalPath(textarea);
			textarea=util_str_escape(util_valid_XHTML(textarea));
			cadenaFormulario += '&'+Formulario.elements[i].name+'='+textarea;
		}else if(Formulario.elements[i].type=="textarea"){			
			 if(Formulario.elements[i].className.indexOf("simple_textarea")!=-1 || Formulario.elements[i].className.indexOf("simpleText")!=-1){
				textarea=Formulario.elements[i].value;				
			 }else
			 	textarea=base64_decode(Formulario.elements[i].value);
				textarea=util_str_escape(util_valid_XHTML(textarea));
				cadenaFormulario += '&'+Formulario.elements[i].name+'='+textarea;			
		}else
			cadenaFormulario += '&'+Formulario.elements[i].name+'='+util_str_escape(Formulario.elements[i].value);
	}
	////alert(cadenaFormulario);
	return cadenaFormulario;
}


function enviarFormulario(formid){
	var Formulario = document.getElementById(formid);
	var cadenaFormulario = "";
	var textarea="";
	cadenaFormulario = "vars=true"
	for (var i=0; i <= Formulario.elements.length-1;i++) {
		if(Formulario.elements[i].type=="radio"){
			if(Formulario.elements[i].checked==true)
				cadenaFormulario += '&'+Formulario.elements[i].id+'='+util_str_escape(Formulario.elements[i].value);
			else
				cadenaFormulario += '&'+Formulario.elements[i].id+'=0';
		}
		else if(Formulario.elements[i].type=="checkbox"){
			if(Formulario.elements[i].checked==true)
				cadenaFormulario += '&'+Formulario.elements[i].name+'='+util_str_escape(Formulario.elements[i].value);
			else
				cadenaFormulario += '&'+Formulario.elements[i].name+'=0';
		}
		else if(Formulario.elements[i].type=="textarea" && Formulario.elements[i].className=="richEditor"){
			textarea=getEditorContent(Formulario.elements[i].id);
			//textarea=setLocalPath(textarea);
			textarea=util_str_escape(util_valid_XHTML(textarea));
			cadenaFormulario += '&'+Formulario.elements[i].name+'='+textarea;
		}else if(Formulario.elements[i].type=="textarea"){			
			 if(Formulario.elements[i].className.indexOf("simple_textarea")!=-1 || Formulario.elements[i].className.indexOf("simpleText")!=-1){
				textarea=Formulario.elements[i].value;
			 }else
			 	textarea=base64_decode(Formulario.elements[i].value);
				textarea=util_str_escape(util_valid_XHTML(textarea));
				cadenaFormulario += '&'+Formulario.elements[i].name+'='+textarea;			
		}else
			cadenaFormulario += '&'+Formulario.elements[i].name+'='+util_str_escape(Formulario.elements[i].value);
	}
	////alert(cadenaFormulario);
	return cadenaFormulario;
}

function forms_XMLHttpRequest(){
         var objetoAjax=false;
         try {
          /*Para navegadores distintos a internet explorer*/
          objetoAjax = new ActiveXObject("Msxml2.XMLHTTP");
         } catch (e) {
          try {
                   /*Para explorer*/
                   objetoAjax = new ActiveXObject("Microsoft.XMLHTTP");
                   }
                   catch (E) {
                   objetoAjax = false;
          }
         }

         if (!objetoAjax && typeof XMLHttpRequest!='undefined') {
          objetoAjax = new XMLHttpRequest();
         }
         return objetoAjax;
}

function forms_export_file(url,valores)
{
	//alert(valores);
	var ajax=forms_XMLHttpRequest();
	document.getElementById('link_export').style.display='none';
	document.getElementById('feedback_export').style.display='block';
	//alert(url+'*'+valores);
	ajax.open ('POST', url, true);
	ajax.onreadystatechange = function() {
		if (ajax.readyState==1) {
			document.getElementById('feedback_export').innerHTML="Exportando.......";
		}
		else if (ajax.readyState==4){
			   if(ajax.status==200)
			   {					
					util_download_file(ajax.responseText);
					document.getElementById('feedback_export').style.display='none';
					document.getElementById('link_export').style.display='block';										
			   }
			   else if(ajax.status==404){
					document.getElementById('feedback_export').innerHTML = "La direccion no existe";					
			   }else{
					document.getElementById('feedback_export').innerHTML = "Error: ".ajax.status;
			   }
								
		}
	}
	ajax.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
	ajax.send(valores);
	return;
}