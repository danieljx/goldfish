// JavaScript Document
/*
function editor_fondos_eventos() {
            // console.log("agregando eventos");
            $('#fondoss div a').hover(
                function(e) {
            
                    var span = $(this).find('span');
                    $('<div id="tooltip">'+span.html()+'</div>')
                            .appendTo('#fondoss')
                            .css( {
                                top: $(this).offset().top - 100,
                                left: $(this).offset().left - 100
                            })
                            .show();
                },
                function() {
                    $('#tooltip').remove();
                }
            );
}
*/
// JavaScript Document
function editor_skills_indicator_edit(){
	parent.frames[frames["core"]].document.core.skills_indicator_edit();
}

function editor_skills_indicator_disable_edit(){
	parent.frames[frames["core"]].document.core.skills_indicator_disable_edit();
}

function editor_skill_indicator_add(param){
	parent.frames[frames["core"]].document.core.skill_indicator_add(param);
}

function editor_skills_indicator_save(id_form){
	parent.frames[frames["core"]].document.core.skills_indicator_save(forms_form_complete_send(id_form));
}

function editor_skill_indicator_remove(id){
	util_set_opacity("skill_indicator_"+id+"_box",5,"");
	parent.frames[frames["core"]].document.core.skill_indicator_remove(id);
}


function editor_skills_edit(){
	parent.frames[frames["core"]].document.core.skills_edit();
}

function editor_skills_disable_edit(id_form){
	//cambio
	parent.frames[frames["core"]].document.core.skills_disable_edit(forms_form_complete_send(id_form));
}

function editor_skill_add(){
	parent.frames[frames["core"]].document.core.skill_add();
}

function editor_skills_save(id_form){
	parent.frames[frames["core"]].document.core.skills_save(forms_form_complete_send(id_form));
}

function editor_skill_remove(id){
	util_set_opacity("skill_"+id+"_box",5,"");
	parent.frames[frames["core"]].document.core.skill_remove(id);
}

function editor_debug_log(){
	parent.frames[frames["core"]].document.core.debug_log();
}

function editor_get_content_table(){
	return parent.frames[frames["core"]].document.core.get_content_table();
}

function editor_update_title(param){
	parent.document.title = ''+param+'';
}

function editor_name_save(id_form){
	parent.frames[frames["core"]].document.core.name_save(forms_form_send(id_form));
}

function editor_theme_save(param){
	parent.frames[frames["core"]].document.core.theme_save(param);
	util_change_theme(param);
}

function editor_name_disable_edit(){
	parent.frames[frames["core"]].document.core.name_disable_edit();
}

function editor_name_enable_edit(){
	parent.frames[frames["core"]].document.core.name_enable_edit();
}

function editor_slide_move(move,param,id_form){
	parent.frames[frames["core"]].document.core.slide_move(move,param,forms_form_send(id_form));
}

function editor_slide_remove(param){
	parent.frames[frames["core"]].document.core.slide_remove(param);
}

function editor_slide_add(param,id_form){
	parent.frames[frames["core"]].document.core.slide_add(param,forms_form_send(id_form));
}

function editor_slide_duplicate(param,id_form){
	parent.frames[frames["core"]].document.core.slide_duplicate(param,forms_form_send(id_form));
}

function editor_slide_edit(param,id_form){
	parent.frames[frames["core"]].document.core.slide_edit(param,forms_form_send(id_form));
}
// FERNEY funcion que se llama si le dicen que si quiere el buscador
function editor_export_SCORM_buscador() {
    editor_export_SCORM(true);
}
// FERNEY Se agrego que reciba parametro
function editor_export_SCORM(pBus){
    var vBus = false;
    if(typeof pBus !== "undefined") vBus = pBus;
    if(vBus)
    {
        vBus = "si";
    } 
    else
    {
        vBus = "no";
    }
    var testExport = 0;
    var menuframe =  parent.frames[frames["core_menu"]].document;
    $.ajax({
    	url:'ajax/validateXmlExport.php?buscador='+vBus,
    	async:false,
    	success:function(response){
    		console.log(response);
    		if(response == '1')
    			testExport=response;
    	}
    });
    if (menuframe.getElementById('manifest-state').value == '15' || testExport == "1"){
        parent.frames[frames["core"]].document.core.export_SCORM('empty');
    }
    else{ 
        var obj = {type:'Notification', loading:true, title:'El manifiesto de la unidad no está completo', text:'', duration:'1500'};
	editor_show_notification(obj);
    }
//    $.ajax({url: "../services/webservices/scorm_xml/myProjec.html.php",
//                        type: "POST",
//                        datetype: "html",
//                        async: false,
//                        data: ({action: "search", search: search, user_id: user_id}),
//                        success: function(data) {
//                            $('#respuesta2').html(data);
//                        }
//                    });
    //alert();
    //document.write(document.URL);
    
    // return false;
	//var obj = {type:'Notification', loading:true, title:'El manifiesto de la unidad no esta completo', text:'', duration:'1500'};
	//var obj = {type:'Notification', loading:true, title:'El manifiesto de la unidad no esta completo', text:'', duration:50000};
	//editor_show_notification(obj);
	
	//alert('Exportando a SCORM... por favor espere.');
}

function editor_import_template(aTemplate) {
	// Mats Fjellner 2015.09.03
	// Función para insertar plantillas (importar unidades del servidor)
	// Usar la utilidad make_filename.php para crear baseFilename (para el array abajo) emparejado con nombre de archivo
	// /[carpeta datos]/uploads/[nmbre de archivo]
	
	var baseFilenames = {};
	baseFilenames['template-1'] = "Y2YyYTI2ZTZhZTY5ZjA4YzA1ZTY5MGJhNTY0N2M4NjRQQVJUTkFNRXRvUEFSVE5BTUU1MTk1UEFSVE5BTUV0b1BBUlROQU1FMTQ0MTM4ODI0NVBBUlROQU1FdG9QQVJUTkFNRXRlc3Quemlw";
	baseFilenames['template-2'] = "NmJjMmNkNTI5OGNmYmM3NGNkMzdmZGM1NzA2MTAyNTVQQVJUTkFNRXRvUEFSVE5BTUUxMTgxNlBBUlROQU1FdG9QQVJUTkFNRTE0NDEzODgyODdQQVJUTkFNRXRvUEFSVE5BTUV0ZXN0LnppcA==";
	baseFilenames['template-3'] = "MmY1NjVhNWUwYTZjNGM1ZDdjYzQyMDk5NGEwZWQyMTVQQVJUTkFNRXRvUEFSVE5BTUUxODU3UEFSVE5BTUV0b1BBUlROQU1FMTQ0MTM4ODMyM1BBUlROQU1FdG9QQVJUTkFNRXRlc3Quemlw";
	baseFilenames['template-4'] = "ZmQwM2RjYmNjMDA5NGVmZjM3NWNjZWM5MGU0Zjg0ZmFQQVJUTkFNRXRvUEFSVE5BTUUyNzAxN1BBUlROQU1FdG9QQVJUTkFNRTE0NDIyNTQxOTFQQVJUTkFNRXRvUEFSVE5BTUV0ZXN0LnppcA==";

	if (baseFilenames.hasOwnProperty(aTemplate)) {
		$('#lu_templates').fadeOut(1000);
		var aStr = "vars=true&import_file=test.zipFILEtoOPERATIONaddOPERATIONtoNAME"+baseFilenames[aTemplate]+"FILEtoFILE&import_content=1&import_task=1&import_quiz=1&import_games=1";
		parent.frames[frames["core"]].document.core.import_object(aStr);

	} else {
		return false;
	}
	
}

function editor_game_memory_disable_edit(){
	parent.frames[frames["core"]].document.core.game_memory_disable_edit();
}

function editor_game_memory_edit(){
	parent.frames[frames["core"]].document.core.game_memory_edit();
}

function editor_game_hangman_disable_edit(){
	parent.frames[frames["core"]].document.core.game_hangman_disable_edit();
}

function editor_game_hangman_edit(){
	parent.frames[frames["core"]].document.core.game_hangman_edit();
}

function editor_game_wordsearch_disable_edit(){
	parent.frames[frames["core"]].document.core.game_wordsearch_disable_edit();
}

function editor_game_wordsearch_edit(){
	parent.frames[frames["core"]].document.core.game_wordsearch_edit();
}

function editor_game_drag_disable_edit(){
	parent.frames[frames["core"]].document.core.game_drag_disable_edit();
}

function editor_game_aplasta_disable_edit(){
	parent.frames[frames["core"]].document.core.game_aplasta_disable_edit();
}

function editor_game_drag_edit(){
	parent.frames[frames["core"]].document.core.game_drag_edit();
}

function editor_game_aplasta_edit(){
	parent.frames[frames["core"]].document.core.game_aplasta_edit();
}

function editor_game_paint_disable_edit(){
	parent.frames[frames["core"]].document.core.game_paint_disable_edit();
}

function editor_game_paint_edit(){
	parent.frames[frames["core"]].document.core.game_paint_edit();
}

function editor_game_phrases_disable_edit(){
	parent.frames[frames["core"]].document.core.game_phrases_disable_edit();
}

function editor_game_phrases_edit(){
	parent.frames[frames["core"]].document.core.game_phrases_edit();
}

function editor_game_arrange_disable_edit(){
	parent.frames[frames["core"]].document.core.game_arrange_disable_edit();
}

function editor_game_arrange_edit(){
	parent.frames[frames["core"]].document.core.game_arrange_edit();
}

function editor_game_truefalse_disable_edit(){
	parent.frames[frames["core"]].document.core.game_truefalse_disable_edit();
}

function editor_game_truefalse_edit(){
	parent.frames[frames["core"]].document.core.game_truefalse_edit();
}

function editor_game_catch_disable_edit(){
	parent.frames[frames["core"]].document.core.game_catch_disable_edit();
}

function editor_game_catch_edit(){
	parent.frames[frames["core"]].document.core.game_catch_edit();
}

function editor_game_memory_settings_save(id_form){
	parent.frames[frames["core"]].document.core.game_memory_settings_save(forms_form_send(id_form));
}

function editor_game_hangman_settings_save(id_form){
	//alert(forms_form_send(id_form));
	parent.frames[frames["core"]].document.core.game_hangman_settings_save(forms_form_send(id_form));
}

function editor_game_wordsearch_settings_save(id_form){
	parent.frames[frames["core"]].document.core.game_wordsearch_settings_save(forms_form_send(id_form));
}

function editor_game_drag_settings_save(id_form){
	parent.frames[frames["core"]].document.core.game_drag_settings_save(forms_form_send(id_form));
}

function editor_game_aplasta_settings_save(id_form){
	parent.frames[frames["core"]].document.core.game_aplasta_settings_save(forms_form_send(id_form));
}

function editor_game_paint_settings_save(id_form){
	parent.frames[frames["core"]].document.core.game_paint_settings_save(forms_form_send(id_form));
}

function editor_game_phrases_settings_save(id_form){
	parent.frames[frames["core"]].document.core.game_phrases_settings_save(forms_form_send(id_form));
}

function editor_game_arrange_settings_save(id_form){
	parent.frames[frames["core"]].document.core.game_arrange_settings_save(forms_form_send(id_form));
}

function editor_game_truefalse_settings_save(id_form){
	parent.frames[frames["core"]].document.core.game_truefalse_settings_save(forms_form_send(id_form));
}

function editor_game_catch_settings_save(id_form){
	parent.frames[frames["core"]].document.core.game_catch_settings_save(forms_form_send(id_form));
}

function editor_games_settings_disable_edit(id){
	parent.frames[frames["core"]].document.core.games_settings_disable_edit();
}

function editor_games_settings_edit(){
	parent.frames[frames["core"]].document.core.games_settings_edit();
}

function editor_import_object(){
	parent.frames[frames["core"]].document.core.import_object(forms_form_complete_send("form_import"));
}

function editor_copy_object(param){
	parent.frames[frames["core"]].document.core.copy_object(param);
}


function editor_export(){
	parent.frames[frames["core"]].document.core.export_object("empty");
}

function editor_show_hidde_comments(id){
	util_show_hidde_effect(id,'slide');
}

function editor_show_hidde(id){
	util_show_hidde_effect(id,'slide');
}

function editor_question_move_up(id){
	parent.frames[frames["core"]].document.core.quiz_question_move_up(id);
}

function editor_question_move_down(id){
	parent.frames[frames["core"]].document.core.quiz_question_move_down(id);
}

function editor_quiz_settings_view(){
	parent.frames[frames["core"]].document.core.quiz_settings_view();
}

function editor_quiz_questions_view(){
	parent.frames[frames["core"]].document.core.quiz_questions_view();
}

function editor_quiz_embedded_questions_view(){
	parent.frames[frames["core"]].document.core.quiz_embedded_questions_view();
}

function editor_quiz_settings_disable_edit(id){
	parent.frames[frames["core"]].document.core.quiz_settings_disable_edit(id);
}

function editor_quiz_settings_edit(id){
	parent.frames[frames["core"]].document.core.quiz_settings_edit(id);
}
//--
function editor_quiz_settings_save(id_form){
	parent.frames[frames["core"]].document.core.quiz_settings_save(forms_form_send(id_form));
}

function editor_answer_option_remove(id){
	util_set_opacity("activity_"+id+"_box",5,"");
	parent.frames[frames["core"]].document.core.answer_option_remove(id);
}

function editor_set_title(param){
	parent.frames[frames["core"]].document.getElementById('lu_name').innerHTML='<strong>'+param+'</strong>';
}

function editor_question_answer_option_add(id){
	parent.frames[frames["core"]].document.core.question_answer_option_add(id);
}

function editor_quiz_question_remove(id){
	util_set_opacity("question_"+id+"_box",5,"");

	parent.frames[frames["core"]].document.core.quiz_question_remove(id);
}

function editor_quiz_question_disable_edit(id){
	parent.frames[frames["core"]].document.core.quiz_question_disable_edit(id);
}

function editor_quiz_question_edit(id){
	parent.frames[frames["core"]].document.core.quiz_question_edit(id);
}

function editor_quiz_question_save(id_form){
	parent.frames[frames["core"]].document.core.quiz_question_save(forms_form_complete_send(id_form));
}

function editor_quiz_question_add(param){
	parent.frames[frames["core"]].document.core.quiz_question_add(param);
}


function editor_slide_disable_edit(param, id_form){
	if ($('#map-canvas').length) {
		window.detachedMap = $('#map-canvas').detach();
	}
	parent.frames[frames["core"]].document.core.slide_disable_edit(param, forms_form_send(id_form));
	$(window).unbind('resize', editor_resize_editable_field);
}
//---
function editor_assignment_activity_remove(id){
	util_set_opacity("activity_"+id+"_box",5,"");
	parent.frames[frames["core"]].document.core.assignment_activity_remove(id);
}

function editor_content_save(id_form){
    //alert(id_form);

        
        
	parent.frames[frames["core"]].document.core.content_save(forms_form_send(id_form));
}

function editor_assignment_activity_save(id_form){
	parent.frames[frames["core"]].document.core.assignment_activity_save(forms_form_send(id_form));
}

function editor_assignment_activity_add(){
	parent.frames[frames["core"]].document.core.assignment_activity_add();
}

function editor_assignment_activity_edit(id){
	parent.frames[frames["core"]].document.core.assignment_activity_edit(id);
}

function editor_assignment_activity_disable_edit(id){
	parent.frames[frames["core"]].document.core.assignment_activity_disable_edit(id);
}

function editor_slide_enable_edit(param){
	parent.frames[frames["core"]].document.core.slide_enable_edit(param);
	//editor_resize_editable_field();
	$(window).bind('resize', editor_resize_editable_field);
}

function editor_change_answer_option_state(flag, id){
	if(flag==true){
		document.getElementById(id).className='editor_answer_option editor_answer_option_correct';
	}else{
		document.getElementById(id).className='editor_answer_option editor_answer_option_incorrect';
	}
}

function editor_map_save(id_form){
	parent.frames[frames["core"]].document.core.map_save(forms_form_send(id_form));
}

function editor_get_slides()
{
	return parent.frames[frames["core"]].document.core.get_slides();
}

function editor_logout()
{
	parent.frames[frames["core"]].document.core.logout();
}

function editor_resize_editable_field(){
	//alert('editor_resize_editable_field');
	var iframe = $(document.documentElement);
	var windowHeight = $(window).height();
	var div = iframe.find('.editor_html_editor_box');
	var divOffset = $(div).offset();
	div.height((windowHeight-divOffset.top-32)+ 'px');
//	var iframe2 = iframe.find('iframe');
//	var iframe2Offset = $(iframe2).offset();
//	iframe2.height((windowHeight-iframe2Offset.top-32)+ 'px');
}

function editor_menu_select_option(param){
        // FERNEY DURAN IMPLEMENTAR BUSCADOR
        if(enableSearch == "1" && param.indexOf('export') > -1)
        {
            parent.frames[frames["workspace"]].fnExportQuestion(param);
        }
        else
        {
            parent.frames[frames["workspace"]].fnExportClose();
            parent.frames[frames["core"]].document.core.menu_select_option(param);
        }
}

var notificationTimeout;
function editor_show_notification(param){
	var obj = eval(param);
	if(obj.title=='' && !obj.loading && !obj.text){
		return;
	}
	var nGroup = parent.frames[frames["workspace"]].$('#NotificationGroup');
	nGroup.show();
	var nDiv = nGroup.find('#Notification');
	var nLoading = nGroup.find('#Notification .loading');
	var nBackground = nGroup.find('#NotificationBackground');
	var nConfirm = nGroup.find('#Notification .confirm');
	var nTitle = nDiv.find('.title');
	var nText = nDiv.find('.text');
	//nGroup.find('#Overlay').show();
	//parent.frames[frames["core_menu"]].$('#Overlay').show();
	if(obj.loading){
		nLoading.show();
	}else{
		nLoading.hide();
	}
	if(obj.title){
		nTitle.show();
		nTitle.html(obj.title);
	}else{
		nTitle.hide();
	}
	if(obj.text){
		nText.show();
		nText.html(obj.text);
	}else{
		nText.hide();
	}
	nDiv.css({top:'50%',left:'50%',marginTop:'-'+(nDiv.height()/2)+'px',marginLeft:'-'+((nDiv.width()+128)/2)+'px'});
	nDiv.show();
	nBackground.width(nDiv.width()+100);
	nBackground.height(nDiv.height()+50);
	nBackground.css({top:'50%',left:'50%',marginTop:'-'+(nBackground.height()/2)+'px',marginLeft:'-'+((nBackground.width()+128)/2)+'px'});
	nBackground.show();
	if(obj.type.toUpperCase() == 'CONFIRMATION'){
		nConfirm.show();
	}else{
		nConfirm.hide();
	}
	// console.log(obj);
	// console.log(notificationTimeout);
	clearTimeout(notificationTimeout);
	if(!obj.duration || obj.duration!=-1){
		notificationTimeout = setTimeout(function(){nGroup.fadeOut()}, obj.duration?obj.duration:1500);
	}
	// console.log(notificationTimeout);
}

window.getSlides = function(){
	return editor_get_slides();
}

function toggleEditable (value)
	{
		value=value * 1;

		for (i=1; i<=value; i++)
		{
		document.getElementById (i).style.display = "block";
		document.getElementById (i+10).style.display = "block";
		document.getElementById (i+20).style.display = "block";
		document.getElementById (i+30).style.display = "block";
		}

		for (j=value+1; j<=10; j++)
		{
		document.getElementById (j).style.display = "none";
		document.getElementById (j+10).style.display = "none";
		document.getElementById (j+20).style.display = "none";
		document.getElementById (j+30).style.display = "none";
		document.getElementById (j).value = "";
		document.getElementById (j+10).value = "";
		document.getElementById (j+20).value = "";
		document.getElementById (j+30).value = "";
		}
	}

function toggleEditableTrueFalse (value)
	{
		value=value * 1;

		for (i=1; i<=value; i++)
		{
		document.getElementById (i).style.display = "inline";
		document.getElementById (i+10).style.display = "inline";
		document.getElementById (i+20).style.display = "inline";
		document.getElementById ("radio"+(i+30)).style.display = "inline";
		document.getElementById (i+40).style.display = "inline";
		document.getElementById ("radio"+(i+50)).style.display = "inline";
		}

		for (j=value+1; j<=10; j++)
		{
		document.getElementById (j).style.display = "none";
		document.getElementById (j+10).style.display = "none";
		document.getElementById (j+20).style.display = "none";
		document.getElementById ("radio"+(j+30)).style.display = "none";
		document.getElementById (j+40).style.display = "none";
		document.getElementById ("radio"+(j+50)).style.display = "none";
		document.getElementById (j).value = "";
		document.getElementById (j+10).value = "";
		document.getElementById (j+20).value = "";
		document.getElementById ("radio"+(j+30)).value = "";
		document.getElementById (j+40).value = "";
		document.getElementById ("radio"+(j+50)).value = "";
		}
	}

function toggleEditableCatch (value)
	{

		value=value * 1;

		for (i=1; i<=value; i++)
		{
		document.getElementById (i).style.display = "inline";
		document.getElementById (i+10).style.display = "inline";
		}

		for (j=value+1; j<=10; j++)
		{
		document.getElementById (j).style.display = "none";
		document.getElementById (j+10).style.display = "none";
		document.getElementById (j).value = "";
		document.getElementById (j+10).value = "";
		}
	}

function toggleEditableMultipleresp (value)
	{
		value=value * 1;

		for (i=1; i<=value; i++)
		{
		document.getElementById (i).style.display = "inline";
		document.getElementById (i+10).style.display = "inline";
		document.getElementById (i+20).style.display = "inline";
		document.getElementById (i+30).style.display = "inline";
		document.getElementById ("radio"+(i+40)).style.display = "inline";
		document.getElementById (i+50).style.display = "inline";
		document.getElementById (i+60).style.display = "inline";
		document.getElementById ("radio"+(i+70)).style.display = "inline";
		document.getElementById (i+80).style.display = "inline";
		document.getElementById (i+90).style.display = "inline";
		document.getElementById ("radio"+(i+100)).style.display = "inline";
		}

		for (j=value+1; j<=10; j++)
		{
		document.getElementById (j).style.display = "none";
		document.getElementById (j+10).style.display = "none";
		document.getElementById (j+20).style.display = "none";
		document.getElementById (j+30).style.display = "none";
		document.getElementById ("radio"+(j+40)).style.display = "none";
		document.getElementById (j+50).style.display = "none";
		document.getElementById (j+60).style.display = "none";
		document.getElementById ("radio"+(j+70)).style.display = "none";
		document.getElementById (j+80).style.display = "none";
		document.getElementById (j+90).style.display = "none";
		document.getElementById ("radio"+(j+100)).style.display = "none";

		document.getElementById (j).value = "";
		document.getElementById (j+10).value = "";
		document.getElementById (j+20).value = "";
		document.getElementById (j+30).value = "";
		document.getElementById ("radio"+(j+40)).value = "";
		document.getElementById (j+50).value = "";
		document.getElementById (j+60).value = "";
		document.getElementById ("radio"+(j+70)).value = "";
		document.getElementById (j+80).value = "";
		document.getElementById (j+90).value = "";
		document.getElementById ("radio"+(j+100)).value = "";
		}
	}

	function toggleEditablePhrases (value)
	{

		value=value * 1;

		for (i=1; i<=value; i++)
		{
		document.getElementById (i).style.display = "inline";
		document.getElementById (i+10).style.display = "inline";
		}

		for (j=value+1; j<=5; j++)
		{
		document.getElementById (j).style.display = "none";
		document.getElementById (j+10).style.display = "none";
		document.getElementById (j).value = "";
		document.getElementById (j+10).value = "";
		}
	}
	function toggleEditablePaint (value)
	{
		value=value * 1;

		for (i=1; i<=value; i++)
		{
			for (j=0; j<=30; j=j+10)
			{
				document.getElementById (i+j).style.display = "inline";
			}
		}

		for (j=value+1; j<=10; j++)
		{
			for (i=0; i<=30; i=i+10)
			{
				document.getElementById (j+i).style.display = "none";
				document.getElementById (j+i).value = "";
			}
		}
	}

	
 