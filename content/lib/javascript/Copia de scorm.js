// JavaScript Document
var date = new Date();
date.setTime(date.getTime()+(60*24*60*60*1000));
lu_quiz_result=util_get_cookie('lu_quiz_result');
quiz_results=lu_quiz_result.split(";"); 
array_quiz_result = new Object();
for(i=0;i<quiz_results.length;i++){		
	question_results=quiz_results[i].split(",");
	array_quiz_result[question_results[0]] = question_results[1];
} 
alert(lu_quiz_result);

function scorm_save_result_quiz(){
	lu_quiz_result='0,0';
	for(var clave in array_quiz_result){
		if(array_quiz_result[clave]!=undefined)
			lu_quiz_result=lu_quiz_result+';'+clave+','+array_quiz_result[clave];
	}
	util_set_cookie('lu_quiz_result',lu_quiz_result);
	alert(lu_quiz_result);
	alert(util_get_cookie('lu_quiz_result',lu_quiz_result));
	
}

function scorm_get_result_quiz(){
	lu_quiz_result='0,0';
	total=0;
	for(var clave in array_quiz_result){
		if(array_quiz_result[clave]!=undefined)
			total+=parseInt(array_quiz_result[clave]);			
	}
	return total;
}

//util_set_cookie('lu_quiz_result',"",date);

function util_set_cookie (name, value, expires) {
	// n�mero de par�metros variable.
	var argv = util_set_cookie.arguments;
	var argc = util_set_cookie.arguments.length;
	// asociaci�n de par�metros a los campos cookie. 
	var expires = (argc > 2) ? argv[2] : null
	var path = (argc > 3) ? argv[3] : null
	var domain = (argc > 4) ? argv[4] : null
	var secure = (argc > 5) ? argv[5] : false
	// asignaci�n de la propiedad tras la codificaci�n URL
	document.cookie = name + "=" + escape(value) +
		((expires==null) ? "" : ("; expires=" + expires.toGMTString())) +
		((path==null) ? "" : (";path=" + path)) +
		((domain==null) ? "" : ("; domain=" + domain)) +
		((secure==true) ? "; secure" : "");
}

function util_get_cookie (name) {
	InCookie=document.cookie;
	var prop = name + "="; // propiedad buscada
	var plen = prop.length;
	var clen = InCookie.length;
	var i=0;
	if (clen>0) { // Cookie no vac�o
		i = InCookie.indexOf(prop,0); // aparici�n de la propiedad
		if (i!=-1) { // propiedad encontrada
			// Buscamos el valor correspondiente
			j = InCookie.indexOf(";",i+plen);
			if(j!=-1) // valor encontrado
				return unescape(InCookie.substring(i+plen,j));
			else //el �ltimo no lleva ";"
				return unescape(InCookie.substring(i+plen,clen));
		}
		else
			return "";
	}
	else
		return "";
}

function scorm_check_answers_multichoice(id){
	var form=document.getElementById(id);
	var points=0;
	var _points=0;
	var correct=0;
	var question_points=document.getElementById("points_"+id).value;
	var counter=0;
	var result=0;
	for (var i=0; i<form.elements.length;i++) {
		if(form.elements[i].type=="radio"||form.elements[i].type=="checkbox"){	
			counter++;
			if(form.elements[i].value!="0")
				correct++;				
			if(form.elements[i].checked==true){
				if(form.elements[i].value!="0"){
					points++;
					document.getElementById('frame_'+form.elements[i].id).className='viewer_option_correct';
				}else{
					_points++;
					document.getElementById('frame_'+form.elements[i].id).className='viewer_option_incorrect';
				}
			}				
		}
	}
	total_points=parseInt(points)-parseInt(_points);
	total_points=total_points<0?0:total_points;
	result=question_points*(total_points/correct);	
	document.getElementById('score_'+id).innerHTML='Score: '+result+'/'+question_points;
	array_quiz_result[id] = result;
	scorm_save_result_quiz();
}

function scorm_clear_answers_multichoice(id){
	var form=document.getElementById(id);
	for (var i=0; i<form.elements.length;i++) {
		if(form.elements[i].type=="radio"||form.elements[i].type=="checkbox"){	
			document.getElementById('frame_'+form.elements[i].id).className='viewer_option_answer';						
		}
	}
	document.getElementById('score_'+id).innerHTML='&nbsp;';
	form.reset();
}

function scorm_check_answer_shortanswer(id){
	var form=document.getElementById(id);
	var question_points=document.getElementById("points_"+id).value;
	var id_element="";
	var j=0;
	var flag=false;
	var points=0;
	for (var i=0; i<form.elements.length;i++) {
		if(form.elements[i].type=="text"){	
			id_element=form.elements[i].id;
			for (j=0; j<form.elements.length;j++)
				if(form.elements[j].name.indexOf("scorm_answer_option")!=-1&&form.elements[i].value==form.elements[j].value){
				flag=true;
				points++;
				}
		}
	}
	if(flag){
		document.getElementById('frame_'+id_element).className='viewer_option_correct';
	}else
		document.getElementById('frame_'+id_element).className='viewer_option_incorrect';	
	result=question_points*points;
	document.getElementById('score_'+id).innerHTML='Score: '+result+'/'+question_points;
	array_quiz_result[id] = result;
	scorm_save_result_quiz();
}

function scorm_clear_answer_shortanswer(id){
	var form=document.getElementById(id);
	var id_element="";
	for (var i=0; i<form.elements.length;i++) {
		if(form.elements[i].type=="text"){	
			id_element=form.elements[i].id;
		}
	}	
	document.getElementById('frame_'+id_element).className='viewer_option_answer';	
	document.getElementById('score_'+id).innerHTML='&nbsp;';
	form.reset();
}

function scorm_check_answers_matching(id){
	var form=document.getElementById(id);
	var question_points=document.getElementById("points_"+id).value;
	var points=0;
	var counter=0;
	var result=0;
	for (var i=0; i<form.elements.length;i++) {
		if(form.elements[i].type=="select-one"){	
			counter++;
			if(document.getElementById('match_'+form.elements[i].id).value==form.elements[i].value){
				points++;
				document.getElementById('frame_'+form.elements[i].id).className='viewer_option_correct';
			}else{
				document.getElementById('frame_'+form.elements[i].id).className='viewer_option_incorrect';
			}		
		}
	}
	result=question_points*(points/counter);	
	document.getElementById('score_'+id).innerHTML='Score: '+result+'/'+question_points;
	array_quiz_result[id] = result;
	scorm_save_result_quiz();
}

function scorm_clear_answers_matching(id){
	var form=document.getElementById(id);
	for (var i=0; i<form.elements.length;i++) 
		if(form.elements[i].type=="select-one")	
			document.getElementById('frame_'+form.elements[i].id).className='viewer_option_answer_select';
	document.getElementById('score_'+id).innerHTML='&nbsp;';
	form.reset();
}