function rich_editor_toggle(id) {
	if (!tinyMCE.get(id))
	tinyMCE.execCommand('mceAddControl', false, id);
	else
	tinyMCE.execCommand('mceRemoveControl', false, id);
}
