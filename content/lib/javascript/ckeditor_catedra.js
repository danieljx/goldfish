CKEDITOR.appFolder = window.location.pathname.substr(1,window.location.pathname.indexOf("/",1)-1);
CKEDITOR.dataFolder = dataroot.substr(dataroot.lastIndexOf("/")+1);

CKEDITOR.on( 'instanceCreated', function( ev ) {
	CKEDITOR.config.disableNativeSpellChecker = false;
	CKEDITOR.config.scayt_autoStartup = false;
	CKEDITOR.config.removePlugins = 'forms,wsc,scayt,eqneditor,bidi,flash,cut,copy,paste,pastetext,pastefromword';
	CKEDITOR.config.removeButtons = 'paste';
	CKEDITOR.config.extraPlugins = 'voiceUpload,mathjax,video,eqneditor,gmap,iframedialog,conceptmap,plugins_html5,catplugin,edgeanimate,juegos_html5,catjuego,oembed,index,lineheight,blockimagepaste,catembed,catdocument,externalload';
	//CKEDITOR.config.extraPlugins = 'plugins_flash,abbr,voice,video,eqneditor,gmap,iframedialog,conceptmap,plugins_html5,catplugin,edgeanimate,juegos_html5,catjuego';
	ev.editor.config.allowedContent = true;
	CKEDITOR.config.allowedContent = true;
	CKEDITOR.config.format_tags = 'p;h3;h4;h5;h6';
	CKEDITOR.config.format_h3 = { element: 'span', attributes: { 'class': 'h3' } };
	CKEDITOR.config.format_h4 = { element: 'span', attributes: { 'class': 'h4' } };
	CKEDITOR.config.format_h5 = { element: 'span', attributes: { 'class': 'h5' } };
	CKEDITOR.config.format_h6 = { element: 'span', attributes: { 'class': 'h6' } };

	CKEDITOR.on( 'dialogDefinition', function( ev ) {});

});
