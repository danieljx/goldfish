// Mats Fjellner 2014.10.24
// Abre una ventana modal para añadir una competencia

function indicatorAdmin(origin, windowFit) {

    var indicatorAdminTpl = "";
    var indicatorAdminDom = $('<div id="indicator-admin" class="skill-admin"></div>');
    var winH = $(window).height();

    $.get('../'+wwwroot + '/lib/templates/indicatorAdmin.mustache', function(templatesFile) {
        indicatorAdminTpl = templatesFile;
        $.ajax({
            type: "POST",
            url: '../'+wwwroot + "/app/ajax/getSkills.php",
            data: {
                q: "",
                all: "",
                id: (id_unidad !== 0 ? id_unidad : "")
            },
            dataType: 'json',
            success: function(result) {
                result.translation_skill = JSLang.getTranslation("SKILL");
                result.translation_add_indicator = JSLang.getTranslation("ADD_INDICATOR");
                result.translation_code = JSLang.getTranslation("CODE");
                result.translation_description = JSLang.getTranslation("DESCRIPTION");
                result.translation_name = JSLang.getTranslation("NAME");
                result.translation_keywords = JSLang.getTranslation("KEYWORDS");
                result.translation_save = JSLang.getTranslation("SAVE");
                result.translation_cancel = JSLang.getTranslation("CANCEL");
                rendered = Mustache.to_html(indicatorAdminTpl, result);
                indicatorAdminDom.html(rendered)
                        .appendTo($('body'))
                        .css({
                            top: $(window).scrollTop() + (winH * 0.05),
                            height: (windowFit ? (winH * 0.9) : 'auto')
                        })
                        .fadeIn(300);

                indicatorAdminDom.find("#ind_metadata").tokenInput([], {
                    hintText: "",
                    searchingText: "......",
                    noResultsText: "",
                    minChars: 2,
                    allowFreeTagging: true,
                    prePopulate: {}
                });

                indicatorAdminDom.find("ul li").click(function(e) {
                    $(this).find("ul").slideToggle(300);
                });

                indicatorAdminDom.find("button#btn-ok").click(function(e) {
                    e.preventDefault();
		    console.log("add indicator");
                    if ($('#ind_codigo').val() !== "" && $('#ind_nombre').val() !== "" && $('#ind_descripcion').val() !== "" && $('input[name=skill]:checked').length === 1) {
                        console.log("valid");
                        var metaData = $('#ind_metadata').tokenInput("get");
                        var keyList = [];

                        for (var i = 0; i < metaData.length; i++) {
                            keyList.push(metaData[i].name);
                        }

                        $.ajax({
                            type: "POST",
                            url: '../'+wwwroot+"/app/ajax/saveIndicator.php",
                            data: {
                                ind_codigo: $('#ind_codigo').val(),
                                ind_nombre: $('#ind_nombre').val(),
                                ind_descripcion: $('#ind_descripcion').val(),
                                ind_metadata: JSON.stringify(keyList),
                                au_competencia: $('input[name=skill]:checked').val()
                            },
                            dataType: 'json',
                            success: function(result) {
                                origin.tokenInput("add", {id: result.id, name: result.name});
                                indicatorAdminDom.fadeOut(300, function() {
                                    indicatorAdminDom.remove();
                                });
                            }
                        });
                    }
                });

                indicatorAdminDom.find("button#btn-cancel").click(function(e) {
                    e.preventDefault();
                    indicatorAdminDom.fadeOut(300, function() {
                        indicatorAdminDom.remove();
                    });
                });
            }
        });
    });


}

// Mats Fjellner 2014.10.24
// Muy similar a la función indicatorAdmin, dado tiempo se deberían unir.

function skillAdmin(origin) {

    var skillAdminTpl = "";
    var skillAdminDom = $('<div id="skill-admin" class="skill-admin"></div>');
    var winH = $(window).height();

    $.get('../'+wwwroot + '/lib/templates/skillAdmin.mustache', function(templatesFile) {
        skillAdminTpl = templatesFile;
        $.ajax({
            type: "POST",
            url: '../'+wwwroot + "/app/ajax/getSkills.php",
            data: {
                q: "",
                all: "true"
            },
            dataType: 'json',
            success: function(result) {
                result.translation_skills = JSLang.getTranslation("SKILLS");
                result.translation_add_skill = JSLang.getTranslation("ADD_SKILL");
                result.translation_code = JSLang.getTranslation("CODE");
                result.translation_description = JSLang.getTranslation("DESCRIPTION");
                result.translation_name = JSLang.getTranslation("NAME");
                result.translation_keywords = JSLang.getTranslation("KEYWORDS");
                result.translation_save = JSLang.getTranslation("SAVE");
                result.translation_cancel = JSLang.getTranslation("CANCEL");
                
                rendered = Mustache.to_html(skillAdminTpl, result);
                skillAdminDom.html(rendered)
                        .appendTo($('body'))
                        .css({
                            top: $(window).scrollTop() + (winH * 0.05),
                            height: (winH * 0.9)
                        })
                        .fadeIn(300);

                skillAdminDom.find("#com_metadata").tokenInput([], {
                    hintText: "",
                    searchingText: "......",
                    noResultsText: "",
                    minChars: 2,
                    allowFreeTagging: true,
                    prePopulate: {}
                });

                skillAdminDom.find("ul li").click(function(e) {
                    $(this).find("ul").slideToggle(300);
                });

                skillAdminDom.find("button#btn-ok").click(function(e) {
                    e.preventDefault();

                    if ($('#com_codigo').val() !== "" && $('#com_nombre').val() !== "" && $('#com_descripcion').val() !== "" && $('#com_metadata').val() !== "") {

                        var metaData = $('#com_metadata').tokenInput("get");
                        var keyList = [];

                        for (var i = 0; i < metaData.length; i++) {
                            keyList.push(metaData[i].name);
                        }

                        $.ajax({
                            type: "POST",
                            url: wwwroot+"/app/ajax/saveSkill.php",
                            data: {
                                com_codigo: $('#com_codigo').val(),
                                com_nombre: $('#com_nombre').val(),
                                com_descripcion: $('#com_descripcion').val(),
                                com_metadata: JSON.stringify(keyList)
                            },
                            dataType: 'json',
                            success: function(result) {
                                origin.tokenInput("add", {id: result.id, name: result.name});
                                skillAdminDom.fadeOut(300, function() {
                                    skillAdminDom.remove();
                                });
                            }
                        });
                    }
                });

                skillAdminDom.find("button#btn-cancel").click(function(e) {
                    e.preventDefault();
                    skillAdminDom.fadeOut(300, function() {
                        skillAdminDom.remove();
                    });
                });
            }
        });
    });

}
    