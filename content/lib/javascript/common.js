function getFileName(aPath) {
    var sep = (aPath.indexOf("/") > -1) ? "/" : "\\";  
    return aPath.substr(aPath.lastIndexOf(sep)+1);
} 

function getExtension(aPath) {
    var fileName = getFileName(aPath);
    return fileName.substr(fileName.lastIndexOf(".")+1);
}

function removeExtension(aPath) {
    var fileName = getFileName(aPath);
    return fileName.substr(0,fileName.lastIndexOf("."));
}

// shim para navegadores antiguos
if (!Date.now) {
    Date.now = function() { return new Date().getTime(); };
}