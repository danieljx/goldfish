/* 
 * Freddy Rojas 03.02.2014
 * Archivo que define el script para escoger si un slide se agrega o no al indice
 * cuando es exportado a un archivo de objeto SCORM.
 */

function checkAddToIndex(checkbox,modified){
    var addedToIndex = $(checkbox).prop("checked");
    console.log("Chequeado: "+addedToIndex);
    if(addedToIndex==true){
        $(modified).attr("class", "addedToIndex"); 
    }
    else{
         $(modified).removeAttr("class");
    }
}


