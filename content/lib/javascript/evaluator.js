// JavaScript Document

function evaluator_sort_task_summary(filter,by,on){
	parent.frames[frames["core"]].document.core.sort_task_summary(filter,by,on);
}

function evaluator_sort_quiz_attempts(by,on){
	parent.frames[frames["core"]].document.core.sort_quiz_attempts(by,on);
}

function evaluator_sort_quiz_attempts_summary(filter,by,on){
	parent.frames[frames["core"]].document.core.sort_quiz_attempts_summary(filter,by,on);
}

function evaluator_score_students_view(option, text){	
	if(option=='ALL'){		
		document.getElementById("active_score_students").className='link_view_fill';		
		document.getElementById("active_score_students").href="javascript:evaluator_score_students_view('FILL', '"+document.getElementById("active_score_students").innerHTML+"'); void(0);";
		document.getElementById("active_score_students").innerHTML=text;
	}else{
		document.getElementById("active_score_students").className='link_view_all';		
		document.getElementById("active_score_students").href="javascript:evaluator_score_students_view('ALL', '"+document.getElementById("active_score_students").innerHTML+"'); void(0);";
		document.getElementById("active_score_students").innerHTML=text;
	}
	parent.frames[frames["core"]].document.core.score_students_view(option);
	return false;
}

function evaluator_submissions_student_view(option, text){	
	if(option=='ALL'){		
		document.getElementById("active_submissions_student").className='link_view_fill';		
		document.getElementById("active_submissions_student").href="javascript:evaluator_submissions_student_view('FILL', '"+document.getElementById("active_submissions_student").innerHTML+"'); void(0);";
		document.getElementById("active_submissions_student").innerHTML=text;
	}else{
		document.getElementById("active_submissions_student").className='link_view_all';		
		document.getElementById("active_submissions_student").href="javascript:evaluator_submissions_student_view('ALL', '"+document.getElementById("active_submissions_student").innerHTML+"'); void(0);";
		document.getElementById("active_submissions_student").innerHTML=text;
	}
	parent.frames[frames["core"]].document.core.submissions_student_view(option);
	return false;
}

function evaluator_quiz_attempts_view(){
	parent.frames[frames["core"]].document.core.quiz_attempts_view();
}

function evaluator_quiz_attempt_view_all(){
	parent.frames[frames["core"]].document.core.quiz_attempt_view_all();
}

function evaluator_quiz_attempts_review(form){
	//alert(forms_form_standar_send(form));
	parent.frames[frames["core"]].document.core.quiz_attempts_review(forms_form_standar_send(form));
}

function evaluator_quiz_attempt_review(form){
	//alert(forms_form_standar_send(form));
	parent.frames[frames["core"]].document.core.quiz_attempts_review(forms_form_standar_send(form));
}

function evaluator_quiz_attempt_remove(id,msg){
	if(confirm(msg)){
		util_set_opacity("quiz_attempt_"+id+"_row",5,"");
		parent.frames[frames["core"]].document.core.quiz_attempt_remove(id);
	}
}

function evaluator_quiz_attempt_display_remove(id,msg){
	if(confirm(msg)){
		util_set_opacity("quiz_attempt_"+id+"_row",5,"");
		parent.frames[frames["core"]].document.core.quiz_attempt_display_remove(id);
	}
}

function evaluator_assignment_submission_grade_disable_edit(id){
	parent.frames[frames["core"]].document.core.assignment_submission_grade_disable_edit(id);
}
function evaluator_user_goal_save(form){
	parent.frames[frames["core"]].document.core.user_goal_save(forms_form_send(form));
}
function evaluator_assignment_submission_grade_edit(id){
	parent.frames[frames["core"]].document.core.assignment_submission_grade_edit(id);
}
function evaluator_assignment_submission_grade_remove(id){
	util_set_opacity("submission_grade_"+id+"_box",5,"");
	parent.frames[frames["core"]].document.core.assignment_submission_grade_remove(id);
}

function evaluator_assignment_submission_grade_save(form){
	//alert(forms_form_send(form));
	parent.frames[frames["core"]].document.core.assignment_submission_grade_save(forms_form_send(form));
}

function evaluator_assignment_submission_grade_add(id){
	parent.frames[frames["core"]].document.core.assignment_submission_grade_add(id);
}


function evaluator_team_save_changes(){
	parent.frames[frames["core"]].document.core.team_save_changes(forms_form_send('form_teams'));	
}

var Teams= new Array();
function evaluator_synchronize_javascript_vars(vars){
	var brokenstringvars=vars.split(";");
	var brokenstringvar=brokenstringvars[0].split("/"); 
	var brokenstring=brokenstringvar[1].split(","); 
	var _brokenstring;
	var i;
 	Teams = new Array();
	for(i=0;i<brokenstring.length;i++){		
		Teams[i]=brokenstring[i].split(":");
	}
}


function evaluator_team_options_add(idTeam){
	var strXHML='';
	strXHML+='<div class="edit_question_header"> ';   
	strXHML+='<div class="edit_question_icons"> ';   	
	strXHML+='<a class="link_delete" href="javascript:if(confirm(\'Esta seguro que desea eliminar este equipo?\')){document.getElementById(\''+idTeam+'\').parentNode.removeChild(document.getElementById(\''+idTeam+'\'));} void(0);">&nbsp;&nbsp;</a>';   
	strXHML+='</div> '; 	
	strXHML+='</div> '; 
	return strXHML;
}

function evaluator_team_add(parent) {
	container = document.getElementById(parent);
	team = document.createElement('DIV');
	id=time()+(++nelement);
	team.id = id; 
	team.className = 'evaluator_boxteam_frame';
	
	var strXHML='';	
	strXHML+=evaluator_team_options_add(team.id);
	strXHML+='<div class="edit_boxteam">';
	strXHML+='<div class="edit_team">';

	strXHML+='<input type="hidden" value="'+team.id+'" name="form_evaluator_team_id"/>';	
	strXHML+='<div id="team_'+team.id+'">';		

	//MIEMBROS
	strXHML+='<div class="edit_boxmemberlist" id="memebers_'+team.id+'">';
	strXHML+='</div>';
	
	strXHML+='<a class="edit_link_add" href="javascript:evaluator_team_member_add(\'memebers_'+team.id+'\',\''+team.id+'\')">A&ntilde;adir un miembro al equipo</a>';	
	
	strXHML+='</div>'; 	
	strXHML+='</div>';	
	strXHML+='</div>';
	team.innerHTML = strXHML;    
	container.appendChild(team);
}

function evaluator_member_options_add(idMember){
	var strXHML='';
	strXHML+='<div  class="edit_answer_options">';
	strXHML+='<a href="javascript:if(confirm(\'Esta seguro que desea eliminar este miembro del equipo?\')){ document.getElementById(\''+idMember+'\').parentNode.removeChild(document.getElementById(\''+idMember+'\')); } void(0);" class="link_delete" >&nbsp;&nbsp;</a>';
	strXHML+='</div>';
	return strXHML;
}


function evaluator_team_member_add(parent,idTeam) {
	question = document.getElementById(parent);
	member = document.createElement('DIV');
	id='n'+(++nelement);
	member.id = 'form_evaluator_member_'+id; 
	member.className = 'evaluator_boxmember_frame';
	
	var strXHML='';
	strXHML+='<div class="edit_boxanswer">';
	strXHML+='<div id="flag_'+member.id+'" class="edit_answer">';
	strXHML+='<input type="hidden" name="form_evaluator_team_member" value="'+idTeam+'" />';	
	strXHML+='<div class="edit_answer_bar">';
	strXHML+='<div class="edit_answer_label">';
	strXHML+='<select name="form_evaluator_team_member_id" >';
	strXHML+='<option value="0" selected="selected">Elija</option>';
	
	var form = document.getElementById('form_teams')
	var selectItem=form.getElementsByTagName("select"); 	
	for(i=0;i<Teams.length;i++){
		var flag=true;
		for(j = 0; j < selectItem.length; j++){			
			if(Teams[i][0]==selectItem[j].value)
				flag=false;			
		}
		if(flag)
			strXHML+='<option value="'+Teams[i][0]+'">'+Teams[i][1]+'</option>';			
	}		
	
	strXHML+='</select>';
	strXHML+='</div>';
	
	strXHML+=evaluator_member_options_add(member.id);
	
	strXHML+='</div>';		
	strXHML+='</div>';
	strXHML+='</div>';	
	
	member.innerHTML = strXHML;    
	question.appendChild(member);
}

//END TEAMS
function evaluator_quiz_attempt_delete(param){
	document.getElementById('attempt_'+param).style.display='none';
	parent.frames[frames["core"]].document.core.quiz_attempt_delete(param);	
}

function evaluator_assignment_submission_delete(param){
	document.getElementById('submission_'+param).style.display='none';
	parent.frames[frames["core"]].document.core.assignment_submission_delete(param);	
}

function evaluator_assignment_submission_display(param){
	document.getElementById('submission').innerHTML=param;
	util_show_hidden("calificar","tareas,cuestionario,calificar,revisar,equipos");	
	util_amath('submission');
}

function evaluator_quiz_attempt_display(param){
	document.getElementById('attempt').innerHTML=param;
	util_show_hidden("revisar","tareas,cuestionario,calificar,revisar,equipos");	
	util_amath('attempt');
}


function evaluator_teams_display(param){
	document.getElementById('teams').innerHTML=param;
	util_show_hidden("revisar","tareas,cuestionario,calificar,revisar,equipos");	
	util_amath('attempt');
}

function evaluator_quiz_attempt_review(param){
	parent.frames[frames["core"]].document.core.quiz_attempt_review(param);	
}

function evaluator_assignment_submission_eval(param){
	parent.frames[frames["core"]].document.core.assignment_submission_eval(param);	
}

function evaluator_assignment_submission_score_save(param){
	parent.frames[frames["core"]].document.core.assignment_submission_score_save(forms_form_send('form_assignment_grade'));	
}