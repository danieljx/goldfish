<?php
/*
Uploadify v2.1.4
Release Date: November 8, 2010

Copyright (c) 2010 Ronnie Garcia, Travis Nickels

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
function clean($s){
  //return $s;
  $s = utf8_decode($s);
  $t = '';
  for($si=0; $si<strlen($s); $si++){
    $ok = false;
    $c = $s[$si];
    $cc = ord($c);
    //Rangos Permitidos
    $r = 'azAZ09';
    //Caracteres Permitidos
    $p = '-._';
    //Reemplazos (si coincide alguno de una cadena se cambia por el primero de la misma)
    $rs = array(
      'aáàâãä',
      'eéèêë',
      'iíìîï',
      'oóòôõö',
      'uúùûü',
      'cç',
      'nñ',
      'AÁÀÂÃÄ',
      'EÉÈÊË',
      'IÍÌÎÏ',
      'OÓÒÔÕÖ',
      'UÚÙÛÜ',
      'CÇ',
      'NÑ'
    );
    for($i=0; $i<strlen($r) && !$ok; $i+=2){
      if(ord($r[$i])<=$cc && $cc<=ord($r[$i+1])){
        $t .= $c;
        $ok = true;
      }
    }
    for($i=0; $i<strlen($p) && !$ok; $i++){
      if(ord($p[$i]) == $cc){
        $t .= $c;
        $ok = true;
      }
    }
    for($i=0; $i<count($rs) && !$ok; $i++){
      for($j=1; $j<strlen($rs[$i]) && !$ok; $j++){
        if(ord($rs[$i][$j]) == $cc){
          $t .= $rs[$i][0];
          $ok = true;
        }
      }
    }
    if(!$ok){
      $t .= '_';
    }
  }
  return $t;
}
if (!empty($_FILES)) {
	$tempFile = $_FILES['Filedata']['tmp_name'];
	$targetPath = $_REQUEST['folder'] . '/';
	$fullName = $_FILES['Filedata']['name'];
	$fullName = clean($fullName);
        $fileName = strtolower(substr($fullName, 0, strrpos($fullName, '.')));
        $fileExt = strtolower(substr($fullName, strrpos($fullName, '.')));
        $fileDir = str_replace('//','/',$targetPath);
	$targetFile = $fileDir.$fileName.$fileExt;
        $i = 0;
        while(file_exists($targetFile)){
		$targetFile = $fileDir.$fileName.'_'.$i.$fileExt;
		$i++;
	}
	
	// $fileTypes  = str_replace('*.','',$_REQUEST['fileext']);
	// $fileTypes  = str_replace(';','|',$fileTypes);
	// $typesArray = split('\|',$fileTypes);
	// $fileParts  = pathinfo($_FILES['Filedata']['name']);
	
	// if (in_array($fileParts['extension'],$typesArray)) {
		// Uncomment the following line if you want to make the directory if it doesn't exist
		// mkdir(str_replace('//','/',$targetPath), 0755, true);
		
		move_uploaded_file($tempFile,$targetFile);
		echo str_replace($_SERVER['DOCUMENT_ROOT'],'',$targetFile);
	// } else {
	// 	echo 'Invalid file type.';
	// }
}
?>