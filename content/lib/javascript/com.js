// JavaScript Document
function com_workspace_bgcolor_change(color){		
	document.getElementById('workspace_body').style.backgroundColor='#'+color;
}

function com_rich_text_render(id){
	oDiv = document.getElementById(id); 
	e = oDiv.getElementsByTagName("textarea"); 	
	for(var i=0;i<e.length;i++){
		if(e[i].className=='_rich_text')
			rich_editor_toggle(e[i].id); 
	}
}

function com_amath_render(id) {
	oDiv = document.getElementById(id); 
	mathItems = oDiv.getElementsByTagName("latex"); 
	var str;
	var outnode;
	var n;
	var j;
	for(var i = 0; i < mathItems.length; i++){
		tex=mathItems[i].innerHTML;
		//if(tex.indexOf('array')!=-1){			
			nodeImg = document.createElement("img");
			nodeImg.setAttribute("hspace","8");
			nodeImg.setAttribute("vspace","2");
			nodeImg.setAttribute("alt",tex);
			nodeImg.setAttribute("title","Math");
			nodeImg.src = wwwroot+'/filter/tex/view.php?tex='+util_str_escape(tex);
			outnode = mathItems[i];
			n = outnode.childNodes.length;		
			for (j=0; j<n; j++)
				outnode.removeChild(outnode.firstChild);		
			outnode.appendChild(nodeImg);
		/*}else{			
			str = 'amath   `'+mathItems[i].innerHTML+'`';
			str=str.replace(/\\mbox/gi, "\\text");
			str=str.replace(/\\triangle/gi, "\\Delta");
			str=str.replace(/\\mp/g, "\\ {::}_(\\+)^\\-\\ ");
			str=str.replace(/Julio/gi, "\"Julio\"");
			str=str.replace(/&gt;/g, ">");
			str=str.replace(/&lt;/g, "<");
			str=str.replace(/\\,/g, " ");
			outnode = mathItems[i];
			n = outnode.childNodes.length;		
			for (j=0; j<n; j++)
				outnode.removeChild(outnode.firstChild);		
			outnode.appendChild(document.createTextNode(str));
			AMprocessNode(outnode);		
		}*/

	}
}

function com_image_render(id) {
	var str;
	var outnode;
	var n;
	var j;
	var nodeLabel;
	var nodeBgLabel;
	var height=0;
	oDiv = document.getElementById(id); 
	imgItems = oDiv.getElementsByTagName("img"); 
	for(i = 0; i < imgItems.length; i++){
		if(imgItems[i].alt!=""&&imgItems[i].width>100){
			if(imgItems[i].id=="")
				imgItems[i].id="img"+i;
			imgItems[i].className='viewer_image';
			
			
			nodeLabel = document.createElement("div");
			nodeLabel.className='viewer_label';
			nodeLabel.style.witdh=imgItems[i].width+'px';
			
			height=Math.max((imgItems[i].alt.length*40)/50,20);
			//alert(height);
			nodeLabel.style.height=height+'px';
			imgItems[i].style.paddingBottom=(height+2)+'px';
			
			nodeLabel.id='label_'+imgItems[i].id;
			
			//<strong>Figura '+i+'.</strong> 
			nodeLabel.innerHTML = '<span>'+imgItems[i].alt+'<span>';		
			
			
			nodeBgLabel = document.createElement("div");		
			nodeBgLabel.id='bg_label_'+imgItems[i].id;
			nodeBgLabel.className='bg_label';
			nodeBgLabel.innerHTML="&nbsp;";
	
			oDiv.appendChild(nodeLabel);
			oDiv.appendChild(nodeBgLabel);
			com_media_label(imgItems[i].id);		
		}
	}
}

function com_image_update_render(id) {
	var n;
	var j;
	oDiv = document.getElementById(id); 
	imgItems = oDiv.getElementsByTagName("img"); 
	for(i = 0; i < imgItems.length; i++){
		if(imgItems[i].alt!=""&&imgItems[i].width>100){
			com_media_label(imgItems[i].id);		
		}
	}
}


function com_media_label(id) {
  var top;
  var height='0';
  var image = document.getElementById(id);
  var label = document.getElementById('label_'+id);
  var bg_label = document.getElementById('bg_label_'+id);
	
  var pos = util_get_absolute_element_position(image);
  label.style.left=(pos.left+2)+'px';
  top=image.height+pos.top-height+2;
  label.style.top=top+'px';
  label.style.display='block';
  label.style.width=(image.width-8)+'px'; 

  bg_label.style.left=(pos.left+2)+'px';
  top=image.height+pos.top-height+2;
  bg_label.style.top=top+'px';
  bg_label.style.display='block'; 
  bg_label.style.width=image.width+'px';  
  bg_label.style.height=height+'px';    
}