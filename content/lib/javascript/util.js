function util_set_class(id,c){
	//alert(id+'/'+class);
	document.getElementById(id).className=c;
}


function util_set_class_option(id,name){	
	var Elements=document.getElementsByName(name);
	for(var i=0;i<Elements.length;i++){
		document.getElementById(Elements[i].id+'_content').className='viewer_answer_option';
    }
	document.getElementById(id).className='viewer_answer_option_selected';
}

function util_get_cookie (name) {
	InCookie=document.cookie;
	var prop = name + "="; // propiedad buscada
	var plen = prop.length;
	var clen = InCookie.length;
	var i=0;
	if (clen>0) { // Cookie no vac�o
		i = InCookie.indexOf(prop,0); // aparici�n de la propiedad
		if (i!=-1) { // propiedad encontrada
			// Buscamos el valor correspondiente
			j = InCookie.indexOf(";",i+plen);
			if(j!=-1) // valor encontrado
				return unescape(InCookie.substring(i+plen,j));
			else //el �ltimo no lleva ";"
				return unescape(InCookie.substring(i+plen,clen));
		}
		else
			return "";
	}
	else
		return "";
}
function util_set_cookie (name, value, expires) {
	// n�mero de par�metros variable.
	var argv = util_set_cookie.arguments;
	var argc = util_set_cookie.arguments.length;
	// asociaci�n de par�metros a los campos cookie. 
	var expires = (argc > 2) ? argv[2] : null
	var path = (argc > 3) ? argv[3] : null
	var domain = (argc > 4) ? argv[4] : null
	var secure = (argc > 5) ? argv[5] : false
	// asignaci�n de la propiedad tras la codificaci�n URL
	document.cookie = name + "=" + escape(value) +
		((expires==null) ? "" : ("; expires=" + expires.toGMTString())) +
		((path==null) ? "" : (";path=" + path)) +
		((domain==null) ? "" : ("; domain=" + domain)) +
		((secure==true) ? "; secure" : "");
}

function util_remove_cookie (name) {
	var today = new Date();
	var expire = new Date("Feb 12, 1980"); // fecha pasada
	// Asignaci�n de la propiedad username
	util_set_cookie(name, null, expire);
	// Asignaci�n de la propiedad lastime
	return true;
}

function util_change_bgcolor(color){
	//68B066
	//parent.frames[frames["core"]].document.getElementById("core_body").style.background = '#588EBD';
	//parent.frames[frames["workspace"]].document.getElementById("workspace_body").style.backgroundColor = color;
	

	//parent.frames[frames["workspace"]].document.getElementById("content").className = 'bg_box';
	//parent.frames[frames["workspace"]].document.getElementById("workspace_body").style.backgroundColor = '#1C5598';
	parent.frames[frames["core"]].document.getElementById("core_body").className = 'appearance_windows_top';
	parent.frames[frames["workspace"]].document.getElementById("workspace_body").className = 'appearance_windows';
	//parent.frames[frames["core"]].document.getElementById("core_body").className = 'apparance_windows';
	util_remove_cookie('lu_bgcolor');
	var date = new Date();
	date.setTime(date.getTime()+(60*24*60*60*1000));
	util_set_cookie('lu_bgcolor',color,date);
}

/* Santiago Peñuela Arcila 26/06/2014
 * Esta función es la que realiza el cambio de "tema" (Theme) del editor. (El cambio de fondo y tono del editor).
 */
function util_change_theme(theme){
	/*/
	theme = 'appearance_2';
	parent.frames[frames["core_menu"]].document.getElementById("core_menu_body").className = theme+'_top';
	parent.frames[frames["workspace"]].document.getElementById("workspace_body").className = theme;
	/*/
	f = ['core_menu', 'workspace'];
	ai = theme.lastIndexOf('&');
	if(ai >= 0){
		bg = theme.substring(ai+1, theme.length);
		theme = theme.substring(0, ai);
        }else{
		bg = 'white';
	}
	for(i=0; i<f.length; i++){
		o = parent.frames[frames[f[i]]].document.getElementById(f[i]+ "_body");
		os = o.style;
		os.backgroundColor = '#' +bg;
		o.className = 'theme';
		if(f[i] == 'workspace'){
			os.backgroundImage = 'url(../wallpapers/' +theme+ ')';
		}
	}

        util_remove_cookie('lu_theme');
        var date = new Date();
        date.setTime(date.getTime()+(60*24*60*60*1000));
        util_set_cookie('lu_theme',theme,date);   
}
/* Santiago Peñuela Arcila 26/06/2014
 * Esta función permite capturar el "theme" actual y aplicar el nuevo solo para previsualizarlo.
 * Se necesita guardar los cambios para que el cambio perdure.
 */
function util_obtener_theme()
{
    f = ['workspace'];
    var fondoActual = "";
    var fondoActualArreglado = "";
    var colorDeFondoActual = "";
    var colorDeFondoActualArreglado = "";
    var themeActual = "";
    
    for(i=0; i<f.length; i++)
    {
        o = parent.frames[frames[f[i]]].document.getElementById(f[i]+ "_body");
        os = o.style;
        fondoActual = os.backgroundImage;
        colorDeFondoActual = os.backgroundColor;
        o.className = 'theme';
    }
    
    fondoActualArreglado = fondoActual.split('url("../wallpapers/');
    fondoActualArreglado = fondoActualArreglado.join('');
    fondoActualArreglado = fondoActualArreglado.split('")');
    fondoActualArreglado = fondoActualArreglado.join('');
    
    colorDeFondoActualArreglado = jQuery.Color(colorDeFondoActual).toHexString('#');
    colorDeFondoActualArreglado = colorDeFondoActualArreglado.split("#");
    colorDeFondoActualArreglado = colorDeFondoActualArreglado.join('');
    colorDeFondoActualArreglado = colorDeFondoActualArreglado.split("ff");
    colorDeFondoActualArreglado = colorDeFondoActualArreglado.join('');
    
    themeActual = fondoActualArreglado + "&" + colorDeFondoActualArreglado;
    
    // console.log("Este es el tema Actual: "+themeActual);
    return themeActual;
}

function util_recuperar_theme(theme,persistir)
{
    if(persistir == 'false')
    {
        util_change_theme(theme);
    }
    else
    {
        console.log("NADA");
    }
}

function util_print(id,title)
{
	var prtContent = document.getElementById(id);
	var WinPrint = window.open('','','left=0,top=0,width=600,height=480,toolbar=0,scrollbars=1,status=0,menubar =1');
	var style='<link rel="stylesheet" type="text/css" href="theme/nothing/css/default.css"><link rel="stylesheet" type="text/css" href="theme/nothing/css/viewer.css"><link rel="stylesheet" type="text/css" href="theme/nothing/css/evaluator.css"><link rel="stylesheet" type="text/css" href="theme/nothing/css/editor.css">';
	
	WinPrint.document.write('<html><head><title>'+title+'</title>'+style+'</head><body><div style="width:600px">'+prtContent.innerHTML+'</div></body></html>');
	WinPrint.document.close();
	WinPrint.focus();
	WinPrint.print();
	//WinPrint.close();	
}//sysroot+'/editor/' ;

function util_download_file(url)
{
	var WinPrint = window.open(url);
}//sysroot+'/editor/' ;

function util_goto(anchor){
	//alert('Espere('+anchor+')');
	//setTimeout("util_go_to(\'"+anchor+"\')",0);
	setTimeout("window.location=\'"+anchor+"\'",0);
	//window.location=anchor;
}

function util_go_to(anchor){
	//alert(anchor);
	window.location=anchor;
}

function util_drop_inner_html(id, frame){	
	if(frame==""){ 
		if(document.getElementById(id)){
		document.getElementById(id).parentNode.removeChild(document.getElementById(id));
		}
	}else{		
		parent.frames[frames[frame]].document.getElementById(id).parentNode.removeChild(parent.frames[frames[frame]].document.getElementById(id));		
	}
}

function util_set_opacity(id,value,frame) {
	if(frame==""){ 
		if(document.getElementById(id)){
			document.getElementById(id).style.opacity = value/10;
			document.getElementById(id).style.filter = 'alpha(opacity=' + value*10 + ')';
		}
	}else{		
	//alert(frame+'/'+id);
		parent.frames[frames[frame]].document.getElementById(id).style.opacity = value/10;
		parent.frames[frames[frame]].document.getElementById(id).style.filter = 'alpha(opacity=' + value*10 + ')';
	}	
}


function util_alert(param){
	alert(param);
}

function util_amath(id) {
	oDiv = document.getElementById(id); 
	mathItems = oDiv.getElementsByTagName("latex"); 
	var str;
	var outnode;
	var n;
	var j;
	for(var i = 0; i < mathItems.length; i++){
		tex=mathItems[i].innerHTML;
		//alert(tex);
		
		//alert(tex+tex.indexOf('array'));
		if(tex.indexOf('array')!=-1){			
			nodeImg = document.createElement("img");
			nodeImg.src = wwwroot+'/filter/tex/view.php?tex='+base64_encode(tex);
			outnode = mathItems[i];
			n = outnode.childNodes.length;		
			for (j=0; j<n; j++)
				outnode.removeChild(outnode.firstChild);		
			outnode.appendChild(nodeImg);
		}else{			
			str = 'amath   `'+mathItems[i].innerHTML+'`';
			str=str.replace(/\\mbox/gi, "\\text");
			str=str.replace(/\\triangle/gi, "\\Delta");
			str=str.replace(/\\mp/g, "\\ {::}_(\\+)^\\-\\ ");
			str=str.replace(/Julio/gi, "\"Julio\"");
			str=str.replace(/&gt;/g, ">");
			str=str.replace(/&lt;/g, "<");
			outnode = mathItems[i];
			n = outnode.childNodes.length;		
			for (j=0; j<n; j++)
				outnode.removeChild(outnode.firstChild);		
			outnode.appendChild(document.createTextNode(str));
			AMprocessNode(outnode);		
		}

	}
}

function util_new_window(title, content)
{
  var save = window.open(' ', title);  
  save.document.write('<html>');
  save.document.write('<head>');  
  save.document.write('<title>'+title+'</title>');
  save.document.write('</head>');
  save.document.write('<body>');
  save.document.write(content);
  save.document.write('</body>');
  save.document.write('</html>');
}

function util_show_row_options(id){	
	document.getElementById(id).style.display='inline';
}

function util_hide_row_options(id){	
	document.getElementById(id).style.display='none';
}

function util_show_hidde(option, alloptions, frame){	
	var brokenstring=alloptions.split(","); 
	var i;
	for(i=0;i<brokenstring.length;i++){		
		if(parent.frames[frames[frame]].document.getElementById(brokenstring[i]))
			parent.frames[frames[frame]].document.getElementById(brokenstring[i]).style.display='none';
	} 
	parent.frames[frames[frame]].document.getElementById(option).style.display='block';
	parent.frames[frames[frame]].scroll(0,0);
}

function util_show_hidde_effect(id, effect){
    
    /* Freddy Rojas 07.07.2014
     * Verificar qué tipo de bloques son los que aparecen */
        console.log("util_show_hidde_effect id"+document.getElementById(id).style.display);
        
	if(document.getElementById(id).style.display=='none'||document.getElementById(id).style.display==''){
		document.getElementById(id).style.display='block';	
	}else
		document.getElementById(id).style.display='none';
	return false;
}

function util_show_hidde_dialog(id, effect){	
	if(document.getElementById("active_show_hidde_"+id).className=='link_hidde'){		
		document.getElementById(id).style.display='none';	
		document.getElementById("active_show_hidde_"+id).className='link_show';
		/* Freddy Rojas 2014.01.23
                 * Se cambian los textos de mostrar/ocultar al usuario para que
                 * queden en español
                 */
                document.getElementById("active_show_hidde_"+id).innerHTML='Mostrar';
	}else{
		document.getElementById(id).style.display='block';	
		document.getElementById("active_show_hidde_"+id).className='link_hidde';		
		document.getElementById("active_show_hidde_"+id).innerHTML='Ocultar';
	}
	return false;
}


function util_show_hidden(option, alloptions, frame){	
	var brokenstring=alloptions.split(","); 
	var i;
	for(i=0;i<brokenstring.length;i++){		
		if(document.getElementById(brokenstring[i]))
			document.getElementById(brokenstring[i]).style.display='none';
	} 
	document.getElementById(option).style.display='block';
	scroll(0,0);
}


function util_get_absolute_element_position(element) {
  if (typeof element == "string")
    element = document.getElementById(element)
    
  if (!element) return { top:0,left:0 };
  
  var y = 0;
  var x = 0;
  while (element.offsetParent) {
	x += element.offsetLeft;
    y += element.offsetTop;
    element = element.offsetParent;
  }
  return {top:y,left:x};
}

function alerta(param){
	alert(param);
	}

function util_valid_TEXT(data){	
	data = data.replace(/\�/g, "&aacute;");
	data = data.replace(/\�/g, "&eacute;");
	data = data.replace(/\�/g, "&iacute;");
	data = data.replace(/\�/g, "&oacute;");
	data = data.replace(/\�/g, "&uacute;");
	data = data.replace(/\�/g, "&Aacute;");
	data = data.replace(/\�/g, "&Eacute;");
	data = data.replace(/\�/g, "&Iacute;");	
	data = data.replace(/\�/g, "&Oacute;");
	data = data.replace(/\�/g, "&Uacute;");
	data = data.replace(/\�/g, "&ntilde;");	
	data = data.replace(/\�/g, "&Ntilde;");
	
	return data;
}

function util_valid_XHTML(data){
    //alert(data);
	data = data.replace(/\&lt;/gi, "*lt;");
	data = data.replace(/\&gt;/gi, "*gt;");
	data = data.replace(/\[/gi, "&cl;");
	data = data.replace(/\]/gi, "&cr;");						   
	data = data.replace( /&/g, '&amp;' ) ;
	data = data.replace(/(<br[^>]*>)/gi, "[br/]");
	data = data.replace(/(<hr \/[^>]*>)/gi, "[hr/]");	
	data = data.replace(/<(img [^>]*)>/gi, "[$1/]");
	
	data = data.replace(/(<embed)/gi, "[embed");    
	data = data.replace(/(<\/embed[^>]*>)/gi, "[/embed]");
	
	data = data.replace(/(<object[^>]*>)/gi, "");
	data = data.replace(/(<\/object[^>]*>)/gi, "");
	//data = data.replace(/(<object)/gi, "[object");
	//data = data.replace(/(<\/object[^>]*>)/gi, "[/object]");
	
	data = data.replace(/(<param[^>]*>)/gi, "");
	data = data.replace(/(<\/param[^>]*>)/gi, "");
	//data = data.replace(/(<param)/gi, "[param");
	//data = data.replace(/(<\/param[^>]*>)/gi, "[/param]");
        
        data = data.replace(/(<style[^>]*>)/gi, "[style]");
        data = data.replace(/(<\/style[^>]*>)/gi, "[/style]");

        data = data.replace(/(<cite[^>]*>)/gi, "[cite]");
        data = data.replace(/(<\/cite[^>]*>)/gi, "[/cite]"); 

        data = data.replace(/(<blockquote[^>]*>)/gi, "[blockquote]");
        data = data.replace(/(<\/blockquote[^>]*>)/gi, "[/blockquote]");        
        
        data = data.replace(/(<hr[^>]*>)/gi, "[hr]");
	//data = data.replace(/<(a class=\"[^]*)/gi, "[$1");
	data = data.replace(/<(a)/gi, "[a");
	data = data.replace(/(<\/a[^>]*>)/gi, "[/a]");									   
	
	data = data.replace(/(<p)/gi, "[p");
	data = data.replace(/<(span style=\"background[^>]*)>/gi, "[$1]");
	data = data.replace(/<(span style=\"font-size[^>]*)>/gi, "[$1]");
	data = data.replace(/<(span style=\"line-height[^>]*)>/gi, "[$1]");
	data = data.replace(/<(span style=\"font-family[^>]*)>/gi, "[$1]");
	data = data.replace(/<(span style=\"color[^>]*)>/gi, "[$1]");
	data = data.replace(/<(span id=\"[^>]*)>/gi, "[$1]");
        data = data.replace(/<(span class=\"[^>]*)>/gi, "[$1]");
        
	data = data.replace(/<(video [^>]*)>/gi, "[$1]");
	data = data.replace(/(<video[^>]*>)/gi, "[video]");	
        data = data.replace(/(<\/video[^>]*>)/gi, "[/video]");
        
        data = data.replace(/<(source [^>]*)>/gi, "[$1]");
	data = data.replace(/(<source[^>]*>)/gi, "[source]");
        data = data.replace(/(<\/source[^>]*>)/gi, "[/source]");
	
        data = data.replace(/(<span[^>]*>)/gi, "[span]");	
	data = data.replace(/(<strong[^>]*>)/gi, "[strong]");
	
	data = data.replace(/(<h2[^>]*>)/gi, "[h2]");
	data = data.replace(/(<h3[^>]*>)/gi, "[h3]");
	data = data.replace(/(<h4[^>]*>)/gi, "[h4]");
	data = data.replace(/(<h5[^>]*>)/gi, "[h5]");
	data = data.replace(/(<h6[^>]*>)/gi, "[h6]");
	
	data = data.replace(/(<ol)/gi, "[ol");
	data = data.replace(/(<\/ol[^>]*>)/gi, "[/ol]");
	data = data.replace(/(<ul)/gi, "[ul");
	data = data.replace(/(<\/ul[^>]*>)/gi, "[/ul]");
	data = data.replace(/(<li)/gi, "[li");
	data = data.replace(/(<\/li[^>]*>)/gi, "[/li]");	
	
	
	data = data.replace(/(<\/p[^>]*>)/gi, "[/p]");
	data = data.replace(/(<\/span[^>]*>)/gi, "[/span]");
	data = data.replace(/(<\/strong[^>]*>)/gi, "[/strong]");		
	data = data.replace(/(<\/h2[^>]*>)/gi, "[/h2]");
	data = data.replace(/(<\/h3[^>]*>)/gi, "[/h3]");
	data = data.replace(/(<\/h4[^>]*>)/gi, "[/h4]");
	data = data.replace(/(<\/h5[^>]*>)/gi, "[/h5]");
	data = data.replace(/(<\/h6[^>]*>)/gi, "[/h6]");
	
	data = data.replace(/(<u>)/gi, "[u]");
	data = data.replace(/(<\/u>)/gi, "[/u]");
	data = data.replace(/(<strike[^>]*>)/gi, "[strike]");
	data = data.replace(/(<\/strike[^>]*>)/gi, "[/strike]");
	data = data.replace(/(<em>)/gi, "[em]");
	data = data.replace(/(<\/em>)/gi, "[/em]");	
	
	//TABLE
	data = data.replace(/(<table)/gi, "[table");
	data = data.replace(/(<tbody)/gi, "[tbody");
	data = data.replace(/(<tr)/gi, "[tr");
	data = data.replace(/(<td)/gi, "[td");
	data = data.replace(/(<th)/gi, "[th");
	
	data = data.replace(/(<\/table[^>]*>)/gi, "[/table]");
	data = data.replace(/(<\/tbody[^>]*>)/gi, "[/tbody]");
	data = data.replace(/(<\/tr[^>]*>)/gi, "[/tr]");
	data = data.replace(/(<\/td[^>]*>)/gi, "[/td]");
	data = data.replace(/(<\/th[^>]*>)/gi, "[/th]");
	
	data = data.replace(/(<sup)/gi, "[sup");
	data = data.replace(/(<sub)/gi, "[sub");
	data = data.replace(/(<\/sup[^>]*>)/gi, "[/sup]");
	data = data.replace(/(<\/sub[^>]*>)/gi, "[/sub]");
	
	data = data.replace(/(<iframe)/gi, "[iframe");
	data = data.replace(/(<\/iframe[^>]*>)/gi, "[/iframe]");
	
	//data = data.replace(/(<style [^>]*>[^<]*<\/style[^>]*>)/gi, "");
	data = data.replace(/<(div[^>]*)>/gi, "[$1]");
	data = data.replace(/(<\/div[^>]*>)/gi, "[/div]");
	data = data.replace(/(<[^>]*>)/gi, "");
	
	data = data.replace(/\[/gi, "<");
	data = data.replace(/\]/gi, ">");
	data = data.replace(/(&cl;)/gi, "[");
	data = data.replace(/(&cr;)/gi, "]");	
	data = util_separe(data);
	
	data = data.replace(/(<br[^>]*>)/gi, "<br/>");
	data = data.replace(/(<hr[^>]*>)/gi, "<hr/>");	
	data = data.replace(/<(img [^>]*)>/gi, "<$1/>");	
	data = data.replace(/\*lt;/gi, "&lt;");
	data = data.replace(/\*gt;/gi, "&gt;");
	return data;
}

function util_str_escape(str){
	str = escape(str);
	str = str.replace(/\+/g,"%2B");
	return str;
}


function util_separe(str){

	var output = '';
	var i = 0;
	
	str = str.replace(/<(DIV[^>]*)>/gi, "[$1]");
	str = str.replace(/(<\/DIV[^>]*>)/gi, "[/div]");	
	str = '<div>'+str+'</div>';
	str = str.replace(/(<h[^>]*>)/gi,"</div><div>$1");
	
	var target = document.createElement("body");
	
	target.innerHTML = str;
	divItems = target.getElementsByTagName("div"); 
	
	for(i = 0; i < divItems.length; i++){
		var ObjDOM = document.createElement("div");
		ObjDOM.innerHTML=divItems[i].innerHTML;		
		output+=getInnerHTML(ObjDOM);			
	}
	
	output = output.replace(/\[(DIV[^\]]*)\]/gi, "<$1>");
	output = output.replace(/(\[\/DIV[^\]]*\])/gi, "</div>");
	
	return output;
}
/*
function getInnerHTML(data){
	var ObjDOM = document.createElement("div");
	ObjDOM.innerHTML=data;
	return ObjDOM.innerHTML;
}
*/
//Correction to Error in mozInnerHTML.js, Jan Ehlers Pedersen, November 18, 2004
function getInnerHTML(ObjDOM) {
	var str = "";
	for (var i=0; i<ObjDOM.childNodes.length; i++)
		str += getOuterHTML(ObjDOM.childNodes.item(i));
	return str;
}

function getOuterHTML(node) {

	var str = "";
	
	switch (node.nodeType) {
		case 1: // ELEMENT_NODE
			str += "<" + node.nodeName;
			for (var i=0; i<node.attributes.length; i++) {
				if (node.attributes.item(i).nodeValue != null) {
					str += " ";
					str += node.attributes.item(i).nodeName;
					str += "=\"";
					str += node.attributes.item(i).nodeValue;
					str += "\"";
				}
			}

			if (node.childNodes.length == 0 && leafElems[node.nodeName])
				str += ">";
			else {
				str += ">";
				str += getInnerHTML(node);
				str += "</" + node.nodeName + ">"
			}
			break;
				
		case 3:	//TEXT_NODE
			str += node.nodeValue;
			break;
			
		case 4: // CDATA_SECTION_NODE
			str += "<![CDATA[" + node.nodeValue + "]]>";
			break;
					
		case 5: // ENTITY_REFERENCE_NODE
			str += "&" + node.nodeName + ";"
			break;

		case 8: // COMMENT_NODE
			str += "<!--" + node.nodeValue + "-->"
			break;
	}

	return str;
}
var _leafElems = ["IMG", "HR", "BR", "INPUT"];
var leafElems = {};
for (var i=0; i<_leafElems.length; i++)
leafElems[_leafElems[i]] = true;

//-------- PHP.js
function htmlentities( s ){
    // Convert all applicable characters to HTML entities
    // 
    // +    discuss at: http://kevin.vanzonneveld.net/techblog/article/javascript_equivalent_for_phps_htmlentities/
    // +       version: 804.1712
    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // *     example 1: htmlentities('Kevin & van Zonneveld');
    // *     returns 1: 'Kevin &amp; van Zonneveld'

    var div = document.createElement('div');
	div.innerHTML=s;
	//alert(div.innerHTML);
    return div.innerHTML;
}

// {{{ utf8_encode
function utf8_encode ( str_data ) {
    // Encodes an ISO-8859-1 string to UTF-8
    // 
    // +    discuss at: http://kevin.vanzonneveld.net/techblog/article/javascript_equivalent_for_phps_utf8_encode/
    // +       version: 805.821
    // +   original by: Webtoolkit.info (http://www.webtoolkit.info/)
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)        
    // *     example 1: utf8_encode('Kevin van Zonneveld');
    // *     returns 1: 'Kevin van Zonneveld'

    str_data = str_data.replace(/\r\n/g,"\n");
    var tmp_arr = [], ac = 0;

    for (var n = 0; n < str_data.length; n++) {
        var c = str_data.charCodeAt(n);
        if (c < 128) {
            tmp_arr[ac++] = String.fromCharCode(c);
        } else if((c > 127) && (c < 2048)) {
            tmp_arr[ac++] = String.fromCharCode((c >> 6) | 192);
            tmp_arr[ac++] = String.fromCharCode((c & 63) | 128);
        } else {
            tmp_arr[ac++] = String.fromCharCode((c >> 12) | 224);
            tmp_arr[ac++] = String.fromCharCode(((c >> 6) & 63) | 128);
            tmp_arr[ac++] = String.fromCharCode((c & 63) | 128);
        }
    }
    
    return tmp_arr.join('');
}// }}}

function base64_encode( data ) {
    // Encodes data with MIME base64
    // 
    // +    discuss at: http://kevin.vanzonneveld.net/techblog/article/javascript_equivalent_for_phps_base64_encode/
    // +       version: 805.821
    // +   original by: Tyler Akins (http://rumkin.com)
    // +   improved by: Bayron Guevara
    // +   improved by: Thunder.m
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)        
    // -    depends on: utf8_encode
    // *     example 1: base64_encode('Kevin van Zonneveld');
    // *     returns 1: 'S2V2aW4gdmFuIFpvbm5ldmVsZA=='

    // mozilla has this native
    // - but breaks in 2.0.0.12!
    //if (typeof window['atob'] == 'function') {
    //    return atob(data);
    //}
        
    var b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    var o1, o2, o3, h1, h2, h3, h4, bits, i = ac = 0, enc="", tmp_arr = [];
    data = utf8_encode(data);
    
    do { // pack three octets into four hexets
        o1 = data.charCodeAt(i++);
        o2 = data.charCodeAt(i++);
        o3 = data.charCodeAt(i++);

        bits = o1<<16 | o2<<8 | o3;

        h1 = bits>>18 & 0x3f;
        h2 = bits>>12 & 0x3f;
        h3 = bits>>6 & 0x3f;
        h4 = bits & 0x3f;

        // use hexets to index into b64, and append result to encoded string
        tmp_arr[ac++] = b64.charAt(h1) + b64.charAt(h2) + b64.charAt(h3) + b64.charAt(h4);
    } while (i < data.length);
    
    enc = tmp_arr.join('');
    
    switch( data.length % 3 ){
        case 1:
            enc = enc.slice(0, -2) + '==';
        break;
        case 2:
            enc = enc.slice(0, -1) + '=';
        break;
    }

    return enc;
}

function utf8_decode ( str_data ) {
    // Converts a string with ISO-8859-1 characters encoded with UTF-8   to single-byte
    // ISO-8859-1
    // 
    // +    discuss at: http://kevin.vanzonneveld.net/techblog/article/javascript_equivalent_for_phps_utf8_decode/
    // +       version: 805.821
    // +   original by: Webtoolkit.info (http://www.webtoolkit.info/)
    // +      input by: Aman Gupta
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // *     example 1: utf8_decode('Kevin van Zonneveld');
    // *     returns 1: 'Kevin van Zonneveld'

    var tmp_arr = [], i = ac = c = c1 = c2 = 0;

    while ( i < str_data.length ) {
        c = str_data.charCodeAt(i);
        if (c < 128) {
            tmp_arr[ac++] = String.fromCharCode(c); 
            i++;
        } else if ((c > 191) && (c < 224)) {
            c2 = str_data.charCodeAt(i+1);
            tmp_arr[ac++] = String.fromCharCode(((c & 31) << 6) | (c2 & 63));
            i += 2;
        } else {
            c2 = str_data.charCodeAt(i+1);
            c3 = str_data.charCodeAt(i+2);
            tmp_arr[ac++] = String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
            i += 3;
        }
    }
    
    return tmp_arr.join('');
}


function base64_decode( data ) {
    // Decodes data encoded with MIME base64
    // 
    // +    discuss at: http://kevin.vanzonneveld.net/techblog/article/javascript_equivalent_for_phps_base64_decode/
    // +       version: 805.821
    // +   original by: Tyler Akins (http://rumkin.com)
    // +   improved by: Thunder.m
    // +      input by: Aman Gupta
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)    
    // -    depends on: utf8_decode
    // *     example 1: base64_decode('S2V2aW4gdmFuIFpvbm5ldmVsZA==');
    // *     returns 1: 'Kevin van Zonneveld'
    
    // mozilla has this native 
    // - but breaks in 2.0.0.12!
    //if (typeof window['btoa'] == 'function') {
    //    return btoa(data);
    //}
    
    var b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    var o1, o2, o3, h1, h2, h3, h4, bits, i = ac = 0, dec = "", tmp_arr = [];

    do {  // unpack four hexets into three octets using index points in b64
        h1 = b64.indexOf(data.charAt(i++));
        h2 = b64.indexOf(data.charAt(i++));
        h3 = b64.indexOf(data.charAt(i++));
        h4 = b64.indexOf(data.charAt(i++));

        bits = h1<<18 | h2<<12 | h3<<6 | h4;

        o1 = bits>>16 & 0xff;
        o2 = bits>>8 & 0xff;
        o3 = bits & 0xff;

        if (h3 == 64) {
            tmp_arr[ac++] = String.fromCharCode(o1);
        } else if (h4 == 64) {
            tmp_arr[ac++] = String.fromCharCode(o1, o2);
        } else {
            tmp_arr[ac++] = String.fromCharCode(o1, o2, o3);
        }
    } while (i < data.length);
    
    dec = tmp_arr.join('');
    dec = utf8_decode(dec);
    
    return dec;
}

// {{{ time
function time() {
    // Return current Unix timestamp
    // 
    // +    discuss at: http://kevin.vanzonneveld.net/techblog/article/javascript_equivalent_for_phps_time/
    // +       version: 807.1808
    // +   original by: GeekFG (http://geekfg.blogspot.com)
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // *     example 1: time();
    // *     returns 1: 1216363871
    
    var d = new Date();
    return Math.round(d.getTime()/1000);
}// }}}

// {{{ md5
function md5 ( str ) {
    // Calculate the md5 hash of a string
    // 
    // +    discuss at: http://kevin.vanzonneveld.net/techblog/article/javascript_equivalent_for_phps_md5/
    // +       version: 804.1712
    // +   original by: Webtoolkit.info (http://www.webtoolkit.info/)
    // + namespaced by: Michael White (http://crestidg.com)
    // -    depends on: utf8_encode
    // *     example 1: md5('Kevin van Zonneveld');
    // *     returns 1: '6e658d4bfcb59cc13f96c14450ac40b9'

    var RotateLeft = function(lValue, iShiftBits) {
            return (lValue<<iShiftBits) | (lValue>>>(32-iShiftBits));
        };

    var AddUnsigned = function(lX,lY) {
            var lX4,lY4,lX8,lY8,lResult;
            lX8 = (lX & 0x80000000);
            lY8 = (lY & 0x80000000);
            lX4 = (lX & 0x40000000);
            lY4 = (lY & 0x40000000);
            lResult = (lX & 0x3FFFFFFF)+(lY & 0x3FFFFFFF);
            if (lX4 & lY4) {
                return (lResult ^ 0x80000000 ^ lX8 ^ lY8);
            }
            if (lX4 | lY4) {
                if (lResult & 0x40000000) {
                    return (lResult ^ 0xC0000000 ^ lX8 ^ lY8);
                } else {
                    return (lResult ^ 0x40000000 ^ lX8 ^ lY8);
                }
            } else {
                return (lResult ^ lX8 ^ lY8);
            }
        };

    var F = function(x,y,z) { return (x & y) | ((~x) & z); };
    var G = function(x,y,z) { return (x & z) | (y & (~z)); };
    var H = function(x,y,z) { return (x ^ y ^ z); };
    var I = function(x,y,z) { return (y ^ (x | (~z))); };

    var FF = function(a,b,c,d,x,s,ac) {
            a = AddUnsigned(a, AddUnsigned(AddUnsigned(F(b, c, d), x), ac));
            return AddUnsigned(RotateLeft(a, s), b);
        };

    var GG = function(a,b,c,d,x,s,ac) {
            a = AddUnsigned(a, AddUnsigned(AddUnsigned(G(b, c, d), x), ac));
            return AddUnsigned(RotateLeft(a, s), b);
        };

    var HH = function(a,b,c,d,x,s,ac) {
            a = AddUnsigned(a, AddUnsigned(AddUnsigned(H(b, c, d), x), ac));
            return AddUnsigned(RotateLeft(a, s), b);
        };

    var II = function(a,b,c,d,x,s,ac) {
            a = AddUnsigned(a, AddUnsigned(AddUnsigned(I(b, c, d), x), ac));
            return AddUnsigned(RotateLeft(a, s), b);
        };

    var ConvertToWordArray = function(str) {
            var lWordCount;
            var lMessageLength = str.length;
            var lNumberOfWords_temp1=lMessageLength + 8;
            var lNumberOfWords_temp2=(lNumberOfWords_temp1-(lNumberOfWords_temp1 % 64))/64;
            var lNumberOfWords = (lNumberOfWords_temp2+1)*16;
            var lWordArray=Array(lNumberOfWords-1);
            var lBytePosition = 0;
            var lByteCount = 0;
            while ( lByteCount < lMessageLength ) {
                lWordCount = (lByteCount-(lByteCount % 4))/4;
                lBytePosition = (lByteCount % 4)*8;
                lWordArray[lWordCount] = (lWordArray[lWordCount] | (str.charCodeAt(lByteCount)<<lBytePosition));
                lByteCount++;
            }
            lWordCount = (lByteCount-(lByteCount % 4))/4;
            lBytePosition = (lByteCount % 4)*8;
            lWordArray[lWordCount] = lWordArray[lWordCount] | (0x80<<lBytePosition);
            lWordArray[lNumberOfWords-2] = lMessageLength<<3;
            lWordArray[lNumberOfWords-1] = lMessageLength>>>29;
            return lWordArray;
        };

    var WordToHex = function(lValue) {
            var WordToHexValue="",WordToHexValue_temp="",lByte,lCount;
            for (lCount = 0;lCount<=3;lCount++) {
                lByte = (lValue>>>(lCount*8)) & 255;
                WordToHexValue_temp = "0" + lByte.toString(16);
                WordToHexValue = WordToHexValue + WordToHexValue_temp.substr(WordToHexValue_temp.length-2,2);
            }
            return WordToHexValue;
        };

    var x=Array();
    var k,AA,BB,CC,DD,a,b,c,d;
    var S11=7, S12=12, S13=17, S14=22;
    var S21=5, S22=9 , S23=14, S24=20;
    var S31=4, S32=11, S33=16, S34=23;
    var S41=6, S42=10, S43=15, S44=21;

    str = utf8_encode(str);
    x = ConvertToWordArray(str);
    a = 0x67452301; b = 0xEFCDAB89; c = 0x98BADCFE; d = 0x10325476;

    for (k=0;k<x.length;k+=16) {
        AA=a; BB=b; CC=c; DD=d;
        a=FF(a,b,c,d,x[k+0], S11,0xD76AA478);
        d=FF(d,a,b,c,x[k+1], S12,0xE8C7B756);
        c=FF(c,d,a,b,x[k+2], S13,0x242070DB);
        b=FF(b,c,d,a,x[k+3], S14,0xC1BDCEEE);
        a=FF(a,b,c,d,x[k+4], S11,0xF57C0FAF);
        d=FF(d,a,b,c,x[k+5], S12,0x4787C62A);
        c=FF(c,d,a,b,x[k+6], S13,0xA8304613);
        b=FF(b,c,d,a,x[k+7], S14,0xFD469501);
        a=FF(a,b,c,d,x[k+8], S11,0x698098D8);
        d=FF(d,a,b,c,x[k+9], S12,0x8B44F7AF);
        c=FF(c,d,a,b,x[k+10],S13,0xFFFF5BB1);
        b=FF(b,c,d,a,x[k+11],S14,0x895CD7BE);
        a=FF(a,b,c,d,x[k+12],S11,0x6B901122);
        d=FF(d,a,b,c,x[k+13],S12,0xFD987193);
        c=FF(c,d,a,b,x[k+14],S13,0xA679438E);
        b=FF(b,c,d,a,x[k+15],S14,0x49B40821);
        a=GG(a,b,c,d,x[k+1], S21,0xF61E2562);
        d=GG(d,a,b,c,x[k+6], S22,0xC040B340);
        c=GG(c,d,a,b,x[k+11],S23,0x265E5A51);
        b=GG(b,c,d,a,x[k+0], S24,0xE9B6C7AA);
        a=GG(a,b,c,d,x[k+5], S21,0xD62F105D);
        d=GG(d,a,b,c,x[k+10],S22,0x2441453);
        c=GG(c,d,a,b,x[k+15],S23,0xD8A1E681);
        b=GG(b,c,d,a,x[k+4], S24,0xE7D3FBC8);
        a=GG(a,b,c,d,x[k+9], S21,0x21E1CDE6);
        d=GG(d,a,b,c,x[k+14],S22,0xC33707D6);
        c=GG(c,d,a,b,x[k+3], S23,0xF4D50D87);
        b=GG(b,c,d,a,x[k+8], S24,0x455A14ED);
        a=GG(a,b,c,d,x[k+13],S21,0xA9E3E905);
        d=GG(d,a,b,c,x[k+2], S22,0xFCEFA3F8);
        c=GG(c,d,a,b,x[k+7], S23,0x676F02D9);
        b=GG(b,c,d,a,x[k+12],S24,0x8D2A4C8A);
        a=HH(a,b,c,d,x[k+5], S31,0xFFFA3942);
        d=HH(d,a,b,c,x[k+8], S32,0x8771F681);
        c=HH(c,d,a,b,x[k+11],S33,0x6D9D6122);
        b=HH(b,c,d,a,x[k+14],S34,0xFDE5380C);
        a=HH(a,b,c,d,x[k+1], S31,0xA4BEEA44);
        d=HH(d,a,b,c,x[k+4], S32,0x4BDECFA9);
        c=HH(c,d,a,b,x[k+7], S33,0xF6BB4B60);
        b=HH(b,c,d,a,x[k+10],S34,0xBEBFBC70);
        a=HH(a,b,c,d,x[k+13],S31,0x289B7EC6);
        d=HH(d,a,b,c,x[k+0], S32,0xEAA127FA);
        c=HH(c,d,a,b,x[k+3], S33,0xD4EF3085);
        b=HH(b,c,d,a,x[k+6], S34,0x4881D05);
        a=HH(a,b,c,d,x[k+9], S31,0xD9D4D039);
        d=HH(d,a,b,c,x[k+12],S32,0xE6DB99E5);
        c=HH(c,d,a,b,x[k+15],S33,0x1FA27CF8);
        b=HH(b,c,d,a,x[k+2], S34,0xC4AC5665);
        a=II(a,b,c,d,x[k+0], S41,0xF4292244);
        d=II(d,a,b,c,x[k+7], S42,0x432AFF97);
        c=II(c,d,a,b,x[k+14],S43,0xAB9423A7);
        b=II(b,c,d,a,x[k+5], S44,0xFC93A039);
        a=II(a,b,c,d,x[k+12],S41,0x655B59C3);
        d=II(d,a,b,c,x[k+3], S42,0x8F0CCC92);
        c=II(c,d,a,b,x[k+10],S43,0xFFEFF47D);
        b=II(b,c,d,a,x[k+1], S44,0x85845DD1);
        a=II(a,b,c,d,x[k+8], S41,0x6FA87E4F);
        d=II(d,a,b,c,x[k+15],S42,0xFE2CE6E0);
        c=II(c,d,a,b,x[k+6], S43,0xA3014314);
        b=II(b,c,d,a,x[k+13],S44,0x4E0811A1);
        a=II(a,b,c,d,x[k+4], S41,0xF7537E82);
        d=II(d,a,b,c,x[k+11],S42,0xBD3AF235);
        c=II(c,d,a,b,x[k+2], S43,0x2AD7D2BB);
        b=II(b,c,d,a,x[k+9], S44,0xEB86D391);
        a=AddUnsigned(a,AA);
        b=AddUnsigned(b,BB);
        c=AddUnsigned(c,CC);
        d=AddUnsigned(d,DD);
    }

    var temp = WordToHex(a)+WordToHex(b)+WordToHex(c)+WordToHex(d);

    return temp.toLowerCase();
}// }}}


function util_js_params_decode(param){
	return base64_decode(param);
}

function getAjax(){
  if(window.XMLHttpRequest){
    return new XMLHttpRequest();
  }else{
    return new ActiveXObject("Microsoft.XMLHTTP");
  }
}
function getBrowserInfo(){
  //Tomado de http://www.javascripter.net/faq/browsern.htm
  var nVer = navigator.appVersion;
  var nAgt = navigator.userAgent;
  var browserName  = navigator.appName;
  var fullVersion  = ''+parseFloat(navigator.appVersion); 
  var majorVersion = parseInt(navigator.appVersion,10);
  var nameOffset,verOffset,ix;
  
  if((verOffset=nAgt.indexOf('Opera')) != -1){//In Opera, the true version is after 'Opera' or after 'Version'
    browserName = 'Opera';
    fullVersion = nAgt.substring(verOffset+6);
    if((verOffset=nAgt.indexOf("Version")) != -1){
      fullVersion = nAgt.substring(verOffset+8);
    }
  }else if((verOffset=nAgt.indexOf('MSIE')) != -1){//In MSIE, the true version is after 'MSIE' in userAgent
    browserName = 'Microsoft Internet Explorer';
    fullVersion = nAgt.substring(verOffset+5);
  }else if ((verOffset=nAgt.indexOf("Chrome")) != -1){//In Chrome, the true version is after 'Chrome'
    browserName = 'Chrome';
    fullVersion = nAgt.substring(verOffset+7);
  }else if((verOffset=nAgt.indexOf('Safari')) != -1){//In Safari, the true version is after 'Safari' or after 'Version'
    browserName = 'Safari';
    fullVersion = nAgt.substring(verOffset+7);
    if((verOffset=nAgt.indexOf("Version")) != -1){
      fullVersion = nAgt.substring(verOffset+8);
    }
  }else if((verOffset=nAgt.indexOf('Firefox')) != -1){//In Firefox, the true version is after 'Firefox'
    browserName = 'Firefox';
    fullVersion = nAgt.substring(verOffset+8);
  }else if((nameOffset=nAgt.lastIndexOf(' ')+1) < (verOffset=nAgt.lastIndexOf('/'))){//In most other browsers, 'name/version' is at the end of userAgent
    browserName = nAgt.substring(nameOffset, verOffset);
    fullVersion = nAgt.substring(verOffset+1);
    if(browserName.toLowerCase() == browserName.toUpperCase()){
      browserName = navigator.appName;
    }
  }
  
  //Trim the fullVersion string at semicolon/space if present
  if((ix=fullVersion.indexOf(';')) != -1){
    fullVersion = fullVersion.substring(0, ix);
  }
  if((ix=fullVersion.indexOf(' ')) != -1){
    fullVersion = fullVersion.substring(0, ix);
  }
  
  majorVersion = parseInt(''+fullVersion, 10);
  if(isNaN(majorVersion)){
    fullVersion  = '' +parseFloat(navigator.appVersion); 
    majorVersion = parseInt(navigator.appVersion, 10);
  }
  return new Array(browserName, majorVersion, fullVersion);
}
function getHTMLButClass(obj, cls){
  html = '';
  oc = obj.childNodes;
  for(i=0; i<oc.length; i++){
    o = oc[i];
    if(o.className != cls){
      html += o.innerHTML;
    }
  }
  return html;
}
function getText(html){

  txt = html;
  txt = txt.replace(/(<(?:.|\n)*?>|&nbsp;)/gm, '.');
  txt = txt.replace(/[.]+/gi, '.');
  txt = txt.replace(/(^\.| \.)/gi, '');
  txt = txt.replace(/\.[^ ]/gi, '. ');
  return txt;
}
function util_listen_slide(lang, genre){
  txt = getText(getHTMLButClass(document.getElementById('content_display'), 'viewer_slide_navigation'));
  util_listen_text(txt, lang, genre);
}
function util_listen(num, lang, genre){
  util_listen_id('voice' +num, lang, genre);
}
function util_listen_id(id, lang, genre){
  obj = document.getElementById(id);
  util_listen_object(obj, lang, genre);
}
function util_listen_object(obj, lang, genre){
  txt = getText(obj.innerHTML);
  util_listen_text(txt, lang, genre);
}
function util_listen_text(text, lang, genre){
  genre = (genre=='f'||genre=='F' ? 1 : 0);
  par = 'uid=' +parent.luid+ '&idi=' +escape(lang)+ '&vel=170&ton=10&ora=' +escape(genre)+ '&del=0&txt=' +escape(text)+ '';
  util_listen_params(null, par);
}
function util_listen_params(where, params){
  srv = '../tts/';
  fil = 'index.php';
  fol = 'files/';
  met = 'POST';
  createFile(srv, fil, fol, met, params, where);
}
function util_get_sound_filename(parameters){
  server = '../tts/';
  file = 'index.php';
  folder = 'files/';
  method = 'POST';
  ajax = getAjax();
  if(method == 'POST'){
    ajax.open('POST', server+file, false);
    ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  }else{
    ajax.open('GET', server+file+ '?' +parameters, false);
  }
  ajax.send(method=='POST'?parameters:null);
  s = ajax.responseText;
  x = s.indexOf('.Debug.');
  debug = (x<0 ? '' : s.substring(x+7));
  return wwwupload+ '/tts/' +(x<0?s:s.substring(0,x));
}
function createFile(server, file, folder, method, parameters, where){
  ajax = getAjax();
  if(method == 'POST'){
    ajax.open('POST', server+file, true);
    ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  }else{
    ajax.open('GET', server+file+ '?' +parameters, true);
  }
  ajax.onreadystatechange = function(){
    if(ajax.readyState==4 && ajax.status==200){
      s = ajax.responseText;
      x = s.indexOf('.Debug.');
      debug = (x<0 ? '' : s.substring(x+7));
      s = (x<0 ? s : s.substring(0, x));
      sExl = s;
      util_play_file(sExl, where);
    }
  }
  ajax.send(method=='POST'?parameters:null);
}
function util_play_file(file, where){
  if(file.indexOf('/') == -1){
    //file = wwwupload+ '/tts/' +file;
    
    //Se modifica esta ruta debido a problemas de configuración del servidor
     file = '../../' + datafolder + '/tts/' + file;
    
  }
  if(where!=null && where!=undefined && where.document){
    tts = where.document.getElementById('dPlayer');
  }else{
    tts = document.getElementById('dPlayer');
  }
  mp3file = false;
  if(file.lastIndexOf('.mp3') == file.length-4){
    mp3file = true;
  }
  if(!mp3file){
    sOgg = file+'.ogg';
    sMp3 = file+'.mp3';
  }else{
    sMp3 = file;
  }
  ttsi = '<style>.voice{width:300px;height:10px;}</style>';//'<style>.voice{position:absolute;left:-1000px;}</style>';
  nn = getBrowserInfo();
  nn = nn[0];
  try{
    if(!mp3file){
      o = document.createElement('audio');
      o.play();
      if(o.canPlayType('audio/ogg')){
        ttsi += '<audio class="voice" autoplay="true" controls="true" src="' +sOgg+ '" type="audio/ogg"></audio>';
      }else{
        ttsi += '<audio class="voice" autoplay="true" controls="true" src="' +sMp3+ '" type="audio/mp3"></audio>';
      }
    }else{
      throw 'Error';
    }
  }catch(e){
    if(nn.indexOf('Safari') < 0){
      ttsi += '<object class="voice" autostart="true" src="' +sMp3+ '">';
      ttsi += '<embed class="voice" autostart="true" src="' +sMp3+ '">';
      //ttsi += 'Su navegador no soporta audio';
      ttsi += '</embed>';
      ttsi += '</object>';
    }else{
      ttsi += '<embed class="voice" autostart="true" src="' +sMp3+ '" />';
    }
  }
  tts.innerHTML = ttsi+ '<a style="cursor:pointer;" onclick="parentElement.innerHTML=\'\';"><img src="' +wwwroot+ '/theme/nothing/images/close.png" /></a>';
}
function util_get_valid_xml_value(txt){
    //alert("util_get_valid_xml_value");
  if(txt == undefined){
    return '';
  }
  var cr = [
    '0-8,',
    '11-12,',
    '14-31,',
    '56320-57343,',
    '65534-65535,'
  ];
  for(var i=0; i<cr.length; i++){
    var r = cr[i].split('-');
    var n = cr[i].split(',')[0];
    var ini = Number(r[0]);
    var fin = Number(r[1]);
    for(var c=ini; c<=fin; c++){
      var re = new RegExp(String.fromCharCode(c), 'gi');
      txt = txt.replace(re, n);
    }
  }
  var amp = txt.indexOf('&');
  if(txt.charAt(0)=='\n' || txt.indexOf('<')!=-1 || amp!=-1){
    if(amp != -1){
      txt = txt.replace(/&amp;/gi,'*amp;').replace(/&/gi,'&amp;').replace(/\*amp;/gi,'&amp;');
    }
    txt = '<![CDATA[' +txt+ ']]>';
  }
  return txt;
}

/* Obtener el código de idioma, en caso tal de que vaya dado por
   la URL */
function getURLParam(nombre) {
    var listaParamsURL = window.location.search.substring(1);
    var params = listaParamsURL.split("&");

    for (var i=0;i<params.length;i++){
        var param = params[i].split("=");

        if (param[0] == nombre) {
            return param[1]; //Retorna el valor asociado al nombre
        }
    }                    
    return false;
}