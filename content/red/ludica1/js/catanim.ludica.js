function catanim_ludica(container, title, baseColor, instanceColor, content, logo, transition, avatar) {
    var that = this;
    
    this.borderRRel = 4;
    
    this.container = container;
    //this.id = "dialog_" + bg.toLowerCase();
    this.ready = false;
    this.playing = false;
    this.title = title;
    this.baseColor = baseColor;
    this.instanceColor = instanceColor;
    this.content = content;
    this.logo = logo;
    this.transition = transition;
    this.avatar = avatar;
    this.height = 0;
    this.virgin = true;
    
    this.loadedCallback = function() {};

    this.setTitle = function(aTitle) {
        this.title = aTitle;
    };

    this.setBaseColor = function(aColor) {
        this.baseColor = aColor;
    };

    this.setInstanceColor = function(aColor) {
        this.instanceColor = aColor;
    };

    this.setAvatar = function(aSrc) {
        this.avatar = aSrc !== '' ? aSrc : false;
    };

    this.setContent = function(aContent) {
        this.content = aContent;
    };

    this.setLogo = function(aLogo) {
        this.logo = aLogo;
    };    

    this.hover = function() {
        if (this.virgin) {
            var fullW = $(this.container).outerWidth(true);
            var w = $(this.container).width();
            var h = $(this.container).height();
            var hover = $(this.container).next('.hover');
            hover.css({
                'width': w,
                'height': h,
                'top': (fullW-w) / 2, 
                'left': (fullW-w) / 2
            });
            hover.animate({
                'opacity': 0,
                'width': fullW,
                'height': h + (fullW-w),
                'top': 0,
                'left': 0

            }, 500, function() {
                hover.remove();
            });
        }
        this.virgin = false;
    };

    this.setTransition = function(aTransition) {
        this.transition = aTransition;
    };    

    this.setImg = function(imgSrc) {
        $('#main-img').attr('src', imgSrc ).one('load', function() {
            mapa1.setAspectRatio(this.width / this.height);
        }).each( function() {
            if (this.complete) $(this).load();
        });
    };

    this.getWidth = function() {
        return $(this.container).width();
    };

    this.getHeight = function() {
        return $(this.container).height();
    };

    this.draw = function() {
        $(this.container).addClass( this.transition );
        $(this.container).append($('<div class="front"><div></div><h1>'+this.title+'</h1></div>'));
        $(this.container).append($('<div class="back"><h2>'+this.title+'</h2><div class="avatar"></div><section>'+this.content+'</section><footer></footer></div>'));

        $(this.container).find('.front').css({
            'background-color': that.instanceColor,
            'color': '#fff'
        })
        .find('div').css({
            'background-image': 'url('+that.logo+')'
        });

        $(this.container).find('.back').css({
            'background-color': that.baseColor
        })
        .find('h2').css({
            'background-image': 'url('+that.logo+')',
            'color': '#fff',
            'background-color': that.instanceColor
        })
        .end().find('.avatar').css({
            'background-image': (that.avatar !== '') ? 'url('+that.avatar+')' : 'none'
        })
        .end().find('section').css({
            'border-color': that.instanceColor
        })
        
        .find('strong').css({
            'color': that.instanceColor
        })
        .end().end().find('footer').css({
          'background-color': that.instanceColor  
        });

        $(this.container).after('<div class="hover"></div>');

        $(this.container).show();

        $(this.container).click( function(e) {
            $(this).toggleClass("flipped");
        })
        .hover( function(e) {
            that.hover();
        });



        $( function() {
            that.redraw();
        });


    };
    
    this.redraw = function() {

        if (that.avatar !== "") {
                var avatarRadius = that.getWidth() * 0.2;
                $(that.container).find('.avatar').css({
                    'height': avatarRadius
                });
            }

            that.height = Math.max( $(that.container).find('.front').height(), $(that.container).find('.back').height());

            $(that.container).height( that.height );
            $(that.container).find('.front').height( that.height );     

            cat_plugin.waitForHeight(that.height*1.2);

            // var h2h = $('.back section').prevAll('h2').outerHeight();
            // h2h += that.avatar ? $(that.container).find('.avatar').outerHeight(true) - 40 : 0; 


            // $('.back section').css({
            //     'top': h2h+'px'
            // });
        
    };
    
    $(window).resize( function() {
       that.redraw(); 
    });

    
    this.prepare = function(loadedCallback) {
        this.loadedCallback = loadedCallback;
        if (this.scenes.length > 0)
            this.scenes[this.pointer].load(that.sceneLoaded.bind(this));
    };
    
    this.play = function(limitByHeight) {
        this.playing = true;
        this.scenes[this.pointer]._scale(limitByHeight);
        this.sceneHandle().fadeIn(fadeSpeed);
        if (this.pointer + 1 == this.scenes.length) {
            $('#loading').hide();
        }
        
        this.scenes[this.pointer].play(that.completeScene.bind(this));

    };

    this.toggle = function() {

    };

    this.pause = function() {
    };

    this.stop = function() {
        
    };
}


