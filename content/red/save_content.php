<?php
session_start();
set_time_limit(120);
error_reporting(E_ERROR);
// require_once("../../backend/general.php");
require_once("common.php");
// $db = getDb();


// Leer parámetros
if (get_magic_quotes_gpc()) {
    $xml = stripslashes($_POST['xml']);
} else {
    $xml = $_POST['xml'];
}

$plugin_name = $_POST['plugin_name'];

$includes = $_POST['includes'];

$conceptMap = ($plugin_name == "conceptmap");

$now = date("Y-m-d H:i:s");

$w = "50";
$wunit = "%";

$op = "insert";

if ($xml != "" && !($conceptMap && ($_POST["src"] == ""))) {
    $op = "update";
}

if ($op == "update") {
    // Infografía existente
    $xmlParse = simplexml_load_string($xml);
    $xmlParse->metadata->lastModified = $now;
    $xmlParse->metadata->autor = $_SESSION['nombre'].' '.$_SESSION['apellido'];

    $w = $xmlParse->metadata->w;
    $wunit = $xmlParse->metadata->wunit;

    $dir2 = getPluginDir($_POST['src']);
    
    if ($plugin_name == "conceptmap") {
        $tmp = explode("-", $xmlParse->userdata->concepto->id);
        $plugin_id = strval($tmp[1]);
    } else {
        $plugin_id = strval($xmlParse->metadata->id);
    }
    
    $xml = $xmlParse->asXML();

    // quitar saltos de línea del XML, no los necesitamos y complican el output por Javascript
    $xml = str_replace(array("\n", "\r"), '', $xml); 
    // escapar comillas etc
    $xml = addslashes($xml);
    
} else {
    // Infografía nueva, insertamos un registro sin XML primero
    $query = array(
        "folder"=>$plugin_name,
        "data"=>"",
        "learningunit_id"=>$_SESSION['unit_id']
    );
    // $DB_CONNECTION->insert(TABLE_LEARNINGUNIT_PLUGIN_SETTINGS, $query);
    $plugin_id = $DB_CONNECTION->lastInsertId();
}

// GUARDAMOS METADATOS

$q = array();
$q['fk_autor_id'] = $_SESSION['user_id'];
$q['title'] = $xmlParse->metadata->title;
$q['description'] = $xmlParse->metadata->description;
$q['fk_resource_type'] = FIELD_LEARNINGUNIT_RESOURCE_TYPES_INFOGRAFIAHTML5;
$q['fk_resource_id'] = $plugin_id;


if ($op == "update") {
    $q['last_modified'] = $now;
    /*$DB_CONNECTION->update(
            TABLE_LEARNINGUNIT_RESOURCE_METADATA, 
            $q, 
            'fk_resource_id = '.$plugin_id.' AND fk_resource_type = '.FIELD_LEARNINGUNIT_RESOURCE_TYPES_INFOGRAFIAHTML5);*/
} else {
    $q['created'] = $now;
    /*$DB_CONNECTION->insert(TABLE_LEARNINGUNIT_RESOURCE_METADATA, $q);*/
}

// GUARDAMOS DATOS GENERALES

if ($op == "update") {
    /*$DB_CONNECTION->update(TABLE_LEARNINGUNIT_PLUGIN_SETTINGS, array("data" => $xml), "id = ".$plugin_id);*/
} else { 
    
    // Crear nombre de carpeta destino único
    $plugin_name_unique = $plugin_name."-".$plugin_id;
    $dir2 = mkSessionDir($_PATH->sys->datafolder)."/".$plugin_name_unique;
    
    $appFolder = substr($_PATH->sys->path,strrpos($_PATH->sys->path,"/"));
    // Copiar plugin al destino 
    recurse_copy($_SERVER['DOCUMENT_ROOT'] . $appFolder."/plugins_html/" . $plugin_name, $dir2);
//    $xmlFile = fopen($dir2."/userdata.xml","r");
    
    $xmlMetadata = '<metadata><created>'.$now.'</created><lastmodified></lastmodified><id>'.$plugin_id.'</id>';
    $xmlMetadata .= '<description></description><autor>'.$_SESSION['nombre'].' '.$_SESSION['apellido'].'</autor><title></title><w>'.$w.'</w><wunit>'.$wunit.'</wunit></metadata>';
    
    
    if ($conceptMap) {
        $xmlParse = simplexml_load_string($xml);
    } elseif (!($xmlParse = simplexml_load_file($dir2."/userdata.xml"))) {
        // Crear un XML base si no existe
        $xmlParse = simplexml_load_string('<?xml version="1.0" encoding="UTF-8"?><root></root>');
    }
    
    if (!$xmlParse->metadata) {
        $xmlParse->addChild('metadata');
    }
    
    // Manipulaciones para insertar el bloque de metadatos en el XML
    $xmlDoc = dom_import_simplexml($xmlParse)->ownerDocument;
    $metadataFragment = $xmlDoc->createDocumentFragment();
    $metadataFragment->appendXML($xmlMetadata);
    $oldMetadata = $xmlDoc->getElementsByTagName("metadata")->item(0);
    $xmlDoc->documentElement->replaceChild($metadataFragment, $oldMetadata);
    
    $xml = $xmlDoc->saveXML();
    
    // Traducir contenidos al idioma actual
    $localLang = new Local_Lang();

    if (isset($_REQUEST['lang'])) {
        $localLang->setLangCode($_REQUEST['lang']);
    }
    
    $translatables = array("/index.html","/js/main.js");
    
    foreach ($translatables as $f) {
        $contents = file_get_contents($dir2.$f);
        if ($contents) {
            // Reemplazar tags estilo Mustache con contenido traducido
            $replaced = preg_replace_callback("/{{(.*?)}}/", 
                    function($matches, $localLang) use($localLang) { 
                        return $localLang->getTranslation( trim($matches[1], '{}' ) );
                    }, $contents);
            $translated = fopen($dir2.$f,"w");
            fwrite($translated, $replaced);
            fclose($translated);
        }
    }
    
    $query = array(
        "data"=>$xml
    );
    // $DB_CONNECTION->update(TABLE_LEARNINGUNIT_PLUGIN_SETTINGS, $query, "id = ".$plugin_id);
}

// GUARDAMOS RELACION DE INDICADORES

if ($op == "update") {
    // $DB_CONNECTION->delete(TABLE_LEARNINGUNIT_RESOURCE_INDICATORS, 'resource_id = '.$plugin_id.' AND fk_resource_types_id = '.FIELD_LEARNINGUNIT_RESOURCE_TYPES_INFOGRAFIAHTML5);
    foreach ($xmlParse->metadata->indicadores->children() as $indicador) {
        $q = array();
        $q['fk_au_indicadores_id'] = $indicador->id;
        $q['fk_resource_types_id'] = FIELD_LEARNINGUNIT_RESOURCE_TYPES_INFOGRAFIAHTML5;
        $q['resource_id'] = $plugin_id;
        // $DB_CONNECTION->insert(TABLE_LEARNINGUNIT_RESOURCE_INDICATORS, $q);
    }
}
    
// Escribir nuevos contenidos (si es que los hay) al XML destino
if ($xml != "") {
    // Abrir en modo "w": escritura, si el archivo no existe, se crea automaticamente.
    $xmlFile = fopen($dir2."/userdata.xml","w");
    // JS para duplicar el XML para casos donde no se puede leer XML (Google Chrome en modo file://, de momento)
    $xmlFileJs = fopen($dir2."/userdata.js","w");
    fwrite($xmlFile, stripslashes($xml));
    fwrite($xmlFileJs, "var xmlFix = '".str_replace(array("\r", "\t", "\n"),array("", "", ""), $xml)."'; addContent((new DOMParser()).parseFromString(xmlFix,'text/xml'));");
    fclose($xmlFile);
    fclose($xmlFileJs);
}

$extras = array();
//echo("includes: ".$includes."<br/>");

$q = "";
$p = "";
$s = "all";
$edge_i = 0;

// Escribir contenidos adicionales, si es que los hay, a la carpeta destino
if ($includes != "") {
    $extras = explode(',', $includes);

    for ($i = 0; $i < count($extras); $i++) {
        $file = urldecode($extras[$i]);
        
        if (substr($file, strlen($file) -1) == "/") {
            // Caso especial: es una animacion Edge de nuestro repositorio
            if (strpos($file,'_common/edge_animations/') !== false) {
                $edge_i++;
                recurse_copy($_SERVER['DOCUMENT_ROOT'] . "/". $file, $dir2."/files/edge_".$edge_i);
                foreach (glob($dir2."/files/edge_".$edge_i."/*_edgePreload.js") as $filename) {
                    $txt = file_get_contents($filename);
                    $txt = preg_replace('/load: "(.*?)_edge.js"/', 'load: "files/edge_'.$edge_i.'/$1_edge.js"', $txt);
                    $txt = preg_replace('/load: "(.*?)_edgeActions.js"/', 'load: "files/edge_'.$edge_i.'/$1_edgeActions.js"', $txt);
                    
                    $file = fopen($filename, "w");
                    fwrite($file, $txt);
                    fclose($file);
                }
                
                foreach (glob($dir2."/files/edge_".$edge_i."/*_edge.js") as $filename) {
                    $txt = file_get_contents($filename);
                    
                    $txt = preg_replace('/files\/(.*?)\/images/', 'files/edge_'.$edge_i.'/images', $txt);
//                    $txt = preg_replace('/load: "(.*?)_edgeActions.js"/', 'load: "files/edge_'.$edge_i.'/$1_edgeActions.js"', $txt);
                    
                    $file = fopen($filename, "w");
                    fwrite($file, $txt);
                    fclose($file);
                }
                
            // otro tipo de carpeta:
            } else {
                recurse_copy($_SERVER['DOCUMENT_ROOT'] . "/". $file, $dir2."/files");
            }
        } else {
            $s = copy($_SERVER['DOCUMENT_ROOT'] . "/" . $file, $dir2."/files".substr($file, strrpos($file,"/")));
            $q = $dir2."/files".substr($file, strrpos($file,"/"));
            $p = $_SERVER['DOCUMENT_ROOT'] . "/" . $file;
        }
    }
}

// Guardar resultado
$src = str_replace($_SERVER['DOCUMENT_ROOT'], "", $dir2)."/index.html";
// echo("http://localhost/learningunit/plugins_html/conceptmap/");


$result = array("src" => $src, "id" => $plugin_id,"extras" => count($extras), "op" => $op, "w" => $w.$wunit );
header('Content-Type: application/json');
echo json_encode($result);
?>
