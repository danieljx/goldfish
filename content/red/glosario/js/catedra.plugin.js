
function catedra_plugin() {
    var that = this;
    var timeStamp = new Date().getTime();
    this.id = "";
    this.plugin = false;
    this.ready = false;
    this.iframeId = false;
    
    this.setReady = function() {
        this.ready = true;
        console.log("#### PLUGIN READY ####");
        this.play();
    };
    
    this.loadedCallback = function() {};
    
    this.setParameters = function(plugin, idPrefix) {
        console.log("setting parameters..");
        this.plugin = plugin;
        this.id = idPrefix + "_" + timeStamp + "_" + Math.floor(Math.random()*1000);
    };
    
    this.play = function() {
        console.log("*** catplugin play");
        if (this.plugin && this.ready)
            this.plugin.play();
    };

    this.growFrameH = function(newH) {
        try {
            if (window.parent && that.iframeId) {
                window.parent.postMessage(JSON.stringify({msg:'growFrameH',id: that.iframeId, height: newH}),'*'); 
            }
        } catch(e) {

        }
    };

    this.waitForHeight = function(h) {
        console.log("waitForHeight");
        console.log(h);
        if (typeof h === 'undefined') {
            h = $('body > div').height();
        }
        console.log(h);
        
        if (h > 0 && that.iframeId) {
            that.growFrameH(h);
        } else {
            setTimeout(function() { that.waitForHeight(h); }, 500);
        }
    };

    this.receiveMessage = function (event) {
        console.log("msg received");
        var id = event.data.split("I am your father,")[1];
        that.iframeId = id;
    };

}

var cat_plugin = new catedra_plugin();

window.addEventListener("message", cat_plugin.receiveMessage, false);


