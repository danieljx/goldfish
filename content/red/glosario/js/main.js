//Interacción de REDs con LMS
// Tipos de recursos educativos digitales (RED)
var FIELD_LEARNINGUNIT_RESOURCE_TYPES_INFOGRAFIAHTML5 = 4;
var reporte = 0;
var autoplay = false;
var soundInstance = false;

function setInteractionReporting(report){
            //Envío de datos de interacción al LMS
        try {
                if(!report && (window.frameElement.getAttribute('data-red-id') != null) ){
                    //Ejecutar función de interfaz con el API de SCORM (incluida cuando se ha exportado la unidad, ver SCO.js)            
                    //Al hacer el reporte no aplican puntuaciones (se dejan en cero)            
                    window.parent.report_red_to_lms(window.frameElement.getAttribute('data-red-id'), FIELD_LEARNINGUNIT_RESOURCE_TYPES_INFOGRAFIAHTML5, 0, 0, 0);

                    report = 1;
                }
            } catch(e) {

            }

        return report;
}

function addContent(xmlContents) {
    xml = $(xmlContents);
    
    autoplay = (xml.find('autoplay').text() == 'true');
    xml.find('letter').each( function(i) {
       $('<li id="letter-'+$(this).text()+'"><span>'+$(this).text()+'</span></li>').appendTo('#letters');
    });
    
    var letters = [];
    var cssAdd = '';

    // Mats Fjellner 20170811 Se ordenan los términos por orden alfabético
    var terms = xml.find('term').sort(function(a,b){
        return ($(a).find('title').text() > $(b).find('title').text() ? 1 : -1);
    });
    
    /* Santiago Peñuela 05/09/2014
     * Se agrega una expresión regular que modifique los espacios en blanco por "_" para evitar bug de no abrir divs con términos con espacios.
     */
     

    terms.each( function(i) {
        var letter = $(this).find('title').text().substring(0,1).toUpperCase();
        var term = $('<a href="#explanation-'+$(this).find('title').text().toLowerCase().replace(/\s+/g,"_")+'">'+$(this).find('title').text()+'</a>'); //Expresión Regular que cambia los espacios en blanco por "_"
        $('#terms-all').append(term);
        if ($.inArray(letter, letters) > -1) { // ya hay otros términos de esta letra
            $('#terms-'+letter).append(term.clone());
        } else {
            $('#letter-'+letter).html('<a href="#">'+letter+'</a>');
            $('<li id="terms-'+letter+'"></li>').append(term.clone()).appendTo('#terms');
        }
        letters.push(letter);
        
        var texto = $(this).find('content texto').text().trim();
        
        if (texto.charAt(0) != "<") {
            texto = "<p>"+texto+"</p>";
        }
        
        var thisContainer = 'explanation-'+$(this).find('title').text().toLowerCase().replace(/\s+/g,"_");
        
        var sound = $(this).find('content sound src').text();
        if (sound != "") {
            var audioTag = '<a href="#" class="audio-control">&nbsp;</a>';
            audioTag += '<audio>';
            audioTag += '<source src="'+sound+'" type="audio/ogg"></source>';
            audioTag += '<source src="'+sound+'" type="audio/mp3"></source>';
            audioTag += '</audio>';
            console.log(audioTag);
            sound = audioTag;
        }
        
        var img = $(this).find('content image src').text();
        if (img != "") {
            imgTop = ($(this).find('content image position').text() == "top");
            img = '<img src="' + img + '" alt="" />';
            if (imgTop) {
                texto = img + texto;
            } else {
                texto = texto + img;
            }
        }   
        
        $('#explanations-container').append('<div class="explanation" id="'+thisContainer.replace(/\s+/g,"_")+'"><h3>'+$(this).find('title').text()+sound+'</h3><div>'+texto+'</div></div');

        $('.explanation').last().find('img').click( function(e) {
            cat_plugin.imgFull(this.src); 
        });
                
        $(this).find('css').each( function() {
            cssAdd += '#'+thisContainer + ' p { '+$(this).text() + ' word-wrap: break-word; }\n';
            cssAdd += '#'+thisContainer + ' h3 { font-family: '+findCssValue($(this).text(),"font-family") + '; }\n';
        });
    });

    
    cssAdd += 'body { '+xml.find('styles body').text() + ' }\n';
    cssAdd += '#letters-container, .explanation h3 { '+xml.find('styles header').text()+' }\n';
    $('<style type="text/css">'+cssAdd+'</style>').appendTo("head");
    $('#letters-container a').css('color', $('#letters-container').css('color')); // fix    
    contentsLoaded();

}

function findCssValue(cssText, property)
{
    var a = cssText.substr(cssText.indexOf(property));
    var ab = a.indexOf(":");
    var c = a.indexOf(";");

    return a.substring((ab+1),c).trim();
}

function stopSounds() {
    if (soundInstance && soundInstance.playState) {
        soundInstance.stop();
        $('source[src$="' + soundInstance.src + '"]').closest("audio").siblings("a").removeClass("playing");
        createjs.Sound.removeSound(soundInstance.src);
        soundInstance = false;    
    }
    
}


function contentsLoaded() {
    
    /* define events */
    
    fullW = $('#letters-container').width();    
    /* on start */
    
//    $('#letters-container, #terms-container, #explanations-container').css({
//        'border-radius': fullW*0.02+'px'
//    });
//    
//    $('.explanation h3').css({
//       'border-bottom-left-radius': fullW*0.01+'px',
//       'border-bottom-right-radius': fullW*0.01+'px'
//    });

    createjs.Sound.registerPlugins([createjs.HTMLAudioPlugin]);
    
    /* define events */

    createjs.Sound.addEventListener("fileload", function(event) {
        soundInstance = createjs.Sound.play(event.src);
        soundInstance.addEventListener("complete", function(e) {
            $('source[src$="'+e.target.src+'"]').closest("audio").siblings("a").removeClass("playing");
            createjs.Sound.removeSound(e.target.src);
            soundInstance = false;
        });
    });
    
    function sameNameTrunk(str1, str2) {
        return (str1.substring(0, str1.lastIndexOf(".")) === str2.substring(0, str2.lastIndexOf(".")));
    }

    // LISTENER play/pause sonido
    $('.audio-control').click( function(e) {
        
        e.preventDefault();

            if ($(e.target).hasClass("playing")) {
                soundInstance.stop();
                createjs.Sound.removeSound(soundInstance.src);
                soundInstance = 'undefined';
                $(e.target).removeClass("playing");
            } else {
                var audioTag = $(e.target).siblings("audio");
                if (audioTag.length > 0)
                {
                    var oggSrc = audioTag.find("source").eq(0).attr("src");
                    if (typeof soundInstance === 'undefined' || typeof soundInstance.src === 'undefined' || !sameNameTrunk(soundInstance.src, oggSrc)) {
                        if (typeof soundInstance !== 'undefined' && typeof soundInstance.src !== 'undefined') {
                            stopSounds();
                        }
                        createjs.Sound.alternateExtensions = ["mp3"];
                        createjs.Sound.registerSound({id: "currentSound", src: oggSrc});
                    } else {
                        soundInstance.play();
                    }
                    $(e.target).addClass("playing");
                }
            }
    });
    

    
    var lettersN = $('#letters li').length;
    $('#letters li').each( function() {
        //$(this).width((100/lettersN)+"%");
    });
    
    $('#letters a').click( function(e) {
        e.preventDefault();

        reporte = setInteractionReporting(reporte);
        stopSounds();
        //Hace todo lo demás de cara al usuario         
        $('#terms li').hide();
        $('#explanations-container').fadeOut(200);
        $('#terms-'+$(this).text()).each( function() {
            $(this).show();
            $('.link-todos').show();
        });
        cat_plugin.waitForHeight();
    });

    $('#letters a').first().each( function() {
        
        $('#terms li').hide();
        $('#explanations-container').fadeOut(200);
        $('#terms-'+$(this).text()).each( function() {
            $(this).show();
            $('.link-todos').show();
        });
    });


    
    $('.link-todos').click( function(e) {

        e.preventDefault();

        reporte = setInteractionReporting(reporte);
        
        //Hace todo lo demás de cara al usuario
        $('#terms li').hide();
        $('#terms-all').show();
        $(this).hide();
        cat_plugin.waitForHeight();
    });
    
    // $('#terms-all').show();
    
    $('#terms a').click( function(e) {
       e.preventDefault();

        reporte = setInteractionReporting(reporte);
        stopSounds();
        
        //Hace todo lo demás de cara al usuario
       $('#explanations-container:hidden').fadeIn(200);
       var nextEl = $($(this).attr('href')); 

       if ((nextEl).filter(':hidden').length > 0) {
            $('.explanation:not('+ $(this).attr('href')+')').hide();
            $('#explanations-container').css('background-color', '#FFFFFF');
            $('#explanations-container').animate( { 'height':nextEl.outerHeight(true)+'px' }, 200, function() {
                cat_plugin.waitForHeight();
            });  
            nextEl.fadeIn(200);
            if (autoplay) {
                nextEl.find('.audio-control').trigger('click');
            }
            $('#explanations-container').css('background-color',nextEl.find('p').css('background-color'));           
        }    

    });
    cat_plugin.waitForHeight();

}

    xmlFile = 'userdata.xml';

    if (window.XMLHttpRequest) {
            // Navegadores civilizados
            $.support.cors = true;
            $.ajax({
                type: 'GET',
                url: xmlFile,
                dataType: ($.browser.msie) ? "text" : "xml",
                async: false,
                success: addContent,
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    console.log('Error al leer XML - ' + errorThrown);
                    document.write('<script src="userdata.js"></script>');
                }
            });
        } else {
            if (typeof window.ActiveXObject != 'undefined') {
                // Internet Explorer
                xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
                xmlDoc.async = false;
                while (xmlDoc.readyState != 4) {
                }
                xmlDoc.load(xmlFile);
                addContent(xmlDoc);
            } 
        }