function catplug_glosario(container) {
    var that = this;
    var baseW = 760;
    var baseH = 668;
    // this.aspectRatio = (baseW / baseH);
    this.aspectRatio = false;
	this.container = container;
    this.ready = false;

    this.limitByHeight = false;
    this.loadedCallback = function() {};
    
    this.redraw = function() {
        console.log("redrawing..");
        var borderR = 20;
        if (this.limitByHeight) {
            console.log("redimensioning, h limit");
            // (limitByHeight ?  ($(window).height() / $('#'+this.htmlId).height()) : ($('body').innerWidth() / $('#'+this.htmlId).width()));
            // $(container).height( $(window).height() ).width( this.aspectRatio * $(window).height() );
        } else {
            console.log("redimensioning, w limit");
            // $(container).width( $(window).width() ).height( $(window).width() / this.aspectRatio );
        }
        borderR = (this.aspectRatio > 1 ? $(container).width() : $(container).height()) * (this.borderRRel / 100);

        $(container).css('border-radius', borderR+'px');
        $(container).find("div").first().css('border-radius',borderR+'px');
        $(container).find("h1").css('border-top-left-radius', borderR+'px').css('border-top-right-radius', borderR+'px');
        cat_plugin.waitForHeight();
    };
    
    $(window).resize( function() {
       that.redraw(); 
    });
    
    this.setAspectRatio = function(z) {
        console.log(this.aspectRatio = z);
        this.redraw();
        $('#img-container').css('background-image', 'url('+$('#main-img').attr('src')+')');
        this.playing = true;
        console.log($('#main-img').attr('src') + " -> " + $('#img-container').css('background-image'));
        
    };
    
    this.setLimitByHeight = function(lbh) {
        this.limitByHeight = lbh;
    };
    this.play = function(limitByHeight) {
        console.log("### GLOSARIO PLAY "+limitByHeight);
        this.playing = true;

    };

}


