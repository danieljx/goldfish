var soundInstance;
var fullH = 0;


// 2015.03.20 Mats Fjellner
// Esto debería ir en una clase "catplugin" separado, siguiendo el ejemplo de otras infografías

function growFrameH(newH) {
    try {
        if ( window.frameElement ) {
            // Si estamos dentro de un IFRAME, hazlo crecer en sentido Y para que tenga la misma altura que la infografía
            $('#'+window.frameElement.getAttribute('id'), window.parent.document).height(newH);
        }
    } catch(e) {

    }
}

function waitForHeight() {
        var h = $('body').height(); 
        if (h > 0) {
            growFrameH(h);
        } else {
            setTimeout(waitForHeight, 500);
        }
}


//Interacción de REDs con LMS
// Tipos de recursos educativos digitales (RED)
var FIELD_LEARNINGUNIT_RESOURCE_TYPES_INFOGRAFIAHTML5 = 4;
var reporte = 0;

function setInteractionReporting(report){
            //Envío de datos de interacción al LMS
        try {
                if(!report && (window.frameElement.getAttribute('data-red-id') != null) ){
                    //Ejecutar función de interfaz con el API de SCORM (incluida cuando se ha exportado la unidad, ver SCO.js)            
                    //Al hacer el reporte no aplican puntuaciones (se dejan en cero)            
                    window.parent.report_red_to_lms(window.frameElement.getAttribute('data-red-id'), FIELD_LEARNINGUNIT_RESOURCE_TYPES_INFOGRAFIAHTML5, 0, 0, 0);

                    report = 1;
                }
            } catch(e) {

            }

        return report;
}

function prepareImgLink(aDiv) {
    $('#img-link').fadeOut(100);

    if (aDiv.children('img').length > 0) {
        $('#img-link').fadeIn(200);
    }
}

function prepareAudioJs(aDiv) {
        
    if ($("#controls .audio-control").hasClass("playing")) {
        soundInstance.stop();
        $("#controls .audio-control").removeClass("playing");
    }
    
    if (aDiv.children('audio').length > 0) {
        $('.audio-control').show();
    } else {
        $('.audio-control').hide();
    }
    
}

function setBackground(divOld,divNew) {
    if (divOld != '') {
        divOld.css('background-image', $('#explanation').css('background-image') );
        divOld.css('background-color', $('#explanation').css('background-color') );

    }
    $('#explanation').css('background-image', divNew.css('background-image') );
    $('#explanation').css('background-color', divNew.css('background-color') );
    divNew.css('background-image','none');
    divNew.css('background-color','transparent');
} 

function connectLine(termDiv) {
    $("#line-holder > .line").remove();
    //CALCULAR ANCHO
        setTimeout(function(){
            x1 = Math.round(termDiv.position().left + termDiv.width() / 2 );
            x2 = Math.round($('#explanation').position().left + $('#explanation').width() / 2 );
            y1 = Math.round(termDiv.position().top + termDiv.height() / 2 );
            y2 = Math.round($('#explanation').position().top + $('#explanation').height() / 2 );
 
        var length = Math.sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));
        
        var angle  = Math.atan2(y2 - y1, x2 - x1) * 180 / Math.PI;
        var transform = 'rotate('+angle+'deg)';

        var line = $('<div>')
            .appendTo('#line-holder')
            .addClass('line')
            .css({
              'position': 'absolute',
              'transform': transform
                })
            .width(length)
            .offset({left: Math.round(x1 < x2 ? x1 : x1 - (x1-x2)), top: Math.round(y1 < y2 ? y1 : y1 - (y1-y2))});

        return line;
        }, 400 );
}

function toggleText(forceText) {
        currentDiv = $( '#explanation div:visible:not(#controls)' );
        if (forceText) {
            $('#img-link').removeClass('txt');
            currentDiv.children('img').hide();
            currentDiv.children('article').show();
            
        } else {
            if (!$('#img-link').hasClass('txt')) {
                $('#img-link').addClass('txt');
                currentDiv.children('article').fadeOut(200, function() {
                    currentDiv.children('img').fadeIn(200);
                    growFrameH( $('body').outerHeight(true) );
                });
            } else {
                $('#img-link').removeClass('txt');
                currentDiv.children('img').fadeOut(200, function() {
                    currentDiv.children('article').fadeIn(200);
                    growFrameH( $('body').outerHeight(true) );
                });
            }
        }
}
//variable global agregada por DUVAN VALENCIA 11012017
var position_;
function addContent(xmlContents) {
    xml = $(xmlContents);
      
  

    var cssAdd = '';
    xml.find('term').each(function(i) {
        n = i + 1;
        var repAuto, rep, id_one;
        var src = $(this).find('explanation sound src').text();
        if ( src != "" ) {
            repAuto = $(this).find('explanation sound repAuto').text();
        }

        $('<div class="term" id="term'+n+'" data-auto-repro="'+repAuto+'"><h2>'+$(this).find('title').text()+'</h2></div>').appendTo( '.span3:eq('+(n > 4 ? 1 : 0)+')' );
        var explanation = $('<div id="explanation'+n+'" class="explanation"></div>');
        var cont_media  = $('<div id="cont_media'+n+'"></div>');
        var img = $(this).find('explanation image src').text();

        //DUVAN
        if( img != '' ){
            cont_media.append('<a class="img-link" id="'+n+'" style="display: inline-block;">Imagen</a>');
        }

        //guardamos la posicion de la imagen
        position_ = $(this).find('explanation image position').text();
        if ( img != "" ) {
            explanation.append('<img src="'+img+'" id="image_'+n+'"/>');
        }

        
        if ( src != "" ) {
            if( repAuto == 'true' && n == 1){
                rep = 'autoplay';
            }

            explanation.append('<audio '+rep+' id="audio-'+n+'"><source src="'+src+'.ogg" type="audio/ogg"><source src="'+src+'.mp3" type="audio/mp3"></audio>');
        }

        //DUVAN COLCAR ID PARA QUE AL DAR CLICK SE DE DONDE ES...
        if( src != '' ){
           cont_media.append('<a href="#" class="audio-control '+n+'">&nbsp;</a>');
        }

        //DUVAN 
        explanation.append(cont_media);

        explanation.append('<article>'+$(this).find('explanation texto').text()+'</article>');
        explanation.appendTo('#explanation');
        
        $(this).find('explanationcss').each( function(j) {
            cssAdd += '#explanation'+n+' { '+$(this).text()+' } ';
        });
        cssAdd += '#term'+n+'{'+$(this).find('css').text()+'}';
        cssAdd += '#explanation'+n+'{'+$(this).find('css').text()+'}';
        
    });
    $('<style type="text/css">'+cssAdd+'</style>').appendTo("head");

    contentsLoaded();
    call();
}

    function call(){
        $(".img-link").click(function(e){
            reporte = setInteractionReporting(reporte);
            //Hace todo lo demás de cara al usuario
            toggleText(false);  
        });
    }
        
//DUVAN/ALEXIS/VALENCIA 11012017
function preparePosition(){
    var parent = $("#explanation"), parentHeight = parent.css('height'), class_;
    var elem_ = $('#explanation > div#controls');
        elem_.attr('data-position', position_);


    ((position_ == 'top') ? class_ = 'above' : class_ = 'below');
    elem_.addClass(class_);

}
//FIN
function contentsLoaded() {
    preparePosition(); 
    prepareImgLink( $( '#explanation > div:visible:not(#controls)' ) );
    prepareAudioJs( $( '#explanation > div:visible:not(#controls)' ) );
    
    setBackground( '', $( '#explanation > div:visible:not(#controls)') );
    
    connector = connectLine($('#term1'));
    
    createjs.Sound.registerPlugins([createjs.HTMLAudioPlugin]);
    
    createjs.Sound.addEventListener("fileload", function(event) {
            soundInstance = createjs.Sound.play(event.src);
        });
        
    // LISTENER play/pause sonido
    $('.audio-control').click(function(e) {

        e.preventDefault();

        reporte = setInteractionReporting(reporte);

        //Hace todo lo demás de cara al usuario
        if ($(e.target).hasClass("playing")) {
                soundInstance.pause();
                $(e.target).removeClass("playing");
            } else {
                var audioTag = $("#explanation").find("div:visible audio");

                if (audioTag.length > 0) {
                    createjs.Sound.alternateExtensions = ["mp3"];
                    createjs.Sound.registerSound({id: "currentSound", src: audioTag.find("source").eq(1).attr("src")});
                    $(e.target).addClass("playing");
                }
            }

    });
    
$("#explanation").children().each(function(){
        if( $(this).attr('id') != 'explanation1' )
            { $(this).css("display","none"); }
   })
   
   $('.term').click( function(e) {
        
        e.preventDefault();

        reporte = setInteractionReporting(reporte);
        
        //Hace todo lo demás de cara al usuario
        toggleText(true);

        var divNext = $( '#explanation' + $(this).attr('id').match(/\d+/)[0] );
        var divPrev = $( '#explanation > div:visible:not(#controls)' );
        var id_ = $(this).attr('id').match(/\d+/)[0];
        console.warn($(".explanation").find("audio").length);
       
         $(".explanation").find("audio").each(function(){
                document.getElementById($(this).attr("id")).pause();
         });

         console.warn($(this).attr("data-auto-repro"));
         if( $(this).attr("data-auto-repro") != 'undefined' ){
            document.getElementById("audio-"+id_).play();
         }

        //prepareImgLink(divNext);
        //prepareAudioJs(divNext);  

        console.log(divNext.selector);
        console.warn();   
        
        divPrev.fadeOut(200, function() {
            setBackground( divPrev, divNext );
            divNext.fadeIn(400);
            growFrameH( $('body').outerHeight(true) );
        });
             //connector.remove();
        connector = connectLine($(this));   
    });    

    try {
        if (window.frameElement) {
            waitForHeight();
            $(window).resize( waitForHeight );
        }
    } catch(e) {

    }
}
xmlFile = 'userdata.xml';

if (window.XMLHttpRequest) {

    // Navegadores civilizados
    $.support.cors = true;
    $.ajax({
        type: 'GET',
        url: xmlFile,
        dataType: ($.browser.msie) ? "text" : "xml",
        async: false,
        success: addContent,
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            document.write('<script src="userdata.js"></script>');
//            addContent(xmlFix);
        }
    });
} else {
    if (typeof window.ActiveXObject != 'undefined') {
        // Internet Explorer
        xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
        xmlDoc.async = false;
        while (xmlDoc.readyState != 6) {
        }
        xmlDoc.load(xmlFile);
        addContent(xmlDoc);
    }
}