var fullW = 0;
var fullH = 0;

//Interacción de REDs con LMS
// Tipos de recursos educativos digitales (RED)
var FIELD_LEARNINGUNIT_RESOURCE_TYPES_INFOGRAFIAHTML5 = 4;
var reporte = 0;


function setInteractionReporting(report){
            //Envío de datos de interacción al LMS
        try {
                if(!report && (window.frameElement.getAttribute('data-red-id') != null) ){
                    //Ejecutar función de interfaz con el API de SCORM (incluida cuando se ha exportado la unidad, ver SCO.js)            
                    //Al hacer el reporte no aplican puntuaciones (se dejan en cero)            
                    window.parent.report_red_to_lms(window.frameElement.getAttribute('data-red-id'), FIELD_LEARNINGUNIT_RESOURCE_TYPES_INFOGRAFIAHTML5, 0, 0, 0);

                    report = 1;
                }
            } catch(e) {

            }

        return report;
}

function addContent(xmlContents) {
    xml = $(xmlContents);
    
    ludica1.setTitle( xml.find('ludicTitle').text() );
    ludica1.setBaseColor( xml.find('basecolor').text() );
    ludica1.setInstanceColor( xml.find('instancecolor').text() );
    ludica1.setContent( xml.find('content').text() );
    ludica1.setLogo( xml.find('logo').text() );
    ludica1.setTransition( xml.find('transition').text() );
    ludica1.setAvatar( xml.find('avatar src').text() );
    contentsLoaded();
}

$(window).resize( function() {
    fullW = $('#img-container').width();
    fullH = $('#img-container').height();
});

function contentsLoaded() {
    ludica1.draw();
}

    xmlFile = 'userdata.xml';

    if (window.XMLHttpRequest) {
            // Navegadores civilizados
            $.support.cors = true;
            $.ajax({
                type: 'GET',
                url: xmlFile,
                dataType: ($.browser.msie) ? "text" : "xml",
                async: false,
                success: addContent,
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    console.log('Error al leer XML - ' + errorThrown);
                    document.write('<script src="userdata.js"></script>');
                }
            });
        } else {
            if (typeof window.ActiveXObject != 'undefined') {
                // Internet Explorer
                xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
                xmlDoc.async = false;
                while (xmlDoc.readyState != 4) {
                }
                xmlDoc.load(xmlFile);
                addContent(xmlDoc);
            } 
        }
