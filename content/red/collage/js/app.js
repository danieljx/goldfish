var Collage = Collage || {};
/** The application main model
* @author Duvan Valencia
* @augments Backbone.Model
* @constructor
* @name app
*/
Collage.app = Backbone.Model.extend( /** @lends app */ {
	/** Defaults:
		* Sets a default value for: 
		* Selected  = (class) select element element-dinamyc.
		* Inactive  = (class) hidde a element.
		* Window 	= (id) where put the app's resources.
		* Console   = (id) console errors.
		* DinamycEl = (used as class) container for 'images', 'audios', 'icons' characterrs.
		* Folders   = (link) used for save the new characters
		* idCopied  = (id) used for add an event target
    */
	defaults:{
		"selected": ".selected-obj",
		"inactive": "element-inactive",
		"window":   '#window-edition',
		"console":	'#console',
		"dinamycEl": 'element-dinamyc',
		"folders": "files/recursos/Personajes/",
		"idCopied": "undefined"
	},
	/** On init, instance the drag and drop and the resizable widget.
    */
	initialize: function(){
		console.info("%c***** Modelo principal app ***** ","font-size:15px;font-weight:bold;color:#804000;");
		this.componentsWidget();
	},
	/** Creates an id for each new element dynamic.
		* @param {string} tag - id alias.
		* @return {string} id with alias concatenated.
    */
	createId: function(tag){
		var date  = new Date(),
			id_ = date.getTime();
		return id_+'-'+tag;
	},
	/** Remove class selected-obj for each element-dinamyc
    */
	deselected: function(){
		$("#window-edition").attr("data-selected","false");
		$("#window-edition").removeClass( "selected-obj" );
		if ( $(this.get("selected")).length == 1 ){
				$(this.get("selected")).each(function(){
					$(this).removeClass( "selected-obj" ).removeClass("active");
				})
		}
	},
	/** Puts de colors for svg object tags html (icons)
		* This is only used in addContent function
		* @param {string} icolors - id tag object.
		* @return {string} none.
    */
	afterLoad: function(icolors){

		var total = icolors.length;
			if ( total >= 1){
				for (var i = 0; i <= icolors.length - 1; i++) {
					var a = document.getElementById( icolors[i].id );
					var svgDoc  = a.contentDocument;
					var svgItem = svgDoc.getElementById("icon");
						svgItem.setAttribute("fill", icolors[i].color );
				}
			}
	},
	/** NEW INSTANCE 
		* Creates a new view alert
		* @param {string} type - sets the alert types.
		* @param {string} text - info for the user.
		* @return {object} none.
    */
	showAlert: function(type, text){
		var classAlert, spanClass;
			switch (type) {
				case "error":
					classAlert = "alert-danger";
					spanClass  = "glyphicon-remove";
				break;
				case "ok":
					classAlert = "alert-success";
					spanClass  = "glyphicon-ok";
				break;
				case "warning":
					classAlert = " alert-warning";
					spanClass  = "glyphicon-warning-sign";
				break;
			}
		var alert = new Collage.alert({ classA:classAlert, icon:spanClass, text: text });
	},
	/** NEW TEMPLATE
		* Creates a new element tag object
		* @param {string} src - link source.
		* @return {object} object tag.
    */
	templateSvg: function(src){
		var id_ = this.createId('svg');
		var obj_ = $("<object/>");
			obj_.attr("class", "icon-color set-color-svg");
			obj_.attr("id", id_);
			obj_.attr("data",src);
			obj_.attr("type","image/svg+xml");
			obj_.attr("height","50");
			obj_.attr("width","50");
			return obj_;
	},
	/** NEW TEMPLATE
		* Creates a new element text  ( used for plugin text and addContent ).
		* @param {string} attrTxt - type of header h1, h2 ... h6 .
		* @param {string} text - info for the user.
		* @return {object} object html template.
    */
	tmp_text: function(attrTxt, text){
		var Id_ = this.createId("text");
		var div_ = $("<div/>");
			div_.attr("id", Id_+"-plugin")
			div_.addClass("text-plugin");
			div_.attr("data-font-size", attrTxt );
								
		var set_title = $("<section/>");
			set_title.addClass("editable-text-header");
			set_title.text(text);
			set_title.attr("id", Id_);

		var section_ = $("<section/>");
			section_.addClass("editable-text-input element-inactive");//
			section_.attr("id", Id_+"-editable");

		var input_ = $("<input/>");
			input_.attr("type","text");
			input_.addClass("withoutBorder editable");
			input_.attr("data-id-parent", Id_);
			input_.attr("value", attrTxt );

			section_.append( input_ );

			div_.append( set_title );
			div_.append( section_ );
			return div_;
	},
	/** NEW TEMPLATE
		* Creates a new element audio ( used for plugin multimedia and addContent ).
		* @param {file} attrTxt - link source.
		* @return {object} object html template.
    */
	tmp_audio: function(file){
		var id_ = this.createId('audio');
		
		var soundBase = $("<div/>");
			soundBase.attr("data-child-audio", id_);
			soundBase.attr("data-state", "false");
			soundBase.attr("class", "playSound");

		var secPlay = $("<section/>");
			secPlay.attr("class","audioPlayIcon");

		var Icon = $("<div/>");

		var obj_ = $("<object/>");
			obj_.attr("class", "audio-color set-color-svg");
			obj_.attr("id", id_+"-svg");
			obj_.attr("data","files/play.svg");
			obj_.attr("type","image/svg+xml");
			obj_.attr("height","55");
			obj_.attr("width","55");

			Icon.append(obj_);

		var secAudioTag = $("<section/>");
			secAudioTag.attr("class", "audioTagElement");

		var AudioTag = $("<audio/>");
			AudioTag.attr("id", id_);

		var sourceTag = $("<source/>");
			sourceTag.attr("src",file);
			sourceTag.attr("type","audio/mpeg");

			AudioTag.append(sourceTag);
			secAudioTag.append(AudioTag);
			secPlay.append(Icon);
			soundBase.append(secAudioTag);
			soundBase.append(secPlay);

		return soundBase;
	},
	/** NEW TEMPLATE
		* Creates a new element image ( used for plugin multimedia and addContent ).
		* @param {file} attrTxt - link source.
		* @return {object} object html template.
    */
	tmp_Image: function(file){
		var id_ = this.createId('img');
			var img_ = $("<img/>");
				img_.attr("class",'dynamic-image');
				img_.addClass("gf-object-image");
				img_.attr("src",file);
				img_.attr("id",id_);
		return img_;
	},
	/** NEW TEMPLATE
		* Creates a new dinamy element container for images, audios, icons, text.
		* @param {object} html template.
		* @return {object} object html template.
    */
	templateDinamyc: function( tmp ){
		var zIndex_ = parseInt(this.getZindex());
			zIndex_ += 1;
		this.setZindex( zIndex_ );

		var id_ = this.createId('dynamic');
		var section_ = $('<section/>');
		    section_.addClass( this.get("dinamycEl")+" gf-dinamyc-element-draggable" );
		    section_.attr("id",id_);
		    section_.css("z-index",zIndex_);
		var div_circle = $("<div/>");
			div_circle.attr("class","gf-circle-edit-dinamyc-element");
		var span_ = $("<span/>");
			span_.attr("class","glyphicon glyphicon-pushpin");
		var div_body = $("<div/>");
			div_body.attr("class","gf-dinamyc-body");
			div_body.append( tmp );
			div_circle.append( span_ );
			section_.append( div_body );
		return section_;
	},
	/** Return the global z-index.
		* @param {object} none.
		* @return {object} string.
    */
	getZindex: function(){
		return  $("#window-edition").attr("data-global-zindex");
	},
	/** Set the new value for the global z-index.
		* @param {object} new Number.
		* @return {object} none.
    */
	setZindex: function( nmb ){
		$("#window-edition").attr("data-global-zindex", nmb );	
	},
	/** drag and drop, resizable
 	*/
	componentsWidget: function(){
		/*var this_ = this;
		$(".element-dinamyc, .draggable-initial")
			.draggable({
				stop:
				function(event, ui){
					var dragElem = $(ui.helper[0]);
				if ( dragElem.hasClass('draggable-initial') == true){
						if ( dragElem.hasClass("drag-icons") == true ) {
							var src_ = dragElem.find("img").attr("src");
							var obj  = this_.templateSvg(src_);
							var dinm = this_.templateDinamyc(obj);
								dinm.css({ "left":"50%",top:"25%"});
							$("#window-edition").append( dinm )
								dragElem.css({ "left":"auto",top:"auto"});	
						}
						if ( dragElem.hasClass("drag-resource") == true ) {
							var src_ = dragElem.find("img").attr("src");
							var img_ = this_.tmp_Image(src_);
							var dinm = this_.templateDinamyc(img_);
								dinm.css({ "left":"50%",top:"25%"});
							$("#window-edition").append( dinm )
							dragElem.css({ "left":"auto",top:"auto"});	
						}
						if ( dragElem.hasClass("panel-text") == true ) {
							var attrTxt = dragElem.attr("data-font-size");
							var div_ = this_.tmp_text(attrTxt, "Lorem ipsum");
							var dinm =  this_.templateDinamyc(div_) ;
								dinm.css({ "left":"50%",top:"25%"});
							$("#window-edition").append(dinm)
							dragElem.css({ "left":"auto",top:"auto"});	
						}
					}else{
						console.warn( "arrastrar normal" );
					}
				this_.componentsWidget();	
				}
			});
		//resiable
		$("#window-edition .element-dinamyc").resizable({
			resize:function(event,ui){
					var idResizer = $("#"+$(ui.element[0]).attr("id"));
						idResizer.addClass("activeBorder");
					//Imagen
					if ( idResizer.find("img").length == 1 ){
						 idResizer.find("img").css({
						 	width: idResizer.width()+"px",
						 	height:idResizer.height()+"px"
						});	
					}
					//Audio
					if ( idResizer.find("div.playSound").length == 1){
							idResizer.find("div.playSound").css({
							 	width: idResizer.width()+"px",
							 	height:idResizer.height()+"px"
							})
							idResizer.find("div.playSound")
								.find("div.audioPlayIcon")
								.find("object.set-color-svg")
								.css({
									"height": idResizer.height()+"px",
									"width": idResizer.width()+"px",
								})
					}//end if
					if ( idResizer.find("object.set-color-svg").length == 1){
						idResizer.find("object.set-color-svg")
							.css({
							 	width: idResizer.width()+"px",
							 	height:idResizer.height()+"px"
							})
					}
			},
			stop: function(event,ui){
				var idResizer = $("#"+$(ui.element[0]).attr("id"));
					idResizer.removeClass("activeBorder");	
			}
		});*/
	}
});



