/*
CREAR UN NUEVO PLUGIN LLAMADO (MARCAR NAVEGACION)
CAMBIAR EL CURSOR POR UN ICONO, QUE PUEDA GRABAR EL RECORRIDO DE ESTE
PARA MOSTRARLE AL USUARIO COMO DEBE NAVEGAR
mousedown = grabar left y top, guardarlos en un array json y grabaro en una animación


*/

//145

function pxToPercentage( valor, widthPadre ){
	var equivalent = (valor / widthPadre)*100;
	return equivalent;
}

function preLoad(){
	var widthMain = $(window).outerWidth();
	var heightMain = $(window).outerHeight();

	$(".element-dinamyc").each(function(){
		var findImg = $(this).find("img");
		var findSound = $(this).find(".playSound")
	//Esta es la imagen	
		if ( findImg.length >= 1){  findImg.css({"width":"100%","height":"100%"})}
		if ( findSound.length >= 1){ findSound.css({"width":"100%","height":"100%"})}
			var widthMe  = $(this).outerWidth()  ,
				heightMe = $(this).outerHeight()  ,
				leftMe   = parseInt($(this).css("left")) - 145, 
				topMe    = parseInt($(this).css("top"));
			var toPercentageW = pxToPercentage( widthMe , widthMain ),
				toPercentageH = pxToPercentage( heightMe , widthMe ) ,
				toPercentageL = pxToPercentage( leftMe , widthMain ),
				toPercentageT = pxToPercentage( topMe , widthMain );

			$(this).css({ 
				"width": toPercentageW+"%", "height": toPercentageH+"%",
				"left": toPercentageL+"%", "top": toPercentageT+"%"
			});
	})
}


$("body").on("click",".element-dinamyc[data-ref-id-event!='']", function(){
	var IDS = $(this).attr("data-ref-id-event").split(",");
	for (var i = 0 ; i <= IDS.length -2 ; i++) {
		 //$("#"+IDS[i]).fadeIn("fast");
		 $("#"+IDS[i]).css("visibility","visible");
	  }

})

$("body").on("click",".element-dinamyc[data-collection-to-disable!='']", function(){
	var IDS = $(this).attr("data-collection-to-disable").split(",");
	for (var i = 0 ; i <= IDS.length -2 ; i++) { 
			//$("#"+IDS[i]).fadeOut("fast");
			$("#"+IDS[i]).css("visibility","hidden")
		 }

})
function reajustratamanho(){
	var anchoDeDocumentoTotal = $(document).outerWidth(),
		altoDeDocumentoTotal  = $(document).outerHeight(),
		anchoDeVentanaDinamyc = $("#window-edition").outerWidth(),
		altoDeVentanaDinamyc  = $("#window-edition").outerHeight();

		//$("#window-edition").css("width", pxToPercentage(anchoDeVentanaDinamyc, anchoDeDocumentoTotal)+"%")
	console.warn( pxToPercentage(altoDeVentanaDinamyc, altoDeDocumentoTotal) );
}


function addContent(xml){
	
	console.warn("%caddContent","font-size:14px;font-weight:bold;color:magenta;");

	var collage    = $(xml).find("root").find("userdata"),
		globalCss  = collage.find("collageDelabObjects").find('css');
	var app = new Collage.app();
	
	console.info("left: " + globalCss.find("leftMinus").text());

	$("#window-edition")
		.attr("data-global-zindex", globalCss.find("globalZindex").text() )
		.attr("style", unescape(globalCss.find("window").attr("data-style")) );	

	collage.find("gfObject").each(function(){
		var typeSource  = unescape($(this).attr("data-child"));
		var childStyles = (unescape($(this).attr("data-child-styles"))!= "undefined") ? unescape($(this).attr("data-child-styles")) : "";
		var source_     = unescape($(this).attr("data-child-src"));
		var styleParent = (unescape($(this).attr ("data-style")) != "undefined") ? unescape($(this).attr ("data-style")) : "";
		var dcy, idcons = [];

		var isVisble = $(this).attr("data-visibility"),
			myIdent  = $(this).attr("data-my-id"),
			dataDisa = $(this).attr("data-ids-disbaled"),
			evntIden = $(this).attr("data-event-id");


		switch (typeSource) {
			//Colocamos las imagenes
				case "img":
					var tmpImg = app.tmp_Image( source_ );
						tmpImg.attr("style", childStyles);

						dinamyc = app.templateDinamyc(tmpImg);
						dinamyc.attr("data-visibility-state", isVisble);
						dinamyc.attr("id", myIdent);
						dinamyc.attr("data-ref-id-event",evntIden);
						dinamyc.attr("data-collection-to-disable", dataDisa);
						dinamyc.attr("style",styleParent);
				break;
				//Colocamos el texto
				case "texto":
					var text = unescape($(this).attr("data-texto"));
					var txtTmp = app.tmp_text( source_, text);
						txtTmp.attr("style",childStyles);

						dinamyc = app.templateDinamyc(txtTmp);
						dinamyc.attr("data-visibility-state", isVisble);
						dinamyc.attr("id", myIdent);
						dinamyc.attr("data-ref-id-event",evntIden);
						dinamyc.attr("data-collection-to-disable", dataDisa);
						dinamyc.attr("style",styleParent);
				break;
					//Colocamos el audio
				case "audio":
					var colorIcon = $(this).attr("data-child-icon-color");
					var sound_ = app.tmp_audio(source_);
						sound_.attr("style",childStyles);
						sound_.find("object").attr("style",childStyles);
						sound_.find("object").css("background-color","transparent");
							
					if ( colorIcon != undefined){
							sound_.find("object").attr("data-color",colorIcon);
							idcons.push({
								"id": sound_.find("object").attr("id"),
								"color":colorIcon
							})
					}

					dinamyc = app.templateDinamyc(sound_);
					dinamyc.attr("data-visibility-state", isVisble);
					dinamyc.attr("id", myIdent);
					dinamyc.attr("data-ref-id-event",evntIden);
					dinamyc.attr("data-collection-to-disable", dataDisa);
					dinamyc.attr("style",styleParent);
				break;
				//Colocamos Iconos
				case "icon":
					var colorIcon = $(this).attr("data-child-icon-color");
					var svg_ = app.templateSvg( source_ );
						svg_.attr("style",childStyles);

					if ( colorIcon != "undefined" || colorIcon != undefined ){
						idcons.push({
							"id": svg_.attr("id"),
							"color":colorIcon
							})
					}
						dinamyc = app.templateDinamyc(svg_);
						dinamyc.attr("data-visibility-state", isVisble);
						dinamyc.attr("id", myIdent);
						dinamyc.attr("data-ref-id-event",evntIden);
						dinamyc.attr("data-collection-to-disable", dataDisa);
						dinamyc.attr("style",styleParent);
				break;
		}

		$("#window-edition").append( dinamyc );
		app.componentsWidget();	
				setTimeout(
					function(){
							app.afterLoad(idcons);
							reajustratamanho();
					}, 1000 )
	})
}//fin del addContent

xmlFile = 'userdata.xml';

if ( window.XMLHttpRequest ) {

		    // Navegadores civilizados
		    $.support.cors = true;
		    $.ajax({
		        type: 'GET',
		        url: xmlFile,
		        dataType: "xml",//($.browser.msie) ? "text" :  
		        async: false,
		        success: addContent,
		        error: function(XMLHttpRequest, textStatus, errorThrown) {
		            document.write('<script src="userdata.js"></script>');
		        }
		    });
		} else {
		    if (typeof window.ActiveXObject != 'undefined') {
		        // Internet Explorer
		        xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
		        xmlDoc.async = false;
		        while (xmlDoc.readyState != 6) {
		        }
	        xmlDoc.load(xmlFile);
	        addContent(xmlDoc);
    	}
}