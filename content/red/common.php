<?php
function getPluginXml($db, $pluginId) {
    $select = $db->select();
    $select->from(TABLE_LEARNINGUNIT_PLUGIN_SETTINGS, array('data'));
    $select->where('id= ?', $pluginId);
    $row = $db->fetchRow($select);
    return stripslashes($row['data']);
}

function getPluginDir($src) {
    return $_SERVER['DOCUMENT_ROOT'] . $src;
}

function mkSessionDir($dataFolder = "editordata") {
    if (!isset($_SESSION)) { 
        session_start(); 
    }
    $sesDir = $_SERVER['DOCUMENT_ROOT'] . "/".$dataFolder."/sessiondata";
    if (!is_dir($sesDir))
        mkdir($sesDir);
    $session_id = session_id();
    $dirName = $sesDir."/ses_".date('Ymd')."_".$session_id;
    if (!is_dir($dirName))
        mkdir($dirName);
    return $dirName;
}

function rmdir_recursive($dir) {
    $ok = rmdir($dir);
    if ($ok) {
        return true;
    }
    else {
        $dircontents = scandir($dir);
        foreach ($dircontents as $file) {
            if (is_dir($dir."/".$file)) {
                    if ($file != "." && $file != "..")
                        rmdir_recursive($dir."/".$file);
            }
            else
                unlink ($dir."/".$file);
        }
        return rmdir($dir);
    }
}

function cleanSessionDir($dataFolder = "editordata") {
    $sessionData = $_SERVER['DOCUMENT_ROOT'] . "/".$dataFolder."/sessiondata";
    $files = scandir($sessionData);
    $limitDate = strtotime("-2 day");
    foreach ($files as $file) {
        if (substr($file, 0, 6) == "clean_")
                unlink($sessionData."/".$file);
        if ((substr($file, 0, 4) == "ses_") && (is_dir($sessionData."/".$file)) ) {
            $fileDate = mktime(0,0,0,substr($file,8,2)*1,substr($file,10,2)*1,substr($file,4,4)*1);
            if ($fileDate < $limitDate) {
                rmdir_recursive($sessionData."/".$file);
            }
            
        }
    } 
    $file = fopen($sessionData."/clean_".date('Ymd').".tmp","w");
    fclose($file);
    
}

function cleanedToday($dataFolder = "editordata") {
    if (!isset($_SESSION)) { 
        session_start(); 
    }
    $cleanedFile = $_SERVER['DOCUMENT_ROOT'] . "/".$dataFolder."/sessiondata/clean_".date('Ymd').".tmp";
    return is_file($cleanedFile);
}
function rrmdir($dir) {
  if (is_dir($dir)) {
    $objects = scandir($dir);
    foreach ($objects as $object) {
      if ($object != "." && $object != "..") {
        if (filetype($dir."/".$object) == "dir") 
           rrmdir($dir."/".$object); 
        else unlink   ($dir."/".$object);
      }
    }
    reset($objects);
    rmdir($dir);
  }
 }
function recurse_copy($src, $dst) {
    rrmdir($dst);
    if (is_dir($src)) {
        $dir = opendir($src);
        mkdir($dst, 0750);
        while ($file = readdir($dir)) {
            if (( $file != '.' ) && ( $file != '..' )) {
                if (is_dir($src . '/' . $file)) {
                    recurse_copy($src . '/' . $file, $dst . '/' . $file);
                } else {
                    copy($src . '/' . $file, $dst . '/' . $file);
                }
            }
        }
        closedir($dir);
    }
}
?>
