<?php

session_start();
set_time_limit(120);
error_reporting(E_ALL);

set_include_path('../../library');
require_once("Zend/Loader.php");
Zend_Loader::loadClass('Zend_Config_Xml');
Zend_Loader::loadClass('Zend_Registry');
Zend_Loader::loadClass('Zend_Db');
require_once("Local/config.load.php");
$configXml = getConfig();

include 'Local/MdlAnimationsLibraryModel.class.php';

$category = (isset($_REQUEST['cat']))? intval($_REQUEST['cat']) : 0;


$DB_CONFIG = new Zend_Config_Xml($configXml, 'database');

$Prefix_tables = $DB_CONFIG->db->prefix;
$registry = Zend_Registry::getInstance();
$registry->set('config', $DB_CONFIG);
$DB_CONNECTION = Zend_Db::factory($DB_CONFIG->db->adapter, $DB_CONFIG->db->config->toArray());

require_once('../../services/application/config/tables.php');
require_once("../../services/webservices/db/LearningUnitConceptMap.class.php");
require_once("../../services/webservices/db/LearningUnitPluginSettings.class.php");

$pdo = new PDO('mysql:host='.$DB_CONFIG->db->config->host.';dbname='.$DB_CONFIG->db->config->dbname.';charset=utf8', $DB_CONFIG->db->config->username, $DB_CONFIG->db->config->password);


$filter = array();
if ($category > 0) {
    $filter[] = new DFC(MdlAnimationsLibraryModel::FIELD_ANIMATIONTYPE, $category, DFC::EXACT);
}
error_reporting(E_ALL);
$animations = MdlAnimationsLibraryModel::findByFilter($pdo, $filter);

$library = array();

foreach($animations as $a) {
    $library[] = array(
        "id" => $a->getCompositionId(), 
        "thumb" => $a->getFolder()."/thumb.".($a->getAnimationType() == 10 ? "jpg" : "png"), 
        "name" => $a->getTitle(), 
        "type" => $a->getAnimationType(), 
        "loader" => $a->getLoader(),
        "folder" => "plugins_html/_common/edge_animations/" . $a->getFolder() ."/"
    );
}

$result = array("error" => "", "library" => $library);
header('Content-Type: application/json; charset=utf-8');
echo json_encode($result);
?>
