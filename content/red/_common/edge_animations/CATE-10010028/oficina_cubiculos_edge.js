/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='files/CATE-10010028/images/';

var fonts = {};


var resources = [
];
var symbols = {
"stage": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
         dom: [
         {
            id:'oficina-cubiculo-01',
            type:'image',
            rect:['0px','0','760px','465px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"oficina-cubiculo-01.jpg",'0px','0px']
         },
         {
            id:'reloj',
            type:'rect',
            rect:['261px','118px','auto','auto','auto','auto']
         },
         {
            id:'luz',
            type:'rect',
            rect:['204px','157px','auto','auto','auto','auto']
         },
         {
            id:'luzCopy',
            type:'rect',
            rect:['204px','157px','auto','auto','auto','auto']
         },
         {
            id:'luzCopy2',
            type:'rect',
            rect:['204px','157px','auto','auto','auto','auto']
         },
         {
            id:'luzCopy3',
            type:'rect',
            rect:['204px','157px','auto','auto','auto','auto']
         },
         {
            id:'luzCopy4',
            type:'rect',
            rect:['204px','157px','auto','auto','auto','auto']
         },
         {
            id:'luzCopy5',
            type:'rect',
            rect:['204px','157px','auto','auto','auto','auto']
         },
         {
            id:'luzCopy6',
            type:'rect',
            rect:['204px','157px','auto','auto','auto','auto']
         }],
         symbolInstances: [
         {
            id:'luzCopy6',
            symbolName:'luz'
         },
         {
            id:'luzCopy',
            symbolName:'luz'
         },
         {
            id:'luz',
            symbolName:'luz'
         },
         {
            id:'luzCopy2',
            symbolName:'luz'
         },
         {
            id:'luzCopy5',
            symbolName:'luz'
         },
         {
            id:'luzCopy4',
            symbolName:'luz'
         },
         {
            id:'reloj',
            symbolName:'reloj_1'
         },
         {
            id:'luzCopy3',
            symbolName:'luz'
         }
         ]
      },
   states: {
      "Base State": {
         "${_luzCopy4}": [
            ["style", "top", '51px'],
            ["transform", "scaleY", '1.61665'],
            ["style", "left", '470px'],
            ["transform", "scaleX", '1.29898']
         ],
         "${_luzCopy}": [
            ["style", "top", '98px'],
            ["transform", "scaleX", '1.29898'],
            ["transform", "scaleY", '1.61665'],
            ["style", "left", '180px']
         ],
         "${_luzCopy5}": [
            ["style", "top", '176px'],
            ["transform", "scaleX", '1.29898'],
            ["transform", "scaleY", '1.61665'],
            ["style", "left", '63px']
         ],
         "${_luzCopy6}": [
            ["transform", "scaleX", '1.29898'],
            ["style", "left", '-32px'],
            ["transform", "scaleY", '1.61665'],
            ["style", "top", '176px']
         ],
         "${_luz}": [
            ["style", "top", '98px'],
            ["transform", "scaleY", '1.61665'],
            ["style", "left", '180px'],
            ["transform", "scaleX", '1.29898']
         ],
         "${_luzCopy3}": [
            ["transform", "scaleX", '1.29898'],
            ["style", "top", '68px'],
            ["style", "left", '463px'],
            ["transform", "scaleY", '1.61665']
         ],
         "${_oficina-cubiculo-01}": [
            ["style", "height", '465px'],
            ["style", "width", '760px']
         ],
         "${_luzCopy2}": [
            ["transform", "scaleX", '1.29898'],
            ["style", "left", '451px'],
            ["transform", "scaleY", '1.61665'],
            ["style", "top", '89px']
         ],
         "${_Stage}": [
            ["color", "background-color", 'rgba(255,255,255,1)'],
            ["style", "width", '760px'],
            ["style", "height", '465px'],
            ["style", "overflow", 'hidden']
         ],
         "${_reloj}": [
            ["style", "top", '155px'],
            ["transform", "scaleY", '0.48108'],
            ["style", "overflow", 'hidden'],
            ["transform", "scaleX", '0.48108'],
            ["style", "left", '356px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 0,
         autoPlay: true,
         timeline: [
            { id: "eid45", tween: [ "style", "${_luzCopy5}", "left", '63px', { fromValue: '63px'}], position: 0, duration: 0 },
            { id: "eid32", tween: [ "style", "${_luzCopy4}", "top", '51px', { fromValue: '51px'}], position: 0, duration: 0 },
            { id: "eid17", tween: [ "style", "${_reloj}", "left", '356px', { fromValue: '356px'}], position: 0, duration: 0 },
            { id: "eid30", tween: [ "style", "${_luzCopy3}", "top", '68px', { fromValue: '68px'}], position: 0, duration: 0 },
            { id: "eid25", tween: [ "style", "${_luz}", "left", '180px', { fromValue: '180px'}], position: 0, duration: 0 },
            { id: "eid31", tween: [ "style", "${_luzCopy4}", "left", '470px', { fromValue: '470px'}], position: 0, duration: 0 },
            { id: "eid28", tween: [ "style", "${_luzCopy2}", "top", '89px', { fromValue: '89px'}], position: 0, duration: 0 },
            { id: "eid3", tween: [ "transform", "${_reloj}", "scaleX", '0.48108', { fromValue: '0.48108'}], position: 0, duration: 0 },
            { id: "eid18", tween: [ "style", "${_reloj}", "top", '155px', { fromValue: '155px'}], position: 0, duration: 0 },
            { id: "eid49", tween: [ "style", "${_luzCopy6}", "left", '-32px', { fromValue: '-32px'}], position: 0, duration: 0 },
            { id: "eid22", tween: [ "transform", "${_luz}", "scaleY", '1.61665', { fromValue: '1.61665'}], position: 0, duration: 0 },
            { id: "eid26", tween: [ "style", "${_luz}", "top", '98px', { fromValue: '98px'}], position: 0, duration: 0 },
            { id: "eid21", tween: [ "transform", "${_luz}", "scaleX", '1.29898', { fromValue: '1.29898'}], position: 0, duration: 0 },
            { id: "eid29", tween: [ "style", "${_luzCopy3}", "left", '463px', { fromValue: '463px'}], position: 0, duration: 0 },
            { id: "eid48", tween: [ "style", "${_luzCopy6}", "top", '176px', { fromValue: '176px'}], position: 0, duration: 0 },
            { id: "eid46", tween: [ "style", "${_luzCopy5}", "top", '176px', { fromValue: '176px'}], position: 0, duration: 0 },
            { id: "eid27", tween: [ "style", "${_luzCopy2}", "left", '451px', { fromValue: '451px'}], position: 0, duration: 0 },
            { id: "eid4", tween: [ "transform", "${_reloj}", "scaleY", '0.48108', { fromValue: '0.48108'}], position: 0, duration: 0 }         ]
      }
   }
},
"reloj_1": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'reloj-02',
      type: 'image',
      rect: ['0','0','251px','26px','auto','auto'],
      fill: ['rgba(0,0,0,0)',im+'reloj-02.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_reloj-02}": [
            ["style", "left", '0px']
         ],
         "${symbolSelector}": [
            ["style", "height", '25px'],
            ["style", "width", '25px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 10000,
         autoPlay: true,
         timeline: [
            { id: "eid7", tween: [ "style", "${_reloj-02}", "left", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid8", tween: [ "style", "${_reloj-02}", "left", '-25px', { fromValue: '0px'}], position: 1000, duration: 0 },
            { id: "eid9", tween: [ "style", "${_reloj-02}", "left", '-50px', { fromValue: '-25px'}], position: 2000, duration: 0 },
            { id: "eid10", tween: [ "style", "${_reloj-02}", "left", '-75px', { fromValue: '-50px'}], position: 3000, duration: 0 },
            { id: "eid11", tween: [ "style", "${_reloj-02}", "left", '-100px', { fromValue: '-75px'}], position: 4000, duration: 0 },
            { id: "eid12", tween: [ "style", "${_reloj-02}", "left", '-125px', { fromValue: '-100px'}], position: 5000, duration: 0 },
            { id: "eid13", tween: [ "style", "${_reloj-02}", "left", '-150px', { fromValue: '-125px'}], position: 6000, duration: 0 },
            { id: "eid14", tween: [ "style", "${_reloj-02}", "left", '-175px', { fromValue: '-150px'}], position: 7000, duration: 0 },
            { id: "eid15", tween: [ "style", "${_reloj-02}", "left", '-200px', { fromValue: '-175px'}], position: 8000, duration: 0 },
            { id: "eid16", tween: [ "style", "${_reloj-02}", "left", '-225px', { fromValue: '-200px'}], position: 9000, duration: 0 }         ]
      }
   }
},
"luz": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      rect: ['3px','0px','147px','115px','auto','auto'],
      id: 'luz-01',
      opacity: 0.35164835164835,
      type: 'image',
      fill: ['rgba(0,0,0,0)',im+'luz-01.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_luz-01}": [
            ["style", "top", '0px'],
            ["style", "opacity", '0.35164835164835'],
            ["style", "left", '3px']
         ],
         "${symbolSelector}": [
            ["style", "height", '126px'],
            ["style", "width", '154px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 2800,
         autoPlay: true,
         timeline: [
            { id: "eid4", tween: [ "style", "${_luz-01}", "opacity", '1', { fromValue: '0.35164836049079895'}], position: 0, duration: 2800 },
            { id: "eid3", tween: [ "style", "${_luz-01}", "left", '3px', { fromValue: '3px'}], position: 0, duration: 0 }         ]
      }
   }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "CATE-10010028");
