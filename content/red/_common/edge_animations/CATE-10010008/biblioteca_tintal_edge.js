/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='files/CATE-10010008/images/';

var fonts = {};


var resources = [
];
var symbols = {
"stage": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
         dom: [
         {
            id:'cielo-05',
            type:'image',
            rect:['0','0px','760px','330px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"cielo-05.jpg",'0px','0px']
         },
         {
            id:'avion',
            type:'rect',
            rect:['26','9','auto','auto','auto','auto']
         },
         {
            id:'nube23Copy',
            type:'rect',
            rect:['186','41','auto','auto','auto','auto']
         },
         {
            id:'nube23',
            type:'rect',
            rect:['186','41','auto','auto','auto','auto']
         },
         {
            id:'Symbol_1Copy',
            type:'rect',
            rect:['516','20','188px','105px','auto','auto']
         },
         {
            id:'nubes-04',
            type:'image',
            rect:['78px','91px','676px','126px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"nubes-04.png",'0px','0px']
         },
         {
            id:'biblioteca_el_tintalFINAL-01',
            type:'image',
            rect:['0','0','760px','465px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"biblioteca%20el%20tintalFINAL-01.png",'0px','0px']
         }],
         symbolInstances: [
         {
            id:'nube23',
            symbolName:'nube23'
         },
         {
            id:'Symbol_1Copy',
            symbolName:'Symbol_1'
         },
         {
            id:'nube23Copy',
            symbolName:'nube23'
         },
         {
            id:'avion',
            symbolName:'avion'
         }
         ]
      },
   states: {
      "Base State": {
         "${_avion}": [
            ["style", "left", '0px'],
            ["style", "top", '8px']
         ],
         "${_nube23Copy}": [
            ["style", "top", '91px'],
            ["transform", "scaleX", '0.91555'],
            ["transform", "scaleY", '0.91555'],
            ["style", "left", '124px']
         ],
         "${_nubes-04}": [
            ["style", "left", '78px'],
            ["style", "top", '91px']
         ],
         "${_cielo-05}": [
            ["style", "height", '330px'],
            ["style", "width", '760px']
         ],
         "${_Stage}": [
            ["color", "background-color", 'rgba(255,255,255,1)'],
            ["style", "width", '760px'],
            ["style", "height", '465px'],
            ["style", "overflow", 'hidden']
         ],
         "${_Symbol_1Copy}": [
            ["style", "height", '96px'],
            ["style", "top", '31px'],
            ["style", "left", '0px'],
            ["style", "width", '464px']
         ],
         "${_nube23}": [
            ["style", "top", '67px'],
            ["transform", "scaleY", '0.91555'],
            ["style", "left", '167px'],
            ["transform", "scaleX", '0.91555']
         ],
         "${_biblioteca_el_tintalFINAL-01}": [
            ["style", "height", '465px'],
            ["style", "width", '760px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 0,
         autoPlay: true,
         timeline: [
            { id: "eid110", tween: [ "style", "${_avion}", "top", '8px', { fromValue: '8px'}], position: 0, duration: 0 },
            { id: "eid72", tween: [ "style", "${_Symbol_1Copy}", "left", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid49", tween: [ "style", "${_Symbol_1Copy}", "height", '96px', { fromValue: '96px'}], position: 0, duration: 0 },
            { id: "eid109", tween: [ "style", "${_avion}", "left", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid97", tween: [ "style", "${_nube23Copy}", "left", '124px', { fromValue: '124px'}], position: 0, duration: 0 },
            { id: "eid74", tween: [ "transform", "${_nube23}", "scaleX", '0.91555', { fromValue: '0.91555'}], position: 0, duration: 0 },
            { id: "eid98", tween: [ "style", "${_nube23Copy}", "top", '91px', { fromValue: '91px'}], position: 0, duration: 0 },
            { id: "eid51", tween: [ "style", "${_Symbol_1Copy}", "width", '464px', { fromValue: '464px'}], position: 0, duration: 0 },
            { id: "eid92", tween: [ "style", "${_nube23}", "left", '167px', { fromValue: '167px'}], position: 0, duration: 0 },
            { id: "eid96", tween: [ "style", "${_nube23}", "top", '67px', { fromValue: '67px'}], position: 0, duration: 0 },
            { id: "eid75", tween: [ "transform", "${_nube23}", "scaleY", '0.91555', { fromValue: '0.91555'}], position: 0, duration: 0 },
            { id: "eid99", tween: [ "style", "${_Symbol_1Copy}", "top", '31px', { fromValue: '31px'}], position: 0, duration: 0 }         ]
      }
   }
},
"Symbol_1": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: true,
   content: {
   dom: [
   {
      id: 'nubes-032',
      type: 'image',
      rect: ['495','29','125px','65px','auto','auto'],
      fill: ['rgba(0,0,0,0)',im+'nubes-03.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_nubes-032}": [
            ["style", "top", '55px'],
            ["style", "height", '59px'],
            ["style", "opacity", '1'],
            ["style", "left", '753px'],
            ["style", "width", '114px']
         ],
         "${symbolSelector}": [
            ["style", "height", '157px'],
            ["style", "width", '760px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 62915.544521557,
         autoPlay: true,
         timeline: [
            { id: "eid33", tween: [ "style", "${_nubes-032}", "left", '-131px', { fromValue: '753px'}], position: 0, duration: 62916 },
            { id: "eid29", tween: [ "style", "${symbolSelector}", "width", '760px', { fromValue: '760px'}], position: 0, duration: 0 },
            { id: "eid94", tween: [ "style", "${_nubes-032}", "height", '59px', { fromValue: '59px'}], position: 0, duration: 0 },
            { id: "eid95", tween: [ "style", "${_nubes-032}", "width", '114px', { fromValue: '114px'}], position: 0, duration: 0 },
            { id: "eid34", tween: [ "style", "${_nubes-032}", "top", '54px', { fromValue: '55px'}], position: 0, duration: 62916 },
            { id: "eid30", tween: [ "style", "${symbolSelector}", "height", '157px', { fromValue: '157px'}], position: 0, duration: 0 },
            { id: "eid102", tween: [ "style", "${_nubes-032}", "opacity", '1', { fromValue: '1'}], position: 0, duration: 0 }         ]
      }
   }
},
"nube2": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      rect: ['0px','0px','160px','115px','auto','auto'],
      id: 'Rectangle2',
      stroke: [0,'rgb(0, 0, 0)','none'],
      type: 'rect',
      fill: ['rgba(192,192,192,1)']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${symbolSelector}": [
            ["style", "height", '115px'],
            ["style", "width", '160px']
         ],
         "${_Rectangle2}": [
            ["style", "left", '0px'],
            ["style", "top", '0px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 0,
         autoPlay: true,
         timeline: [
         ]
      }
   }
},
"nube23": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'nubes-022',
      type: 'image',
      rect: ['-89px','16px','89px','55px','auto','auto'],
      fill: ['rgba(0,0,0,0)',im+'nubes-02.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_nubes-022}": [
            ["style", "top", '16px'],
            ["style", "height", '39px'],
            ["style", "left", '-89px'],
            ["style", "width", '64px']
         ],
         "${symbolSelector}": [
            ["style", "height", '126px'],
            ["style", "width", '760px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 120000,
         autoPlay: true,
         timeline: [
            { id: "eid88", tween: [ "style", "${_nubes-022}", "height", '39px', { fromValue: '39px'}], position: 0, duration: 0 },
            { id: "eid65", tween: [ "style", "${_nubes-022}", "left", '792px', { fromValue: '-89px'}], position: 0, duration: 120000 },
            { id: "eid66", tween: [ "style", "${_nubes-022}", "top", '25px', { fromValue: '16px'}], position: 0, duration: 120000 },
            { id: "eid89", tween: [ "style", "${_nubes-022}", "width", '64px', { fromValue: '64px'}], position: 0, duration: 0 }         ]
      }
   }
},
"avion": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'avion-062',
      type: 'image',
      rect: ['-94px','58px','80px','21px','auto','auto'],
      fill: ['rgba(0,0,0,0)',im+'avion-06.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_avion-062}": [
            ["style", "height", '14px'],
            ["style", "top", '141px'],
            ["style", "left", '67px'],
            ["style", "width", '52px']
         ],
         "${symbolSelector}": [
            ["style", "height", '115px'],
            ["style", "width", '760px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 20000,
         autoPlay: true,
         timeline: [
            { id: "eid108", tween: [ "style", "${_avion-062}", "top", '-38px', { fromValue: '141px'}], position: 0, duration: 20000 },
            { id: "eid106", tween: [ "style", "${_avion-062}", "left", '476px', { fromValue: '67px'}], position: 0, duration: 20000 },
            { id: "eid112", tween: [ "style", "${_avion-062}", "width", '52px', { fromValue: '52px'}], position: 0, duration: 0 },
            { id: "eid111", tween: [ "style", "${_avion-062}", "height", '14px', { fromValue: '14px'}], position: 0, duration: 0 }         ]
      }
   }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "CATE-10010008");
