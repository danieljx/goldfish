/***********************
* Acciones de composición de Adobe Edge Animate
*
* Editar este archivo con precaución, teniendo cuidado de conservar 
* las firmas de función y los comentarios que comienzan con "Edge" para mantener la 
* capacidad de interactuar con estas acciones en Adobe Edge Animate
*
***********************/
(function($, Edge, compId){
var Composition = Edge.Composition, Symbol = Edge.Symbol; // los alias más comunes para las clases de Edge

   //Edge symbol: 'stage'
   (function(symbolName) {
      
      
   })("stage");
   //Edge symbol end:'stage'

   //=========================================================
   
   //Edge symbol: 'Symbol_1'
   (function(symbolName) {   
   
   })("Symbol_1");
   //Edge symbol end:'Symbol_1'

   //=========================================================
   
   //Edge symbol: 'nube2'
   (function(symbolName) {   
   
   })("nube2");
   //Edge symbol end:'nube2'

   //=========================================================
   
   //Edge symbol: 'nube23'
   (function(symbolName) {   
   
   })("nube23");
   //Edge symbol end:'nube23'

   //=========================================================
   
   //Edge symbol: 'avion'
   (function(symbolName) {   
   
   })("avion");
   //Edge symbol end:'avion'

})(jQuery, AdobeEdge, "CATE-10010008");