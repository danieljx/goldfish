/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='files/CATE-10010007/images/';

var fonts = {};


var resources = [
];
var symbols = {
"stage": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
         dom: [
         {
            id:'ofic1-01',
            type:'image',
            rect:['0','0','760px','465px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"ofic1-01.jpg",'0px','0px']
         },
         {
            id:'luz',
            type:'rect',
            rect:['545px','162px','auto','auto','auto','auto']
         },
         {
            id:'luzCopy2',
            type:'rect',
            rect:['545px','162px','auto','auto','auto','auto']
         },
         {
            id:'luzCopy3',
            type:'rect',
            rect:['545px','162px','auto','auto','auto','auto']
         },
         {
            id:'luzCopy5',
            type:'rect',
            rect:['545px','162px','auto','auto','auto','auto']
         },
         {
            id:'luzCopy6',
            type:'rect',
            rect:['545px','162px','auto','auto','auto','auto']
         },
         {
            id:'luzCopy',
            type:'rect',
            rect:['545px','162px','auto','auto','auto','auto']
         },
         {
            id:'luzCopy4',
            type:'rect',
            rect:['545px','162px','auto','auto','auto','auto']
         }],
         symbolInstances: [
         {
            id:'luzCopy5',
            symbolName:'luz'
         },
         {
            id:'luz',
            symbolName:'luz'
         },
         {
            id:'luzCopy2',
            symbolName:'luz'
         },
         {
            id:'luzCopy4',
            symbolName:'luz'
         },
         {
            id:'luzCopy6',
            symbolName:'luz'
         },
         {
            id:'luzCopy3',
            symbolName:'luz'
         },
         {
            id:'luzCopy',
            symbolName:'luz'
         }
         ]
      },
   states: {
      "Base State": {
         "${_ofic1-01}": [
            ["style", "height", '465px'],
            ["style", "width", '760px']
         ],
         "${_luzCopy4}": [
            ["transform", "scaleX", '2.38817'],
            ["style", "left", '491px'],
            ["transform", "scaleY", '2.38817'],
            ["style", "top", '92px']
         ],
         "${_luzCopy2}": [
            ["style", "top", '48px'],
            ["transform", "scaleX", '2.11111'],
            ["transform", "scaleY", '2.11111'],
            ["style", "left", '106px']
         ],
         "${_luzCopy5}": [
            ["transform", "scaleX", '2.11111'],
            ["style", "top", '355px'],
            ["style", "left", '135px'],
            ["transform", "scaleY", '1.00794']
         ],
         "${_luzCopy3}": [
            ["transform", "scaleX", '2.11111'],
            ["style", "left", '106px'],
            ["transform", "scaleY", '2.11111'],
            ["style", "top", '41px']
         ],
         "${_Stage}": [
            ["color", "background-color", 'rgba(255,255,255,1)'],
            ["style", "width", '760px'],
            ["style", "height", '465px'],
            ["style", "overflow", 'hidden']
         ],
         "${_luzCopy6}": [
            ["style", "top", '355px'],
            ["transform", "scaleY", '1.00794'],
            ["style", "left", '491px'],
            ["transform", "scaleX", '2.11111']
         ],
         "${_luz}": [
            ["style", "top", '65px'],
            ["transform", "scaleY", '1.42063'],
            ["style", "left", '491px'],
            ["transform", "scaleX", '1.42063']
         ],
         "${_luzCopy}": [
            ["style", "top", '58px'],
            ["transform", "scaleX", '2.38817'],
            ["transform", "scaleY", '2.38817'],
            ["style", "left", '491px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 0,
         autoPlay: true,
         timeline: [
            { id: "eid47", tween: [ "transform", "${_luzCopy5}", "scaleY", '1.00794', { fromValue: '1.00794'}], position: 0, duration: 0 },
            { id: "eid49", tween: [ "style", "${_luzCopy5}", "left", '135px', { fromValue: '135px'}], position: 0, duration: 0 },
            { id: "eid40", tween: [ "style", "${_luzCopy4}", "top", '92px', { fromValue: '92px'}], position: 0, duration: 0 },
            { id: "eid50", tween: [ "style", "${_luzCopy5}", "top", '355px', { fromValue: '355px'}], position: 0, duration: 0 },
            { id: "eid37", tween: [ "style", "${_luzCopy3}", "top", '41px', { fromValue: '41px'}], position: 0, duration: 0 },
            { id: "eid9", tween: [ "style", "${_luz}", "left", '491px', { fromValue: '491px'}], position: 0, duration: 0 },
            { id: "eid33", tween: [ "transform", "${_luzCopy2}", "scaleY", '2.11111', { fromValue: '2.11111'}], position: 0, duration: 0 },
            { id: "eid21", tween: [ "transform", "${_luzCopy}", "scaleY", '2.38817', { fromValue: '2.38817'}], position: 0, duration: 0 },
            { id: "eid39", tween: [ "style", "${_luzCopy2}", "top", '48px', { fromValue: '48px'}], position: 0, duration: 0 },
            { id: "eid25", tween: [ "style", "${_luzCopy}", "top", '58px', { fromValue: '58px'}], position: 0, duration: 0 },
            { id: "eid38", tween: [ "style", "${_luzCopy2}", "left", '106px', { fromValue: '106px'}], position: 0, duration: 0 },
            { id: "eid20", tween: [ "transform", "${_luzCopy}", "scaleX", '2.38817', { fromValue: '2.38817'}], position: 0, duration: 0 },
            { id: "eid24", tween: [ "style", "${_luzCopy}", "left", '491px', { fromValue: '491px'}], position: 0, duration: 0 },
            { id: "eid51", tween: [ "style", "${_luzCopy6}", "left", '491px', { fromValue: '491px'}], position: 0, duration: 0 },
            { id: "eid6", tween: [ "transform", "${_luz}", "scaleY", '1.42063', { fromValue: '1.42063'}], position: 0, duration: 0 },
            { id: "eid10", tween: [ "style", "${_luz}", "top", '65px', { fromValue: '65px'}], position: 0, duration: 0 },
            { id: "eid5", tween: [ "transform", "${_luz}", "scaleX", '1.42063', { fromValue: '1.42063'}], position: 0, duration: 0 },
            { id: "eid36", tween: [ "style", "${_luzCopy3}", "left", '106px', { fromValue: '106px'}], position: 0, duration: 0 },
            { id: "eid32", tween: [ "transform", "${_luzCopy2}", "scaleX", '2.11111', { fromValue: '2.11111'}], position: 0, duration: 0 },
            { id: "eid29", tween: [ "transform", "${_luzCopy3}", "scaleY", '2.11111', { fromValue: '2.11111'}], position: 0, duration: 0 },
            { id: "eid28", tween: [ "transform", "${_luzCopy3}", "scaleX", '2.11111', { fromValue: '2.11111'}], position: 0, duration: 0 }         ]
      }
   }
},
"luz": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      rect: ['3px','0px','147px','115px','auto','auto'],
      id: 'luz-01',
      opacity: 0.35164835164835,
      type: 'image',
      fill: ['rgba(0,0,0,0)',im+'luz-01.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_luz-01}": [
            ["style", "top", '0px'],
            ["style", "opacity", '0.74725274725275'],
            ["style", "left", '3px']
         ],
         "${symbolSelector}": [
            ["style", "height", '126px'],
            ["style", "width", '154px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 1000,
         autoPlay: true,
         timeline: [
            { id: "eid4", tween: [ "style", "${_luz-01}", "opacity", '1', { fromValue: '0.74725274725275'}], position: 0, duration: 1000 },
            { id: "eid3", tween: [ "style", "${_luz-01}", "left", '3px', { fromValue: '3px'}], position: 0, duration: 0 }         ]
      }
   }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "CATE-10010007");
