/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='files/CATE-10010027/images/';

var fonts = {};


var resources = [
];
var symbols = {
"stage": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
         dom: [
         {
            id:'ofice-impresora-01',
            type:'image',
            rect:['0','0','760px','469px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"ofice-impresora-01.jpg",'0px','0px']
         },
         {
            id:'luz',
            type:'rect',
            rect:['197px','157px','auto','auto','auto','auto']
         },
         {
            id:'luzCopy',
            type:'rect',
            rect:['204px','157px','auto','auto','auto','auto']
         },
         {
            id:'luzCopy2',
            type:'rect',
            rect:['351px','172px','auto','auto','auto','auto']
         },
         {
            id:'reloj',
            type:'rect',
            rect:['261px','118px','auto','auto','auto','auto']
         }],
         symbolInstances: [
         {
            id:'reloj',
            symbolName:'reloj_1'
         },
         {
            id:'luzCopy',
            symbolName:'luz'
         },
         {
            id:'luz',
            symbolName:'luz'
         },
         {
            id:'luzCopy2',
            symbolName:'luz'
         }
         ]
      },
   states: {
      "Base State": {
         "${_ofice-impresora-01}": [
            ["style", "height", '469px'],
            ["style", "width", '760px']
         ],
         "${_luz}": [
            ["style", "left", '197px'],
            ["style", "top", '157px']
         ],
         "${_Stage}": [
            ["color", "background-color", 'rgba(255,255,255,1)'],
            ["style", "width", '760px'],
            ["style", "height", '465px'],
            ["style", "overflow", 'hidden']
         ],
         "${_reloj}": [
            ["style", "top", '118px'],
            ["style", "left", '261px'],
            ["style", "overflow", 'hidden']
         ],
         "${_luzCopy}": [
            ["style", "left", '204px'],
            ["style", "top", '157px']
         ],
         "${_luzCopy2}": [
            ["style", "left", '351px'],
            ["style", "top", '172px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 0,
         autoPlay: true,
         timeline: [
         ]
      }
   }
},
"reloj": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'ofice-impresora-023',
      type: 'image',
      rect: ['0','0','1501px','151px','auto','auto'],
      fill: ['rgba(0,0,0,0)',im+'ofice-impresora-02.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_ofice-impresora-023}": [
            ["style", "left", '0px']
         ],
         "${symbolSelector}": [
            ["style", "height", '151px'],
            ["style", "width", '151px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 3700,
         autoPlay: true,
         timeline: [
            { id: "eid24", tween: [ "style", "${_ofice-impresora-023}", "left", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid25", tween: [ "style", "${_ofice-impresora-023}", "left", '-150px', { fromValue: '0px'}], position: 400, duration: 0 },
            { id: "eid26", tween: [ "style", "${_ofice-impresora-023}", "left", '-300px', { fromValue: '-150px'}], position: 800, duration: 0 },
            { id: "eid27", tween: [ "style", "${_ofice-impresora-023}", "left", '-450px', { fromValue: '-300px'}], position: 1200, duration: 0 },
            { id: "eid28", tween: [ "style", "${_ofice-impresora-023}", "left", '-600px', { fromValue: '-450px'}], position: 1600, duration: 0 },
            { id: "eid29", tween: [ "style", "${_ofice-impresora-023}", "left", '-750px', { fromValue: '-600px'}], position: 2000, duration: 0 },
            { id: "eid30", tween: [ "style", "${_ofice-impresora-023}", "left", '-900px', { fromValue: '-750px'}], position: 2400, duration: 0 },
            { id: "eid31", tween: [ "style", "${_ofice-impresora-023}", "left", '-1050px', { fromValue: '-900px'}], position: 2800, duration: 0 },
            { id: "eid32", tween: [ "style", "${_ofice-impresora-023}", "left", '-1200px', { fromValue: '-1050px'}], position: 3200, duration: 0 },
            { id: "eid33", tween: [ "style", "${_ofice-impresora-023}", "left", '-1350px', { fromValue: '-1200px'}], position: 3700, duration: 0 }         ]
      }
   }
},
"luz": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      type: 'image',
      id: 'luz-01',
      opacity: 0.35164835164835,
      rect: ['3px','0px','147px','115px','auto','auto'],
      fill: ['rgba(0,0,0,0)',im+'luz-01.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${symbolSelector}": [
            ["style", "height", '126px'],
            ["style", "width", '154px']
         ],
         "${_luz-01}": [
            ["style", "top", '0px'],
            ["style", "opacity", '0.35164835164835'],
            ["style", "left", '3px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 2800,
         autoPlay: true,
         timeline: [
            { id: "eid3", tween: [ "style", "${_luz-01}", "left", '3px', { fromValue: '3px'}], position: 0, duration: 0 },
            { id: "eid4", tween: [ "style", "${_luz-01}", "opacity", '1', { fromValue: '0.35164836049079895'}], position: 0, duration: 2800 }         ]
      }
   }
},
"reloj_1": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'reloj-02',
      type: 'image',
      rect: ['0','0','251px','26px','auto','auto'],
      fill: ['rgba(0,0,0,0)',im+'reloj-02.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_reloj-02}": [
            ["style", "left", '0px']
         ],
         "${symbolSelector}": [
            ["style", "height", '25px'],
            ["style", "width", '25px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 10000,
         autoPlay: true,
         timeline: [
            { id: "eid7", tween: [ "style", "${_reloj-02}", "left", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid8", tween: [ "style", "${_reloj-02}", "left", '-25px', { fromValue: '0px'}], position: 1000, duration: 0 },
            { id: "eid9", tween: [ "style", "${_reloj-02}", "left", '-50px', { fromValue: '-25px'}], position: 2000, duration: 0 },
            { id: "eid10", tween: [ "style", "${_reloj-02}", "left", '-75px', { fromValue: '-50px'}], position: 3000, duration: 0 },
            { id: "eid11", tween: [ "style", "${_reloj-02}", "left", '-100px', { fromValue: '-75px'}], position: 4000, duration: 0 },
            { id: "eid12", tween: [ "style", "${_reloj-02}", "left", '-125px', { fromValue: '-100px'}], position: 5000, duration: 0 },
            { id: "eid13", tween: [ "style", "${_reloj-02}", "left", '-150px', { fromValue: '-125px'}], position: 6000, duration: 0 },
            { id: "eid14", tween: [ "style", "${_reloj-02}", "left", '-175px', { fromValue: '-150px'}], position: 7000, duration: 0 },
            { id: "eid15", tween: [ "style", "${_reloj-02}", "left", '-200px', { fromValue: '-175px'}], position: 8000, duration: 0 },
            { id: "eid16", tween: [ "style", "${_reloj-02}", "left", '-225px', { fromValue: '-200px'}], position: 9000, duration: 0 }         ]
      }
   }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "CATE-10010027");
