/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='files/CATE-10010030/images/';

var fonts = {};


var resources = [
];
var symbols = {
"stage": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
         dom: [
         {
            id:'parque_infantil-01',
            type:'image',
            rect:['0','0','760px','465px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"parque_infantil-01.jpg",'0px','0px']
         },
         {
            id:'aves',
            type:'rect',
            rect:['79px','-26px','auto','auto','auto','auto'],
            transform:[[],[],[],['0.1609','0.1609']]
         },
         {
            id:'avesCopy',
            type:'rect',
            rect:['79px','-26px','auto','auto','auto','auto'],
            transform:[[],[],[],['0.1609','0.1609']]
         },
         {
            id:'arbol-01',
            type:'image',
            rect:['0px','0px','760px','465px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"arbol-01.png",'0px','0px']
         },
         {
            id:'culombio',
            type:'rect',
            rect:['479px','159px','auto','auto','auto','auto']
         },
         {
            id:'hojaok',
            type:'rect',
            rect:['485','0','auto','auto','auto','auto']
         },
         {
            id:'hojaokCopy7',
            type:'rect',
            rect:['485','0','auto','auto','auto','auto']
         },
         {
            id:'hojaokCopy5',
            type:'rect',
            rect:['485','0','auto','auto','auto','auto']
         }],
         symbolInstances: [
         {
            id:'avesCopy',
            symbolName:'aves'
         },
         {
            id:'hojaokCopy5',
            symbolName:'hojaok'
         },
         {
            id:'culombio',
            symbolName:'culombio'
         },
         {
            id:'hojaok',
            symbolName:'hojaok'
         },
         {
            id:'hojaokCopy7',
            symbolName:'hojaok'
         },
         {
            id:'aves',
            symbolName:'aves'
         }
         ]
      },
   states: {
      "Base State": {
         "${_hojaokCopy7}": [
            ["style", "left", '66px'],
            ["style", "top", '-102px']
         ],
         "${_parque_infantil-01}": [
            ["style", "height", '465px'],
            ["style", "width", '760px']
         ],
         "${_hojaok}": [
            ["style", "left", '561px'],
            ["style", "top", '-157px']
         ],
         "${_avesCopy}": [
            ["transform", "scaleX", '0.07736'],
            ["style", "top", '-41px'],
            ["style", "left", '-49px'],
            ["transform", "scaleY", '0.07736']
         ],
         "${_hojaokCopy5}": [
            ["style", "left", '423px'],
            ["style", "top", '-194px']
         ],
         "${_Stage}": [
            ["color", "background-color", 'rgba(255,255,255,1)'],
            ["style", "width", '760px'],
            ["style", "height", '465px'],
            ["style", "overflow", 'hidden']
         ],
         "${_arbol-01}": [
            ["style", "height", '465px'],
            ["style", "width", '760px']
         ],
         "${_aves}": [
            ["transform", "scaleX", '0.07736'],
            ["style", "left", '-87px'],
            ["transform", "scaleY", '0.07736'],
            ["style", "top", '-15px']
         ],
         "${_culombio}": [
            ["transform", "scaleX", '0.72376'],
            ["style", "left", '478px'],
            ["transform", "scaleY", '0.72376'],
            ["style", "top", '121px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 45725.2777572,
         autoPlay: true,
         timeline: [
            { id: "eid556", tween: [ "style", "${_avesCopy}", "left", '282px', { fromValue: '-49px'}], position: 0, duration: 28923 },
            { id: "eid558", tween: [ "style", "${_aves}", "left", '403px', { fromValue: '-87px'}], position: 0, duration: 30000 },
            { id: "eid675", tween: [ "style", "${_aves}", "left", '741px', { fromValue: '403px'}], position: 30000, duration: 5000 },
            { id: "eid642", tween: [ "style", "${_hojaok}", "top", '-157px', { fromValue: '-157px'}], position: 0, duration: 0 },
            { id: "eid664", tween: [ "style", "${_hojaokCopy7}", "top", '-102px', { fromValue: '-102px'}], position: 45725, duration: 0 },
            { id: "eid662", tween: [ "style", "${_hojaokCopy7}", "left", '561px', { fromValue: '66px'}], position: 45725, duration: 0 },
            { id: "eid631", tween: [ "style", "${_hojaok}", "left", '561px', { fromValue: '561px'}], position: 10000, duration: 0 },
            { id: "eid632", tween: [ "style", "${_hojaok}", "left", '561px', { fromValue: '561px'}], position: 32500, duration: 0 },
            { id: "eid559", tween: [ "style", "${_aves}", "top", '-111px', { fromValue: '-15px'}], position: 0, duration: 30000 },
            { id: "eid676", tween: [ "style", "${_aves}", "top", '-8px', { fromValue: '-111px'}], position: 30000, duration: 5000 },
            { id: "eid9", tween: [ "style", "${_culombio}", "left", '478px', { fromValue: '478px'}], position: 0, duration: 0 },
            { id: "eid6", tween: [ "transform", "${_culombio}", "scaleY", '0.72376', { fromValue: '0.72376'}], position: 0, duration: 0 },
            { id: "eid5", tween: [ "transform", "${_culombio}", "scaleX", '0.72376', { fromValue: '0.72376'}], position: 0, duration: 0 },
            { id: "eid633", tween: [ "style", "${_hojaokCopy5}", "left", '423px', { fromValue: '423px'}], position: 2500, duration: 0 },
            { id: "eid630", tween: [ "style", "${_hojaokCopy5}", "left", '423px', { fromValue: '423px'}], position: 25000, duration: 0 },
            { id: "eid10", tween: [ "style", "${_culombio}", "top", '121px', { fromValue: '121px'}], position: 0, duration: 0 },
            { id: "eid557", tween: [ "style", "${_avesCopy}", "top", '-124px', { fromValue: '-41px'}], position: 0, duration: 28923 }         ]
      }
   }
},
"culombio": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'culombio-02',
      type: 'image',
      rect: ['0','0','87px','181px','auto','auto'],
      fill: ['rgba(0,0,0,0)',im+'culombio-02.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_culombio-02}": [
            ["style", "-webkit-transform-origin", [50,0], {valueTemplate:'@@0@@% @@1@@%'} ],
            ["style", "-moz-transform-origin", [50,0],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "-ms-transform-origin", [50,0],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "msTransformOrigin", [50,0],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "-o-transform-origin", [50,0],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "left", '0px'],
            ["transform", "rotateZ", '0deg']
         ],
         "${symbolSelector}": [
            ["style", "height", '181px'],
            ["style", "width", '96px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 1400,
         autoPlay: true,
         timeline: [
            { id: "eid13", tween: [ "transform", "${_culombio-02}", "rotateZ", '2deg', { fromValue: '0deg'}], position: 0, duration: 1400 },
            { id: "eid1", tween: [ "style", "${_culombio-02}", "left", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid12", tween: [ "style", "${_culombio-02}", "-webkit-transform-origin", [50,0], { valueTemplate: '@@0@@% @@1@@%', fromValue: [50,0]}], position: 1400, duration: 0 },
            { id: "eid713", tween: [ "style", "${_culombio-02}", "-moz-transform-origin", [50,0], { valueTemplate: '@@0@@% @@1@@%', fromValue: [50,0]}], position: 1400, duration: 0 },
            { id: "eid714", tween: [ "style", "${_culombio-02}", "-ms-transform-origin", [50,0], { valueTemplate: '@@0@@% @@1@@%', fromValue: [50,0]}], position: 1400, duration: 0 },
            { id: "eid715", tween: [ "style", "${_culombio-02}", "msTransformOrigin", [50,0], { valueTemplate: '@@0@@% @@1@@%', fromValue: [50,0]}], position: 1400, duration: 0 },
            { id: "eid716", tween: [ "style", "${_culombio-02}", "-o-transform-origin", [50,0], { valueTemplate: '@@0@@% @@1@@%', fromValue: [50,0]}], position: 1400, duration: 0 }         ]
      }
   }
},
"hoja": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'hoja-03',
      type: 'image',
      rect: ['0','0','69px','36px','auto','auto'],
      fill: ['rgba(0,0,0,0)',im+'hoja-03.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_hoja-03}": [
            ["style", "top", '-4px'],
            ["transform", "rotateZ", '1deg'],
            ["style", "height", '36px'],
            ["style", "opacity", '0'],
            ["style", "left", '3px'],
            ["style", "width", '69px']
         ],
         "${symbolSelector}": [
            ["style", "height", '497px'],
            ["style", "width", '491px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 71000,
         autoPlay: true,
         timeline: [
            { id: "eid253", tween: [ "transform", "${_hoja-03}", "rotateZ", '1deg', { fromValue: '1deg'}], position: 0, duration: 0 },
            { id: "eid255", tween: [ "style", "${_hoja-03}", "top", '447px', { fromValue: '-4px'}], position: 0, duration: 71000 },
            { id: "eid254", tween: [ "style", "${_hoja-03}", "left", '7px', { fromValue: '3px'}], position: 0, duration: 71000 },
            { id: "eid257", tween: [ "style", "${_hoja-03}", "opacity", '0.76829268292683', { fromValue: '0'}], position: 0, duration: 9000 },
            { id: "eid258", tween: [ "style", "${_hoja-03}", "opacity", '0.743902779207', { fromValue: '0.768293023109436'}], position: 9000, duration: 53000 },
            { id: "eid259", tween: [ "style", "${_hoja-03}", "opacity", '0', { fromValue: '0.7439029812812805'}], position: 62000, duration: 9000 }         ]
      }
   }
},
"hojaok": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'hoja-032',
      type: 'image',
      rect: ['0','0','27px','14px','auto','auto'],
      fill: ['rgba(0,0,0,0)',im+'hoja-032.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_hoja-032}": [
            ["transform", "rotateZ", '15deg'],
            ["style", "opacity", '0.92073170731707'],
            ["style", "left", '2px'],
            ["style", "width", '27px'],
            ["style", "top", '4px'],
            ["style", "height", '14px']
         ],
         "${symbolSelector}": [
            ["style", "height", '440px'],
            ["style", "width", '112px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 19000,
         autoPlay: true,
         timeline: [
            { id: "eid568", tween: [ "style", "${_hoja-032}", "left", '78px', { fromValue: '2px'}], position: 0, duration: 3886 },
            { id: "eid571", tween: [ "style", "${_hoja-032}", "left", '6px', { fromValue: '78px'}], position: 3886, duration: 5614 },
            { id: "eid572", tween: [ "style", "${_hoja-032}", "left", '76px', { fromValue: '6px'}], position: 9500, duration: 6909 },
            { id: "eid574", tween: [ "style", "${_hoja-032}", "left", '-14px', { fromValue: '76px'}], position: 16409, duration: 2591 },
            { id: "eid569", tween: [ "style", "${_hoja-032}", "top", '152px', { fromValue: '4px'}], position: 0, duration: 3886 },
            { id: "eid570", tween: [ "style", "${_hoja-032}", "top", '252px', { fromValue: '152px'}], position: 3886, duration: 5614 },
            { id: "eid573", tween: [ "style", "${_hoja-032}", "top", '385px', { fromValue: '252px'}], position: 9500, duration: 6909 },
            { id: "eid575", tween: [ "style", "${_hoja-032}", "top", '443px', { fromValue: '385px'}], position: 16409, duration: 2591 },
            { id: "eid578", tween: [ "style", "${_hoja-032}", "opacity", '0', { fromValue: '0.92073170731707'}], position: 16409, duration: 2591 },
            { id: "eid589", tween: [ "transform", "${_hoja-032}", "rotateZ", '-32deg', { fromValue: '15deg'}], position: 0, duration: 3886 },
            { id: "eid590", tween: [ "transform", "${_hoja-032}", "rotateZ", '17deg', { fromValue: '-32deg'}], position: 3886, duration: 5614 },
            { id: "eid591", tween: [ "transform", "${_hoja-032}", "rotateZ", '-18deg', { fromValue: '17deg'}], position: 9500, duration: 6909 }         ]
      }
   }
},
"aves": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'pajaro-01',
      type: 'image',
      rect: ['0','0','600px','127px','auto','auto'],
      fill: ['rgba(0,0,0,0)',im+'pajaro-01.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_pajaro-01}": [
            ["style", "left", '0px']
         ],
         "${symbolSelector}": [
            ["style", "height", '169px'],
            ["style", "overflow", 'hidden'],
            ["style", "width", '152px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 2600,
         autoPlay: true,
         timeline: [
            { id: "eid1", tween: [ "style", "${_pajaro-01}", "left", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid2", tween: [ "style", "${_pajaro-01}", "left", '-150px', { fromValue: '0px'}], position: 100, duration: 0 },
            { id: "eid4", tween: [ "style", "${_pajaro-01}", "left", '-300px', { fromValue: '-150px'}], position: 200, duration: 0 },
            { id: "eid5", tween: [ "style", "${_pajaro-01}", "left", '-450px', { fromValue: '-300px'}], position: 300, duration: 0 }         ]
      }
   }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "CATE-10010030");
