/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='files/CATE-10010025/images/';

var fonts = {};


var resources = [
];
var symbols = {
"stage": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
         dom: [
         {
            id:'ejecutivo_5-01',
            type:'image',
            rect:['0','0','760px','479px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"ejecutivo_5-01.jpg",'0px','0px']
         },
         {
            id:'luz',
            type:'rect',
            rect:['380px','43px','auto','auto','auto','auto']
         },
         {
            id:'luzCopy',
            type:'rect',
            rect:['380px','43px','auto','auto','auto','auto']
         },
         {
            id:'luzCopy2',
            type:'rect',
            rect:['380px','43px','auto','auto','auto','auto']
         },
         {
            id:'luzCopy3',
            type:'rect',
            rect:['380px','43px','auto','auto','auto','auto']
         },
         {
            id:'luzCopy4',
            type:'rect',
            rect:['380px','43px','auto','auto','auto','auto']
         },
         {
            id:'luzCopy5',
            type:'rect',
            rect:['380px','43px','auto','auto','auto','auto']
         },
         {
            id:'luzCopy6',
            type:'rect',
            rect:['380px','43px','auto','auto','auto','auto']
         }],
         symbolInstances: [
         {
            id:'luzCopy',
            symbolName:'luz'
         },
         {
            id:'luz',
            symbolName:'luz'
         },
         {
            id:'luzCopy2',
            symbolName:'luz'
         },
         {
            id:'luzCopy6',
            symbolName:'luz'
         },
         {
            id:'luzCopy4',
            symbolName:'luz'
         },
         {
            id:'luzCopy3',
            symbolName:'luz'
         },
         {
            id:'luzCopy5',
            symbolName:'luz'
         }
         ]
      },
   states: {
      "Base State": {
         "${_luzCopy4}": [
            ["style", "top", '-10px'],
            ["transform", "scaleY", '1.47919'],
            ["style", "left", '160px'],
            ["transform", "scaleX", '1.47919']
         ],
         "${_luzCopy2}": [
            ["transform", "scaleX", '1.47919'],
            ["style", "left", '160px'],
            ["transform", "scaleY", '1.47919'],
            ["style", "top", '-10px']
         ],
         "${_luzCopy5}": [
            ["style", "top", '176px'],
            ["transform", "scaleX", '1.47919'],
            ["transform", "scaleY", '4.39923'],
            ["style", "left", '160px']
         ],
         "${_luzCopy3}": [
            ["transform", "scaleX", '1.47919'],
            ["style", "top", '-10px'],
            ["style", "left", '160px'],
            ["transform", "scaleY", '1.47919']
         ],
         "${_ejecutivo_5-01}": [
            ["style", "height", '479px'],
            ["style", "width", '760px']
         ],
         "${_Stage}": [
            ["color", "background-color", 'rgba(255,255,255,1)'],
            ["style", "width", '760px'],
            ["style", "height", '465px'],
            ["style", "overflow", 'hidden']
         ],
         "${_luzCopy6}": [
            ["transform", "scaleX", '1.47919'],
            ["style", "left", '507px'],
            ["transform", "scaleY", '4.39923'],
            ["style", "top", '176px']
         ],
         "${_luz}": [
            ["style", "top", '-10px'],
            ["transform", "scaleY", '1.47919'],
            ["style", "left", '498px'],
            ["transform", "scaleX", '1.47919']
         ],
         "${_luzCopy}": [
            ["style", "top", '-10px'],
            ["transform", "scaleX", '1.47919'],
            ["transform", "scaleY", '1.47919'],
            ["style", "left", '498px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 0,
         autoPlay: true,
         timeline: [
            { id: "eid16", tween: [ "transform", "${_luzCopy5}", "scaleY", '4.39923', { fromValue: '4.39923'}], position: 0, duration: 0 },
            { id: "eid15", tween: [ "style", "${_luzCopy5}", "left", '160px', { fromValue: '160px'}], position: 0, duration: 0 },
            { id: "eid19", tween: [ "style", "${_luzCopy6}", "left", '507px', { fromValue: '507px'}], position: 0, duration: 0 },
            { id: "eid6", tween: [ "transform", "${_luz}", "scaleY", '1.47919', { fromValue: '1.47919'}], position: 0, duration: 0 },
            { id: "eid10", tween: [ "style", "${_luz}", "top", '-10px', { fromValue: '-10px'}], position: 0, duration: 0 },
            { id: "eid9", tween: [ "style", "${_luz}", "left", '498px', { fromValue: '498px'}], position: 0, duration: 0 },
            { id: "eid5", tween: [ "transform", "${_luz}", "scaleX", '1.47919', { fromValue: '1.47919'}], position: 0, duration: 0 },
            { id: "eid11", tween: [ "style", "${_luzCopy2}", "left", '160px', { fromValue: '160px'}], position: 0, duration: 0 },
            { id: "eid18", tween: [ "style", "${_luzCopy5}", "top", '176px', { fromValue: '176px'}], position: 0, duration: 0 }         ]
      }
   }
},
"luz": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      rect: ['3px','0px','147px','115px','auto','auto'],
      id: 'luz-01',
      opacity: 0.35164835164835,
      type: 'image',
      fill: ['rgba(0,0,0,0)',im+'luz-01.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_luz-01}": [
            ["style", "top", '0px'],
            ["style", "opacity", '0.35164835164835'],
            ["style", "left", '3px']
         ],
         "${symbolSelector}": [
            ["style", "height", '126px'],
            ["style", "width", '154px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 2800,
         autoPlay: true,
         timeline: [
            { id: "eid3", tween: [ "style", "${_luz-01}", "left", '3px', { fromValue: '3px'}], position: 0, duration: 0 },
            { id: "eid4", tween: [ "style", "${_luz-01}", "opacity", '1', { fromValue: '0.35164836049079895'}], position: 0, duration: 2800 }         ]
      }
   }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "CATE-10010025");
