/***********************
* Acciones de composición de Adobe Edge Animate
*
* Editar este archivo con precaución, teniendo cuidado de conservar 
* las firmas de función y los comentarios que comienzan con "Edge" para mantener la 
* capacidad de interactuar con estas acciones en Adobe Edge Animate
*
***********************/
(function($, Edge, compId){
var Composition = Edge.Composition, Symbol = Edge.Symbol; // los alias más comunes para las clases de Edge

   //Edge symbol: 'stage'
   (function(symbolName) {
      
      
   })("stage");
   //Edge symbol end:'stage'

   //=========================================================
   
   //Edge symbol: 'nubes'
   (function(symbolName) {   
   
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 0, function(sym, e) {
         sym.play();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 172500, function(sym, e) {
         sym.playReverse();

      });
      //Edge binding end

   })("nubes");
   //Edge symbol end:'nubes'

   //=========================================================
   
   //Edge symbol: 'avion'
   (function(symbolName) {   
   
   })("avion");
   //Edge symbol end:'avion'

   //=========================================================
   
   //Edge symbol: 'paloma'
   (function(symbolName) {   
   
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 0, function(sym, e) {
         sym.play();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 700, function(sym, e) {
         sym.play();

      });
      //Edge binding end

   })("paloma");
   //Edge symbol end:'paloma'

})(jQuery, AdobeEdge, "EDGE-12738160");