/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='images/';

var fonts = {};


var resources = [
];
var symbols = {
"stage": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
         dom: [
         {
            id:'cielo-01',
            type:'image',
            rect:['0','0','760px','446px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"cielo-01.png",'0px','0px']
         },
         {
            id:'nubes',
            type:'rect',
            rect:['0','0','auto','auto','auto','auto']
         },
         {
            id:'palomaCopy',
            type:'rect',
            rect:['676px','95px','auto','auto','auto','auto'],
            transform:[[],[],[],['0.19119','0.19119']]
         },
         {
            id:'cityok-01',
            type:'image',
            rect:['0','0','760px','465px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"cityok-01.png",'0px','0px']
         },
         {
            id:'paloma',
            type:'rect',
            rect:['676px','95px','auto','auto','auto','auto'],
            transform:[[],[],[],['0.19119','0.19119']]
         }],
         symbolInstances: [
         {
            id:'palomaCopy',
            symbolName:'paloma'
         },
         {
            id:'nubes',
            symbolName:'nubes'
         },
         {
            id:'paloma',
            symbolName:'paloma'
         }
         ]
      },
   states: {
      "Base State": {
         "${_palomaCopy}": [
            ["style", "top", '190px'],
            ["transform", "scaleY", '0.19235'],
            ["style", "overflow", 'hidden'],
            ["transform", "scaleX", '-0.24702'],
            ["style", "left", '-95px'],
            ["transform", "rotateZ", '7deg']
         ],
         "${_paloma}": [
            ["style", "top", '130px'],
            ["transform", "scaleY", '0.25512'],
            ["style", "overflow", 'hidden'],
            ["transform", "scaleX", '-0.27159'],
            ["style", "left", '-85px'],
            ["transform", "rotateZ", '7deg']
         ],
         "${_cielo-01}": [
            ["style", "height", '446px'],
            ["style", "width", '760px']
         ],
         "${_Stage}": [
            ["color", "background-color", 'rgba(255,255,255,1)'],
            ["style", "overflow", 'hidden'],
            ["style", "height", '465px'],
            ["style", "width", '760px']
         ],
         "${_cityok-01}": [
            ["style", "height", '465px'],
            ["style", "width", '760px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 30000,
         autoPlay: true,
         timeline: [
            { id: "eid22", tween: [ "style", "${_paloma}", "top", '-60px', { fromValue: '130px'}], position: 0, duration: 30000 },
            { id: "eid35", tween: [ "style", "${_palomaCopy}", "left", '623px', { fromValue: '-95px'}], position: 0, duration: 30000 },
            { id: "eid36", tween: [ "style", "${_palomaCopy}", "top", '-78px', { fromValue: '190px'}], position: 0, duration: 30000 },
            { id: "eid32", tween: [ "transform", "${_palomaCopy}", "scaleY", '0.19235', { fromValue: '0.19235'}], position: 0, duration: 0 },
            { id: "eid20", tween: [ "style", "${_paloma}", "left", '584px', { fromValue: '-85px'}], position: 0, duration: 30000 },
            { id: "eid37", tween: [ "transform", "${_palomaCopy}", "scaleX", '-0.24702', { fromValue: '-0.24702'}], position: 0, duration: 0 }         ]
      }
   }
},
"nubes": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'nube-02',
      type: 'image',
      rect: ['0','0','746px','332px','auto','auto'],
      fill: ['rgba(0,0,0,0)','images/nube-02.png','0px','0px']
   },
   {
      id: 'nube-03',
      type: 'image',
      rect: ['460px','-47px','493px','158px','auto','auto'],
      fill: ['rgba(0,0,0,0)','images/nube-03.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_nube-03}": [
            ["style", "top", '-110px'],
            ["style", "height", '158px'],
            ["style", "left", '262px'],
            ["style", "width", '493px']
         ],
         "${_nube-02}": [
            ["style", "height", '463px'],
            ["style", "left", '0px'],
            ["style", "width", '1040px']
         ],
         "${symbolSelector}": [
            ["style", "height", '380px'],
            ["style", "width", '760px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 175000,
         autoPlay: true,
         timeline: [
            { id: "eid8", tween: [ "style", "${_nube-03}", "top", '-110px', { fromValue: '-110px'}], position: 175000, duration: 0 },
            { id: "eid7", tween: [ "style", "${_nube-03}", "left", '-476px', { fromValue: '262px'}], position: 0, duration: 175000 },
            { id: "eid4", tween: [ "style", "${_nube-02}", "height", '428px', { fromValue: '463px'}], position: 0, duration: 5000 },
            { id: "eid9", tween: [ "style", "${_nube-02}", "height", '239px', { fromValue: '428px'}], position: 5000, duration: 82500 },
            { id: "eid1", tween: [ "style", "${_nube-02}", "left", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid5", tween: [ "style", "${_nube-02}", "width", '963px', { fromValue: '1040px'}], position: 0, duration: 5000 },
            { id: "eid10", tween: [ "style", "${_nube-02}", "width", '536px', { fromValue: '963px'}], position: 5000, duration: 82500 }         ]
      }
   }
},
"avion": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      transform: [[0,0],['-3'],[],['1','1.58898']],
      id: 'avion-062',
      type: 'image',
      rect: ['-94px','58px','80px','21px','auto','auto'],
      fill: ['rgba(0,0,0,0)','images/avion-06.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${symbolSelector}": [
            ["style", "height", '309px'],
            ["style", "width", '760px']
         ],
         "${_avion-062}": [
            ["style", "top", '138px'],
            ["transform", "scaleY", '1.58898'],
            ["transform", "rotateZ", '-17deg'],
            ["style", "height", '12px'],
            ["style", "left", '-99px'],
            ["style", "width", '45px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 21840,
         autoPlay: true,
         timeline: [
            { id: "eid108", tween: [ "style", "${_avion-062}", "top", '-42px', { fromValue: '138px'}], position: 0, duration: 21840 },
            { id: "eid11", tween: [ "transform", "${_avion-062}", "rotateZ", '-17deg', { fromValue: '-17deg'}], position: 21840, duration: 0 },
            { id: "eid112", tween: [ "style", "${_avion-062}", "width", '45px', { fromValue: '45px'}], position: 0, duration: 0 },
            { id: "eid106", tween: [ "style", "${_avion-062}", "left", '584px', { fromValue: '-99px'}], position: 0, duration: 21840 },
            { id: "eid111", tween: [ "style", "${_avion-062}", "height", '12px', { fromValue: '12px'}], position: 0, duration: 0 }         ]
      }
   }
},
"paloma": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'paloma-032',
      type: 'image',
      rect: ['0','0','1042px','145px','auto','auto'],
      fill: ['rgba(0,0,0,0)','images/paloma-03.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${symbolSelector}": [
            ["style", "height", '166px'],
            ["style", "width", '17.11%']
         ],
         "${_paloma-032}": [
            ["style", "left", '0px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 700,
         autoPlay: true,
         timeline: [
            { id: "eid5", tween: [ "style", "${_paloma-032}", "left", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid6", tween: [ "style", "${_paloma-032}", "left", '-130px', { fromValue: '0px'}], position: 100, duration: 0 },
            { id: "eid7", tween: [ "style", "${_paloma-032}", "left", '-260px', { fromValue: '-130px'}], position: 200, duration: 0 },
            { id: "eid9", tween: [ "style", "${_paloma-032}", "left", '-390px', { fromValue: '-260px'}], position: 300, duration: 0 },
            { id: "eid10", tween: [ "style", "${_paloma-032}", "left", '-520px', { fromValue: '-390px'}], position: 400, duration: 0 },
            { id: "eid11", tween: [ "style", "${_paloma-032}", "left", '-650px', { fromValue: '-520px'}], position: 500, duration: 0 },
            { id: "eid12", tween: [ "style", "${_paloma-032}", "left", '-780px', { fromValue: '-650px'}], position: 600, duration: 0 },
            { id: "eid13", tween: [ "style", "${_paloma-032}", "left", '-910px', { fromValue: '-780px'}], position: 700, duration: 0 }         ]
      }
   }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "EDGE-12738160");
