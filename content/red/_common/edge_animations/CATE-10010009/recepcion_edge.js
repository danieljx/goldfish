/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='files/CATE-10010009/images/';

var fonts = {};


var resources = [
];
var symbols = {
"stage": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
         dom: [
         {
            id:'recepcionok-01',
            type:'image',
            rect:['0px','0','760px','465px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"recepcionok-01.png",'0px','0px']
         },
         {
            id:'luzCopy',
            type:'rect',
            rect:['380px','43px','auto','auto','auto','auto']
         },
         {
            id:'luzCopy2',
            type:'rect',
            rect:['380px','43px','auto','auto','auto','auto']
         },
         {
            id:'luzCopy3',
            type:'rect',
            rect:['380px','43px','auto','auto','auto','auto']
         },
         {
            id:'luzCopy4',
            type:'rect',
            rect:['380px','43px','auto','auto','auto','auto']
         },
         {
            id:'puertas',
            type:'rect',
            rect:['583','126','auto','auto','auto','auto'],
            overflow:'hidden'
         },
         {
            id:'numeros',
            type:'rect',
            rect:['527','92','auto','auto','auto','auto']
         }],
         symbolInstances: [
         {
            id:'puertas',
            symbolName:'puertas'
         },
         {
            id:'luzCopy',
            symbolName:'luz'
         },
         {
            id:'numeros',
            symbolName:'numeros'
         },
         {
            id:'luzCopy2',
            symbolName:'luz'
         },
         {
            id:'luzCopy4',
            symbolName:'luz'
         },
         {
            id:'luzCopy3',
            symbolName:'luz'
         }
         ]
      },
   states: {
      "Base State": {
         "${_puertas}": [
            ["style", "top", '133px'],
            ["transform", "scaleY", '0.90713'],
            ["style", "overflow", 'hidden'],
            ["transform", "scaleX", '0.97574'],
            ["style", "left", '532px']
         ],
         "${_luzCopy4}": [
            ["style", "left", '248px'],
            ["style", "top", '14px']
         ],
         "${_luzCopy}": [
            ["style", "left", '129px'],
            ["style", "top", '29px']
         ],
         "${_recepcionok-01}": [
            ["style", "height", '465px'],
            ["style", "width", '760px']
         ],
         "${_luzCopy3}": [
            ["style", "left", '248px'],
            ["style", "top", '14px']
         ],
         "${_Stage}": [
            ["color", "background-color", 'rgba(255,255,255,1)'],
            ["style", "width", '760px'],
            ["style", "height", '465px'],
            ["style", "overflow", 'hidden']
         ],
         "${_numeros}": [
            ["style", "left", '0px'],
            ["style", "top", '0px']
         ],
         "${_luzCopy2}": [
            ["style", "left", '129px'],
            ["style", "top", '29px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 0,
         autoPlay: true,
         timeline: [
            { id: "eid9", tween: [ "transform", "${_puertas}", "scaleX", '0.97574', { fromValue: '0.97574'}], position: 0, duration: 0 },
            { id: "eid15", tween: [ "transform", "${_puertas}", "scaleY", '0.90713', { fromValue: '0.90713'}], position: 0, duration: 0 },
            { id: "eid10", tween: [ "style", "${_puertas}", "left", '532px', { fromValue: '532px'}], position: 0, duration: 0 },
            { id: "eid61", tween: [ "style", "${_luzCopy3}", "top", '14px', { fromValue: '14px'}], position: 0, duration: 0 },
            { id: "eid77", tween: [ "style", "${_numeros}", "top", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid60", tween: [ "style", "${_luzCopy3}", "left", '248px', { fromValue: '248px'}], position: 0, duration: 0 },
            { id: "eid58", tween: [ "style", "${_luzCopy}", "top", '29px', { fromValue: '29px'}], position: 0, duration: 0 },
            { id: "eid14", tween: [ "style", "${_puertas}", "top", '133px', { fromValue: '133px'}], position: 0, duration: 0 },
            { id: "eid76", tween: [ "style", "${_numeros}", "left", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid59", tween: [ "style", "${_luzCopy}", "left", '129px', { fromValue: '129px'}], position: 0, duration: 0 }         ]
      }
   }
},
"puertas": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'recepcionok-02',
      type: 'image',
      rect: ['0','0','53px','143px','auto','auto'],
      fill: ['rgba(0,0,0,0)',im+'recepcionok-02.png','0px','0px']
   },
   {
      id: 'recepcionok-03',
      type: 'image',
      rect: ['55px','0','51px','143px','auto','auto'],
      fill: ['rgba(0,0,0,0)',im+'recepcionok-03.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_recepcionok-03}": [
            ["style", "height", '143px'],
            ["style", "overflow", 'hidden'],
            ["style", "left", '55px'],
            ["style", "width", '51px']
         ],
         "${_recepcionok-02}": [
            ["style", "height", '143px'],
            ["style", "top", '0px'],
            ["style", "left", '0px'],
            ["style", "width", '53px']
         ],
         "${symbolSelector}": [
            ["style", "height", '143px'],
            ["style", "width", '106px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 2500,
         autoPlay: true,
         timeline: [
            { id: "eid23", tween: [ "style", "${_recepcionok-02}", "left", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid19", tween: [ "style", "${_recepcionok-02}", "left", '-53px', { fromValue: '0px'}], position: 1250, duration: 750 },
            { id: "eid22", tween: [ "style", "${_recepcionok-03}", "left", '55px', { fromValue: '55px'}], position: 0, duration: 0 },
            { id: "eid17", tween: [ "style", "${_recepcionok-03}", "left", '106px', { fromValue: '55px'}], position: 1250, duration: 750 },
            { id: "eid20", tween: [ "style", "${_recepcionok-03}", "left", '106px', { fromValue: '106px'}], position: 2500, duration: 0 },
            { id: "eid21", tween: [ "style", "${_recepcionok-02}", "top", '0px', { fromValue: '0px'}], position: 2500, duration: 0 }         ]
      }
   }
},
"luz": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      rect: ['3px','0px','147px','115px','auto','auto'],
      id: 'luz-01',
      opacity: 0.35164835164835,
      type: 'image',
      fill: ['rgba(0,0,0,0)',im+'luz-01.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_luz-01}": [
            ["style", "top", '0px'],
            ["style", "opacity", '0.35164835164835'],
            ["style", "left", '3px']
         ],
         "${symbolSelector}": [
            ["style", "height", '126px'],
            ["style", "width", '154px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 2800,
         autoPlay: true,
         timeline: [
            { id: "eid4", tween: [ "style", "${_luz-01}", "opacity", '1', { fromValue: '0.35164836049079895'}], position: 0, duration: 2800 },
            { id: "eid3", tween: [ "style", "${_luz-01}", "left", '3px', { fromValue: '3px'}], position: 0, duration: 0 }         ]
      }
   }
},
"numeros": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      rect: ['570px','125px','12px','15px','auto','auto'],
      font: ['Arial, Helvetica, sans-serif',11,'rgba(255,255,255,1)','normal','none','normal'],
      id: 'Text2Copy7',
      text: '1',
      align: 'left',
      type: 'text'
   },
   {
      rect: ['574px','125px','12px','15px','auto','auto'],
      font: ['Arial, Helvetica, sans-serif',11,'rgba(255,255,255,1)','normal','none','normal'],
      id: 'Text2Copy6',
      text: '2',
      align: 'left',
      type: 'text'
   },
   {
      rect: ['586px','125px','12px','15px','auto','auto'],
      font: ['Arial, Helvetica, sans-serif',11,'rgba(255,255,255,1)','normal','none','normal'],
      id: 'Text2Copy5',
      text: '3',
      align: 'left',
      type: 'text'
   },
   {
      rect: ['598px','125px','12px','15px','auto','auto'],
      font: ['Arial, Helvetica, sans-serif',11,'rgba(241,16,16,1)','normal','none','normal'],
      id: 'Text2Copy4',
      text: '4',
      align: 'left',
      type: 'text'
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_Text2Copy5}": [
            ["style", "top", '125px'],
            ["style", "width", '12px'],
            ["style", "height", '15px'],
            ["color", "color", 'rgba(255,255,255,1.00)'],
            ["style", "left", '586px'],
            ["style", "font-size", '11px']
         ],
         "${_Text2Copy4}": [
            ["style", "top", '125px'],
            ["style", "font-size", '11px'],
            ["style", "height", '15px'],
            ["color", "color", 'rgba(241,16,16,1)'],
            ["style", "left", '598px'],
            ["style", "width", '12px']
         ],
         "${symbolSelector}": [
            ["style", "height", '67px'],
            ["style", "width", '122px']
         ],
         "${_Text2Copy6}": [
            ["style", "top", '125px'],
            ["style", "font-size", '11px'],
            ["style", "height", '15px'],
            ["color", "color", 'rgba(255,255,255,1)'],
            ["style", "left", '574px'],
            ["style", "width", '12px']
         ],
         "${_Text2Copy7}": [
            ["style", "top", '125px'],
            ["style", "width", '12px'],
            ["style", "height", '15px'],
            ["color", "color", 'rgba(255,255,255,1)'],
            ["style", "left", '562px'],
            ["style", "font-size", '11px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 4750,
         autoPlay: true,
         timeline: [
            { id: "eid72", tween: [ "color", "${_Text2Copy7}", "color", 'rgba(255,0,0,1.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(255,255,255,1)'}], position: 1125, duration: 125 },
            { id: "eid62", tween: [ "style", "${_Text2Copy4}", "left", '598px', { fromValue: '598px'}], position: 0, duration: 0 },
            { id: "eid78", tween: [ "style", "${_Text2Copy4}", "left", '598px', { fromValue: '598px'}], position: 4750, duration: 0 },
            { id: "eid73", tween: [ "style", "${_Text2Copy7}", "left", '562px', { fromValue: '562px'}], position: 0, duration: 0 },
            { id: "eid74", tween: [ "style", "${_Text2Copy7}", "left", '562px', { fromValue: '562px'}], position: 1125, duration: 0 },
            { id: "eid75", tween: [ "style", "${_Text2Copy7}", "left", '562px', { fromValue: '562px'}], position: 1250, duration: 0 },
            { id: "eid81", tween: [ "style", "${_Text2Copy7}", "left", '562px', { fromValue: '562px'}], position: 4750, duration: 0 },
            { id: "eid70", tween: [ "style", "${_Text2Copy6}", "left", '574px', { fromValue: '574px'}], position: 750, duration: 0 },
            { id: "eid71", tween: [ "style", "${_Text2Copy6}", "left", '574px', { fromValue: '574px'}], position: 1125, duration: 0 },
            { id: "eid80", tween: [ "style", "${_Text2Copy6}", "left", '574px', { fromValue: '574px'}], position: 4750, duration: 0 },
            { id: "eid68", tween: [ "color", "${_Text2Copy6}", "color", 'rgba(255,1,1,1.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(255,255,255,1)'}], position: 750, duration: 250 },
            { id: "eid69", tween: [ "color", "${_Text2Copy6}", "color", 'rgba(255,255,255,1.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(255,1,1,1)'}], position: 1000, duration: 125 },
            { id: "eid64", tween: [ "color", "${_Text2Copy5}", "color", 'rgba(255,255,255,1.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(255,255,255,1.00)'}], position: 250, duration: 0 },
            { id: "eid65", tween: [ "color", "${_Text2Copy5}", "color", 'rgba(255,255,255,1.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(255,0,0,1.00)'}], position: 500, duration: 250 },
            { id: "eid66", tween: [ "style", "${_Text2Copy5}", "left", '586px', { fromValue: '586px'}], position: 250, duration: 0 },
            { id: "eid67", tween: [ "style", "${_Text2Copy5}", "left", '586px', { fromValue: '586px'}], position: 750, duration: 0 },
            { id: "eid79", tween: [ "style", "${_Text2Copy5}", "left", '586px', { fromValue: '586px'}], position: 4750, duration: 0 },
            { id: "eid63", tween: [ "color", "${_Text2Copy4}", "color", 'rgba(255,255,255,1.00)', { animationColorSpace: 'RGB', valueTemplate: undefined, fromValue: 'rgba(241,16,16,1)'}], position: 0, duration: 250 }         ]
      }
   }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "CATE-10010009");
