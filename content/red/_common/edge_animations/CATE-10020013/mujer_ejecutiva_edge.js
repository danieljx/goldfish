/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='files/CATE-10020013/images/';

var fonts = {};


var resources = [
];
var symbols = {
"stage": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
         dom: [
         {
            id:'saluda_animacion1',
            type:'rect',
            rect:['8px','0','auto','auto','auto','auto']
         }],
         symbolInstances: [
         {
            id:'saluda_animacion1',
            symbolName:'saluda_animacion1'
         }
         ]
      },
   states: {
      "Base State": {
         "${_Stage}": [
            ["color", "background-color", 'rgba(255,255,255,0.00)'],
            ["style", "overflow", 'hidden'],
            ["style", "height", '300px'],
            ["style", "width", '150px']
         ],
         "${_saluda_animacion1}": [
            ["style", "left", '8px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 1300,
         autoPlay: false,
         timeline: [
            { id: "eid17", tween: [ "style", "${_saluda_animacion1}", "left", '8px', { fromValue: '8px'}], position: 0, duration: 0 },
            { id: "eid18", tween: [ "style", "${_saluda_animacion1}", "left", '8px', { fromValue: '8px'}], position: 1300, duration: 0 },
            { id: "eid19", trigger: [ function executeSymbolFunction(e, data) { this._executeSymbolAction(e, data); }, ['play', '${_saluda_animacion1}', [] ], ""], position: 0 }         ]
      }
   }
},
"saluda_animacion1": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'personaje1',
      type: 'image',
      rect: ['0','0','1050px','300px','auto','auto'],
      fill: ['rgba(0,0,0,0)',im+'personaje1.svg','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_personaje1}": [
            ["style", "left", '0px']
         ],
         "${symbolSelector}": [
            ["style", "height", '300px'],
            ["style", "width", '150px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 1300,
         autoPlay: false,
         timeline: [
            { id: "eid1", tween: [ "style", "${_personaje1}", "left", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid2", tween: [ "style", "${_personaje1}", "left", '-150px', { fromValue: '0px'}], position: 100, duration: 0 },
            { id: "eid4", tween: [ "style", "${_personaje1}", "left", '-300px', { fromValue: '-150px'}], position: 200, duration: 0 },
            { id: "eid6", tween: [ "style", "${_personaje1}", "left", '-450px', { fromValue: '-300px'}], position: 300, duration: 0 },
            { id: "eid7", tween: [ "style", "${_personaje1}", "left", '-600px', { fromValue: '-450px'}], position: 400, duration: 0 },
            { id: "eid8", tween: [ "style", "${_personaje1}", "left", '-750px', { fromValue: '-600px'}], position: 500, duration: 0 },
            { id: "eid9", tween: [ "style", "${_personaje1}", "left", '-900px', { fromValue: '-750px'}], position: 700, duration: 0 },
            { id: "eid10", tween: [ "style", "${_personaje1}", "left", '-750px', { fromValue: '-900px'}], position: 900, duration: 0 },
            { id: "eid12", tween: [ "style", "${_personaje1}", "left", '-300px', { fromValue: '-600px'}], position: 1100, duration: 0 },
            { id: "eid13", tween: [ "style", "${_personaje1}", "left", '-150px', { fromValue: '-300px'}], position: 1200, duration: 0 },
            { id: "eid14", tween: [ "style", "${_personaje1}", "left", '0px', { fromValue: '-150px'}], position: 1300, duration: 0 }         ]
      }
   }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "CATE-10020013");
