/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='files/CATE-10010026/images/';

var fonts = {};


var resources = [
];
var symbols = {
"stage": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
         dom: [
         {
            id:'ejecutivo-23-01',
            type:'image',
            rect:['0px','0','760px','465px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"ejecutivo-23-01.jpg",'0px','0px']
         },
         {
            id:'reloj',
            type:'rect',
            rect:['518px','77px','auto','auto','auto','auto']
         },
         {
            id:'burbuja',
            type:'rect',
            rect:['262','135','auto','auto','auto','auto']
         },
         {
            id:'burbujaCopy7',
            type:'rect',
            rect:['262','135','auto','auto','auto','auto']
         },
         {
            id:'burbujaCopy8',
            type:'rect',
            rect:['262','135','auto','auto','auto','auto']
         },
         {
            id:'burbujaCopy9',
            type:'rect',
            rect:['262','135','auto','auto','auto','auto']
         },
         {
            id:'burbujaCopy10',
            type:'rect',
            rect:['262','135','auto','auto','auto','auto']
         },
         {
            id:'burbujaCopy11',
            type:'rect',
            rect:['262','135','auto','auto','auto','auto']
         },
         {
            id:'burbujaCopy',
            type:'rect',
            rect:['262','135','auto','auto','auto','auto']
         },
         {
            id:'burbujaCopy6',
            type:'rect',
            rect:['262','135','auto','auto','auto','auto']
         },
         {
            id:'burbujaCopy2',
            type:'rect',
            rect:['262','135','auto','auto','auto','auto']
         },
         {
            id:'burbujaCopy3',
            type:'rect',
            rect:['262','135','auto','auto','auto','auto']
         },
         {
            id:'burbujaCopy4',
            type:'rect',
            rect:['262','135','auto','auto','auto','auto']
         },
         {
            id:'burbujaCopy5',
            type:'rect',
            rect:['262','135','auto','auto','auto','auto']
         }],
         symbolInstances: [
         {
            id:'burbujaCopy3',
            symbolName:'burbuja'
         },
         {
            id:'burbujaCopy11',
            symbolName:'burbuja'
         },
         {
            id:'burbuja',
            symbolName:'burbuja'
         },
         {
            id:'burbujaCopy10',
            symbolName:'burbuja'
         },
         {
            id:'reloj',
            symbolName:'reloj'
         },
         {
            id:'burbujaCopy9',
            symbolName:'burbuja'
         },
         {
            id:'burbujaCopy7',
            symbolName:'burbuja'
         },
         {
            id:'burbujaCopy5',
            symbolName:'burbuja'
         },
         {
            id:'burbujaCopy6',
            symbolName:'burbuja'
         },
         {
            id:'burbujaCopy2',
            symbolName:'burbuja'
         },
         {
            id:'burbujaCopy4',
            symbolName:'burbuja'
         },
         {
            id:'burbujaCopy',
            symbolName:'burbuja'
         },
         {
            id:'burbujaCopy8',
            symbolName:'burbuja'
         }
         ]
      },
   states: {
      "Base State": {
         "${_burbujaCopy5}": [
            ["style", "left", '251px'],
            ["style", "top", '142px']
         ],
         "${_burbujaCopy10}": [
            ["style", "left", '260px'],
            ["style", "top", '134px']
         ],
         "${_burbujaCopy7}": [
            ["style", "left", '253px'],
            ["style", "top", '142px']
         ],
         "${_burbujaCopy11}": [
            ["style", "left", '260px'],
            ["style", "top", '144px']
         ],
         "${_reloj}": [
            ["style", "top", '82px'],
            ["style", "left", '117px'],
            ["style", "overflow", 'hidden']
         ],
         "${_burbujaCopy9}": [
            ["style", "left", '258px'],
            ["style", "top", '134px']
         ],
         "${_burbujaCopy4}": [
            ["style", "left", '258px'],
            ["style", "top", '155px']
         ],
         "${_burbujaCopy6}": [
            ["style", "left", '263px'],
            ["style", "top", '130px']
         ],
         "${_burbujaCopy8}": [
            ["style", "left", '254px'],
            ["style", "top", '134px']
         ],
         "${_burbujaCopy2}": [
            ["style", "left", '251px'],
            ["style", "top", '135px']
         ],
         "${_burbujaCopy3}": [
            ["style", "left", '240px'],
            ["style", "top", '134px']
         ],
         "${_Stage}": [
            ["color", "background-color", 'rgba(255,255,255,1)'],
            ["style", "width", '760px'],
            ["style", "height", '465px'],
            ["style", "overflow", 'hidden']
         ],
         "${_burbuja}": [
            ["style", "left", '251px'],
            ["style", "top", '142px']
         ],
         "${_ejecutivo-23-01}": [
            ["style", "height", '465px'],
            ["style", "width", '760px']
         ],
         "${_burbujaCopy}": [
            ["style", "left", '263px'],
            ["style", "top", '142px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 10000,
         autoPlay: true,
         timeline: [
            { id: "eid60", tween: [ "style", "${_burbuja}", "left", '251px', { fromValue: '251px'}], position: 0, duration: 0 },
            { id: "eid4", tween: [ "style", "${_reloj}", "left", '117px', { fromValue: '117px'}], position: 0, duration: 0 },
            { id: "eid79", tween: [ "style", "${_burbujaCopy3}", "left", '240px', { fromValue: '240px'}], position: 0, duration: 0 },
            { id: "eid63", tween: [ "style", "${_burbujaCopy8}", "top", '134px', { fromValue: '134px'}], position: 0, duration: 0 },
            { id: "eid50", tween: [ "style", "${_burbujaCopy2}", "left", '251px', { fromValue: '251px'}], position: 0, duration: 0 },
            { id: "eid80", tween: [ "style", "${_burbujaCopy3}", "top", '134px', { fromValue: '134px'}], position: 0, duration: 0 },
            { id: "eid3", tween: [ "style", "${_reloj}", "top", '82px', { fromValue: '82px'}], position: 0, duration: 0 },
            { id: "eid78", tween: [ "style", "${_burbujaCopy4}", "top", '155px', { fromValue: '155px'}], position: 0, duration: 0 },
            { id: "eid52", tween: [ "style", "${_burbujaCopy}", "top", '142px', { fromValue: '142px'}], position: 0, duration: 0 },
            { id: "eid51", tween: [ "style", "${_burbujaCopy2}", "top", '135px', { fromValue: '135px'}], position: 0, duration: 0 },
            { id: "eid70", tween: [ "style", "${_burbujaCopy11}", "top", '144px', { fromValue: '144px'}], position: 1000, duration: 0 },
            { id: "eid83", tween: [ "style", "${_burbujaCopy6}", "top", '130px', { fromValue: '130px'}], position: 0, duration: 0 },
            { id: "eid62", tween: [ "style", "${_burbujaCopy8}", "left", '254px', { fromValue: '254px'}], position: 0, duration: 0 },
            { id: "eid65", tween: [ "style", "${_burbujaCopy10}", "left", '260px', { fromValue: '260px'}], position: 0, duration: 0 },
            { id: "eid77", tween: [ "style", "${_burbujaCopy4}", "left", '258px', { fromValue: '258px'}], position: 0, duration: 0 },
            { id: "eid49", tween: [ "style", "${_burbuja}", "top", '142px', { fromValue: '142px'}], position: 0, duration: 0 },
            { id: "eid64", tween: [ "style", "${_burbujaCopy9}", "left", '258px', { fromValue: '258px'}], position: 0, duration: 0 },
            { id: "eid59", tween: [ "style", "${_burbujaCopy}", "left", '263px', { fromValue: '263px'}], position: 0, duration: 0 },
            { id: "eid61", tween: [ "style", "${_burbujaCopy7}", "left", '253px', { fromValue: '253px'}], position: 0, duration: 0 }         ]
      }
   }
},
"reloj": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'reloj-02',
      type: 'image',
      rect: ['0','0','251px','26px','auto','auto'],
      fill: ['rgba(0,0,0,0)',im+'reloj-02.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_reloj-02}": [
            ["style", "left", '0px']
         ],
         "${symbolSelector}": [
            ["style", "height", '25px'],
            ["style", "width", '25px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 10000,
         autoPlay: true,
         timeline: [
            { id: "eid7", tween: [ "style", "${_reloj-02}", "left", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid8", tween: [ "style", "${_reloj-02}", "left", '-25px', { fromValue: '0px'}], position: 1000, duration: 0 },
            { id: "eid9", tween: [ "style", "${_reloj-02}", "left", '-50px', { fromValue: '-25px'}], position: 2000, duration: 0 },
            { id: "eid10", tween: [ "style", "${_reloj-02}", "left", '-75px', { fromValue: '-50px'}], position: 3000, duration: 0 },
            { id: "eid11", tween: [ "style", "${_reloj-02}", "left", '-100px', { fromValue: '-75px'}], position: 4000, duration: 0 },
            { id: "eid12", tween: [ "style", "${_reloj-02}", "left", '-125px', { fromValue: '-100px'}], position: 5000, duration: 0 },
            { id: "eid13", tween: [ "style", "${_reloj-02}", "left", '-150px', { fromValue: '-125px'}], position: 6000, duration: 0 },
            { id: "eid14", tween: [ "style", "${_reloj-02}", "left", '-175px', { fromValue: '-150px'}], position: 7000, duration: 0 },
            { id: "eid15", tween: [ "style", "${_reloj-02}", "left", '-200px', { fromValue: '-175px'}], position: 8000, duration: 0 },
            { id: "eid16", tween: [ "style", "${_reloj-02}", "left", '-225px', { fromValue: '-200px'}], position: 9000, duration: 0 }         ]
      }
   }
},
"burbuja": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      rect: ['10px','49px','3px','4px','auto','auto'],
      id: 'burbuja-01',
      opacity: 0.43292682781452,
      type: 'image',
      fill: ['rgba(0,0,0,0)',im+'burbuja-01.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_burbuja-01}": [
            ["style", "top", '59px'],
            ["style", "height", '4px'],
            ["style", "opacity", '0'],
            ["style", "left", '12px'],
            ["style", "width", '3px']
         ],
         "${symbolSelector}": [
            ["style", "height", '54px'],
            ["style", "width", '39px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 5750,
         autoPlay: true,
         timeline: [
            { id: "eid19", tween: [ "style", "${_burbuja-01}", "top", '0px', { fromValue: '59px'}], position: 0, duration: 5053 },
            { id: "eid55", tween: [ "style", "${_burbuja-01}", "top", '0px', { fromValue: '0px'}], position: 5053, duration: 697 },
            { id: "eid17", tween: [ "style", "${_burbuja-01}", "left", '17px', { fromValue: '12px'}], position: 0, duration: 5053 },
            { id: "eid54", tween: [ "style", "${_burbuja-01}", "left", '18px', { fromValue: '17px'}], position: 5053, duration: 697 },
            { id: "eid21", tween: [ "style", "${_burbuja-01}", "opacity", '0.66463414634146', { fromValue: '0'}], position: 0, duration: 1375 },
            { id: "eid32", tween: [ "style", "${_burbuja-01}", "opacity", '0.2847949226455', { fromValue: '0.66463414634146'}], position: 1375, duration: 3300 },
            { id: "eid35", tween: [ "style", "${_burbuja-01}", "opacity", '0.2381028780478', { fromValue: '0.2847949226455'}], position: 4675, duration: 378 },
            { id: "eid56", tween: [ "style", "${_burbuja-01}", "opacity", '0', { fromValue: '0.2381030023097992'}], position: 5053, duration: 697 }         ]
      }
   }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "CATE-10010026");
