/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='files/CATE-10010032/images/';

var fonts = {};


var resources = [
];
var symbols = {
"stage": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
         dom: [
         {
            id:'fondo-01-03',
            type:'image',
            rect:['0px','0px','760px','465px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"fondo-01-03.jpg",'0px','0px']
         },
         {
            id:'mar',
            type:'rect',
            rect:['0px','239px','auto','auto','auto','auto']
         },
         {
            id:'aves',
            type:'rect',
            rect:['79px','-20px','auto','auto','auto','auto'],
            transform:[[],[],[],['0.1609','0.1609']]
         },
         {
            id:'avesCopy',
            type:'rect',
            rect:['79px','-20px','auto','auto','auto','auto'],
            transform:[[],[],[],['0.1609','0.1609']]
         },
         {
            id:'avesCopy2',
            type:'rect',
            rect:['79px','-20px','auto','auto','auto','auto'],
            transform:[[],[],[],['0.1609','0.1609']]
         },
         {
            id:'punto-de-reciclaje-01',
            type:'image',
            rect:['0px','0px','760px','465px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"punto-de-reciclaje-01.png",'0px','0px']
         },
         {
            id:'oja',
            type:'rect',
            rect:['530','14','auto','auto','auto','auto']
         },
         {
            id:'ojaCopy',
            type:'rect',
            rect:['530','14','auto','auto','auto','auto']
         },
         {
            id:'ojaCopy2',
            type:'rect',
            rect:['530','14','auto','auto','auto','auto']
         },
         {
            id:'ojaCopy3',
            type:'rect',
            rect:['530','14','auto','auto','auto','auto']
         },
         {
            id:'ojaCopy4',
            type:'rect',
            rect:['530','14','auto','auto','auto','auto']
         },
         {
            id:'ojaCopy5',
            type:'rect',
            rect:['530','14','auto','auto','auto','auto']
         },
         {
            id:'ojaCopy6',
            type:'rect',
            rect:['530','14','auto','auto','auto','auto']
         }],
         symbolInstances: [
         {
            id:'avesCopy',
            symbolName:'aves'
         },
         {
            id:'mar',
            symbolName:'mar'
         },
         {
            id:'aves',
            symbolName:'aves'
         },
         {
            id:'ojaCopy6',
            symbolName:'oja'
         },
         {
            id:'ojaCopy',
            symbolName:'oja'
         },
         {
            id:'ojaCopy3',
            symbolName:'oja'
         },
         {
            id:'ojaCopy5',
            symbolName:'oja'
         },
         {
            id:'ojaCopy4',
            symbolName:'oja'
         },
         {
            id:'oja',
            symbolName:'oja'
         },
         {
            id:'ojaCopy2',
            symbolName:'oja'
         },
         {
            id:'avesCopy2',
            symbolName:'aves'
         }
         ]
      },
   states: {
      "Base State": {
         "${_ojaCopy}": [
            ["style", "top", '61px'],
            ["transform", "scaleX", '0.76923'],
            ["style", "left", '592px'],
            ["transform", "scaleY", '0.76923']
         ],
         "${_ojaCopy2}": [
            ["transform", "scaleX", '0.76923'],
            ["transform", "scaleY", '0.76923'],
            ["style", "left", '309px'],
            ["style", "top", '17px']
         ],
         "${_ojaCopy6}": [
            ["transform", "scaleX", '0.76923'],
            ["transform", "scaleY", '0.76923'],
            ["style", "left", '334px'],
            ["style", "top", '31px']
         ],
         "${_fondo-01-03}": [
            ["style", "height", '465px'],
            ["style", "top", '0px'],
            ["style", "left", '0px'],
            ["style", "width", '760px']
         ],
         "${_Stage}": [
            ["color", "background-color", 'rgba(255,255,255,1)'],
            ["style", "overflow", 'hidden'],
            ["style", "height", '465px'],
            ["style", "width", '760px']
         ],
         "${_punto-de-reciclaje-01}": [
            ["style", "height", '465px'],
            ["style", "top", '0px'],
            ["style", "left", '0px'],
            ["style", "width", '760px']
         ],
         "${_aves}": [
            ["transform", "scaleX", '0.07736'],
            ["style", "top", '77px'],
            ["style", "left", '75px'],
            ["transform", "scaleY", '0.07736']
         ],
         "${_ojaCopy3}": [
            ["transform", "scaleX", '0.76923'],
            ["style", "top", '1px'],
            ["transform", "scaleY", '0.76923'],
            ["style", "left", '72px']
         ],
         "${_avesCopy}": [
            ["style", "top", '106px'],
            ["transform", "scaleY", '0.07736'],
            ["style", "left", '56px'],
            ["transform", "scaleX", '0.07736']
         ],
         "${_oja}": [
            ["style", "top", '31px'],
            ["style", "left", '496px'],
            ["transform", "scaleY", '0.76923'],
            ["transform", "scaleX", '0.76923']
         ],
         "${_mar}": [
            ["style", "top", '189px'],
            ["style", "left", '0px'],
            ["transform", "scaleY", '0.37736']
         ],
         "${_ojaCopy5}": [
            ["style", "top", '67px'],
            ["transform", "scaleX", '0.76923'],
            ["style", "left", '617px'],
            ["transform", "scaleY", '0.76923']
         ],
         "${_ojaCopy4}": [
            ["style", "top", '31px'],
            ["style", "left", '506px'],
            ["transform", "scaleY", '0.76923'],
            ["transform", "scaleX", '0.76923']
         ],
         "${_avesCopy2}": [
            ["style", "top", '99px'],
            ["transform", "scaleX", '0.07736'],
            ["transform", "scaleY", '0.07736'],
            ["style", "left", '92px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 39000,
         autoPlay: true,
         timeline: [
            { id: "eid9", tween: [ "transform", "${_mar}", "scaleY", '0.37736', { fromValue: '0.37736'}], position: 0, duration: 0 },
            { id: "eid59", tween: [ "style", "${_oja}", "top", '31px', { fromValue: '31px'}], position: 13750, duration: 0 },
            { id: "eid14", tween: [ "style", "${_aves}", "left", '714px', { fromValue: '75px'}], position: 0, duration: 33000 },
            { id: "eid58", tween: [ "style", "${_oja}", "left", '496px', { fromValue: '496px'}], position: 13750, duration: 0 },
            { id: "eid10", tween: [ "style", "${_mar}", "top", '189px', { fromValue: '189px'}], position: 0, duration: 0 },
            { id: "eid60", tween: [ "style", "${_ojaCopy}", "left", '592px', { fromValue: '592px'}], position: 13750, duration: 0 },
            { id: "eid48", tween: [ "transform", "${_oja}", "scaleX", '0.76923', { fromValue: '0.76923'}], position: 13750, duration: 0 },
            { id: "eid65", tween: [ "style", "${_ojaCopy3}", "top", '1px', { fromValue: '1px'}], position: 13750, duration: 0 },
            { id: "eid72", tween: [ "style", "${_ojaCopy5}", "left", '617px', { fromValue: '617px'}], position: 13750, duration: 0 },
            { id: "eid74", tween: [ "style", "${_ojaCopy6}", "left", '334px', { fromValue: '334px'}], position: 13750, duration: 0 },
            { id: "eid24", tween: [ "style", "${_avesCopy2}", "top", '116px', { fromValue: '99px'}], position: 180, duration: 23389 },
            { id: "eid26", tween: [ "style", "${_avesCopy2}", "top", '-114px', { fromValue: '116px'}], position: 23569, duration: 431 },
            { id: "eid22", tween: [ "style", "${_avesCopy2}", "left", '693px', { fromValue: '92px'}], position: 180, duration: 23389 },
            { id: "eid25", tween: [ "style", "${_avesCopy2}", "left", '708px', { fromValue: '693px'}], position: 23569, duration: 431 },
            { id: "eid73", tween: [ "style", "${_ojaCopy5}", "top", '67px', { fromValue: '67px'}], position: 13750, duration: 0 },
            { id: "eid16", tween: [ "style", "${_aves}", "top", '13px', { fromValue: '77px'}], position: 0, duration: 33000 },
            { id: "eid75", tween: [ "style", "${_ojaCopy6}", "top", '31px', { fromValue: '31px'}], position: 13750, duration: 0 },
            { id: "eid71", tween: [ "style", "${_ojaCopy4}", "top", '31px', { fromValue: '31px'}], position: 13750, duration: 0 },
            { id: "eid70", tween: [ "style", "${_ojaCopy4}", "left", '506px', { fromValue: '506px'}], position: 13750, duration: 0 },
            { id: "eid49", tween: [ "transform", "${_oja}", "scaleY", '0.76923', { fromValue: '0.76923'}], position: 13750, duration: 0 },
            { id: "eid61", tween: [ "style", "${_ojaCopy}", "top", '61px', { fromValue: '61px'}], position: 13750, duration: 0 },
            { id: "eid64", tween: [ "style", "${_ojaCopy3}", "left", '72px', { fromValue: '72px'}], position: 13750, duration: 0 },
            { id: "eid62", tween: [ "style", "${_ojaCopy2}", "left", '309px', { fromValue: '309px'}], position: 13750, duration: 0 },
            { id: "eid20", tween: [ "style", "${_avesCopy}", "top", '34px', { fromValue: '106px'}], position: 180, duration: 26820 },
            { id: "eid19", tween: [ "style", "${_avesCopy}", "left", '727px', { fromValue: '56px'}], position: 180, duration: 26820 },
            { id: "eid63", tween: [ "style", "${_ojaCopy2}", "top", '17px', { fromValue: '17px'}], position: 13750, duration: 0 },
            { id: "eid5", tween: [ "style", "${_mar}", "left", '0px', { fromValue: '0px'}], position: 0, duration: 0 }         ]
      }
   }
},
"mar": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'mar-04',
      type: 'image',
      rect: ['0','0','1783px','159px','auto','auto'],
      fill: ['rgba(0,0,0,0)',im+'mar-042.jpg','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${symbolSelector}": [
            ["style", "height", '159px'],
            ["style", "width", '760px']
         ],
         "${_mar-04}": [
            ["style", "height", '159px'],
            ["style", "left", '-156px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 39184.579692862,
         autoPlay: true,
         timeline: [
            { id: "eid11", tween: [ "style", "${_mar-04}", "left", '-1013px', { fromValue: '-156px'}], position: 0, duration: 39167 },
            { id: "eid15", tween: [ "style", "${_mar-04}", "left", '-614px', { fromValue: '-1013px'}], position: 39167, duration: 17 }         ]
      }
   }
},
"aves": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'pajaro-01',
      type: 'image',
      rect: ['0','0','600px','127px','auto','auto'],
      fill: ['rgba(0,0,0,0)',im+'pajaro-01.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${symbolSelector}": [
            ["style", "height", '169px'],
            ["style", "width", '152px'],
            ["style", "overflow", 'hidden']
         ],
         "${_pajaro-01}": [
            ["style", "left", '0px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 2600,
         autoPlay: true,
         timeline: [
            { id: "eid1", tween: [ "style", "${_pajaro-01}", "left", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid2", tween: [ "style", "${_pajaro-01}", "left", '-150px', { fromValue: '0px'}], position: 100, duration: 0 },
            { id: "eid4", tween: [ "style", "${_pajaro-01}", "left", '-300px', { fromValue: '-150px'}], position: 200, duration: 0 },
            { id: "eid5", tween: [ "style", "${_pajaro-01}", "left", '-450px', { fromValue: '-300px'}], position: 300, duration: 0 }         ]
      }
   }
},
"oja": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'oja-02',
      type: 'image',
      rect: ['0','0','49px','78px','auto','auto'],
      fill: ['rgba(0,0,0,0)',im+'oja-02.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_oja-02}": [
            ["style", "top", '1px'],
            ["transform", "scaleX", '0.92995'],
            ["transform", "scaleY", '0.89411'],
            ["transform", "rotateZ", '-4deg'],
            ["style", "height", '67px'],
            ["style", "opacity", '0.71341463414634'],
            ["style", "left", '0px'],
            ["style", "width", '41px']
         ],
         "${symbolSelector}": [
            ["style", "height", '78px'],
            ["style", "width", '66px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 1250,
         autoPlay: true,
         timeline: [
            { id: "eid66", tween: [ "style", "${_oja-02}", "left", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid69", tween: [ "transform", "${_oja-02}", "rotateZ", '4deg', { fromValue: '-4deg'}], position: 0, duration: 1250 }         ]
      }
   }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "CATE-10010032");
