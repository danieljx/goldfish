/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='files/CATE-10010004/images/';

var fonts = {};


var resources = [
];
var symbols = {
"stage": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
         dom: [
         {
            id:'fonsdoejecutivo2-01',
            type:'image',
            rect:['0','0','760px','465px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"fonsdoejecutivo2-01.jpg",'0px','0px']
         },
         {
            id:'avion',
            type:'rect',
            rect:['-6px','0px','auto','auto','auto','auto']
         },
         {
            id:'aros',
            type:'rect',
            rect:['296px','163px','auto','auto','auto','auto'],
            transform:[[],[],[],['0.61538','0.43589']]
         },
         {
            id:'escenario_maloka-012',
            type:'image',
            rect:['0','0','760px','465px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"escenario_maloka-012.png",'0px','0px']
         }],
         symbolInstances: [
         {
            id:'avion',
            symbolName:'avion'
         },
         {
            id:'aros',
            symbolName:'aros'
         }
         ]
      },
   states: {
      "Base State": {
         "${_avion}": [
            ["style", "left", '-6px'],
            ["style", "top", '0px']
         ],
         "${_fonsdoejecutivo2-01}": [
            ["style", "height", '465px'],
            ["style", "width", '760px']
         ],
         "${_aros}": [
            ["style", "top", '163px'],
            ["transform", "scaleY", '0.43589'],
            ["style", "left", '296px'],
            ["transform", "scaleX", '0.61538']
         ],
         "${_Stage}": [
            ["color", "background-color", 'rgba(255,255,255,1)'],
            ["style", "width", '760px'],
            ["style", "height", '465px'],
            ["style", "overflow", 'hidden']
         ],
         "${_escenario_maloka-012}": [
            ["style", "height", '465px'],
            ["style", "width", '760px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 0,
         autoPlay: true,
         timeline: [
         ]
      }
   }
},
"aros": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'aros2',
      type: 'image',
      rect: ['0','0','1182px','150px','auto','auto'],
      fill: ['rgba(0,0,0,0)',im+'aros.svg','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_aros2}": [
            ["style", "left", '0px']
         ],
         "${symbolSelector}": [
            ["style", "height", '156px'],
            ["style", "overflow", 'hidden'],
            ["style", "width", '142px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 1000,
         autoPlay: true,
         timeline: [
            { id: "eid1", tween: [ "style", "${_aros2}", "left", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid2", tween: [ "style", "${_aros2}", "left", '-150px', { fromValue: '0px'}], position: 200, duration: 0 },
            { id: "eid4", tween: [ "style", "${_aros2}", "left", '-300px', { fromValue: '-150px'}], position: 400, duration: 0 },
            { id: "eid5", tween: [ "style", "${_aros2}", "left", '-450px', { fromValue: '-300px'}], position: 600, duration: 0 },
            { id: "eid6", tween: [ "style", "${_aros2}", "left", '-600px', { fromValue: '-450px'}], position: 800, duration: 0 },
            { id: "eid7", tween: [ "style", "${_aros2}", "left", '-750px', { fromValue: '-600px'}], position: 1000, duration: 0 }         ]
      }
   }
},
"avion": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'avion-062',
      type: 'image',
      rect: ['-94px','58px','80px','21px','auto','auto'],
      fill: ['rgba(0,0,0,0)',im+'avion-06.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_avion-062}": [
            ["style", "height", '14px'],
            ["style", "top", '333px'],
            ["style", "left", '215px'],
            ["style", "width", '52px']
         ],
         "${symbolSelector}": [
            ["style", "height", '309px'],
            ["style", "width", '760px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 21840,
         autoPlay: true,
         timeline: [
            { id: "eid108", tween: [ "style", "${_avion-062}", "top", '-114px', { fromValue: '333px'}], position: 0, duration: 21840 },
            { id: "eid106", tween: [ "style", "${_avion-062}", "left", '625px', { fromValue: '215px'}], position: 0, duration: 21840 },
            { id: "eid112", tween: [ "style", "${_avion-062}", "width", '52px', { fromValue: '52px'}], position: 0, duration: 0 },
            { id: "eid111", tween: [ "style", "${_avion-062}", "height", '14px', { fromValue: '14px'}], position: 0, duration: 0 }         ]
      }
   }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "CATE-10010004");
