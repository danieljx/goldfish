/***********************
* Acciones de composición de Adobe Edge Animate
*
* Editar este archivo con precaución, teniendo cuidado de conservar 
* las firmas de función y los comentarios que comienzan con "Edge" para mantener la 
* capacidad de interactuar con estas acciones en Adobe Edge Animate
*
***********************/
(function($, Edge, compId){
var Composition = Edge.Composition, Symbol = Edge.Symbol; // los alias más comunes para las clases de Edge

   //Edge symbol: 'stage'
   (function(symbolName) {
      
      
   })("stage");
   //Edge symbol end:'stage'

   //=========================================================
   
   //Edge symbol: 'saluda_animacion1'
   (function(symbolName) {   
   
//      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 0, function(sym, e) {
//         sym.playReverse();
//
//      });
      //Edge binding end

   })("saluda_animacion1");
   //Edge symbol end:'saluda_animacion1'

})(jQuery, AdobeEdge, "CATE-10020022");