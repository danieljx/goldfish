/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='files/CATE-10010001/images/';

var fonts = {};


var resources = [
];
var symbols = {
"stage": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
         dom: [
         {
            id:'edificioexterior1-01',
            type:'image',
            rect:['0px','0px','760px','465px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"edificioexterior1-01.jpg",'0px','0px']
         },
         {
            id:'luz-04',
            type:'image',
            rect:['261px','156px','98px','89px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"luz-04.png",'0px','0px']
         },
         {
            id:'luz-06',
            type:'image',
            rect:['253px','134px','100px','111px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"luz-06.png",'0px','0px']
         },
         {
            id:'luz-05',
            type:'image',
            rect:['277px','134px','65px','72px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"luz-05.png",'0px','0px']
         },
         {
            id:'luz',
            type:'rect',
            rect:['-33','-19','auto','auto','auto','auto'],
            transform:[[],[],[],['0.82994','0.63273']]
         }],
         symbolInstances: [
         {
            id:'luz',
            symbolName:'luz'
         }
         ]
      },
   states: {
      "Base State": {
         "${_luz-06}": [
            ["style", "top", '134px'],
            ["style", "opacity", '0'],
            ["style", "left", '253px']
         ],
         "${_edificioexterior1-01}": [
            ["style", "height", '465px'],
            ["style", "width", '760px']
         ],
         "${_Stage}": [
            ["color", "background-color", 'rgba(255,255,255,1)'],
            ["style", "overflow", 'hidden'],
            ["style", "height", '465px'],
            ["style", "width", '760px']
         ],
         "${_luz-04}": [
            ["style", "top", '156px'],
            ["style", "height", '89px'],
            ["style", "opacity", '0.1978021978022'],
            ["style", "left", '261px'],
            ["style", "width", '98px']
         ],
         "${_luz-05}": [
            ["style", "top", '134px'],
            ["style", "height", '72px'],
            ["style", "opacity", '0'],
            ["style", "left", '277px'],
            ["style", "width", '65px']
         ],
         "${_luz}": [
            ["transform", "scaleX", '0.99154'],
            ["style", "top", '-110px'],
            ["style", "left", '324px'],
            ["transform", "scaleY", '0.57063']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 53264,
         autoPlay: true,
         timeline: [
            { id: "eid95", tween: [ "style", "${_luz-05}", "left", '277px', { fromValue: '277px'}], position: 13000, duration: 0 },
            { id: "eid101", tween: [ "style", "${_luz-05}", "left", '277px', { fromValue: '277px'}], position: 46000, duration: 0 },
            { id: "eid55", tween: [ "style", "${_luz-04}", "left", '261px', { fromValue: '261px'}], position: 0, duration: 0 },
            { id: "eid60", tween: [ "style", "${_luz-04}", "left", '261px', { fromValue: '261px'}], position: 300, duration: 0 },
            { id: "eid63", tween: [ "style", "${_luz-04}", "left", '261px', { fromValue: '261px'}], position: 600, duration: 0 },
            { id: "eid66", tween: [ "style", "${_luz-04}", "left", '261px', { fromValue: '261px'}], position: 3000, duration: 0 },
            { id: "eid69", tween: [ "style", "${_luz-04}", "left", '261px', { fromValue: '261px'}], position: 3100, duration: 0 },
            { id: "eid73", tween: [ "style", "${_luz-04}", "left", '261px', { fromValue: '261px'}], position: 3400, duration: 0 },
            { id: "eid75", tween: [ "style", "${_luz-04}", "left", '261px', { fromValue: '261px'}], position: 3600, duration: 0 },
            { id: "eid118", tween: [ "transform", "${_luz}", "scaleY", '0.57063', { fromValue: '0.57063'}], position: 53264, duration: 0 },
            { id: "eid115", tween: [ "style", "${_luz}", "top", '-110px', { fromValue: '-110px'}], position: 53264, duration: 0 },
            { id: "eid116", tween: [ "style", "${_luz}", "left", '324px', { fromValue: '324px'}], position: 53264, duration: 0 },
            { id: "eid117", tween: [ "transform", "${_luz}", "scaleX", '0.99154', { fromValue: '0.99154'}], position: 53264, duration: 0 },
            { id: "eid59", tween: [ "style", "${_luz-04}", "opacity", '1', { fromValue: '0.1978021978022'}], position: 0, duration: 200 },
            { id: "eid61", tween: [ "style", "${_luz-04}", "opacity", '0', { fromValue: '1'}], position: 200, duration: 100 },
            { id: "eid62", tween: [ "style", "${_luz-04}", "opacity", '1', { fromValue: '0'}], position: 300, duration: 200 },
            { id: "eid64", tween: [ "style", "${_luz-04}", "opacity", '0.18131864955137', { fromValue: '1'}], position: 500, duration: 100 },
            { id: "eid65", tween: [ "style", "${_luz-04}", "opacity", '1', { fromValue: '0.181318998336792'}], position: 600, duration: 200 },
            { id: "eid70", tween: [ "style", "${_luz-04}", "opacity", '0', { fromValue: '1'}], position: 800, duration: 2300 },
            { id: "eid71", tween: [ "style", "${_luz-04}", "opacity", '1', { fromValue: '0'}], position: 3100, duration: 200 },
            { id: "eid76", tween: [ "style", "${_luz-04}", "opacity", '0', { fromValue: '1'}], position: 3400, duration: 200 },
            { id: "eid67", tween: [ "style", "${_luz-06}", "left", '253px', { fromValue: '253px'}], position: 4000, duration: 0 },
            { id: "eid84", tween: [ "style", "${_luz-06}", "left", '253px', { fromValue: '253px'}], position: 12000, duration: 0 },
            { id: "eid87", tween: [ "style", "${_luz-06}", "left", '253px', { fromValue: '253px'}], position: 12100, duration: 0 },
            { id: "eid90", tween: [ "style", "${_luz-06}", "left", '253px', { fromValue: '253px'}], position: 12300, duration: 0 },
            { id: "eid79", tween: [ "style", "${_luz-06}", "opacity", '1', { fromValue: '0'}], position: 4000, duration: 200 },
            { id: "eid80", tween: [ "style", "${_luz-06}", "opacity", '0', { fromValue: '1'}], position: 4200, duration: 100 },
            { id: "eid81", tween: [ "style", "${_luz-06}", "opacity", '1', { fromValue: '0'}], position: 4300, duration: 200 },
            { id: "eid82", tween: [ "style", "${_luz-06}", "opacity", '0', { fromValue: '1'}], position: 4500, duration: 100 },
            { id: "eid83", tween: [ "style", "${_luz-06}", "opacity", '1', { fromValue: '0'}], position: 4600, duration: 200 },
            { id: "eid88", tween: [ "style", "${_luz-06}", "opacity", '1', { fromValue: '1'}], position: 4800, duration: 0 },
            { id: "eid89", tween: [ "style", "${_luz-06}", "opacity", '0', { fromValue: '1'}], position: 12000, duration: 100 },
            { id: "eid91", tween: [ "style", "${_luz-06}", "opacity", '1', { fromValue: '0'}], position: 12100, duration: 200 },
            { id: "eid92", tween: [ "style", "${_luz-06}", "opacity", '0', { fromValue: '1'}], position: 12300, duration: 100 },
            { id: "eid93", tween: [ "style", "${_luz-06}", "opacity", '1', { fromValue: '0'}], position: 12400, duration: 200 },
            { id: "eid94", tween: [ "style", "${_luz-06}", "opacity", '0', { fromValue: '1'}], position: 12600, duration: 100 },
            { id: "eid96", tween: [ "style", "${_luz-05}", "opacity", '1', { fromValue: '0'}], position: 13000, duration: 1000 },
            { id: "eid97", tween: [ "style", "${_luz-05}", "opacity", '0', { fromValue: '1'}], position: 14000, duration: 1000 },
            { id: "eid98", tween: [ "style", "${_luz-05}", "opacity", '1', { fromValue: '0'}], position: 15000, duration: 2000 },
            { id: "eid99", tween: [ "style", "${_luz-05}", "opacity", '0', { fromValue: '1'}], position: 17000, duration: 1000 },
            { id: "eid100", tween: [ "style", "${_luz-05}", "opacity", '1', { fromValue: '0'}], position: 18000, duration: 2000 },
            { id: "eid102", tween: [ "style", "${_luz-05}", "opacity", '1', { fromValue: '1'}], position: 20000, duration: 0 },
            { id: "eid103", tween: [ "style", "${_luz-05}", "opacity", '0', { fromValue: '1'}], position: 46000, duration: 1000 },
            { id: "eid104", tween: [ "style", "${_luz-05}", "opacity", '1', { fromValue: '0'}], position: 47000, duration: 1000 },
            { id: "eid105", tween: [ "style", "${_luz-05}", "opacity", '0', { fromValue: '1'}], position: 48000, duration: 1000 },
            { id: "eid107", tween: [ "style", "${_luz-05}", "opacity", '0', { fromValue: '0'}], position: 50000, duration: 0 }         ]
      }
   }
},
"luz": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      type: 'rect',
      rect: ['230px','-99px','88px','656px','auto','auto'],
      transform: [[0,0],['-34']],
      id: 'Rectangle3',
      opacity: 0.072625698324022,
      stroke: [0,'rgb(0, 0, 0)','none'],
      fill: ['rgba(241,237,206,1)']
   },
   {
      transform: [[0,0],['-34']],
      rect: ['176px','-71px','88px','656px','auto','auto'],
      id: 'Rectangle3Copy',
      stroke: [0,'rgb(0, 0, 0)','none'],
      type: 'rect',
      fill: ['rgba(241,237,206,1)']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_Rectangle3}": [
            ["style", "top", '-99px'],
            ["transform", "rotateZ", '-34deg'],
            ["style", "height", '656px'],
            ["style", "opacity", '0.23463687150838'],
            ["style", "left", '230px'],
            ["style", "overflow", 'hidden']
         ],
         "${_Rectangle3Copy}": [
            ["style", "top", '-71px'],
            ["transform", "rotateZ", '-34deg'],
            ["style", "height", '656px'],
            ["style", "opacity", '0.11173184357542'],
            ["style", "left", '176px']
         ],
         "${symbolSelector}": [
            ["style", "height", '514px'],
            ["style", "width", '432px'],
            ["style", "overflow", 'hidden']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 2000,
         autoPlay: true,
         timeline: [
            { id: "eid82", tween: [ "style", "${_Rectangle3Copy}", "opacity", '0', { fromValue: '0.21787709497207'}], position: 0, duration: 2000 },
            { id: "eid79", tween: [ "style", "${_Rectangle3}", "opacity", '0.07262569665908813', { fromValue: '0.23463687150838'}], position: 0, duration: 1000 },
            { id: "eid80", tween: [ "style", "${_Rectangle3Copy}", "left", '176px', { fromValue: '176px'}], position: 0, duration: 0 },
            { id: "eid77", tween: [ "style", "${_Rectangle3}", "left", '230px', { fromValue: '230px'}], position: 0, duration: 0 }         ]
      }
   }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "CATE-10010001");
