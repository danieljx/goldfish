/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='files/CATE-10010006/images/';

var fonts = {};


var resources = [
];
var symbols = {
"stage": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
         dom: [
         {
            id:'oficina2-01',
            type:'image',
            rect:['0','0','760px','465px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"oficina2-01.jpg",'0px','0px']
         },
         {
            id:'pantalla',
            type:'rect',
            rect:['458','165','auto','auto','auto','auto']
         },
         {
            id:'luz',
            type:'rect',
            rect:['144px','130px','auto','auto','auto','auto']
         },
         {
            id:'luzCopy',
            type:'rect',
            rect:['132px','102px','auto','auto','auto','auto']
         },
         {
            id:'luzCopy3',
            type:'rect',
            rect:['151px','142px','auto','auto','auto','auto']
         },
         {
            id:'luzCopy2',
            type:'rect',
            rect:['176px','188px','auto','auto','auto','auto']
         },
         {
            id:'reloj',
            type:'rect',
            rect:['518px','77px','auto','auto','auto','auto'],
            overflow:'hidden'
         }],
         symbolInstances: [
         {
            id:'luzCopy',
            symbolName:'luz'
         },
         {
            id:'pantalla',
            symbolName:'pantalla'
         },
         {
            id:'luzCopy2',
            symbolName:'luz'
         },
         {
            id:'reloj',
            symbolName:'reloj'
         },
         {
            id:'luzCopy3',
            symbolName:'luz'
         },
         {
            id:'luz',
            symbolName:'luz'
         }
         ]
      },
   states: {
      "Base State": {
         "${_reloj}": [
            ["style", "top", '77px'],
            ["style", "left", '518px'],
            ["style", "overflow", 'hidden']
         ],
         "${_luz}": [
            ["style", "left", '144px'],
            ["style", "top", '130px']
         ],
         "${_luzCopy3}": [
            ["style", "left", '151px'],
            ["style", "top", '142px']
         ],
         "${_Stage}": [
            ["color", "background-color", 'rgba(255,255,255,1)'],
            ["style", "width", '760px'],
            ["style", "height", '465px'],
            ["style", "overflow", 'hidden']
         ],
         "${_luzCopy}": [
            ["style", "left", '132px'],
            ["style", "top", '102px']
         ],
         "${_luzCopy2}": [
            ["style", "left", '176px'],
            ["style", "top", '188px']
         ],
         "${_oficina2-01}": [
            ["style", "height", '465px'],
            ["style", "width", '760px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 0,
         autoPlay: true,
         timeline: [
         ]
      }
   }
},
"pantalla": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'logo_catedra2',
      type: 'image',
      rect: ['6px','18px','71px','19px','auto','auto'],
      fill: ['rgba(0,0,0,0)',im+'logo_catedra2.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_logo_catedra2}": [
            ["style", "top", '18px'],
            ["style", "height", '19px'],
            ["style", "opacity", '1'],
            ["style", "left", '6px'],
            ["style", "width", '71px']
         ],
         "${symbolSelector}": [
            ["style", "height", '56px'],
            ["style", "width", '93px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 1550,
         autoPlay: true,
         timeline: [
            { id: "eid1", tween: [ "style", "${_logo_catedra2}", "left", '6px', { fromValue: '6px'}], position: 0, duration: 0 },
            { id: "eid2", tween: [ "style", "${_logo_catedra2}", "opacity", '0', { fromValue: '1'}], position: 0, duration: 1550 }         ]
      }
   }
},
"luz": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      rect: ['3px','0px','147px','115px','auto','auto'],
      id: 'luz-01',
      opacity: 0.35164835164835,
      type: 'image',
      fill: ['rgba(0,0,0,0)',im+'luz-01.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_luz-01}": [
            ["style", "top", '0px'],
            ["style", "opacity", '0.35164835164835'],
            ["style", "left", '3px']
         ],
         "${symbolSelector}": [
            ["style", "height", '126px'],
            ["style", "width", '154px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 2800,
         autoPlay: true,
         timeline: [
            { id: "eid4", tween: [ "style", "${_luz-01}", "opacity", '1', { fromValue: '0.35164836049079895'}], position: 0, duration: 2800 },
            { id: "eid3", tween: [ "style", "${_luz-01}", "left", '3px', { fromValue: '3px'}], position: 0, duration: 0 }         ]
      }
   }
},
"reloj": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'reloj-02',
      type: 'image',
      rect: ['0','0','251px','26px','auto','auto'],
      fill: ['rgba(0,0,0,0)',im+'reloj-02.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_reloj-02}": [
            ["style", "left", '0px']
         ],
         "${symbolSelector}": [
            ["style", "height", '25px'],
            ["style", "width", '25px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 3600,
         autoPlay: true,
         timeline: [
            { id: "eid7", tween: [ "style", "${_reloj-02}", "left", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid8", tween: [ "style", "${_reloj-02}", "left", '-25px', { fromValue: '0px'}], position: 400, duration: 0 },
            { id: "eid9", tween: [ "style", "${_reloj-02}", "left", '-50px', { fromValue: '-25px'}], position: 800, duration: 0 },
            { id: "eid10", tween: [ "style", "${_reloj-02}", "left", '-75px', { fromValue: '-50px'}], position: 1200, duration: 0 },
            { id: "eid11", tween: [ "style", "${_reloj-02}", "left", '-100px', { fromValue: '-75px'}], position: 1600, duration: 0 },
            { id: "eid12", tween: [ "style", "${_reloj-02}", "left", '-125px', { fromValue: '-100px'}], position: 2000, duration: 0 },
            { id: "eid13", tween: [ "style", "${_reloj-02}", "left", '-150px', { fromValue: '-125px'}], position: 2400, duration: 0 },
            { id: "eid14", tween: [ "style", "${_reloj-02}", "left", '-175px', { fromValue: '-150px'}], position: 2800, duration: 0 },
            { id: "eid15", tween: [ "style", "${_reloj-02}", "left", '-200px', { fromValue: '-175px'}], position: 3200, duration: 0 },
            { id: "eid16", tween: [ "style", "${_reloj-02}", "left", '-225px', { fromValue: '-200px'}], position: 3600, duration: 0 }         ]
      }
   }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "CATE-10010006");
