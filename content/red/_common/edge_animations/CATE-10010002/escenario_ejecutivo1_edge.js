/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='files/CATE-10010002/images/';

var fonts = {};


var resources = [
];
var symbols = {
"stage": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
         dom: [
         {
            id:'escejecuti1-012',
            type:'image',
            rect:['0','0','760px','465px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"escejecuti1-012.jpg",'0px','0px']
         },
         {
            id:'luz',
            type:'rect',
            rect:['534px','29px','auto','auto','auto','auto']
         },
         {
            id:'luzCopy',
            type:'rect',
            rect:['380px','43px','auto','auto','auto','auto']
         },
         {
            id:'luzCopy2',
            type:'rect',
            rect:['226px','58px','auto','auto','auto','auto']
         },
         {
            id:'luzCopy3',
            type:'rect',
            rect:['545px','162px','auto','auto','auto','auto']
         },
         {
            id:'luzCopy4',
            type:'rect',
            rect:['380px','170px','auto','auto','auto','auto']
         },
         {
            id:'luzCopy5',
            type:'rect',
            rect:['37px','170px','auto','auto','auto','auto']
         }],
         symbolInstances: [
         {
            id:'luzCopy',
            symbolName:'luz'
         },
         {
            id:'luz',
            symbolName:'luz'
         },
         {
            id:'luzCopy2',
            symbolName:'luz'
         },
         {
            id:'luzCopy4',
            symbolName:'luz'
         },
         {
            id:'luzCopy3',
            symbolName:'luz'
         },
         {
            id:'luzCopy5',
            symbolName:'luz'
         }
         ]
      },
   states: {
      "Base State": {
         "${_luzCopy5}": [
            ["style", "left", '37px'],
            ["style", "top", '170px']
         ],
         "${_escejecuti1-012}": [
            ["style", "height", '465px'],
            ["style", "width", '760px']
         ],
         "${_luz}": [
            ["style", "left", '534px'],
            ["style", "top", '29px']
         ],
         "${_luzCopy4}": [
            ["style", "left", '380px'],
            ["style", "top", '170px']
         ],
         "${_Stage}": [
            ["color", "background-color", 'rgba(255,255,255,1)'],
            ["style", "width", '760px'],
            ["style", "height", '465px'],
            ["style", "overflow", 'hidden']
         ],
         "${_luzCopy}": [
            ["style", "left", '380px'],
            ["style", "top", '43px']
         ],
         "${_luzCopy3}": [
            ["style", "left", '545px'],
            ["style", "top", '162px']
         ],
         "${_luzCopy2}": [
            ["style", "left", '226px'],
            ["style", "top", '58px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 0,
         autoPlay: true,
         timeline: [
         ]
      }
   }
},
"luz": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      rect: ['3px','0px','147px','115px','auto','auto'],
      id: 'luz-01',
      opacity: 0.35164835164835,
      type: 'image',
      fill: ['rgba(0,0,0,0)',im+'luz-01.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_luz-01}": [
            ["style", "top", '0px'],
            ["style", "opacity", '0.35164835164835'],
            ["style", "left", '3px']
         ],
         "${symbolSelector}": [
            ["style", "height", '126px'],
            ["style", "width", '154px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 2800,
         autoPlay: true,
         timeline: [
            { id: "eid4", tween: [ "style", "${_luz-01}", "opacity", '1', { fromValue: '0.35164836049079895'}], position: 0, duration: 2800 },
            { id: "eid3", tween: [ "style", "${_luz-01}", "left", '3px', { fromValue: '3px'}], position: 0, duration: 0 }         ]
      }
   }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "CATE-10010002");
