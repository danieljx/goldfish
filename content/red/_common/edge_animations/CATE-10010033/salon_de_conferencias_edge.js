/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='files/CATE-10010033/images/';

var fonts = {};


var resources = [
];
var symbols = {
"stage": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
         dom: [
         {
            id:'salon_de_conferencias-01',
            type:'image',
            rect:['0','0','764px','465px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"salon_de_conferencias-01.jpg",'0px','0px']
         },
         {
            id:'luz',
            type:'rect',
            rect:['380px','43px','auto','auto','auto','auto']
         },
         {
            id:'luzCopy',
            type:'rect',
            rect:['380px','43px','auto','auto','auto','auto']
         },
         {
            id:'luzCopy2',
            type:'rect',
            rect:['380px','43px','auto','auto','auto','auto']
         },
         {
            id:'luzCopy6',
            type:'rect',
            rect:['380px','43px','auto','auto','auto','auto']
         },
         {
            id:'luzCopy3',
            type:'rect',
            rect:['380px','43px','auto','auto','auto','auto']
         },
         {
            id:'luzCopy4',
            type:'rect',
            rect:['380px','43px','auto','auto','auto','auto']
         }],
         symbolInstances: [
         {
            id:'luzCopy',
            symbolName:'luz'
         },
         {
            id:'luz',
            symbolName:'luz'
         },
         {
            id:'luzCopy2',
            symbolName:'luz'
         },
         {
            id:'luzCopy6',
            symbolName:'luz'
         },
         {
            id:'luzCopy3',
            symbolName:'luz'
         },
         {
            id:'luzCopy4',
            symbolName:'luz'
         }
         ]
      },
   states: {
      "Base State": {
         "${_luzCopy4}": [
            ["style", "left", '652px'],
            ["style", "top", '-12px']
         ],
         "${_luzCopy2}": [
            ["style", "left", '98px'],
            ["style", "top", '-19px']
         ],
         "${_salon_de_conferencias-01}": [
            ["style", "height", '465px'],
            ["style", "width", '764px']
         ],
         "${_luzCopy3}": [
            ["style", "top", '27px'],
            ["transform", "scaleY", '1.47619'],
            ["style", "left", '545px'],
            ["transform", "scaleX", '1.47619']
         ],
         "${_Stage}": [
            ["color", "background-color", 'rgba(255,255,255,1)'],
            ["style", "width", '760px'],
            ["style", "height", '465px'],
            ["style", "overflow", 'hidden']
         ],
         "${_luzCopy6}": [
            ["style", "left", '321px'],
            ["style", "top", '13px']
         ],
         "${_luz}": [
            ["style", "left", '340px'],
            ["style", "top", '27px']
         ],
         "${_luzCopy}": [
            ["style", "left", '228px'],
            ["style", "top", '-12px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 0,
         autoPlay: true,
         timeline: [
            { id: "eid38", tween: [ "transform", "${_luzCopy3}", "scaleY", '1.47619', { fromValue: '1.47619'}], position: 0, duration: 0 },
            { id: "eid46", tween: [ "style", "${_luzCopy6}", "top", '13px', { fromValue: '13px'}], position: 0, duration: 0 },
            { id: "eid45", tween: [ "style", "${_luzCopy6}", "left", '321px', { fromValue: '321px'}], position: 0, duration: 0 },
            { id: "eid12", tween: [ "style", "${_luzCopy4}", "top", '-12px', { fromValue: '-12px'}], position: 0, duration: 0 },
            { id: "eid31", tween: [ "style", "${_luz}", "top", '27px', { fromValue: '27px'}], position: 0, duration: 0 },
            { id: "eid37", tween: [ "transform", "${_luzCopy3}", "scaleX", '1.47619', { fromValue: '1.47619'}], position: 0, duration: 0 },
            { id: "eid30", tween: [ "style", "${_luz}", "left", '340px', { fromValue: '340px'}], position: 0, duration: 0 },
            { id: "eid42", tween: [ "style", "${_luzCopy3}", "top", '27px', { fromValue: '27px'}], position: 0, duration: 0 },
            { id: "eid11", tween: [ "style", "${_luzCopy4}", "left", '652px', { fromValue: '652px'}], position: 0, duration: 0 },
            { id: "eid35", tween: [ "style", "${_luzCopy2}", "top", '-19px', { fromValue: '-19px'}], position: 0, duration: 0 },
            { id: "eid33", tween: [ "style", "${_luzCopy}", "top", '-12px', { fromValue: '-12px'}], position: 0, duration: 0 },
            { id: "eid34", tween: [ "style", "${_luzCopy2}", "left", '98px', { fromValue: '98px'}], position: 0, duration: 0 },
            { id: "eid41", tween: [ "style", "${_luzCopy3}", "left", '545px', { fromValue: '545px'}], position: 0, duration: 0 },
            { id: "eid32", tween: [ "style", "${_luzCopy}", "left", '228px', { fromValue: '228px'}], position: 0, duration: 0 }         ]
      }
   }
},
"luz": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      rect: ['3px','0px','147px','115px','auto','auto'],
      id: 'luz-01',
      opacity: 0.35164835164835,
      type: 'image',
      fill: ['rgba(0,0,0,0)',im+'luz-01.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_luz-01}": [
            ["style", "top", '0px'],
            ["style", "opacity", '0'],
            ["style", "left", '3px']
         ],
         "${symbolSelector}": [
            ["style", "height", '126px'],
            ["style", "width", '154px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 2800,
         autoPlay: true,
         timeline: [
            { id: "eid4", tween: [ "style", "${_luz-01}", "opacity", '1', { fromValue: '0'}], position: 0, duration: 2800 },
            { id: "eid3", tween: [ "style", "${_luz-01}", "left", '3px', { fromValue: '3px'}], position: 0, duration: 0 }         ]
      }
   }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "CATE-10010033");
