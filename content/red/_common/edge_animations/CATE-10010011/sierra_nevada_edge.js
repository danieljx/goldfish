/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='files/CATE-10010011/images/';

var fonts = {};


var resources = [
];
var symbols = {
"stage": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
         dom: [
         {
            id:'escenario_sierra_nevada-01',
            type:'image',
            rect:['0px','0px','760px','465px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"escenario%20sierra%20nevada-01.jpg",'0px','0px']
         },
         {
            id:'avesCopy2',
            type:'rect',
            rect:['79px','-20px','auto','auto','auto','auto'],
            transform:[[],[],[],['0.1609','0.1609']]
         },
         {
            id:'avesCopy3',
            type:'rect',
            rect:['79px','-20px','auto','auto','auto','auto'],
            transform:[[],[],[],['0.1609','0.1609']]
         },
         {
            id:'Symbol_1Copy5',
            type:'rect',
            rect:['-122px','379px','auto','auto','auto','auto'],
            opacity:0.38578680203046,
            transform:[[],[],[],['0.39839','0.53464']]
         },
         {
            id:'luz',
            type:'rect',
            rect:['-33','-19','auto','auto','auto','auto'],
            transform:[[],[],[],['0.82994','0.63273']]
         }],
         symbolInstances: [
         {
            id:'avesCopy3',
            symbolName:'aves'
         },
         {
            id:'Symbol_1Copy5',
            symbolName:'Symbol_1'
         },
         {
            id:'luz',
            symbolName:'luz'
         },
         {
            id:'avesCopy2',
            symbolName:'aves'
         }
         ]
      },
   states: {
      "Base State": {
         "${_avesCopy3}": [
            ["style", "top", '-56px'],
            ["transform", "scaleY", '0.07736'],
            ["style", "left", '-31px'],
            ["transform", "scaleX", '0.07736']
         ],
         "${_Symbol_1Copy5}": [
            ["style", "top", '379px'],
            ["transform", "scaleY", '0.53464'],
            ["transform", "scaleX", '0.39839'],
            ["style", "opacity", '0.3857868015766144'],
            ["style", "left", '-122px']
         ],
         "${_Stage}": [
            ["color", "background-color", 'rgba(255,255,255,1)'],
            ["style", "width", '760px'],
            ["style", "height", '465px'],
            ["style", "overflow", 'hidden']
         ],
         "${_luz}": [
            ["style", "top", '-94px'],
            ["transform", "scaleX", '0.82994'],
            ["transform", "scaleY", '0.63273'],
            ["style", "left", '-37px']
         ],
         "${_avesCopy2}": [
            ["transform", "scaleX", '0.07736'],
            ["style", "top", '-2px'],
            ["style", "left", '12px'],
            ["transform", "scaleY", '0.07736']
         ],
         "${_escenario_sierra_nevada-01}": [
            ["style", "height", '465px'],
            ["style", "top", '0px'],
            ["style", "left", '0px'],
            ["style", "width", '760px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 47500,
         autoPlay: true,
         timeline: [
            { id: "eid83", tween: [ "style", "${_luz}", "left", '-37px', { fromValue: '-37px'}], position: 0, duration: 0 },
            { id: "eid22", tween: [ "style", "${_avesCopy3}", "top", '-11px', { fromValue: '-56px'}], position: 0, duration: 17 },
            { id: "eid67", tween: [ "style", "${_avesCopy3}", "top", '-91px', { fromValue: '-11px'}], position: 17, duration: 29983 },
            { id: "eid24", tween: [ "style", "${_avesCopy2}", "top", '122px', { fromValue: '-2px'}], position: 0, duration: 17 },
            { id: "eid68", tween: [ "style", "${_avesCopy2}", "top", '-104px', { fromValue: '122px'}], position: 17, duration: 47483 },
            { id: "eid21", tween: [ "style", "${_avesCopy3}", "left", '-29px', { fromValue: '-31px'}], position: 0, duration: 17 },
            { id: "eid66", tween: [ "style", "${_avesCopy3}", "left", '684px', { fromValue: '-29px'}], position: 17, duration: 29983 },
            { id: "eid84", tween: [ "style", "${_luz}", "top", '-94px', { fromValue: '-94px'}], position: 0, duration: 0 },
            { id: "eid23", tween: [ "style", "${_avesCopy2}", "left", '-29px', { fromValue: '12px'}], position: 0, duration: 17 },
            { id: "eid69", tween: [ "style", "${_avesCopy2}", "left", '602px', { fromValue: '-29px'}], position: 17, duration: 47483 }         ]
      }
   }
},
"aves": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'pajaro-01',
      type: 'image',
      rect: ['0','0','600px','127px','auto','auto'],
      fill: ['rgba(0,0,0,0)',im+'pajaro-01.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_pajaro-01}": [
            ["style", "left", '0px']
         ],
         "${symbolSelector}": [
            ["style", "height", '169px'],
            ["style", "overflow", 'hidden'],
            ["style", "width", '152px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 2600,
         autoPlay: true,
         timeline: [
            { id: "eid1", tween: [ "style", "${_pajaro-01}", "left", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid2", tween: [ "style", "${_pajaro-01}", "left", '-150px', { fromValue: '0px'}], position: 100, duration: 0 },
            { id: "eid4", tween: [ "style", "${_pajaro-01}", "left", '-300px', { fromValue: '-150px'}], position: 200, duration: 0 },
            { id: "eid5", tween: [ "style", "${_pajaro-01}", "left", '-450px', { fromValue: '-300px'}], position: 300, duration: 0 }         ]
      }
   }
},
"arbol": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'pajaro-02',
      type: 'image',
      rect: ['0','0','1059px','118px','auto','auto'],
      fill: ['rgba(0,0,0,0)',im+'pajaro-02.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_pajaro-02}": [
            ["style", "left", '0px']
         ],
         "${symbolSelector}": [
            ["style", "height", '127px'],
            ["style", "overflow", 'hidden'],
            ["style", "width", '42.5%']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 10000,
         autoPlay: true,
         labels: {
            "arbol": 0
         },
         timeline: [
            { id: "eid25", tween: [ "style", "${_pajaro-02}", "left", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid28", tween: [ "style", "${_pajaro-02}", "left", '-353px', { fromValue: '0px'}], position: 2500, duration: 0 },
            { id: "eid30", tween: [ "style", "${_pajaro-02}", "left", '-713px', { fromValue: '-353px'}], position: 5000, duration: 0 },
            { id: "eid37", tween: [ "style", "${_pajaro-02}", "left", '-361px', { fromValue: '-713px'}], position: 7500, duration: 0 },
            { id: "eid38", tween: [ "style", "${_pajaro-02}", "left", '-5px', { fromValue: '-361px'}], position: 10000, duration: 0 }         ]
      }
   }
},
"Symbol_1": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'pajaro-023',
      type: 'image',
      rect: ['0','0','1059px','118px','auto','auto'],
      fill: ['rgba(0,0,0,0)',im+'pajaro-023.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_pajaro-023}": [
            ["style", "left", '0px']
         ],
         "${symbolSelector}": [
            ["style", "height", '113px'],
            ["style", "overflow", 'hidden'],
            ["style", "width", '359px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 300,
         autoPlay: true,
         timeline: [
            { id: "eid60", tween: [ "style", "${_pajaro-023}", "left", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid61", tween: [ "style", "${_pajaro-023}", "left", '-353px', { fromValue: '0px'}], position: 100, duration: 0 },
            { id: "eid62", tween: [ "style", "${_pajaro-023}", "left", '-706px', { fromValue: '-353px'}], position: 200, duration: 0 }         ]
      }
   }
},
"luz": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      transform: [[0,0],['-34']],
      rect: ['230px','-99px','88px','656px','auto','auto'],
      type: 'rect',
      id: 'Rectangle3',
      stroke: [0,'rgb(0, 0, 0)','none'],
      opacity: 0.072625698324022,
      fill: ['rgba(241,237,206,1)']
   },
   {
      transform: [[0,0],['-34']],
      rect: ['176px','-71px','88px','656px','auto','auto'],
      id: 'Rectangle3Copy',
      stroke: [0,'rgb(0, 0, 0)','none'],
      type: 'rect',
      fill: ['rgba(241,237,206,1)']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_Rectangle3}": [
            ["style", "top", '-99px'],
            ["transform", "rotateZ", '-34deg'],
            ["style", "height", '656px'],
            ["style", "opacity", '0.23463687150838'],
            ["style", "left", '230px'],
            ["style", "overflow", 'hidden']
         ],
         "${_Rectangle3Copy}": [
            ["style", "top", '-71px'],
            ["transform", "rotateZ", '-34deg'],
            ["style", "height", '656px'],
            ["style", "opacity", '0.11173184357542'],
            ["style", "left", '176px']
         ],
         "${symbolSelector}": [
            ["style", "height", '514px'],
            ["style", "width", '432px'],
            ["style", "overflow", 'hidden']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 2000,
         autoPlay: true,
         timeline: [
            { id: "eid82", tween: [ "style", "${_Rectangle3Copy}", "opacity", '0', { fromValue: '0.21787709497207'}], position: 0, duration: 2000 },
            { id: "eid79", tween: [ "style", "${_Rectangle3}", "opacity", '0.07262569665908813', { fromValue: '0.23463687150838'}], position: 0, duration: 1000 },
            { id: "eid80", tween: [ "style", "${_Rectangle3Copy}", "left", '176px', { fromValue: '176px'}], position: 0, duration: 0 },
            { id: "eid77", tween: [ "style", "${_Rectangle3}", "left", '230px', { fromValue: '230px'}], position: 0, duration: 0 }         ]
      }
   }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "CATE-10010011");
