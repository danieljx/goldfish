/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='files/CATE-10010036/images/';

var fonts = {};


var resources = [
];
var symbols = {
"stage": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
         dom: [
         {
            id:'biblioteca_sala-01',
            type:'image',
            rect:['0','0','760px','465px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"biblioteca_sala-01.jpg",'0px','0px']
         },
         {
            id:'luz',
            type:'rect',
            rect:['-33','-19','auto','auto','auto','auto'],
            transform:[[],[],[],['0.82994','0.63273']]
         },
         {
            id:'luzCopy',
            type:'rect',
            rect:['-33','-19','auto','auto','auto','auto'],
            transform:[[],[],[],['0.82994','0.63273']]
         },
         {
            id:'lapiz',
            type:'rect',
            rect:['182','170','auto','auto','auto','auto']
         }],
         symbolInstances: [
         {
            id:'lapiz',
            symbolName:'lapiz'
         },
         {
            id:'luzCopy',
            symbolName:'luz'
         },
         {
            id:'luz',
            symbolName:'luz'
         }
         ]
      },
   states: {
      "Base State": {
         "${_Stage}": [
            ["color", "background-color", 'rgba(255,255,255,1)'],
            ["style", "width", '760px'],
            ["style", "height", '465px'],
            ["style", "overflow", 'hidden']
         ],
         "${_luzCopy}": [
            ["style", "top", '-24px'],
            ["transform", "scaleY", '1.87038'],
            ["transform", "rotateZ", '-86deg'],
            ["transform", "scaleX", '0.83717'],
            ["style", "left", '-281px']
         ],
         "${_luz}": [
            ["style", "top", '39px'],
            ["transform", "scaleY", '2.47682'],
            ["transform", "rotateZ", '-85deg'],
            ["transform", "scaleX", '1.48316'],
            ["style", "left", '156px']
         ],
         "${_biblioteca_sala-01}": [
            ["style", "height", '465px'],
            ["style", "width", '760px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 0,
         autoPlay: true,
         timeline: [
            { id: "eid75", tween: [ "transform", "${_luz}", "rotateZ", '-85deg', { fromValue: '-85deg'}], position: 0, duration: 0 },
            { id: "eid92", tween: [ "transform", "${_luz}", "scaleY", '2.47682', { fromValue: '2.47682'}], position: 0, duration: 0 },
            { id: "eid94", tween: [ "style", "${_luz}", "top", '39px', { fromValue: '39px'}], position: 0, duration: 0 },
            { id: "eid93", tween: [ "style", "${_luz}", "left", '156px', { fromValue: '156px'}], position: 0, duration: 0 },
            { id: "eid91", tween: [ "transform", "${_luz}", "scaleX", '1.48316', { fromValue: '1.48316'}], position: 0, duration: 0 },
            { id: "eid70", tween: [ "transform", "${_luzCopy}", "rotateZ", '-86deg', { fromValue: '-86deg'}], position: 0, duration: 0 },
            { id: "eid74", tween: [ "style", "${_luzCopy}", "top", '-24px', { fromValue: '-24px'}], position: 0, duration: 0 },
            { id: "eid63", tween: [ "transform", "${_luzCopy}", "scaleY", '1.87038', { fromValue: '1.87038'}], position: 0, duration: 0 },
            { id: "eid62", tween: [ "transform", "${_luzCopy}", "scaleX", '0.83717', { fromValue: '0.83717'}], position: 0, duration: 0 },
            { id: "eid73", tween: [ "style", "${_luzCopy}", "left", '-281px', { fromValue: '-281px'}], position: 0, duration: 0 }         ]
      }
   }
},
"luz": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      transform: [[0,0],['-34']],
      rect: ['230px','-99px','88px','656px','auto','auto'],
      type: 'rect',
      id: 'Rectangle3',
      stroke: [0,'rgb(0, 0, 0)','none'],
      opacity: 0.072625698324022,
      fill: ['rgba(241,237,206,1)']
   },
   {
      transform: [[0,0],['-34']],
      rect: ['176px','-71px','88px','656px','auto','auto'],
      id: 'Rectangle3Copy',
      stroke: [0,'rgb(0, 0, 0)','none'],
      type: 'rect',
      fill: ['rgba(241,237,206,1)']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_Rectangle3}": [
            ["style", "top", '-99px'],
            ["transform", "rotateZ", '-34deg'],
            ["style", "height", '656px'],
            ["style", "opacity", '0.23463687150838'],
            ["style", "left", '230px'],
            ["style", "overflow", 'hidden']
         ],
         "${_Rectangle3Copy}": [
            ["style", "top", '-71px'],
            ["transform", "rotateZ", '-34deg'],
            ["style", "height", '656px'],
            ["style", "opacity", '0.11173184357542'],
            ["style", "left", '176px']
         ],
         "${symbolSelector}": [
            ["style", "height", '514px'],
            ["style", "width", '432px'],
            ["style", "overflow", 'hidden']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 2000,
         autoPlay: true,
         timeline: [
            { id: "eid82", tween: [ "style", "${_Rectangle3Copy}", "opacity", '0', { fromValue: '0.21787709497207'}], position: 0, duration: 2000 },
            { id: "eid79", tween: [ "style", "${_Rectangle3}", "opacity", '0.07262569665908813', { fromValue: '0.23463687150838'}], position: 0, duration: 1000 },
            { id: "eid80", tween: [ "style", "${_Rectangle3Copy}", "left", '176px', { fromValue: '176px'}], position: 0, duration: 0 },
            { id: "eid77", tween: [ "style", "${_Rectangle3}", "left", '230px', { fromValue: '230px'}], position: 0, duration: 0 }         ]
      }
   }
},
"lapiz": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'biblioteca_sala-02',
      type: 'image',
      rect: ['0','0','8px','11px','auto','auto'],
      fill: ['rgba(0,0,0,0)',im+'biblioteca_sala-02.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_biblioteca_sala-02}": [
            ["style", "left", '0px'],
            ["style", "top", '1px']
         ],
         "${symbolSelector}": [
            ["style", "height", '21px'],
            ["style", "width", '20px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 2455,
         autoPlay: true,
         timeline: [
            { id: "eid101", tween: [ "style", "${_biblioteca_sala-02}", "top", '1px', { fromValue: '1px'}], position: 1227, duration: 0 },
            { id: "eid98", tween: [ "style", "${_biblioteca_sala-02}", "left", '2px', { fromValue: '0px'}], position: 0, duration: 614 },
            { id: "eid100", tween: [ "style", "${_biblioteca_sala-02}", "left", '4px', { fromValue: '2px'}], position: 614, duration: 614 },
            { id: "eid103", tween: [ "style", "${_biblioteca_sala-02}", "left", '2px', { fromValue: '4px'}], position: 1227, duration: 614 },
            { id: "eid105", tween: [ "style", "${_biblioteca_sala-02}", "left", '0px', { fromValue: '2px'}], position: 1841, duration: 614 }         ]
      }
   }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "CATE-10010036");
