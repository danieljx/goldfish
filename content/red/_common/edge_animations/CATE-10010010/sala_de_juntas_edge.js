/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='files/CATE-10010010/images/';

var fonts = {};


var resources = [
];
var symbols = {
"stage": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
         dom: [
         {
            id:'sala_de_juntas-01',
            type:'image',
            rect:['0','0','760px','465px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"sala_de_juntas-01.jpg",'0px','0px']
         },
         {
            id:'tele',
            type:'rect',
            rect:['420','85','auto','auto','auto','auto'],
            overflow:'hidden',
            opacity:0.16463414634146
         },
         {
            id:'luz',
            type:'rect',
            rect:['380px','43px','auto','auto','auto','auto']
         },
         {
            id:'luzCopy',
            type:'rect',
            rect:['380px','43px','auto','auto','auto','auto']
         },
         {
            id:'luzCopy2',
            type:'rect',
            rect:['380px','43px','auto','auto','auto','auto']
         }],
         symbolInstances: [
         {
            id:'tele',
            symbolName:'tele'
         },
         {
            id:'luzCopy',
            symbolName:'luz'
         },
         {
            id:'luz',
            symbolName:'luz'
         },
         {
            id:'luzCopy2',
            symbolName:'luz'
         }
         ]
      },
   states: {
      "Base State": {
         "${_luz}": [
            ["style", "left", '578px'],
            ["style", "top", '22px']
         ],
         "${_sala_de_juntas-01}": [
            ["style", "height", '465px'],
            ["style", "width", '760px']
         ],
         "${_Stage}": [
            ["color", "background-color", 'rgba(255,255,255,1)'],
            ["style", "width", '760px'],
            ["style", "height", '465px'],
            ["style", "overflow", 'hidden']
         ],
         "${_luzCopy}": [
            ["style", "left", '578px'],
            ["style", "top", '22px']
         ],
         "${_tele}": [
            ["style", "opacity", '0.16463414634146'],
            ["style", "overflow", 'hidden']
         ],
         "${_luzCopy2}": [
            ["style", "left", '612px'],
            ["style", "top", '60px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 0,
         autoPlay: true,
         timeline: [
            { id: "eid1", tween: [ "style", "${_luz}", "left", '578px', { fromValue: '578px'}], position: 0, duration: 0 },
            { id: "eid5", tween: [ "style", "${_luzCopy2}", "left", '612px', { fromValue: '612px'}], position: 0, duration: 0 },
            { id: "eid2", tween: [ "style", "${_luz}", "top", '22px', { fromValue: '22px'}], position: 0, duration: 0 },
            { id: "eid6", tween: [ "style", "${_luzCopy2}", "top", '60px', { fromValue: '60px'}], position: 0, duration: 0 }         ]
      }
   }
},
"luz": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      rect: ['3px','0px','147px','115px','auto','auto'],
      id: 'luz-01',
      opacity: 0.35164835164835,
      type: 'image',
      fill: ['rgba(0,0,0,0)',im+'luz-01.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_luz-01}": [
            ["style", "top", '0px'],
            ["style", "opacity", '0.35164835164835'],
            ["style", "left", '3px']
         ],
         "${symbolSelector}": [
            ["style", "height", '126px'],
            ["style", "width", '154px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 2800,
         autoPlay: true,
         timeline: [
            { id: "eid4", tween: [ "style", "${_luz-01}", "opacity", '1', { fromValue: '0.35164836049079895'}], position: 0, duration: 2800 },
            { id: "eid3", tween: [ "style", "${_luz-01}", "left", '3px', { fromValue: '3px'}], position: 0, duration: 0 }         ]
      }
   }
},
"tele": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'sala_de_juntas-02',
      type: 'image',
      rect: ['0','0','371px','72px','auto','auto'],
      fill: ['rgba(0,0,0,0)',im+'sala%20de%20juntas-02.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_sala_de_juntas-02}": [
            ["style", "left", '0px']
         ],
         "${symbolSelector}": [
            ["style", "height", '67px'],
            ["style", "width", '124px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 200,
         autoPlay: true,
         timeline: [
            { id: "eid7", tween: [ "style", "${_sala_de_juntas-02}", "left", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid8", tween: [ "style", "${_sala_de_juntas-02}", "left", '-123px', { fromValue: '0px'}], position: 100, duration: 0 },
            { id: "eid10", tween: [ "style", "${_sala_de_juntas-02}", "left", '-246px', { fromValue: '-123px'}], position: 200, duration: 0 }         ]
      }
   }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "CATE-10010010");
