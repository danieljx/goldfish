/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='files/CATE-10010012/images/';

var fonts = {};


var resources = [
];
var symbols = {
"stage": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
         dom: [
         {
            id:'fondo_simon_bolivar-01',
            type:'image',
            rect:['0','0px','760px','465px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"fondo_simon_bolivar-01.jpg",'0px','0px']
         },
         {
            id:'aves',
            type:'rect',
            rect:['79px','-20px','auto','auto','auto','auto'],
            transform:[[],[],[],['0.1609','0.1609']]
         },
         {
            id:'avesCopy2',
            type:'rect',
            rect:['79px','-20px','auto','auto','auto','auto'],
            transform:[[],[],[],['0.1609','0.1609']]
         },
         {
            id:'avesCopy3',
            type:'rect',
            rect:['79px','-20px','auto','auto','auto','auto'],
            transform:[[],[],[],['0.1609','0.1609']]
         },
         {
            id:'Symbol_1',
            type:'rect',
            rect:['-63px','6px','auto','auto','auto','auto'],
            opacity:0.38578680203046,
            transform:[[],[],[],['0.32861','0.65263']]
         },
         {
            id:'Symbol_1Copy4',
            type:'rect',
            rect:['-47px','95px','auto','auto','auto','auto'],
            opacity:0.38578680203046,
            transform:[[],[],[],['0.25334','0.52561']]
         },
         {
            id:'Symbol_1Copy5',
            type:'rect',
            rect:['-121px','379px','auto','auto','auto','auto'],
            opacity:0.38578680203046,
            transform:[[],[],[],['0.44955','0.52561']]
         },
         {
            id:'Symbol_1Copy6',
            type:'rect',
            rect:['434px','109px','auto','auto','auto','auto'],
            opacity:0.38578680203046,
            transform:[[],[],[],['0.31115','0.36379']]
         }],
         symbolInstances: [
         {
            id:'Symbol_1Copy4',
            symbolName:'Symbol_1'
         },
         {
            id:'Symbol_1',
            symbolName:'Symbol_1'
         },
         {
            id:'avesCopy3',
            symbolName:'aves'
         },
         {
            id:'aves',
            symbolName:'aves'
         },
         {
            id:'Symbol_1Copy6',
            symbolName:'Symbol_1'
         },
         {
            id:'Symbol_1Copy5',
            symbolName:'Symbol_1'
         },
         {
            id:'avesCopy2',
            symbolName:'aves'
         }
         ]
      },
   states: {
      "Base State": {
         "${_avesCopy3}": [
            ["transform", "scaleX", '0.07736'],
            ["style", "left", '-31px'],
            ["transform", "scaleY", '0.07736'],
            ["style", "top", '-56px']
         ],
         "${_Symbol_1Copy6}": [
            ["style", "top", '109px'],
            ["transform", "scaleY", '0.36379'],
            ["transform", "scaleX", '0.31115'],
            ["style", "opacity", '0.3857868015766144'],
            ["style", "left", '434px']
         ],
         "${_Symbol_1Copy5}": [
            ["style", "top", '379px'],
            ["transform", "scaleY", '0.52561'],
            ["transform", "scaleX", '0.44955'],
            ["style", "opacity", '0.3857868015766144'],
            ["style", "left", '-121px']
         ],
         "${_Symbol_1}": [
            ["style", "top", '6px'],
            ["transform", "scaleY", '0.65263'],
            ["transform", "scaleX", '0.32861'],
            ["style", "opacity", '0.38578680203046'],
            ["style", "left", '-121px']
         ],
         "${_fondo_simon_bolivar-01}": [
            ["style", "height", '465px'],
            ["style", "width", '760px']
         ],
         "${_Symbol_1Copy4}": [
            ["style", "top", '95px'],
            ["transform", "scaleY", '0.52561'],
            ["transform", "scaleX", '0.25334'],
            ["style", "opacity", '0.3857868015766144'],
            ["style", "left", '-47px']
         ],
         "${_Stage}": [
            ["color", "background-color", 'rgba(255,255,255,1)'],
            ["style", "width", '760px'],
            ["style", "height", '465px'],
            ["style", "overflow", 'hidden']
         ],
         "${_aves}": [
            ["style", "top", '-15px'],
            ["transform", "scaleY", '0.07736'],
            ["style", "left", '35px'],
            ["transform", "scaleX", '0.07736']
         ],
         "${_avesCopy2}": [
            ["style", "top", '-2px'],
            ["transform", "scaleX", '0.07736'],
            ["transform", "scaleY", '0.07736'],
            ["style", "left", '12px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 47500,
         autoPlay: true,
         timeline: [
            { id: "eid9", tween: [ "style", "${_aves}", "top", '-54px', { fromValue: '-15px'}], position: 0, duration: 30000 },
            { id: "eid8", tween: [ "style", "${_aves}", "left", '716px', { fromValue: '35px'}], position: 0, duration: 30000 },
            { id: "eid15", tween: [ "transform", "${_aves}", "scaleX", '0.07736', { fromValue: '0.07736'}], position: 0, duration: 0 },
            { id: "eid63", tween: [ "style", "${_Symbol_1}", "left", '-121px', { fromValue: '-121px'}], position: 0, duration: 0 },
            { id: "eid16", tween: [ "transform", "${_aves}", "scaleY", '0.07736', { fromValue: '0.07736'}], position: 0, duration: 0 },
            { id: "eid22", tween: [ "style", "${_avesCopy3}", "top", '-91px', { fromValue: '-56px'}], position: 0, duration: 30000 },
            { id: "eid21", tween: [ "style", "${_avesCopy3}", "left", '684px', { fromValue: '-31px'}], position: 0, duration: 30000 },
            { id: "eid24", tween: [ "style", "${_avesCopy2}", "top", '-104px', { fromValue: '-2px'}], position: 0, duration: 47500 },
            { id: "eid23", tween: [ "style", "${_avesCopy2}", "left", '602px', { fromValue: '12px'}], position: 0, duration: 47500 }         ]
      }
   }
},
"aves": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'pajaro-01',
      type: 'image',
      rect: ['0','0','600px','127px','auto','auto'],
      fill: ['rgba(0,0,0,0)',im+'pajaro-01.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_pajaro-01}": [
            ["style", "left", '0px']
         ],
         "${symbolSelector}": [
            ["style", "height", '169px'],
            ["style", "overflow", 'hidden'],
            ["style", "width", '152px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 2600,
         autoPlay: true,
         timeline: [
            { id: "eid1", tween: [ "style", "${_pajaro-01}", "left", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid2", tween: [ "style", "${_pajaro-01}", "left", '-150px', { fromValue: '0px'}], position: 100, duration: 0 },
            { id: "eid4", tween: [ "style", "${_pajaro-01}", "left", '-300px', { fromValue: '-150px'}], position: 200, duration: 0 },
            { id: "eid5", tween: [ "style", "${_pajaro-01}", "left", '-450px', { fromValue: '-300px'}], position: 300, duration: 0 }         ]
      }
   }
},
"arbol": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'pajaro-02',
      type: 'image',
      rect: ['0','0','1059px','118px','auto','auto'],
      fill: ['rgba(0,0,0,0)',im+'pajaro-02.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_pajaro-02}": [
            ["style", "left", '0px']
         ],
         "${symbolSelector}": [
            ["style", "height", '127px'],
            ["style", "overflow", 'hidden'],
            ["style", "width", '42.5%']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 10000,
         autoPlay: true,
         labels: {
            "arbol": 0
         },
         timeline: [
            { id: "eid25", tween: [ "style", "${_pajaro-02}", "left", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid28", tween: [ "style", "${_pajaro-02}", "left", '-353px', { fromValue: '0px'}], position: 2500, duration: 0 },
            { id: "eid30", tween: [ "style", "${_pajaro-02}", "left", '-713px', { fromValue: '-353px'}], position: 5000, duration: 0 },
            { id: "eid37", tween: [ "style", "${_pajaro-02}", "left", '-361px', { fromValue: '-713px'}], position: 7500, duration: 0 },
            { id: "eid38", tween: [ "style", "${_pajaro-02}", "left", '-5px', { fromValue: '-361px'}], position: 10000, duration: 0 }         ]
      }
   }
},
"Symbol_1": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'pajaro-023',
      type: 'image',
      rect: ['0','0','1059px','118px','auto','auto'],
      fill: ['rgba(0,0,0,0)',im+'pajaro-023.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_pajaro-023}": [
            ["style", "left", '0px']
         ],
         "${symbolSelector}": [
            ["style", "height", '113px'],
            ["style", "overflow", 'hidden'],
            ["style", "width", '359px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 300,
         autoPlay: true,
         timeline: [
            { id: "eid60", tween: [ "style", "${_pajaro-023}", "left", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid61", tween: [ "style", "${_pajaro-023}", "left", '-353px', { fromValue: '0px'}], position: 100, duration: 0 },
            { id: "eid62", tween: [ "style", "${_pajaro-023}", "left", '-706px', { fromValue: '-353px'}], position: 200, duration: 0 }         ]
      }
   }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "CATE-10010012");
