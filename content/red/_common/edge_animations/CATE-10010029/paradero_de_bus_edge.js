/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='files/CATE-10010029/images/';

var fonts = {};


var resources = [
];
var symbols = {
"stage": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
         dom: [
         {
            id:'paradero-de-bus-01',
            type:'image',
            rect:['0','0','760px','475px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"paradero-de-bus-01.jpg",'0px','0px']
         },
         {
            id:'p4',
            type:'rect',
            rect:['0px','295px','auto','auto','auto','auto']
         },
         {
            id:'p4Copy',
            type:'rect',
            rect:['0px','295px','auto','auto','auto','auto']
         },
         {
            id:'p4Copy2',
            type:'rect',
            rect:['0px','295px','auto','auto','auto','auto']
         },
         {
            id:'p3',
            type:'rect',
            rect:['17px','294px','auto','auto','auto','auto']
         },
         {
            id:'palm1',
            type:'rect',
            rect:['-11','44','auto','auto','auto','auto']
         },
         {
            id:'palm1Copy2',
            type:'rect',
            rect:['-11','44','auto','auto','auto','auto']
         },
         {
            id:'p2',
            type:'rect',
            rect:['-22','141','auto','auto','auto','auto']
         },
         {
            id:'p2Copy',
            type:'rect',
            rect:['-22','141','auto','auto','auto','auto']
         },
         {
            id:'p2Copy2',
            type:'rect',
            rect:['-22','141','auto','auto','auto','auto']
         }],
         symbolInstances: [
         {
            id:'palm1Copy2',
            symbolName:'palm1'
         },
         {
            id:'p4Copy2',
            symbolName:'p4'
         },
         {
            id:'palm1',
            symbolName:'palm1'
         },
         {
            id:'p2Copy2',
            symbolName:'p2'
         },
         {
            id:'p4',
            symbolName:'p4'
         },
         {
            id:'p2',
            symbolName:'p2'
         },
         {
            id:'p3',
            symbolName:'p3'
         },
         {
            id:'p2Copy',
            symbolName:'p2'
         },
         {
            id:'p4Copy',
            symbolName:'p4'
         }
         ]
      },
   states: {
      "Base State": {
         "${_palm1}": [
            ["style", "left", '-66px'],
            ["style", "top", '-5px']
         ],
         "${_p4Copy}": [
            ["style", "top", '87px'],
            ["transform", "scaleY", '0.43451'],
            ["transform", "rotateZ", '23deg'],
            ["transform", "scaleX", '0.43451'],
            ["style", "left", '586px']
         ],
         "${_p4}": [
            ["style", "top", '143px'],
            ["style", "left", '-21px'],
            ["transform", "rotateZ", '23deg']
         ],
         "${_p4Copy2}": [
            ["style", "top", '89px'],
            ["transform", "scaleY", '0.43451'],
            ["transform", "rotateZ", '-16deg'],
            ["transform", "scaleX", '-0.36606'],
            ["style", "left", '520px']
         ],
         "${_p2Copy2}": [
            ["style", "top", '74px'],
            ["transform", "scaleX", '0.25183'],
            ["transform", "scaleY", '0.30216'],
            ["style", "left", '548px']
         ],
         "${_paradero-de-bus-01}": [
            ["style", "height", '475px'],
            ["style", "width", '760px']
         ],
         "${_palm1Copy2}": [
            ["style", "top", '-5px'],
            ["transform", "scaleY", '0.38842'],
            ["style", "left", '569px'],
            ["transform", "scaleX", '0.38842']
         ],
         "${_Stage}": [
            ["color", "background-color", 'rgba(255,255,255,1)'],
            ["style", "width", '760px'],
            ["style", "height", '465px'],
            ["style", "overflow", 'hidden']
         ],
         "${_p2Copy}": [
            ["style", "top", '72px'],
            ["transform", "scaleY", '0.30216'],
            ["style", "left", '600px'],
            ["transform", "scaleX", '0.30216']
         ],
         "${_p2}": [
            ["style", "top", '106px'],
            ["style", "left", '0px']
         ],
         "${_p3}": [
            ["style", "top", '110px'],
            ["style", "left", '-28px'],
            ["transform", "rotateZ", '8deg']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 0,
         autoPlay: true,
         timeline: [
            { id: "eid259", tween: [ "transform", "${_p2Copy}", "scaleX", '0.30216', { fromValue: '0.30216'}], position: 0, duration: 0 },
            { id: "eid285", tween: [ "style", "${_palm1Copy2}", "left", '569px', { fromValue: '569px'}], position: 0, duration: 0 },
            { id: "eid361", tween: [ "style", "${_p4Copy2}", "left", '520px', { fromValue: '520px'}], position: 0, duration: 0 },
            { id: "eid260", tween: [ "transform", "${_p2Copy}", "scaleY", '0.30216', { fromValue: '0.30216'}], position: 0, duration: 0 },
            { id: "eid360", tween: [ "transform", "${_p4Copy2}", "rotateZ", '-16deg', { fromValue: '-16deg'}], position: 0, duration: 0 },
            { id: "eid359", tween: [ "transform", "${_p4Copy2}", "scaleX", '-0.36606', { fromValue: '-0.36606'}], position: 0, duration: 0 },
            { id: "eid26", tween: [ "style", "${_palm1}", "left", '-66px', { fromValue: '-66px'}], position: 0, duration: 0 },
            { id: "eid191", tween: [ "style", "${_p4}", "left", '-21px', { fromValue: '-21px'}], position: 0, duration: 0 },
            { id: "eid135", tween: [ "style", "${_p3}", "left", '-28px', { fromValue: '-28px'}], position: 0, duration: 0 },
            { id: "eid47", tween: [ "style", "${_p2}", "left", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid192", tween: [ "style", "${_p4}", "top", '143px', { fromValue: '143px'}], position: 0, duration: 0 },
            { id: "eid134", tween: [ "transform", "${_p3}", "rotateZ", '8deg', { fromValue: '8deg'}], position: 0, duration: 0 },
            { id: "eid263", tween: [ "style", "${_p2Copy}", "left", '600px', { fromValue: '600px'}], position: 0, duration: 0 },
            { id: "eid264", tween: [ "style", "${_p2Copy}", "top", '72px', { fromValue: '72px'}], position: 0, duration: 0 },
            { id: "eid334", tween: [ "transform", "${_p4Copy}", "scaleY", '0.43451', { fromValue: '0.43451'}], position: 0, duration: 0 },
            { id: "eid337", tween: [ "style", "${_p4Copy}", "top", '87px', { fromValue: '87px'}], position: 0, duration: 0 },
            { id: "eid338", tween: [ "style", "${_p4Copy}", "left", '586px', { fromValue: '586px'}], position: 0, duration: 0 },
            { id: "eid281", tween: [ "transform", "${_palm1Copy2}", "scaleX", '0.38842', { fromValue: '0.38842'}], position: 0, duration: 0 },
            { id: "eid190", tween: [ "transform", "${_p4}", "rotateZ", '23deg', { fromValue: '23deg'}], position: 0, duration: 0 },
            { id: "eid136", tween: [ "style", "${_p3}", "top", '110px', { fromValue: '110px'}], position: 0, duration: 0 },
            { id: "eid398", tween: [ "style", "${_p4Copy2}", "top", '89px', { fromValue: '89px'}], position: 0, duration: 0 },
            { id: "eid295", tween: [ "transform", "${_p2Copy2}", "scaleX", '0.25183', { fromValue: '0.25183'}], position: 0, duration: 0 },
            { id: "eid397", tween: [ "style", "${_p2Copy2}", "top", '74px', { fromValue: '74px'}], position: 0, duration: 0 },
            { id: "eid333", tween: [ "transform", "${_p4Copy}", "scaleX", '0.43451', { fromValue: '0.43451'}], position: 0, duration: 0 },
            { id: "eid46", tween: [ "style", "${_p2}", "top", '106px', { fromValue: '106px'}], position: 0, duration: 0 },
            { id: "eid282", tween: [ "transform", "${_palm1Copy2}", "scaleY", '0.38842', { fromValue: '0.38842'}], position: 0, duration: 0 },
            { id: "eid395", tween: [ "style", "${_p2Copy2}", "left", '548px', { fromValue: '548px'}], position: 0, duration: 0 },
            { id: "eid27", tween: [ "style", "${_palm1}", "top", '-5px', { fromValue: '-5px'}], position: 0, duration: 0 },
            { id: "eid286", tween: [ "style", "${_palm1Copy2}", "top", '-5px', { fromValue: '-5px'}], position: 0, duration: 0 }         ]
      }
   }
},
"palm1": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'm1-02',
      type: 'image',
      rect: ['12px','36px','209px','154px','auto','auto'],
      fill: ['rgba(0,0,0,0)',im+'m1-02.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_m1-02}": [
            ["style", "top", '36px'],
            ["transform", "rotateZ", '0deg'],
            ["style", "height", '154px'],
            ["style", "-webkit-transform-origin", [50,50], {valueTemplate:'@@0@@% @@1@@%'} ],
            ["style", "-moz-transform-origin", [50,50],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "-ms-transform-origin", [50,50],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "msTransformOrigin", [50,50],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "-o-transform-origin", [50,50],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "left", '12px'],
            ["style", "width", '209px']
         ],
         "${symbolSelector}": [
            ["style", "height", '250px'],
            ["style", "width", '257px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 3715,
         autoPlay: true,
         timeline: [
            { id: "eid5", tween: [ "transform", "${_m1-02}", "rotateZ", '-4deg', { fromValue: '0deg'}], position: 0, duration: 3715 },
            { id: "eid4", tween: [ "style", "${_m1-02}", "-webkit-transform-origin", [28.23,96.1], { valueTemplate: '@@0@@% @@1@@%', fromValue: [50,50]}], position: 0, duration: 3715 },
            { id: "eid439", tween: [ "style", "${_m1-02}", "-moz-transform-origin", [28.23,96.1], { valueTemplate: '@@0@@% @@1@@%', fromValue: [50,50]}], position: 0, duration: 3715 },
            { id: "eid440", tween: [ "style", "${_m1-02}", "-ms-transform-origin", [28.23,96.1], { valueTemplate: '@@0@@% @@1@@%', fromValue: [50,50]}], position: 0, duration: 3715 },
            { id: "eid441", tween: [ "style", "${_m1-02}", "msTransformOrigin", [28.23,96.1], { valueTemplate: '@@0@@% @@1@@%', fromValue: [50,50]}], position: 0, duration: 3715 },
            { id: "eid442", tween: [ "style", "${_m1-02}", "-o-transform-origin", [28.23,96.1], { valueTemplate: '@@0@@% @@1@@%', fromValue: [50,50]}], position: 0, duration: 3715 },
            { id: "eid1", tween: [ "style", "${_m1-02}", "left", '12px', { fromValue: '12px'}], position: 0, duration: 0 }         ]
      }
   }
},
"p2": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'm1-03',
      type: 'image',
      rect: ['0','11px','226px','83px','auto','auto'],
      fill: ['rgba(0,0,0,0)',im+'m1-03.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_m1-03}": [
            ["style", "top", '11px'],
            ["transform", "rotateZ", '0deg'],
            ["style", "height", '83px'],
            ["style", "-webkit-transform-origin", [50,50], {valueTemplate:'@@0@@% @@1@@%'} ],
            ["style", "-moz-transform-origin", [50,50],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "-ms-transform-origin", [50,50],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "msTransformOrigin", [50,50],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "-o-transform-origin", [50,50],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "width", '226px']
         ],
         "${symbolSelector}": [
            ["style", "height", '139px'],
            ["style", "width", '213px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 3000,
         autoPlay: true,
         timeline: [
            { id: "eid45", tween: [ "transform", "${_m1-03}", "rotateZ", '-1deg', { fromValue: '0deg'}], position: 0, duration: 3000 },
            { id: "eid44", tween: [ "style", "${_m1-03}", "-webkit-transform-origin", [0.88,59.04], { valueTemplate: '@@0@@% @@1@@%', fromValue: [50,50]}], position: 0, duration: 3000 },
            { id: "eid443", tween: [ "style", "${_m1-03}", "-moz-transform-origin", [0.88,59.04], { valueTemplate: '@@0@@% @@1@@%', fromValue: [50,50]}], position: 0, duration: 3000 },
            { id: "eid444", tween: [ "style", "${_m1-03}", "-ms-transform-origin", [0.88,59.04], { valueTemplate: '@@0@@% @@1@@%', fromValue: [50,50]}], position: 0, duration: 3000 },
            { id: "eid445", tween: [ "style", "${_m1-03}", "msTransformOrigin", [0.88,59.04], { valueTemplate: '@@0@@% @@1@@%', fromValue: [50,50]}], position: 0, duration: 3000 },
            { id: "eid446", tween: [ "style", "${_m1-03}", "-o-transform-origin", [0.88,59.04], { valueTemplate: '@@0@@% @@1@@%', fromValue: [50,50]}], position: 0, duration: 3000 }         ]
      }
   }
},
"p3": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      rect: ['0','49px','270px','100px','auto','auto'],
      id: 'm1-04',
      transform: [],
      type: 'image',
      fill: ['rgba(0,0,0,0)',im+'m1-04.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_m1-04}": [
            ["style", "top", '49px'],
            ["style", "-webkit-transform-origin", [7.78,25], {valueTemplate:'@@0@@% @@1@@%'} ],
            ["style", "-moz-transform-origin", [7.78,25],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "-ms-transform-origin", [7.78,25],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "msTransformOrigin", [7.78,25],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "-o-transform-origin", [7.78,25],{valueTemplate:'@@0@@% @@1@@%'}],
            ["transform", "rotateZ", '-9deg']
         ],
         "${symbolSelector}": [
            ["style", "height", '149px'],
            ["style", "width", '294px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 2250,
         autoPlay: true,
         timeline: [
            { id: "eid99", tween: [ "transform", "${_m1-04}", "rotateZ", '-8deg', { fromValue: '-9deg'}], position: 0, duration: 2250 }         ]
      }
   }
},
"p4": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      rect: ['0','32px','229px','80px','auto','auto'],
      id: 'm1-05',
      transform: [[],[],[],[],['5.6768%','47.5%']],
      type: 'image',
      fill: ['rgba(0,0,0,0)',im+'m1-05.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_m1-05}": [
            ["style", "top", '32px'],
            ["transform", "rotateZ", '4deg'],
            ["style", "height", '80px'],
            ["style", "-webkit-transform-origin", [5.68,47.5], {valueTemplate:'@@0@@% @@1@@%'} ],
            ["style", "-moz-transform-origin", [5.68,47.5],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "-ms-transform-origin", [5.68,47.5],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "msTransformOrigin", [5.68,47.5],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "-o-transform-origin", [5.68,47.5],{valueTemplate:'@@0@@% @@1@@%'}],
            ["style", "width", '229px']
         ],
         "${symbolSelector}": [
            ["style", "height", '132px'],
            ["style", "width", '243px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 3500,
         autoPlay: true,
         timeline: [
            { id: "eid186", tween: [ "transform", "${_m1-05}", "rotateZ", '0deg', { fromValue: '4deg'}], position: 0, duration: 3500 }         ]
      }
   }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "CATE-10010029");
