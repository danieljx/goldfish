/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='files/CATE-10010031/images/';

var fonts = {};


var resources = [
];
var symbols = {
"stage": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
         dom: [
         {
            id:'Punto_de_informacion-01',
            type:'image',
            rect:['0px','0','760px','465px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"Punto_de_informacion-01.jpg",'0px','0px']
         },
         {
            id:'luz',
            type:'rect',
            rect:['380px','43px','auto','auto','auto','auto']
         },
         {
            id:'luzCopy',
            type:'rect',
            rect:['380px','43px','auto','auto','auto','auto']
         },
         {
            id:'luzCopy2',
            type:'rect',
            rect:['380px','43px','auto','auto','auto','auto']
         },
         {
            id:'luzCopy3',
            type:'rect',
            rect:['380px','43px','auto','auto','auto','auto']
         },
         {
            id:'luzCopy4',
            type:'rect',
            rect:['380px','43px','auto','auto','auto','auto']
         },
         {
            id:'luzCopy5',
            type:'rect',
            rect:['380px','43px','auto','auto','auto','auto']
         },
         {
            id:'luzCopy6',
            type:'rect',
            rect:['380px','43px','auto','auto','auto','auto']
         },
         {
            id:'luzCopy7',
            type:'rect',
            rect:['380px','43px','auto','auto','auto','auto']
         },
         {
            id:'saluda_animacion1',
            type:'rect',
            rect:['21px','0','auto','auto','auto','auto'],
            overflow:'hidden'
         }],
         symbolInstances: [
         {
            id:'luzCopy5',
            symbolName:'luz'
         },
         {
            id:'luz',
            symbolName:'luz'
         },
         {
            id:'luzCopy2',
            symbolName:'luz'
         },
         {
            id:'luzCopy4',
            symbolName:'luz'
         },
         {
            id:'luzCopy',
            symbolName:'luz'
         },
         {
            id:'luzCopy7',
            symbolName:'luz'
         },
         {
            id:'luzCopy6',
            symbolName:'luz'
         },
         {
            id:'luzCopy3',
            symbolName:'luz'
         },
         {
            id:'saluda_animacion1',
            symbolName:'saluda_animacion1'
         }
         ]
      },
   states: {
      "Base State": {
         "${_saluda_animacion1}": [
            ["style", "top", '150px'],
            ["transform", "scaleY", '0.39932'],
            ["style", "overflow", 'hidden'],
            ["transform", "scaleX", '0.39932'],
            ["style", "left", '354px']
         ],
         "${_luzCopy4}": [
            ["style", "top", '130px'],
            ["style", "left", '559px'],
            ["transform", "scaleY", '1.95014']
         ],
         "${_Punto_de_informacion-01}": [
            ["style", "height", '465px'],
            ["style", "width", '760px']
         ],
         "${_luzCopy7}": [
            ["transform", "scaleX", '0.54923'],
            ["transform", "scaleY", '0.71104'],
            ["style", "left", '473px'],
            ["style", "top", '25px']
         ],
         "${_luzCopy5}": [
            ["style", "top", '130px'],
            ["style", "left", '559px'],
            ["transform", "scaleY", '1.95014']
         ],
         "${_luzCopy6}": [
            ["style", "top", '25px'],
            ["transform", "scaleX", '0.54923'],
            ["style", "left", '473px'],
            ["transform", "scaleY", '0.71104']
         ],
         "${_luzCopy3}": [
            ["style", "top", '130px'],
            ["style", "left", '453px'],
            ["transform", "scaleY", '1.95014']
         ],
         "${_Stage}": [
            ["color", "background-color", 'rgba(255,255,255,1)'],
            ["style", "width", '760px'],
            ["style", "height", '465px'],
            ["style", "overflow", 'hidden']
         ],
         "${_luz}": [
            ["style", "top", '112px'],
            ["style", "left", '38px'],
            ["transform", "scaleY", '1.95014']
         ],
         "${_luzCopy}": [
            ["style", "top", '112px'],
            ["style", "left", '139px'],
            ["transform", "scaleY", '1.95014']
         ],
         "${_luzCopy2}": [
            ["style", "top", '112px'],
            ["style", "left", '216px'],
            ["transform", "scaleY", '1.95014']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 0,
         autoPlay: true,
         timeline: [
            { id: "eid43", tween: [ "style", "${_saluda_animacion1}", "top", '150px', { fromValue: '150px'}], position: 0, duration: 0 },
            { id: "eid12", tween: [ "style", "${_luzCopy3}", "top", '130px', { fromValue: '130px'}], position: 0, duration: 0 },
            { id: "eid7", tween: [ "style", "${_luz}", "left", '38px', { fromValue: '38px'}], position: 0, duration: 0 },
            { id: "eid13", tween: [ "style", "${_luzCopy4}", "left", '559px', { fromValue: '559px'}], position: 0, duration: 0 },
            { id: "eid10", tween: [ "style", "${_luzCopy2}", "left", '216px', { fromValue: '216px'}], position: 0, duration: 0 },
            { id: "eid9", tween: [ "style", "${_luzCopy}", "left", '139px', { fromValue: '139px'}], position: 0, duration: 0 },
            { id: "eid41", tween: [ "transform", "${_saluda_animacion1}", "scaleY", '0.39932', { fromValue: '0.39932'}], position: 0, duration: 0 },
            { id: "eid57", tween: [ "style", "${_luzCopy6}", "left", '473px', { fromValue: '473px'}], position: 0, duration: 0 },
            { id: "eid5", tween: [ "transform", "${_luz}", "scaleY", '1.95014', { fromValue: '1.95014'}], position: 0, duration: 0 },
            { id: "eid8", tween: [ "style", "${_luz}", "top", '112px', { fromValue: '112px'}], position: 0, duration: 0 },
            { id: "eid42", tween: [ "style", "${_saluda_animacion1}", "left", '354px', { fromValue: '354px'}], position: 0, duration: 0 },
            { id: "eid11", tween: [ "style", "${_luzCopy3}", "left", '453px', { fromValue: '453px'}], position: 0, duration: 0 },
            { id: "eid53", tween: [ "transform", "${_luzCopy6}", "scaleX", '0.54923', { fromValue: '0.54923'}], position: 0, duration: 0 },
            { id: "eid54", tween: [ "transform", "${_luzCopy6}", "scaleY", '0.71104', { fromValue: '0.71104'}], position: 0, duration: 0 },
            { id: "eid58", tween: [ "style", "${_luzCopy6}", "top", '25px', { fromValue: '25px'}], position: 0, duration: 0 },
            { id: "eid40", tween: [ "transform", "${_saluda_animacion1}", "scaleX", '0.39932', { fromValue: '0.39932'}], position: 0, duration: 0 }         ]
      }
   }
},
"luz": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      type: 'image',
      id: 'luz-01',
      opacity: 0.35164835164835,
      rect: ['3px','0px','147px','115px','auto','auto'],
      fill: ['rgba(0,0,0,0)',im+'luz-01.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${symbolSelector}": [
            ["style", "height", '126px'],
            ["style", "width", '154px']
         ],
         "${_luz-01}": [
            ["style", "top", '0px'],
            ["style", "opacity", '0'],
            ["style", "left", '3px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 2800,
         autoPlay: true,
         timeline: [
            { id: "eid3", tween: [ "style", "${_luz-01}", "left", '3px', { fromValue: '3px'}], position: 0, duration: 0 },
            { id: "eid4", tween: [ "style", "${_luz-01}", "opacity", '1', { fromValue: '0'}], position: 0, duration: 2800 }         ]
      }
   }
},
"saluda_animacion1": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'personaje1',
      type: 'image',
      rect: ['0px','0','1050px','300px','auto','auto'],
      fill: ['rgba(0,0,0,0)',im+'personaje1.svg','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_personaje1}": [
            ["style", "left", '0px']
         ],
         "${symbolSelector}": [
            ["style", "height", '132px'],
            ["style", "width", '150px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 2500,
         autoPlay: true,
         timeline: [
            { id: "eid1", tween: [ "style", "${_personaje1}", "left", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid23", tween: [ "style", "${_personaje1}", "left", '0px', { fromValue: '0px'}], position: 2300, duration: 0 },
            { id: "eid24", tween: [ "style", "${_personaje1}", "left", '-900px', { fromValue: '0px'}], position: 2400, duration: 0 }         ]
      }
   }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "CATE-10010031");
