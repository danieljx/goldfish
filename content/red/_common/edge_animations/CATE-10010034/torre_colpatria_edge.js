/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='files/CATE-10010034/images/';

var fonts = {};


var resources = [
];
var symbols = {
"stage": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
         dom: [
         {
            id:'cielo-01',
            type:'image',
            rect:['0','0','760px','465px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"cielo-01.jpg",'0px','0px']
         },
         {
            id:'nubes',
            type:'rect',
            rect:['0','0','auto','auto','auto','auto']
         },
         {
            id:'palomaCopy',
            type:'rect',
            rect:['676px','95px','auto','auto','auto','auto'],
            transform:[[],[],[],['0.19119','0.19119']]
         },
         {
            id:'palomaCopy3',
            type:'rect',
            rect:['676px','95px','auto','auto','auto','auto'],
            transform:[[],[],[],['0.19119','0.19119']]
         },
         {
            id:'torre_colpatria-01',
            type:'image',
            rect:['0px','0','760px','465px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"torre_colpatria-01.png",'0px','0px']
         },
         {
            id:'paloma',
            type:'rect',
            rect:['676px','95px','auto','auto','auto','auto'],
            transform:[[],[],[],['0.19119','0.19119']]
         }],
         symbolInstances: [
         {
            id:'palomaCopy3',
            symbolName:'paloma'
         },
         {
            id:'palomaCopy',
            symbolName:'paloma'
         },
         {
            id:'nubes',
            symbolName:'nubes'
         },
         {
            id:'paloma',
            symbolName:'paloma'
         }
         ]
      },
   states: {
      "Base State": {
         "${_nubes}": [
            ["transform", "scaleX", '0.78708'],
            ["style", "top", '-40px'],
            ["transform", "scaleY", '0.78708'],
            ["style", "left", '81px']
         ],
         "${_palomaCopy3}": [
            ["style", "top", '156px'],
            ["transform", "scaleY", '0.1433'],
            ["style", "overflow", 'hidden'],
            ["transform", "scaleX", '-0.15255'],
            ["style", "left", '-105px'],
            ["transform", "rotateZ", '7deg']
         ],
         "${_torre_colpatria-01}": [
            ["style", "height", '465px'],
            ["style", "width", '760px']
         ],
         "${_paloma}": [
            ["style", "top", '243px'],
            ["transform", "scaleY", '0.25512'],
            ["style", "overflow", 'hidden'],
            ["transform", "scaleX", '-0.27159'],
            ["style", "left", '-119px'],
            ["transform", "rotateZ", '7deg']
         ],
         "${_cielo-01}": [
            ["style", "height", '465px'],
            ["style", "width", '760px']
         ],
         "${_palomaCopy}": [
            ["style", "top", '156px'],
            ["transform", "scaleY", '0.1433'],
            ["style", "overflow", 'hidden'],
            ["transform", "scaleX", '-0.15255'],
            ["style", "left", '-105px'],
            ["transform", "rotateZ", '7deg']
         ],
         "${_Stage}": [
            ["color", "background-color", 'rgba(255,255,255,1)'],
            ["style", "width", '760px'],
            ["style", "height", '465px'],
            ["style", "overflow", 'hidden']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 252500,
         autoPlay: true,
         timeline: [
            { id: "eid29", tween: [ "transform", "${_palomaCopy}", "scaleY", '0.1433', { fromValue: '0.1433'}], position: 0, duration: 0 },
            { id: "eid38", tween: [ "transform", "${_palomaCopy3}", "scaleY", '0.1433', { fromValue: '0.1433'}], position: 1000, duration: 0 },
            { id: "eid47", tween: [ "transform", "${_nubes}", "scaleY", '0.78708', { fromValue: '0.78708'}], position: 17500, duration: 0 },
            { id: "eid28", tween: [ "transform", "${_palomaCopy}", "scaleX", '-0.15255', { fromValue: '-0.15255'}], position: 0, duration: 0 },
            { id: "eid48", tween: [ "style", "${_nubes}", "left", '81px', { fromValue: '81px'}], position: 17500, duration: 0 },
            { id: "eid36", tween: [ "style", "${_palomaCopy3}", "top", '-24px', { fromValue: '156px'}], position: 2000, duration: 15500 },
            { id: "eid3", tween: [ "style", "${_paloma}", "left", '720px', { fromValue: '-119px'}], position: 0, duration: 11500 },
            { id: "eid39", tween: [ "transform", "${_palomaCopy3}", "scaleX", '-0.15255', { fromValue: '-0.15255'}], position: 1000, duration: 0 },
            { id: "eid4", tween: [ "style", "${_paloma}", "top", '52px', { fromValue: '243px'}], position: 0, duration: 11500 },
            { id: "eid46", tween: [ "transform", "${_nubes}", "scaleX", '0.78708', { fromValue: '0.78708'}], position: 17500, duration: 0 },
            { id: "eid31", tween: [ "style", "${_palomaCopy}", "top", '-24px', { fromValue: '156px'}], position: 1000, duration: 15500 },
            { id: "eid30", tween: [ "style", "${_palomaCopy}", "left", '711px', { fromValue: '-105px'}], position: 1000, duration: 15500 },
            { id: "eid49", tween: [ "style", "${_nubes}", "top", '-40px', { fromValue: '-40px'}], position: 17500, duration: 0 },
            { id: "eid37", tween: [ "style", "${_palomaCopy3}", "left", '711px', { fromValue: '-105px'}], position: 2000, duration: 15500 }         ]
      }
   }
},
"paloma": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'paloma-032',
      type: 'image',
      rect: ['0','0','1042px','145px','auto','auto'],
      fill: ['rgba(0,0,0,0)',im+'paloma-03.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${symbolSelector}": [
            ["style", "height", '166px'],
            ["style", "width", '17.11%']
         ],
         "${_paloma-032}": [
            ["style", "left", '0px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 700,
         autoPlay: true,
         timeline: [
            { id: "eid5", tween: [ "style", "${_paloma-032}", "left", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid6", tween: [ "style", "${_paloma-032}", "left", '-130px', { fromValue: '0px'}], position: 100, duration: 0 },
            { id: "eid7", tween: [ "style", "${_paloma-032}", "left", '-260px', { fromValue: '-130px'}], position: 200, duration: 0 },
            { id: "eid9", tween: [ "style", "${_paloma-032}", "left", '-390px', { fromValue: '-260px'}], position: 300, duration: 0 },
            { id: "eid10", tween: [ "style", "${_paloma-032}", "left", '-520px', { fromValue: '-390px'}], position: 400, duration: 0 },
            { id: "eid11", tween: [ "style", "${_paloma-032}", "left", '-650px', { fromValue: '-520px'}], position: 500, duration: 0 },
            { id: "eid12", tween: [ "style", "${_paloma-032}", "left", '-780px', { fromValue: '-650px'}], position: 600, duration: 0 },
            { id: "eid13", tween: [ "style", "${_paloma-032}", "left", '-910px', { fromValue: '-780px'}], position: 700, duration: 0 }         ]
      }
   }
},
"nubes": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'nube-02',
      type: 'image',
      rect: ['0','0','746px','332px','auto','auto'],
      fill: ['rgba(0,0,0,0)',im+'nube-02.png','0px','0px']
   },
   {
      id: 'nube-03',
      type: 'image',
      rect: ['460px','-47px','493px','158px','auto','auto'],
      fill: ['rgba(0,0,0,0)',im+'nube-03.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_nube-03}": [
            ["style", "top", '-68px'],
            ["transform", "rotateZ", '3deg'],
            ["style", "height", '158px'],
            ["style", "left", '262px'],
            ["style", "width", '493px']
         ],
         "${_nube-02}": [
            ["style", "height", '463px'],
            ["style", "top", '-19px'],
            ["style", "left", '-5px'],
            ["style", "width", '1040px']
         ],
         "${symbolSelector}": [
            ["style", "height", '380px'],
            ["style", "width", '760px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 252862.93661567,
         autoPlay: true,
         timeline: [
            { id: "eid51", tween: [ "style", "${_nube-03}", "top", '-171px', { fromValue: '-68px'}], position: 25286, duration: 227577 },
            { id: "eid5", tween: [ "style", "${_nube-02}", "width", '963px', { fromValue: '1040px'}], position: 0, duration: 5000 },
            { id: "eid10", tween: [ "style", "${_nube-02}", "width", '536px', { fromValue: '963px'}], position: 5000, duration: 82500 },
            { id: "eid4", tween: [ "style", "${_nube-02}", "height", '428px', { fromValue: '463px'}], position: 0, duration: 5000 },
            { id: "eid9", tween: [ "style", "${_nube-02}", "height", '239px', { fromValue: '428px'}], position: 5000, duration: 82500 },
            { id: "eid7", tween: [ "style", "${_nube-03}", "left", '133px', { fromValue: '262px'}], position: 0, duration: 25286 },
            { id: "eid50", tween: [ "style", "${_nube-03}", "left", '-463px', { fromValue: '133px'}], position: 25286, duration: 227577 },
            { id: "eid52", tween: [ "transform", "${_nube-03}", "rotateZ", '3deg', { fromValue: '3deg'}], position: 25286, duration: 0 },
            { id: "eid40", tween: [ "style", "${_nube-02}", "left", '224px', { fromValue: '-5px'}], position: 0, duration: 87500 },
            { id: "eid45", tween: [ "style", "${_nube-02}", "top", '-10px', { fromValue: '-19px'}], position: 0, duration: 87500 }         ]
      }
   }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "CATE-10010034");
