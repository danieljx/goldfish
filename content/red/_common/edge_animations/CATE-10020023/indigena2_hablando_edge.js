/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='files/CATE-10020023/images/';

var fonts = {};


var resources = [
];
var symbols = {
"stage": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
         dom: [
         {
            id:'saluda_animacion1',
            type:'rect',
            rect:['-22px','0','auto','auto','auto','auto']
         }],
         symbolInstances: [
         {
            id:'saluda_animacion1',
            symbolName:'saluda_animacion1'
         }
         ]
      },
   states: {
      "Base State": {
         "${_Stage}": [
            ["color", "background-color", 'rgba(255,255,255,0.00)'],
            ["style", "overflow", 'hidden'],
            ["style", "height", '300px'],
            ["style", "width", '150px']
         ],
         "${_saluda_animacion1}": [
            ["style", "left", '-22px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 2300,
         autoPlay: false,
         timeline: [
            { id: "eid24", tween: [ "style", "${_saluda_animacion1}", "left", '-22px', { fromValue: '-22px'}], position: 0, duration: 0 },
            { id: "eid25", tween: [ "style", "${_saluda_animacion1}", "left", '-22px', { fromValue: '-22px'}], position: 2300, duration: 0 },
            { id: "eid26", trigger: [ function executeSymbolFunction(e, data) { this._executeSymbolAction(e, data); }, ['play', '${_saluda_animacion1}', [] ], ""], position: 0 }         ]
      }
   }
},
"saluda_animacion1": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'personaje1',
      type: 'image',
      rect: ['0px','0','1050px','300px','auto','auto'],
      fill: ['rgba(0,0,0,0)',im+'personaje1.svg','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_personaje1}": [
            ["style", "left", '0px']
         ],
         "${symbolSelector}": [
            ["style", "height", '300px'],
            ["style", "width", '150px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 2300,
         autoPlay: false,
         timeline: [
            { id: "eid1", tween: [ "style", "${_personaje1}", "left", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid2", tween: [ "style", "${_personaje1}", "left", '-150px', { fromValue: '0px'}], position: 100, duration: 0 },
            { id: "eid4", tween: [ "style", "${_personaje1}", "left", '-300px', { fromValue: '-150px'}], position: 300, duration: 0 },
            { id: "eid6", tween: [ "style", "${_personaje1}", "left", '-450px', { fromValue: '-300px'}], position: 500, duration: 0 },
            { id: "eid7", tween: [ "style", "${_personaje1}", "left", '-600px', { fromValue: '-450px'}], position: 700, duration: 0 },
            { id: "eid17", tween: [ "style", "${_personaje1}", "left", '0px', { fromValue: '-600px'}], position: 900, duration: 0 },
            { id: "eid18", tween: [ "style", "${_personaje1}", "left", '-150px', { fromValue: '0px'}], position: 1100, duration: 0 },
            { id: "eid19", tween: [ "style", "${_personaje1}", "left", '-300px', { fromValue: '-150px'}], position: 1300, duration: 0 },
            { id: "eid20", tween: [ "style", "${_personaje1}", "left", '-450px', { fromValue: '-300px'}], position: 1500, duration: 0 },
            { id: "eid21", tween: [ "style", "${_personaje1}", "left", '-600px', { fromValue: '-450px'}], position: 1700, duration: 0 },
            { id: "eid22", tween: [ "style", "${_personaje1}", "left", '0px', { fromValue: '-600px'}], position: 1900, duration: 0 },
            { id: "eid23", tween: [ "style", "${_personaje1}", "left", '0px', { fromValue: '0px'}], position: 2300, duration: 0 }         ]
      }
   }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "CATE-10020023");
 