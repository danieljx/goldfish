/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='files/CATE-10010005/images/';

var fonts = {};


var resources = [
];
var symbols = {
"stage": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
         dom: [
         {
            id:'fondo-01-03',
            type:'image',
            rect:['0','0','760px','465px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"fondo-01-03.jpg",'0px','0px']
         },
         {
            id:'mar',
            type:'rect',
            rect:['0','239','auto','auto','auto','auto']
         },
         {
            id:'bandera',
            type:'rect',
            rect:['213px','177px','auto','auto','auto','auto'],
            transform:[[],[],[],['0.20191','0.20388']]
         },
         {
            id:'murallas_cartagenafondo-01',
            type:'image',
            rect:['0','0','760px','465px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"murallas%20cartagenafondo-01.png",'0px','0px']
         }],
         symbolInstances: [
         {
            id:'mar',
            symbolName:'mar'
         },
         {
            id:'bandera',
            symbolName:'bandera'
         }
         ]
      },
   states: {
      "Base State": {
         "${_bandera}": [
            ["style", "top", '177px'],
            ["transform", "scaleY", '0.20388'],
            ["style", "overflow", 'hidden'],
            ["transform", "scaleX", '0.20191'],
            ["style", "left", '213px']
         ],
         "${_Stage}": [
            ["color", "background-color", 'rgba(255,255,255,1)'],
            ["style", "overflow", 'hidden'],
            ["style", "height", '465px'],
            ["style", "width", '760px']
         ],
         "${_fondo-01-03}": [
            ["style", "height", '465px'],
            ["style", "width", '760px']
         ],
         "${_murallas_cartagenafondo-01}": [
            ["style", "height", '465px'],
            ["style", "width", '760px']
         ],
         "${_mar}": [
            ["style", "top", '306px'],
            ["style", "left", '-369px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 0,
         autoPlay: true,
         timeline: [
            { id: "eid12", tween: [ "style", "${_mar}", "top", '306px', { fromValue: '306px'}], position: 0, duration: 0 },
            { id: "eid14", tween: [ "style", "${_mar}", "left", '-369px', { fromValue: '-369px'}], position: 0, duration: 0 }         ]
      }
   }
},
"bandera": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'bandera-02',
      type: 'image',
      rect: ['0','0','912px','103px','auto','auto'],
      fill: ['rgba(0,0,0,0)',im+'bandera-02.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${symbolSelector}": [
            ["style", "height", '103px'],
            ["style", "width", '151px']
         ],
         "${_bandera-02}": [
            ["style", "left", '0px'],
            ["style", "overflow", 'hidden']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 500,
         autoPlay: true,
         timeline: [
            { id: "eid1", tween: [ "style", "${_bandera-02}", "left", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid2", tween: [ "style", "${_bandera-02}", "left", '-152px', { fromValue: '0px'}], position: 100, duration: 0 },
            { id: "eid6", tween: [ "style", "${_bandera-02}", "left", '-304px', { fromValue: '-152px'}], position: 200, duration: 0 },
            { id: "eid7", tween: [ "style", "${_bandera-02}", "left", '-456px', { fromValue: '-304px'}], position: 300, duration: 0 },
            { id: "eid8", tween: [ "style", "${_bandera-02}", "left", '-608px', { fromValue: '-456px'}], position: 400, duration: 0 },
            { id: "eid9", tween: [ "style", "${_bandera-02}", "left", '-760px', { fromValue: '-608px'}], position: 500, duration: 0 }         ]
      }
   }
},
"mar": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'mar-04',
      type: 'image',
      rect: ['0','0','1783px','159px','auto','auto'],
      fill: ['rgba(0,0,0,0)',im+'mar-04.jpg','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${symbolSelector}": [
            ["style", "height", '159px'],
            ["style", "width", '760px']
         ],
         "${_mar-04}": [
            ["style", "height", '159px'],
            ["style", "left", '-156px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 27000,
         autoPlay: true,
         timeline: [
            { id: "eid11", tween: [ "style", "${_mar-04}", "left", '-1013px', { fromValue: '-156px'}], position: 0, duration: 26983 },
            { id: "eid15", tween: [ "style", "${_mar-04}", "left", '-614px', { fromValue: '-1013px'}], position: 26983, duration: 17 }         ]
      }
   }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "CATE-10010005");
