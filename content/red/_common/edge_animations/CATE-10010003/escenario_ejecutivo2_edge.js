/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='files/CATE-10010003/images/';

var fonts = {};


var resources = [
];
var symbols = {
"stage": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
         dom: [
         {
            id:'fonsdoejecutivo2-01',
            type:'image',
            rect:['0px','0px','760px','465px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"fonsdoejecutivo2-01.jpg",'0px','0px']
         },
         {
            id:'Symbol_1',
            type:'rect',
            rect:['516','20','188px','105px','auto','auto']
         },
         {
            id:'nube23',
            type:'rect',
            rect:['186','41','auto','auto','auto','auto']
         },
         {
            id:'escenarioejecutivo2-01',
            type:'image',
            rect:['0px','0px','760px','465px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"escenarioejecutivo2-01.png",'0px','0px']
         },
         {
            id:'luz',
            type:'rect',
            rect:['301','218','auto','auto','auto','auto']
         },
         {
            id:'luzCopy',
            type:'rect',
            rect:['301','218','auto','auto','auto','auto']
         },
         {
            id:'luzCopy4',
            type:'rect',
            rect:['301','218','auto','auto','auto','auto']
         },
         {
            id:'luzCopy2',
            type:'rect',
            rect:['301','218','auto','auto','auto','auto']
         },
         {
            id:'luzCopy3',
            type:'rect',
            rect:['301','218','auto','auto','auto','auto']
         }],
         symbolInstances: [
         {
            id:'nube23',
            symbolName:'nube23'
         },
         {
            id:'luzCopy',
            symbolName:'luz'
         },
         {
            id:'luz',
            symbolName:'luz'
         },
         {
            id:'luzCopy2',
            symbolName:'luz'
         },
         {
            id:'luzCopy4',
            symbolName:'luz'
         },
         {
            id:'luzCopy3',
            symbolName:'luz'
         },
         {
            id:'Symbol_1',
            symbolName:'Symbol_1'
         }
         ]
      },
   states: {
      "Base State": {
         "${_luzCopy4}": [
            ["style", "top", '279px'],
            ["style", "left", '121px']
         ],
         "${_luzCopy}": [
            ["style", "top", '273px'],
            ["style", "left", '396px']
         ],
         "${_Symbol_1}": [
            ["style", "height", '157px'],
            ["style", "top", '243px'],
            ["style", "left", '-118px'],
            ["style", "width", '760px']
         ],
         "${_luzCopy3}": [
            ["style", "top", '279px'],
            ["style", "left", '316px']
         ],
         "${_luzCopy2}": [
            ["style", "top", '279px'],
            ["style", "left", '555px']
         ],
         "${_nube23}": [
            ["transform", "scaleX", '0.91555'],
            ["style", "top", '259px'],
            ["style", "left", '457px'],
            ["transform", "scaleY", '0.91555']
         ],
         "${_luz}": [
            ["style", "top", '273px'],
            ["style", "left", '153px']
         ],
         "${_Stage}": [
            ["color", "background-color", 'rgba(255,255,255,1)'],
            ["style", "width", '760px'],
            ["style", "height", '465px'],
            ["style", "overflow", 'hidden']
         ],
         "${_escenarioejecutivo2-01}": [
            ["style", "height", '465px'],
            ["style", "top", '0px'],
            ["style", "left", '0px'],
            ["style", "width", '760px']
         ],
         "${_fonsdoejecutivo2-01}": [
            ["style", "height", '465px'],
            ["style", "width", '760px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 0,
         autoPlay: true,
         timeline: [
            { id: "eid32", tween: [ "style", "${_luzCopy4}", "top", '279px', { fromValue: '279px'}], position: 0, duration: 0 },
            { id: "eid25", tween: [ "style", "${_luzCopy2}", "left", '555px', { fromValue: '555px'}], position: 0, duration: 0 },
            { id: "eid22", tween: [ "style", "${_luz}", "top", '273px', { fromValue: '273px'}], position: 0, duration: 0 },
            { id: "eid27", tween: [ "style", "${_luzCopy3}", "left", '316px', { fromValue: '316px'}], position: 0, duration: 0 },
            { id: "eid23", tween: [ "style", "${_luz}", "left", '153px', { fromValue: '153px'}], position: 0, duration: 0 },
            { id: "eid18", tween: [ "style", "${_nube23}", "top", '259px', { fromValue: '259px'}], position: 0, duration: 0 },
            { id: "eid31", tween: [ "style", "${_luzCopy4}", "left", '121px', { fromValue: '121px'}], position: 0, duration: 0 },
            { id: "eid26", tween: [ "style", "${_luzCopy2}", "top", '279px', { fromValue: '279px'}], position: 0, duration: 0 },
            { id: "eid17", tween: [ "style", "${_nube23}", "left", '457px', { fromValue: '457px'}], position: 0, duration: 0 },
            { id: "eid15", tween: [ "style", "${_Symbol_1}", "left", '-118px', { fromValue: '-118px'}], position: 0, duration: 0 },
            { id: "eid16", tween: [ "style", "${_Symbol_1}", "top", '243px', { fromValue: '243px'}], position: 0, duration: 0 },
            { id: "eid24", tween: [ "style", "${_luzCopy}", "left", '396px', { fromValue: '396px'}], position: 0, duration: 0 }         ]
      }
   }
},
"Symbol_1": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: true,
   content: {
   dom: [
   {
      id: 'nubes-032',
      type: 'image',
      rect: ['495','29','125px','65px','auto','auto'],
      fill: ['rgba(0,0,0,0)',im+'nubes-03.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_nubes-032}": [
            ["style", "top", '55px'],
            ["style", "height", '59px'],
            ["style", "opacity", '1'],
            ["style", "left", '753px'],
            ["style", "width", '114px']
         ],
         "${symbolSelector}": [
            ["style", "height", '157px'],
            ["style", "width", '760px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 62916,
         autoPlay: true,
         timeline: [
            { id: "eid94", tween: [ "style", "${_nubes-032}", "height", '59px', { fromValue: '59px'}], position: 0, duration: 0 },
            { id: "eid29", tween: [ "style", "${symbolSelector}", "width", '760px', { fromValue: '760px'}], position: 0, duration: 0 },
            { id: "eid95", tween: [ "style", "${_nubes-032}", "width", '114px', { fromValue: '114px'}], position: 0, duration: 0 },
            { id: "eid33", tween: [ "style", "${_nubes-032}", "left", '-131px', { fromValue: '753px'}], position: 0, duration: 62916 },
            { id: "eid34", tween: [ "style", "${_nubes-032}", "top", '54px', { fromValue: '55px'}], position: 0, duration: 62916 },
            { id: "eid30", tween: [ "style", "${symbolSelector}", "height", '157px', { fromValue: '157px'}], position: 0, duration: 0 },
            { id: "eid102", tween: [ "style", "${_nubes-032}", "opacity", '1', { fromValue: '1'}], position: 0, duration: 0 }         ]
      }
   }
},
"nube23": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'nubes-022',
      type: 'image',
      rect: ['-89px','16px','89px','55px','auto','auto'],
      fill: ['rgba(0,0,0,0)',im+'nubes-02.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_nubes-022}": [
            ["style", "top", '16px'],
            ["style", "height", '39px'],
            ["style", "left", '-89px'],
            ["style", "width", '64px']
         ],
         "${symbolSelector}": [
            ["style", "height", '126px'],
            ["style", "width", '760px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 120000,
         autoPlay: true,
         timeline: [
            { id: "eid88", tween: [ "style", "${_nubes-022}", "height", '39px', { fromValue: '39px'}], position: 0, duration: 0 },
            { id: "eid65", tween: [ "style", "${_nubes-022}", "left", '792px', { fromValue: '-89px'}], position: 0, duration: 120000 },
            { id: "eid66", tween: [ "style", "${_nubes-022}", "top", '25px', { fromValue: '16px'}], position: 0, duration: 120000 },
            { id: "eid89", tween: [ "style", "${_nubes-022}", "width", '64px', { fromValue: '64px'}], position: 0, duration: 0 }         ]
      }
   }
},
"avion": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'avion-062',
      type: 'image',
      rect: ['-94px','58px','80px','21px','auto','auto'],
      fill: ['rgba(0,0,0,0)',im+'avion-06.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_avion-062}": [
            ["style", "height", '14px'],
            ["style", "top", '141px'],
            ["style", "left", '67px'],
            ["style", "width", '52px']
         ],
         "${symbolSelector}": [
            ["style", "height", '115px'],
            ["style", "width", '760px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 20000,
         autoPlay: true,
         timeline: [
            { id: "eid108", tween: [ "style", "${_avion-062}", "top", '-38px', { fromValue: '141px'}], position: 0, duration: 20000 },
            { id: "eid106", tween: [ "style", "${_avion-062}", "left", '476px', { fromValue: '67px'}], position: 0, duration: 20000 },
            { id: "eid112", tween: [ "style", "${_avion-062}", "width", '52px', { fromValue: '52px'}], position: 0, duration: 0 },
            { id: "eid111", tween: [ "style", "${_avion-062}", "height", '14px', { fromValue: '14px'}], position: 0, duration: 0 }         ]
      }
   }
},
"luz": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'luz-02',
      type: 'image',
      rect: ['0','0','63px','65px','auto','auto'],
      fill: ['rgba(0,0,0,0)',im+'luz-02.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_luz-02}": [
            ["style", "height", '65px'],
            ["style", "opacity", '0.11538461538462'],
            ["style", "left", '0px'],
            ["style", "width", '63px']
         ],
         "${symbolSelector}": [
            ["style", "height", '66px'],
            ["style", "width", '64px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 2600,
         autoPlay: true,
         timeline: [
            { id: "eid21", tween: [ "style", "${_luz-02}", "opacity", '1', { fromValue: '0.11538500338792801'}], position: 0, duration: 2460 },
            { id: "eid28", tween: [ "style", "${_luz-02}", "opacity", '0.70879120879121', { fromValue: '1'}], position: 2460, duration: 140 },
            { id: "eid19", tween: [ "style", "${_luz-02}", "left", '0px', { fromValue: '0px'}], position: 0, duration: 0 }         ]
      }
   }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "CATE-10010003");
