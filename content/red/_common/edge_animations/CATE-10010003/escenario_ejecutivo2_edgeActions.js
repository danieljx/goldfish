/***********************
* Acciones de composición de Adobe Edge Animate
*
* Editar este archivo con precaución, teniendo cuidado de conservar 
* las firmas de función y los comentarios que comienzan con "Edge" para mantener la 
* capacidad de interactuar con estas acciones en Adobe Edge Animate
*
***********************/
(function($, Edge, compId){
var Composition = Edge.Composition, Symbol = Edge.Symbol; // los alias más comunes para las clases de Edge

   //Edge symbol: 'stage'
   (function(symbolName) {
      
      
   })("stage");
   //Edge symbol end:'stage'

   //=========================================================
   
   //Edge symbol: 'Symbol_1'
   (function(symbolName) {   
   
   })("Symbol_1");
   //Edge symbol end:'Symbol_1'

   //=========================================================
   
   //Edge symbol: 'nube23'
   (function(symbolName) {   
   
   })("nube23");
   //Edge symbol end:'nube23'

   //=========================================================
   
   //Edge symbol: 'avion'
   (function(symbolName) {   
   
   })("avion");
   //Edge symbol end:'avion'

   //=========================================================
   
   //Edge symbol: 'luz'
   (function(symbolName) {   
   
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 0, function(sym, e) {
         sym.play();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 2460, function(sym, e) {
         sym.playReverse();

      });
      //Edge binding end

   })("luz");
   //Edge symbol end:'luz'

})(jQuery, AdobeEdge, "CATE-10010003");