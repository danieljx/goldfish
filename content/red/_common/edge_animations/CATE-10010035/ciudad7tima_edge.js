/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='files/CATE-10010035/images/';

var fonts = {};


var resources = [
];
var symbols = {
"stage": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
         dom: [
         {
            id:'cielo-01',
            type:'image',
            rect:['0','0','760px','446px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"cielo-01.png",'0px','0px']
         },
         {
            id:'nubes',
            type:'rect',
            rect:['0px','0','auto','auto','auto','auto']
         },
         {
            id:'ciudad7tima-01',
            type:'image',
            rect:['0','0','760px','465px','auto','auto'],
            fill:["rgba(0,0,0,0)",im+"ciudad7tima-01.png",'0px','0px']
         },
         {
            id:'banderaCopy',
            type:'rect',
            rect:['209px','159px','auto','auto','auto','auto'],
            transform:[[],[],[],['0.17992','0.20388']]
         },
         {
            id:'Rectangle',
            type:'rect',
            rect:['424px','120px','3px','21px','auto','auto'],
            fill:["rgba(45,45,45,1.00)"],
            stroke:[0,"rgba(0,0,0,1)","none"],
            transform:[[],['-10'],[],['1','1.34641']]
         },
         {
            id:'bandera',
            type:'rect',
            rect:['209px','159px','auto','auto','auto','auto'],
            transform:[[],[],[],['0.17992','0.20388']]
         }],
         symbolInstances: [
         {
            id:'banderaCopy',
            symbolName:'bandera'
         },
         {
            id:'nubes',
            symbolName:'nubes'
         },
         {
            id:'bandera',
            symbolName:'bandera'
         }
         ]
      },
   states: {
      "Base State": {
         "${_nubes}": [
            ["transform", "scaleX", '0.59211'],
            ["style", "top", '-77px'],
            ["style", "left", '-50px'],
            ["transform", "scaleY", '0.59211']
         ],
         "${_bandera}": [
            ["style", "top", '79px'],
            ["transform", "scaleY", '0.20388'],
            ["style", "overflow", 'hidden'],
            ["transform", "scaleX", '-0.21743'],
            ["style", "left", '335px'],
            ["transform", "rotateZ", '-16deg']
         ],
         "${_Rectangle}": [
            ["color", "background-color", 'rgba(45,45,45,1.00)'],
            ["transform", "scaleY", '1.34641'],
            ["transform", "rotateZ", '-10deg'],
            ["style", "height", '21px'],
            ["style", "top", '120px'],
            ["style", "left", '424px'],
            ["style", "width", '3px']
         ],
         "${_Stage}": [
            ["color", "background-color", 'rgba(255,255,255,1)'],
            ["style", "width", '760px'],
            ["style", "height", '465px'],
            ["style", "overflow", 'hidden']
         ],
         "${_cielo-01}": [
            ["style", "height", '446px'],
            ["style", "width", '760px']
         ],
         "${_banderaCopy}": [
            ["style", "top", '181px'],
            ["transform", "scaleY", '0.11069'],
            ["style", "overflow", 'hidden'],
            ["transform", "scaleX", '-0.10275'],
            ["style", "left", '208px'],
            ["transform", "rotateZ", '-16deg']
         ],
         "${_ciudad7tima-01}": [
            ["style", "height", '465px'],
            ["style", "width", '760px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 1328,
         autoPlay: true,
         timeline: [
            { id: "eid29", tween: [ "style", "${_banderaCopy}", "left", '208px', { fromValue: '208px'}], position: 1328, duration: 0 },
            { id: "eid11", tween: [ "transform", "${_nubes}", "scaleY", '0.59211', { fromValue: '0.59211'}], position: 0, duration: 0 },
            { id: "eid28", tween: [ "transform", "${_banderaCopy}", "scaleY", '0.11069', { fromValue: '0.11069'}], position: 1328, duration: 0 },
            { id: "eid3", tween: [ "style", "${_nubes}", "left", '-204px', { fromValue: '-50px'}], position: 0, duration: 0 },
            { id: "eid27", tween: [ "transform", "${_banderaCopy}", "scaleX", '-0.10275', { fromValue: '-0.10275'}], position: 1328, duration: 0 },
            { id: "eid30", tween: [ "style", "${_bandera}", "left", '335px', { fromValue: '335px'}], position: 1328, duration: 0 },
            { id: "eid20", tween: [ "style", "${_banderaCopy}", "top", '181px', { fromValue: '181px'}], position: 1328, duration: 0 },
            { id: "eid6", tween: [ "transform", "${_nubes}", "scaleX", '0.59211', { fromValue: '0.59211'}], position: 0, duration: 0 },
            { id: "eid35", tween: [ "style", "${_bandera}", "top", '79px', { fromValue: '79px'}], position: 1328, duration: 0 },
            { id: "eid16", tween: [ "transform", "${_bandera}", "rotateZ", '-16deg', { fromValue: '-16deg'}], position: 1328, duration: 0 },
            { id: "eid12", tween: [ "style", "${_nubes}", "top", '-77px', { fromValue: '-77px'}], position: 0, duration: 0 },
            { id: "eid15", tween: [ "transform", "${_bandera}", "scaleX", '-0.21743', { fromValue: '-0.21743'}], position: 1328, duration: 0 }         ]
      }
   }
},
"nubes": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'nube-02',
      type: 'image',
      rect: ['0','0','746px','332px','auto','auto'],
      fill: ['rgba(0,0,0,0)',im+'nube-02.png','0px','0px']
   },
   {
      id: 'nube-03',
      type: 'image',
      rect: ['460px','-47px','493px','158px','auto','auto'],
      fill: ['rgba(0,0,0,0)',im+'nube-03.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_nube-03}": [
            ["style", "height", '158px'],
            ["style", "top", '-110px'],
            ["style", "left", '262px'],
            ["style", "width", '493px']
         ],
         "${_nube-02}": [
            ["style", "height", '463px'],
            ["style", "left", '0px'],
            ["style", "width", '1040px']
         ],
         "${symbolSelector}": [
            ["style", "height", '380px'],
            ["style", "width", '760px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 175000,
         autoPlay: true,
         timeline: [
            { id: "eid8", tween: [ "style", "${_nube-03}", "top", '-110px', { fromValue: '-110px'}], position: 175000, duration: 0 },
            { id: "eid7", tween: [ "style", "${_nube-03}", "left", '-476px', { fromValue: '262px'}], position: 0, duration: 175000 },
            { id: "eid5", tween: [ "style", "${_nube-02}", "width", '963px', { fromValue: '1040px'}], position: 0, duration: 5000 },
            { id: "eid10", tween: [ "style", "${_nube-02}", "width", '536px', { fromValue: '963px'}], position: 5000, duration: 82500 },
            { id: "eid1", tween: [ "style", "${_nube-02}", "left", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid4", tween: [ "style", "${_nube-02}", "height", '428px', { fromValue: '463px'}], position: 0, duration: 5000 },
            { id: "eid9", tween: [ "style", "${_nube-02}", "height", '239px', { fromValue: '428px'}], position: 5000, duration: 82500 }         ]
      }
   }
},
"bandera": {
   version: "2.0.0",
   minimumCompatibleVersion: "2.0.0",
   build: "2.0.0.250",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: false,
   resizeInstances: false,
   content: {
   dom: [
   {
      id: 'bandera-02',
      type: 'image',
      rect: ['0','0','912px','103px','auto','auto'],
      fill: ['rgba(0,0,0,0)',im+'bandera-02.png','0px','0px']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_bandera-02}": [
            ["style", "left", '0px'],
            ["style", "overflow", 'hidden']
         ],
         "${symbolSelector}": [
            ["style", "height", '103px'],
            ["style", "width", '151px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 500,
         autoPlay: true,
         timeline: [
            { id: "eid1", tween: [ "style", "${_bandera-02}", "left", '0px', { fromValue: '0px'}], position: 0, duration: 0 },
            { id: "eid2", tween: [ "style", "${_bandera-02}", "left", '-152px', { fromValue: '0px'}], position: 100, duration: 0 },
            { id: "eid6", tween: [ "style", "${_bandera-02}", "left", '-304px', { fromValue: '-152px'}], position: 200, duration: 0 },
            { id: "eid7", tween: [ "style", "${_bandera-02}", "left", '-456px', { fromValue: '-304px'}], position: 300, duration: 0 },
            { id: "eid8", tween: [ "style", "${_bandera-02}", "left", '-608px', { fromValue: '-456px'}], position: 400, duration: 0 },
            { id: "eid9", tween: [ "style", "${_bandera-02}", "left", '-760px', { fromValue: '-608px'}], position: 500, duration: 0 }         ]
      }
   }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "CATE-10010035");
