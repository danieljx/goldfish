function cyclesApp(){
 myself = this;
	this.start = function(){
		myself.fnInternal();
		$("div#circles-container")
		.animate({width: '50%'}, 
			function(){
				$("#this-box-is-1").addClass('this-is-active');
				$("div#circles-container").removeClass('col-sm-12').addClass('col-sm-6');
					$("div#box-ref-1")
					.css("display","block")
					.animate({
						opacity:'1',
						left:'280'
					},"slow");
			})
	}

	//funciones internas
	this.fnInternal = function(){
		$(".wcircle-menu-item").click(function(){
			
			$(".wcircle-menu-item").removeClass("this-is-active");
			$(this).addClass("this-is-active");
			$(".box-ref")
			.css({"display":"none",
				  "opacity":"0",
				  "left":"0px"});
			
			$("#box-ref-"+$(this).attr("data-ref-box"))
			.css("display","block")
			.animate({
						opacity:'1',
						left:'280'
					},"slow");
		});

		$(".play-audio").click(function(){
			var id = $(this).parent().find("audio").attr("id");
			var state = $(this).attr("data-state-sound");
			var audio = document.getElementById(id);

			if( state == "false" ){
				 $(this).attr("data-state-sound","true");
				 audio.play();	
				 $(this).removeClass("glyphicon-play").addClass("glyphicon-pause");
			}else if( state == "true" ){
				 $(this).attr("data-state-sound","false");
				 audio.pause();	
				 $(this).removeClass("glyphicon-pause").addClass("glyphicon-play");	
			}
		});
	}
}

cycles = new cyclesApp();

xmlFile = 'userdata.xml';

function checkSize(){
	var height_desk  = $("header").height() + $("body").height() + 100;
	var height_movil = $("header").height() + $("body").height() + $("body").find(".box-ref").height();
	  
	  if ( $(window).width() < 770 ){
	   cat_plugin.growFrameH(height_movil); 
	  }
	  else{ 
	   cat_plugin.growFrameH(height_desk);
	    }
}

$(".wcircle-icon").click(function(){ checkSize(); })
$( window ).resize(function() { checkSize(); });
function addContent(xml){

	$(xml).find("userdata").each(function(){
		$(this).find("interface").each(function(){
			$("body").css("background-color", $(this).find("bgMain").text());
			$("header.circles-title")
			.css({
				"background-color": $(this).find("headerBg").text(),
				"color": $(this).find("titleMain").text(),
				"font-family": $(this).find("fontFamily").text()
			})
			.find("h1").text(  $(this).find("title").text() );
		});

		$(this).find("instructions").each(function(){

			$(".wcircle-icon")
				.find("div")
				.css({
					"color": $(this).find("textColor").text(),
					"background-color":$(this).find("bgColor").text(),
					"font-family": $(this).find("fontFamily").text()
				})
				.text($(this).find("text").text());
			});

		
			$(this).find("boton").each(function(){
				var html = '', class_ = '';
					var currentClass = 'class-circle-'+$(this).find("id").text();

						class_ += '<style>';
							class_ += '.'+currentClass+'{';
								class_ += 'background-color:'+$(this).find("bg").text()+' !important;';
								class_ += 'color:'+$(this).find("color").text()+' !important;';
								class_ += 'font-family:'+$(this).find("color").text();
							class_ += '}';
						class_ += '</style>';
							
					$("head").append(class_);		
					var id_ = $(this).find("id").text();
					html += '<div class="wcircle-menu-item '+currentClass+'" id="this-box-is-'+id_+'" data-ref-box="'+id_+'">';
						html += '<div>'+$(this).find("textBoton").text()+'</div>';
					html += '</div>';

					$(".wcircle-menu").append(html);
			});

			$(this).find("boton").find("box").each(function(){
			var htmlBox = '';
			var text_ = $(this).find("text").text();
			var id_   = $(this).find("idParent").text();
			var classForTxt = '', classImg ='';

			if( text_.length < 200 ){
					 classImg = 'box-medium', classForTxt = 'box-medium box-text-medium';
			if( text_.length < 90 ){ 
				classImg = 'box-short', classForTxt = 'box-text box-text-short'; }  
			} 


				htmlBox += '<div class="col-xs-12 col-sm-12 box-ref" id="box-ref-'+id_+'">';
					htmlBox += 	'<div class="box-global-container" style="background-color:'+$(this).find("bgBox").text()+'">';
							
							htmlBox +='<div class="box-type-img '+classImg+'" style="text-align: center;">';
						  		htmlBox +='<img src="files/'+$(this).find("img").text()+'" style="width:200px;" />';
						  	htmlBox +='</div>';
							  	

						  	htmlBox +='<div class="box-type-text '+classForTxt+'" style="color:'+$(this).find("colorText").text()+'">';
						  		htmlBox += text_;
						  	htmlBox +='</div>';
						  		htmlBox += '<div class="clear"></div>'
					  	htmlBox +='<div class="box-type-sound" class="col-xs-12">';
					  		htmlBox +='<span class="glyphicon glyphicon-play play-audio" data-state-sound="false"></span>';
					  			
					  			$(this).find("audio").each(function(){
					  				if( $(this).find("state").text() == 'true'){
					  					htmlBox += '<audio controls id="audio-'+id_+'">';
					  					//htmlBox += '<source src="'$(this).find("srcMp3").text()'" type="audio/ogg">';
										  	htmlBox += '<source src="files/'+$(this).find("srcMp3").text()+'" type="audio/mpeg">';
									htmlBox += '</audio>';
					  				}
								})

					  			

					  	htmlBox +='</div>';
					htmlBox += 	'</div>';
				htmlBox += 	'</div>';

				$("#global-boxs").append(htmlBox);
					
			});
		});
	
}


if ( window.XMLHttpRequest ) {

		    // Navegadores civilizados
		    $.support.cors = true;
		    $.ajax({
		        type: 'GET',
		        url: xmlFile,
		        dataType: ($.browser.msie) ? "text" : "xml",
		        async: false,
		        success: addContent,
		        error: function(XMLHttpRequest, textStatus, errorThrown) {
		            document.write('<script src="userdata.js"></script>');
		        }
		    });
		} else {
		    if (typeof window.ActiveXObject != 'undefined') {
		        // Internet Explorer
		        xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
		        xmlDoc.async = false;
		        while (xmlDoc.readyState != 6) {
		        }
	        xmlDoc.load(xmlFile);
	        addContent(xmlDoc);
    	}
}
