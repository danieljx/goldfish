/*
CREAR UN NUEVO PLUGIN LLAMADO (MARCAR NAVEGACION)
CAMBIAR EL CURSOR POR UN ICONO, QUE PUEDA GRABAR EL RECORRIDO DE ESTE
PARA MOSTRARLE AL USUARIO COMO DEBE NAVEGAR
mousedown = grabar left y top, guardarlos en un array json y grabaro en una animación
*/
function renderId(name){
		var id_   = new Date(),
			idEnd = id_.getTime();
		return idEnd+'-'+name;
	}

function pxToPercentage( valor, widthPadre ){
	var equivalent = (valor / widthPadre)*100;
	return equivalent;
}
//Funciones para el audio
function TemplateSound(file){
		var IdChild = renderId('audio');
		
		var soundBase = $("<div/>");
			soundBase.attr("data-child-audio", IdChild);
			soundBase.attr("class", "playSound");

		var secPlay = $("<section/>");
			secPlay.attr("class","audioPlayIcon");

		var Icon = $("<div/>");
		var img = $("<img/>");
			img.attr("src","files/play.svg");
			Icon.append(img);
			//Icon.attr("class","glyphicon glyphicon-play");

		var secAudioTag = $("<section/>");
			secAudioTag.attr("class", "audioTagElement");

		var AudioTag = $("<audio/>");
			AudioTag.attr("id", IdChild);

		var sourceTag = $("<source/>");
			sourceTag.attr("src",file);
			sourceTag.attr("type","audio/mpeg");

			AudioTag.append(sourceTag);
			secAudioTag.append(AudioTag);
			secPlay.append(Icon);
			soundBase.append(secAudioTag);
			soundBase.append(secPlay);

		return soundBase;
	}
//contenedor de un svg
function tmpSvg(file){
	var obj = $("<object/>");
		obj.attr("id", renderId('svg'));
		obj.addClass("gf-object-svg");
		obj.attr("type","image/svg+xml");
		obj.attr("data",file);
	return obj;
}//fin tmpSvg
//Template para las imagenes
function templateIMage(file){
		var selfChild = renderId('img');
		var img_ = $("<img/>");
			img_.attr("class",'dynamic-image');
			img_.addClass("gf-object-image");
			img_.attr("src",file);
			img_.attr("id",selfChild+"-image");
		return img_;
	}
//Template para el audio
function TemplateSound(file){
		var IdChild = renderId('audio');
		
		var soundBase = $("<div/>");
			soundBase.attr("data-child-audio", IdChild);
			soundBase.attr("class", "playSound");

		var secPlay = $("<section/>");
			secPlay.attr("class","audioPlayIcon");

		var Icon = $("<div/>");
		var img = $("<img/>");
			img.attr("src","files/play.svg");
			Icon.append(img);
			//Icon.attr("class","glyphicon glyphicon-play");

		var secAudioTag = $("<section/>");
			secAudioTag.attr("class", "audioTagElement");

		var AudioTag = $("<audio/>");
			AudioTag.attr("id", IdChild);

		var sourceTag = $("<source/>");
			sourceTag.attr("src",file);
			sourceTag.attr("type","audio/mpeg");

			AudioTag.append(sourceTag);
			secAudioTag.append(AudioTag);
			secPlay.append(Icon);
			soundBase.append(secAudioTag);
			soundBase.append(secPlay);

		return soundBase;
	}
//Template para cada texto
function tmpText(data){
	var div_1 = $("<div/>");
		div_1.addClass("gf-ecm-text-section");
		div_1.attr(renderId("text"));
		div_1.attr("data-font-size", data.header);//h1, h2, h3, h4
	var div_2 = $("<div/>");
		div_2.addClass("gf-ecm-text-show-header");
		div_2.text( data.text );
		div_1.append(div_2);
return div_1;
}
function TemplateDynamic(rack_){
		var this_id = renderId('dynamic');
		var section_ = $('<section/>');
		    section_.addClass( " gf-dinamyc-element" );
		    section_.attr("id",this_id);
		var div_circle = $("<div/>");
			div_circle.attr("class","gf-circle-edit-dinamyc-element");
		var span_ = $("<span/>");
			span_.attr("class","glyphicon glyphicon-pushpin");
		var div_body = $("<div/>");
			div_body.attr("class","gf-dinamyc-body");
			div_body.append(rack_.attr("id", this_id));
			div_circle.append( span_ );
			section_.append( div_body );
		return section_;
	}

function preLoad(){
	var widthMain = $(window).outerWidth();
	var heightMain = $(window).outerHeight();

	$(".gf-dinamyc-element").each(function(){
		var findImg = $(this).find("img");
		var findSound = $(this).find(".playSound")
	//Esta es la imagen	
		if ( findImg.length >= 1){  findImg.css({"width":"100%","height":"100%"})}
		if ( findSound.length >= 1){ findSound.css({"width":"100%","height":"100%"})}
			var widthMe  = $(this).outerWidth()  ,
				heightMe = $(this).outerHeight()  ,
				leftMe   = parseInt($(this).css("left")) - 145, 
				topMe    = parseInt($(this).css("top"));
			var toPercentageW = pxToPercentage( widthMe , widthMain ),
				toPercentageH = pxToPercentage( heightMe , widthMe ) ,
				toPercentageL = pxToPercentage( leftMe , widthMain ),
				toPercentageT = pxToPercentage( topMe , widthMain );

			$(this).css({ 
				"width": toPercentageW+"%", "height": toPercentageH+"%",
				"left": toPercentageL+"%", "top": toPercentageT+"%"
			});
	})
}

function addContent(xml){;
	var collageData    = $(xml).find("root").find("userdata");
	var collageVarsCss = collageData.find("collageDelabObjects").find('css');

	console.warn(collageVarsCss.find("leftMinus").text());

	collageData
		.find("gfObject").each(function(){
			 	var typeSource  = unescape($(this).attr("data-child"));
			 	var childStyles = (unescape($(this).attr("data-child-styles"))!= "undefined") ? unescape($(this).attr("data-child-styles")) : "";
			 	var source_     = unescape($(this).attr("data-child-src"));
				var styleParent = (unescape($(this).attr ("data-style")) != "undefined") ? unescape($(this).attr ("data-style")) : "";
				var dcy;

				console.warn(typeSource);

				switch (typeSource) {
					case "img":
						var img_ = templateIMage(source_);
							img_.attr("style",childStyles);
							dcy = TemplateDynamic( img_ );
							dcy.attr("style", styleParent);
					break;
					case "audio":
						var sound_ = TemplateSound(source_);
							sound_.attr("style",childStyles);
							dcy    = TemplateDynamic( sound_ );
							dcy.attr("style", styleParent);
					break;
					case "svg":
						var svg_ = tmpSvg( source_ );
							svg_.attr("style",childStyles);
							dcy = TemplateDynamic( svg_ );
							dcy.attr("style", styleParent);
					break;
					case "texto":
					var tmptext_   = {"header": source_, "text": unescape($(this).attr("data-texto"))};
					var texto_ = tmpText(tmptext_);
						texto_.attr("style",childStyles);
						dcy = TemplateDynamic( texto_ );
						dcy.attr("style", styleParent);
					break;
				}

				$("#gf-ecm-render-window").append( dcy );
			})
		preLoad();
}//fin del addContent
xmlFile = 'userdata.xml';

if ( window.XMLHttpRequest ) {

		    // Navegadores civilizados
		    $.support.cors = true;
		    $.ajax({
		        type: 'GET',
		        url: xmlFile,
		        dataType: ($.browser.msie) ? "text" : "xml",
		        async: false,
		        success: addContent,
		        error: function(XMLHttpRequest, textStatus, errorThrown) {
		            document.write('<script src="userdata.js"></script>');
		        }
		    });
		} else {
		    if (typeof window.ActiveXObject != 'undefined') {
		        // Internet Explorer
		        xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
		        xmlDoc.async = false;
		        while (xmlDoc.readyState != 6) {
		        }
	        xmlDoc.load(xmlFile);
	        addContent(xmlDoc);
    	}
}

/*$(window).resize(function(){
		console.info( $(window).outerHeight() );
		console.info( $(window).outerWidth() );

	$(".gf-dinamyc-body").each(function(){
		$(this).css("border","1px solid red");
		$(this).find("img").css("border","1px solid orange")
		console.info( "contenido "+ $(this).find("img").outerHeight());
		console.warn( "contenido "+ $(this).find("img").outerWidth());

		$(this).css({
			"max-width": $(this).find("img").outerWidth(), 
			"width" : $(window).outerWidth() - $(this).find("img").outerWidth() 
				})
	});
})*/