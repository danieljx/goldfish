/*ESCAPAR CARACTERES*/
function text(){
	var textSelf = this;
	//Inicializa toda la aplicación
	this.init = function(){
		textSelf.uiFunctions();
	}

	this.DraggableText = function(){
		//console.info("%c start text", "font-size:16px; background-color:orange;color:#fff;");
		$("#plugin-texto .gf-ecm-text-section").draggable({
			stop:function(event, ui){
				if($(this).attr("data-draggable")){
					var cloneThis = $(ui.helper[0]).clone();
					var header_   = {"header": cloneThis.attr("data-font-size"), "text":"Lorem ipsum..."}; 
					var tmptext   = textMain.tmpText(header_);
					var left_ 	  = cloneThis.css("left");
					var top_      = cloneThis.css("top");

					var panelEdit = ecm.TemplateDynamic(tmptext);	
						panelEdit.addClass("gf-dinamyc-element-draggable");
						panelEdit.css({"left": "50%","top":"20%"});

					$("#gf-ecm-render-window").append(panelEdit);
					$(ui.helper[0]).css({ "left":"auto",top:"auto"});
					
					setTimeout(function(){
						ecm.draggableDinamyc();
					},2000);
				}else{ console.info("solo arrastrar"); }
			}
		});
	}
	//Template para cada texto
	this.tmpText = function(data){
		var div_1 = $("<div/>");
			div_1.addClass("gf-ecm-text-section col-xs-4 marginNone gf-bg-1-opacity gf-text-draggable-tag");
			div_1.attr(ecm.renderId("text"));
			div_1.attr("data-font-size", data.header);//h1, h2, h3, h4
		var div_2 = $("<div/>");
			div_2.addClass("gf-ecm-text-show-header");
			div_2.text( data.text );
		var section_ = $("<section/>");
			section_.addClass("gf-ecm-text-input-for-edition gf-ecm-element-inactive");
		var input_ = $("<input/>");
			input_.attr("type","text");
			input_.addClass("input-font");
			section_.append(input_);
			div_1.append(section_);
			div_1.append(div_2);
		return div_1;
	}

	this.uiFunctions = function(){
		//Guardar con un enter
		$("body").on("keypress", '.gf-ecm-text-input-for-edition input.input-font', function(e){
			if ( e.keyCode == 13 ){ $(".gf-text-draggable-tag").trigger("dblclick") }
		});
		//mostrar/ocultar input edición de texto
		$("body").on("dblclick",".gf-text-draggable-tag", function(){
			var tagHead = $(this).find(".gf-ecm-text-show-header"),
				input_tag = $(this).find(".gf-ecm-text-input-for-edition");
			if ( input_tag.hasClass('gf-ecm-element-inactive') == true ){
				input_tag.removeClass("gf-ecm-element-inactive");
				tagHead.addClass("gf-ecm-element-inactive");
				input_tag.find('input[type=text]').val(tagHead.html());
			}else{
				console.warn(input_tag.find('input[type=text]').val());
				tagHead.html(input_tag.find('input[type=text]').val());
				tagHead.removeClass("gf-ecm-element-inactive");
				input_tag.addClass("gf-ecm-element-inactive");
			}
		})//fin mostrar/ocultar input edición de texto
	}//fin uiFunctions
}
var textMain = new text();
	textMain.init();

$(document).ready(function(){
	setTimeout(function(){
		textMain.DraggableText();
	},0)
})

