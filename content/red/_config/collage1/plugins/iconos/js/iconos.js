/*RECUERDA EL FILTRO POR ESTADOS DE ANIMOS PARA LOS PERSONAJES*/
/*CREAR FILTROS PARA LAS IMAGENES, POR TAMAÑO ESTUDIAR POSIBILIDAD DE UN RANGO()*/

	/*

	$scss = new scssc();

		$scss->setFormatter('scss_formatter_compressed');
		// add_log('themesass:('.$theme->sass.')');
		// add_log('blocksass:('.$block->sass.')');
		$css = $scss->compile($theme->sass.' '.$block->sass);

	Objetivos fijos.
	Donde se vuelva mas complejo crear submodelos.

	*/
function iconsP(){
	var iconSelf = this;
	this.init = function(){
		iconSelf.uiFunctions();
		//iconSelf.snapFunctionLoad();
		setTimeout(function(){
			iconSelf.putResources();
		},1000)
		//console.warn( resources.getToolsCharacter() ); 
	}
	this.putResources = function(){
		var resources = new iconSelf.resources();
		var char = resources.getCharacters();
			if ( char.length >= 1 ) {
				for (var i = 0; i <= char.length -1; i++) {
					var li_list = $("<li/>");
						li_list.addClass("gf-dinamyc-element-draggable gallery-img");
						li_list.attr("data-dinamyc","true");
						li_list.attr("data-type-filter", char[i].type);
						li_list.attr("data-gender-filter", char[i].gender);
						
					var img_list = $("<img/>");
						img_list.attr("src", "files/recursos/Personajes/"+char[i].file);

						li_list.append(img_list);
					$("#character-gallery ul.Gallery-icon").append(li_list);
				}
			}

		var itemsChar = resources.getToolsCharacter();
		for (var i = 0; i <= itemsChar.length -1; i++) {
				var figure_tool = $("<figure/>");
					figure_tool.attr("data-tag-filter",itemsChar[i].tag);
					figure_tool.attr("data-type-filter",itemsChar[i].type);
					figure_tool.attr("data-gender-filter",itemsChar[i].gender);
					figure_tool.attr("data-emotion-filter",itemsChar[i].emotion);
					figure_tool.attr("data-disposition-filter",itemsChar[i].compatibility);
			
				var img_tool = $("<img>");
					img_tool.attr("src", "files/recursos/Materiales/"+itemsChar[i].file);
					figure_tool.append(img_tool);
				$("#tools-character-each").append(figure_tool);
		}
			ecm.draggableDinamyc();
	}
	//Nueva clase
	this.resources = function(url){
		var link_url = url ? url : "";
		//recursos para añadir a los personajes
		this.getToolsCharacter = function(){
			var resourceSelf = this;
				this.resources;
				$.ajax({
					url: 'plugins/iconos/json/resources.json',
					type:"get",
					async:false,
					success: function(res) {
						resourceSelf.resources = res;
					},
					error : function(error){
						console.info(error)
					}
				});
			return this.resources;
		}
		//Personajes
		this.getCharacters = function(){
			var charSelft = this;
				this.customsResources;
			$.ajax({
				url: 'plugins/iconos/json/personajes.json',
				type:"get",
				async:false,
				success: function(response) {
					charSelft.customsResources = response;
				},
				error : function(error){
					console.info(error)
				}
			});
			return charSelft.customsResources;
		}
	}
	/*FUNCIONES PARA EL SVG*/
	var dropSvg = function(){
			$("#character").find("g.svg-selected").each(
				function(){ 
					$(this).removeClass("svg-selected");
					$(this).attr("class","");
			});
			$(this.node).attr("class","svg-selected");
			/*$(this.node).attr("class","svg-selected");
			$(this.node).addClass("svg-selected");
			$("#"+ $(this.node).attr("id")).addClass("svg-selected");
			$("footer#listBtns").attr("data-svg-id", $(this.node).attr("id"));*/
		}

	var move = function(dx, dy){
			dropSvg()
			this.attr({
				transform: this.data('origTransform') + (this.data('origTransform') ? "T": "t")+ [dx, dy]
			})
		}
	var start = function(){
			this.data('origTransform', this.transform().local )
		}
	var stop = function(){
			dropSvg();
			$(".gf-incons-save-btn button.dropGroupSvg").attr("data-drop-id");
		}
	var nextFrame = function(el, arrayFrame, cualFrame){
	     	if ( cualFrame >= arrayFrame.length ){ return };
	     	el.animate( arrayFrame[cualFrame].animation, arrayFrame[cualFrame].dur, nextFrame.bind(null, el, arrayFrame, cualFrame+1) )
	     }
	/************/

	this.snapFunctionLoad = function(){
		console.info("%c  Funciones de snap","font-size:14px;background-color:orange;color:#fff;padding:2px;");
		
		var parpadeo = [
			/*{	animation: { transform: 'r360,150,150' }, dur: 1000 },
	        {   animation: { transform: 't100,-100s2,3' }, dur: 1000 },
	        {   animation: { transform: 't100,100' }, dur: 1000 },
	        {   animation: { transform: 's0,1' }, dur: 1000 },*/
	        {   animation: { transform: 's1,0' }, dur: 500 },
	        {   animation: { transform: 's1,1' }, dur: 500 },
	        //{   animation: { transform: 'r20,200,200' }, dur: 1000 },
	        //{   animation: { transform: 'r0,90,100' }, dur: 1000 },
	        //{   animation: { transform: 'r-20,200,200' }, dur: 1000 },
	        //{   animation: { transform: 'r0,90,100' }, dur: 1000 }
	        ];

	    var talking = [
	    		{   animation: { transform: 's0.2,0.5' }, dur: 500 },
	        	{   animation: { transform: 's0.3,0.2' }, dur: 500 },
	        	{   animation: { transform: 's0.6,0.7' }, dur: 500 },
	        	//{   animation: { transform: 't4,4' }, dur: 500 },
	        	{   animation: { transform: 's0.2,0.3' }, dur: 500 },
	        	//{   animation: { transform: 't6,4' }, dur: 500 },
	        	{   animation: { transform: 's0.3,0' }, dur: 500 },
	        	{   animation: { transform: 's1,1' }, dur: 500 },
	    	]
		var personaje  = Snap.select("#character");

		var cabello = personaje.select("#hair");
		var cabeza  = personaje.select("#faces")
		
		var cejas   = personaje.select("#eyebrows")
		var cejaDer = cejas.select("#eyebrowsRight")
		var cejaIzq = cejas.select("#eyebrowsLeft")

		var ojos    = personaje.select("#eyes")
		var ojosDer = ojos.select("#eyeRight")
		var ojosIzq = ojos.select("#eyeLeft")

		var nariz   = personaje.select("#nose");

		var boca   = personaje.select("#mouths");

		var lentes  = personaje.select("#lentes");
		var bigote  = personaje.select("#bigote");
		var gorra  = personaje.select("#cap");

		
		cabello.click(dropSvg)
		ojos.click(dropSvg);
		gorra.click(dropSvg);
		ojos.click(function(){
			nextFrame(ojos, parpadeo, 0);
		})
		cabeza.click(dropSvg)
		cejaDer.click(dropSvg)
		cejaIzq.click(dropSvg)
		ojosDer.click(dropSvg)
		ojosIzq.click(dropSvg)
		boca.click(function(){
			nextFrame(boca, talking, 0);
		})
		boca.click(dropSvg);
		nariz.click(dropSvg)
		lentes.click(dropSvg)
		bigote.click(dropSvg);

		//Arrastrar
		gorra.drag( move, start, stop);
		bigote.drag( move, start, stop);
		lentes.drag( move, start, stop);
		ojosDer.drag( move, start, stop);
		ojosIzq.drag( move, start, stop);
		boca.drag( move, start, stop);
		nariz.drag( move, start, stop);
		cejaDer.drag( move, start, stop);
		cejaIzq.drag( move, start, stop);
		cabello.drag( move, start, stop);
		cabeza.drag( move, start, stop);
	}
	this.addSpectrumColor = function( thisStyle ){
		console.info("%c Agregar spectrum color","font-size:22px;color:orange;");
		var styles_tags = ""; 
		for (var i = 1; i <= thisStyle.length - 1; i++) {
			styles_tags = "";
			var ident = ecm.renderId("clases");
			var classMain = $("<div/>");
				classMain.addClass("class-char-svg");
			var div_title = $("<div/>");
				div_title.addClass("class-char-svg-name");
				div_title.text(thisStyle[i].className);
			var div_btn = $("<div/>");
				div_btn.addClass("class-char-svg-btns");
			var btn = $("<button/>");
				btn.addClass("btn btn-default btn-show-styles");
				btn.attr("data-id-child",ident);
			var span_ = $("<span/>");
				span_.addClass("glyphicon glyphicon-chevron-right");

				btn.append(span_);
				div_btn.append(btn);
				classMain.append(div_title);
				classMain.append(div_btn);

			var div_container = $("<div/>");
				div_container.addClass("svg-styles-all gf-ecm-element-inactive");
				div_container.attr("id",ident);

			//Recoore los estilos
			for (var j = 0; j <= thisStyle[i].styles.length - 1; j++) {
				var attr_type;
				var buscarRule = thisStyle[i].styles[j].attrName.search("rule");
				var buscarFill = thisStyle[i].styles[j].attrName.search("fill");
				var class_ = "", color_ = "#fff";

				if ( buscarRule != -1 ){
					var input_text = $("<input/>");
						input_text.attr("type","text");
						input_text.attr("name","propertie"+[i]);
						input_text.addClass("value-style-svg");
						input_text.val( thisStyle[i].styles[j].attrVal );
						attr_type = "attr";
				}

				if ( buscarFill == 0) {
					color_ = thisStyle[i].styles[j].attrVal;
					var div_fill = $("<div/>");
						div_fill.addClass("fill-style-svg");
					var div_fill_color = $("<div/>");
						div_fill_color.addClass("fill-tag-color");
					var inputColor = $("<input/>");
						inputColor.attr("type","text");
						inputColor.addClass("custom");
					var div_fill_value = $("<div/>");
						div_fill_value.addClass("fill-tag-value");
						div_fill_value.text( color_ );
					class_ = "special-svg-color";
					div_fill_color.append( inputColor );
					div_fill.append( div_fill_color );
					div_fill.append( div_fill_value );
				}

				var div_style = $("<div/>");
					div_style.addClass("this-svg-style "+class_);
				var div_label = $("<label/>");
					div_label.attr("for", "propertie"+[j]);
					div_label.addClass("title-style-svg");
					div_label.attr("data-edition-style", attr_type );
					div_label.text( thisStyle[i].styles[j].attrName );
				var icon_style = $("<div/>");
					icon_style.addClass("icon-style-svg");
				var span_icon = $("<span/>");
					span_icon.addClass("glyphicon glyphicon-cog");

					icon_style.append(span_icon);
						if ( buscarFill != -1) { div_label.append(div_fill); }
					div_style.append( div_label );
					div_style.append( icon_style );
						if ( buscarRule != -1 ){ div_style.append( input_text ); }
					div_container.append( div_style );
				}

			$("#class-svg").append(classMain , div_container);

			$(".custom").spectrum({
				color: color_,
				showAlpha: true,
				showButtons: false,
					move: function(color) {
					    color.toHexString(); 
					}
			});
		}	
	}

 this.uiFunctions = function(){
 	$("body").on("click",".menu-filter-main div.item", function(){
 		var tag_type_filter_   = $("#filter-style-character").val() ? $("#filter-style-character").val() : "";;
 		var tag_gender_filter_ = $("#filter-gender-character").val() ? $("#filter-gender-character").val() : "";;
 		$(".galleria-personajes .Gallery-icon li").each(function(){
 			var tag_type_filter  = $(this).attr("data-type-filter");
 			var tag_gender_filter = $(this).attr("data-gender-filter");

 			if ( tag_type_filter_ != tag_type_filter) {
 				$(this).fadeOut(0);
 			}else if (tag_type_filter_ == tag_type_filter ){
 				if ( tag_gender_filter != tag_gender_filter_ ){
 					$(this).fadeOut(0);
 				}else if ( tag_gender_filter == tag_gender_filter_) {
 					$(this).fadeIn("slow");
 				}
 			}
 		})
 	})
 	$("body").on("click", "#tools-character-each figure", function(){
 		var url = $(this).find("img").attr("src");
 		var currId = $(this).attr("data-tag-filter");
 		var s = Snap("#character");
 		var g = s.group();
 		console.warn(currId);
 		 $("#character #"+currId).remove()
		var tux = Snap.load( url , function ( loadedFragment ) {
					g.append( loadedFragment );
                    g.click(dropSvg);
				    g.drag( move, start, stop);

				   
				    $("svg.svg-edition").attr("width","200px");
				    $("svg.svg-small").attr("width","100px")
                } );
 	})
 		$("body").on("click",".menu-filter div.item", function(){
 			var filterTag  = $("#filter-tools").val() ? $("#filter-tools").val() : "";
 			
 			var filterStyle  = $("#style-char").val() ? $("#style-char").val() : "";
 			var filterGender = $("#gender-char").val() ? $("#gender-char").val() : "";
 			var filterEmotion = $("#emotion-char").val() ? $("#emotion-char").val() : "";

 			//console.info(filterStyle);

 			$("#tools-character-each figure").each(function(){
 				var filterTag_    = $(this).attr("data-tag-filter");
 				var filterStyle_  = $(this).attr("data-type-filter");
 				var filterGender_ = $(this).attr("data-gender-filter");
 				var filterEmotion_= $(this).attr("data-emotion-filter");

 				if ( filterStyle != filterStyle_ ){
 						$(this).fadeOut("fast");
 				}else if ( filterStyle == filterStyle_ ){
 					//mostra o oculta los generos
 						
 					if ( filterGender_.indexOf(filterGender) == -1 )
 						{ $(this).fadeOut("fast"); console.info("Ocultar genero"); }
 					else if ( filterGender_.indexOf(filterGender) != -1 )
 						{ 
 							//Muestra y oculta las emociones
 							if ( filterEmotion_.indexOf(filterEmotion) == -1 )
 								{ $(this).fadeOut("fast"); console.info("Ocultar Emoción"); }
 							if ( filterEmotion_.indexOf(filterEmotion) != -1 )
 								{ 	
 									console.warn(filterTag);
 									console.info(filterTag_);
 									if ( filterTag_.indexOf(filterTag) == -1){ $(this).fadeOut("fast"); }
 									if ( filterTag_.indexOf(filterTag) != -1){ 
 										$(this).fadeIn(); 
 									}
 								}
 						}
 				}
 			})
 		});
		$("body").on("click",".gf-gallery-icon-global", function(){
			$("#char-menu-contextmenu").addClass("gf-ecm-element-inactive");
		})
		$("body").on("click","#prueba", function(){
			console.info("hello info");
			$('.segment').dimmer('show');
		})
		$("body").on("click","#char-menu-contextmenu ul li", function(){
			var attr_act = $(this).attr("data-action");
				if ( attr_act == "custom" ){
					var imag_obj = $(".gf-elemen-this-selected").find("img");
					var object_tag  = $("<object/>");
						object_tag.attr("id","character");
						object_tag.attr("data",imag_obj.attr("src"));
						object_tag.attr("type","image/svg+xml");
					$("#character").html("");

					var s = Snap("#character");
					var g = s.group();

					var tux = Snap.load( imag_obj.attr("src") , function ( loadedFragment ) {
                                              g.append( loadedFragment );
                                        });
					
					setTimeout(function(){
						console.warn("loaded")
						iconSelf.snapFunctionLoad()
					},1000);

					$(".gf-ecm-view-main ul li").trigger("click");
				}

				
		});
		$("body").on("contextmenu","#character-gallery ul.Gallery-icon li", function(e){
			$(".gf-elemen-this-selected").each(function(){
				$(this).removeClass("gf-elemen-this-selected");
			})
			$(this).addClass("gf-elemen-this-selected");
			e.preventDefault();
				var x = event.clientX;  
				var y = event.clientY; 
				$("#char-menu-contextmenu")
					.removeClass("gf-ecm-element-inactive")
					.css({"left": (x-100)+"px", "top": (y-100)+"px"});
		});
		$("body").on("click","#close-edition-panel", function(){
				$(".panel-edition-skill").addClass("gf-ecm-element-inactive");
		})
		//editar 
		$("body").on("click", ".gf-incons-save-btn button#editSvgGroup", function(){
			console.info("valiria");
			/*var styles = $("style#styles_by_svg").html();
			var tagClass = [];
			var clases = styles.split(".");

			for (var i = 0; i <= clases.length - 1; i++) {
					var thisStyle =  clases[i].split("{");
						tagClass.push({className: thisStyle[0], styles:[]})
				//Recorre los estilos
				for (var j = 0; j <= thisStyle.length - 1 ; j++) {
					//solo los estilos no la clase
					if ( j == 1){
					//Cortamos la cadena para sacar la sección de cada estilos
						var attr_Cls = thisStyle[j].split(";");
					for (var k = 0; k <= attr_Cls.length - 2; k++) {
						var attr_ = attr_Cls[k].split(":");
							tagClass[i].styles.push({ "attrName": attr_[0], "attrVal": attr_[1]}) 
						}//fin del for
					}//fin del if
				}//fin del for
			}//fin del for

			//Abrir edición
			iconSelf.addSpectrumColor(tagClass);
			*/$(".panel-edition-skill").removeClass("gf-ecm-element-inactive").draggable({ cancel: ".container_" });;
		});
	
		$("body").on("click", ".gf-incons-save-btn button.dropGroupSvg", function(){
			console.error("%c Eliminar tag SVG","padding:5px;font-size:22px;");
			$(".svg-selected").remove();
		});
		$("body").on("click",".create-elements-nav-main ul li", function(){
			$(".create-elements-nav-main ul li").removeClass("active");
			$(this).addClass("active")
		});
		$("body").on("click",".gf-incons-save-btn button.saveAsSvg", function(){
			//console.info("Edición...");
			$("#save-personaje-config").html( "" );
			//var svgEnd = $("#character-creating").html(); 
			var img = $("#character-creating").html();
				$("#save-personaje-config").append( img );
				
				$('.modal').modal('show');

			console.warn( $('.modal').length );

  			$('.animation-for-character .item').tab();
			/*var name_ = ecm.renderId("character");
			iconSelf.personajesJSON.push( {file: name_+'.svg', type: "flat", gender: "female"} );
			svg = { svg:svgEnd, name: name_, toJson: iconSelf.personajesJSON };
			$.ajax({
			 		url: "plugins/iconos/php/newSvg.php",
			 		type:"post",	
			 		async:false,
			 		data: svg,
			 		success: function(result){
			        	console.info(result);
			    }});
			$("#gf-gallery-show ul li.gf-bg1-icons").attr("data-load", "true");*/
		})
		$("body").on("click","#resize-full", function(){
			$("article.gf-ecm-body").css({"transition":"0s all"})
			var state_  = $(this).attr('data-full');
			var height_ = ($("#gf-ecm-body-lateral-menu").outerHeight() - 100);
			
			if ( state_ == "false"){
					$(this).find("span.glyphicon").removeClass("glyphicon-resize-full").addClass("glyphicon-resize-small");
					$(this).attr('data-full','true');
					$("#gf-ecm-body-lateral-menu").addClass("resize-full-active");
					$(".character-preview").css("height",height_+"px");
					$(".gallery-character-properties").css("height",height_+"px");
				}

				if ( state_ == "true"){
					$(this).find("span.glyphicon").addClass("glyphicon-resize-small").removeClass("glyphicon-resize-full");
					$(this).attr('data-full','false');
					$("#gf-ecm-body-lateral-menu").removeClass("resize-full-active");	
					$(".character-preview").css("height","350px");
					$(".gallery-character-properties").css("height","350px");
				}
				$("article.gf-ecm-body").css({"transition":"0.5s all"})
		})
			
		$("body").on("click",".return-view-icons", function(){
			$(".gf-gallery-icon-global").addClass("gf-ecm-element-inactive");
			$(".main-tools-icons").removeClass("gf-ecm-element-inactive");
			$("#gf-gallery-show").addClass("gf-ecm-element-inactive");
			$("#tool-main-title").find("span.return-view-icons").addClass("gf-ecm-element-inactive");
			$("#tool-main-title").find("h5").text( "Recursos" );
			ecm.sizeEditionPanel( 400 );
		})
		$("body").on("click",".gf-ecm-view-main ul li", function(param){
			var attrId 	   = $(this).attr("data-child-show");
			var headerMain = $("#tool-main-title");
				headerMain.removeClass("gf-ecm-element-inactive");
				headerMain.find("h5").text( $(this).find('.title').text() );
				headerMain.find("span.return-view-icons").removeClass("gf-ecm-element-inactive");
			var heihg_box, width_box = 400;
				switch (attrId) {
					case "create-character":  width_box = 500	; break;
				}
				if ( attrId ){
					$(".gf-ecm-view-main").addClass("gf-ecm-element-inactive");
					$(".gf-gallery-icon-global").addClass("gf-ecm-element-inactive");
					$("#"+attrId).removeClass("gf-ecm-element-inactive");
				}
				ecm.sizeEditionPanel( width_box );
		});
	}
}

var icons_ = new iconsP();
	icons_.init();

$(document).ready(function(){
	setTimeout(function(){
		$('.dropdown').dropdown();
		$('.tag.example .ui.dropdown') .dropdown({ allowAdditions: true })
	},2000);
})