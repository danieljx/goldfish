/*
CREAR UN NUEVO PLUGIN LLAMADO (MARCAR NAVEGACION)
CAMBIAR EL CURSOR POR UN ICONO, QUE PUEDA GRABAR EL RECORRIDO DE ESTE
PARA MOSTRARLE AL USUARIO COMO DEBE NAVEGAR
mousedown = grabar left y top, guardarlos en un array json y grabaro en una animación
*/
function addContent(){
	console.info("%c addContent collage",'font-size:19px; color:magenta;');
	xml = $(window.xmlContent);
	var collageData    = xml.find("root").find("userdata");
	var collageVarsCss = collageData.find("collageDelabObjects").find('css');

	console.info("z-index:"+ collageVarsCss.find("globalZindex").text());
	$("#gf-ecm-render-window").attr("data-global-zindex", collageVarsCss.find("globalZindex").text());

	collageData
		.find("gfObject").each(function(){
			 	var typeSource  = unescape($(this).attr("data-child"));
			 	var childStyles = (unescape($(this).attr("data-child-styles"))!= "undefined") ? unescape($(this).attr("data-child-styles")) : "";
			 	var source_     = unescape($(this).attr("data-child-src"));
				var styleParent = (unescape($(this).attr ("data-style")) != "undefined") ? unescape($(this).attr ("data-style")) : "";
				var dcy;
				switch (typeSource) {
					case "img":
						var img_ = multimedia.templateIMage(source_);
							img_.attr("style",childStyles);
						    dcy = ecm.TemplateDynamic( img_ );
							dcy.attr("style", styleParent);
					break;
					case "audio":
						var sound_ = multimedia.TemplateSound(source_);
							sound_.attr("style",childStyles);
							dcy = ecm.TemplateDynamic( sound_ );
							dcy.attr("style", styleParent);
					break;
					case "svg":
						var svg_ = ecm.tmpSvg( source_ );
							svg_.attr("style",childStyles);
							dcy = ecm.TemplateDynamic( svg_ );
							dcy.attr("style", styleParent);
					break;
					case "texto":
					var tmptext   = {"header": source_, "text": unescape($(this).attr("data-texto"))};
					var texto_ = textMain.tmpText(tmptext);
						texto_.attr("style",childStyles);
						
						dcy = ecm.TemplateDynamic( texto_ );
						dcy.attr("style", styleParent);
						var fntS = dcy.css("font-size");
						var fntF = dcy.css("font-family");
						//Colocar el estilo de la fiente a cada texto
						if (  fntS != "" && fntF != ""){
							dcy.find(".gf-ecm-text-show-header").css({
								"font-size": fntS,
								"font-family":fntF
							})
						}
					break;
				}
			$("#gf-ecm-render-window").append( dcy );
		 });
		ecm.draggableDinamyc();
		textMain.DraggableText();
}//fin del addContent

function readParamsToXml(){
	console.info("%c Guardando contenido...",'font-size:19px; color:#fff;background-color:#5cd65c;');
	var xml_ = '<?xml version="1.0" encoding="UTF-8"?><root><userdata>';
	xml_ += '<collageDelabObjects>';
		xml_ += '<css>';
			xml_ += "<leftMinus>"+($("aside.gf-ecm-left-menu").outerWidth() + 145)+"</leftMinus>";
			xml_ += '<globalZindex>'+$("#gf-ecm-render-window").attr("data-global-zindex")+'</globalZindex>';
		xml_ += '</css>';
	xml_ += '</collageDelabObjects>';
		$("#gf-ecm-render-window")
			.find(".gf-dinamyc-element")
				.each(function(){

					//ESTILOS DEL ELEMENTO GLOBAL
					var style_ = (escape($(this).attr("style")) != "undefined") ? escape($(this).attr("style")) : "";
					var conten = $(this).find(".gf-dinamyc-body");
					var images = conten.find("img.gf-object-image");
					var audios = conten.find("div.playSound");
					var SVGS   = conten.find("object.gf-object-svg");
					var texto  = conten.find("div.gf-ecm-text-section");
 					xml_ += '<gfObject data-style="'+style_+'" ';
					//Buscando Imagenes
						if ( images.length >= 1){
							var styles_child = ( escape(images.attr("style")) != "undefined") ? escape(images.attr("style")) : "";
								xml_ += ' data-child="img" ';
								xml_ += ' data-child-styles="'+ styles_child +'" ';
								xml_ += ' data-child-src="'+escape(images.attr("src"))+'">';
							//console.info(images.attr("id"); ?
						}
					//Buscando audio	
						if ( audios.length >= 1 ){
							var audio_src = audios.find(".audioTagElement").find('audio').find("source").attr("src")
							//var styles_child = ( escape(audios.attr("style")) != "undefined" );
								xml_ += ' data-child="audio" ';
								xml_ += ' data-child-styles="'+escape(audios.attr("style"))+'" ';
								xml_ += ' data-child-src="'+escape(audio_src)+'">';
								//console.info(audios.attr("id"); ?
								/*Nota: pensar que esta opción tiene un svg
								 que cambia de color según la selección del usuario*/
						}
					//Buscando SVG
						if ( SVGS.length >= 1 ){
								xml_ += ' data-child="svg" ';
								xml_ += ' data-child-styles="'+escape(SVGS.attr("style"))+'" ';
								xml_ += ' data-child-src="'+escape(SVGS.attr("data"))+'">';
						}
					//Buscando Texto
						if ( texto.length >= 1 ){
								xml_ += ' data-child="texto" ';
								xml_ += ' data-texto="'+escape(texto.find(".gf-ecm-text-show-header").text().trim())+'" ';
								xml_ += ' data-child-styles="'+escape(texto.attr("style"))+'" ';
								xml_ += ' data-child-src="'+escape(texto.attr("data-font-size"))+'">';
						}
					xml_ += "</gfObject>";
				});
			//xml_ += '<gfObject data-style="" data-child="" data-src="" data-child-styles="" data-child-src=""></gfObject>';
		xml_ += "</userdata></root>";

	console.warn( xml_ );
	return xml_;
}