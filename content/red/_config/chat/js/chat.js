//var main, where set the XML data
var XMLdata;
/*::::::::::::::::::LOAD XML, TEMPLATES, SET STYLES AND PUT CONVERSATION:::::::::::::::::::*/
/*
 * Esta funcion trabaja como una clase, incluyendo diferentes metodos para cargar contenido
 * XML()    = Carga los datos del config.xml
 * THEME()  = Llama al template o html de la aplicación
 * STYLES() = Con los datos del XML coloca todos los estilos por defecto o modificados, según sea el caso
 * CONVERSATION() = Coloca los mensajes del chat
 */

var count = 0;
function load(){
	var self = this;
	this.THEME = function(data){
		
		$.get('tpl/theme.mustache', function(templatesFile) {
			var template = templatesFile;
			var viewParam = [];
				viewParam.id = data.view;
			var output = Mustache.render( $(template).filter(data.id).html(), viewParam );
				$( "#"+data.container+"").append(output);
			addCounter();
		});
	}

	this.STYLES = function(xml){
		//para los personajes principales
		$(XMLdata).find('characters').each(function($index){
				var imag_conf = $(this).find('image_config');
				var indi = $index+1;
				var eleme = $("#main-char-"+indi);
				
					eleme
					.find("#image-user-container")
					.css({"border-color": imag_conf.find('border_color').text().trim()});

					eleme
					.find("#bgChar")
					.css({"background-color": imag_conf.find('background_color').text().trim()});

					eleme
					.find("#snackUnique")
					.css({
						"background-color": $(this).find('snack_config').find('bg_color').text().trim(),
						"color":$(this).find('snack_config').find('color_text').text().trim(),
						"font-family":$(this).find('snack_config').find('text_font').text().trim()});

					eleme
					.find("#snack-comilla")
					.css({"border-bottom-color": $(this).find('snack_config').find('bg_snack_comilla').text().trim()});

					eleme
					.find("#bgImgCnt")
					.css({
						"background-image": "url("+window.location.origin+'/'+datafolder+'/uploads/'+imag_conf.find('image').find('url').text().trim()+")",

						//"background-image": "url("+wwwupload+'/uploads/'+imag_conf.find('image').find('url').text().trim()+")",
						"background-size" : imag_conf.find('image').find('width').text().trim(),
						"background-position" : imag_conf.find('image').find('position').find('margin_left').text().trim() +" "+imag_conf.find('image').find('position').find('margin_top').text().trim()
					});
		});//end styles characters main
		//put main image
		$(XMLdata).find('viewMain').each(function($index){
					$("#edit-interfaz")
					.find('.container-personal-Addchat')
					.find('#appBgMain').find('img')
					//.attr('src', wwwupload+'/uploads/'+$(this).find('imgBg').text());
					.attr('src', window.location.origin+'/'+datafolder+'/uploads/'+$(this).find('imgBg').text());

		 });
		//put chat image
		$(XMLdata).find('chatImage').each(function($index){	
					$('#mainBgChat')
					//.find('img').attr('src', wwwupload+'/uploads/'+$(this).text());
					.find('img').attr('src', window.location.origin+'/'+datafolder+'/uploads/'+$(this).text());

		 });
		//put data screen main chat
		 $(XMLdata).find('viewMain').each(function($index){
	    		var text      = $(this).find("text_Main");
	    		var mainInput = $("input#bgMainText");
	    	    /*put text in edit input */
		    	text.find('addClass').find('style').each(function(){
		    		mainInput.addClass($(this).text());
		    	});
		    	mainInput.val(text.find('text').text()).css({
		    		"font-size": text.find('text_size').text()
		});
		    
	/*Datos del contenedor del texto principal, ancho, colores , posición, etc*/
		var boxcont = $(this).find('contBox');
		 	$("#prevFondo")
		 	.css({
		 		"width":parseInt(boxcont.find('width').text())+"%",
		 		"border-color": boxcont.find('brColor').text(),
		 		"background-color": boxcont.find('bgColor').text(),
		 		"color":"#fff",
		 		"top": parseInt(boxcont.find('position').find('top').text())+"%",
		 		"left": parseInt(boxcont.find('position').find('left').text())+"%"
		 	})
			//ancho
			$("input#ancBr").attr('value', boxcont.find('Anchobr').text())
			//tamaño
			$("#boxBgMainWidth").attr('value', parseInt(boxcont.find('width').text()));
			//color del borde
			$("#boxBgMain").css({"background-color": boxcont.find('brColor').text()});
			//color del fondo
			$("#boxBrMain").css({"background-color": boxcont.find('bgColor').text()});
			//Posición 1.top 2.left

			$('input#pos_up').attr("value",parseInt(boxcont.find('position').find('top').text()));
			$('input#pos_left').attr("value", parseInt(boxcont.find('position').find('left').text()));
		/*FIN*/
		/*Propiedades de la imagen, tamaño*/
			var wht 	   = $(this).find('weight');
			var ste_custom = wht.find('custom').find('state').text();
			var ste_fixed  = wht.find('fixed').find('state').text();
			$(".imgWeight").removeClass('btn-success').attr('data-state', 'false');

			if( ste_custom == 'true' &&  ste_fixed == 'false' ){
				$("#customData").addClass('btn-success').attr('data-state', 'true');
				$('input#ancho_img')
				.attr('value', parseInt(wht.find('custom').find('width').text()));
				$('input#alto_img')
				.attr('value',parseInt(wht.find('custom').find('height').text()));
			}
			else if( wht.find('custom').find('state').text() == 'false' && wht.find('fixed').find('state').text() == 'true')
			{ 	$("#fixed").addClass('btn-success').attr('data-state', 'true'); }
		});
		 $("#loadAllpage").css("display","none");
	}//end STYLES()

	this.CONVERSATION = function(){
			$.get('tpl/templates.mustache', function(templatesFile) {
			var templates = templatesFile;
			var msjDirimg, msjDirText;
			var chatClass = [];
			var newPage   = [], stat, faudio = [];


		$(XMLdata).find('conversation').each(function(){
			if( $(this).find('character_active').text().trim() == 'char-1' )
						{
							msjDirimg   = '',
							msjDirText  = '';

							chatClass.msjChar 	   = "dialog-1-char";
							chatClass.msjCharSnack = "dialog-snack-1-char";
							chatClass.imageChar    = "dialog-snack-1-char";
							chatClass.borderIMg    = "char-img-border-1";
							chatClass.bgColor  	   = "char-img-bg-1";

						}else{
							msjDirimg = 'right-Image-person',
							msjDirText ='right-snack-person';

							chatClass.msjChar 	   = "dialog-2-char";
							chatClass.msjCharSnack = "dialog-snack-2-char";
							chatClass.imageChar    = "dialog-snack-2-char";
							chatClass.borderIMg    = "char-img-border-2";
							chatClass.bgColor  	   = "char-img-bg-2";
						}

				   	if( $(this).find('audio').find('state').text().trim() == 'true'){
				    	stat = 'block';

				     			 $(this)
				     			 .find('audio')
				     			 .find('files')
				     			 .find('type').each(function($index){
				     			 	var txt = $(this).text().trim();
									if( txt.indexOf(".ogg") != -1){ newPage.fileogg = window.location.origin+'/'+datafolder+'/tts/'+txt;  }
									if( txt.indexOf(".mp3") != -1){ newPage.filemp3 = window.location.origin+'/'+datafolder+'/tts/'+txt;  }

									//if( txt.indexOf(".ogg") != -1){ newPage.fileogg = wwwupload+'/tts/'+txt;  }
									//if( txt.indexOf(".mp3") != -1){ newPage.filemp3 = wwwupload+'/tts/'+txt;  }
								});
					}

				    else{ stat = 'none'; faudio = ''; newPage.fileogg = ''; newPage.filemp3 = '';}

	
					newPage.character = $(this).find('character_active').text().trim();
					newPage.id   	  = $(this).find('id_').text().trim();
					newPage.text      = $(this).find('text_chat').text().trim();
					newPage.image 	  = msjDirimg;
					newPage.snack	  = msjDirText;
					newPage.class	  = chatClass;
					newPage.audio_    = stat;
					
					var output = Mustache.render( $(templates).filter('#pageTpl').html(), newPage );
					$( "#global-chat").append(output);
					
					eventsConversation(newPage.id);

			});
		
				
    	});
	}
}//end load()
/*:::::::::::::::::END CLASS::::::::::::::*/


/*
 * Esta clase trabaja con el xml, lo que hace su metodo
 * overWrite() = reescribe la información del xml (Guardando cambios finales),
 *               El parametro que recibe es la información del xml
 */

/*::::::::::::::::::::CLASS GENERATE AUDIO::::::::::::::::*/
function Audio(){
	var me = this;
	this.audioEvents = function(){
		    //open dialog generate audio
			$("#openDialog").click(function(){
				$("#convertOptions").fadeIn();
				$(this).toggleClass('active_voice');
			});//fin
			//Close dialog generate audio
			$(".close_audio_edit").click(function(){
				$("#convertOptions").fadeOut();
				$("#openDialog").removeClass('active_voice');
			});//fin
			//Generando Audio
			$("#saveAudioConvet").click(function(){
				var params = [];
					if( $("#gender").val() == '' ){ alert("Debes escoger un genero"); }
					else if( $("#lang").val() == '' ){ alert("Debes escoger un Idioma"); }
			 		else if($("textarea#editTextEdition").val() == ''){ alert('No hay ningún texto'); }
					else{
						params.speed  = $("#speed").val();  params.tone   = $("#tono").val();
						params.gender = $("#gender").val(); params.lang   = $("#lang").val();
						me.generateVoice($("textarea#editTextEdition").val(), params);
					}	
			});
			//Quitar audio generado
			$("#quitAudio").click(function(){
				var _id_ = $("#saveMsj").attr("data-current-msj");
				$("#micro"+_id_).css({ "display":"none" });
				//$("#saveMsj")
				//.attr("data-audio-state","false")
				//.attr("data-audio-name", "")
				//.attr('data-folder-file',"");

			});
	}//fin audioEvents
	
	//voice
	this.generateVoice = function(texto, params){
			var variablesURL;
		    var ubicacionTTS = "../../../tts/";
		    var archivo      = 'index.php';
		    var genFolder    = 'files/';
		    var metodo       = 'GET';
		    var txtEsc       = "txt=" + escape(texto); //Preparar texto "escapado" para inserción en la URL  
		    variablesURL     = "idi=" + params.lang + "&ora=" + params.gender + "&vel=" + params.speed + "&ton=" + params.tone + "&" + txtEsc;
			
			$.ajax(ubicacionTTS + archivo,
		    {
		        "type": metodo,   // usualmente post o get
		        async:false,
		        data: variablesURL,        
		        beforeSend: function () {
		        	$("#estu")
		            .removeClass("alert-danger")
					.addClass("alert-info")
		            .css("display","block")
		            .text("Procesando, espere por favor..."); 
		        },
		        success: function(respuesta){
		        	$("div#sound_").empty();
		        	var soundPath = datafolder+"/tts/" + respuesta;
					var mp3Path   = soundPath + ".mp3";
					var oggPath   = soundPath + ".ogg";
					
					$("#saveMsj")
					.attr('data-audio-state',"true")
					.attr('data-audio-name', respuesta)
					.attr('data-folder-file', 'tts');

					//var _audio_ = '<audio controls class="audio"><source src="'+wwwupload+"/tts/" + respuesta + ".ogg"+'" type="audio/mpeg"></audio>';
					var _audio_ = '<audio controls class="audio"><source src="'+window.location.origin+'/'+datafolder+"/tts/" + respuesta + ".ogg"+'" type="audio/mpeg"></audio>';
	        		
	        		$("div#sound_").append(_audio_);
					
					var valText = $("#editTextEdition").val();
			    		
					$("#estu")
		            .removeClass("alert-danger").addClass("alert-success")
		            .css("display","block").text("audio creado Exitosamente");
		            alert("Conversión realizada exitosamente");
					$("#convertOptions").fadeOut();
				 },
		        error: function(respuesta) {
		            $("#estu")
		            .removeClass("alert-success")
					.addClass("alert-danger")
		            .css("display","block")
		            .text("Ha ocurrido un error "+respuesta);
		        },	    	 
		});   
	}//fin generateVoice
}//END Audio()
/*:::::::::::::::::END CLASS::::::::::::::*/

/*:::::::::::::::PUT  EVENTS TO THE ELEMENTS CREATED TO DYNAMICALLY (TEMPLATES)::::::::::::*/
/*
 * bgMainAndChat() 		= Instancia todas las funcines asociadas con la edición del fondo principal
 					 	  de la aplicación.
 * eventsChars()   		= Instancia todas las funciones para la edición de los personajes (color, imagen, fuente...etc)
 * eventsConversation() = Funcoines asociadas a la edicioón de cada mensaje, este método es llamado desde el inicio
 						  de la aplicación y cuando, se ingresa un nuevo mensaje
 * putNewData() 	    = colocar nuevos mensajes
 */

//COLOCA NUEVOS MENSAJES
function putNewData(char){
		$.get('tpl/templates.mustache', function(templatesFile) {
			var templates = templatesFile;
			var newPage   = [];
			var chatClass = [];
			newPage.character = 'char-1';

			var d  = new Date();
		    var id = d.getTime();

			if( char == 'char-1' ) {
					msjDirimg   = '', msjDirText  = '';
					chatClass.msjChar 	   = "dialog-1-char"; chatClass.msjCharSnack = "dialog-snack-1-char";
					chatClass.imageChar    = "dialog-snack-1-char"; chatClass.borderIMg    = "char-img-border-1";
					chatClass.bgColor  	   = "char-img-bg-1";

			}else{
					msjDirimg = 'right-Image-person', msjDirText ='right-snack-person';

					chatClass.msjChar 	   = "dialog-2-char";
					chatClass.msjCharSnack = "dialog-snack-2-char"; chatClass.imageChar    = "dialog-snack-2-char";
					chatClass.borderIMg    = "char-img-border-2"; chatClass.bgColor  	   = "char-img-bg-2";
			}

					newPage.character = char; newPage.id = id;
					newPage.text      = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque at erat vulputate erat varius varius. Vivamus lacinia enim purus.  ";
					newPage.image 	  = msjDirimg; newPage.snack	  = msjDirText; newPage.class	  = chatClass;
			        newPage.audio_    = "none";
					var output        = Mustache.render( $(templates).filter('#pageTpl').html(), newPage );
					$( "#global-chat").append(output);
						  eventsConversation(newPage.id);
						  $(window).scrollTop( $("#global-cnt-chat").height() + 200 );
						 	
		});
}//end  putNewData

//EVENTS TO CONVERSATION
function eventsConversation(_id){
	var parent = $("#"+_id);
	
		//EDIT CONTENT EACH MSJ
			parent.find(".editionBtn").on( "click", function(){
				var text =  $(this).siblings().parent().siblings().find(".cnt-snack-chat").find(".container_text").text();
				$("#saveMsj").attr("data-current-msj", $(this).attr("id"));
				$("textarea#editTextEdition").val(text.trim());
				$("#edit_interfaz_chat").css({"display":"block"});
				$("#editTextBg").css({"display":"none"})
				$(".modal").addClass("showActivePanel");
			});
			
		//DELETE CURRENT MSJ
			parent.find(".dropThis").on("click", function(){
			var id_msj = $(this).attr("id");
				var action = confirm("¿Seguro que desea eliminar este elemento?");	
				if( action == true ){ $("#"+id_msj).remove() }
			});

		//Reproducir audio
			parent.find(".cnt-microfone").click(function(){
				var id_        = $(this).attr("data-id");
				var status     = $(this).attr("data-status-audio");
				var vid        = document.getElementById("audio"+id_); 
				var totalTime  = parseInt(vid.duration);
				var limitGrade = 380, rot = 0, timePass = 0 ,stopInt = false, interval;

				
				if( status == 'pause' ){
					//$(this).attr("data-status-audio", "play");
					vid.play();
				}
				/*else if( status == 'play' ){
					$(this).attr("//COLOCAR LOS MENSAJES QUE ESTAN GUARDADOS EN EL ARRAYdata-status-audio", "pause");
					vid.pause();
				}*/

				interval =  setInterval(function(){
				
				 	if( stopInt == false ){
				 		rot += parseInt(limitGrade / totalTime);
				 		timePass++;

				 		$("#slice"+id_).find('.bar').css("transform", "rotate("+rot+"deg)");
				 		if( timePass == totalTime ) { stopSetInterval()}

				 		else{ if( rot >= 180 ){ $("#audioDiv"+id_).removeClass('p1').addClass('p51'); }	}
				 	}		 		
				 },1000);
				function stopSetInterval(){ clearInterval(interval); $("#audioDiv"+id_).removeClass('p51'); }
			});
			
}// end eventsConversation

function boxEditMsj(){
		//CLOSE MODAL EDITION
			$(".close_modal").on( "click", function(){
				$(".modal").removeClass("showActivePanel");
			});
		//SAVE CONFIG EDIT
		    $(".saveContent").on("click", function(){
		    	
		    	var _id_ 	   = $(this).attr("data-current-msj"),
		    		audioState = $(this).attr('data-audio-state'),
		    		audioName  = $(this).attr('data-audio-name'),
		    		audioFolder= $(this).attr('data-folder-file'),
		    		txtarea    = $("textarea#editTextEdition").val(),
		    		audioType  = $(this).attr('data-folder-file');

		    	$("#snack_text"+_id_)
		    	.text(txtarea)
		    	.attr('data-audio-state', audioState)
				.attr('data-audio-name',  audioName)
				.attr('data-folder-file', audioFolder);
			
			$('#progress_audio .progress-bar').css( 'width', '0%' );

				if( audioState == 'true' )
				{
					$("audio#audio"+_id_).remove();
					$("#micro"+_id_).css({ "display":"block" });
					//$("#Audio"+_id_).find("source.ogg_").attr("src",wwwupload+"/"+audioFolder+"/"+audioName+".ogg");
					//$("#Audio"+_id_).find("source.mp3_").attr("src",wwwupload+"/"+audioFolder+"/"+audioName+".mp3");
					$("#boxAudio"+_id_).css("display","block")
					$("#Audio"+_id_).find("source.ogg_").attr("src", window.location.origin+"/"+datafolder+"/"+audioFolder+"/"+audioName+".ogg");
					$("#Audio"+_id_).find("source.mp3_").attr("src", window.location.origin+"/"+datafolder+"/"+audioFolder+"/"+audioName+".mp3");


					var _audio_ = '<audio controls id="audio'+_id_+'">';
						//_audio_ +=		'<source src="'+wwwupload+"/"+audioFolder+"/" + audioName + ".ogg"+'" type="audio/ogg"></audio>';
					    //_audio_ += 		'<source src="'+wwwupload+"/"+audioFolder+"/" + audioName + ".mp3"+'" type="audio/mpeg"></audio>';
							_audio_ +=		'<source src="'+ window.location.origin+"/"+datafolder+"/"+audioFolder+"/" + audioName + ".ogg"+'" type="audio/ogg">';
				    		_audio_ += 		'<source src="'+ window.location.origin+"/"+datafolder+"/"+audioFolder+"/" + audioName + ".mp3"+'" type="audio/mpeg">';
					 	_audio_ += '</audio>';

					 $(".audioAdd"+_id_).append(_audio_	);
				}
				$(".modal").removeClass("showActivePanel");
					$(this).attr('data-audio-state',"")
					       .attr('data-audio-name',"")
					       .attr('data-folder-file',"");
		});//end
}	

//EVENTS TO BACKGROUND MAIN AND CHAT
function bgMainAndChat(){
				/*propiedades de la imagen*/
					$(".imgWeight").click(function(){
						$(".imgWeight").removeClass('btn-success').attr('data-state', 'false');;
						$(this).addClass('btn-success').attr('data-state', 'true');;
						$("#customDataSubmenu").css("display","none");
					});

				/*posicion del contenedor de texto*/
					$("#posBackground").click(function(){
						var subActive = $(this).attr('data-state-submenu'), show_, state_new;
						if( subActive == "false"){ show_ = 'block'; state_new = "true"; }
						else if( subActive == "true"){ show_ = 'none'; state_new = "false"; }
						$("#posBg").css("display",show_);
						$(this).attr('data-state-submenu', state_new);
					});

					$("#pos_up").change(function(){
						$("#prevFondo").css({
							"top": $(this).val()+"%"
						})
					});

					$("#ancBr").change(function(){
						$("#prevFondo").css({ "border-width": $(this).val()+"px" })
					})

					$("#pos_left").change(function(){
						$("#prevFondo").css({
							"left": $(this).val()+"%"
						})
					});

					$("#boxBgMainWidth").change(function(){
						$("#prevFondo").css({
							"width": $(this).val()+"%"
						})
					});

				$("#customData").click(function(){
					var subActive = $(this).attr('data-state-submenu'), show_, state_new;
					if( subActive == "false"){ show_ = 'block'; state_new = "true"; }
					else if( subActive == "true"){ show_ = 'none'; state_new = "false"; }
					$("#customDataSubmenu ").css("display",show_);
					$(this).attr('data-state-submenu', state_new);
				});

				$(".editTextConversation").click(function(){
					//call self events
					if( $(this).attr('data-action') == 'bgTextEdit'){

						$("#edit_interfaz_chat").css({"display":"none"});
						$("#editTextBg").css({"display":"block"});
					}
					$(".modal").toggleClass("showActivePanel");
				});

	//edicion del texto principal
	    $(".icon-audio").click(function(){
	    	var attrTbg = $(this).attr("data-bgtext");
	    	if( attrTbg ){
				var actAddRemo;
				var attrActive = $(this).attr('data-active');
				var elMain     = $("input#bgMainText");
				var textSize   = parseInt(elMain.css('font-size'));

				if( attrTbg == 'lessSize' || attrTbg == 'moreSize' ){
					((attrTbg == 'moreSize')? textSize++ : textSize--);
						$("#bgMainText").css({"font-size":textSize+"px"});
				}else{
					if( attrActive == "false" )
					{ $(this).addClass('color_active'); $(this).attr('data-active','true'); }

					if( attrActive == "true" )
					{ $(this).removeClass('color_active'); $(this).attr('data-active','false'); }
					
					((attrActive == "false")? actAddRemo = 'añadir' : actAddRemo = 'remover');
					if( actAddRemo == 'añadir' ){  elMain.addClass(attrTbg);}
					if( actAddRemo == 'remover' ){ elMain.removeClass(attrTbg);}				
				}//else
			}//if
	    });
}//end bgMainAndChat

function eventsChars(){
		$(".bg-main-upl").click(function(){
			events_.current_Upload = 'bgMain';
		});
		$(".upload_").click(function(){
			events_.current_Upload = 'bgMain2';
		});
		//show avalaible fonts
		$(".showAvalaibleFonts").on('click', function(){
				$(this).find('.fonts').fadeToggle();
		});	
		//click in choosed font
		$(".fonts > ul > li").click(function(){
			var id_parent = $(this).parent().parent().attr("id")
			
			$(this).parent().parent().parent()
			.find('b').text($(this).attr("data-font"));

		        $("."+$(this).parent().parent().attr('data-snack')).css({
		             	 "font-family":$(this).attr("data-font")
		         });
		}); 
		//resize main img
		$(".imageMainEvent").click(function(){
			var type   = $(this).attr('data-type'),
			action = $(this).attr('data-act'),
			cpxy = 5, 
			img = $(this).parent().parent().parent().find("div.bgImgCnt")
	 		imgWidth = img.css("background-size");
			img_Top  = img.css("background-position").split(" ")[1];
			img_left = img.css("background-position").split(" ")[0];
		 	this.img_width = parseInt(imgWidth); this.img_Top = img_Top; this.img_left  = img_left; 
			//resize
			if( type == 'resize' ){
				imgWidth = parseInt(imgWidth);
	         	  ((action == "width-plus")? imgWidth += cpxy : imgWidth -= cpxy);
				this.img_width = imgWidth+'px';
			}
			//up go and down
			if( type === 'y' ){
				this.img_Top  = parseInt(this.img_Top);
				((action == "up")? this.img_Top  -= cpxy : this.img_Top  += cpxy);
				this.img_Top = this.img_Top+'px';
			}
			if( type === 'x' ){
				this.img_left = parseInt(this.img_left);
				((action == "left")? this.img_left  -= cpxy : this.img_left  += cpxy);
				this.img_left = this.img_left+'px';
			}
			img.css({ "background-size": imgWidth,  "background-position":this.img_left +" "+this.img_Top  });
	    	});//fin redemsionar imagenes
	}//end eventsChars
/*:::::::::::::::::END CLASS::::::::::::::*/
//Inicio de la aplicacion
var app 	 = new load();
var EvAudio  = new Audio();
//cargamos los datos del xml
		   
function addCounter(){
	count++;
	if( count == 4 ){
			    app.STYLES(); events_.colorPicker();

				bgMainAndChat(); eventsChars();
			 	events_.uploadImage(); events_.uploadAudio();	

				$(".addNewChar").click(function(){
						putNewData($(this).attr('data-add'));
				});

				$(".close_modal").on( "click", function(){
					$(".modal").removeClass("showActivePanel");
				});
		}
}


function readParamsToXml() {

	var JSONxmlbody = '';
		var JSONxmlHeader = '<?xml version="1.0" encoding="UTF-8"?><root><userdata>';
		
	 	JSONxmlbody   += '<viewMain>';

	 	//Si no encuentra datos en el DOM del chat, los busca en el XML
		if($("input#bgMainText").val() == ''){
			$(XMLdata).find('viewMain').each(function($index){

			JSONxmlbody    += '<imgBg>';
				JSONxmlbody    += $(this).find('imgBg').text().trim();
			JSONxmlbody    += '</imgBg>';

			JSONxmlbody    += '<contBox>';
					JSONxmlbody    += '<Anchobr>' + $(this).find('contBox').find('Anchobr').text().trim()+ '</Anchobr>';
					JSONxmlbody    += '<width>'   + $(this).find('contBox').find('width').text().trim()+ '</width>';
					JSONxmlbody    += '<bgColor>' + $(this).find('contBox').find('bgColor').text().trim()+ '</bgColor>';
					JSONxmlbody    += '<brColor>' + $(this).find('contBox').find('brColor').text().trim() + '</brColor>';
					
					JSONxmlbody    += '<position>';
						JSONxmlbody    += '<left>'+ $(this).find('contBox').find('position').find('left').text().trim()   +'</left>';
						JSONxmlbody    += '<top>' + $(this).find('contBox').find('position').find('top').text().trim() +'</top>';
					JSONxmlbody    +=  '</position>';
			JSONxmlbody    += '</contBox>';

			var wht  = $(this).find('weight');
		
			JSONxmlbody    += '<weight>';
					JSONxmlbody    += '<custom>';
						JSONxmlbody    += '<height>'+ $(this).find('weight').find('custom').find('height').text().trim() +'</height>';
						JSONxmlbody    += '<state>'+  $(this).find('weight').find('custom').find('state').text().trim() +'</state>';
						JSONxmlbody    += '<width>'+  $(this).find('weight').find('custom').find('width').text().trim() +'</width>';
					JSONxmlbody    += '</custom>';


					JSONxmlbody    += '<fixed>';
						JSONxmlbody    += '<state>'+ $(this).find('weight').find('fixed').find('state').text().trim() +'</state>';
					JSONxmlbody    += '</fixed>';

			JSONxmlbody    += '</weight>';

			JSONxmlbody      += '<text_Main>';
				JSONxmlbody    += '<addClass>';
						$(this).find('text_Main').find('addClass').find('style').each(function(){
				 				JSONxmlbody += '<style>'+ $(this).text().trim() +'</style>';
			 			});
				JSONxmlbody    += '</addClass>';

				JSONxmlbody    += '<text><![CDATA[';
					JSONxmlbody    += $(this).find('text_Main').find('text').text().trim();
				JSONxmlbody    += ']]></text>';

				JSONxmlbody    += '<text_size>';
					JSONxmlbody    += $(this).find('text_Main').find('text_size').text().trim();
				JSONxmlbody    += '</text_size>';

			JSONxmlbody   += '</text_Main>'; 
	});//fin viewMain each
	
	}else{
			var imaBG = $('#appBgMain').find('img').attr('src');
		
		 		if ( getFileName(imaBG) !== "" ) {
					imaBG  = imaBG.substr(imaBG.indexOf(datafolder));
					imaBG  = removeFolder(imaBG, "uploads");
					}
			JSONxmlbody    += '<imgBg>'+ imaBG;
		JSONxmlbody    += '</imgBg>';

			JSONxmlbody    += '<contBox>';
					JSONxmlbody    += '<Anchobr>' + $("input#ancBr").val() + '</Anchobr>';

					JSONxmlbody    += '<width>'   + $("#boxBgMainWidth").val() + '</width>';
					JSONxmlbody    += '<bgColor>' + $("#boxBrMain").css("background-color")+ '</bgColor>';
					JSONxmlbody    += '<brColor>' + $("#boxBgMain").css("background-color")+ '</brColor>';

					JSONxmlbody    += '<position>';
						JSONxmlbody    += '<left>'+ $('input#pos_left').val() +'</left>';
						JSONxmlbody    += '<top>' + $('input#pos_up').val()   +'</top>';
					JSONxmlbody    +=  '</position>';

			JSONxmlbody    += '</contBox>';

			var wht  = $(this).find('weight');
		
			JSONxmlbody    += '<weight>';
					JSONxmlbody    += '<custom>';
						JSONxmlbody    += '<height>'+$('input#alto_img').val()+'</height>';
						JSONxmlbody    += '<state>'+$("button#customData").attr('data-state-submenu')+'</state>';
						JSONxmlbody    += '<width>'+$('input#ancho_img').val()+'</width>';
					JSONxmlbody    += '</custom>';

					JSONxmlbody    += '<fixed>';
						JSONxmlbody    += '<state>'+$("button#customData").attr('fixed')+'</state>';
					JSONxmlbody    += '</fixed>';

			JSONxmlbody    += '</weight>';


	var mainInput  = $("input#bgMainText");
	var Textclass_ = mainInput.attr('class').split(" ");
			JSONxmlbody      += '<text_Main>';
				JSONxmlbody    += '<addClass>';
						for (var i = 0; i <= Textclass_.length -1; i++) {
								JSONxmlbody += '<style>'+Textclass_[i]+'</style>';
							}	
				JSONxmlbody    += '</addClass>';

				JSONxmlbody    += '<text><![CDATA[';
					JSONxmlbody    += mainInput.val();
				JSONxmlbody    += ']]></text>';

				JSONxmlbody    += '<text_size>';
					JSONxmlbody    += mainInput.css( "font-size" );
				JSONxmlbody    += '</text_size>';

			JSONxmlbody   += '</text_Main>'; 
		}

		JSONxmlbody   += '</viewMain>'; 

		JSONxmlbody   += '<chatImage>'; 

		var imaCHAT = $('#mainBgChat').find('img').attr('src');
		 		if ( getFileName(imaCHAT) !== "" ) {
					imaCHAT  = imaCHAT.substr(imaCHAT.indexOf(datafolder));
					imaCHAT  = removeFolder(imaCHAT, "uploads");
				}else {
					imaCHAT = $('#mainBgChat').find('img').attr('src');
				}
			JSONxmlbody   += imaCHAT;
		JSONxmlbody   += '</chatImage>'; 
		
		$(".characters").each(function(){
			var imgOpt = $(this).find("#image-user-container").find("#bgImgCnt");
			var char;

			(($(this).attr("data-char") == 'char-1') ? char = 'character-1' : char = 'character-2');
			JSONxmlbody   += '<characters>'; 
				JSONxmlbody   += '<character_name>';
					JSONxmlbody   += char;
				JSONxmlbody   += '</character_name>';

				JSONxmlbody   += '<image_config>';
					JSONxmlbody   += '<background_color>';
						JSONxmlbody   += $(this).find("#image-user-container").find('#bgChar').css("background-color");
					JSONxmlbody   += '</background_color>';

					JSONxmlbody    += '<border_color>';
						JSONxmlbody   += $(this).find("#image-user-container").css("border-color");
					JSONxmlbody   += '</border_color>';

					JSONxmlbody    += '<image>';
						JSONxmlbody    += '<position>';
							JSONxmlbody    += '<margin_left>';
								JSONxmlbody   += imgOpt.css("background-position").split(" ")[0]
							JSONxmlbody   += '</margin_left>';

							JSONxmlbody    += '<margin_top>';
								JSONxmlbody   += imgOpt.css("background-position").split(" ")[1];
							JSONxmlbody   += '</margin_top>';	
						JSONxmlbody   += '</position>';

						JSONxmlbody   += '<url>';
							var imgChar = imgOpt.css("background-image").split('url("')[1];
								imgChar = imgChar.split('")')[0];

									if ( getFileName(imgChar) !== "" ) {
										imgChar  = imgChar.substr(imgChar.indexOf(datafolder));
										imgChar  = removeFolder(imgChar, "uploads");
												
										JSONxmlbody += imgChar;
										
									}else{
										 JSONxmlbody += imgChar; 	
									}


						JSONxmlbody   += '</url>';

						JSONxmlbody   += '<width>';
							JSONxmlbody   += imgOpt.css("background-size");
						JSONxmlbody   += '</width>';
					JSONxmlbody   += '</image>';
				JSONxmlbody   += '</image_config>';


				JSONxmlbody   += '<snack_config>';
					JSONxmlbody       += '<bg_color>';
						   JSONxmlbody   += $(this).find("#snackUnique").css("background-color");
					JSONxmlbody       += '</bg_color>';

					JSONxmlbody    += '<bg_snack_comilla>';
						JSONxmlbody   += $(this).find("#snack-comilla").css("border-bottom-color");
					JSONxmlbody   += '</bg_snack_comilla>';

					JSONxmlbody       += '<color_text>';
						   JSONxmlbody   += $(this).find("#snackUnique").css("color");
					JSONxmlbody       += '</color_text>';

					JSONxmlbody    += '<text_font>';
						JSONxmlbody   += $(this).find("#snackUnique").css("font-family");
					JSONxmlbody   += '</text_font>';
				JSONxmlbody   += '</snack_config>';
		JSONxmlbody   += '</characters>'; 
		
		});

		//Si no encuentra datos en el DOM del chat, los busca en el XML
		if( $("#global-chat").find(".chat-global-Container").length <= 0){
			$(XMLdata).find('conversation').each(function(){
			var stateAudio = $(this).find('audio').find("state").text().trim(), state;
						
					JSONxmlbody   += '<conversation>'; 
						JSONxmlbody   += '<character_active>';
							JSONxmlbody   += $(this).find('character_active').text().trim();
						JSONxmlbody   += '</character_active>';

						JSONxmlbody   += '<id_>';
							JSONxmlbody   += $(this).find('id_').text().trim();
						JSONxmlbody   += '</id_>';

						JSONxmlbody   += '<text_chat><![CDATA[';
							JSONxmlbody   += $(this).find('text_chat').text().trim();
						JSONxmlbody   += ']]></text_chat>';
				
				JSONxmlbody   += '<audio>';

						if( stateAudio == 'true'){
							JSONxmlbody += '<state> true </state>';
								JSONxmlbody   += '<files>';
									JSONxmlbody   += '<type>'+$(this).find('audio').find('files').find('type').text().trim()+'</type>';
								JSONxmlbody   += '</files>';
						}else if( stateAudio == '' ){
								JSONxmlbody += '<state>  false </state>';
						}
					
				JSONxmlbody   += '</audio>';
			JSONxmlbody   += '</conversation>'; 
		});// fin conversation each
	}else{
		

			$("#global-chat").each(function(){
				$(this).find(".chat-global-Container").each(function(){
					var stateAudio = $(this).find(".cnt-microfone").attr("style").trim(), state;
				
					JSONxmlbody   += '<conversation>'; 
						JSONxmlbody   += '<character_active>';
							JSONxmlbody   += $(this).attr("data-char");
						JSONxmlbody   += '</character_active>';

						JSONxmlbody   += '<id_>';
							JSONxmlbody   += $(this).attr("id");
						JSONxmlbody   += '</id_>';

						JSONxmlbody   += '<text_chat><![CDATA[';
							JSONxmlbody   += $(this).find(".container_text").text();
						JSONxmlbody   += ']]></text_chat>';
				JSONxmlbody   += '<audio>';

						if( stateAudio == 'display: block;'){
							JSONxmlbody += '<state> true </state>';
							JSONxmlbody   += '<files>';
								$("#audio"+$(this).attr("data-id")).find("source").each(function(){
									var sound_file =  $(this).attr('src')
											if ( getFileName(sound_file) !== "" ) {
												sound_file  = sound_file.substr(sound_file.indexOf(datafolder));
												var upl_ = sound_file.indexOf("uploads");
												var tts_ = sound_file.indexOf("tts");
												
												if( upl_ != -1){
													sound_file  = removeFolder(sound_file, "uploads");
												}
												if( tts_ != -1){
													sound_file  = removeFolder(sound_file, "tts");
												}
											JSONxmlbody   += '<type>'+sound_file+'</type>';
											}else{
												JSONxmlbody   += '<type>'+sound_file+'</type>';	
											}
								});//fin each audio
								JSONxmlbody   += '</files>';
						}else if( stateAudio == 'display: none;' ){
								JSONxmlbody += '<state>  false </state>';
						}
					JSONxmlbody   += '</audio>';
				JSONxmlbody   += '</conversation>'; 
				});// fin each chat-global-Container
			});//fin each global-chat
	}//Fin condicional if else

		JSONxmlHeader += JSONxmlbody;
	JSONxmlHeader += '</userdata></root>';
	
		return JSONxmlHeader;
	}

function removeFolder(text, param){
	value =  text.replace(datafolder, '');
	value =  value.replace('/'+param+'/', '');
	return value;
}


function readIncludes(){	
	var returnStr = '';
	var bgMain    = $("#appBgMain").find("img").attr('src');
	var bgChat    = $("#mainBgChat").find("img").attr('src')

	if ( getFileName(bgMain) !== "" ) 
	{
		returnStr += bgMain.substr(bgMain.indexOf(datafolder)) + ",";
	}

	if ( getFileName(bgChat) !== "" ) 
	{
		returnStr += bgChat.substr(bgChat.indexOf(datafolder)) + ",";
	}


$(".characters").each(function(){
	var imgOpt = $(this).find("#image-user-container").find("#bgImgCnt");
	var char;

	(($(this).attr("data-char") == 'char-1') ? char = 'character-1' : char = 'character-2');
			var imgChar = imgOpt.css("background-image").split('url("')[1];
				imgChar = imgChar.split('")')[0];

			if ( getFileName(imgChar) !== "" ) {
				 returnStr += imgChar.substr(imgChar.indexOf(datafolder)) + ",";
			}
	});

$("#global-chat").each(function(){
	$(this).find(".chat-global-Container").each(function(){
		var stateAudio = $(this).find(".cnt-microfone").attr("style").trim(), state;
			if( stateAudio == 'display: block;'){
				$("#audio"+$(this).attr("data-id")).find("source").each(function(){
					var sound = $(this).attr('src');
					if ( getFileName(sound) !== "" ) 
						 	{ returnStr += sound.substr(sound.indexOf(datafolder)) + ","; }
				});
			}				
		}); 
	});		
	return returnStr;	
}

function contentValid(){
		return true;
	}

function invalidMsg(){
		return 'pues no puedes pasar';
	}

function addContent(){
	console.info("add content chat");
	xml = $(window.xmlContent);
	XMLdata = xml;

	var templates = [	
						{ 
							view :'char-1', 
							id : '#characterTpl1', 
							container: 'panel-char-1'
						},
						{ 
							view :'char-2', 
							id : '#characterTpl2', 
							container: 'panel-char-2'
						},
						{ 
							view :'chat',   
							id : '#chatTpl', 
							container: 'panel-chat'
						},
						{ 
							view :'help',   
						    id : '#INITtpl', 
						    container: 'panel-help'
						}
					];
	
	for (var i = 0; i <= templates.length -1; i++) {
			app.THEME(templates[i]);
	} 
}




