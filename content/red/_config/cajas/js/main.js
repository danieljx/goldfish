// Constants

var UPLOAD_URL = '../../jquery-file-upload-handler.php',
    SOUND_CONVERT_URL = 'save-audio.php',
    SLIDE_LIMIT = 10,
    SLIDE_TAB_CHAR_LIMIT = 10;

// Global vars
var data,
    _cover,
    cover,
    _slides,
    slides,
    nav,
    pill,
    container,
    slide,
    iconPickerSettings,
    includes = [];

// Language strings

var lang = {
    upload: {
        error: {
            noFile: 'El archivo no pudo ser cargado, intente de nuevo en unos instantes.',
        }
    }
};

// Init

$(document).ready(function () {
    populateSelect('.font-selector', fonts);
    populateSelect('.font-size-selector', fontSizes);
    
    initCoverEvents();
    initSlideEvents();
    initFileUpload();
    initSoundPlayer();
    initFormElements();
});

// Cover events

function initCoverEvents()
{
    _cover = $('#cover');
    
    var btRemove = _cover.find('.cover-img .remove');
    
    // Image remove file event
    
    btRemove.click(function () {
        var component = _cover.find('.upload'),
            img = component.find('.cover-img > .img');
        
        // Background image reset
        
        img .find('> img')
            .attr('src', '')
            .parent()
            .find('> input')
            .val('');
        
        // Show upload component
        
        component.removeClass('ready');
        
        return false;
    });
}

// Slide events

function initSlideEvents()
{
    _slides = $('#slides');
    nav = _slides.find('.nav');
    pill = nav.find('> li:first');
    container = _slides.find('.tab-content');
    slide = container.find('> .slide:first');
    
    var btAddSlide = _slides.find('.add-slide'),
        editorSettings;
    
    // Tabs sort setup
    
    nav.sortable({
        items: "li:not(.excluded)"
    });
    
    // Refresh tab content on title change
    
    $(document).on('keyup', '#slides .slide-title', function () {
        var input = $(this),
            slide = input.parents('.slide:first'),
            slideId = '#' + slide.attr('id'),
            anchor = nav.find('li:not(.excluded) > a[href="'+slideId+'"]'),
            text = (input.val().length <= SLIDE_TAB_CHAR_LIMIT) ? input.val() : input.val().substr(0, 10) + '...';
        
        text = (text == '') ? anchor.data('default') : text;
        anchor.find('span').html(text);
    });
    
    // Init Summernote on first slide
    
    createSummernote(slide);
    
    // Icon picker settings
    
    iconPickerSettings = {
        iconset: 'fontawesome'
    };
    
    // Init icon picker on first slide
    
    slide.find('.icon-picker > button').iconpicker(iconPickerSettings);
    
    // Create slide event
    
    btAddSlide.click(function () {
        createSlide(true);
        return false;
    });
    
    // Remove slide
    
    $(document).on('click', '#slides .nav li .close', function () {
        var button = $(this),
            anchor = button.parent(),
            pill = anchor.parent(),
            slide = $(anchor.attr('href')),
            prevAnchor = pill.prev(':first').find('a');
        
        if (pill.hasClass('active')) {
            prevAnchor.click();
            $(prevAnchor.attr('href')).addClass('in active');
        }
        
        pill.remove();
        slide.remove();
    });
    
    // Remove slide image
    
    $(document).on('click', '#slides .slide-img .remove', function () {
        var component = $(this).parents('.upload:first'),
            img = component.find('.slide-img > .img');
        
        // Slide image reset
        
        img .find('> img')
            .attr('src', '')
            .parent()
            .find('> input')
            .val('');
        
        // Show upload component
        
        component.removeClass('ready');
        
        return false;
    });
    
    // SoundJS setup
    
    createjs.Sound.alternateExtensions = ["mp3"];
    
    // Remove slide sound
    
    $(document).on('click', '#slides .slide-sound .remove', function () {
        var component = $(this).parents('.upload:first'),
            sound = component.find('.slide-sound > .sound'),
            soundPlayer = sound.find('.sound-player');
        
        // Stop sound (if any)
        
        createjs.Sound.stop();
        
        // Slide sound reset
        
        soundPlayer
            .removeClass('playing')
            .data('id', '')
            .data('ogg', '')
            .data('mp3', '');
        
        // Show upload component
        
        component.removeClass('ready');
        
        return false;
    });
    
    // Slide icon type toggle
    
    $(document).on('click', '#slides .icon-type-toggle', function () {
        var radio = $(this),
            slide = radio.parents('.slide:first'),
            toggleRadios = slide.find('.icon-type-toggle'),
            iconTypes = slide.find('.icon-type'),
            i = toggleRadios.index(radio),
            iconType = $(iconTypes[i]),
            active = iconTypes.filter('.active');
        
        if (!iconType.hasClass('active')) {
            active.removeClass('in active');
            iconType.addClass('in active');
        }
    });
    
    // Slide icon image remove
    
    $(document).on('click', '#slides .slide-icon-img .remove', function () {
        var component = $(this).parents('.upload:first'),
            img = component.find('.slide-icon-img > .img');
        
        // Slide image reset
        
        img .find('> img')
            .attr('src', '')
            .parent()
            .find('> input')
            .val('');
        
        // Show upload component
        
        component.removeClass('ready');
        
        return false;
    });
}

// Create summernote instace
			
function createSummernote(slide)
{
    // Summernote settings
    
    editorSettings = {
        height: 200,
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['layout', ['ul', 'ol', 'paragraph']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['insert', ['link']]
        ],
        disableDragAndDrop: true
    };
    
    slide.find('.summernote').summernote(editorSettings);
}

// Create new slide

function createSlide(setAsActive)
{
    var newSlide = slide.clone(),
        newPill = pill.clone(),
        newId,
        anchor = newPill.find('a'),
        activePill = nav.find('> li.active'),
        activeSlide = container.find('> .active'),
        count = parseInt(_slides.data('count')),
        total = container.find('> .slide').length;

    // Limit check
    
    if (total >= SLIDE_LIMIT) {
        return false;
    }

    // Slide ID setup

    count++;
    _slides.data('count', count);
    newId = 'slide' + count;
    newSlide.attr('id', newId);
    anchor.attr('href', '#'+newId);

    // Reset content

    newSlide.find(':input:not(:radio), select').val('');
    newSlide.find('.summernote').code('').summernote('destroy');
    newSlide.find('.note-editor').remove();
    newSlide.find('.summernote').attr('id','editor-'+count);
    newSlide.find('.upload').removeClass('ready');
    
    newSlide.find('.icon-type-toggle:first')
            .prop('checked', true);
    
    newSlide.find('.icon-type')
            .removeClass('in active')
            .first()
            .addClass('in active');

    // Init file upload

    initFileUpload(newSlide);

    // Init sound player

    initSoundPlayer(newSlide);

    // Init icon picker

    newSlide.find('.icon-picker > button').iconpicker(iconPickerSettings);

    // Set as active
    
    if (setAsActive) {
        activeSlide.removeClass('in active');
        newSlide.addClass('in active');
    }
    
    // Add slide to container

    createSummernote(newSlide);
    container.append(newSlide);

    // Set as active slide

    activePill.removeClass('active');
    newPill.insertBefore(nav.find('li:last-child'));

    anchor.find('span').html(anchor.data('default'));
    anchor.click();
    
    return newSlide;
}

// File upload setup

function initFileUpload(parent)
{
    var elements;
    
    if (parent) {
        elements = parent.find('.upload');
    } else {
        elements = $('.upload');
    }
    
    elements.each(function () {
        var component = $(this),
            alert = component.find('.alert'),
            progressBar = component.find('.progress .progress-bar'),
            btSelect = component.find('.select'),
            btCancel = component.find('.cancel'),
            request, doneCallback;
        
        component.find('.fileupload').fileupload({
            dropZone: $(this),
            url: UPLOAD_URL,
            dataType: 'json',
            beforeSend: function (xhr) {
                request = xhr;
                alert.removeClass('alert-danger')
                     .text('');
                component.removeClass('show-alert')
                         .addClass('uploading');
            },
            done: function (e, response) {
                if (response.result.files.length) {
                    component.addClass('ready');

                    // Callback function is set

                    if (component.data('done')) {
                        doneCallback = window[component.data('done')];

                        // Callback function exists

                        if (doneCallback) {
                            doneCallback(response.result, component);
                        }
                    }
                } else {
                    alert.addClass('alert-danger')
                         .text(lang.upload.error.noFile);
                    component.addClass('show-alert');
                }
            },
            fail: function (request, status, error) {
                // TODO: Add failed request callback
            },
            always: function () {
                component.removeClass('uploading');
                progressBar.css('width', '0%');
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                progressBar.css('width', progress + '%');
            }
        });
        
        // Cancel upload
        
        btCancel.click(function () {
            request.abort();
        });
    });
}

// UPLOAD CALLBACKS

// Background image upload callback

function coverImageComplete(data, component)
{
    var file = data.files[0],
        img = component.find('.cover-img > .img');

    // Background image setup

    img .find('> img')
        .attr('src', file.url)
        .parent()
        .find('> input')
        .val(file.url);
}

// Slide image upload callback

function slideImageComplete(data, component)
{
    var file = data.files[0],
        img = component.find('.slide-img > .img');

    // Slide image setup

    img .find('> img')
        .attr('src', file.url)
        .parent()
        .find('> input')
        .val(file.url);
}

// Slide sound upload callback

function slideSoundComplete(data, component)
{
    if (data.files.length > 0) {
        var file = data.files[0],
            alert = component.find('.alert'),
            soundPlayer = component.find('.sound-player'),
            ext = getExtension(file.name),
            soundId = getBasename(file.name),
            url = file.url.replace(file.name, ''),
            error = false,
            soundData;
        
        $.ajax({
            url: SOUND_CONVERT_URL,
            method: 'post',
            data: {
                path: data.physPath,
                url: url,
                filename: file.name,
                basename: soundId,
                type: file.type,
                ext: ext,
            },
            dataType: 'json'
        }).done(function (response) {
            if (!response.error) {
                var files = response.files;
                
                soundPlayer.data('ogg', files.ogg.url);
                soundPlayer.data('mp3', files.mp3.url);
                
                soundData = {ogg: files.ogg.url, mp3: files.mp3.url};
                createjs.Sound.registerSounds([{src: soundData, id: soundId}], '');
                soundPlayer.data('id', soundId);
            } else {
                alert.addClass('alert-danger')
                     .text(response.error);
                component.addClass('show-alert')
                         .removeClass('ready');
            }
        }).fail(function (request, status, error) {
            // TODO: Show error
        });
    }
}

// Slide icon (image) upload callback

function slideIconComplete(data, component)
{
    var files = data.files;
    
    if (files.length > 0) {
        var file = files[0],
            img = component.find('.slide-icon-img > .img');
        
        // Background image setup
        
        img .find('> img')
            .attr('src', file.url)
            .parent()
            .find('> input')
            .val(file.url);
    }
}

// Sound player setup

function initSoundPlayer(parent)
{
    var elements;
    
    if (parent) {
        elements = parent.find('.sound-player');
    } else {
        elements = $('.sound-player');
    }
    
    elements.each(function () {
        var player = $(this),
            btPlay = player.find('.play'),
            btStop = player.find('.stop');
        
        // Play sound
        
        btPlay.click(function () {
            var soundId = player.data('id'),
                instance = createjs.Sound.play(soundId);
            
            instance.on('complete', onSoundComplete);
            player.addClass('playing');
            
            return false;
        });
        
        // Stop sound
        
        btStop.click(function () {
            createjs.Sound.stop();
            player.removeClass('playing');
            
            return false;
        });
    });
}

//

function onSoundComplete()
{
    container.find('> .active .sound-player')
             .removeClass('playing');
}

// Load form elements

function initFormElements()
{
 
    // Title setup
    cover = {
        title: {
            text: _cover.find('input[name="cover.title"]'),
            font: {
                name: _cover.find('select[name="cover.title.font"]'),
                size: _cover.find('select[name="cover.title.font.size"]'),
                color: _cover.find('input[name="cover.title.font.color"]'),
                },
        },
        bg: {
            color: _cover.find('input[name="cover.bg.color"]'),
        },
        image: {
            url: _cover.find('input[name="cover.image.url"]'),
        },
        tooltip: {
            font: {
                color: _cover.find('input[name="cover.tooltip.font.color"]'),
            },
            bg: {
                color: _cover.find('input[name="cover.tooltip.bg.color"]'),
            },
        },
    };
}

// Slide elements setup

function getSlideElements(slide)
{
    return {
        title: {
            text: slide.find('input[name="slide.title"]'),
            font: {
                name: slide.find('select[name="slide.title.font"]'),
                size: slide.find('select[name="slide.title.font.size"]'),
                color: slide.find('input[name="slide.title.font.color"]'),
            },
        },
        content: {
            text: slide.find('.summernote'),
            font: {
                name: slide.find('select[name="slide.content.font"]'),
                size: slide.find('select[name="slide.content.font.size"]'),
                color: slide.find('input[name="slide.content.font.color"]'),
            },
            bg: {
                color: slide.find('input[name="slide.content.bg.color"]'),
            },
            border: {
                color: slide.find('input[name="slide.content.border.color"]'),
            },
        },
        close: {
            color: slide.find('input[name="slide.close.color"]'),
        },
        media: {
            image: {
                url: slide.find('input[name="slide.img.url"]'),
            },
            sound: slide.find('.sound-player'),
        },
        icon: {
            bg: {
                color: slide.find('input[name="slide.icon.bg.color"]'),
            },
            type: slide.find('input[name="slide.icon.type"]'),
            code: slide.find('.icon-picker > button > i'),
            color: slide.find('input[name="slide.icon.color"]'),
            url: slide.find('input[name="slide.icon.url"]'),
        }
    };
}

// XML Params Load

function addContent()
{
    // XML Data
    
    data = $(window.xmlContent);
    
    //  XML Nodes
    
    var coverNode = data.find('userdata > cover'),
        coverTitleNode = coverNode.find('> title'),
        tooltipNode = coverNode.find('> tooltip'),
        coverImgNode = coverNode.find('> image'),
        slideNodes = data.find('userdata > slides > slide');
    
    // Set Cover Values
    
    if (coverTitleNode.text() != '') {
        cover.title.text.val(coverTitleNode.text());
    }
    
    if (coverTitleNode.attr('font') != '') {
        cover.title.font.name.val(coverTitleNode.attr('font'));
    }
    
    if (coverTitleNode.attr('fontSize') != '') {
        cover.title.font.size.val(coverTitleNode.attr('fontSize'));
    }
    
    if (coverTitleNode.attr('fontColor') != '') {
        cover.title.font.color.val(coverTitleNode.attr('fontColor'));
    }

    if (coverNode.attr('bgColor') != '') {
        cover.bg.color.val(coverNode.attr('bgColor'));
    }
    
    if (coverImgNode.attr('url') != '') {
        var component = _cover.find('.upload'),
            img = component.find('.cover-img > .img');
        
        // Background image setup
        
        img .find('> img')
            .attr('src', changeurl(coverImgNode.attr('url'), true))
            .parent()
            .find('> input')
            .val(changeurl(coverImgNode.attr('url'), true));
        
        // Hide upload component
        
        component.addClass('ready');
    }
    
    if (tooltipNode.attr('fontColor') != '') {
        cover.tooltip.font.color.val(tooltipNode.attr('fontColor'));
    }
    
    if (tooltipNode.attr('bgColor') != '') {
        cover.tooltip.bg.color.val(tooltipNode.attr('bgColor'));
    }
    
    // Set slides data
    
    slideNodes.each(function (i) {
        var slideNode = $(this),
            titleNode = slideNode.find('> title'),
            contentNode = slideNode.find('> content'),
            closeNode = slideNode.find('> close'),
            imgNode = slideNode.find('> media > image'),
            soundNode = slideNode.find('> media > sound'),
            iconNode = slideNode.find('> icon'),
            _slide, slideData, slideId, anchor, text;
        
        // First slide check
        
        if (i == 0) {
            _slide = slide;
        } else {
            _slide = createSlide();
        }
        
        // Tab anchor setup
        
        slideId = '#' + _slide.attr('id');
        anchor = nav.find('li:not(.excluded) > a[href="'+slideId+'"]');
        text = (titleNode.text().length <= SLIDE_TAB_CHAR_LIMIT) ? titleNode.text() : titleNode.text().substr(0, 10) + '...';
        text = (text == '') ? anchor.data('default') : text;
        anchor.find('span').html(text);
        
        // Slide elements setup
        
        slideData = getSlideElements(_slide);
        
        // Set title data
        
        slideData.title.text.val(titleNode.text());
        slideData.title.font.name.val(titleNode.attr('font'));
        slideData.title.font.size.val(titleNode.attr('fontSize'));
        slideData.title.font.color.val(titleNode.attr('fontColor'));
        
        // Set content data
        
        slideData.content.text.code(contentNode.text());
        slideData.content.font.name.val(contentNode.attr('font'));
        slideData.content.font.size.val(contentNode.attr('fontSize'));
        slideData.content.font.color.val(contentNode.attr('fontColor'));
        slideData.content.bg.color.val(contentNode.attr('bgColor'));
        slideData.content.border.color.val(contentNode.attr('borderColor'));
        
        // Set close button data
        
        slideData.close.color.val(closeNode.attr('color'));
        
        // Set image data
        
        if (imgNode.attr('url') != '') {
            var component = slideData.media.image.url.parents('.upload:first'),
                img = component.find('.slide-img > .img');

            // Slide image setup

            img .find('> img')
                .attr('src', changeurl(imgNode.attr('url'), true))
                .parent()
                .find('> input')
                .val(changeurl(imgNode.attr('url'), true));

            // Hide upload component

            component.addClass('ready');
        }
        
        // Set sound data
        
        if (soundNode.attr('ogg') != '' && soundNode.attr('mp3') != '') {
            var component = slideData.media.sound.parents('.upload:first'),
                sound = component.find('.slide-sound > .sound'),
                soundPlayer = sound.find('.sound-player'),
                filename = getFilenameFromUrl(soundNode.attr('ogg')),
                soundId = soundNode.attr('id'),
                soundData;
            
            // Slide sound setup
            
            soundPlayer
                .data('id', soundId)
                .data('ogg', changeurl(soundNode.attr('ogg'), true))
                .data('mp3', changeurl(soundNode.attr('mp3'), true));
            
            soundData = {ogg: changeurl(soundNode.attr('ogg'), true), mp3: changeurl(soundNode.attr('mp3'), true)};
            createjs.Sound.registerSounds([{src: soundData, id: soundId}], '');
            soundPlayer.data('id', soundId);
            
            // Hide upload component

            component.addClass('ready');
        }
        
        // Set icon data
        
        slideData.icon.bg.color.val(iconNode.attr('bgColor'));
        slideData.icon.type.filter('[value="'+iconNode.attr('type')+'"]').click();
        slideData.icon.code.addClass(iconNode.attr('code'));
        slideData.icon.color.val(iconNode.attr('color'));
        
        if (iconNode.attr('url') != '') {
            var component = slideData.icon.url.parents('.upload:first'),
                img = component.find('.slide-icon-img > .img');

            // Slide icon setup

            img .find('> img')
                .attr('src', changeurl(iconNode.attr('url'), true))
                .parent()
                .find('> input')
                .val(changeurl(iconNode.attr('url'), true));

            // Hide upload component

            component.addClass('ready');
        }
    });
    
    // Set first slide as active
    
    pill.find('a').click();
    slide.addClass('in active');
}

// XML Params Save

function readParamsToXml()
{

    data.find('userdata > cover')
        .attr('bgColor', cover.bg.color.val())
        .find('> title')
        .html(cover.title.text.val())
        .attr('font',     cover.title.font.name.val())
        .attr('fontSize', cover.title.font.size.val())
        .attr('fontColor', cover.title.font.color.val())
        .parent()
        .find('> image')
        .attr('url', changeurl(cover.image.url.val(), false))
        .parent()
        .find('> tooltip')
        .attr('bgColor', cover.tooltip.bg.color.val())
        .attr('fontColor', cover.tooltip.font.color.val());
        
    
    if (cover.image.url.val() != '') {
        includes.push(getFileLocateFromURL(cover.image.url.val()))
    }
    
    // Get slide node
    
    var slideNode = data.find('userdata > slides > slide:first');
    
    // Remove useless slides
    
    data.find('userdata > slides > slide:gt(0)').remove();
    
    // Set slides data
    
    _slides.find('.nav > li:not(.excluded) > a').each(function (i) {
        var anchor = $(this),
            slide = $(anchor.attr('href')),
            node,
            slideData = getSlideElements(slide);
       
        // Set current node
        
        if (i == 0) {
            node = slideNode;
        } else {
            node = slideNode.clone();
            data.find('userdata > slides').append(node);
        }

 
        // Set title data
        
        node.find('> title')
            .html(slideData.title.text.val())
            .attr('font', slideData.title.font.name.val())
            .attr('fontSize', slideData.title.font.size.val())
            .attr('fontColor', slideData.title.font.color.val());
        
        // Set content data
        
        node.find('> content')
            .html('<![CDATA['+slideData.content.text.code()+']]>')
            .attr('font', slideData.content.font.name.val())
            .attr('fontSize', slideData.content.font.size.val())
            .attr('fontColor', slideData.content.font.color.val())
            .attr('bgColor', slideData.content.bg.color.val())
            .attr('borderColor', slideData.content.border.color.val());
        
        // Set close button data
        
        node.find('> close')
            .attr('color', slideData.close.color.val());
        
        // Set media data
        //check
     
        node.find('> media > image')
            .attr('url', changeurl(slideData.media.image.url.val(), false))
            .parent()
            .find('> sound')
            .attr('id', slideData.media.sound.data('id'))
            .attr('ogg', changeurl(slideData.media.sound.data('ogg'), false))
            .attr('mp3', changeurl(slideData.media.sound.data('mp3'), false))
        
        
        if (slideData.media.image.url.val() != '') {
            includes.push(getFileLocateFromURL(slideData.media.image.url.val()));
        }
        
        if (slideData.media.sound.data('ogg') != '' && slideData.media.sound.data('mp3') != '') {
            includes.push(getFileLocateFromURL(slideData.media.sound.data('ogg')));
            includes.push(getFileLocateFromURL(slideData.media.sound.data('mp3')));
        }
        
        // Set icon data
        
        node.find('> icon')
            .attr('bgColor', slideData.icon.bg.color.val())
            .attr('type', slideData.icon.type.filter(':checked').val())
            .attr('code', slideData.icon.code.attr('class'))
            .attr('color', slideData.icon.color.val())
            .attr('url', changeurl(slideData.icon.url.val(), false));
        
        if (slideData.icon.url.val() != '') {
            includes.push(getFileLocateFromURL(slideData.icon.url.val()));
        }
    });
    
    // Remove metadata
    
    data.find('metadata').remove();
    data.find('root').find('userdata').find('autoplay').remove();

    data.find('root').find('userdata').append('<autoplay>'+$('#ckeckAll').is(':checked')+'</autoplay>');
    // Return XML Data
        return '<?xml version="1.0" encoding="UTF-8"?><root>' + data.find('root').html() + '</root>';
}

//

function changeurl(url, reverse)
{
    if (url != "") {
        if (!reverse) {
            url = url.split("/");
            url = url.pop();
            url = "files/" + url;
        } else {
            url = window.srcPath + "/" + url;
        }
        
        return url;
    }
    
    return '';
}

//

function readIncludes()
{
    return includes.join(',');
}