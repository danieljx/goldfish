var gulp     = require("gulp")
var gutil    = require("gulp-util");
var version  = require('gulp-version-number');
var cleancss = require('gulp-clean-css');
var concat   = require('gulp-concat');
var livereload = require('gulp-livereload');

gulp.task("prepare-css", function(){
	var  files = ['css/views/*.css','css/basic.css'];
	return gulp.src(files)
        .pipe(concat('styles.css'))
        .pipe(gulp.dest('dist/css'));
});

gulp.task("prepare-js", function(){
	var files = ['js/backbone/models/*.js','js/backbone/views/*.js','js/collage.js'];

	return gulp.src(files)
		.pipe(concat('cuthbert.js'))
		.pipe(gulp.dest('dist/js'));
});

gulp.task('watch', function(){
	livereload.listen();
	gulp.watch("css/views/*.css",['prepare-css']);
	gulp.watch('css/basic.css',['prepare-css']);
	gulp.watch('js/backbone/models/*.js', ["prepare-js"]);
	gulp.watch('js/backbone/views/*.js', ["prepare-js"]);
	gulp.watch('js/collage.js', ["prepare-js"]);
});