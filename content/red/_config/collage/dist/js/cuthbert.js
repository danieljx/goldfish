var Collage = Collage || {};

Collage.alert = Backbone.Model.extend({
	defaults:{
		classA: "alert-danger",
		icon: "glyphicon-remove",
		text: "Muestra alertas por defecto"
	},
	initialize: function(){
		console.info("%c**** Modelo alerta **** ","font-size:15px;font-weight:bold;color:#804000;");

		this.getAlert();
	},
	getAlert: function(){
		var alert = new Collage.alertView( {model: this} );
	}
});
var Collage = Collage || {};
/** The application main model
* @author Duvan Valencia
* @augments Backbone.Model
* @constructor
* @name app
*/
Collage.app = Backbone.Model.extend( /** @lends app */ {
	/** Defaults:
		* Sets a default value for: 
		* Selected  = (class) select element element-dinamyc.
		* Inactive  = (class) hidde a element.
		* Window 	= (id) where put the app's resources.
		* Console   = (id) console errors.
		* DinamycEl = (used as class) container for 'images', 'audios', 'icons' characterrs.
		* Folders   = (link) used for save the new characters
		* idCopied  = (id) used for add an event target
    */
	defaults:{
		"selected": ".selected-obj",
		"inactive": "element-inactive",
		"window":   '#window-edition',
		"console":	'#console',
		"dinamycEl": 'element-dinamyc',
		"folders": "files/recursos/Personajes/",
		"idCopied": "undefined",
		"alertNotSelect": "¡No hay <b>elementos</b> seleccionados!",
		elem: []
	},
	/** get a list of element-dinamyc on #window for capes plugin 
    */
	getElements: function(){
		console.warn("rocks!");
		var this_ = this;
		$( this.get("window") )
			.find("."+this.get("dinamycEl") )
				.each(function(){
					var isImg   = $(this).find("img.dynamic-image").length,
						isAudio = $(this).find("div.playSound").length,
						isText  = $(this).find("div.text-plugin").length,
						isIcon  = $(this).find("object.icon-color").length,
						whatAmI;

					if ( isImg   >= 1){ whatAmI = "Imagen"; }
					if ( isAudio >= 1){ whatAmI = "Audio"; }
					if ( isText  >= 1){ whatAmI = "Texto"; }
					if ( isIcon  >= 1){ whatAmI = "Icono"; }

			this_.get("elem").push({
				 id:  $(this).attr("id"),
				 visible: $(this).attr("data-visibility-state"),
				 type: whatAmI
			  })
		
		});
	},
	toJSON(){
		console.warn( this.get("elem") );
		return {
			elements: this.get("elem"),
			plugins:[
				{ "name":"Multimedia", "view":"Multimedia", "icon":"film icon" },
				{ "name":"Iconos", "view":"icons", "icon":"star icon" },
				{ "name":"Texto", "view":"text", "icon":"font icon" },
				{ "name":"Estilos", "view":"css", "icon":"magic icon" },
				{ "name":"Elementos", "view":"elements", "icon":"object ungroup icon" },
			],
			upperSideBar:[
				{ "name":"Copiar", "icon":"copy" },
				{ "name":"Pegar",  "icon":"paste" },
				{ "name":"Cortar", "icon":"scissors" },
				{ "name":"Eliminar", "icon":"trash" },
				{ "name":"Organizar", "icon":"indent-left", "subMenu":"true" },
				{ "name":"Color de fondo", "icon":"text-background", "id_":"bg-color", "class":"color-option" },
				{ "name":"Color de letra", "icon":"text-color", "id_":"txt-color", "class":"color-option" },
			],
			headers:[
				{ "fontSize":"h1" },{ "fontSize":"h2" },{ "fontSize":"h3" },
				{ "fontSize":"h4" },{ "fontSize":"h5" },{ "fontSize":"h6" }
			],
			css_: {
				padding:[
					{ "titulo":"Arriba","id":"paddingTop","atributo":"padding-top" },
					{ "titulo":"Abajo","id":"paddingBottom","atributo":"padding-bottom"},
					{ "titulo":"Derecha","id":"paddingRight","atributo":"padding-right"},
					{ "titulo":"Izquierda","id":"paddingLeft","atributo":"padding-left"}
				],
				radius:[
					{ "titulo":"Arriba Iz","id":"RadiusTopLeft","atributo":"border-top-left-radius" },
					{ "titulo":"Arriba Der","id":"RadiusTopRight","atributo":"border-top-right-radius" },
					{ "titulo":"Abajo Iz","id":"RadiusBottompLeft","atributo":"border-bottom-left-radius" },
					{ "titulo":"Abajo Der","id":"RadiusBottompRight","atributo":"border-bottom-right-radius" },	
				]
			}

			// pages: pages
		};
	},
	/** On init, instance the drag and drop and the resizable widget.
    */
	initialize: function(){
		console.info("%c***** Modelo principal app ***** ","font-size:15px;font-weight:bold;color:#804000;");
		this.componentsWidget();
	},
	/** Creates an id for each new element dynamic.
		* @param {string} tag - id alias.
		* @return {string} id with alias concatenated.
    */
	createId: function(tag){
		var date  = new Date(),
			id_ = date.getTime();
		return id_+'-'+tag;
	},
	/** Remove class selected-obj for each element-dinamyc and hidde del context menu
    */
	deselected: function(){
		$("#main-contextmenu").addClass("element-inactive");
		$("#window-edition").attr("data-selected","false");
		$("#window-edition").removeClass( "selected-obj" );
		if ( $(this.get("selected")).length == 1 ){
				$(this.get("selected")).each(function(){
					$(this).removeClass( "selected-obj" ).removeClass("active");
				})
		}
	},
	/** Puts de colors for svg object tags html (icons)
		* This is only used in addContent function
		* @param {string} icolors - id tag object.
		* @return {string} none.
    */
	afterLoad: function(icolors){
		var total = icolors.length;
			if ( total >= 1){
				for (var i = 0; i <= icolors.length - 1; i++) {
					var a = document.getElementById( icolors[i].id );
					var svgDoc  = a.contentDocument;
					var svgItem = svgDoc.getElementById("icon");
						svgItem.setAttribute("fill", icolors[i].color );
				}
			}
	},
	/** NEW INSTANCE 
		* Creates a new view alert
		* @param {string} type - sets the alert types.
		* @param {string} text - info for the user.
		* @return {object} none.
    */
	showAlert: function(type, text){
		var classAlert, spanClass;
			switch (type) {
				case "error":
					classAlert = "alert-danger";
					spanClass  = "glyphicon-remove";
				break;
				case "ok":
					classAlert = "alert-success";
					spanClass  = "glyphicon-ok";
				break;
				case "warning":
					classAlert = " alert-warning";
					spanClass  = "glyphicon-warning-sign";
				break;
			}
		var alert = new Collage.alert({ classA:classAlert, icon:spanClass, text: text });
	},
	/** NEW TEMPLATE
		* Creates a new element tag object
		* @param {string} src - link source.
		* @return {object} object tag.
    */
	templateSvg: function(src){
		var id_ = this.createId('svg');
		var obj_ = $("<object/>");
			obj_.attr("class", "icon-color set-color-svg");
			obj_.attr("id", id_);
			obj_.attr("data",src);
			obj_.attr("type","image/svg+xml");
			obj_.attr("height","50");
			obj_.attr("width","50");
			return obj_;
	},
	/** NEW TEMPLATE
		* Creates a new element text  ( used for plugin text and addContent ).
		* @param {string} attrTxt - type of header h1, h2 ... h6 .
		* @param {string} text - info for the user.
		* @return {object} object html template.
    */
	tmp_text: function(attrTxt, text){
		var Id_ = this.createId("text");
		var div_ = $("<div/>");
			div_.attr("id", Id_+"-plugin")
			div_.addClass("text-plugin");
			div_.attr("data-font-size", attrTxt );
								
		var set_title = $("<section/>");
			set_title.addClass("editable-text-header");
			set_title.text(text);
			set_title.attr("id", Id_);

		var section_ = $("<section/>");
			section_.addClass("editable-text-input element-inactive");//
			section_.attr("id", Id_+"-editable");

		var input_ = $("<input/>");
			input_.attr("type","text");
			input_.addClass("withoutBorder editable");
			input_.attr("data-id-parent", Id_);
			input_.attr("value", attrTxt );

			section_.append( input_ );

			div_.append( set_title );
			div_.append( section_ );
			return div_;
	},
	/** NEW TEMPLATE
		* Creates a new element audio ( used for plugin multimedia and addContent ).
		* @param {file} attrTxt - link source.
		* @return {object} object html template.
    */
	tmp_audio: function(file){
		var id_ = this.createId('audio');
		
		var soundBase = $("<div/>");
			soundBase.attr("data-child-audio", id_);
			soundBase.attr("data-state", "false");
			soundBase.attr("class", "playSound");

		var secPlay = $("<section/>");
			secPlay.attr("class","audioPlayIcon");

		var Icon = $("<div/>");

		var obj_ = $("<object/>");
			obj_.attr("class", "audio-color set-color-svg");
			obj_.attr("id", id_+"-svg");
			obj_.attr("data","files/play.svg");
			obj_.attr("type","image/svg+xml");
			obj_.attr("height","55");
			obj_.attr("width","55");

			Icon.append(obj_);

		var secAudioTag = $("<section/>");
			secAudioTag.attr("class", "audioTagElement");

		var AudioTag = $("<audio/>");
			AudioTag.attr("id", id_);

		var sourceTag = $("<source/>");
			sourceTag.attr("src",file);
			sourceTag.attr("type","audio/mpeg");

			AudioTag.append(sourceTag);
			secAudioTag.append(AudioTag);
			secPlay.append(Icon);
			soundBase.append(secAudioTag);
			soundBase.append(secPlay);

		return soundBase;
	},
	/** NEW TEMPLATE
		* Creates a new element image ( used for plugin multimedia and addContent ).
		* @param {file} attrTxt - link source.
		* @return {object} object html template.
    */
	tmp_Image: function(file){
		var id_ = this.createId('img');
			var img_ = $("<img/>");
				img_.attr("class",'dynamic-image');
				img_.addClass("gf-object-image");
				img_.attr("src",file);
				img_.attr("id",id_);
		return img_;
	},
	/** NEW TEMPLATE
		* Creates a new dinamy element container for images, audios, icons, text.
		* @param {object} html template.
		* @return {object} object html template.
    */
	templateDinamyc: function( tmp ){
		var zIndex_ = parseInt(this.getZindex());
			zIndex_ += 1;
		this.setZindex( zIndex_ );

		var id_ = this.createId('dynamic');
		var section_ = $('<section/>');
		    section_.addClass( this.get("dinamycEl")+" gf-dinamyc-element-draggable" );
		    section_.attr("id",id_);
		    section_.attr("data-visibility-state","true");
		    section_.attr("data-state-event","false");
		    section_.attr("data-ref-id-event","")
		    section_.attr("data-collection-to-disable","");
		    section_.css("z-index",zIndex_);
		var div_circle = $("<div/>");
			div_circle.attr("class","gf-circle-edit-dinamyc-element");
		var span_ = $("<span/>");
			span_.attr("class","glyphicon glyphicon-pushpin");
		var div_body = $("<div/>");
			div_body.attr("class","gf-dinamyc-body");
			div_body.append( tmp );
			div_circle.append( span_ );
			section_.append( div_body );
		return section_;
	},
	/** Return the global z-index.
		* @param {object} none.
		* @return {object} string.
    */
	getZindex: function(){
		return  $("#window-edition").attr("data-global-zindex");
	},
	/** Set the new value for the global z-index.
		* @param {object} new Number.
		* @return {object} none.
    */
	setZindex: function( nmb ){
		$("#window-edition").attr("data-global-zindex", nmb );	
	},
	/** drag and drop, resizable
 	*/
	componentsWidget: function(){
		var this_ = this;
		$(".element-dinamyc, .draggable-initial")
			.draggable({
				stop:
				function(event, ui){
					var dragElem = $(ui.helper[0]);
				if ( dragElem.hasClass('draggable-initial') == true){
						if ( dragElem.hasClass("drag-icons") == true ) {
							var src_ = dragElem.find("img").attr("src");
							var obj  = this_.templateSvg(src_);
							var dinm = this_.templateDinamyc(obj);
								dinm.css({ "left":"50%",top:"25%"});
							$("#window-edition").append( dinm )
								dragElem.css({ "left":"auto",top:"auto"});	
						}
						if ( dragElem.hasClass("drag-resource") == true ) {
							var src_ = dragElem.find("img").attr("src");
							var img_ = this_.tmp_Image(src_);
							var dinm = this_.templateDinamyc(img_);
								dinm.css({ "left":"50%",top:"25%"});
							$("#window-edition").append( dinm )
							dragElem.css({ "left":"auto",top:"auto"});	
						}
						if ( dragElem.hasClass("panel-text") == true ) {
							var attrTxt = dragElem.attr("data-font-size");
							var div_ = this_.tmp_text(attrTxt, "Lorem ipsum");
							var dinm =  this_.templateDinamyc(div_) ;
								dinm.css({ "left":"50%",top:"25%"});
							$("#window-edition").append(dinm)
							dragElem.css({ "left":"auto",top:"auto"});	
						}
					}else{
						console.warn( "arrastrar normal" );
					}
				this_.componentsWidget();	
				}
			});
		//resiable
		$("#window-edition .element-dinamyc").resizable({
			resize:function(event,ui){
					var idResizer = $("#"+$(ui.element[0]).attr("id"));
						idResizer.addClass("activeBorder");
					//Imagen
					if ( idResizer.find("img").length == 1 ){
						 idResizer.find("img").css({
						 	width: idResizer.width()+"px",
						 	height:idResizer.height()+"px"
						});	
					}
					//Audio
					if ( idResizer.find("div.playSound").length == 1){
							idResizer.find("div.playSound").css({
							 	width: idResizer.width()+"px",
							 	height:idResizer.height()+"px"
							})
							idResizer.find("div.playSound")
								.find("div.audioPlayIcon")
								.find("object.set-color-svg")
								.css({
									"height": idResizer.height()+"px",
									"width": idResizer.width()+"px",
								})
					}//end if
					if ( idResizer.find("object.set-color-svg").length == 1){
						idResizer.find("object.set-color-svg")
							.css({
							 	width: idResizer.width()+"px",
							 	height:idResizer.height()+"px"
							})
					}
			},
			stop: function(event,ui){
				var idResizer = $("#"+$(ui.element[0]).attr("id"));
					idResizer.removeClass("activeBorder");	
			}
		});
	}
});




var Collage = Collage || {};
/** The application main model
* @author Duvan Valencia
* @augments Backbone.View
* @constructor
* @name alertView
*/
Collage.alertView = Backbone.View.extend(  /** @lends alertView */  {
	el: $("#console-body"),
	events: {
		'click .remove_this':"alertClose"
	},
	template: _.template( $("#bottomAlert").html() ),
	initialize: function(){ this.render(); },
	render: function(){
		this.$el.append( this.template( this.model.toJSON() ) );
	},
	/** Remove current (or this) alert from console
    */
	alertClose: function(e){
		var thisEl = $(e.currentTarget);
		thisEl.parent().remove();
	}
})
var Collage = Collage || {};

Collage.character = Backbone.View.extend({
	"el":"#modal-box",
	selectedClass:'svg-selected',
	mainsvgId:"customer-character",
	parentId:'',
	template: _.template( $("#character").html() ),
	initialize: function(){
		this.render();
		this.putFeatures();
		//transform: scale(0.5,0.5);
	},
	events:{
		"click .menu-filter div.item": "filter",
		"click #features div.custom-sets": "insertNewFeature",
		"click #delete-customer-svg": "deleteSvg",
		"click #listingCss":"listarColores",
		"click .editAnimOptions":"animateOptions",
		"click .close-tooltip-animation":"close",
		"click .save-config-animation":"save",
		"click .checkBoxAnimation":"checkExits",
		"click .start-prev-naimation":"startAnimation",
		"click #save-complete-svg":"saveCompleteSvg",
		"change .stiles-control-input input":"transformCss"
	},
	transformCss:function(e){
		console.info("%cTransformamos el svg","font-weight:bold;color:#000;");
		var this_ = $($(e.currentTarget));
		var attr_transform = this_.attr("data-transformation");
		var selecionado = $("#"+this.mainsvgId).find("."+this.selectedClass);

		if ( selecionado.length >= 1 ){
			if( attr_transform ){
				switch (attr_transform) {
					case "scale":
					var x_ = $(".stiles-control-scale-x").val(),
						y_ = $(".stiles-control-scale-y").val();
							selecionado.attr("style","transform: scale("+x_+","+y_+")");
						break;
					case "rotate":
							selecionado.attr("style","transform: rotate("+this_.val()+"deg)");
						break;
					default:
						this.model.showAlert("error","No existe información para esta transformación");
					break;
				}
			}
			else{
				this.model.showAlert("error","Se ha producido un error en la transformación");
				}
			}else{
				this.model.showAlert("warning","No hay elementos seleccionados");
			}
	},
	tmp_Image: function(file){
		var id_ = this.model.createId('character');
			var img_ = $("<img/>");
				img_.attr("class",'dynamic-image');
				img_.addClass("gf-object-image");
				img_.attr("src",file);
				img_.attr("id",id_);
		return img_;
	},
	saveCompleteSvg: function(e){

		//width: 389px; height: 392px;
		var svg_ = $("#preview-custom-character").html();
		var result = '';
		var codeData = {
				"folder": this.model.get("folders"),
				"name"   :this.model.createId("character"),
				"type"   : "flat",
				"gender" : "female",
				"emotion": "uno,dos",
				"animation": "talk,toBlink", 
				"codeImg": svg_
			};
			
		$.ajax({
			url: "php/create-svg.php",
			type:"post",	
			async:false,
			data: codeData,
			 	success: function(re){
			        	re = result;	
			    }
			});
			var tmpImage = this.tmp_Image( "files/recursos/Personajes/"+codeData.name+".svg" );
			var tmpDynam = this.model.templateDinamyc( tmpImage );

		 $(this.model.get("window")).append( tmpDynam );
				this.model.componentsWidget();
	},
	startAnimation: function(e){
		var this_  = $(e.currentTarget),
			id_    = this_.attr("data-svg-exits"),
			Exists = $("svg#customer-character").find("#"+id_).length,
			dataId = $("#"+this_.attr("data-parent"));

		if( Exists <= 0 ){
			this.model.showAlert("error","No se puede animar, el elemento <b>no existe</b>");
		}else{
			console.warn("repeat: "+dataId.attr("data-reproduction"));
			console.warn("velocidad: "+dataId.attr("data-velocity"));
			console.warn("evento: "+dataId.attr("data-event"));
			console.info(id_);
			//$("#"+id_).trigger("click");
		}
	},
	removeSelect: function(e){
			console.info("Removiendo selección");
			$("#customer-character").find("g.svg-selected").removeClass("svg-selected")	
	},
	checkExits: function(e){
		var this_  = $(e.currentTarget),
			alertText = "",
			id_    = this_.attr("data-svg-exits"),
			Exists = $("svg#customer-character").find("#"+id_).length;
		
			switch (id_) {
				case "eyes":
					alertText = "El personaje no tiene <b>ojos</b>. Cuando los tenga, la animación se ejecutará ";
				break;

				case "mouth":
					alertText = "El personaje no tiene <b>Boca</b>. Cuando tenga, la animación se ejecutará ";
					break;
			}

		if ( Exists <= 0){
			this.model.showAlert("warning",alertText);
		}
	},
	save: function(){
		var id_parent = this.parentId;
		this.close();
		var toggleRepeat   = $("#repeat-config"),
			toggleEvent    = $("#event-config"),
			inputVelocity  = $("#velocity-config");

		$("#"+id_parent)
			.attr("data-reproduction", toggleRepeat.prop("checked"))
			.attr("data-velocity", inputVelocity.val())
			.attr("data-event", toggleEvent.prop("checked"));
	},
	close: function(){
		this.parentId = "";
		$("#basic-animation-tooltip").addClass(this.model.get("inactive"));
	},
	animateOptions: function(e){
		var id_parent = $(e.currentTarget).attr("data-parent");
		this.parentId = id_parent;
		var toggleRepeat   = $("#repeat-config"),
			toggleEvent    = $("#event-config"),
			inputVelocity  = $("#velocity-config");

		var stateRepeat	 = $("#"+id_parent).attr("data-reproduction"),
			stateEvent	 = $("#"+id_parent).attr("data-event"),
			valVelocity  = $("#"+id_parent).attr("data-velocity");

		$("#basic-animation-tooltip").removeClass(this.model.get("inactive"));

		( stateRepeat == "false")  ? toggleRepeat.prop("checked",false) : toggleRepeat.prop("checked", true);
		( stateEvent == "false" )  ? toggleEvent.prop("checked",false) : toggleEvent.prop("checked", true);
		inputVelocity.val(valVelocity);
		console.info( this.parentId );
	},
	deleteSvg:function(){
		var confirm_ = confirm("¿Desea eliminar esta imagen?");
		if ( confirm_ == true ){
			$("."+this.selectedClass).remove();
				this.model.showAlert("ok", "Imagen eliminada exitosamente");
			}
		},
	listarColores: function(){
		console.info("%cListando colores","font-weigh:bold;color:#000;");
		var elemento = $("#customer-character").find(".svg-selected");

		
		if ( elemento.length >= 1 ){
			$(".color-section").html("");
			var styleTag = elemento.find("style");
			var class_   = elemento.find("style").html().split(" ");
			var id_		 = class_[0].trim();
			$(".color-section").append("<h6>colores</h6>");
			//console.warn("Id: "+ id_);
				for (var i = 1; i <= class_.length - 1; i++) {
					var currentClass = class_[i].split("{")[0];
					var color_fill = $(id_).find(currentClass).css("fill");
					//console.warn( "clase: "+ currentClass);
					var color_ = $("<input/>");
						color_.attr("type","text");
						color_.attr("value",color_fill);
						color_.attr("class","custom-color");
						color_.attr("data-class",currentClass);
						color_.attr("data-id",id_);
					$(".color-section").append(color_);
				}
		
				$(".custom-color").spectrum({
					showAlpha: true,
					chooseText: "OK",
		   			cancelText: "Cancelar",
				    move: function(color) { 
				    	var id_parent  = $(this).attr("data-id"),
				    		myClass    = $(this).attr("data-class"),
				    		colorFinal = color.toHexString();
				    	$("#customer-character").find(id_parent).find(myClass).attr("style","fill:"+ colorFinal);
				    	
				    }
				});
			
		}else{
			this.model.showAlert("error", "<b>No se puede listar los colores</b> porqué no hay elementos seleccionados ");
		}
	},
	//añadir elemento al svg
	insertNewFeature: function(e){
		

		var this_ = $(e.currentTarget);
		var url   = this_.find("img").attr("src");

		var currId = this_.attr("data-tag-filter");

		var selectSvg =  function(){ 
			console.info("trigger");
			$("#customer-character").find("g.svg-selected").removeClass("svg-selected");
			$(this.node).attr("class","svg-selected");
				//nextFrame( this , parpadeo, 0);
			 }

		var move = function(dx, dy){
			var this_ 	 = $($(e.currentTarget)),
				elSelect = $(".svg-selected");
		//elSelect.css("transform","translate("+dx+"px,"+dy+"px) scale(0.7,0.7)");

		var transformMatrix = $(".svg-selected").css("transform");
				console.warn( transformMatrix );
			if ( transformMatrix == "none" ){
				elSelect.css("transform","translate("+dx+"px,"+dy+"px)");
			}else{
				
				var valuesMatrix = transformMatrix.split("matrix(")[1].split(",");
				var scaleX = valuesMatrix[0];
				//var skewY  = valuesMatrix[1];
				//var skewX  = valuesMatrix[2];
				var scaleY = valuesMatrix[3];
				//var transX = valuesMatrix[4];
				//var transY = valuesMatrix[5];

				elSelect.css("transform","translate("+dx+"px,"+dy+"px) scale("+scaleX+","+scaleY+")");

				//console.info( transformMatrix );
				//console.info( scaleX );
				//console.info( skewY );
				//console.info( skewX );
				//console.info( scaleY );
				//console.info( transX );
				//console.info( transY );
			}

  
			/*
				this.attr({
					transform: this.data('origTransform') + (this.data('origTransform') ? "T": "t")+ [dx, dy]
				})*/
		}
		
		var start = function(){
				this.data('origTransform', this.transform().local )
		}
		var stop = function(){
				
				$(".gf-incons-save-btn button.dropGroupSvg").attr("data-drop-id");
		}

		 		var s = Snap("#customer-character");
		 		    g = s.group();
		 		 $("#character #"+currId).remove()
				var tux = Snap.load( url , function ( loadedFragment ) {
					//console.warn(loadedFragment);
							g.append( loadedFragment );
		                    g.click( selectSvg );
						    g.drag( move, start, stop);
		                } );
		},
	filter:function(e){
		var filterTag     = $("#filter-tools").val() ? $("#filter-tools").val() : "";
	 	var filterStyle   = $("#style-char").val() ? $("#style-char").val() : "";
	 	var filterGender  = $("#gender-char").val() ? $("#gender-char").val() : "";
	 	var filterEmotion = $("#emotion-char").val() ? $("#emotion-char").val() : "";

	 	$("#features div").each(function(){
	 		var filterTag_    = $(this).attr("data-tag-filter");
	 		var filterStyle_  = $(this).attr("data-type-filter");
	 		var filterGender_ = $(this).attr("data-gender-filter");
	 		var filterEmotion_= $(this).attr("data-emotion-filter");

	 		if ( filterStyle != filterStyle_ ){ $(this).fadeOut("fast"); }

	 			else if ( filterStyle == filterStyle_ ){
	 					//mostra o oculta los generos
	 						
	 				if ( filterGender_.indexOf(filterGender) == -1 )
	 					{ $(this).fadeOut("fast"); console.info("Ocultar genero"); }
	 				else if ( filterGender_.indexOf(filterGender) != -1 )
	 					{ 
	 						//Muestra y oculta las emociones
	 					if ( filterEmotion_.indexOf(filterEmotion) == -1 )
	 						{ $(this).fadeOut("fast"); console.info("Ocultar Emoción"); }
	 					if ( filterEmotion_.indexOf(filterEmotion) != -1 )
	 						{ 	
	 					if ( filterTag_.indexOf(filterTag) == -1){ $(this).fadeOut("fast"); }
	 					if ( filterTag_.indexOf(filterTag) != -1){ 
	 										$(this).fadeIn(); 
	 									}
	 								}
	 						}
	 				}
	 			})
	},
	render: function(){
		this.$el.append( this.template( this.model.toJSON() ) );
	},
	features: function(){
		var response = '';
		$.ajax({
			url: 'json/resources.json',
			type:"get",
			async:false,
				success: function(res) { response = res; },
				error : function(error){ response = error; }
		});
		return response;
	},
	putFeatures: function(){
		var res = this.features();
		for (var i = 0; i <= res.length - 1; i++) {
			var div_ = $("<div/>");
				div_.addClass("custom-sets");
				div_.attr("data-tag-filter",res[i].tag);
				div_.attr("data-type-filter",res[i].type);
				div_.attr("data-gender-filter",res[i].gender);
				div_.attr("data-emotion-filter",res[i].emotion);
						
			var img_ = $("<img/>");
				img_.attr("src",'files/recursos/materials/'+res[i].file);
				div_.append(img_);

				$("#features").append(div_);
		}
	},

});
var Collage = Collage || {};
/** The application main model
* @author Duvan Valencia
* @augments Backbone.View
* @constructor
* @name cssPlugin
*/
Collage.cssPlugin = Backbone.View.extend( /** @lends cssPlugin */ {
	"el":"#gf-ecm-config-lateral-menu",
	template: _.template( $("#css-plugin").html() ),
	initialize: function(){
		this.render();
		console.warn("%c¡Plugin css Cargado!","font-size:12px;font-weight:bold;color:#9933ff;");
		$('.menu .item').tab();
		this.colorPicker();
	},
	/** Set the 'spectrum' properties to input#border-color
    */
	colorPicker: function(){
		$("#border-color").spectrum({ 
			color: "#fff",
			move: function(color) {
				$("#colorBorder").val( color.toHexString() );
			 }
		 });
	},
	render: function(){
		this.$el.append( this.template( this.model.toJSON() ))
	},
	events:{
		"click .css-group-link":"showMoreProperties",
		"keypress .cssPadding":"changePadding",
		"keypress .sizeCss":"changeSize",
		"keypress #css-rotate-grades":"changeRotation",
		"keypress .cssBorderRadius":"changeBorderRadius",
		"click .cssBorderRadius":"changeBorderRadius",
		"click #changeToMultiCurve":"completeBorderRadius"
	},
	/** Show the panel for complete border radius properties css
    */
	completeBorderRadius: function(e){
		var this_ 	  = $(e.currentTarget);
		var hasClass_ = this_.hasClass("data-active");
		var tag_style = $("#borderRadiusCss").attr("data-ref-style");
		if ( hasClass_ == true ){
			$("#borderRadiusCss").attr("data-ref-style","forParts");
		} else if (  hasClass_ == false ){
			$("#borderRadiusCss").attr("data-ref-style","complete");
		}
	},
	/** Set the css border-radius properties for selected element
    */
	changeBorderRadius: function(e){
		var din         = $( this.model.get("window") ).find(this.model.get("selected") ),
			this_       = $(e.currentTarget),
			tag_style   = $("#borderRadiusCss").attr("data-ref-style"),
			color       = $("#colorBorder").val(),
			ancho       = $("#weightBorder").val(),
			anchoIsNan  = isNaN(ancho),
			radius      = $("#borderRadiusCss").val(),
			radiusIsNan = isNaN(radius);

		if ( din.length >= 1 ){	
			if (e.which == 13){
				if (  anchoIsNan == false & ancho >= 0 & ancho <= 10 &  radiusIsNan == false ){
					if ( tag_style == "complete" ){ din.css("border-radius", radius+"px"); }
					else if ( tag_style == "forParts" ){
							$("input.cssBorderRadius[data-style-border]").each(function(){
								var attr_val = $(this).val();
								var attrName = $(this).attr("data-style-border");
									din.css( attrName, attr_val+"px" );
							});
						}
							din.css({
								"border-color": color,
								"border-style":"solid",
								"border-width": ancho+"px"
							 });
					}
				}
		}else{ this.model.showAlert("warning","No hay elementos selecionados"); }
	},
	/** Change the css rotate propertie for selected element
    */
	changeRotation: function(e){
		var this_ = $(e.currentTarget);
		var meVal  = this_.val(),
			NotNum = isNaN(meVal),
			din    = $( this.model.get("window") ).find(this.model.get("selected") );
		if ( din.length >= 1 ){
			if (e.which == 13){
				if (  NotNum == false & meVal >= -360 & meVal <= 360  ){
					  din.css( "transform","rotate("+meVal+"deg)" );
				}else{ this.model.showAlert("error","Por favor ingresa un valor <b>válido</b>"); }
			}
		}else{
			this.model.showAlert("warning","No hay elementos selecionados");
		}
	},
	/** Change the width and height properties for selected element and secondaries elements as images, audios, icons (well, into this selected element)
    */
	changeSize: function(e){
		var din    = $( this.model.get("window") ).find(this.model.get("selected") );
		var window_active = $("#window-edition").attr("data-selected");
			if ( window_active == "true" ) {
				$("input.sizeCss[data-size-attr]").each(function(){
					var meVal  = $(this).val(),
						NotNum = isNaN(meVal),
						attrName  = $(this).attr("data-size-attr");
						$("#window-edition").css( attrName, meVal+"px" );
				})
			}else if (  window_active == "false" ){
				if ( din.length >= 1 ){
					if (e.which == 13){
						$("input.sizeCss[data-size-attr]").each(function(){
							var meVal  = $(this).val(),
								NotNum = isNaN(meVal),
								attrName  = $(this).attr("data-size-attr");

							var hasImage  =  din.find(".dynamic-image"),
								hasObjec  = din.find(".set-color-svg"),
								hasSound  = din.find(".playSound");

							if (  NotNum == false & meVal >= 0 & meVal <= 600  ){

									din.css( attrName, meVal+"px" );
									if ( hasImage.length >= 1){ hasImage.css(  attrName, meVal+"px" ); }
									if ( hasObjec.length >= 1 ){ hasObjec.css(  attrName, meVal+"px" ); }
									if ( hasSound.length >= 1 ){
										hasObjec.css(  attrName, (meVal-20)+"px" );
										hasSound.css(  attrName, (meVal-20)+"px" );
									}
							}
						})
					}
				}else{ this.model.showAlert("warning","No hay elementos selecionados"); }
		}
	},
	/** Change the padding propertie for selected element 
    */
	changePadding: function(e){
		var this_        = $(e.currentTarget),
			completeAttr = this_.attr("data-ref-style"),
			meVal        = this_.val(),
			NotNumber    = isNaN( meVal ),
			din          = $( this.model.get("window") ).find(this.model.get("selected") );
		
		if ( din.length >= 1 ){
			if (e.which == 13){
				if ( NotNumber == false & meVal >= 0 & meVal <= 40 ) {
					if ( completeAttr == "complete" ){
						din.css("padding", meVal+"px");
					}else{
						$("input.cssPadding[data-style-attr]").each(function(){
							var attrName   = $(this).attr("data-style-attr");
							var attr_value = $(this).val();
							din.css( attrName, attr_value+"px" );
						})
					}	
				}else{ this.model.showAlert("error","Por favor ingresa un valor <b>válido</b>")}
			}		
		}else{ this.model.showAlert("warning","No hay elementos selecionados"); }		
	},
	/** Show the complete properties
    */
	showMoreProperties: function(e){
		var this_    = $(e.currentTarget),
			attr_    = this_.attr("data-show"),
			classHas = this_.hasClass("data-active");

		$(".unlinkProperties").addClass( this.model.get("inactive") );
		if ( attr_ ){ 
			if ( classHas == false ){
				 this_.addClass("data-active");
				$("#"+attr_).removeClass( this.model.get("inactive") )
			}
			else if ( classHas == true ) {
					console.info("true!");
				 this_.removeClass("data-active");
				$("#"+attr_).addClass( this.model.get("inactive") )
			}
			
		}
	}
})
var Collage = Collage || {};

Collage.capeElements = Backbone.View.extend({
	"el":"#gf-ecm-config-lateral-menu",
	template: _.template( $("#capesElements").html() ),
	initialize: function(){
		this.model.getElements();
		console.warn("%c¡Plugin capes, Cargado!","font-size:12px;font-weight:bold;color:#9933ff;");
		this.render();
	},
	events:{
		"click li.swicthVisibility" :"swicthVisibility",
		"click li.delete-cape":"deleteMe"
	},
	deleteMe: function(e){
		var this_ = $(e.currentTarget),
			idfp  = this_.attr("data-ref-id");

		var dropTHis = confirm("Al eliminar la capa, también eliminas el elemento. ¿Estás de acuerdo?");
			if ( dropTHis == true ){
				$(".main-cape[data-id='"+idfp+"']").remove();
				$(".element-dinamyc#"+idfp).remove();
				this.model.alert("ok","¡La capa se ha elimindo!");
			}
	},
	swicthVisibility: function(e){
		console.warn("info hello!")
		var this_ = $(e.currentTarget),
			idfp  = this_.attr("data-ref-id"),
			state = this_.attr("data-visible"),
			statePadre, rmSpan, addSpan,  isVisible;
		this_.add("visiblekhjsd")	
	
		
		if ( state == "false" ){
			this_.attr("data-visible", "true");
				rmSpan  = "glyphicon-eye-close inactive";
				addSpan = "glyphicon-eye-open active-color";
			statePadre = "true"; isVisible = "true";
			
		}else if ( state == "true" ){
			this_.attr("data-visible", "false");
			statePadre = "false"; isVisible = "false";
			addSpan  = "glyphicon-eye-close inactive";
			rmSpan   = "glyphicon-eye-open active-color";
		}
		
		this_.find("span.glyphicon").addClass(addSpan).removeClass(rmSpan);
		$(".element-dinamyc#"+idfp).attr("data-visibility-state", isVisible);
		$(".main-cape[data-id='"+idfp+"']").attr("data-visible", statePadre);
	},
	render: function(){
		this.$el.append( this.template( this.model.toJSON() ));
	}
});
var Collage = Collage || {};
/** The application main model
* @author Duvan Valencia
* @augments Backbone.View
* @constructor
* @name icons
*/
Collage.icons = Backbone.View.extend( /** @lends icons */ {
	"el":"#gf-ecm-config-lateral-menu",
	template:_.template( $("#view-icons").html() ),
	initialize: function(){ this.render(); },
	render: function(){
		this.$el.append( this.template( this.model.toJSON() ));
		$('.dropdown').dropdown();
		console.warn("%c!Plugin icons cargado¡","color:#9933ff;font-size:12px;font-weight:bold;");
	},
	events: {
		"click ul li.show-galleries":"materials",
		"contextmenu ul.tooltip-avalaible li.draggable-initial":"options",
		"click .options li.item[data-action='custom']":"personalizar",
		"click .options li.item[data-action='remove']":"remove",
		"click .return-gallery": "returnMain",
		"click .icons-filter .item":"filterIconsGallery",
		"click .filter-chars .item":"filterCharactersGallery"
	},
	/** Filter for icons
    */
	filterIconsGallery: function(e){
		var this_ 	  = $(e.currentTarget),
			valFilter = this_.attr("data-value");
		if ( valFilter ) {
			$("ul#gallery-icons-list li").each(function(){
				var tagStyle = $(this).attr("data-style-icon");
					if ( tagStyle != valFilter ){
						$(this).fadeOut("fast");
					}else if (  tagStyle == valFilter ){
						$(this).fadeIn("fast");
					}
			});
		}
	},
	/** Filter for characters
    */
	filterCharactersGallery: function(e){
		var this_ 	   = $(e.currentTarget),
			typeFilter1   = $("#st-character").val(),
			genderFilter2 = $("#gd-character").val();

			if ( typeFilter1 && genderFilter2 ){
				$("ul#gallery-chars li").each(function(){
					var typeFlt = $(this).attr("data-type-filter"), 
						gendFlt = $(this).attr("data-gender-filter");
					if ( typeFilter1 != typeFlt || genderFilter2 != gendFlt){
						$(this).fadeOut("fast");								
						}
					else if ( typeFilter1 == typeFlt || genderFilter2 == gendFlt){
						$(this).fadeIn("fast");	
					}
				})
			}
	},
	/** Return to main menu icons
    */
	returnMain: function(){
		$(".behind-stage").addClass(this.model.get("inactive"));
		$("#main-menu-gallery").removeClass( this.model.get("inactive") );
		$(".edition-header .return").addClass( this.model.get("inactive") );
		$(".edition-header h5").text("Materiales:");
	},
	/** Remove character if th user is the same 
	*/
	remove: function(){
		var removethis = confirm("¿Deseas eliminar este contenido?");
		if ( removethis == true ){
				console.info("Si tu lo creaste, lo puedes eliminar");
		}
	},
	/** Select current materials (Iconios, personajes, imagenes) or open modal
	*/
	materials:function(e){
		var this_      = $(e.currentTarget),
			Gid = this_.attr("data-child-show"),//gallery id
			noDisappear = this_.attr("data-disapear")//No abrir galeria, Abrir modal
			if ( !noDisappear ){
				$(".edition-header div.return").removeClass( this.model.get("inactive") );
				$(".edition-header h5").text( this_.find(".title").text() +":");
				$(".behind-stage").addClass( this.model.get("inactive") )
				$("#"+Gid).removeClass( this.model.get("inactive") );
			} else{
				$('#modal-box').modal("show");
				$('.dropdown').dropdown();
				$('.accordion') .accordion({  selector: { trigger: '.title .icon'  } });
				
				this.model.showAlert("ok","!Modal de edición de personajes, abierto!");
			}
		e.stopPropagation();
	},
	/** Active contexMenu for gallery characters
	*/
	options: function(e){
		var this_ = $(e.currentTarget);
			e.preventDefault();
			var x = event.clientX;  
				var y = event.clientY; 
				$("#character-gallery .options")
					.removeClass( this.model.get("inactive") )
					.css({"left": (x-100)+"px", "top": (y-100)+"px"});
	}
});
var Collage = Collage || {};
/** The view for upper side panel
* @author Duvan Valencia
* @augments Backbone.View
* @constructor
* @name leftSideBar
*/
Collage.leftSideBar = Backbone.View.extend( /** @lends leftSideBar */ {
	"el":"#side-left-menu",
	plugins: [],
	template: _.template( $("#left-side-bar").html() ),
	render: function(){
		console.info("%c///barra lateral izquierda///","font-size:14px;color:green;font-weight:bold;")
		this.$el.append( this.template( this.model.toJSON() ));
	},
	initialize: function(){ this.render(); },
	events: {
		"click li.left-tool": "activePanel",
		"click article.left-side-bar-edition #close":"closepPanel"
	},
	/** Show active plugin on the left-side-bar-edition panel
    */
	activePanel: function(e){
		var this_ = $(e.currentTarget);
		var viewEdit = this_.attr("data-view");
		$("li.left-tool").removeClass("left-active");
			this_.addClass("left-active");
		$(".plugins").addClass( this.model.get("inactive") );
		switch ( viewEdit ){
			case "Multimedia":
				$("#view-multi-section").removeClass( this.model.get("inactive") );
			break;
			case "icons":
				$("#plugin-icons").removeClass( this.model.get("inactive") );
				this.model.componentsWidget();
			break;
			case "text":
				$("#view-text-plugin").removeClass( this.model.get("inactive") );
				this.model.componentsWidget();
			break;
			case "css":
				$("#view-css-plugin").removeClass( this.model.get("inactive") );
				this.model.componentsWidget();
			break;
			case "elements":
				var capesElem  = new Collage.capeElements({ model: this.model });
				$("#elements-as-capes").removeClass( this.model.get("inactive") );
			break;
		}
		$(".left-side-bar-edition").removeClass( this.model.get("inactive") );

	},
	/** close ppanel de edición = left-side-bar-edition panel
    */
	closepPanel: function(){
		console.warn(this.model.get("inactive"));
		$(".left-side-bar-edition").addClass( this.model.get("inactive") );	
	}	
});


var Collage = Collage || {};
/** The application main model
* @author Duvan Valencia
* @augments Backbone.View
* @constructor
* @name multimedia
*/
Collage.multimedia = Backbone.View.extend( /** @lends multimedia */ {
	"el":"#gf-ecm-config-lateral-menu",
	template: _.template( $("#view-multimedia").html() ),
	initialize: function(){
		this.render();
	},
	render: function(){
		this.$el.append( this.template(this.model.toJSON()) );
		console.warn("%c!Plugin multimedia cargado¡","color:#9933ff;font-size:12px;font-weight:bold;");
	},
	events: {
		"click .multi-item": "selectItem",
		"click #imgtest": "tmpImage",
		"click #audiotest": "tmpAudio",
		"click #generate-audio":"generate"
	},
	/** Generate audio ttf
    */
	generate: function(){
		var params = [];
		var txt = $(".textarea-to-generate").val();

			params.gender =  $("#gender").val();
			params.lang   =  $("#lang").val();
			params.tone   =  $("#tono").val();
			params.speed  =  $("#speed").val();
			
			if ( txt.length <= 10){
				this.model.showAlert("error","Por favor ingresa un texto")
			}else{
				this.generateVoice(txt, params);
			}
	},
	/** load tts
    */
	generateVoice: function(texto, params){
			var variablesURL;
		    var ubicacionTTS = "../../../tts/";
		    var archivo      = 'index.php';
		    var genFolder    = 'files/';
		    var metodo       = 'GET';
		    var txtEsc       = "txt=" + escape(texto); //Preparar texto "escapado" para inserción en la URL  
		    variablesURL     = "idi=" + params.lang + "&ora=" + params.gender + "&vel=" + params.speed + "&ton=" + params.tone + "&" + txtEsc;
			
			$.ajax(ubicacionTTS + archivo,
		    {
		        "type": metodo,   // usualmente post o get
		        async:false,
		        data: variablesURL,        
		        beforeSend: function () { },
		        success: function(respuesta){
		        	console.warn("Todo correcto");
		        	/*$("div#sound_").empty();
		        	var soundPath = datafolder+"/tts/" + respuesta;
					var mp3Path   = soundPath + ".mp3";
					var oggPath   = soundPath + ".ogg";
					
					$("#saveMsj")
					.attr('data-audio-state',"true")
					.attr('data-audio-name', respuesta)
					.attr('data-folder-file', 'tts');

					//var _audio_ = '<audio controls class="audio"><source src="'+wwwupload+"/tts/" + respuesta + ".ogg"+'" type="audio/mpeg"></audio>';
					var _audio_ = '<audio controls class="audio"><source src="'+window.location.origin+'/'+datafolder+"/tts/" + respuesta + ".ogg"+'" type="audio/mpeg"></audio>';
	        		
	        		$("div#sound_").append(_audio_);
					
					var valText = $("#editTextEdition").val();
			    		
					$("#estu")
		            .removeClass("alert-danger").addClass("alert-success")
		            .css("display","block").text("audio creado Exitosamente");
		            alert("Conversión realizada exitosamente");
					$("#convertOptions").fadeOut();*/
				 },
		        error: function(respuesta) {
		            $("#estu")
		            .removeClass("alert-success")
					.addClass("alert-danger")
		            .css("display","block")
		            .text("Ha ocurrido un error "+respuesta);
		        },	    	 
		});   
	},
	/** switch between upload audio or image
    */
	selectItem: function(e){
		var this_   = $(e.currentTarget),
			subMenu = this_.attr("data-sub-menu-child");
		$(".multi-item").removeClass("active");
		$(".multimedia-tools .tools").addClass("element-inactive");
		$("#"+subMenu).removeClass("element-inactive");
		$(e.currentTarget).addClass("active");
	},
	/** upload audio/image
    */
	upload_:function(){
		//console.warn("instanciado");
		$("body").on("click",'#char-upload', function(){
			//console.warn( "hello!!" );
		})
		$(function () {
		    'use strict';
		    // Change this to the location of your server-side upload handler:
		    var url = '../../jquery-file-upload-handler.php';
		 
		    $('#char-upload').fileupload({
		    	dropZone: $(this),
		        url: url,
		        dataType: 'json',
		        autoUpload: true,
		        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
                maxFileSize: 1 * 1000000, // 1 Mb
		        done: function (e, data) {
		        	//console.warn("audio upladed");
		        	/*var elem_ = $(this);
		        	var file_ = data.result.files[0];
		        	if( myself.current_Upload != 'bgMain' ){
		        			$.each(data.result.files, function (index, file) {
								elem_
								.parent().parent().parent().siblings().find("div#bgImgCnt")
								.css("background-image","url("+file_.url+")");
							});
		        	}
		        	else if( myself.current_Upload == 'bgMain' ){
		        			$.each(data.result.files, function (index, file) {
			        				elem_
									.parent().parent().parent().siblings()
									.find("div").find("img")
									.attr("src", file_.url);
		        				});
		        	}*/
				},
		      	progressall: function (e, data) {
		      		//console.info("here")
					/*var progress = parseInt(data.loaded / data.total * 100, 10);
					$(this).parent().siblings()
					.find('.progress-bar')
					.css('width', progress + '%');*/
				}
		    })
		    .on('fileuploadfail', function(e, data) {
					console.warn("FALLO!!")
			})
            .prop('disabled', !$.support.fileInput)
		    .parent().addClass($.support.fileInput ? undefined : 'disabled');
		});
	},
	/** Pruebas
    */
	tmpImage: function(){
		var file_image = "files/web.jpg";
		$("#window-edition").append( this.model.templateDinamyc( this.model.tmp_Image(file_image) ) );
		this.model.componentsWidget();
		this.model.showAlert("ok","Recurso añadido correctamente");
	},
	tmpAudio: function(){
		var file_audio = "files/short.mp3";
		$("#window-edition").append( this.model.templateDinamyc( this.model.tmp_audio(file_audio) ) );
		this.model.componentsWidget();
		this.model.showAlert("ok","Recurso añadido correctamente");
	}
});
var Collage = Collage || {};
/** The application main model
* @author Duvan Valencia
* @augments Backbone.View
* @constructor
* @name texto
*/
/** All events about text as create it or edit it
  * are into view.window.js. because the double click event 
  * is triggered into div#window-edition 
  */
Collage.texto = Backbone.View.extend( /** @lends texto */ {
	"el":"#gf-ecm-config-lateral-menu",
	template: _.template( $("#text-plugin").html() ),
	initialize: function(){
		this.render();
	},
	render: function(){
		console.warn( "%c¡Plugin texto, cargado!","font-size:12px;font-weight:bold;color:#9933ff;" );
		this.$el.append( this.template(this.model.toJSON()) );
	}
});
var Collage = Collage || {};
/** The view for upper side panel
* @author Duvan Valencia
* @augments Backbone.View
* @constructor
* @name upperSideBar
*/
Collage.upperSideBar = Backbone.View.extend( /** @lends upperSideBar */ {
	events:{ 
		"click li.tool[data-tool-tip-text='Copiar']":"copy",
		"click li.tool[data-tool-tip-text='Pegar']":"paste",
		"click li.tool[data-tool-tip-text='Cortar']":"cut",
		"click li.tool[data-tool-tip-text='Eliminar']":"delete",
		"click li.tool[data-tool-tip-text='Color de fondo']":"bgColor",
		"click li.tool[data-tool-tip-text='Color de letra']":"txtColor",
		"click li.tool[data-tool-tip-text='Organizar']":"ShowSubMenu",
		"click .sub-menu-tool": "organize",
		"mouseover li.tool":"showTooltip",
		"mouseleave li.tool": "hiddeTooltip",
	},
	/** Check if exists a element copied or cuted
    */
	copyActive: false,
	/** container of html object copied or cuted
    */
	cloneEl: null,
	'el': "#upper-side-bar",
	template: _.template($("#upperSideBar").html()),
	initialize: function(){ 
		this.render();
		this.color();
	},
	render: function(){
		console.info("%c///barra superior///","font-size:14px;color:green;font-weight:bold;")
		this.$el.append(this.template( this.model.toJSON() ));
	},
	/** set the color (text and background) for icons, text and audio
    */
	color: function(){
		var window_  = this.model.get("window"),
			selectd_ = this.model.get("selected");

		$(".color-option-input").spectrum({
			preferredFormat: "hex",
			showInput: true,
			cancelText: "CERRAR",
    		chooseText: "OK",
			color: "#fff",
			move: function(color){
				var widw_active  = $("#window-edition").attr("data-selected");
				var obj_         = $( window_ ).find( selectd_ ),
					iconColor    = obj_.find("object.icon-color"),
					AudioColor   = obj_.find("object.audio-color"),
					audioBgColor = obj_.find('div.playSound'),
					txtBgColor   = obj_.find('div.text-plugin');

				var action  = $(this).attr("id"), myColor = color.toHexString();
			if ( widw_active == "true" ){
				if ( action == "bg-color" ){
					$("#window-edition").css("background-color", myColor+" !important");
				}
			}else if ( widw_active == "false" ){

				if ( obj_.length >= 1 ){
					//Inicio del color de fondo
					if ( action == "bg-color" ){
						//Audio
						if ( audioBgColor.length >= 1 ){
							audioBgColor.css({"background-color": myColor})
						} 
						else{
							obj_.css( "background-color", myColor );
						}
					}// fin del color de fondo
					//Inicio color de la letra
					if ( action == "txt-color" ){
						//iconos
						if ( iconColor.length >= 1 ){
							iconColor.attr("data-icon-color",myColor );
							var a = document.getElementById( iconColor.attr("id") ),
								svgDoc = a.contentDocument,
								svgItem = svgDoc.getElementById("icon");
								svgItem.setAttribute("fill", myColor );		
						}//fin del color de los iconos
						//ICONO DEL AUDIO
						if ( AudioColor.length >= 1) {
							AudioColor.attr("data-color",myColor);
							var a = document.getElementById( AudioColor.attr("id") ),
								svgDoc = a.contentDocument,
								svgItem = svgDoc.getElementById("icon");
								svgItem.setAttribute("fill", myColor );
						}//FIN ICONO DEL AUDIO
						//Color de la letra del plugin texto
						if ( txtBgColor.length >= 1 ){
							txtBgColor.css("color", myColor);
						}//fin
					}
				}
		}
	}//fin  widw_active == "false"
		});
	},
	/** Copy the selected element width class selected-obj
    */
	copy: function(){
		var eleSlc  = $(this.model.get("selected"));
			if ( eleSlc.length > 0 ){
				this.copyActive = true;

				var elemClone =  $( this.model.get("selected") ).clone();
					elemClone.removeClass( "selected-obj ui-draggable ui-draggable-handle ui-resizable active" );
					elemClone.attr( "id" , this.model.createId("dinamyc") );

				var object_ = elemClone.find(".set-color-svg");
				var text_   = elemClone.find(".text-plugin");
				if ( object_.length >= 1 )
					{ object_.attr("id", this.model.createId("icon")); }
				if ( text_.length >= 1)
					{	
						var id_ = this.model.createId("text");
							text_.attr("id", id_+"-plugin");
							text_.find(".editable-text-header").attr("id",id_);
							text_.find(".editable-text-input").attr("id", id_+"-editable");
							text_.find(".editable-text-input").find("input").attr("data-id-parent",id_)
					}

						this.cloneEl = elemClone;	
						this.model.showAlert("ok","Elemento Copiado");
			}else if ( eleSlc.length == 0 ){ this.model.showAlert("warning","Primero, <b>selecciona</b> un elemento");}
	},
	/** paste the copied/cutted element
    */
	paste: function(){
		if ( this.copyActive != false ){
				this.model.deselected();
				this.cloneEl.addClass( this.model.get("selected").replace(".","") );
					$(this.model.get("window")).append(this.cloneEl);
				this.copyActive = false;
				this.model.componentsWidget();
				this.model.showAlert("ok","<b>Elemento pegado</b>");
			}else{  this.model.showAlert("warning","Primero, <b>Copia/Corta</b> un elemento"); }
	},
	/** cut the selected element width class selected-obj
    */
	cut: function(){
		var eleSlc  = $(this.model.get("selected"));
			if ( eleSlc.length > 0 ){
				this.copyActive = true;

				var elemClone =  $( this.model.get("selected") ).clone();
					elemClone.removeClass( "selected-obj ui-draggable ui-draggable-handle ui-resizable active" );
					elemClone.attr( "id" , this.model.createId("dinamyc") );

				var object_ = elemClone.find(".set-color-svg");
				var text_   = elemClone.find(".text-plugin");
				if ( object_.length >= 1 )
					{ object_.attr("id", this.model.createId("icon")); }
				if ( text_.length >= 1)
					{	
						var id_ = this.model.createId("text");
							text_.attr("id", id_+"-plugin");
							text_.find(".editable-text-header").attr("id",id_);
							text_.find(".editable-text-input").attr("id", id_+"-editable");
							text_.find(".editable-text-input").find("input").attr("data-id-parent",id_)
					}
					this.cloneEl = elemClone;
				$("#"+$( this.model.get("selected") ).attr("id")).remove();
				this.model.showAlert("ok","Cortado");
			}else if ( eleSlc.length == 0 ){
				this.model.showAlert("warning","Primero, <b>selecciona</b> un elemento");
		}
	},
	/** Delete the selected element width class selected-obj
    */
	delete: function(){
		var eleSlc  = $(this.model.get("selected"));
			if ( eleSlc.length > 0 ){
				if ( confirm("¿Desea eliminar este elemento?" ) == true ){
						var elem =  $( this.model.get("selected") ).clone();
							$("#"+elem.attr("id")).remove();
						this.model.showAlert("ok","Eliminado con exito");
					}
			}else{
				this.model.showAlert("warning","No hay Elementos <b>seleccionados</b>");
			}
	},
	/** Show the menu for organize elements
    */
	ShowSubMenu: function(e){
			e.stopPropagation();
		var this_ = $(e.currentTarget),
			classHas = this_.hasClass("data-submenu-inactive"),
			remIcon, addIcon;
		if ( classHas == true ){
			 this_.removeClass("data-submenu-inactive");
			 this_.addClass("thisclosed");
			 this_.find(".sub-menu-upper").removeClass( this.model.get("inactive") );
			 remIcon = "glyphicon-indent-left";
			 addIcon = "glyphicon-remove";
		}else if ( classHas == false ){
			this_.addClass("data-submenu-inactive");
			this_.removeClass("thisclosed");
			remIcon = "glyphicon-remove";
			addIcon = "glyphicon-indent-left";
			$(".sub-menu-upper").addClass( this.model.get("inactive") );
		}	
		this_.find("span.glyphicon")
			 .removeClass(remIcon)
			 .addClass(addIcon);
	},
	/** organize the seleted elements
    */
	organize: function(e){
		var this_ = $(e.currentTarget),
		 	eleSlc  = $(this.model.get("selected")),
		 	IndexGlobal = this.model.getZindex(),
		 	action_ = this_.attr("data-action");

			if ( eleSlc.length > 0 ){
				var Index_ = parseInt ( this.model.getZindex() );
				//Subir z index
				if ( action_ == "toUp" ){
					Index_ += 1;
					this.model.setZindex(Index_ );} 
				//Bajar z-index
				else if ( action_ == "toDown" ) {
					if (Index_ != 0){ Index_ -= 1; }
				}

				eleSlc.css("z-index",Index_);
			}else{
				this.model.showAlert("warning","No hay elementos seleccionados");
			}	
	},
	/** Show the tooltip for each tool
    */
	showTooltip: function(e){
		var infoAct = $(e.target).attr("data-tool-tip-text");
		if ( infoAct ) {
			var x = event.clientX,  y = event.clientY;
			var toolTip 	= $(".tooltip");
				toolTip.removeClass( this.model.get("inactive") );
				toolTip.text(infoAct);
				toolTip.css({"left":x+"px"});
		}
	},
	/** Hide the tooltip for each tool
    */
	hiddeTooltip: function(e){
		var toolTip 	= $(".tooltip");
			toolTip.addClass(this.model.get("inactive"));
	}

});


var Collage = Collage || {};
/** The application main model
* @author Duvan Valencia
* @augments Backbone.View
* @constructor
* @name window
*/
Collage.window = Backbone.View.extend( /** @lends window */ {
	el:"#window-edition",
	/** show the help info into div#console, ex:audio play 
    */
	noHelps:[],
	template: _.template( $("#view-window-main").html() ),
	initialize: function(){ this.render(); },
	render:function(){
		console.info( "%c>Ventana principal<","color:#008ae6;font-size:14px;font-weight:bold;" );
		this.$el.append( this.template( this.model.toJSON() ) );
	},
	events: {
		"click .element-dinamyc":"selected",
		"dblclick div.playSound":"playAudio",
		"contextmenu .element-dinamyc": "showOptions",
		"click .close-menu":"closeSubMenu",
		"dblclick .editable-text-header":"setTextToInput",
		"keypress input.editable":"setText",
		"click .window-menu":"showPropertiesForWindow",
		"click .window-options-list .item-option[data-action='select']": "selectWindow",
		"click .window-options-list .item-option[data-action='getId']": "getId",
		"click .window-options-list .item-option[data-action='meassure']": "changeMeassure",
		"click .window-options-list .item-option[data-action='deselectAll']": "removeSelect",
		"click .sub-menu-click-right ul li.gf-ecm-tool[data-action='visibility']":"switchVisibility",
		"click .sub-menu-click-right ul li.gf-ecm-tool[data-action='show-not-visible-elements']":"listAllNotVisibleElements",
		"click .sub-menu-click-right ul li.gf-ecm-tool[data-action='show-hidden']":"showHiddenMenu",
		"click #notVisiblesElem .li-element":"seiIdToEvent",
		"mouseover #listAllElDinamyc .li-element, #notVisiblesElem .li-element":"whereItIs",
		"mouseleave #listAllElDinamyc .li-element, #notVisiblesElem .li-element":"removeAnimation",
		"click #allElementsApp": "showDinElements",
		"click #listAllElDinamyc .li-element": "saveForHidde"
	},
	/** Hidde the 'sub menu' for click right over dinamyc  element
	  * submenu mostrar elemento and ocultar elemento
      */
	hiddeSubMenu: function(){
		$(".listElements").addClass( this.model.get("inactive") );
	},
	/** ANIMATION
	  * Quit animation the dinamyc element, hidde o visible
      */
	removeAnimation: function(){
		$(this.model.get("window")).find( ".element-dinamyc" ).each(function(){
			$(this).removeClass("whereIsIt");
		});
	},
	/** ANIMATION
	  * Add animation to find the element for add the to event 
      */
	whereItIs : function(e){
		var this_ = $(e.currentTarget);
		$("#"+this_.attr("data-ref-id")).addClass("whereIsIt");
	},
	/** CONTEXT MENU EVENT
	  * save the new id into selected dinamyc element, 
	  * is used for hidde it when someone click in element
      */
	saveForHidde: function(e){
		var this_ = $(e.currentTarget),
			currentId = this_.attr("data-ref-id"),
			selectd = $( this.model.get("window") ).find( this.model.get("selected") ),
			attrIds  = selectd.attr("data-collection-to-disable"),
			finder = attrIds.indexOf(currentId);	
		if ( finder != -1 ){
		 	var ids_ = attrIds.replace(currentId+",", "");	
		 		selectd.attr( "data-collection-to-disable" , ids_ );
		 		this_.removeClass("active");
		}else{
		 		this_.addClass("active"); attrIds += currentId+",";
		 		selectd.attr("data-collection-to-disable", attrIds);
		}	
	},
	/** CONTEXT MENU EVENT
	  * Open the secondary menu for option ocultar elementos
      */
	showDinElements: function(e){
		var this_ = $(e.currentTarget);
		var selectd = $( this.model.get("window") ).find( this.model.get("selected") ),
			idsDisable = selectd.attr("data-collection-to-disable");
		this.hiddeSubMenu();
			$("#main-contextmenu").find("li.gf-ecm-tool[data-action='show-not-visible-elements']").removeClass("active");
			$("#listAllElDinamyc").removeClass( this.model.get("inactive") );
		this_.addClass("active");
		$("#listAllElDinamyc li.li-element").each(function(){
			var finder = idsDisable.indexOf($(this).attr("data-ref-id"));
			console.warn(finder);
			if ( finder != -1 ){
				$(this).addClass("active");
			}
		});
	},
	/** CONTEXT MENU EVENT
	  * get the id reference to show it element
      */
	seiIdToEvent: function(e){
		var this_     = $(e.currentTarget),
			currentId = this_.attr("data-ref-id"),
		 	select    = $(this.model.get("window")).find( this.model.get("selected"));

			if ( currentId == select.attr("id") ){ 	
		 		this.model.showAlert("error","No puedes seleccionar el <b>MISMO</b> elemento")
		 	}else{
		 		
		 	var attrIds  = select.attr("data-ref-id-event");
			var finder   = attrIds.indexOf(currentId);
		 		
		 		if ( finder != -1 ){
		 			var ids_ = attrIds.replace(currentId+",", "");	
		 				select.attr( "data-ref-id-event" , ids_ );
		 				this_.removeClass("active");
		 		}else{
		 			this_.addClass("active");
		 			attrIds += currentId+",";
		 			select.attr("data-ref-id-event", attrIds);
		 		}	
		}	
	}, 
	/** CONTEXT MENU EVENT
	  * create li for sub menu context menu
      */
	tmpElementDinamy: function(  ide, title, classIcon  ){
		var li_ = $("<li/>");
			li_.addClass("li-element");
			li_.attr("data-ref-id", ide );
		
		var spanTxt  = $("<span/>");
		    spanTxt.addClass("title-list-element");
			spanTxt.text( title );

		var spanIcon = $("<span/>");
			spanIcon.addClass("icon-menu-hidden glyphicon glyphicon-"+classIcon);
			li_.append(spanIcon);
			li_.append(spanTxt);

		return li_;
	},
	/** CONTEXT MENU EVENT
	  * click in option mostrar elementos
      */
	listAllNotVisibleElements: function(e){
		this.hiddeSubMenu();
		var eleSelectd = $( this.model.get("window") ).find( this.model.get("selected") );
		$("#main-contextmenu").find("li.gf-ecm-tool[data-action='hidde-others']").removeClass("active");
	 	var this_ = $(e.currentTarget),
	 		menu_ = $("#notVisiblesElem"), title,
	 		ids   = eleSelectd.attr("data-ref-id-event");

	 		$("#notVisiblesElem li.li-element").each(function(){
		 		if ( ids != undefined ){
		 			if ( ids.indexOf($(this).attr("data-ref-id")) != -1 ){
		 				$(this).addClass("active");
		 			};
		 		}
	 		})

	 		menu_.removeClass("element-inactive");
	 		this_.addClass("active");
		e.stopPropagation();
	},
	/** CONTEXT MENU EVENT
	  * switch between visible or not visible
      */
	switchVisibility: function(e){
		var this_ = $(e.currentTarget);
		var eleSelectd = $( this.model.get("window") ).find( this.model.get("selected") );
			stateVisility = eleSelectd.attr("data-visibility-state");
		
		if ( stateVisility == "true" ){
			eleSelectd.attr("data-visibility-state", "false");	
			this_.addClass("active");
		}else if ( stateVisility == "false" ){
			eleSelectd.attr("data-visibility-state", "true");
			this_.removeClass("active");	
		}
	},
	/** get all dinamyc elements in #window
    */
	GetDinamycElements: function(){
		var idents = [];
		$( this.model.get("window") )
			.find(".element-dinamyc").each(function(){
			var Isimg_  = $(this).find("img.gf-object-image"),
				IsAudio = $(this).find("div.playSound"),
				IsIcon  = $(this).find("object.icon-color"),
				IsText  = $(this).find("div.text-plugin"), 
				isVisible = $(this).attr("data-visibility-state"),
				Id = $(this).attr("id"), whatItIs;

				if ( Isimg_.length >= 1){ whatItIs  = "img"; }
				if ( IsAudio.length >= 1){ whatItIs = "audio"; }
				if ( IsIcon.length >= 1){ whatItIs  = "icon"; }
				if ( IsText.length >= 1){ whatItIs  = "text"; }
			idents.push({"ide":Id, "type": whatItIs, "visibility":isVisible });
		});
		return idents;
	},
	/** remove all .selected-obj class for all elements (windiw and dinamyc)
    */
	removeSelect: function(){ this.model.deselected(); },
	/** switch meassure between px and %
    */
	changeMeassure: function(e){
		var this_    =  $(e.currentTarget),
			showInfo = this_.attr("data-info"),
			valMs    = this_.find("#unity-window"),
			messure_val = valMs.text();

			if ( messure_val == "px" || messure_val == "%"){
				switch (messure_val) {
					case "px": valMs.text("%"); break;
					case "%": valMs.text("px"); break;
				}
				if ( showInfo == "false" ){
					this.model.showAlert(
							"ok",
							"Si seleccionas <b>px</b> los elementos se mantendran <b>fijos</b>. "+
							"Si seleccionas <b>%</b> los elementos se <b>redimensionaran</b> hasta cierto punto."
							);
					this_.attr("data-info","true");
				} 
			}else{
				this.model.showAlert("error","El valor de medida no es válido");
			}
	},
	/** Get id for selected element or window
    */
	getId: function(e){
		var isWindow_ = $('#window-edition[data-selected="true"]'),
			isDinamy  = $("#window-edition").find(".selected-obj");
			if ( isWindow_.length != 0 || isDinamy.length != 0){
				var elemento = ( isWindow_.length >= 1 )? isWindow_ : isDinamy; 
					this.model.set("idCopied", elemento.attr("id"));
					this.model.showAlert("ok","El Id: <b>"+ this.model.get("idCopied")+ "</b> Ha sido copiado");
			}else{
				this.model.showAlert("error","No hay elementos seleccionados");
			}
	},
	/** Click in dinamyc element
    */
	selectWindow: function(e){
		var this_ = $(e.currentTarget);
			this.model.deselected();
		$('#window-edition').attr("data-selected","true");
		$("#css-menu").find(".item")
			.not('.item[data-tab="size"]')
				.each(function(){
					$(this).fadeOut("fast").removeClass("active");
				});
				$("#css-menu").find(".item[data-tab='size']").addClass("active");
		$(".css-behind-stage").removeClass("active");
		$(".css-behind-stage[ data-tab='size']").addClass("active");
		$("#sizeWidthCss").val( parseInt( $('#window-edition').css("width") ) );
		$("#sizeHeightCss").val( parseInt( $('#window-edition').css("height") ) );
	},
	/** Menu for window (selccionar, obtener Id, unidad px...)
    */
	showPropertiesForWindow:function(e){
		var this_ 	 = $(e.currentTarget),
			attrClas = this_.hasClass("show-menu"),
			classIcondAdd, classIcondRmv;

			if ( attrClas == true ){
				this_.removeClass("show-menu").addClass("close-it")
				classIcondAdd = "glyphicon-remove";
				classIcondRmv = "glyphicon-menu-hamburger";
				$(".window-options").removeClass( this.model.get("inactive") );
			} else if ( attrClas == false ){
					this_.addClass("show-menu").removeClass("close-it");
					classIcondRmv = "glyphicon-remove";
					classIcondAdd = "glyphicon-menu-hamburger";
					$(".window-options").addClass( this.model.get("inactive") );
			}

			this_.find("span").removeClass(classIcondRmv).addClass(classIcondAdd)
	},
	/** Enter into input plugin text
    */
	setText: function(e){
		var this_ = $(e.currentTarget);
		 if( e.which == 13 ) {
		 	var id_padre = this_.attr("data-id-parent");
	        $("#"+id_padre).text(this_.val()).removeClass('element-inactive');
	        this_.parent(".editable-text-input").addClass("element-inactive");
	        this.model.showAlert("ok","¡Texto cambiado con exito!")
	    }
	},
	/** Double click over selected element type text, click for edit it's text
    */
	setTextToInput: function(e){
		var this_ = $(e.currentTarget);
		var this_val = this_.attr("id");
		var sectionTxt = $("section#"+this_val+"-editable");

		this_.addClass("element-inactive");
		sectionTxt.removeClass("element-inactive");
		sectionTxt.find("input.editable").val(this_.text());
	},
	/** close context menu
    */
	closeSubMenu: function(){
		this.hiddeSubMenu();
		$("#main-contextmenu").addClass("element-inactive");
	},
	/** open contextmenu
    */
	showOptions: function(e){
		e.preventDefault();
		this.selected(e);
		this.hiddeSubMenu();
		var ElemsHidden = this.GetDinamycElements(),
			selectd     =  $( this.model.get("window") ).find( this.model.get("selected") );

		if ( selectd.length <= 0 ){
			this.model.showAlert("error", this.model.get("alertNotSelect") );
		}else{
			var stateVisility = selectd.attr("data-visibility-state"),
				visibilityOption = $("#main-contextmenu").find("li.gf-ecm-tool[data-action='visibility']");
			var topClient = event.clientY, leftClient = event.clientX;

			$("#main-contextmenu").css({"left": (leftClient-100)+"px","top":(topClient-100)+"px"});
			
			$("#main-contextmenu").find("li.gf-ecm-tool[data-action]").removeClass("active");
			if ( stateVisility == "false" ){ visibilityOption.addClass("active"); }
			else{ visibilityOption.removeClass("active"); }
			/*Agregamos elementos que estan escondidos*/
			$("#notVisiblesElem").html("");
			for (var i = 0; i <= ElemsHidden.length - 1 ; i++) {
				var classIcon ,title;
				if ( ElemsHidden[i].visibility == "false" ){
					var classIcon ;
						switch ( ElemsHidden[i].type ) {
						case "img": classIcon = 'picture'; title = "Imagen";  break;
						case "audio": classIcon = 'music'; title = "Audio";  break;
						case "icon": classIcon = 'star-empty'; title = "Icono";  break;
						case "text": classIcon = 'font'; title = "Texto";  break;
					}
					var li_tmpl = this.tmpElementDinamy( ElemsHidden[i].ide, title, classIcon );
					$("#notVisiblesElem").append( li_tmpl );
				}
			}
			/*Lista de TODOS los elementos*/
			$("#listAllElDinamyc").html("");
			for (var i = 0; i <= ElemsHidden.length - 1 ; i++) {
				var classIcon ,title;
				var classIcon ;
					switch ( ElemsHidden[i].type ) {
						case "img": classIcon = 'picture'; title = "Imagen";  break;
						case "audio": classIcon = 'music'; title = "Audio";  break;
						case "icon": classIcon = 'star-empty'; title = "Icono";  break;
						case "text": classIcon = 'font'; title = "Fuente";  break;
					}
					var li_tmpl = this.tmpElementDinamy( ElemsHidden[i].ide, title, classIcon );
					$("#listAllElDinamyc").append( li_tmpl );
			}

			$("#main-contextmenu").removeClass("element-inactive");
		} 		
	},
	/** Click in dinamy element and set it's styles into plugin css
    */
	selected: function(e){
		this.model.deselected();
		var this_ = $(e.currentTarget);
			this_.addClass("selected-obj");

			$("#css-menu").find(".item").each(function(){ $(this).fadeIn("fast").removeClass("active"); });
			$("#css-menu").find(".item[data-tab='padding']").addClass("active");
			$(".css-behind-stage").removeClass("active");
			$(".css-behind-stage[ data-tab='padding']").addClass("active");

			var widthCSS    = parseInt(this_.css("width"));
			var tranform_ 	= this_.css("transform");
			var heightCSS   = parseInt(this_.css("height"));
			var paddinCSS   = this_.css("padding").split(" "),
				top_ = 0, right_ = 0, bottom_ = 0, left_ = 0;

			var borderColor  = this_.css("border-color"),
				borderWidth  = this_.css("border-width"),
				borderRadius = this_.css("border-radius").split(" "),
				Rtop_ = 0, Rright_ = 0, Rbottom_ = 0, Rleft_ = 0;

			//Colocar propiedades del Border	
				if ( borderRadius.length > 1 ) {
					Rtop_    = parseInt(borderRadius[0]),
					Rright_  = parseInt(borderRadius[1]),
					Rbottom_ = parseInt(borderRadius[2]),
					Rleft_ 	 = parseInt(borderRadius[3]);

					$("#borderRadiusCss").val( 0 );
				}else if( borderRadius.length <= 1 ){
					$("#borderRadiusCss").val( parseInt(borderRadius[0]) );
				}

					$("#RadiusTopLeft").val ( Rtop_ );
					$("#RadiusTopRight").val ( Rright_ );
					$("#RadiusBottompLeft").val ( Rleft_);
					$("#RadiusBottompRight").val ( Rbottom_);
					$("#weightBorder").val( parseInt(borderWidth) );
					$("#colorBorder").val( borderColor );
			//fin border
			//Colocar padding
			if ( paddinCSS.length > 1 ){
				top_    = parseInt(paddinCSS[0]);
				bottom_ = parseInt(paddinCSS[1]);
				right_  = parseInt(paddinCSS[2]);
				left_   = parseInt(paddinCSS[3]);
				$("#paddingGlobal").val( 0 );
			}
			else if ( paddinCSS.length <= 1 ){
				$("#paddingGlobal").val( parseInt(paddinCSS[0]) );
			}//fin colocar padding

			if ( tranform_ != "none" ){
				var values = tranform_.split('(')[1],
			   	 	values = values.split(')')[0],
			   	 	values = values.split(',');
				var b = values[1];
				var angle = Math.round(Math.asin(b) * (180/Math.PI));
				$("#css-rotate-grades").val(angle);
			}else if ( tranform_ == "none" ){  $("#css-rotate-grades").val(0); }
			//console.info( tranform_ );
			//padding
			$("#paddingTop").val( top_ );
			$("#paddingBottom").val( right_ );
			$("#paddingRight").val( bottom_ );
			$("#paddingLeft").val( left_ );
			//Tamaño
			$("#sizeWidthCss").val(widthCSS);
			$("#sizeHeightCss").val(heightCSS);

			$(".element-dinamyc").removeClass("active");
			this_.addClass("active");

		var el_audio = this_.find("div.playSound");
			if ( this.noHelps.indexOf("audio") == -1) {
				if ( el_audio.length >= 1 ){
					this.noHelps.push("audio");
					this.model.showAlert("warning","Si le das <b>Doble clic</b>, el audio se reproducirá. (Solo en la edición)");
				}
			}
	},
	/** check audio
    */
	playAudio:function(e){
		var this_ = $(e.currentTarget),
			doc   = this_.attr("data-child-audio"),
			state_audio = this_.attr("data-state"),
			audio = document.getElementById(doc),
			Img   = this_.find(".audioPlayIcon > div img"),
			attrImg;
		if ( state_audio == "false" ){
			audio.play();
			this_.attr("data-state","true");
			attrImg = "files/pausa.svg";
		}else if ( state_audio == "true" ){
			audio.pause();
			this_.attr("data-state","false");
			attrImg = "files/play.svg";
		}
		Img.attr("src",attrImg);

	},

});
var Collage = Collage || {};

Collage.start = function(){
	var app     = new this.app();
	//Vistas
	var appView     = new this.upperSideBar({ model: app });//barra de herramientas superiror
	var leftSideBar = new this.leftSideBar({ model: app });//barra lateral izquierda
	var window_     = new this.window({ model: app });//ventana Principal

	var pluginMulti = new Collage.multimedia({model: app });//Multimedia
	var pluginIcons = new Collage.icons({ model: app });//Iconos
	var character   = new this.character({ model: app });//personajes
	var pluginText  = new this.texto({ model: app });//texto
	var pluginCss  = new this.cssPlugin({ model: app });//css
	//var capesElem  = new this.capeElements({ model: app });//elementos convertidos a capas
}
