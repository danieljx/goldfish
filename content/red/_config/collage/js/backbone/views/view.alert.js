var Collage = Collage || {};
/** The application main model
* @author Duvan Valencia
* @augments Backbone.View
* @constructor
* @name alertView
*/
Collage.alertView = Backbone.View.extend(  /** @lends alertView */  {
	el: $("#console-body"),
	events: {
		'click .remove_this':"alertClose"
	},
	template: _.template( $("#bottomAlert").html() ),
	initialize: function(){ this.render(); },
	render: function(){
		this.$el.append( this.template( this.model.toJSON() ) );
	},
	/** Remove current (or this) alert from console
    */
	alertClose: function(e){
		var thisEl = $(e.currentTarget);
		thisEl.parent().remove();
	}
})