var Collage = Collage || {};
/** The view for upper side panel
* @author Duvan Valencia
* @augments Backbone.View
* @constructor
* @name upperSideBar
*/
Collage.upperSideBar = Backbone.View.extend( /** @lends upperSideBar */ {
	events:{ 
		"click li.tool[data-tool-tip-text='Copiar']":"copy",
		"click li.tool[data-tool-tip-text='Pegar']":"paste",
		"click li.tool[data-tool-tip-text='Cortar']":"cut",
		"click li.tool[data-tool-tip-text='Eliminar']":"delete",
		"click li.tool[data-tool-tip-text='Color de fondo']":"bgColor",
		"click li.tool[data-tool-tip-text='Color de letra']":"txtColor",
		"click li.tool[data-tool-tip-text='Organizar']":"ShowSubMenu",
		"click .sub-menu-tool": "organize",
		"mouseover li.tool":"showTooltip",
		"mouseleave li.tool": "hiddeTooltip",
	},
	/** Check if exists a element copied or cuted
    */
	copyActive: false,
	/** container of html object copied or cuted
    */
	cloneEl: null,
	'el': "#upper-side-bar",
	template: _.template($("#upperSideBar").html()),
	initialize: function(){ 
		this.render();
		this.color();
	},
	render: function(){
		console.info("%c///barra superior///","font-size:14px;color:green;font-weight:bold;")
		this.$el.append(this.template( this.model.toJSON() ));
	},
	/** set the color (text and background) for icons, text and audio
    */
	color: function(){
		var window_  = this.model.get("window"),
			selectd_ = this.model.get("selected");

		$(".color-option-input").spectrum({
			preferredFormat: "hex",
			showInput: true,
			cancelText: "CERRAR",
    		chooseText: "OK",
			color: "#fff",
			move: function(color){
				var widw_active  = $("#window-edition").attr("data-selected");
				var obj_         = $( window_ ).find( selectd_ ),
					iconColor    = obj_.find("object.icon-color"),
					AudioColor   = obj_.find("object.audio-color"),
					audioBgColor = obj_.find('div.playSound'),
					txtBgColor   = obj_.find('div.text-plugin');

				var action  = $(this).attr("id"), myColor = color.toHexString();
			if ( widw_active == "true" ){
				if ( action == "bg-color" ){
					$("#window-edition").css("background-color", myColor+" !important");
				}
			}else if ( widw_active == "false" ){

				if ( obj_.length >= 1 ){
					//Inicio del color de fondo
					if ( action == "bg-color" ){
						//Audio
						if ( audioBgColor.length >= 1 ){
							audioBgColor.css({"background-color": myColor})
						} 
						else{
							obj_.css( "background-color", myColor );
						}
					}// fin del color de fondo
					//Inicio color de la letra
					if ( action == "txt-color" ){
						//iconos
						if ( iconColor.length >= 1 ){
							iconColor.attr("data-icon-color",myColor );
							var a = document.getElementById( iconColor.attr("id") ),
								svgDoc = a.contentDocument,
								svgItem = svgDoc.getElementById("icon");
								svgItem.setAttribute("fill", myColor );		
						}//fin del color de los iconos
						//ICONO DEL AUDIO
						if ( AudioColor.length >= 1) {
							AudioColor.attr("data-color",myColor);
							var a = document.getElementById( AudioColor.attr("id") ),
								svgDoc = a.contentDocument,
								svgItem = svgDoc.getElementById("icon");
								svgItem.setAttribute("fill", myColor );
						}//FIN ICONO DEL AUDIO
						//Color de la letra del plugin texto
						if ( txtBgColor.length >= 1 ){
							txtBgColor.css("color", myColor);
						}//fin
					}
				}
		}
	}//fin  widw_active == "false"
		});
	},
	/** Copy the selected element width class selected-obj
    */
	copy: function(){
		var eleSlc  = $(this.model.get("selected"));
			if ( eleSlc.length > 0 ){
				this.copyActive = true;

				var elemClone =  $( this.model.get("selected") ).clone();
					elemClone.removeClass( "selected-obj ui-draggable ui-draggable-handle ui-resizable active" );
					elemClone.attr( "id" , this.model.createId("dinamyc") );

				var object_ = elemClone.find(".set-color-svg");
				var text_   = elemClone.find(".text-plugin");
				if ( object_.length >= 1 )
					{ object_.attr("id", this.model.createId("icon")); }
				if ( text_.length >= 1)
					{	
						var id_ = this.model.createId("text");
							text_.attr("id", id_+"-plugin");
							text_.find(".editable-text-header").attr("id",id_);
							text_.find(".editable-text-input").attr("id", id_+"-editable");
							text_.find(".editable-text-input").find("input").attr("data-id-parent",id_)
					}

						this.cloneEl = elemClone;	
						this.model.showAlert("ok","Elemento Copiado");
			}else if ( eleSlc.length == 0 ){ this.model.showAlert("warning","Primero, <b>selecciona</b> un elemento");}
	},
	/** paste the copied/cutted element
    */
	paste: function(){
		if ( this.copyActive != false ){
				this.model.deselected();
				this.cloneEl.addClass( this.model.get("selected").replace(".","") );
					$(this.model.get("window")).append(this.cloneEl);
				this.copyActive = false;
				this.model.componentsWidget();
				this.model.showAlert("ok","<b>Elemento pegado</b>");
			}else{  this.model.showAlert("warning","Primero, <b>Copia/Corta</b> un elemento"); }
	},
	/** cut the selected element width class selected-obj
    */
	cut: function(){
		var eleSlc  = $(this.model.get("selected"));
			if ( eleSlc.length > 0 ){
				this.copyActive = true;

				var elemClone =  $( this.model.get("selected") ).clone();
					elemClone.removeClass( "selected-obj ui-draggable ui-draggable-handle ui-resizable active" );
					elemClone.attr( "id" , this.model.createId("dinamyc") );

				var object_ = elemClone.find(".set-color-svg");
				var text_   = elemClone.find(".text-plugin");
				if ( object_.length >= 1 )
					{ object_.attr("id", this.model.createId("icon")); }
				if ( text_.length >= 1)
					{	
						var id_ = this.model.createId("text");
							text_.attr("id", id_+"-plugin");
							text_.find(".editable-text-header").attr("id",id_);
							text_.find(".editable-text-input").attr("id", id_+"-editable");
							text_.find(".editable-text-input").find("input").attr("data-id-parent",id_)
					}
					this.cloneEl = elemClone;
				$("#"+$( this.model.get("selected") ).attr("id")).remove();
				this.model.showAlert("ok","Cortado");
			}else if ( eleSlc.length == 0 ){
				this.model.showAlert("warning","Primero, <b>selecciona</b> un elemento");
		}
	},
	/** Delete the selected element width class selected-obj
    */
	delete: function(){
		var eleSlc  = $(this.model.get("selected"));
			if ( eleSlc.length > 0 ){
				if ( confirm("¿Desea eliminar este elemento?" ) == true ){
						var elem =  $( this.model.get("selected") ).clone();
							$("#"+elem.attr("id")).remove();
						this.model.showAlert("ok","Eliminado con exito");
					}
			}else{
				this.model.showAlert("warning","No hay Elementos <b>seleccionados</b>");
			}
	},
	/** Show the menu for organize elements
    */
	ShowSubMenu: function(e){
			e.stopPropagation();
		var this_ = $(e.currentTarget),
			classHas = this_.hasClass("data-submenu-inactive"),
			remIcon, addIcon;
		if ( classHas == true ){
			 this_.removeClass("data-submenu-inactive");
			 this_.addClass("thisclosed");
			 this_.find(".sub-menu-upper").removeClass( this.model.get("inactive") );
			 remIcon = "glyphicon-indent-left";
			 addIcon = "glyphicon-remove";
		}else if ( classHas == false ){
			this_.addClass("data-submenu-inactive");
			this_.removeClass("thisclosed");
			remIcon = "glyphicon-remove";
			addIcon = "glyphicon-indent-left";
			$(".sub-menu-upper").addClass( this.model.get("inactive") );
		}	
		this_.find("span.glyphicon")
			 .removeClass(remIcon)
			 .addClass(addIcon);
	},
	/** organize the seleted elements
    */
	organize: function(e){
		var this_ = $(e.currentTarget),
		 	eleSlc  = $(this.model.get("selected")),
		 	IndexGlobal = this.model.getZindex(),
		 	action_ = this_.attr("data-action");

			if ( eleSlc.length > 0 ){
				var Index_ = parseInt ( this.model.getZindex() );
				//Subir z index
				if ( action_ == "toUp" ){
					Index_ += 1;
					this.model.setZindex(Index_ );} 
				//Bajar z-index
				else if ( action_ == "toDown" ) {
					if (Index_ != 0){ Index_ -= 1; }
				}

				eleSlc.css("z-index",Index_);
			}else{
				this.model.showAlert("warning","No hay elementos seleccionados");
			}	
	},
	/** Show the tooltip for each tool
    */
	showTooltip: function(e){
		var infoAct = $(e.target).attr("data-tool-tip-text");
		if ( infoAct ) {
			var x = event.clientX,  y = event.clientY;
			var toolTip 	= $(".tooltip");
				toolTip.removeClass( this.model.get("inactive") );
				toolTip.text(infoAct);
				toolTip.css({"left":x+"px"});
		}
	},
	/** Hide the tooltip for each tool
    */
	hiddeTooltip: function(e){
		var toolTip 	= $(".tooltip");
			toolTip.addClass(this.model.get("inactive"));
	}

});

