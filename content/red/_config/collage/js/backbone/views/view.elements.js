var Collage = Collage || {};

Collage.capeElements = Backbone.View.extend({
	"el":"#gf-ecm-config-lateral-menu",
	template: _.template( $("#capesElements").html() ),
	initialize: function(){
		this.model.getElements();
		console.warn("%c¡Plugin capes, Cargado!","font-size:12px;font-weight:bold;color:#9933ff;");
		this.render();
	},
	events:{
		"click li.swicthVisibility" :"swicthVisibility",
		"click li.delete-cape":"deleteMe"
	},
	deleteMe: function(e){
		var this_ = $(e.currentTarget),
			idfp  = this_.attr("data-ref-id");

		var dropTHis = confirm("Al eliminar la capa, también eliminas el elemento. ¿Estás de acuerdo?");
			if ( dropTHis == true ){
				$(".main-cape[data-id='"+idfp+"']").remove();
				$(".element-dinamyc#"+idfp).remove();
				this.model.alert("ok","¡La capa se ha elimindo!");
			}
	},
	swicthVisibility: function(e){
		console.warn("info hello!")
		var this_ = $(e.currentTarget),
			idfp  = this_.attr("data-ref-id"),
			state = this_.attr("data-visible"),
			statePadre, rmSpan, addSpan,  isVisible;
		this_.add("visiblekhjsd")	
	
		
		if ( state == "false" ){
			this_.attr("data-visible", "true");
				rmSpan  = "glyphicon-eye-close inactive";
				addSpan = "glyphicon-eye-open active-color";
			statePadre = "true"; isVisible = "true";
			
		}else if ( state == "true" ){
			this_.attr("data-visible", "false");
			statePadre = "false"; isVisible = "false";
			addSpan  = "glyphicon-eye-close inactive";
			rmSpan   = "glyphicon-eye-open active-color";
		}
		
		this_.find("span.glyphicon").addClass(addSpan).removeClass(rmSpan);
		$(".element-dinamyc#"+idfp).attr("data-visibility-state", isVisible);
		$(".main-cape[data-id='"+idfp+"']").attr("data-visible", statePadre);
	},
	render: function(){
		this.$el.append( this.template( this.model.toJSON() ));
	}
});