<?php
	set_include_path('../../../library');
	require_once("Zend/Loader.php");
	Zend_Loader::loadClass('Zend_Config_Xml');
	Zend_Loader::loadClass('Zend_Registry');
	Zend_Loader::loadClass('Zend_Db');
	require_once("Local/config.load.php");
	$configXml = getConfig();

	$DB_CONFIG = new Zend_Config_Xml($configXml, 'database');
	$VERSION = new Zend_Config_Xml($configXml, 'version');
	$_PATH = new Zend_Config_Xml($configXml, 'path');

	$dataRoot = "../../../../".$_PATH->sys->datafolder . "/uploads/";

	$allowed = array('png', 'jpg', 'gif', 'svg');
    if(isset($_FILES['file']) && $_FILES['file']['error'] == 0){

	    $extension = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
	    if(!in_array(strtolower($extension), $allowed)){
	    echo '{"status":"error"}';
	    exit;
    }
     if(move_uploaded_file($_FILES['file']['tmp_name'],$dataRoot.$_FILES['file']['name'])){
	    $tmp = $dataRoot.$_FILES['file']['name'];
	    $result = array('path' => $tmp,
	    				'basePath' => $dataRoot,
	    				'filename' => $_FILES['file']['name']
	    				);
	    echo json_encode($result);
     exit;
    }
    }
     echo '{"status":"error"}';
     exit;