function events(){
	var myself = this;
   this.current_Upload = '';

   this.colorPicker = function(){
   	console.warn("team color");
  	 	var clickActive;

		$('.color-option').click(function(){
			clickActive = $(this).attr('data-type');
			alert("hello!")
		});

		$('.color-option').ColorPicker({
			onChange: function (hsb, hex, rgb) {
				console.warn( "#"+hex );
				
			}
		});
   }//end colorPicker

   this.uploadImage = function(){
   	/*global window, $ */
		$(function () {
		    'use strict';
		    // Change this to the location of your server-side upload handler:
		    var url = '../../jquery-file-upload-handler.php';
		 
		    $('.char-upload').fileupload({
		    	dropZone: $(this),
		        url: url,
		        dataType: 'json',
		        autoUpload: true,
		        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
                maxFileSize: 1 * 1000000, // 1 Mb
		        done: function (e, data) {
		        	var elem_ = $(this);
		        	var file_ = data.result.files[0];
		        	if( myself.current_Upload != 'bgMain' ){
		        			$.each(data.result.files, function (index, file) {
								elem_
								.parent().parent().parent().siblings().find("div#bgImgCnt")
								.css("background-image","url("+file_.url+")");
							});
		        	}
		        	else if( myself.current_Upload == 'bgMain' ){
		        			$.each(data.result.files, function (index, file) {
			        				elem_
									.parent().parent().parent().siblings()
									.find("div").find("img")
									.attr("src", file_.url);
		        				});
		        	}
				},
		      	progressall: function (e, data) {
					var progress = parseInt(data.loaded / data.total * 100, 10);
					$(this).parent().siblings()
					.find('.progress-bar')
					.css('width', progress + '%');
				}
		    })
		    .on('fileuploadfail', function(e, data) {
					alert("FALLO!!")
			})
            .prop('disabled', !$.support.fileInput)
		    .parent().addClass($.support.fileInput ? undefined : 'disabled');
		});
   }//uploadImage

   this.uploadAudio = function(){
   		/*Subir archivos*/
		$(function () {
			'use strict';
			// Change this to the location of your server-side upload handler:
			var url = '../../jquery-file-upload-handler.php';
			$('.upload_sound').fileupload({
					dropZone: $(this),
					url: url,
					dataType: 'json',
			        autoUpload: true,
			        acceptFileTypes: /(\.|\/)(mp3|ogg|wav|wma)$/i,
			        maxFileSize: 50 * 1000000, // 5 Mb
				done: function (e, data) {
					$.each(data.result.files, function (index, file) {
				 });
				},
				
				progressall: function (e, data) {
					var progress = parseInt(data.loaded / data.total * 100, 10);
					        $('#progress_audio .progress-bar').css( 'width', progress + '%' );
					        }
					})
					.on('fileuploadfail', function(e, data) {
								alert("FALLO!!")
					})
					.on('fileuploaddone', function(e, data) {
						var file = data.result.files[0];
							
					$.ajax({
						    type: "POST",
						    url: "process_upload.php",
						    data: {file_name: file.name, file_path: data.result.physPath},
						    dataType: 'json',
						error : function(error){
						    alert(error);
						},
						success: function(result) {
							$("#saveMsj")
							.attr("data-audio-state","true")
							.attr("data-audio-name", result.path_html)
							.attr('data-folder-file', 'uploads');
							   }
						});
						})
			            .prop('disabled', !$.support.fileInput)
					    .parent().addClass($.support.fileInput ? undefined : 'disabled');
					});
   }//uploadAudio

}//end events

var events_ = new events();
	events_.colorPicker()
	events_.uploadImage();