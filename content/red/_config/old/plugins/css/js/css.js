function cssPlugin(){
	var cssSelf = this;
	this.uiFunctions = function(){
	//Click en el menu de las opciones
		$("body")
			.on("click", ".gf-ecm-edition-circle-tools ul li[data-list-child]", function(){
			$(".gf-emc-plugin-css-form").addClass( ecm.classHiddenElem );
				var attrNm = $(this).attr("data-list-child");
				var width_, height_;
				if( attrNm == 'rotateCSSPlugin' ){
					width_ = 320, height_ = 260;
				}
				 if( attrNm == "PaddingCSSplugin" ){
					width_ = 320, height_ = 255;
				}
				 if( attrNm == "borderCSSplugin" ){
					width_ = 320, height_ = 340;
				}
				 if ( attrNm == "TextCSSplugin" ){
					width_ = 320, height_ = 310;
				}
				 if( attrNm ==  "BgColorCSSplugin" ){
					width_ = 320, height_ = 107;
				}
				$("#"+attrNm).removeClass( ecm.classHiddenElem );
				ecm.sizeEditionPanel( width_ , height_);
		});
	//click para guardar los cambios
		$("body").on("click",".gf-ecm-form-outline-buttons button", function(){
			var ElToStylize = $(".gf-elemen-this-selected");
			var typeStyle = $(this).attr("data-save");
				
			switch (typeStyle){
				case 'texto':
					ElToStylize.css({
						"font-family":$("#font-family-css").val(),
						"font-size": $("#size-text-css").val()+"px"
					});
					ElToStylize.find(".gf-ecm-text-show-header").css("font-size", $("#size-text-css").val()+"px");
				break;

				case 'border': 
					var Cborder = $("b.valBrColor").text();
					var Wborder = parseInt($("#border-width-css").val());
					var Rborder = parseInt($("#border-radius-css").val());
					
					ElToStylize
						.find(".gf-dinamyc-body div:nth-child(1)")
						.css({ "border-color":Cborder, "border-width":Wborder, "border-radius":Rborder })
				break;

				case 'padding': 
					var padding = $("#css-padding-value").val();
						ElToStylize.find(".gf-dinamyc-body div:nth-child(1)").css("padding", padding+"px");
				break;

				case 'rotate': 
				var rot = $("#css-rotate-grades").val();
					ElToStylize.css("transform","rotate("+rot+"deg)")
				break;
			}
		})
	}
}

var cssClass = new cssPlugin();
	cssClass.uiFunctions();

	$(window).ready(function(){
		console.warn("asdasd");
		setTimeout(function(){
			$(".customer").spectrum({
				color: "#fff",
				showAlpha: true,
				showButtons: false,
					move: function(color) {
					    var val = color.toHexString(); 
					    //console.info( color.toHexString() );
					    $(".valBrColor").html(val);
					}
			});	
		},2000)
		
	});