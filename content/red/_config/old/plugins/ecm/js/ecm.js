/*HACER LOS PLUGINS INDEPENDIENTES DE ESTA CLASE PADRE*/
/*VERIFICAR .offset() PARA LA POSICIÓN DE LA VENTANA*/
/*TRABAJAR CON vh y vw, CONTENIDO QUE SE ESCALE, TEXTO, ELEMENTOS*/
function rack(){
	/*4 JSON CON XML*/
	var RackSelft = this;
		this.classForSelect  = "gf-elemen-this-selected"; 
		this.idWindowEdition = "gf-ecm-render-window";
		this.classHiddenElem = "gf-ecm-element-inactive";
		this.CreateDynamicEl = "gf-dinamyc-element";
		this.PluginsUrls = 'plugins';
		this.avalaibleFonts = ["Verdana","Arial","Helvetica","Comic sans"];
		this.heightPanelEdition = 0;

		this.plugins =[ 
			/*{ name: 'rack', width: 330, height:107,
			  icon:'glyphicon-th', title: 'Cuadricula' },*/
			{ name: 'multimedia',  width: 250,
			  icon: 'glyphicon-film', title: 'Multimedia'},
			{ name: 'css',  width: 320,
			  icon: 'glyphicon-cog', title: 'Css'},
			{ name: 'texto',  width: 350,
			  icon: 'glyphicon-font', title: 'Texto'},
			{ name: 'iconos',  width: 410,
			  icon: 'glyphicon-tags', title: 'Iconos'},
			/*{ name: 'responsive',  width: 250, height:300,
			  icon: 'glyphicon-phone', title: 'Responsive'}*/
		];
		//Coloca las fuentes disponobles en su sitio adecuado
		this.setFonts = function(){
			for (var i = 0; i <= RackSelft.avalaibleFonts.length -1; i++) {
				var font_cnt = $("<div/>");
					font_cnt.addClass("font");
					font_cnt.attr("data-font",RackSelft.avalaibleFonts[i]);
					font_cnt.css("font-family", RackSelft.avalaibleFonts[i])					
					font_cnt.text(RackSelft.avalaibleFonts[i]);
				$(".available-fonts").append(font_cnt);
			}
		}//fin setFonts
		//contenedor de un svg
		this.tmpSvg = function(file){
			var obj = $("<object/>");
				obj.attr("id", RackSelft.renderId('svg'));
				obj.addClass("gf-object-svg");
				obj.attr("type","image/svg+xml");
				obj.attr("data",file);
			return obj;
		}//fin tmpSvg
		//Alertas a la consola para el usuario

	//Elementos arrastrables
	this.draggableDinamyc = function(){
		/*$(".gf-dinamyc-element-draggable")
		.draggable({
			stop:
			function(event, ui){
				var dragElem = $(ui.helper[0]).clone();
				if ( $(ui.helper[0]).hasClass("locked") == false){
					
					if ( dragElem.hasClass("gallery-icon") == true && dragElem.attr("data-dinamyc") == 'true'){
						var attr_file = dragElem.find("img").attr("src");
						var svgTmp 	  = RackSelft.tmpSvg(attr_file);
						//var left_     = $(ui.helper[0]).css("left");
						//var top_      = $(ui.helper[0]).css("top");

						var Tmp = RackSelft.TemplateDynamic(svgTmp);
							Tmp.css({"left":"50%", "top":"25%"});

						$("#"+RackSelft.idWindowEdition).append(Tmp);
						$(ui.helper[0]).css({ "left":"auto",top:"auto"});
						console.info("archivos svg");
					}

					if ( dragElem.hasClass("gallery-img") == true && dragElem.attr("data-dinamyc") == 'true'){
						var img_drag = dragElem.find("img");
							img_drag.addClass("gf-object-image");
							img_drag.css({"width":"200px","height":"200px"});

						//var left_ = $(ui.helper[0]).css("left");
						//var top_  = $(ui.helper[0]).css("top");

						var Tmp = RackSelft.TemplateDynamic(img_drag);
							Tmp.css({"left":"50%", "top":"10%"});
							Tmp.css({"width":"220px","height":"220px"});

						$("#"+RackSelft.idWindowEdition).append(Tmp);
						$(ui.helper[0]).css({ "left":"auto",top:"auto"});
						console.info("archivos img");
					}

					setTimeout(function(){
						 RackSelft.draggableDinamyc();
					},1000);

				}else{
					$("#"+$(ui.helper[0]).attr("id")).draggable("destroy");
					 console.warn("este elemento esta bloqueado!!");
				}
			}
		})*/
		/*$("#gf-ecm-render-window .gf-dinamyc-element-draggable").resizable({
			resize:function(event,ui){
				if ( $(ui.helper[0]).hasClass("locked") == false){
					var idResizer = $("#"+$(ui.element[0]).attr("id"));
						idResizer.addClass("activeBorder");
					if ( idResizer.find("img").length == 1 ){
						 idResizer.find("img").css({
						 	width: idResizer.width()+"px",
						 	height:idResizer.height()+"px"
						});	
					}//end if
					if ( idResizer.find("div.playSound").length == 1){
							idResizer.find("div.playSound").css({
							 	width: idResizer.width()+"px",
							 	height:idResizer.height()+"px"
							})
							idResizer.find("div.playSound")
								.find("div.glyphicon")
								.css({"height": idResizer.height()+"px"})
					}//end if
				}else{
					$("#"+$(ui.helper[0]).attr("id")).resizable( "destroy" );
				}
			},
			stop: function(event,ui){
				var idResizer = $("#"+$(ui.element[0]).attr("id"));
					idResizer.removeClass("activeBorder");	
			}
		});*/
	}//fin draggableDinamyc
	//calculamos que equivale un porcentaje a pixeles
	this.PercentageEquivalent = function( baseNum, baseNumParent ){
		var equivalent = (baseNum / baseNumParent)*100;
			return equivalent;
	}
	
	this.init = function(){
		var Plugins = RackSelft.plugins;
			RackSelft.uifunctions();
		for ( var i = 0; i <= Plugins.length - 1; i++ ){ RackSelft.importTempMustache( Plugins[i] ); }
		$(document).ready(function(){
			$("#gf-ecm-render-window").css("height", ($(window).outerHeight()-250)+"px");
		});
			RackSelft.setFonts();
	}
	//Tamaño del panel de las herramientas
	this.sizeEditionPanel = function(width, height ){
		lateralMenu = $("#gf-ecm-body-lateral-menu");
		lateralMenu.css({ "width":  width+"px", "height": "100%" });
	}
	//Panel de laS HERRAMIENTAS del panel izquierdo
	/*this.importTempMustache = function(plugin){ 
		var liLeftMenu = $("<li/>");
			liLeftMenu.addClass('gf-ecm-tool');
			liLeftMenu.attr('data-width-edition-panel', plugin.width);
			liLeftMenu.attr('data-height-edition-panel', plugin.height);
			liLeftMenu.attr('data-name-temp-edition', plugin.name);

		var parentDiv = $("<div/>");
			parentDiv.addClass("gf-ecm-tools-left");

		var divIcon = $("<div/>");
			divIcon.addClass("gf-ecm-tools-icon");

		var spanIcon = $("<span/>");
			spanIcon.addClass("glyphicon").addClass(plugin.icon);

		var header_ = $("<header/>");
			header_.addClass("gf-ecm-tools-title");
			header_.html("<h6>"+ plugin.title +"</h6>");

			divIcon.append(spanIcon);
			parentDiv.append(divIcon);
			parentDiv.append(header_);
			liLeftMenu.append(parentDiv);

			$("#gf-plugins-tools").append(liLeftMenu);
				$.get( RackSelft.PluginsUrls+'/'+plugin.name+'/tpl/'+plugin.name+'.mustache' , function(templatesFile) {
					var template = templatesFile;
					var viewParam = [];
					var output = Mustache.render( $(template).filter("#"+plugin.name+'Base').html(), viewParam );
					$( "#gf-ecm-config-lateral-menu").append(output);
				});
	}*/

	this.addRack = function(){
		var hParent    = $("#gf-ecm-render-window").outerHeight();
		var rack_      = $("#ecm-rack-element-main").clone();
		var template   = RackSelft.TemplateDynamic(rack_);
		
		if ( $("#gf-ecm-body-edition .gf-dinamyc-element").length < 1 ){
			$("#gf-ecm-render-window").append( template );
			$("#"+template.attr("id")).find(" .ecm-rack-element-main").css("height",hParent+"px");
		}
	}
	
	/*//Renderizar un nuevo Id
	this.renderId = function(name){
		var id_   = new Date(),
			idEnd = id_.getTime();
		return idEnd+'-'+name;
	}*/
	/*Template para añadir al docuemnto*/
	/*this.TemplateDynamic = function(rack_){
		var this_id = RackSelft.renderId('dynamic');
		var section_ = $('<section/>');
		    section_.addClass( "gf-dinamyc-element gf-dinamyc-element-draggable" );
		    section_.attr("id",this_id);
		var div_circle = $("<div/>");
			div_circle.attr("class","gf-circle-edit-dinamyc-element");
		var span_ = $("<span/>");
			span_.attr("class","glyphicon glyphicon-pushpin");
		var div_body = $("<div/>");
			div_body.attr("class","gf-dinamyc-body");
			div_body.append(rack_.attr("id", this_id));
			div_circle.append( span_ );
			section_.append( div_body );
		return section_;
	}*/
	//Eventos
	this.uifunctions = function(){
		//Remover la consola
		/*
		$("body").on("click",".gf-console-body .alert .remove_this", function(){
			console.info("eliminar");
			$(this).parent().remove();
		})*/


		$("body").on("click","div.class-char-svg .class-char-svg-btns button", function(){
			$(".btn-show-styles").find("span").removeClass("glyphicon-chevron-left").addClass("glyphicon-chevron-right");
			$(".svg-styles-all").addClass("gf-ecm-element-inactive");
			var attr_id = $(this).attr("data-id-child");
				
				if ( attr_id ){
					var attSpan = $(this).find("span");
					console.warn( $("#"+attr_id).hasClass("gf-ecm-element-inactive") );
					if ( $("#"+attr_id).hasClass("gf-ecm-element-inactive") == true){
						$("#"+attr_id).removeClass("gf-ecm-element-inactive");
						attSpan.removeClass("glyphicon-chevron-right").addClass("glyphicon-chevron-left");
					}else{
						$("#"+attr_id).addClass("gf-ecm-element-inactive");
						attSpan.addClass("glyphicon-chevron-right").removeClass("glyphicon-chevron-left");	
					}
				}
		})

		//??????????
		$("body").on("blur","input.value-style-svg", function(){
			$(".title-style-svg").removeClass("active-edition");
		});
		$("body").on("click","label.title-style-svg", function(){
			console.warn("aksldhjklas");
			var attrAction = $(this).attr("data-edition-style");
			console.warn( attrAction );
			if (attrAction){
				switch( attrAction){
					case "attr":
					console.info("Ahora lo movemos");
						$(".this-svg-style").removeClass('current-edition')
						$(".title-style-svg").removeClass("active-edition");
						//$("input.value-style-svg").css("opacity",0);
						$(this).addClass("active-edition");
						$(this).parent(".this-svg-style").addClass("current-edition");
						$(this).siblings("input.value-style-svg").focus();
					break;

					case "fill":

					break;
				}
			}
		})
		
		/*INICIO EDICIÓN DE ELEMENTOS
		var act = this; this.clone;
		$("body").on("click", ".gf-ecm-tools-window nav ul li, .sub-menu-click-right ul li", function(){
			var attr_edition = $(this).attr("data-tool-tip-text");
			var toolsMenu = $(".gf-ecm-tools-window nav ul li.block");
			if ( attr_edition == "Pegar" ){
				if ( act.clone != undefined ){
					$("."+RackSelft.classForSelect).each(function(){
						$(this).removeClass( RackSelft.classForSelect );
					});
					act.clone.addClass(RackSelft.classForSelect);
					act.clone.css({"top":"0px", left:"0px"});
					$("#"+RackSelft.idWindowEdition).append(act.clone);
					RackSelft.draggableDinamyc();
				}	
			}

			//si existe algun elemento seleccinado
			if ( $("."+RackSelft.classForSelect).length == 1 ){
				if ( attr_edition == "Copiar" ){
					var elemClone =  $("."+RackSelft.classForSelect).clone();
						elemClone.attr("id",RackSelft.renderId("dinamyc"));
						act.clone = elemClone;							
				}
				if ( attr_edition == "Cortar" ){
					var elemClone =  $("."+RackSelft.classForSelect).clone();
						elemClone.attr("id",RackSelft.renderId("dinamyc"));
						act.clone = elemClone;
					$("#"+$("."+RackSelft.classForSelect).attr("id")).remove();
				}
				if ( attr_edition == "Eliminar"){
					if ( confirm("¿Desea eliminar este elemento?") == true ){
						var elemClone =  $("."+RackSelft.classForSelect).clone();
						$("#"+elemClone.attr("id")).remove();
					}
				}
				
			}//FIN DEL IF	
			else{
				RackSelft.alertToConsole("warning","No hay elementos seleccionados");
			}
			$("#main-contextmenu").addClass("gf-ecm-element-inactive");
		})/*FIN DE EDICIÓN DE DOCUMENTOS*/
		//Click en la ventana principal de edición
		$("body").on("click","#"+RackSelft.idWindowEdition, function(event){
			if ( $(event.target).attr('id') == RackSelft.idWindowEdition) {
				$(".subMenuTopTools").addClass("gf-ecm-element-inactive");
				var toolsMenu = $(".gf-ecm-tools-window nav ul li.block");
				$(".gf-dinamyc-element").removeClass(RackSelft.classForSelect);
				if ( $(".gf-dinamyc-element").hasClass("locked") ){
					toolsMenu.attr("data-tool-tip-text","Desbloquear");
					toolsMenu.find("span").removeClass("glyphicon-star-empty").addClass("glyphicon-star");
				}else{
					toolsMenu.attr("data-tool-tip-text","Bloquear");
					toolsMenu.find("span").removeClass("glyphicon-star").addClass("glyphicon-star-empty");
				}
			}
		});
		//Click al elemento creado dinamicamente agregando la clase seleccionada
		$("body").on("click", ".gf-dinamyc-element", function(){
			$(".gf-dinamyc-element").removeClass("gf-elemen-this-selected");
			$(this).addClass("gf-elemen-this-selected");

			var fntFami = $(this).css("font-family");
			var fntSize = $(this).css("font-size");
			var tr = $(this).css("transform");//Atrapa el css rotate
			var padg = parseInt($(this).find(".gf-dinamyc-body div:nth-child(1)").css("padding"));
			var Cborder = $(this).find(".gf-dinamyc-body div:nth-child(1)").css("border-color");
			var Wborder = parseInt($(this).find(".gf-dinamyc-body div:nth-child(1)").css("border-width"));
			var Rborder = parseInt($(this).find(".gf-dinamyc-body div:nth-child(1)").css("border-radius"));
			/*
				Cuando le damos click a un elemento y encuentra que es un texto, 
				guardamos el estilo de la fuente al plugin css
			*/
			//fuente
			if ( fntFami != '"Helvetica Neue", Helvetica, Arial, sans-serif' && fntSize != '14px' ){
				$("#font-family-css").val( fntFami );
				$("#font-family-css-panel").find(".text").html( fntFami );
				$("#size-text-css").val( parseInt(fntSize) );
			}
			//Grados rotación
			if ( tr != "none" ){
				var values = tr.split('(')[1],
			   	 	values = values.split(')')[0],
			   	 	values = values.split(',');
				var b = values[1];
				var angle = Math.round(Math.asin(b) * (180/Math.PI));
				$("#css-rotate-grades").val(angle);
			}else if (tr == "none"){ $("#css-rotate-grades").val(0); }
			if ( padg != undefined ){
					$("#css-padding-value").val(padg);
			}
			if ( Rborder != undefined && Wborder != undefined) {
				$("#border-radius-css").val(Rborder);
				$("#border-width-css").val(Wborder);
			}
			
			
		});//fin click elemento creado dinamicament
		
		//Mover el elemento seleccionado al frente
		$("body").on("click","#organize-elements .font", function(){
			var selecteTrue = $("#gf-ecm-render-window .gf-elemen-this-selected");
			if ( selecteTrue.length >= 1 ) {
				var zIndexCurrent = $("#gf-ecm-render-window").attr("data-global-zindex");
				var zIndexNumber  = parseInt(zIndexCurrent), thisZindex;

				console.warn(zIndexCurrent);
				console.warn(zIndexNumber);
				//Aumentar z-index
				if ( $(this).attr("data-action") == "toUp" ){ thisZindex    = zIndexNumber++; }
				//Disminuir z-index
				if (  $(this).attr("data-action") == "toDown" ){ thisZindex    = zIndexNumber--;}
				selecteTrue.css("z-index",thisZindex);
				console.info(thisZindex);
				$("#gf-ecm-render-window").attr("data-global-zindex",zIndexNumber)
			}
		})
		//Pasar por encima de las herramientas del panel principal(copiar/pegar/pegar)
		/*$("body").on("mouseover",'header.gf-ecm-tools-window nav ul li.gf-ecm-tool', function(event){
			
			if ( $(event.target).attr("data-tool-tip-text")){
				var x = event.clientX;  
				var y = event.clientY; 
				var toolTip 	= $(".gf-ecm-tools-tooltip");
				var textToolTip = $(this).attr("data-tool-tip-text");	
					if ( $(this).attr("data-tool-tip-text") ){
						toolTip.removeClass("gf-ecm-element-inactive");
						toolTip.text(textToolTip);
						toolTip.css({"left":x+"px"})
					}
			}
			
		});*/
		/*$("body").on("mouseleave", 'header.gf-ecm-tools-window nav ul li.gf-ecm-tool' , function(){
			var toolTip 	= $(".gf-ecm-tools-tooltip");
				toolTip.addClass("gf-ecm-element-inactive");
		})*/
		//click sobre las herramientas principales (Barra superior)
		/*$("body").on("click",'header.gf-ecm-tools-window nav ul li.dropDownGf', function(){
			var child_ = $("#"+$(this).attr("data-child"));
				$(".subMenuTopTools").addClass("gf-ecm-element-inactive");
				child_.removeClass("gf-ecm-element-inactive");
		});
		
		/*$("body").on("contextmenu",".gf-dinamyc-element", function(event){
			event.preventDefault();
			$("#main-contextmenu").removeClass("gf-ecm-element-inactive");
			var topClient = event.clientY, leftClient = event.clientX;
			$("#main-contextmenu").css({"left": (leftClient-100)+"px","top":(topClient-100)+"px"})
		})*/
		//Cerrar el panel principal
		$("body").on("click","#close-main-edition", function(){
			console.info( "panel: " + RackSelft.heightPanelEdition );
			RackSelft.heightPanelEdition = $("#gf-ecm-body-lateral-menu").outerHeight();
			$("#gf-ecm-body-lateral-menu").addClass("gf-ecm-element-inactive");
			$("#rackSizeXY").addClass("gf-ecm-element-inactive");
		})
		/*//MOSTRAR HERRAMIENTAS DE EDICIÓN DE LA BARRA IZQUIERDA
		$("body").on("click",".gf-ecm-left-menu ul li[data-name-temp-edition]", function(){
			var toolsEdition  = $(".gf-multimedia-global-container"),
				itemsMenuLeft = $(".gf-ecm-left-menu ul li[data-name-temp-edition]"),
				currentTool   = $(this).attr("data-name-temp-edition");

			var attr_data   = $(this).attr("data-show-panel"), boxEdition,
			 	attr_width  = $(this).attr("data-width-edition-panel"),
			 	lateralMenu = $("#gf-ecm-body-lateral-menu");

			 	lateralMenu.removeClass("gf-ecm-element-inactive");
				RackSelft.sizeEditionPanel( attr_width);
					
				itemsMenuLeft.removeClass('gf-ecm-active');
				$(this).addClass('gf-ecm-active');

				toolsEdition.addClass("gf-ecm-element-inactive");
				$("#plugin-"+currentTool).removeClass("gf-ecm-element-inactive");
		});//FIN

			$("body").on("click", "#gfEcmAddRack", function(){
				RackSelft.addRack();
			});*/
		}
} 

var ecm = new rack();
	ecm.init();

$(window).resize(function(){
	console.info($("body").outerHeight());
	$(".gf-ecm-render-window").height();
})