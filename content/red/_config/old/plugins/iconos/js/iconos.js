/*RECUERDA EL FILTRO POR ESTADOS DE ANIMOS PARA LOS PERSONAJES*/
/*CREAR FILTROS PARA LAS IMAGENES, POR TAMAÑO ESTUDIAR POSIBILIDAD DE UN RANGO()*/

	/*

	$scss = new scssc();

		$scss->setFormatter('scss_formatter_compressed');
		// add_log('themesass:('.$theme->sass.')');
		// add_log('blocksass:('.$block->sass.')');
		$css = $scss->compile($theme->sass.' '.$block->sass);

	Objetivos fijos.
	Donde se vuelva mas complejo crear submodelos.

	*/
function iconsP(){
	var iconSelf = this;
	this.init = function(){
		iconSelf.uiFunctions();
		//iconSelf.snapFunctionLoad();
		setTimeout(function(){
			iconSelf.putResources();
		},1000)
		//console.warn( resources.getToolsCharacter() ); 
	}

	//Nueva clase
	this.resources = function(url){
	
		//Personajes
		this.getCharacters = function(){
			var charSelft = this;
				this.customsResources;
			$.ajax({
				url: 'plugins/iconos/json/personajes.json',
				type:"get",
				async:false,
				success: function(response) {
					charSelft.customsResources = response;
				},
				error : function(error){
					console.info(error)
				}
			});
			return charSelft.customsResources;
		}
	}
	/*FUNCIONES PARA EL SVG*/
	/*var dropSvg = function(){
			$("#character").find("g.svg-selected").each(
				function(){ 
					$(this).removeClass("svg-selected");
					$(this).attr("class","");
			});
			$(this.node).attr("class","svg-selected");
		}

	var move = function(dx, dy){
			dropSvg()
			this.attr({
				transform: this.data('origTransform') + (this.data('origTransform') ? "T": "t")+ [dx, dy]
			})
		}
	var start = function(){
			this.data('origTransform', this.transform().local )
		}
	var stop = function(){
			dropSvg();
			$(".gf-incons-save-btn button.dropGroupSvg").attr("data-drop-id");
		}*/
	var nextFrame = function(el, arrayFrame, cualFrame){
	     	if ( cualFrame >= arrayFrame.length ){ return };
	     	el.animate( arrayFrame[cualFrame].animation, arrayFrame[cualFrame].dur, nextFrame.bind(null, el, arrayFrame, cualFrame+1) )
	     }
	/************/

	this.snapFunctionLoad = function(){
		console.info("%c  Funciones de snap","font-size:14px;background-color:orange;color:#fff;padding:2px;");
		
		var parpadeo = [
			/*{	animation: { transform: 'r360,150,150' }, dur: 1000 },
	        {   animation: { transform: 't100,-100s2,3' }, dur: 1000 },
	        {   animation: { transform: 't100,100' }, dur: 1000 },
	        {   animation: { transform: 's0,1' }, dur: 1000 },*/
	        {   animation: { transform: 's1,0' }, dur: 500 },
	        {   animation: { transform: 's1,1' }, dur: 500 },
	        //{   animation: { transform: 'r20,200,200' }, dur: 1000 },
	        //{   animation: { transform: 'r0,90,100' }, dur: 1000 },
	        //{   animation: { transform: 'r-20,200,200' }, dur: 1000 },
	        //{   animation: { transform: 'r0,90,100' }, dur: 1000 }
	        ];

	    var talking = [
	    		{   animation: { transform: 's0.2,0.5' }, dur: 500 },
	        	{   animation: { transform: 's0.3,0.2' }, dur: 500 },
	        	{   animation: { transform: 's0.6,0.7' }, dur: 500 },
	        	//{   animation: { transform: 't4,4' }, dur: 500 },
	        	{   animation: { transform: 's0.2,0.3' }, dur: 500 },
	        	//{   animation: { transform: 't6,4' }, dur: 500 },
	        	{   animation: { transform: 's0.3,0' }, dur: 500 },
	        	{   animation: { transform: 's1,1' }, dur: 500 },
	    	]
		var personaje  = Snap.select("#character");

		var cabello = personaje.select("#hair");
		var cabeza  = personaje.select("#faces")
		
		var cejas   = personaje.select("#eyebrows")
		var cejaDer = cejas.select("#eyebrowsRight")
		var cejaIzq = cejas.select("#eyebrowsLeft")

		var ojos    = personaje.select("#eyes")
		var ojosDer = ojos.select("#eyeRight")
		var ojosIzq = ojos.select("#eyeLeft")

		var nariz   = personaje.select("#nose");

		var boca   = personaje.select("#mouths");

		var lentes  = personaje.select("#lentes");
		var bigote  = personaje.select("#bigote");
		var gorra  = personaje.select("#cap");

		
		cabello.click(dropSvg)
		ojos.click(dropSvg);
		gorra.click(dropSvg);
		ojos.click(function(){
			nextFrame(ojos, parpadeo, 0);
		})
		cabeza.click(dropSvg)
		cejaDer.click(dropSvg)
		cejaIzq.click(dropSvg)
		ojosDer.click(dropSvg)
		ojosIzq.click(dropSvg)
		boca.click(function(){
			nextFrame(boca, talking, 0);
		})
		boca.click(dropSvg);
		nariz.click(dropSvg)
		lentes.click(dropSvg)
		bigote.click(dropSvg);

		//Arrastrar
		gorra.drag( move, start, stop);
		bigote.drag( move, start, stop);
		lentes.drag( move, start, stop);
		ojosDer.drag( move, start, stop);
		ojosIzq.drag( move, start, stop);
		boca.drag( move, start, stop);
		nariz.drag( move, start, stop);
		cejaDer.drag( move, start, stop);
		cejaIzq.drag( move, start, stop);
		cabello.drag( move, start, stop);
		cabeza.drag( move, start, stop);
	}
	

 this.uiFunctions = function(){

		$("body").on("click","#char-menu-contextmenu ul li", function(){
			var attr_act = $(this).attr("data-action");
				if ( attr_act == "custom" ){
					var imag_obj = $(".gf-elemen-this-selected").find("img");
					var object_tag  = $("<object/>");
						object_tag.attr("id","character");
						object_tag.attr("data",imag_obj.attr("src"));
						object_tag.attr("type","image/svg+xml");
					$("#character").html("");

					var s = Snap("#character");
					var g = s.group();

					var tux = Snap.load( imag_obj.attr("src") , function ( loadedFragment ) {
                                              g.append( loadedFragment );
                                        });
					
					setTimeout(function(){
						console.warn("loaded")
						iconSelf.snapFunctionLoad()
					},1000);

					$(".gf-ecm-view-main ul li").trigger("click");
				}
		});
		
		$("body").on("click","#close-edition-panel", function(){
				$(".panel-edition-skill").addClass("gf-ecm-element-inactive");
		})
		//editar 
		$("body").on("click", ".gf-incons-save-btn button#editSvgGroup", function(){
			console.info("valiria");
			$(".panel-edition-skill").removeClass("gf-ecm-element-inactive").draggable({ cancel: ".container_" });;
		});
	
		$("body").on("click", ".gf-incons-save-btn button.dropGroupSvg", function(){
			console.error("%c Eliminar tag SVG","padding:5px;font-size:22px;");
			$(".svg-selected").remove();
		});
		$("body").on("click",".create-elements-nav-main ul li", function(){
			$(".create-elements-nav-main ul li").removeClass("active");
			$(this).addClass("active")
		});
		$("body").on("click",".gf-incons-save-btn button.saveAsSvg", function(){
			//console.info("Edición...");
			$("#save-personaje-config").html( "" );
			//var svgEnd = $("#character-creating").html(); 
			var img = $("#character-creating").html();
				$("#save-personaje-config").append( img );
				
				$('.modal').modal('show');

			console.warn( $('.modal').length );

  			$('.animation-for-character .item').tab();
			
		})
		$("body").on("click","#resize-full", function(){
			$("article.gf-ecm-body").css({"transition":"0s all"})
			var state_  = $(this).attr('data-full');
			var height_ = ($("#gf-ecm-body-lateral-menu").outerHeight() - 100);
			
			if ( state_ == "false"){
					$(this).find("span.glyphicon").removeClass("glyphicon-resize-full").addClass("glyphicon-resize-small");
					$(this).attr('data-full','true');
					$("#gf-ecm-body-lateral-menu").addClass("resize-full-active");
					$(".character-preview").css("height",height_+"px");
					$(".gallery-character-properties").css("height",height_+"px");
				}

				if ( state_ == "true"){
					$(this).find("span.glyphicon").addClass("glyphicon-resize-small").removeClass("glyphicon-resize-full");
					$(this).attr('data-full','false');
					$("#gf-ecm-body-lateral-menu").removeClass("resize-full-active");	
					$(".character-preview").css("height","350px");
					$(".gallery-character-properties").css("height","350px");
				}
				$("article.gf-ecm-body").css({"transition":"0.5s all"})
		})
			
		$("body").on("click",".return-view-icons", function(){
			$(".gf-gallery-icon-global").addClass("gf-ecm-element-inactive");
			$(".main-tools-icons").removeClass("gf-ecm-element-inactive");
			$("#gf-gallery-show").addClass("gf-ecm-element-inactive");
			$("#tool-main-title").find("span.return-view-icons").addClass("gf-ecm-element-inactive");
			$("#tool-main-title").find("h5").text( "Recursos" );
			ecm.sizeEditionPanel( 400 );
		})
		
	}
}

var icons_ = new iconsP();
	icons_.init();

$(document).ready(function(){
	setTimeout(function(){
		$('.dropdown').dropdown();
		$('.tag.example .ui.dropdown') .dropdown({ allowAdditions: true })
	},2000);
})