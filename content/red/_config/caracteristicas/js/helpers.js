function getFilenameFromUrl(url)
{
    if ($.type(url) === 'string') {
        if (url.indexOf('/') != -1) {
            return url.substring(url.lastIndexOf('/') + 1);
        }
    }
    
    return '';
}
function getFileLocateFromURL(url){
    if($.type('url') == 'string'){
        if (url.indexOf('/') != -1) {
            url = url.split('/');
            url = url.slice(Math.max(url.length - 3,1));
            url = url.join('/');
            return url;
        }
    }
    return '';
}
function getExtension(str)
{
    if ($.type(str) === 'string') {
        return str.split('.').pop();
    }
    
    return '';
}

function getBasename(str)
{
    if ($.type(str) === 'string') {
        return str.substr(0, str.lastIndexOf('.')) || str;
    }
    
    return '';
}

function populateSelect(selector, options)
{
    if ($.type(selector) === 'string') {
        if ($.isArray(options)) {
            $.each(options, function (i, data) {
              /*  if( data.value == 'Verdana' ){
                    $(selector).append($('<option></option>').val(data.value).html(data.text).attr("selected","selected"));
                }

                if ( data.value == '1em' ){
                    $(selector).append($('<option></option>').val(data.value).html(data.text).attr("selected","selected"));
                }*/

                $(selector).append($('<option></option>').val(data.value).html(data.text));
            });
        }
    }
}