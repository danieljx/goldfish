function caracteristicas(){
	var mySelf = this;

	this.init = function(data){
		console.error(data);
	} 

	this.templateSlides = function(slidesData){
		console.info(slidesData);
		$.get('slides.mustache', function(templatesFile) {
			var template = templatesFile;	
			var output   = Mustache.render( $(template).filter("#pageTpl").html(), data );
			console.warn(output);
			$( "#global-container").append(output);
			mySelf.functionsSlides(data.id);
		});
	}

	this.functionsSlides = function(){

	}
}

var caracteristicas = new caracteristicas();

function addContent(){
	var slideData = [];
	data = $(window.xmlContent);
		data.find('title').each(function(index){
			if ( index == 1){
				//console.error($(this).text());
				//console.error($(this).attr("font"));
				//console.error($(this).attr("fontSize"));
				//console.error($(this).attr("fontColor"));
				//console.error($(this).attr("bgColor"));
			}
		});

		//console.error(data.find('autoplay').text());
		//console.error(data.find('bg').attr("type"));
		//console.error(data.find('bg').attr("url"));
		//console.error(data.find('bg').attr("color"));

		

		data.find('slides').each(function(index){
			$(this).find("slide").each(function(index){
				
				slideData.title = [];
				slideData.title.text      = $(this).find('title').text();
				slideData.title.font 	  = $(this).find('title').attr("font");
				slideData.title.fontSize  = $(this).find('title').attr("fontSize");
				slideData.title.fontColor = $(this).find('title').attr("fontColor");
				slideData.title.bgColor   = $(this).find('title').attr("bgColor");

				slideData.content = [];
				slideData.content.font 	    = $(this).find('content').attr("font");
				slideData.content.fontSize  = $(this).find('content').attr("fontSize");
				slideData.content.fontColor = $(this).find('content').attr("fontColor");
				slideData.content.bgColor   = $(this).find('content').attr("bgColor");
				slideData.content.bgColor   = $(this).find('close').attr("color");

				slideData.media = [];
				slideData.media.image       = $(this).find('media').find("image").attr('url');
				slideData.media.sound = [];
				slideData.media.sound.id    = $(this).find('media').find("sound").attr("id");
				slideData.media.sound.ogg   = $(this).find('media').find("sound").attr("ogg");
				slideData.media.sound.mp3   = $(this).find('media').find("sound").attr("mp3");

				slideData.icon = [];
				slideData.icon.bgColor  =  $(this).find('icon').attr("bgColor");
				slideData.icon.typ      =  $(this).find('icon').attr("type");
				slideData.icon.code     =  $(this).find('icon').attr("code");
				slideData.icon.color    =  $(this).find('icon').attr("color");
				slideData.icon.url      =  $(this).find('icon').attr("url");
				
				console.log(slideData);
				caracteristicas.templateSlides(slideData);
			})
		});

		
}
