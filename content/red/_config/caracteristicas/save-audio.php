<?php

set_include_path('../../../library');
require_once("Zend/Loader.php");
require_once("Local/config.load.php");

@$configXml = getConfig();
$basedir = '';

Zend_Loader::loadClass('Zend_Config_Xml');

if (file_exists($configXml)) {
    $_PATH = new Zend_Config_Xml($configXml, 'path');

    $basedir = $_PATH->sys->dirroot.
               DIRECTORY_SEPARATOR.
               $_PATH->sys->appfolder.
               DIRECTORY_SEPARATOR;
}

$response = new stdClass();
$validate = array('path', 'url', 'filename', 'basename', 'type', 'ext');
if (PHP_OS == 'Linux') {
    $ffmpeg = '../../../' . $basedir . 'filters' . DIRECTORY_SEPARATOR . 'ffmpeg';
} else {
    $ffmpeg = str_replace('\\','/',dirname(__FILE__)) . '../../../../filters/ffmpeg.exe' ;
}
$ffmpeg = (!file_exists($ffmpeg)) ? 'libs/ffmpeg/2.8.3/ffmpeg' : $ffmpeg;

if ($_POST) {
    
    // Data validation
    
    foreach ($validate as $i => $key) {
        if (!isset($_POST[$key])) {
            break;
        }
    }
    
    if (($i + 1) == count($validate)) {
        $post = (object) $_POST;
        $file = $post->path . '/' . $post->filename;
        $exts = array('mp3', 'ogg');
        $files = array();
        
        foreach ($exts as $ext) {
            
            // New file name and path

            $filename = $post->basename . '.' . $ext;
            $_file = $post->path . '/' . $filename;
            
            // File does not exists
            
            if ($ext != $post->ext) {
                
                // Conversion
                
                $command = $ffmpeg . ' -i "' . $file . '" "' . $_file . '" 2>&1';
                shell_exec($command);
            }
            
            // Validate file conversion

            if (file_exists($_file)) {
                
                // Response data

                $files[$ext] = new stdClass();
                $files[$ext]->name = $filename;
                $files[$ext]->url = $post->url . $filename;
            } else {
                $response->error = 'Ocurrió un problema con el decodificador y el archivo no fue generado.';
                break;
            }
        }
        
        $response->files = $files;
    } else {
        $response->error = 'No hay suficientes datos para convertir el sonido.';
    }
} else {
    $response->error = 'No hay suficientes datos para convertir el sonido.';
}

echo json_encode($response);