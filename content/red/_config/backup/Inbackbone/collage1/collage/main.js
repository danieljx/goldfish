/*
CREAR UN NUEVO PLUGIN LLAMADO (MARCAR NAVEGACION)
CAMBIAR EL CURSOR POR UN ICONO, QUE PUEDA GRABAR EL RECORRIDO DE ESTE
PARA MOSTRARLE AL USUARIO COMO DEBE NAVEGAR
mousedown = grabar left y top, guardarlos en un array json y grabaro en una animación
Crear un documento con las diferentes opciones para 
mostrar Svg's dentro de una pagina
*/

function addContent(){
	console.info("%cFunción addContent",'font-size:19px; color:magenta;');
	xml = $(window.xmlContent);
	var collageData    = xml.find("root").find("userdata");
	var collageVarsCss = collageData.find("collageDelabObjects").find('css');
	var app = new Collage.app();

	collageData.
		find("gfObject").each(function(){
				var typeSource  = unescape($(this).attr("data-child"));
			 	var childStyles = (unescape($(this).attr("data-child-styles"))!= "undefined") ? unescape($(this).attr("data-child-styles")) : "";
			 	var source_     = unescape($(this).attr("data-child-src"));
				var styleParent = (unescape($(this).attr ("data-style")) != "undefined") ? unescape($(this).attr ("data-style")) : "";
				var dinamyc, idcons = [];

				console.info( typeSource );
				switch (typeSource) {
					//Colocamos las imagenes
					case "img":
						var tmpImg = app.tmp_Image( source_ );
							tmpImg.attr("style", childStyles);

							dinamyc = app.templateDinamyc(tmpImg);
							dinamyc.attr("style",styleParent);
					break;
					//Colocamos el texto
					case "texto":
						var text = unescape($(this).attr("data-texto"));
						var txtTmp = app.tmp_text( source_, text);
							txtTmp.attr("style",childStyles);

							dinamyc = app.templateDinamyc(txtTmp);
							dinamyc.attr("style",styleParent);
					break;
					//Colocamos el audio
					case "audio":
						var colorIcon = $(this).attr("data-child-icon-color");
						var sound_ = app.tmp_audio(source_);
							sound_.attr("style",childStyles);
							sound_.find("object").attr("style",childStyles);
							sound_.find("object").css("background-color","transparent");
							
							if ( colorIcon != undefined){
								sound_.find("object").attr("data-color",colorIcon);
								idcons.push({
									"id": sound_.find("object").attr("id"),
									"color":colorIcon
								})
							}

							dinamyc = app.templateDinamyc(sound_);
							dinamyc.attr("style",styleParent);
					break;
					//Colocamos Iconos
					case "icon":
						var colorIcon = $(this).attr("data-child-icon-color");
						var svg_ = app.templateSvg( source_ );
							svg_.attr("style",childStyles);

							console.info( colorIcon );

							if ( colorIcon != "undefined" || colorIcon != undefined ){
								
								idcons.push({
									"id": svg_.attr("id"),
									"color":colorIcon
								})
							}

							dinamyc = app.templateDinamyc(svg_);
							dinamyc.attr("style",styleParent);
					break;

				}

				$("#window-edition").append( dinamyc );
				app.componentsWidget();	
				setTimeout(
					function(){
						console.warn("seteando colores");
							app.afterLoad(idcons);
					}, 2000)
			
			
		});

}//fin del addContent

function readParamsToXml(){
	console.info("%cFunción readParamsToXml",'font-size:19px; color:magenta;');
	var xml_ = '<?xml version="1.0" encoding="UTF-8"?><root><userdata>';
			
			xml_ += '<collageDelabObjects>';
				xml_ += '<css>';
					xml_ += "<leftMinus>500</leftMinus>";
					xml_ += '<globalZindex>12</globalZindex>';
				xml_ += '</css>';
			xml_ += '</collageDelabObjects>';

			$("#window-edition").find(".element-dinamyc").each(function(){
				var style_ = (escape($(this).attr("style")) != "undefined") ? escape($(this).attr("style")) : "";
				var conten = $(this).find(".gf-dinamyc-body");
				var images = conten.find("img.gf-object-image");
				var audios = conten.find("div.playSound");
				var icons   = conten.find("object.icon-color");
				var texto  = conten.find("div.text-plugin");

			xml_ += '<gfObject data-style="'+style_+'" ';
				//Guardamos imagenes
				if ( images.length >= 1){
					var myCss = ( escape(images.attr("style")) != "undefined") ? escape(images.attr("style")) : "";
					var link_ = escape(images.attr("src"));
						
						xml_ += ' data-child="img" ';
						xml_ += ' data-child-styles="'+ myCss +'" ';
						xml_ += ' data-child-src="'+link_+'">';
				}
				//Guardamos  audios
				if ( audios.length >= 1 ){
					var dataColor = audios.find("object.audio-color").attr("data-color");
					var ClIcon = ( dataColor != "undefined" ) ? dataColor : ""; 
					var myCss  = ( escape(audios.attr("style")) != "undefined") ? escape(audios.attr("style")) : "";
					var link_  = escape(audios.find(".audioTagElement").find('audio').find("source").attr("src"));
						
						xml_ += ' data-child="audio" ';
						xml_ += ' data-child-styles="'+myCss+'" ';
						xml_ += ' data-child-icon-color="'+ClIcon+'" ';
						xml_ += ' data-child-src="'+link_+'">';
				}
				//Guardamos Iconos SVG
				if ( icons.length >= 1 ){
					var dataColor = icons.attr("data-icon-color"),
						ClIcon = ( dataColor != "undefined" ) ? dataColor : "",
						myCss  = ( icons.attr("style") != "undefined") ? escape(icons.attr("style")) : "",
					    link_  = escape( icons.attr("data") );

						xml_ += ' data-child="icon" ';
						xml_ += ' data-child-styles="'+myCss+'" ';
						xml_ += ' data-child-icon-color="'+ClIcon+'" ';
						xml_ += ' data-child-src="'+link_+'">';
				}
				//Guardamos los bloques de texto
				if ( texto.length >= 1 ){
					var myCss  = ( escape(texto.attr("style")) != "undefined" ) ? escape(texto.attr("style")) : "",
						texto_ = texto.find(".editable-text-header").text(),
						fnType = texto.attr("data-font-size");

						xml_ += ' data-child="texto" ';
						xml_ += ' data-texto="'+texto_+'" ';
						xml_ += ' data-child-styles="'+myCss+'" ';
						xml_ += ' data-child-src="'+fnType+'">';
				}

			xml_ += "</gfObject>";
			});

		xml_ += "</userdata></root>";
	console.warn( xml_ );
	return xml_;
}