var Collage = Collage || {};

Collage.window = Backbone.View.extend({
	el:"#window-edition",
	noHelps:[],
	template: _.template( $("#view-window-main").html() ),
	initialize: function(){
		this.render();
	},
	render:function(){
		console.info( "%cCargando ventana principal","color:#008ae6;font-size:15px;" );
		this.$el.append( this.template( this.model.toJSON() ) );
	},
	events: {
		"click .element-dinamyc":"selected",
		"dblclick div.playSound":"playAudio",
		"contextmenu .element-dinamyc": "showOptions",
		"click .close-menu":"closeSubMenu",
		"dblclick .editable-text-header":"setTextToInput",
		"keypress input.editable":"setText",
		"click .selected-window-tool":"selectedWindow"
	},
	selectedWindow: function(){
		this.model.deselected();
		console.info("selecionar ventana princpial");
		$("#window-edition").addClass( "selected-obj" );
	},
	setText: function(e){
		var this_ = $(e.currentTarget);
		 if( e.which == 13 ) {
		 	var id_padre = this_.attr("data-id-parent");
	        $("#"+id_padre).text(this_.val()).removeClass('element-inactive');
	        this_.parent(".editable-text-input").addClass("element-inactive");
	        this.model.showAlert("ok","¡Texto cambiado con exito!")
	    }
	},
	setTextToInput: function(e){
		var this_ = $(e.currentTarget);
		var this_val = this_.attr("id");
		var sectionTxt = $("section#"+this_val+"-editable");

		this_.addClass("element-inactive");
		sectionTxt.removeClass("element-inactive");
		sectionTxt.find("input.editable").val(this_.text());
	},
	closeSubMenu: function(){
		$("#main-contextmenu").addClass("element-inactive");	
	},
	showOptions: function(e){
		e.preventDefault();
		this.model.deselected();
		$(e.currentTarget).addClass("selected-obj");
		var topClient = event.clientY, leftClient = event.clientX;
			$("#main-contextmenu")
			.css({"left": (leftClient-100)+"px","top":(topClient-100)+"px"});
			$("#main-contextmenu").removeClass("element-inactive");
	},
	selected: function(e){
		this.model.deselected();
		var this_ = $(e.currentTarget);
			this_.addClass("selected-obj");

			var widthCSS    = parseInt(this_.css("width"));
			var tranform_ 	= this_.css("transform");
			var heightCSS   = parseInt(this_.css("height"));
			var paddinCSS   = this_.css("padding").split(" "),
				top_ = 0, right_ = 0, bottom_ = 0, left_ = 0;

			var borderColor  = this_.css("border-color"),
				borderWidth  = this_.css("border-width"),
				borderRadius = this_.css("border-radius").split(" "),
				Rtop_ = 0, Rright_ = 0, Rbottom_ = 0, Rleft_ = 0;

			//Colocar propiedades del Border
			console.warn( borderRadius.length );
			
				if ( borderRadius.length > 1 ) {
					Rtop_    = parseInt(borderRadius[0]),
					Rright_  = parseInt(borderRadius[1]),
					Rbottom_ = parseInt(borderRadius[2]),
					Rleft_ 	 = parseInt(borderRadius[3]);
					$("#borderRadiusCss").val( 0 );
				}else if( borderRadius.length <= 1 ){
					$("#borderRadiusCss").val( parseInt(borderRadius[0]) );
				}

					$("#RadiusTopLeft").val ( Rtop_ );
					$("#RadiusTopRight").val ( Rright_ );
					$("#RadiusBottompLeft").val ( Rleft_);
					$("#RadiusBottompRight").val ( Rleft_);
					$("#weightBorder").val( parseInt(borderWidth) );
					$("#colorBorder").val( borderColor );
			//fin border

			//Colocar padding
			if ( paddinCSS.length > 1 ){
				top_    = parseInt(paddinCSS[0]);
				right_  = parseInt(paddinCSS[1]);
				bottom_ = parseInt(paddinCSS[2]);
				left_   = parseInt(paddinCSS[3]);
				$("#paddingGlobal").val( 0 );
			}
			else if ( paddinCSS.length <= 1 ){
				$("#paddingGlobal").val( parseInt(paddinCSS[0]) );
			}//fin colocar padding


			if ( tranform_ != "none" ){
				var values = tranform_.split('(')[1],
			   	 	values = values.split(')')[0],
			   	 	values = values.split(',');
				var b = values[1];
				var angle = Math.round(Math.asin(b) * (180/Math.PI));
				$("#css-rotate-grades").val(angle);
			}else if ( tranform_ == "none" ){
				console.info("pasamos");
			 $("#css-rotate-grades").val(0); }
			//console.info( tranform_ );
			//padding
			$("#paddingTop").val( top_ );
			$("#paddingBottom").val( right_ );
			$("#paddingRight").val( bottom_ );
			$("#paddingLeft").val( left_ );
			//Tamaño
			$("#sizeWidthCss").val(widthCSS);
			$("#sizeHeightCss").val(heightCSS);

			
			$(".element-dinamyc").removeClass("active");
			this_.addClass("active");

		var el_audio = this_.find("div.playSound");
			//Ayuda para reproducri audio
			if ( this.noHelps.indexOf("audio") == -1) {
				if ( el_audio.length >= 1 ){
					this.noHelps.push("audio");
					this.model.showAlert("warning","Si le das <b>Doble clic</b>, el audio se reproducirá. (Solo en la edición)");
				}
			}
	},
	playAudio:function(e){
		var this_ = $(e.currentTarget),
			doc   = this_.attr("data-child-audio"),
			state_audio = this_.attr("data-state"),
			audio = document.getElementById(doc),
			Img   = this_.find(".audioPlayIcon > div img"),
			attrImg;
		if ( state_audio == "false" ){
			audio.play();
			this_.attr("data-state","true");
			attrImg = "files/pausa.svg";
		}else if ( state_audio == "true" ){
			audio.pause();
			this_.attr("data-state","false");
			attrImg = "files/play.svg";
		}
		Img.attr("src",attrImg);

	},

});