var Collage = Collage || {};
/*Quitar y agregar eventos delegate and undelegate*/
Collage.multimedia = Backbone.View.extend({
	"el":"#gf-ecm-config-lateral-menu",
	template: _.template( $("#view-multimedia").html() ),
	initialize: function(){
		this.render();
	},
	render: function(){
		this.$el.append( this.template(this.model.toJSON()) );
		console.info("%cPlugin multimedia cargado","color:#399385;font-size:16px;");
	},
	events: {
		"click .multi-item": "selectItem",
		"click #imgtest": "tmpImage",
		"click #audiotest": "tmpAudio",
	},
	selectItem: function(e){
		var this_   = $(e.currentTarget),
			subMenu = this_.attr("data-sub-menu-child");
		$(".multi-item").removeClass("active");
		$(".multimedia-tools .tools").addClass("element-inactive");
		$("#"+subMenu).removeClass("element-inactive");
		$(e.currentTarget).addClass("active");
	},
	upload_:function(){
		//console.warn("instanciado");
		$("body").on("click",'#char-upload', function(){
			//console.warn( "hello!!" );
		})
		$(function () {
		    'use strict';
		    // Change this to the location of your server-side upload handler:
		    var url = '../../jquery-file-upload-handler.php';
		 
		    $('#char-upload').fileupload({
		    	dropZone: $(this),
		        url: url,
		        dataType: 'json',
		        autoUpload: true,
		        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
                maxFileSize: 1 * 1000000, // 1 Mb
		        done: function (e, data) {
		        	//console.warn("audio upladed");
		        	/*var elem_ = $(this);
		        	var file_ = data.result.files[0];
		        	if( myself.current_Upload != 'bgMain' ){
		        			$.each(data.result.files, function (index, file) {
								elem_
								.parent().parent().parent().siblings().find("div#bgImgCnt")
								.css("background-image","url("+file_.url+")");
							});
		        	}
		        	else if( myself.current_Upload == 'bgMain' ){
		        			$.each(data.result.files, function (index, file) {
			        				elem_
									.parent().parent().parent().siblings()
									.find("div").find("img")
									.attr("src", file_.url);
		        				});
		        	}*/
				},
		      	progressall: function (e, data) {
		      		//console.info("here")
					/*var progress = parseInt(data.loaded / data.total * 100, 10);
					$(this).parent().siblings()
					.find('.progress-bar')
					.css('width', progress + '%');*/
				}
		    })
		    .on('fileuploadfail', function(e, data) {
					console.warn("FALLO!!")
			})
            .prop('disabled', !$.support.fileInput)
		    .parent().addClass($.support.fileInput ? undefined : 'disabled');
		});
	},
	tmp_Image: function(file){
		var id_ = this.model.createId('img');
			var img_ = $("<img/>");
				img_.attr("class",'dynamic-image');
				img_.addClass("gf-object-image");
				img_.attr("src",file);
				img_.attr("id",id_);
		return img_;
	},
	tmp_audio: function(file){
		var id_ = this.model.createId('audio');
		
		var soundBase = $("<div/>");
			soundBase.attr("data-child-audio", id_);
			soundBase.attr("data-state", "false");
			soundBase.attr("class", "playSound");

		var secPlay = $("<section/>");
			secPlay.attr("class","audioPlayIcon");

		var Icon = $("<div/>");

		var obj_ = $("<object/>");
			obj_.attr("class", "audio-color set-color-svg");
			obj_.attr("id", id_+"-svg");
			obj_.attr("data","files/play.svg");
			obj_.attr("type","image/svg+xml");
			obj_.attr("height","55");
			obj_.attr("width","55");

			Icon.append(obj_);

		var secAudioTag = $("<section/>");
			secAudioTag.attr("class", "audioTagElement");

		var AudioTag = $("<audio/>");
			AudioTag.attr("id", id_);

		var sourceTag = $("<source/>");
			sourceTag.attr("src",file);
			sourceTag.attr("type","audio/mpeg");

			AudioTag.append(sourceTag);
			secAudioTag.append(AudioTag);
			secPlay.append(Icon);
			soundBase.append(secAudioTag);
			soundBase.append(secPlay);

		return soundBase;
	},
	tmpImage: function(){
		var file_image = "files/web.jpg";
		$("#window-edition").append( this.model.templateDinamyc( this.tmp_Image(file_image) ) );
		this.model.componentsWidget();
		this.model.showAlert("ok","Recurso añadido correctamente");
	},
	tmpAudio: function(){
		var file_audio = "files/short.mp3";
		$("#window-edition").append( this.model.templateDinamyc( this.tmp_audio(file_audio) ) );
		this.model.componentsWidget();
		this.model.showAlert("ok","Recurso añadido correctamente");
	}
});