var Collage = Collage || {};

Collage.alert = Backbone.Model.extend({
	defaults:{
		classA: "alert-danger",
		icon: "glyphicon-remove",
		text: "Muestra alertas por defecto"
	},
	initialize: function(){
		console.warn("alert instanciado");
		this.getAlert();
	},
	getAlert: function(){
		var alert = new Collage.alertView( {model: this} );
	}
});