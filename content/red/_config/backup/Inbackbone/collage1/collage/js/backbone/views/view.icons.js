var Collage = Collage || {};
//Dtener varios eventos stopPorpagation
Collage.icons = Backbone.View.extend({
	"el":"#gf-ecm-config-lateral-menu",
	template:_.template( $("#view-icons").html() ),
	initialize: function(){
		this.render();
		//this.model.componentsWidget();
	},
	render: function(){
		this.$el.append( this.template( this.model.toJSON() ));
		console.info("%cPlugin iconos, cargado.","font-size:16px;color:#ffad33;");
	},
	events: {
		"click ul li.show-galleries":"materials",
		"contextmenu ul.tooltip-avalaible li.draggable-initial":"options",
		"click .options li.item[data-action='custom']":"personalizar",
		"click .options li.item[data-action='remove']":"remove",
		"click .return-gallery": "returnMain"
	},
	returnMain: function(){
		$(".behind-stage").addClass(this.model.get("inactive"));
		$("#main-menu-gallery").removeClass( this.model.get("inactive") );
		$(".edition-header .return").addClass( this.model.get("inactive") );
		$(".edition-header h5").text("Materiales:");
	},
	remove: function(){
		var removethis = confirm("¿Deseas eliminar este contenido?");
		if ( removethis == true ){
				console.info("Si tu lo creaste, lo puedes eliminar");
		}
	},
	materials:function(e){
		var this_      = $(e.currentTarget),
			Gid = this_.attr("data-child-show"),//gallery id
			noDisappear = this_.attr("data-disapear")//No abrir galeria, Abrir modal

			if ( !noDisappear ){
				$(".edition-header div.return").removeClass( this.model.get("inactive") );
				$(".edition-header h5").text( this_.find(".title").text() +":");
				$(".behind-stage").addClass( this.model.get("inactive") )
				$("#"+Gid).removeClass( this.model.get("inactive") );
			} else{
				$('#modal-box').modal("show");
				$('.dropdown').dropdown();
				$('.accordion') .accordion({  selector: { trigger: '.title .icon'  } });
				
				this.model.showAlert("ok","!Modal de edición de personajes, abierto!");
			}
		e.stopPropagation();
	},
	options: function(e){
		var this_ = $(e.currentTarget);
			e.preventDefault();

			var x = event.clientX;  
				var y = event.clientY; 
				$("#character-gallery .options")
					.removeClass( this.model.get("inactive") )
					.css({"left": (x-100)+"px", "top": (y-100)+"px"});
	}
});