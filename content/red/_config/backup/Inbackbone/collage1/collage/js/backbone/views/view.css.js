var Collage = Collage || {};

Collage.cssPlugin = Backbone.View.extend({
	"el":"#gf-ecm-config-lateral-menu",
	template: _.template( $("#css-plugin").html() ),
	initialize: function(){
		this.render();
		console.warn("%c¡Plugin css Cargado!","font-size:12px;font-weight:bold;color:#4d4d4d;");
		$('.menu .item').tab();
		$("#custom").spectrum({ 
			color: "#fff",
			move: function(color) {
				$("#colorBorder").val( color.toHexString() );
			 }
		 });
	},
	render: function(){
		this.$el.append( this.template( this.model.toJSON() ))
	},
	events:{
		"click .css-group-link":"showMoreProperties",
		"change .cssPadding":"setPadding",
		"keypress .cssPadding":"setPadding",
		"change .sizeCss":"editSize",
		"keypress .sizeCss":"editSize",
		"change #css-rotate-grades":"editRotate",
		"keypress #css-rotate-grades":"editRotate",
		"change .cssBorder":"editBorder",
		"keypress .cssBorder":"editBorder",
		"click #changeToMultiCurve":"changeComplete"
	},
	changeComplete: function(e){
		var this_ 	  = $(e.currentTarget);
		var hasClass_ = this_.hasClass("data-active");
		var tag_style = $("#borderRadiusCss").attr("data-ref-style");
		if ( hasClass_ == true ){
			$("#borderRadiusCss").attr("data-ref-style","forParts");
		} else if (  hasClass_ == false ){
			$("#borderRadiusCss").attr("data-ref-style","complete");
		}
	},
	editBorder: function(e){
		var this_ = $(e.currentTarget);
		var tag_style = $("#borderRadiusCss").attr("data-ref-style");

		var color = $("#colorBorder").val(),
		    ancho = $("#weightBorder").val(),
		    radius = $("#borderRadiusCss").val(),
			anchoIsNan = isNaN(ancho),
			window_   = this.model.get("window"),
			selected  = this.model.get("selected"),
			radiusIsNan = isNaN(radius),
			exists = $( this.model.get("window") ).find(this.model.get("selected"));

		if ( exists.length >= 1 ){
			if (  anchoIsNan == false & ancho >= 0 & ancho <= 10 & radiusIsNan == false ){
				if ( tag_style == "complete" ){
					exists.css("border-radius", radius+"px");	
				}
				else if ( tag_style == "forParts" ){
					console.info("leyes!!");
					$("input.cssBorder[data-style-border]").each(function(){
						var attr_val = $(this).val();
						var attrName = $(this).attr("data-style-border");

							$(window_).find(selected).css( attrName, attr_val+"px" );
					});
				}
					exists.css("border-color",color);
					exists.css("border-style","solid");
					exists.css("border-width", ancho+"px");
			}
		}else{
			this.model.showAlert("warning","No hay elementos selecionados");
		}
	},
	editRotate: function(e){
		var this_ = $(e.currentTarget);
		var meVal  = this_.val(),
			NotNum = isNaN(meVal),
			window_   = this.model.get("window"),
			selected  = this.model.get("selected"),
			exists    = $(window_).find(selected);
		if ( exists.length >= 1 ){
			if (  NotNum == false & meVal >= 0 & meVal <= 1000  ){
				$(window_).find(selected).css( "transform","rotate("+meVal+"deg)" );
			}
		}else{
			this.model.showAlert("warning","No hay elementos selecionados");
		}
	},
	editSize: function(e){
		$("input.sizeCss[data-size-attr]").each(function(){
			var meVal  = $(this).val(),
				NotNum = isNaN(meVal),
				window_   = this.model.get("window"),
				selected  = this.model.get("selected"),
				exists    = $(window_).find(selected);
			if ( exists.length >= 1 ){
				if (  NotNum == false & meVal >= 0 & meVal <= 1000  ){
					var attr_val  = $(this).val();
					var attrName = $(this).attr("data-size-attr");

					$(window_).find(selected).css( attrName, attr_val+"px" );
				}
			}else{
				this.model.showAlert("warning","No hay elementos selecionados");
			}
			
		})
	},
	setPadding: function(e){
		var this_    = $(e.currentTarget),
			completeAttr = this_.attr("data-ref-style"),
			meVal    = this_.val(),
			NotNumber = isNaN( meVal ),
			window_   = this.model.get("window"),
			selected  = this.model.get("selected"),
			exists    = $(window_).find(selected);
			
			if ( NotNumber == false & meVal >= 0 & meVal <= 30 ) {
				if ( exists.length >= 1 ){
					if ( completeAttr == "complete"){
						$(window_).find(selected).css("padding", meVal+"px");
					}else{
						$("input.cssPadding[data-style-attr]").each(function(){
							var attrName   = $(this).attr("data-style-attr");
							var attr_value = $(this).val();

							$(window_).find(selected).css(
								attrName, attr_value+"px"
							);
						})
					}	
				}else{
					this.model.showAlert("warning","No hay elementos selecionados");
				}
				
			}else{
				this.model.showAlert("error","Por favor ingresa un valor <b>válido</b>")
			}	
	},
	showMoreProperties: function(e){
		var this_ = $(e.currentTarget),
			attr_ = this_.attr("data-show");

		$(".unlinkProperties").addClass( this.model.get("inactive") );
		console.warn("ok")
		if ( attr_ ){ 
				console.warn( this_.hasClass("data-active") );
			if ( this_.hasClass("data-active") == false ){
				console.info("aqui!!");
				 this_.addClass("data-active");
				$("#"+attr_).removeClass( this.model.get("inactive") )
			}
			else if ( this_.hasClass("data-active") == true ) {
				console.warn("250.00")
				 this_.removeClass("data-active");
				$("#"+attr_).addClass( this.model.get("inactive") )
			}
			
		}
	}
})