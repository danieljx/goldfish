var Collage = Collage || {};

Collage.texto = Backbone.View.extend({
	"el":"#gf-ecm-config-lateral-menu",
	template: _.template( $("#text-plugin").html() ),
	initialize: function(){
		this.render();
	},
	render: function(){
		console.warn( "%c¡Plugin texto, cargado!","font-size:12px;font-weight:bold;color:orange;" );
		this.$el.append( this.template(this.model.toJSON()) );
	}
});