var Collage = Collage || {};

Collage.leftSideBar = Backbone.View.extend({
	"el":"#side-left-menu",
	plugins: [],
	template: _.template( $("#left-side-bar").html() ),
	render: function(){
		this.$el.append( this.template( this.model.toJSON() ))
	},
	initialize: function(){
		this.render();
	},
	events: {
		"click li.left-tool": "activePanel",
		"click article.left-side-bar-edition #close":"closepPanel"
	},
	activePanel: function(e){

		var this_ = $(e.currentTarget);
		var viewEdit = this_.attr("data-view");
		$("li.left-tool").removeClass("left-active");
			this_.addClass("left-active");
		console.warn( viewEdit );
		$(".plugins").addClass( this.model.get("inactive") )
		switch ( viewEdit ){
			case "Multimedia":
				$("#view-multi-section").removeClass( this.model.get("inactive") );
			break;

			case "icons":
				$("#plugin-icons").removeClass( this.model.get("inactive") );
				this.model.componentsWidget();
			break;

			case "text":
				$("#view-text-plugin").removeClass( this.model.get("inactive") );
				this.model.componentsWidget();
			break;

			case "css":
				$("#view-css-plugin").removeClass( this.model.get("inactive") );
				this.model.componentsWidget();
			break;
		}

		$(".left-side-bar-edition").removeClass("element-inactive");
	},
	closepPanel: function(){
		$(".left-side-bar-edition").addClass("element-inactive");	
	}	
});

