var Collage = Collage || {};

Collage.start = function(){
	var app     = new this.app();
	//Vistas
	var appView     = new this.upperSideBar({ model: app });//barra de herramientas superiror
	var leftSideBar = new this.leftSideBar({ model: app });//barra lateral izquierda
	var window_     = new this.window({ model: app });//ventana Principal

	var pluginMulti = new Collage.multimedia({model: app });//Multimedia
	var pluginIcons = new Collage.icons({ model: app });//Iconos
	var character   = new this.character({ model: app });//personajes
	var pluginText  = new this.texto({ model: app });//texto
	var pluginCss  = new this.cssPlugin({ model: app });//css
}
