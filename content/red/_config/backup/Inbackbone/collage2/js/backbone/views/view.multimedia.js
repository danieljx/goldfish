var Collage = Collage || {};
/** The application main model
* @author Duvan Valencia
* @augments Backbone.View
* @constructor
* @name multimedia
*/
Collage.multimedia = Backbone.View.extend( /** @lends multimedia */ {
	"el":"#gf-ecm-config-lateral-menu",
	template: _.template( $("#view-multimedia").html() ),
	initialize: function(){
		this.render();
	},
	render: function(){
		this.$el.append( this.template(this.model.toJSON()) );
		console.warn("%c---Plugin multimedia cargado---","color:#9933ff;font-size:12px;font-weight:bold;");
	},
	events: {
		"click .multi-item": "selectItem",
		"click #imgtest": "tmpImage",
		"click #audiotest": "tmpAudio",
		"click #generate-audio":"generate"
	},
	/** Generate audio ttf
    */
	generate: function(){
		var params = [];
		var txt = $(".textarea-to-generate").val();

			params.gender =  $("#gender").val();
			params.lang   =  $("#lang").val();
			params.tone   =  $("#tono").val();
			params.speed  =  $("#speed").val();
			
			if ( txt.length <= 10){
				this.model.showAlert("error","Por favor ingresa un texto")
			}else{
				this.generateVoice(txt, params);
			}
	},
	/** load tts
    */
	generateVoice: function(texto, params){
			var variablesURL;
		    var ubicacionTTS = "../../../tts/";
		    var archivo      = 'index.php';
		    var genFolder    = 'files/';
		    var metodo       = 'GET';
		    var txtEsc       = "txt=" + escape(texto); //Preparar texto "escapado" para inserción en la URL  
		    variablesURL     = "idi=" + params.lang + "&ora=" + params.gender + "&vel=" + params.speed + "&ton=" + params.tone + "&" + txtEsc;
			
			$.ajax(ubicacionTTS + archivo,
		    {
		        "type": metodo,   // usualmente post o get
		        async:false,
		        data: variablesURL,        
		        beforeSend: function () { },
		        success: function(respuesta){
		        	console.warn("Todo correcto");
		        	/*$("div#sound_").empty();
		        	var soundPath = datafolder+"/tts/" + respuesta;
					var mp3Path   = soundPath + ".mp3";
					var oggPath   = soundPath + ".ogg";
					
					$("#saveMsj")
					.attr('data-audio-state',"true")
					.attr('data-audio-name', respuesta)
					.attr('data-folder-file', 'tts');

					//var _audio_ = '<audio controls class="audio"><source src="'+wwwupload+"/tts/" + respuesta + ".ogg"+'" type="audio/mpeg"></audio>';
					var _audio_ = '<audio controls class="audio"><source src="'+window.location.origin+'/'+datafolder+"/tts/" + respuesta + ".ogg"+'" type="audio/mpeg"></audio>';
	        		
	        		$("div#sound_").append(_audio_);
					
					var valText = $("#editTextEdition").val();
			    		
					$("#estu")
		            .removeClass("alert-danger").addClass("alert-success")
		            .css("display","block").text("audio creado Exitosamente");
		            alert("Conversión realizada exitosamente");
					$("#convertOptions").fadeOut();*/
				 },
		        error: function(respuesta) {
		            $("#estu")
		            .removeClass("alert-success")
					.addClass("alert-danger")
		            .css("display","block")
		            .text("Ha ocurrido un error "+respuesta);
		        },	    	 
		});   
	},
	/** switch between upload audio or image
    */
	selectItem: function(e){
		var this_   = $(e.currentTarget),
			subMenu = this_.attr("data-sub-menu-child");
		$(".multi-item").removeClass("active");
		$(".multimedia-tools .tools").addClass("element-inactive");
		$("#"+subMenu).removeClass("element-inactive");
		$(e.currentTarget).addClass("active");
	},
	/** upload audio/image
    */
	upload_:function(){
		//console.warn("instanciado");
		$("body").on("click",'#char-upload', function(){
			//console.warn( "hello!!" );
		})
		$(function () {
		    'use strict';
		    // Change this to the location of your server-side upload handler:
		    var url = '../../jquery-file-upload-handler.php';
		 
		    $('#char-upload').fileupload({
		    	dropZone: $(this),
		        url: url,
		        dataType: 'json',
		        autoUpload: true,
		        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
                maxFileSize: 1 * 1000000, // 1 Mb
		        done: function (e, data) {
		        	//console.warn("audio upladed");
		        	/*var elem_ = $(this);
		        	var file_ = data.result.files[0];
		        	if( myself.current_Upload != 'bgMain' ){
		        			$.each(data.result.files, function (index, file) {
								elem_
								.parent().parent().parent().siblings().find("div#bgImgCnt")
								.css("background-image","url("+file_.url+")");
							});
		        	}
		        	else if( myself.current_Upload == 'bgMain' ){
		        			$.each(data.result.files, function (index, file) {
			        				elem_
									.parent().parent().parent().siblings()
									.find("div").find("img")
									.attr("src", file_.url);
		        				});
		        	}*/
				},
		      	progressall: function (e, data) {
		      		//console.info("here")
					/*var progress = parseInt(data.loaded / data.total * 100, 10);
					$(this).parent().siblings()
					.find('.progress-bar')
					.css('width', progress + '%');*/
				}
		    })
		    .on('fileuploadfail', function(e, data) {
					console.warn("FALLO!!")
			})
            .prop('disabled', !$.support.fileInput)
		    .parent().addClass($.support.fileInput ? undefined : 'disabled');
		});
	},
	/** Pruebas
    */
	tmpImage: function(){
		var file_image = "files/web.jpg";
		$("#window-edition").append( this.model.templateDinamyc( this.model.tmp_Image(file_image) ) );
		this.model.componentsWidget();
		this.model.showAlert("ok","Recurso añadido correctamente");
	},
	tmpAudio: function(){
		var file_audio = "files/short.mp3";
		$("#window-edition").append( this.model.templateDinamyc( this.model.tmp_audio(file_audio) ) );
		this.model.componentsWidget();
		this.model.showAlert("ok","Recurso añadido correctamente");
	}
});