var Collage = Collage || {};
/** The application main model
* @author Duvan Valencia
* @augments Backbone.View
* @constructor
* @name window
*/
Collage.window = Backbone.View.extend( /** @lends window */ {
	el:"#window-edition",
	/** show the help info into div#console, ex:audio play 
    */
	noHelps:[],
	template: _.template( $("#view-window-main").html() ),
	initialize: function(){ this.render(); },
	render:function(){
		console.info( "%c>Ventana principal<","color:#008ae6;font-size:14px;font-weight:bold;" );
		this.$el.append( this.template( this.model.toJSON() ) );
	},
	events: {
		"click .element-dinamyc":"selected",
		"dblclick div.playSound":"playAudio",
		"contextmenu .element-dinamyc": "showOptions",
		"click .close-menu":"closeSubMenu",
		"dblclick .editable-text-header":"setTextToInput",
		"keypress input.editable":"setText",
		"click .window-menu":"showPropertiesForWindow",
		"click .window-options-list .item-option[data-action='select']": "selectWindow",
		"click .window-options-list .item-option[data-action='getId']": "getId",
		"click .window-options-list .item-option[data-action='meassure']": "changeMeassure",
		"click .window-options-list .item-option[data-action='deselectAll']": "removeSelect"
	},
	/** remove all .selected-obj class for all elements (windiw and dinamyc)
    */
	removeSelect: function(){ this.model.deselected(); },
	/** switch meassure between px and %
    */
	changeMeassure: function(e){
		var this_    =  $(e.currentTarget),
			showInfo = this_.attr("data-info"),
			valMs    = this_.find("#unity-window"),
			messure_val = valMs.text();

			if ( messure_val == "px" || messure_val == "%"){
				switch (messure_val) {
					case "px": valMs.text("%"); break;
					case "%": valMs.text("px"); break;
				}
				if ( showInfo == "false" ){
					this.model.showAlert(
							"ok",
							"Si seleccionas <b>px</b> los elementos se mantendran <b>fijos</b>. "+
							"Si seleccionas <b>%</b> los elementos se <b>redimensionaran</b> hasta cierto punto."
							);
					this_.attr("data-info","true");
				} 
			}else{
				this.model.showAlert("error","El valor de medida no es válido");
			}
	},
	/** Get id for selected element or window
    */
	getId: function(e){
		var isWindow_ = $('#window-edition[data-selected="true"]'),
			isDinamy  = $("#window-edition").find(".selected-obj");
			if ( isWindow_.length != 0 || isDinamy.length != 0){
				var elemento = ( isWindow_.length >= 1 )? isWindow_ : isDinamy; 
					this.model.set("idCopied", elemento.attr("id"));
					this.model.showAlert("ok","El Id: <b>"+ this.model.get("idCopied")+ "</b> Ha sido copiado");
			}else{
				this.model.showAlert("error","No hay elementos seleccionados");
			}
	},
	/** Click in dinamyc element
    */
	selectWindow: function(e){
		var this_ = $(e.currentTarget);
			this.model.deselected();
		$('#window-edition').attr("data-selected","true");
		$("#css-menu").find(".item")
			.not('.item[data-tab="size"]')
				.each(function(){
					$(this).fadeOut("fast").removeClass("active");
				});
				$("#css-menu").find(".item[data-tab='size']").addClass("active");
		$(".css-behind-stage").removeClass("active");
		$(".css-behind-stage[ data-tab='size']").addClass("active");
		$("#sizeWidthCss").val( parseInt( $('#window-edition').css("width") ) );
		$("#sizeHeightCss").val( parseInt( $('#window-edition').css("height") ) );
	},
	/** Menu for window (selccionar, obtener Id, unidad px...)
    */
	showPropertiesForWindow:function(e){
		var this_ 	 = $(e.currentTarget),
			attrClas = this_.hasClass("show-menu"),
			classIcondAdd, classIcondRmv;

			if ( attrClas == true ){
				this_.removeClass("show-menu").addClass("close-it")
				classIcondAdd = "glyphicon-remove";
				classIcondRmv = "glyphicon-menu-hamburger";
				$(".window-options").removeClass( this.model.get("inactive") );
			} else if ( attrClas == false ){
					this_.addClass("show-menu").removeClass("close-it");
					classIcondRmv = "glyphicon-remove";
					classIcondAdd = "glyphicon-menu-hamburger";
					$(".window-options").addClass( this.model.get("inactive") );
			}

			this_.find("span").removeClass(classIcondRmv).addClass(classIcondAdd)
	},
	/** Enter into input plugin text
    */
	setText: function(e){
		var this_ = $(e.currentTarget);
		 if( e.which == 13 ) {
		 	var id_padre = this_.attr("data-id-parent");
	        $("#"+id_padre).text(this_.val()).removeClass('element-inactive');
	        this_.parent(".editable-text-input").addClass("element-inactive");
	        this.model.showAlert("ok","¡Texto cambiado con exito!")
	    }
	},
	/** Double click over selected element type text, click for edit it's text
    */
	setTextToInput: function(e){
		var this_ = $(e.currentTarget);
		var this_val = this_.attr("id");
		var sectionTxt = $("section#"+this_val+"-editable");

		this_.addClass("element-inactive");
		sectionTxt.removeClass("element-inactive");
		sectionTxt.find("input.editable").val(this_.text());
	},
	/** close context menu
    */
	closeSubMenu: function(){
		$("#main-contextmenu").addClass("element-inactive");	
	},
	/** open contextmenu
    */
	showOptions: function(e){
		e.preventDefault();
		this.model.deselected();
		$(e.currentTarget).addClass("selected-obj");
		var topClient = event.clientY, leftClient = event.clientX;
			$("#main-contextmenu")
			.css({"left": (leftClient-100)+"px","top":(topClient-100)+"px"});
			$("#main-contextmenu").removeClass("element-inactive");
	},
	/** Click in dinamy element and set it's styles into plugin css
    */
	selected: function(e){
		this.model.deselected();
		var this_ = $(e.currentTarget);
			this_.addClass("selected-obj");

			$("#css-menu").find(".item").each(function(){ $(this).fadeIn("fast").removeClass("active"); });
			$("#css-menu").find(".item[data-tab='padding']").addClass("active");
			$(".css-behind-stage").removeClass("active");
			$(".css-behind-stage[ data-tab='padding']").addClass("active");

			var widthCSS    = parseInt(this_.css("width"));
			var tranform_ 	= this_.css("transform");
			var heightCSS   = parseInt(this_.css("height"));
			var paddinCSS   = this_.css("padding").split(" "),
				top_ = 0, right_ = 0, bottom_ = 0, left_ = 0;

			var borderColor  = this_.css("border-color"),
				borderWidth  = this_.css("border-width"),
				borderRadius = this_.css("border-radius").split(" "),
				Rtop_ = 0, Rright_ = 0, Rbottom_ = 0, Rleft_ = 0;

			//Colocar propiedades del Border	
				if ( borderRadius.length > 1 ) {
					Rtop_    = parseInt(borderRadius[0]),
					Rright_  = parseInt(borderRadius[1]),
					Rbottom_ = parseInt(borderRadius[2]),
					Rleft_ 	 = parseInt(borderRadius[3]);
					$("#borderRadiusCss").val( 0 );
				}else if( borderRadius.length <= 1 ){
					$("#borderRadiusCss").val( parseInt(borderRadius[0]) );
				}

					$("#RadiusTopLeft").val ( Rtop_ );
					$("#RadiusTopRight").val ( Rright_ );
					$("#RadiusBottompLeft").val ( Rleft_);
					$("#RadiusBottompRight").val ( Rleft_);
					$("#weightBorder").val( parseInt(borderWidth) );
					$("#colorBorder").val( borderColor );
			//fin border
			//Colocar padding
			if ( paddinCSS.length > 1 ){
				top_    = parseInt(paddinCSS[0]);
				bottom_ = parseInt(paddinCSS[1]);
				right_  = parseInt(paddinCSS[2]);
				left_   = parseInt(paddinCSS[3]);
				$("#paddingGlobal").val( 0 );
			}
			else if ( paddinCSS.length <= 1 ){
				$("#paddingGlobal").val( parseInt(paddinCSS[0]) );
			}//fin colocar padding

			if ( tranform_ != "none" ){
				var values = tranform_.split('(')[1],
			   	 	values = values.split(')')[0],
			   	 	values = values.split(',');
				var b = values[1];
				var angle = Math.round(Math.asin(b) * (180/Math.PI));
				$("#css-rotate-grades").val(angle);
			}else if ( tranform_ == "none" ){  $("#css-rotate-grades").val(0); }
			//console.info( tranform_ );
			//padding
			$("#paddingTop").val( top_ );
			$("#paddingBottom").val( right_ );
			$("#paddingRight").val( bottom_ );
			$("#paddingLeft").val( left_ );
			//Tamaño
			$("#sizeWidthCss").val(widthCSS);
			$("#sizeHeightCss").val(heightCSS);

			$(".element-dinamyc").removeClass("active");
			this_.addClass("active");

		var el_audio = this_.find("div.playSound");
			//Ayuda para reproducri audio
			if ( this.noHelps.indexOf("audio") == -1) {
				if ( el_audio.length >= 1 ){
					this.noHelps.push("audio");
					this.model.showAlert("warning","Si le das <b>Doble clic</b>, el audio se reproducirá. (Solo en la edición)");
				}
			}
	},
	/** check audio
    */
	playAudio:function(e){
		var this_ = $(e.currentTarget),
			doc   = this_.attr("data-child-audio"),
			state_audio = this_.attr("data-state"),
			audio = document.getElementById(doc),
			Img   = this_.find(".audioPlayIcon > div img"),
			attrImg;
		if ( state_audio == "false" ){
			audio.play();
			this_.attr("data-state","true");
			attrImg = "files/pausa.svg";
		}else if ( state_audio == "true" ){
			audio.pause();
			this_.attr("data-state","false");
			attrImg = "files/play.svg";
		}
		Img.attr("src",attrImg);

	},

});