var Collage = Collage || {};
/** The application main model
* @author Duvan Valencia
* @augments Backbone.View
* @constructor
* @name icons
*/
Collage.icons = Backbone.View.extend( /** @lends icons */ {
	"el":"#gf-ecm-config-lateral-menu",
	template:_.template( $("#view-icons").html() ),
	initialize: function(){ this.render(); },
	render: function(){
		this.$el.append( this.template( this.model.toJSON() ));
		$('.dropdown').dropdown();
		console.warn("%c---Plugin icons cargado---","color:#9933ff;font-size:12px;font-weight:bold;");
	},
	events: {
		"click ul li.show-galleries":"materials",
		"contextmenu ul.tooltip-avalaible li.draggable-initial":"options",
		"click .options li.item[data-action='custom']":"personalizar",
		"click .options li.item[data-action='remove']":"remove",
		"click .return-gallery": "returnMain",
		"click .icons-filter .item":"filterIconsGallery",
		"click .filter-chars .item":"filterCharactersGallery"
	},
	/** Filter for icons
    */
	filterIconsGallery: function(e){
		var this_ 	  = $(e.currentTarget),
			valFilter = this_.attr("data-value");
		if ( valFilter ) {
			$("ul#gallery-icons-list li").each(function(){
				var tagStyle = $(this).attr("data-style-icon");
					if ( tagStyle != valFilter ){
						$(this).fadeOut("fast");
					}else if (  tagStyle == valFilter ){
						$(this).fadeIn("fast");
					}
			});
		}
	},
	/** Filter for characters
    */
	filterCharactersGallery: function(e){
		var this_ 	   = $(e.currentTarget),
			typeFilter1   = $("#st-character").val(),
			genderFilter2 = $("#gd-character").val();

			if ( typeFilter1 && genderFilter2 ){
				$("ul#gallery-chars li").each(function(){
					var typeFlt = $(this).attr("data-type-filter"), 
						gendFlt = $(this).attr("data-gender-filter");
					if ( typeFilter1 != typeFlt || genderFilter2 != gendFlt){
						$(this).fadeOut("fast");								
						}
					else if ( typeFilter1 == typeFlt || genderFilter2 == gendFlt){
						$(this).fadeIn("fast");	
					}
				})
			}
	},
	/** Return to main menu icons
    */
	returnMain: function(){
		$(".behind-stage").addClass(this.model.get("inactive"));
		$("#main-menu-gallery").removeClass( this.model.get("inactive") );
		$(".edition-header .return").addClass( this.model.get("inactive") );
		$(".edition-header h5").text("Materiales:");
	},
	/** Remove character if th user is the same 
	*/
	remove: function(){
		var removethis = confirm("¿Deseas eliminar este contenido?");
		if ( removethis == true ){
				console.info("Si tu lo creaste, lo puedes eliminar");
		}
	},
	/** Select current materials (Iconios, personajes, imagenes) or open modal
	*/
	materials:function(e){
		var this_      = $(e.currentTarget),
			Gid = this_.attr("data-child-show"),//gallery id
			noDisappear = this_.attr("data-disapear")//No abrir galeria, Abrir modal
			if ( !noDisappear ){
				$(".edition-header div.return").removeClass( this.model.get("inactive") );
				$(".edition-header h5").text( this_.find(".title").text() +":");
				$(".behind-stage").addClass( this.model.get("inactive") )
				$("#"+Gid).removeClass( this.model.get("inactive") );
			} else{
				$('#modal-box').modal("show");
				$('.dropdown').dropdown();
				$('.accordion') .accordion({  selector: { trigger: '.title .icon'  } });
				
				this.model.showAlert("ok","!Modal de edición de personajes, abierto!");
			}
		e.stopPropagation();
	},
	/** Active contexMenu for gallery characters
	*/
	options: function(e){
		var this_ = $(e.currentTarget);
			e.preventDefault();
			var x = event.clientX;  
				var y = event.clientY; 
				$("#character-gallery .options")
					.removeClass( this.model.get("inactive") )
					.css({"left": (x-100)+"px", "top": (y-100)+"px"});
	}
});