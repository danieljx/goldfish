var Collage = Collage || {};
/** The application main model
* @author Duvan Valencia
* @augments Backbone.View
* @constructor
* @name cssPlugin
*/
Collage.cssPlugin = Backbone.View.extend( /** @lends cssPlugin */ {
	"el":"#gf-ecm-config-lateral-menu",
	template: _.template( $("#css-plugin").html() ),
	initialize: function(){
		this.render();
		console.warn("%c¡Plugin css Cargado!","font-size:12px;font-weight:bold;color:#9933ff;");
		$('.menu .item').tab();
		this.colorPicker();
	},
	/** Set the 'spectrum' properties to input#border-color
    */
	colorPicker: function(){
		$("#border-color").spectrum({ 
			color: "#fff",
			move: function(color) {
				$("#colorBorder").val( color.toHexString() );
			 }
		 });
	},
	render: function(){
		this.$el.append( this.template( this.model.toJSON() ))
	},
	events:{
		"click .css-group-link":"showMoreProperties",
		"keypress .cssPadding":"changePadding",
		"keypress .sizeCss":"changeSize",
		"keypress #css-rotate-grades":"changeRotation",
		"keypress .cssBorderRadius":"changeBorderRadius",
		"click .cssBorderRadius":"changeBorderRadius",
		"click #changeToMultiCurve":"completeBorderRadius"
	},
	/** Show the panel for complete border radius properties css
    */
	completeBorderRadius: function(e){
		var this_ 	  = $(e.currentTarget);
		var hasClass_ = this_.hasClass("data-active");
		var tag_style = $("#borderRadiusCss").attr("data-ref-style");
		if ( hasClass_ == true ){
			$("#borderRadiusCss").attr("data-ref-style","forParts");
		} else if (  hasClass_ == false ){
			$("#borderRadiusCss").attr("data-ref-style","complete");
		}
	},
	/** Set the css border-radius properties for selected element
    */
	changeBorderRadius: function(e){
		var din         = $( this.model.get("window") ).find(this.model.get("selected") ),
			this_       = $(e.currentTarget),
			tag_style   = $("#borderRadiusCss").attr("data-ref-style"),
			color       = $("#colorBorder").val(),
			ancho       = $("#weightBorder").val(),
			anchoIsNan  = isNaN(ancho),
			radius      = $("#borderRadiusCss").val(),
			radiusIsNan = isNaN(radius);

		if ( din.length >= 1 ){	
			if (e.which == 13){
				if (  anchoIsNan == false & ancho >= 0 & ancho <= 10 &  radiusIsNan == false ){
					if ( tag_style == "complete" ){ din.css("border-radius", radius+"px"); }
					else if ( tag_style == "forParts" ){
							$("input.cssBorderRadius[data-style-border]").each(function(){
								var attr_val = $(this).val();
								var attrName = $(this).attr("data-style-border");
									din.css( attrName, attr_val+"px" );
							});
						}
							din.css({
								"border-color": color,
								"border-style":"solid",
								"border-width": ancho+"px"
							 });
					}
				}
		}else{ this.model.showAlert("warning","No hay elementos selecionados"); }
	},
	/** Change the css rotate propertie for selected element
    */
	changeRotation: function(e){
		var this_ = $(e.currentTarget);
		var meVal  = this_.val(),
			NotNum = isNaN(meVal),
			din    = $( this.model.get("window") ).find(this.model.get("selected") );
		if ( din.length >= 1 ){
			if (e.which == 13){
				if (  NotNum == false & meVal >= -360 & meVal <= 360  ){
					  din.css( "transform","rotate("+meVal+"deg)" );
				}else{ this.model.showAlert("error","Por favor ingresa un valor <b>válido</b>"); }
			}
		}else{
			this.model.showAlert("warning","No hay elementos selecionados");
		}
	},
	/** Change the width and height properties for selected element and secondaries elements as images, audios, icons (well, into this selected element)
    */
	changeSize: function(e){
		var din    = $( this.model.get("window") ).find(this.model.get("selected") );
		var window_active = $("#window-edition").attr("data-selected");
			if ( window_active == "true" ) {
				$("input.sizeCss[data-size-attr]").each(function(){
					var meVal  = $(this).val(),
						NotNum = isNaN(meVal),
						attrName  = $(this).attr("data-size-attr");
						$("#window-edition").css( attrName, meVal+"px" );
				})
			}else if (  window_active == "false" ){
				if ( din.length >= 1 ){
					if (e.which == 13){
						$("input.sizeCss[data-size-attr]").each(function(){
							var meVal  = $(this).val(),
								NotNum = isNaN(meVal),
								attrName  = $(this).attr("data-size-attr");

							var hasImage  =  din.find(".dynamic-image"),
								hasObjec  = din.find(".set-color-svg"),
								hasSound  = din.find(".playSound");

							if (  NotNum == false & meVal >= 0 & meVal <= 600  ){

									din.css( attrName, meVal+"px" );
									if ( hasImage.length >= 1){ hasImage.css(  attrName, meVal+"px" ); }
									if ( hasObjec.length >= 1 ){ hasObjec.css(  attrName, meVal+"px" ); }
									if ( hasSound.length >= 1 ){
										hasObjec.css(  attrName, (meVal-20)+"px" );
										hasSound.css(  attrName, (meVal-20)+"px" );
									}
							}
						})
					}
				}else{ this.model.showAlert("warning","No hay elementos selecionados"); }
		}
	},
	/** Change the padding propertie for selected element 
    */
	changePadding: function(e){
		var this_        = $(e.currentTarget),
			completeAttr = this_.attr("data-ref-style"),
			meVal        = this_.val(),
			NotNumber    = isNaN( meVal ),
			din          = $( this.model.get("window") ).find(this.model.get("selected") );
		
		if ( din.length >= 1 ){
			if (e.which == 13){
				if ( NotNumber == false & meVal >= 0 & meVal <= 40 ) {
					if ( completeAttr == "complete" ){
						din.css("padding", meVal+"px");
					}else{
						$("input.cssPadding[data-style-attr]").each(function(){
							var attrName   = $(this).attr("data-style-attr");
							var attr_value = $(this).val();
							din.css( attrName, attr_value+"px" );
						})
					}	
				}else{ this.model.showAlert("error","Por favor ingresa un valor <b>válido</b>")}
			}		
		}else{ this.model.showAlert("warning","No hay elementos selecionados"); }		
	},
	/** Show the complete properties
    */
	showMoreProperties: function(e){
		var this_    = $(e.currentTarget),
			attr_    = this_.attr("data-show"),
			classHas = this_.hasClass("data-active");

		$(".unlinkProperties").addClass( this.model.get("inactive") );
		if ( attr_ ){ 
			if ( classHas == false ){
				 this_.addClass("data-active");
				$("#"+attr_).removeClass( this.model.get("inactive") )
			}
			else if ( classHas == true ) {
					console.info("true!");
				 this_.removeClass("data-active");
				$("#"+attr_).addClass( this.model.get("inactive") )
			}
			
		}
	}
})