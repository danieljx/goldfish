var Collage = Collage || {};
/** The view for upper side panel
* @author Duvan Valencia
* @augments Backbone.View
* @constructor
* @name leftSideBar
*/
Collage.leftSideBar = Backbone.View.extend( /** @lends leftSideBar */ {
	"el":"#side-left-menu",
	plugins: [],
	template: _.template( $("#left-side-bar").html() ),
	render: function(){
		console.info("%c///barra lateral izquierda///","font-size:14px;color:green;font-weight:bold;")
		this.$el.append( this.template( this.model.toJSON() ));
	},
	initialize: function(){ this.render(); },
	events: {
		"click li.left-tool": "activePanel",
		"click article.left-side-bar-edition #close":"closepPanel"
	},
	/** Show active plugin on the left-side-bar-edition panel
    */
	activePanel: function(e){
		var this_ = $(e.currentTarget);
		var viewEdit = this_.attr("data-view");
		$("li.left-tool").removeClass("left-active");
			this_.addClass("left-active");
		$(".plugins").addClass( this.model.get("inactive") )
		switch ( viewEdit ){
			case "Multimedia":
				$("#view-multi-section").removeClass( this.model.get("inactive") );
			break;
			case "icons":
				$("#plugin-icons").removeClass( this.model.get("inactive") );
				this.model.componentsWidget();
			break;
			case "text":
				$("#view-text-plugin").removeClass( this.model.get("inactive") );
				this.model.componentsWidget();
			break;
			case "css":
				$("#view-css-plugin").removeClass( this.model.get("inactive") );
				this.model.componentsWidget();
			break;
		}
		$(".left-side-bar-edition").removeClass( this.model.get("inactive") );
	},
	/** close ppanel de edición = left-side-bar-edition panel
    */
	closepPanel: function(){
		console.warn(this.model.get("inactive"));
		$(".left-side-bar-edition").addClass( this.model.get("inactive") );	
	}	
});

