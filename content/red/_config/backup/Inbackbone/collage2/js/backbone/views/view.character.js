var Collage = Collage || {};

Collage.character = Backbone.View.extend({
	"el":"#modal-box",
	selectedClass:'svg-selected',
	mainsvgId:"customer-character",
	parentId:'',
	template: _.template( $("#character").html() ),
	initialize: function(){
		this.render();
		this.putFeatures();
		//transform: scale(0.5,0.5);
	},
	events:{
		"click .menu-filter div.item": "filter",
		"click #features div.custom-sets": "insertNewFeature",
		"click #delete-customer-svg": "deleteSvg",
		"click #listingCss":"listarColores",
		"click .editAnimOptions":"animateOptions",
		"click .close-tooltip-animation":"close",
		"click .save-config-animation":"save",
		"click .checkBoxAnimation":"checkExits",
		"click .start-prev-naimation":"startAnimation",
		"click #save-complete-svg":"saveCompleteSvg",
		"change .stiles-control-input input":"transformCss"
	},
	transformCss:function(e){
		console.info("%cTransformamos el svg","font-weight:bold;color:#000;");
		var this_ = $($(e.currentTarget));
		var attr_transform = this_.attr("data-transformation");
		var selecionado = $("#"+this.mainsvgId).find("."+this.selectedClass);

		if ( selecionado.length >= 1 ){
			if( attr_transform ){
				switch (attr_transform) {
					case "scale":
					var x_ = $(".stiles-control-scale-x").val(),
						y_ = $(".stiles-control-scale-y").val();
							selecionado.attr("style","transform: scale("+x_+","+y_+")");
						break;
					case "rotate":
							selecionado.attr("style","transform: rotate("+this_.val()+"deg)");
						break;
					default:
						this.model.showAlert("error","No existe información para esta transformación");
					break;
				}
			}
			else{
				this.model.showAlert("error","Se ha producido un error en la transformación");
				}
			}else{
				this.model.showAlert("warning","No hay elementos seleccionados");
			}
	},
	tmp_Image: function(file){
		var id_ = this.model.createId('character');
			var img_ = $("<img/>");
				img_.attr("class",'dynamic-image');
				img_.addClass("gf-object-image");
				img_.attr("src",file);
				img_.attr("id",id_);
		return img_;
	},
	saveCompleteSvg: function(e){

		//width: 389px; height: 392px;
		var svg_ = $("#preview-custom-character").html();
		var result = '';
		var codeData = {
				"folder": this.model.get("folders"),
				"name"   :this.model.createId("character"),
				"type"   : "flat",
				"gender" : "female",
				"emotion": "uno,dos",
				"animation": "talk,toBlink", 
				"codeImg": svg_
			};
			
		$.ajax({
			url: "php/create-svg.php",
			type:"post",	
			async:false,
			data: codeData,
			 	success: function(re){
			        	re = result;	
			    }
			});
			var tmpImage = this.tmp_Image( "files/recursos/Personajes/"+codeData.name+".svg" );
			var tmpDynam = this.model.templateDinamyc( tmpImage );

		 $(this.model.get("window")).append( tmpDynam );
				this.model.componentsWidget();
	},
	startAnimation: function(e){
		var this_  = $(e.currentTarget),
			id_    = this_.attr("data-svg-exits"),
			Exists = $("svg#customer-character").find("#"+id_).length,
			dataId = $("#"+this_.attr("data-parent"));

		if( Exists <= 0 ){
			this.model.showAlert("error","No se puede animar, el elemento <b>no existe</b>");
		}else{
			console.warn("repeat: "+dataId.attr("data-reproduction"));
			console.warn("velocidad: "+dataId.attr("data-velocity"));
			console.warn("evento: "+dataId.attr("data-event"));
			console.info(id_);
			//$("#"+id_).trigger("click");
		}
	},
	removeSelect: function(e){
			console.info("Removiendo selección");
			$("#customer-character").find("g.svg-selected").removeClass("svg-selected")	
	},
	checkExits: function(e){
		var this_  = $(e.currentTarget),
			alertText = "",
			id_    = this_.attr("data-svg-exits"),
			Exists = $("svg#customer-character").find("#"+id_).length;
		
			switch (id_) {
				case "eyes":
					alertText = "El personaje no tiene <b>ojos</b>. Cuando los tenga, la animación se ejecutará ";
				break;

				case "mouth":
					alertText = "El personaje no tiene <b>Boca</b>. Cuando tenga, la animación se ejecutará ";
					break;
			}

		if ( Exists <= 0){
			this.model.showAlert("warning",alertText);
		}
	},
	save: function(){
		var id_parent = this.parentId;
		this.close();
		var toggleRepeat   = $("#repeat-config"),
			toggleEvent    = $("#event-config"),
			inputVelocity  = $("#velocity-config");

		$("#"+id_parent)
			.attr("data-reproduction", toggleRepeat.prop("checked"))
			.attr("data-velocity", inputVelocity.val())
			.attr("data-event", toggleEvent.prop("checked"));
	},
	close: function(){
		this.parentId = "";
		$("#basic-animation-tooltip").addClass(this.model.get("inactive"));
	},
	animateOptions: function(e){
		var id_parent = $(e.currentTarget).attr("data-parent");
		this.parentId = id_parent;
		var toggleRepeat   = $("#repeat-config"),
			toggleEvent    = $("#event-config"),
			inputVelocity  = $("#velocity-config");

		var stateRepeat	 = $("#"+id_parent).attr("data-reproduction"),
			stateEvent	 = $("#"+id_parent).attr("data-event"),
			valVelocity  = $("#"+id_parent).attr("data-velocity");

		$("#basic-animation-tooltip").removeClass(this.model.get("inactive"));

		( stateRepeat == "false")  ? toggleRepeat.prop("checked",false) : toggleRepeat.prop("checked", true);
		( stateEvent == "false" )  ? toggleEvent.prop("checked",false) : toggleEvent.prop("checked", true);
		inputVelocity.val(valVelocity);
		console.info( this.parentId );
	},
	deleteSvg:function(){
		var confirm_ = confirm("¿Desea eliminar esta imagen?");
		if ( confirm_ == true ){
			$("."+this.selectedClass).remove();
				this.model.showAlert("ok", "Imagen eliminada exitosamente");
			}
		},
	listarColores: function(){
		console.info("%cListando colores","font-weigh:bold;color:#000;");
		var elemento = $("#customer-character").find(".svg-selected");

		
		if ( elemento.length >= 1 ){
			$(".color-section").html("");
			var styleTag = elemento.find("style");
			var class_   = elemento.find("style").html().split(" ");
			var id_		 = class_[0].trim();
			$(".color-section").append("<h6>colores</h6>");
			//console.warn("Id: "+ id_);
				for (var i = 1; i <= class_.length - 1; i++) {
					var currentClass = class_[i].split("{")[0];
					var color_fill = $(id_).find(currentClass).css("fill");
					//console.warn( "clase: "+ currentClass);
					var color_ = $("<input/>");
						color_.attr("type","text");
						color_.attr("value",color_fill);
						color_.attr("class","custom-color");
						color_.attr("data-class",currentClass);
						color_.attr("data-id",id_);
					$(".color-section").append(color_);
				}
		
				$(".custom-color").spectrum({
					showAlpha: true,
					chooseText: "OK",
		   			cancelText: "Cancelar",
				    move: function(color) { 
				    	var id_parent  = $(this).attr("data-id"),
				    		myClass    = $(this).attr("data-class"),
				    		colorFinal = color.toHexString();
				    	$("#customer-character").find(id_parent).find(myClass).attr("style","fill:"+ colorFinal);
				    	
				    }
				});
			
		}else{
			this.model.showAlert("error", "<b>No se puede listar los colores</b> porqué no hay elementos seleccionados ");
		}
	},
	//añadir elemento al svg
	insertNewFeature: function(e){
		

		var this_ = $(e.currentTarget);
		var url   = this_.find("img").attr("src");

		var currId = this_.attr("data-tag-filter");

		var selectSvg =  function(){ 
			console.info("trigger");
			$("#customer-character").find("g.svg-selected").removeClass("svg-selected");
			$(this.node).attr("class","svg-selected");
				//nextFrame( this , parpadeo, 0);
			 }

		var move = function(dx, dy){
			var this_ 	 = $($(e.currentTarget)),
				elSelect = $(".svg-selected");
		//elSelect.css("transform","translate("+dx+"px,"+dy+"px) scale(0.7,0.7)");

		var transformMatrix = $(".svg-selected").css("transform");
				console.warn( transformMatrix );
			if ( transformMatrix == "none" ){
				elSelect.css("transform","translate("+dx+"px,"+dy+"px)");
			}else{
				
				var valuesMatrix = transformMatrix.split("matrix(")[1].split(",");
				var scaleX = valuesMatrix[0];
				//var skewY  = valuesMatrix[1];
				//var skewX  = valuesMatrix[2];
				var scaleY = valuesMatrix[3];
				//var transX = valuesMatrix[4];
				//var transY = valuesMatrix[5];

				elSelect.css("transform","translate("+dx+"px,"+dy+"px) scale("+scaleX+","+scaleY+")");

				//console.info( transformMatrix );
				//console.info( scaleX );
				//console.info( skewY );
				//console.info( skewX );
				//console.info( scaleY );
				//console.info( transX );
				//console.info( transY );
			}

  
			/*
				this.attr({
					transform: this.data('origTransform') + (this.data('origTransform') ? "T": "t")+ [dx, dy]
				})*/
		}
		
		var start = function(){
				this.data('origTransform', this.transform().local )
		}
		var stop = function(){
				
				$(".gf-incons-save-btn button.dropGroupSvg").attr("data-drop-id");
		}

		 		var s = Snap("#customer-character");
		 		    g = s.group();
		 		 $("#character #"+currId).remove()
				var tux = Snap.load( url , function ( loadedFragment ) {
					//console.warn(loadedFragment);
							g.append( loadedFragment );
		                    g.click( selectSvg );
						    g.drag( move, start, stop);
		                } );
		},
	filter:function(e){
		var filterTag     = $("#filter-tools").val() ? $("#filter-tools").val() : "";
	 	var filterStyle   = $("#style-char").val() ? $("#style-char").val() : "";
	 	var filterGender  = $("#gender-char").val() ? $("#gender-char").val() : "";
	 	var filterEmotion = $("#emotion-char").val() ? $("#emotion-char").val() : "";

	 	$("#features div").each(function(){
	 		var filterTag_    = $(this).attr("data-tag-filter");
	 		var filterStyle_  = $(this).attr("data-type-filter");
	 		var filterGender_ = $(this).attr("data-gender-filter");
	 		var filterEmotion_= $(this).attr("data-emotion-filter");

	 		if ( filterStyle != filterStyle_ ){ $(this).fadeOut("fast"); }

	 			else if ( filterStyle == filterStyle_ ){
	 					//mostra o oculta los generos
	 						
	 				if ( filterGender_.indexOf(filterGender) == -1 )
	 					{ $(this).fadeOut("fast"); console.info("Ocultar genero"); }
	 				else if ( filterGender_.indexOf(filterGender) != -1 )
	 					{ 
	 						//Muestra y oculta las emociones
	 					if ( filterEmotion_.indexOf(filterEmotion) == -1 )
	 						{ $(this).fadeOut("fast"); console.info("Ocultar Emoción"); }
	 					if ( filterEmotion_.indexOf(filterEmotion) != -1 )
	 						{ 	
	 					if ( filterTag_.indexOf(filterTag) == -1){ $(this).fadeOut("fast"); }
	 					if ( filterTag_.indexOf(filterTag) != -1){ 
	 										$(this).fadeIn(); 
	 									}
	 								}
	 						}
	 				}
	 			})
	},
	render: function(){
		this.$el.append( this.template( this.model.toJSON() ) );
	},
	features: function(){
		var response = '';
		$.ajax({
			url: 'json/resources.json',
			type:"get",
			async:false,
				success: function(res) { response = res; },
				error : function(error){ response = error; }
		});
		return response;
	},
	putFeatures: function(){
		var res = this.features();
		for (var i = 0; i <= res.length - 1; i++) {
			var div_ = $("<div/>");
				div_.addClass("custom-sets");
				div_.attr("data-tag-filter",res[i].tag);
				div_.attr("data-type-filter",res[i].type);
				div_.attr("data-gender-filter",res[i].gender);
				div_.attr("data-emotion-filter",res[i].emotion);
						
			var img_ = $("<img/>");
				img_.attr("src",'files/recursos/materials/'+res[i].file);
				div_.append(img_);

				$("#features").append(div_);
		}
	},

});