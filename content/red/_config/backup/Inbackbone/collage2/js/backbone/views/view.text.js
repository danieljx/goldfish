var Collage = Collage || {};
/** The application main model
* @author Duvan Valencia
* @augments Backbone.View
* @constructor
* @name texto
*/
/** All events about text as create it or edit it
  * are into view.window.js. because the double click event 
  * is triggered into div#window-edition 
  */
Collage.texto = Backbone.View.extend( /** @lends texto */ {
	"el":"#gf-ecm-config-lateral-menu",
	template: _.template( $("#text-plugin").html() ),
	initialize: function(){
		this.render();
	},
	render: function(){
		console.warn( "%c¡Plugin texto, cargado!","font-size:12px;font-weight:bold;color:#9933ff;" );
		this.$el.append( this.template(this.model.toJSON()) );
	}
});