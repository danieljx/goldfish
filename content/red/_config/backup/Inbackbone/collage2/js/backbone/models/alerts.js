var Collage = Collage || {};

Collage.alert = Backbone.Model.extend({
	defaults:{
		classA: "alert-danger",
		icon: "glyphicon-remove",
		text: "Muestra alertas por defecto"
	},
	initialize: function(){
		console.info("%c**** Modelo alerta **** ","font-size:15px;font-weight:bold;color:#804000;");

		this.getAlert();
	},
	getAlert: function(){
		var alert = new Collage.alertView( {model: this} );
	}
});