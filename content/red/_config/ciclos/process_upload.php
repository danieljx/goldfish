<?php
set_include_path('../../../library');
require_once("Zend/Loader.php");
require_once("Local/config.load.php");
$configXml = getConfig();
$pathComponents = explode(DIRECTORY_SEPARATOR, dirname(__FILE__));

Zend_Loader::loadClass('Zend_Controller_Front');
Zend_Loader::loadClass('Zend_Config_Xml');

$_PATH = new Zend_Config_Xml($configXml, 'path');

// Mats 2014.03.13
// Recibe un archivo de sonido y crea copias en mp3 y ogg (

$fileName = $_REQUEST['file_name'];
$filePath = $_REQUEST['file_path'];
$result = array("error" => "", "path_html" => "");

//$zip = new ZipArchive;
//$res = $zip->open($filePath.$fileName);
//$extractTo = $filePath.  str_replace(".zip", "", $fileName);

$log = fopen($_PATH->sys->dataroot . '/log_audiconvert.txt', 'a+');
$m = microtime();
$tmpFile = date('Ymd_His_') . substr($m, strpos($m, '.') + 1, 6);
$file = $fileName;
fwrite($log, "fileName:".$fileName . PHP_EOL);
fwrite($log, "filePath:".$filePath . PHP_EOL);
fwrite($log, "dir:".dirname(__FILE__) . PHP_EOL);

$basedir = $_PATH->sys->dirroot.DIRECTORY_SEPARATOR.$_PATH->sys->appfolder.DIRECTORY_SEPARATOR;
//for ($i = 0; $i < count($pathComponents) - 3; $i++) {
//    $basedir .= $pathComponents[$i].DIRECTORY_SEPARATOR;
//}

fwrite($log, "basedir:".$basedir . PHP_EOL);
$ext = substr($fileName, strrpos($fileName, '.')+1);
$base = substr($fileName, 0, strrpos($fileName, '.'));

fwrite($log, "extension:".$ext . PHP_EOL);
fwrite($log, $basedir.'filters'.DIRECTORY_SEPARATOR.'ffmpeg -i "' .$filePath.$fileName. '" "' .$filePath.$base. '.mp3" 2>&1'. PHP_EOL);

if ($ext != 'mp3') {
    fwrite($log, shell_exec($basedir.'filters'.DIRECTORY_SEPARATOR.'ffmpeg -i "' .$filePath.$fileName. '" "' .$filePath.$base. '.mp3" 2>&1'));
}
if ($ext != 'ogg') {
    fwrite($log, shell_exec($basedir.'filters'.DIRECTORY_SEPARATOR.'ffmpeg -i "' .$filePath.$fileName. '" "' .$filePath.$base. '.ogg" 2>&1'));
}

//$tmpFile .= '.wav';
//
//$tmpFile = $_PATH->sys->dataroot . '/records/' . $tmpFile;
//$newFile = $_PATH->sys->dataroot . '/records/' . $file;
//move_uploaded_file($_FILES['Filedata']['tmp_name'], $tmpFile);
//
//fwrite($log, $tmpFile . PHP_EOL);
//fwrite($log, shell_exec('lame -V2 "' . $tmpFile . '" "' . $newFile . '" 2>&1'));
//
//fwrite($log, PHP_EOL);

$res = file_exists($filePath.$base.'.mp3') && file_exists($filePath.$base.'.ogg');

if ($res) {
    $result["path_html"] = $base;
} else {
    $result["error"] = 1;
}

header('Content-Type: application/json');
echo json_encode($result);
?>

