function events(){
	var myself = this;
   this.current_Upload = '';
	
   	this.addAudio = function( parent, file, twice ){
      var audio_ = "<audio id='"+parent+"' data-folder='uploads'>";
     	 audio_ += '<source src="'+file+'.mp3" type="audio/mpeg" class="mp3">';
	      if ( twice != 'false'){
				audio_ += '<source src="'+file+'.ogg" type="audio/mpeg" class="ogg">';
	      }
     	 audio_ += '</audio>';
     	 return audio_;
	}

   //voice
	this.generateVoice = function(texto, params){
		var variablesURL;
		    var ubicacionTTS = "../../../tts/";
		    var archivo      = 'index.php';
		    var genFolder    = 'files/';
		    var metodo       = 'GET';
		    var txtEsc       = "txt=" + escape(texto); //Preparar texto "escapado" para inserción en la URL  
		    variablesURL = "idi=" + params.lang + "&ora=" + params.gender + "&vel=" + params.speed + "&ton=" + params.tone + "&" + txtEsc;
			
			$.ajax(ubicacionTTS + archivo,
		    {
		        "type": metodo,   // usualmente post o get
		        async:false,
		        data: variablesURL,        
		        beforeSend: function () {
		        	$("#estu")
		            .removeClass("alert-danger")
					.addClass("alert-info")
		            .css("display","block")
		            .text("Procesando, espere por favor..."); 
		        },
		        success: function(respuesta){
		        	$("div#sound_").empty();
		        	var soundPath = datafolder+"/tts/" + respuesta;
					
					var parent    = $("#insertNewNodes"), id_parent = parent.attr("data-id-to-edit") ;

					/*ICONO DE AUDIO*/
					$("#main-"+id_parent)
					.find(".audio-media-"+id_parent)
					.removeClass("ecm-media-inactive")
					.addClass("ecm-media-active")
					.attr({ 
							"data-media-file": soundPath,
							"data-media-convert": "true" 
						});

					$("#Audio-"+id_parent).remove();
			
					var Audio_ = '<audio id="Audio-'+id_parent+'" data-state-audio="true" data-folder="tts">';
							Audio_ +='<source src="../../../../'+soundPath+'.mp3" type="audio/mpeg">';
						Audio_ +='</audio>';

					$("#main-"+id_parent).find(".box-type-sound").append( Audio_ );

					var valText = $("#editTextEdition").val();
			    		
					$("#estu")
		            .removeClass("alert-danger").addClass("alert-success")
		            .css("display","block").text("audio creado Exitosamente");
		            alert("Conversión realizada exitosamente");
				 },
		        error: function(respuesta) {
		            $("#estu")
		            .removeClass("alert-success")
					.addClass("alert-danger")
		            .css("display","block")
		            .text("Ha ocurrido un error "+respuesta);
		        },	    	 
		});   
	}//fin generateVoice

   this.colorPicker = function(){
  	 	var clickActive;

		$('.color-option').click(function(){
			clickActive = $(this).attr('data-type');
		});

		$('.color-option').ColorPicker({
			onChange: function (hsb, hex, rgb) {
				//console.warn( hex );
				var eleGlobal       = $('#insertNewNodes');
				var ElementToChange = eleGlobal.attr("data-type-element"),
				    IdParentGlobal  = eleGlobal.attr("data-id-to-edit");
				var editGlobal      = $("#main-"+IdParentGlobal)
				var useThisElem;
				
		//cambiando color de letra y color del fondo del titulo principal
		if ( $('#insertNewNodes').attr("data-type-element") == 'title' )
			{
				if ( clickActive == 'letterColor'){ $("#"+IdParentGlobal).css("color","#"+hex); }
				if ( clickActive == 'BGColor' ){ $("#"+IdParentGlobal).css("background-color","#"+hex); }
			}

				if ( ElementToChange == 'text_cir' ){
						useThisElem = editGlobal.find('textarea.ecm-edition-page-text-edition');
				}
				else if( ElementToChange == 'circle' ){
					var circleEl = editGlobal.find(".ecm-edition-page-circle");
						if ( clickActive == 'letterColor' ){
							circleEl.find('input').css("color","#"+hex);
						}else if ( clickActive == 'BGColor' ){
							circleEl.css("background-color","#"+hex);
						}
				}

				if ( clickActive == 'letterColor' ){
					if ( ElementToChange == 'text_cir' ){
							editGlobal
							.find('.box-global-container')
							.find(".box-type-text")
							.css("color","#"+hex);

					}
				}
				else if ( clickActive == 'BGColor' ){
					if ( ElementToChange == 'text_cir' ){
							editGlobal
							.find('.box-global-container')
							.css("background-color","#"+hex);
					}
				}

			}
		});
   }//end colorPicker

   this.uploadImage = function(){
   	/*global window, $ */
		$(function () {
		    'use strict';
		    // Change this to the location of your server-side upload handler:
		    var url = '../../jquery-file-upload-handler.php';
		 
		    $('.image-upload').fileupload({
		    	dropZone: $(this),
		        url: url,
		        dataType: 'json',
		        autoUpload: true,
		        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
                maxFileSize: 1 * 1000000, // 1 Mb
		        done: function (e, data) {
		        	var file_ = data.result.files[0];

		        	var parent    = $("#insertNewNodes"), id_parent = parent.attr("data-id-to-edit") ;

					$("#main-"+id_parent)
					.find('.box-global-container')
					.find('.box-type-img')
					.find('img')
					.attr("src", window.location.origin+"/"+datafolder+"/uploads/"+file_.name)
					.attr("data-folder","uploads");

						$("#main-"+id_parent)
						.find(".image-media-"+id_parent)
						.removeClass("ecm-media-inactive")
						.addClass("ecm-media-active")
						.attr("data-media-file", file_.name);

				},
		      	progressall: function (e, data) {
					var progress  = parseInt(data.loaded / data.total * 100, 10);
					
					$(this).parent().siblings()
					.find('.progress-bar')
					.css('width', progress + '%');
					
				}
		    })
		    .on('fileuploadfail', function(e, data) {
					alert("FALLO!!")
			})
            .prop('disabled', !$.support.fileInput)
		    .parent().addClass($.support.fileInput ? undefined : 'disabled');
		});
   }//uploadImage

   this.uploadAudio = function(){
   		/*Subir archivos*/
		$(function () {
			'use strict';
			// Change this to the location of your server-side upload handler:
			var url = '../../jquery-file-upload-handler.php';
			$('.upload_sound').fileupload({
					dropZone: $(this),
					url: url,
					dataType: 'json',
			        autoUpload: true,
			        acceptFileTypes: /(\.|\/)(mp3|ogg|wav|wma)$/i,
			        maxFileSize: 50 * 1000000, // 5 Mb
				done: function (e, data) {
					$.each(data.result.files, function (index, file) {
				 });
				},
				
				progressall: function (e, data) {
					var progress = parseInt(data.loaded / data.total * 100, 10);
					        $('#progress_audio .progress-bar').css( 'width', progress + '%' );
					        }
					})
					.on('fileuploadfail', function(e, data) {
								alert("FALLO!!")
					})
					.on('fileuploaddone', function(e, data) {
						var file = data.result.files[0];
							
					$.ajax({
						    type: "POST",
						    url: "process_upload.php",
						    data: {file_name: file.name, file_path: data.result.physPath},
						    dataType: 'json',
						error : function(error){
						    alert(error);
						},
						success: function(result) {
							var parent = $("#insertNewNodes"), id_parent = parent.attr("data-id-to-edit") ;

							$("#main-"+id_parent)
							.find(".audio-media-"+id_parent)
							.removeClass("ecm-media-inactive")
							.addClass("ecm-media-active")
							.attr("data-media-file", result.path_html );

							$("#Audio-"+id_parent).remove();
							
							var audioAppend = myself.addAudio( "Audio-"+id_parent , window.location.origin+"/"+datafolder+"/uploads/"+result.path_html , 'false');
							
							$("#main-"+id_parent).find(".box-type-sound").append( audioAppend );
							$("#main-"+id_parent)
								.find(".box-type-sound")
									.find('.play-audio')
										.css("visibility","visible")
											.attr({
												"data-audio-tag": "Audio-"+id_parent,
												"data-sound-active":"true"
											});

						}
					});
				})
			            .prop('disabled', !$.support.fileInput)
					    .parent().addClass($.support.fileInput ? undefined : 'disabled');
					});
   }//uploadAudio

}//end events

var events_ = new events();