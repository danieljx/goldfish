function circleApp(){
	var mySelf = this;
	this.functions_app = function(){ 
		$("#edit-title-main").click(function(){
			$(".ecm-edition-banner-right").css("display","block");
			$(".ecm-edit-without-this").css("display","none");
			$('.ecm-edition-page-icon-edit').removeClass("activePencil");
			$(this).addClass("activePencil");
			$("#insertNewNodes").attr({ 'data-id-to-edit': 'MainTitle', 'data-type-element': 'title' });
	});

		$(".ecm-icon-tools")
			.on("click", function(){
				$(".ecm-icon-tools").removeClass('active_');
				$(this).addClass('active_');
				$(".panel-tools-global").addClass("inactive-panel");
			    $(".ecm-tools-box").css({"display":"block","top":$(this).attr('data-top-box')+"px"});
			    $("#"+$(this).attr('data-son-active')).removeClass('inactive-panel');
			    $(".tools-tittle").text($(this).attr("data-text"));
		});

		$(".menu-audio li").click(function(){
			$(".menu-audio li").removeClass("active");
			$(this).addClass('active');
			$(".tools-audio-box").css("display","none");
			$("#"+$(this).attr("data-ref-id-child")).css("display","block");
		});

		$(".ecm-edition-banner-header").click(function(){
			$(".ecm-edition-banner-right").css({"display":"none"});
		});	

		$("select#fontType").on("change", function(){

			var eleGlobal       = $('#insertNewNodes');
			var ElementToChange = eleGlobal.attr("data-type-element"),
				IdParentGlobal  = eleGlobal.attr("data-id-to-edit");

				if ( ElementToChange == 'title' ){
					$("#"+IdParentGlobal).css("font-family", $(this).val() );
				}
				else if ( ElementToChange == 'text_cir' ){
					$("#main-"+IdParentGlobal)
					.find(".box-type-text")
					.css("font-family", $(this).val() );	
				}
				else if ( ElementToChange == 'circle' ){
					$("#main-"+IdParentGlobal)
					.find(".ecm-edition-page-circle")
					.find("input")
					.css("font-family", $(this).val() );
				}
		});

		$("input#fontSize").on("change", function(){
				var eleGlobal       = $('#insertNewNodes');
				var ElementToChange = eleGlobal.attr("data-type-element"),
					IdParentGlobal  = eleGlobal.attr("data-id-to-edit");

				if ( ElementToChange == 'title' ){
				 	$("#"+IdParentGlobal).css("font-size", $(this).val()+"px" );
				 }
				 else if ( ElementToChange == 'text_cir' ){
					$("#main-"+IdParentGlobal)
					.find(".box-type-text")
					.css("font-size", $(this).val()+"px" );	
				}
				else if ( ElementToChange == 'circle' ){
					$("#main-"+IdParentGlobal)
					.find(".ecm-edition-page-circle")
					.find("input")
					.css("font-size", $(this).val()+"px" );
				}
		});

		$("#tools-panel").click(function(){
			$(".ecm-tools-box").css({"display":"none"});
		});

		//Generando Audio
			$("#saveAudioConvet").click(function(){
				var params = [];
				var parent_id = $("#insertNewNodes").attr("data-id-to-edit");
				var txt_ = $("#main-"+parent_id).find('textarea#text_cricle_'+parent_id).val();
				
				if( $("#gender").val() == '' ){ alert("Debes escoger un genero"); }
					else if( $("#lang").val() == '' ){ alert("Debes escoger un Idioma"); }
			 		else if(txt_ == ''){ alert('No hay ningún texto'); }
					else{
						params.speed  = $("#speed").val();  params.tone   = $("#tono").val();
						params.gender = $("#gender").val(); params.lang   = $("#lang").val();
						events_.generateVoice( txt_ , params);
					}	
			});


	}

	this.fnc_circles = function(elId){

		$(".edition-close ul li.action-"+elId).on('click', function(){
			var nmbr = $("#circles-edition").find("div.ecm-edition-pages-content").length;
			if (nmbr > 3) { $("#main-"+elId).fadeOut('slow', function(){
					$(this).remove();
				}) }	else{
				mySelf.showAlerts("El limite Minimo es de 3");
			}	
		});

		$("#main-"+elId)
		.find(".ecm-edition-page-circle-number")
		.focus(function(){
			$(this).css({"background-color":"#fff", "color":"#000"});
		})
		.blur(function(){
			var realColor = $(this).parent().css("background-color");
			$(this).css({"background-color":realColor, "color":"#fff"});
		});

		$("#main-"+elId)
		.find("#close-prev")
		.click(function(){
			$("#main-"+elId)
			.find(".ecm-edition-page-prev").fadeOut();
			$("#action-"+elId)
			.attr("data-active-preview","false")
			.find("span")
			.removeClass('glyphicon glyphicon-eye-close')
			.addClass("glyphicon glyphicon-eye-open");
		})

		$("nav#hiddeThis-"+elId).on('click', function(){
			var state_panel = $(this).attr("data-show");

			if ( state_panel == "true" ){
			 $(this).attr("data-show", "false"); 
			 $(this).find("span").removeClass("glyphicon-chevron-up").addClass("glyphicon-chevron-down");
			} 
			else if ( state_panel == "false" ){
			 $(this).attr("data-show", "true");
			 $(this).find("span").removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-up");
			  }
			$(this).parent().parent().find(".ecm-edition-page").fadeToggle();
			$(this).parent().parent().find('.header-hidde').toggleClass('ecm-header-content')
		});

		$("#text_cricle_"+elId).keypress(function(){
			$("#box-type-text"+elId).text( $(this).val() );
		});

		$("#play-audio-"+elId).click(function(){
			var state = $(this).attr("data-state-sound");

			var audio = document.getElementById($(this).attr("data-audio-tag"));
				if( state == "false") { 
					$(this).attr("data-state-sound","true"); audio.play(); 
					$(this).addClass("glyphicon-pause").removeClass("glyphicon-play");;
				}
				else if( state == "true") {
				 $(this).attr("data-state-sound","false"); audio.pause();
				 $(this).addClass("glyphicon-play").removeClass("glyphicon-pause");;
				  }
		})

		$("li#edition-pencil-"+elId).on("click", function(){
			if($(this).attr("data-id-parent") && $(this).attr("data-element-edit")){
				var parent_id = $(this).attr("data-id-parent"), eleEdition = $(this).attr("data-element-edit") ;
				$(".ecm-edition-banner-right").css("display","block");
				if ( eleEdition == "circle" ) { $(".ecm-edit-without-this").css("display","none");} 
				else{ $(".ecm-edit-without-this").css("display","block"); }
					$("#edit-title-main").removeClass("activePencil");
					$('.ecm-edition-page-icon-edit').removeClass("activePencil");
					$(this).parent().parent().addClass("activePencil");
					$("#insertNewNodes").attr({ 'data-id-to-edit':parent_id, 'data-type-element': eleEdition });
			}
		})

		//PREVIEW
		$(".edition-preview ul li.action-"+elId).on('click', function(){
			var elemtGlobal = $(this).parent().parent().parent();

			if($(this).attr("data-type"))
				{
					var state = $(this).attr("data-active-preview");
					var eleme = $(this).parent().parent()
								.parent()
								.parent()
								.find(".ecm-edition-page-prev")
						
					if( state == 'false') {
						$(this).attr("data-active-preview", "true");
							    eleme.fadeIn();
							    $(this).find("span").removeClass('glyphicon glyphicon-eye-open').addClass("glyphicon glyphicon-eye-close");
					}else if( state == 'true'){
						$(this).attr("data-active-preview", "false");
								eleme.fadeOut();
						 $(this).find("span").removeClass('glyphicon glyphicon-eye-close').addClass("glyphicon glyphicon-eye-open");
					}
				}

			if( $(this).attr('data-action') ){

				var nmbr = $("#circles-edition").find("div.ecm-edition-pages-content").length;
					if (nmbr < 8) { 
						var d = new Date();

							defaultData 				= [];
							defaultData.id 		        = d.getTime();
							defaultData.bg        		= '#ff99dd';
							defaultData.box_audio_mp3   = 'files/test.mp3';
							defaultData.box_audio_ogg   = '';
							defaultData.box_audio_state = "true";
							defaultData.stateImage      = "true";
							defaultData.style_  	    = "visiblity:visible"; 
							defaultData.box_bgb			= '#fff';
							defaultData.box_clt  		= '#000';
							defaultData.box_id  	 	= d.getTime();
							defaultData.box_img 		= 'files/char-ex.jpg'
							defaultData.box_txt 		= 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ullamcorper, odio in blandit eleifend, felis ante varius metus, scelerisque mollis erat justo sit amet leo. Nulla neque eros, lacinia vel elementum et, molestie eu lorem. Nam suscipit consectetur'
							defaultData.color  		    = '#fff'
							defaultData.font   			= 'comic sans';
							defaultData.AudioId         = 'Audio-'+defaultData.id;
							mySelf.addNewCircleEdit(defaultData);
						}
					else if( nmbr >= 8 ){
						mySelf.showAlerts("El limite permitido es de 8");
					}			
			}
		});
	}
	//Quitar esto
	this.addNewCircleEdit = function(data){
		$.ajax({
			type:"get",
			url: 'circles.mustache',
			async:false,
			success: function(templatesFile){
				var template = templatesFile;
				var output   = Mustache.render( $(template).filter("#pageTpl").html(), data );
				$( "#circles-edition").append(output);
				mySelf.fnc_circles(data.id);
			},
			error: function(e){ console.error(e); }
		});
	}

	this.showAlerts = function(text){
		$(".ecm-main-alert").text(text).fadeIn('fast', function(){
			$(".ecm-main-alert").fadeOut(8000);
		})
	}

}


var circle = new circleApp(); 
	circle.functions_app();
	events_.colorPicker();
	events_.uploadImage();
	events_.uploadAudio();

function addContent(){
	xml = $(window.xmlContent);
		        	
	$("#MainTitle")
	.css({
		"background-color": xml.find("interface").find('headerBg').text(),
		"font-family":      xml.find("interface").find('fontFamily').text(),
		"color": 			xml.find("interface").find('titleMain').text()
	})
	.val( xml.find("interface").find('title').text() );

	xml.find('boton').each(function(){
		var view = [];
			view.id    = $(this).find('id').text();
			view.text  = $(this).find('textBoton').text();
			view.bg    = $(this).find('bg').text();
			view.color = $(this).find('color').text();
			view.font  = $(this).find('font').text();
			view.box   = $(this).find('box');

			view.box_id  		  = view.id ;
			view.box_img_folder	  = view.box.find('img').attr('data-folder');
			view.box_img 		  = view.box.find('img').text();
			view.box_txt 		  = view.box.find('text').text().trim();
			view.box_clt	      = view.box.find('colorText').text();
			view.box_bgb 		  = view.box.find('bgBox').text();
			view.box_audio_state  = view.box.find('audio').find('state').text();
			view.AudioId          = 'Audio-'+view.id;

		var http_img = '';
			if ( view.box_img_folder == 'uploads' ){
					http_img = window.location.origin+"/"+datafolder+"/";
			   	}

			view.finalUrl = http_img + view.box_img_folder + "/" + view.box_img;

			view.dataFolder        = view.box.find('folder').text();

			if( view.box_audio_state  == 'true' ){
			    view.style_        =  "visibility: visible;"; 
			    var http_ = '';
			    if ( view.dataFolder == 'uploads' || view.dataFolder == 'tts' ){
					http_ = window.location.origin+"/"+datafolder+"/";
			   	}
			   	 view.box_audio_mp3 =  http_ + view.dataFolder+"/" + $(this).find('srcMp3').text();
			   	
			    if ( $(this).find('srcOgg').text() != ''){
			   	 view.box_audio_ogg =  view.dataFolder+"/" + $(this).find('srcOgg').text();
			    }
			} 
			else if ( view.box_audio_state  == 'false' ){ 
				view.style_ = "visibility: hidden;";
				 }	
			
			if (  view.box_img.length <= 0 )
			    { view.stateImage = 'false'; }
			else{ view.stateImage = 'true'; }

		 	circle.addNewCircleEdit(view);
		
	});	
}

function readIncludes(){
var returnStr = '';
	$(".box-global-container").find(".box-type-img").each(function(){
		var img_ = $(this).find('img').attr('src');
			
			if ( img_.indexOf("files/") != -1 ){
		    	 returnStr += img_ + ",";
		    }
		    else{
		    	returnStr += img_.substr(img_.indexOf(datafolder)) + ",";
		    }
	});

		$("audio").find("source").each(function(){
			var sound_ = $(this).attr("src");
			    if ( sound_.indexOf("files/") != -1 ){ returnStr += sound_ + ","; }
			    else{ returnStr += sound_.substr(sound_.indexOf(datafolder)) + ",";   }
		});

	return returnStr;
}
function replaceFolder(string_){
	var folderFiles   = string_.indexOf("files/");
	var folderUploads = string_.indexOf("uploads/");
	var folderTts     = string_.indexOf("tts/");

	if ( folderFiles != -1){
		return string_.replace( "files/","");
	}

	if ( folderUploads != -1){
		return string_.replace( window.location.origin+"/"+datafolder+"/uploads/","");
	}

	if ( folderTts != -1){
		return string_.replace( "../../../../"+datafolder+"/tts/","");
	}
}

function readParamsToXml(){
	var JSONxmlbody = '';
		var JSONxmlHeader = '<?xml version="1.0" encoding="UTF-8"?><root><userdata>';

	JSONxmlbody += '<interface>';
		JSONxmlbody += '<bgMain>#ff8c1a</bgMain>'; 
		JSONxmlbody += '<headerBg>'+$("#MainTitle").css("background-color")+'</headerBg>'; 
		JSONxmlbody += '<titleMain>'+$("#MainTitle").css("color")+'</titleMain>'; 
		JSONxmlbody += '<fontFamily>'+$("#MainTitle").css("font-family")+'</fontFamily>'; 
		JSONxmlbody += '<title>'+$("#MainTitle").val()+'</title>'; 
	JSONxmlbody +='</interface>';

	$("#circles-edition").find('.ecm-edition-pages-content').each(function(index){
		var circle   = $(this).find('.ecm-edition-page-circle');
		var box      = $(this).find(".box-ref").find('.box-global-container');
		var textArea = $(this).find("#text_cricle_"+(index+1)).val();
 		
			JSONxmlbody += '<boton>';
				JSONxmlbody += '<id>'+    circle.attr("id") +'</id>';
				JSONxmlbody += '<textBoton>'+  circle.find(".ecm-edition-page-circle-number").val() +'</textBoton>'; 
				JSONxmlbody += '<bg>'+    circle.css('background-color') +'</bg>'; 
				JSONxmlbody += '<color>'+ circle.find(".ecm-edition-page-circle-number").css("color") +'</color>'; 
				JSONxmlbody += '<font>'+  circle.find(".ecm-edition-page-circle-number").css("font-family")+'</font>'; 

				var soundState = box.find(".box-type-sound").find('span').attr('data-sound-active');

					JSONxmlbody +='<box>';
	    			JSONxmlbody +='<idParent>'+ circle.attr("id") +'</idParent>';
	    			var img_ = box.find(".box-type-img").find("img");
	    			JSONxmlbody +='<img data-folder="'+img_.attr("data-folder")+'">'+  replaceFolder( img_.attr("src") ) +'</img>';
	    			JSONxmlbody +='<text>';
	    				JSONxmlbody += textArea;
	    			JSONxmlbody +='</text>';
	    			JSONxmlbody +='<colorText>'+ box.find(".box-type-text").css("color") +'</colorText>';
	    			JSONxmlbody +='<bgBox>'+  box.css("background-color") +' </bgBox>';

	    			JSONxmlbody +='<audio>';
	    			JSONxmlbody +='<state>'+soundState+'</state>';
		    			if ( soundState == "true" )
							{	
								JSONxmlbody += "<folder>"+box.find(".box-type-sound").find("audio").attr('data-folder')+"</folder>";
								var sourceTotal = box.find(".box-type-sound").find("audio").find("source").length; 
								if ( sourceTotal <= 1 ){
									var mp3_ = box.find(".box-type-sound").find("audio").find("source").attr("src");
									
									JSONxmlbody += '<srcMp3>';
										JSONxmlbody += replaceFolder(mp3_);
									JSONxmlbody += '</srcMp3>';
								}

								else{
									var mp3_ = $(box.find(".box-type-sound").find("audio").find("source")[1]).attr("src");
									var ogg_ = $(box.find(".box-type-sound").find("audio").find("source")[0]).attr("src");

									JSONxmlbody += '<srcOgg>';
										JSONxmlbody += replaceFolder(ogg_);
									JSONxmlbody += '</srcOgg>';
									
									JSONxmlbody += '<srcMp3>';
									 	JSONxmlbody += replaceFolder(mp3_);
									JSONxmlbody += '</srcMp3>';
								}
							}
					JSONxmlbody +='</audio>';
	    		JSONxmlbody +='</box>';
			JSONxmlbody +='</boton>';

	});
	JSONxmlHeader += JSONxmlbody;
	JSONxmlHeader += '</userdata></root>';
	console.warn(JSONxmlHeader);
	return JSONxmlHeader;
}
