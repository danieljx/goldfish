function rackMain(){
	var selfRack = this;

	this.init = function(){
		selfRack.uiFunction();
		console.info("population");
	}

	this.rackTemp = function(){
		var numberGrig  = 3, totalGrid = 9;
		var HeighWindow = $("#"+ ecm.idWindowEdition).outerHeight();
		var rackTemplate = $("<div/>");
			rackTemplate.attr("style", "height:"+HeighWindow+"px")
			rackTemplate.addClass('ecm-rack-element-main');
			rackTemplate.attr('data-number-grid',numberGrig);
			rackTemplate.attr('data-total-grid',totalGrid);
			rackTemplate.attr('id', ecm.renderId('rack'));

		for (var i = 0; i <= 9 -1; i++) {
			var gridTemplate = $("<div/>");
				gridTemplate.attr("data-magnitude-x","default");
				gridTemplate.attr("data-magnitude-y","default");
				gridTemplate.addClass("ecm-grid-child");
				gridTemplate.addClass( ecm.classForSelect );

			rackTemplate.append(gridTemplate);
		}
		var clearDiv = $("<div/>");
			clearDiv.addClass("clear");

			rackTemplate.append(clearDiv);
		return rackTemplate;
	}

	this.addNewRack = function(){

	}

	this.tmplRack = function(){

	}

	this.removeSelected = function(){

	}

	this.uiFunction = function(){
		//cuando se produce un cambio en el tamaño del contenedor
		$("body").on("keypress, change","#widthRack, #heightRack", function(){
			setTimeout(function(){
				$("#btnSaveSiseXY").trigger("click");
			},0);
		})
		//click para guardar el tamaño
		$("body").on("click","#btnSaveSiseXY", function(){
			var valW = $( "#widthRack" ).val(),
				valH = $( "#heightRack" ).val();
				if ( isNaN(valW) == true || isNaN(valH) == true) {
					console.error('Debes ingresar un numero');
				}else{
					$("body")
						.find(".gf-selected-dynamic")
							.css({"width": valW+"%"})
							.css({"height": valH+"%"})
				}
		});
	

		//Click items del menu edicion izquierdo
		$("body").on("click",".gf-ecm-edition-circle-tools ul li[data-tool-action]", function(){
			var toolAction = $(this).attr("data-tool-action");
			//
			if ( toolAction == 'addNew'){
				$("#"+ ecm.idWindowEdition).append(selfRack.rackTemp());
			}

			if ( toolAction == "sizeXY" ){
				var Selecteds     = $("body").find(".gf-selected-dynamic"),
					bodyConfigCnt = $(".gf-ecm-body"),
					rackSizeXY    = $("#rackSizeXY"); 

				if ( Selecteds ) {
					var widthParent  = Selecteds.parent().outerWidth(),
						heightParent = Selecteds.parent().outerHeight(),
						widthsel     = Selecteds.css("width").replace("px",''),
						heightsel    = Selecteds.css("height").replace("px",'');
						endWidth     = ecm.PercentageEquivalent( widthsel, widthParent),
						endHeigth    = ecm.PercentageEquivalent( heightsel, heightParent);

							if ( rackSizeXY.hasClass('gf-ecm-element-inactive') ){
								 rackSizeXY.removeClass('gf-ecm-element-inactive');
								 bodyConfigCnt.css({ "height":"300px"});
								 $( "#widthRack" ).val( parseInt(endWidth ));
								 $( "#heightRack" ).val( parseInt(endHeigth ));
							}else{
								rackSizeXY.addClass('gf-ecm-element-inactive');
								bodyConfigCnt.css({ "height":"107px"});	
							}

				}
			}
			if ( $(this).attr("data-tool-action") == "removeRack" ){
				if ( $(".gf-selected-dynamic").length >= 1) {
					var delConfirm =  confirm("¿Deseas eliminar este elemento?");
					if ( delConfirm == true){
						var rackDynamic = $("#gf-ecm-body-edition .ecm-grid-element-main");
						if(rackDynamic.length >= 1){
							var nmbRows = parseInt(rackDynamic.attr("data-total-grid"));
								nmbRows--;
								rackDynamic.attr("data-total-grid", nmbRows)
						};
						$(".gf-selected-dynamic").remove();
					}
				}else{
					alert("No hay elementos seleccionados")
				}
			}//fin quitar


			//añadir caja o row
			if ( $(this).attr("data-tool-action") == "addRack" ){
				var rackDynamic = $("#gf-ecm-body-edition .ecm-grid-element-main");
				if ( rackDynamic.length >= 1 ){
					var rackRowbase = $("#gf-basic-row").clone();
					rackRowbase.attr("id", RackSelft.renderId('row'));
					rackDynamic.append(rackRowbase);
					var nmbRows = parseInt(rackDynamic.attr("data-total-grid"));
						nmbRows++;
						rackDynamic.attr("data-total-grid", nmbRows)
				}//Añadir alerta Añadido
			}//fin añadir row o caja
		});
	}

}

var RackMain = new rackMain();
	RackMain.init();