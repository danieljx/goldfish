function rack(){
	var RackSelft = this;
		this.classForSelect  = "gf-elemen-this-selected"; 
		this.idWindowEdition = "gf-ecm-render-window";
		this.classHiddenElem = "gf-ecm-element-inactive";
		this.CreateDynamicEl = "gf-dinamyc-element";

		this.PluginsUrls = 'plugins';
		this.plugins =[ 
		{ name: 'rack', width: 330, height:107,
		  icon:'glyphicon-th', title: 'Cuadricula'
		   },
		{ name: 'multimedia',  width: 250, height:260,
		  icon: 'glyphicon-film', title: 'Multimedia'},
		{ name: 'css',  width: 490, height:107,
		  icon: 'glyphicon-cog', title: 'Estilos'},
		{ name: 'texto',  width: 250, height:460,
		  icon: 'glyphicon-font', title: 'texto'}
		]


	this.init = function(){
		var Plugins = RackSelft.plugins;
			RackSelft.uifunctions();
		
		for ( var i = 0; i <= Plugins.length - 1; i++ ){
			RackSelft.importTempMustache( Plugins[i] );
		}
		$(document).ready(function(){
			$("#gf-ecm-render-window").css("height", ($(window).outerHeight()-250)+"px");
		});
		console.info("crear instancia");
	}

	this.sizeEditionPanel = function(width, height ){
		lateralMenu = $("#gf-ecm-body-lateral-menu");
		lateralMenu.css({ "width":  width+"px", "height": height+"px" })
	}

	this.importTempMustache = function(plugin){ 

		
		var liLeftMenu = $("<li/>");
			liLeftMenu.addClass('gf-ecm-tool');
			liLeftMenu.attr('data-width-edition-panel', plugin.width);
			liLeftMenu.attr('data-height-edition-panel', plugin.height);
			liLeftMenu.attr('data-name-temp-edition', plugin.name);

		var parentDiv = $("<div/>");
			parentDiv.addClass("gf-ecm-tools-left");

		var divIcon = $("<div/>");
			divIcon.addClass("gf-ecm-tools-icon");

		var spanIcon = $("<span/>");
			spanIcon.addClass("glyphicon").addClass(plugin.icon);

		var header_ = $("<header/>");
			header_.addClass("gf-ecm-tools-title");
			header_.html("<h6>"+ plugin.title +"</h6>");

			divIcon.append(spanIcon);
			parentDiv.append(divIcon);
			parentDiv.append(header_);
			liLeftMenu.append(parentDiv);

			$("#gf-plugins-tools").append(liLeftMenu);
				$.get( RackSelft.PluginsUrls+'/'+plugin.name+'/tpl/'+plugin.name+'.mustache' , function(templatesFile) {
					var template = templatesFile;
					var viewParam = [];
						//viewParam.id = data.view;
					var output = Mustache.render( $(template).filter("#"+plugin.name+'Base').html(), viewParam );
					$( "#gf-ecm-config-lateral-menu").append(output);
					//addCounter();
				});
	}

	this.addRack = function(){
		var hParent    = $("#gf-ecm-render-window").outerHeight();
		var rack_      = $("#ecm-rack-element-main").clone();
		var template   = RackSelft.TemplateDynamic(rack_);
		
		if ( $("#gf-ecm-body-edition .gf-dinamyc-element").length < 1 ){
			$("#gf-ecm-render-window").append( template );
			$("#"+template.attr("id")).find(" .ecm-rack-element-main").css("height",hParent+"px");
		}
	}

	this.PercentageEquivalent = function( baseNum, baseNumParent ){
		var equivalent = (baseNum / baseNumParent)*100;
			return equivalent;
	}

	this.renderId = function(name){
		var id_   = new Date(),
			idEnd = id_.getTime();
		return idEnd+'-'+name;
	}

	this.TemplateDynamic = function(rack_){
		var this_id = RackSelft.renderId('dynamic');

		var section_ = $('<section/>');
		    section_.addClass( RackSelft.CreateDynamicEl );
		    section_.attr("id",this_id);

		var header_ = $('<header/>');
			header_.attr("class","gf-elemen-this-selected");
			header_.attr("data-pushpin-selector","true");

		var div_circle = $("<div/>");
			div_circle.attr("class","gf-circle-edit-dinamyc-element");

		var span_ = $("<span/>");
			span_.attr("class","glyphicon glyphicon-pushpin");

		var div_body = $("<div/>");
			div_body.attr("class","gf-dinamyc-body");

			div_body.append(rack_.attr("id", this_id));

			div_circle.append( span_ );
			//header_.append( div_circle );
			section_.append( header_ );
			section_.append( div_body );
			
		return section_;
	}

	//Eventos
	this.uifunctions = function(){
		
		$("body").on("contextmenu","."+RackSelft.CreateDynamicEl, function(event){
			event.preventDefault();
			alert("hi");
		})

		$("body").on("click",".gf-ecm-body-lateral-menu-close", function(){
			$("#gf-ecm-body-lateral-menu").addClass("gf-ecm-element-inactive");
			$("#rackSizeXY").addClass("gf-ecm-element-inactive");
		})
		//MOSTRAR HERRAMIENTAS DE EDICIÓN DE LA BARRA IZQUIERDA
		$("body").on("click",".gf-ecm-left-menu ul li[data-name-temp-edition]", function(){
			var toolsEdition  = $(".gf-multimedia-global-container"),
				itemsMenuLeft = $(".gf-ecm-left-menu ul li[data-name-temp-edition]"),
				currentTool   = $(this).attr("data-name-temp-edition");

			var attr_data   = $(this).attr("data-show-panel"), boxEdition,
			 	attr_width  = $(this).attr("data-width-edition-panel"),
			 	attr_height = $(this).attr("data-height-edition-panel");
			 	lateralMenu = $("#gf-ecm-body-lateral-menu");

			 	lateralMenu.removeClass("gf-ecm-element-inactive");
				RackSelft.sizeEditionPanel( attr_width, attr_height);
					
				itemsMenuLeft.removeClass('gf-ecm-active');
				$(this).addClass('gf-ecm-active');

				toolsEdition.addClass("gf-ecm-element-inactive");
				$("#plugin-"+currentTool).removeClass("gf-ecm-element-inactive");
		});//FIN


		$("body").on("click", "#gfEcmAddRack", function(){
			RackSelft.addRack();
		});
		$("body").on("click",".gf-elemen-this-selected", function(){
			RackSelft.addSelectedClass( $(this) );
		});	

		
		}

	//Seleccionar un elemento (Añade una clase, en base a ella realiza acciones)
	this.addSelectedClass = function( element ){
		var Selecteds = $("body").find(".gf-selected-dynamic").length;
		if ( Selecteds >= 1){
			$("body").find(".gf-selected-dynamic").each(function(){
				$(".gf-selected-dynamic").removeClass('gf-selected-dynamic')
			});	
		}
		
		if ( element.attr("data-pushpin-selector") ){
			element.parent(".gf-dinamyc-element").addClass('gf-selected-dynamic');
		}else{
			element.addClass('gf-selected-dynamic');
		}
		
	}
} 
var ecm = new rack();
	ecm.init();
