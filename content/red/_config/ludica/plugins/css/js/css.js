function cssPlugin(){
	var cssSelf = this;
	this.uiFunctions = function(){
		$("body")
			.on("click", ".gf-ecm-edition-circle-tools ul li[data-list-child]", function(){
				
				
			$(".gf-emc-plugin-css-form").addClass( ecm.classHiddenElem );
				var attrNm = $(this).attr("data-list-child");
				var width_, height_;
				if( attrNm == 'marginCSSPlugin' ){
					$("#"+attrNm).removeClass( ecm.classHiddenElem );
					width_ = 490, height_ = 310;
				}
				else if( attrNm ==  "PaddingCSSplugin" ){
					$("#"+attrNm).removeClass( ecm.classHiddenElem );
					width_ = 490, height_ = 255;
				}
				else if( attrNm ==  "SizeCSSplugin" ){
					$("#"+attrNm).removeClass( ecm.classHiddenElem );
					width_ = 490, height_ = 310;
				}
				else if( attrNm ==  "borderCSSplugin" ){
					$("#"+attrNm).removeClass( ecm.classHiddenElem );
					width_ = 490, height_ = 255;
				}
				else if( attrNm ==  "BgColorCSSplugin" ){
					console.info("aqui");
					$("#"+attrNm).removeClass( ecm.classHiddenElem );

					$(".gf-ecm-edition-circle-tools ul li.color-option").ColorPicker({
						onChange: function (hsb, hex, rgb) {
							$("body")
								.find('.gf-selected-dynamic').each(function(){
									console.warn(hex);
									$(this).css("background-color","#"+hex)
								})
							}
				});
					width_ = 490, height_ = 107;
				}
				ecm.sizeEditionPanel( width_ , height_);
		})
	}
}

var cssClass = new cssPlugin();
	cssClass.uiFunctions();