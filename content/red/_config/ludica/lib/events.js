function events(){
	var myself = this;
   this.current_Upload = '';

   this.colorPicker = function(){
   	console.warn("team color");
  	 	var clickActive;

		$('.color-option').click(function(){
			clickActive = $(this).attr('data-type');
			alert("hello!")
		});

		$('.color-option').ColorPicker({
			onChange: function (hsb, hex, rgb) {
				console.warn( "#"+hex );
				/*var parent = $("#globalInterfaceCharacter").find('.activeMenuMain').find(".container-edit").attr("id"); 
				var charCurrent;
					//id de personaje activo character 1
					if( parent == "main-char-1" )
						{ charCurrent = $("#main-char-1"); }
					//character 2
					if( parent == "main-char-2" )
						{ charCurrent = $("#main-char-2"); }
					//modificando color del borde
					if ( clickActive == 'borderColorEdit')
						{ charCurrent.find('#image-user-container').css("border-color","#"+hex);
							 }
					//modificando el color de fondo
					if ( clickActive == 'BgColorEdit')
						{ charCurrent.find('#image-user-container').find(".bgChar").css("background-color","#"+hex); }
					//Color de fondo de la comilla del mensaje
					if ( clickActive == "bg-letter-color" )
						{	//color de fondo
							charCurrent.find("#snackUnique").css("background-color","#"+hex);
						var red_ = rgb.r-60, green_ = rgb.g-10, blue_  = rgb.b-10;
								
							//color de la comilla opacando el color
							charCurrent
							.find(".snack-comilla").css({
									"border-bottom-color": "rgb("+ red_ +","+ green_  +","+ blue_ +")"
								});
						}
					//color de la letra
					if ( clickActive == "letter-color" )
						{charCurrent.find("#snackUnique").css("color","#"+hex);}
					//color de fondo del contenedor de la imagen
					if( clickActive == "boxBgMain")
						{	$("#prevFondo").css({ "border-color":"#"+hex});
							$("#boxBgMain").css({'background-color':"#"+hex});}
					//color de borde del contenedor
					if( clickActive == "boxBrMain")
						{ 	
							$("#prevFondo").css({ "background-color":"#"+hex});
							$("#boxBrMain").css({'background-color':"#"+hex});}*/
			}
		});
   }//end colorPicker

   this.uploadImage = function(){
   	/*global window, $ */
		$(function () {
		    'use strict';
		    // Change this to the location of your server-side upload handler:
		    var url = '../../jquery-file-upload-handler.php';
		 
		    $('.char-upload').fileupload({
		    	dropZone: $(this),
		        url: url,
		        dataType: 'json',
		        autoUpload: true,
		        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
                maxFileSize: 1 * 1000000, // 1 Mb
		        done: function (e, data) {
		        	var elem_ = $(this);
		        	var file_ = data.result.files[0];
		        	if( myself.current_Upload != 'bgMain' ){
		        			$.each(data.result.files, function (index, file) {
								elem_
								.parent().parent().parent().siblings().find("div#bgImgCnt")
								.css("background-image","url("+file_.url+")");
							});
		        	}
		        	else if( myself.current_Upload == 'bgMain' ){
		        			$.each(data.result.files, function (index, file) {
			        				elem_
									.parent().parent().parent().siblings()
									.find("div").find("img")
									.attr("src", file_.url);
		        				});
		        	}
				},
		      	progressall: function (e, data) {
					var progress = parseInt(data.loaded / data.total * 100, 10);
					$(this).parent().siblings()
					.find('.progress-bar')
					.css('width', progress + '%');
				}
		    })
		    .on('fileuploadfail', function(e, data) {
					alert("FALLO!!")
			})
            .prop('disabled', !$.support.fileInput)
		    .parent().addClass($.support.fileInput ? undefined : 'disabled');
		});
   }//uploadImage

   this.uploadAudio = function(){
   		/*Subir archivos*/
		$(function () {
			'use strict';
			// Change this to the location of your server-side upload handler:
			var url = '../../jquery-file-upload-handler.php';
			$('.upload_sound').fileupload({
					dropZone: $(this),
					url: url,
					dataType: 'json',
			        autoUpload: true,
			        acceptFileTypes: /(\.|\/)(mp3|ogg|wav|wma)$/i,
			        maxFileSize: 50 * 1000000, // 5 Mb
				done: function (e, data) {
					$.each(data.result.files, function (index, file) {
				 });
				},
				
				progressall: function (e, data) {
					var progress = parseInt(data.loaded / data.total * 100, 10);
					        $('#progress_audio .progress-bar').css( 'width', progress + '%' );
					        }
					})
					.on('fileuploadfail', function(e, data) {
								alert("FALLO!!")
					})
					.on('fileuploaddone', function(e, data) {
						var file = data.result.files[0];
							
					$.ajax({
						    type: "POST",
						    url: "process_upload.php",
						    data: {file_name: file.name, file_path: data.result.physPath},
						    dataType: 'json',
						error : function(error){
						    alert(error);
						},
						success: function(result) {
							$("#saveMsj")
							.attr("data-audio-state","true")
							.attr("data-audio-name", result.path_html)
							.attr('data-folder-file', 'uploads');
							   }
						});
						})
			            .prop('disabled', !$.support.fileInput)
					    .parent().addClass($.support.fileInput ? undefined : 'disabled');
					});
   }//uploadAudio

}//end events

var events_ = new events();
	events_.colorPicker()
	events_.uploadImage();