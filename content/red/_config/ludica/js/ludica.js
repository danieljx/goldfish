var backgrounds = new Array();      // librería de fondos
var characters = new Array();       // librería de carácteres
var mySound;                        // global para cargar y reproducir sonido
var templates;                      // plantillas HTML, formato Mustache 
var idToLoader = new Array();
var loadersSave = new Array();
var dlSounds = new Array();
var imgW = 0;
var imgH = 0;
var imgPos;
var spotR = 0;
var spots = new Array();
var count = 0;
var basePath="";

function renderPage(xmlNode) {

    console.log("render");
    console.log($(xmlNode).find("instancecolor").text());
    // Preparamos un modelo Javascript de los datos de una escena obtenidos de XML
    var txt = $(xmlNode).find("content").text(),
        regex = "/\b\"files\/\b/gi",
        selected_ludic = $(xmlNode).find("ludic").text(),
        avatar = $(xmlNode).find("avatar").text(),
        logo = $(xmlNode).find('logo').text(),
        selected_transition = $(xmlNode).find("transition").text();
        avatar = avatar.trim();
        if(avatar===""){ avatar = 'img/avatar.png';}
    txt = txt.replace('="files/','="'+window.srcPath+"/files/");
    avatar = avatar.replace('files/',window.srcPath+"/files/");
    logo = logo.replace('files/',"");
    var newPage = {
        ludic: JSLang.getTranslation("LUDIC_SELECT"),
        ludicType:[
            {"name":JSLang.getTranslation("LUDIC_VERY_IMPORTANT"), "value":"muy_importante",sel:selected('muy_importante',selected_ludic)},
            {"name":JSLang.getTranslation("LUDIC_CONTEMPLATE"), "value":"punto_reflexion",sel:selected('punto_reflexion',selected_ludic)},
            {"name":JSLang.getTranslation("LUDIC_DID_YOU_KNOW"), "value":"sabias_que",sel:selected('sabias_que',selected_ludic)},
            {"name":JSLang.getTranslation("LUDIC_REMEMBER"), "value":"recuerda_que",sel:selected('recuerda_que',selected_ludic)}
        ],
        title:JSLang.getTranslation("LUDIC_TITLE"),
        titleValue:$(xmlNode).find("ludicTitle").text(),
        effect:JSLang.getTranslation("LUDIC_EFFECT"),
        logo:logo,
        effectType:[
            {"name":"Giro horizontal", "value":"horizontalflip",sel:selected('Value1',selected_transition)},
            {"name":"Giro vertical", "value":"verticalflip",sel:selected('Value2',selected_transition)}
        ],
        avatarTitle:JSLang.getTranslation("LUDIC_AVATAR"),
        backgroundTitle:JSLang.getTranslation("LUDIC_BACKGROUND"),
        backgroundColor:$(xmlNode).find("instancecolor").text(),
        contentTitle:JSLang.getTranslation("LUDIC_CONTENT"),
        contentBody:txt
    };
   
//    newPage.img = $(xmlNode).find("image src").text() != "" ? (window.srcPath + "/" + $(xmlNode).find("image src").text()) : "";
//    newPage.imgPos = $(xmlNode).find("image position").text();
//    newPage.src = $(xmlNode).find("sound src").text() != "" ? (window.srcPath + "/" + $(xmlNode).find("sound src").text()) : "";
    

    // Combinamos el modelo Javascript con un template, para crear un bloque de HTML y agregarlo al DOM.
    $('body form').append(Mustache.render($(templates).filter('#pageTpl').html(), newPage));
    createId();
    $('.bg-avatar').css({'background-image':'url('+avatar+')','background-size':'100% 100%'});
    generateUpload();
    // Si no hay mas escenas, seguimos
    
    contentsLoaded();
}
generateUpload = function (){
    
    var url = '../../jquery-file-upload-handler.php',
                uploadButton = $('<button/>')
                .addClass('btn btn-primary')
                .prop('disabled', true)
                .text('Processing...')
                .on('click', function() {
                    var $this = $(this),
                            data = $this.data();
                    $this
                            .off('click')
                            .text('Abort')
                            .on('click', function() {
                                $this.remove();
                                data.abort();
                            });
                    data.submit().always(function() {
                        $this.remove();
                    });
                });


        // Upload imagen principal
        $('.fileupload').fileupload({
            url: url,
            dataType: 'json',
            autoUpload: true,
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
            maxFileSize: 50 * 1000000 // 50 Mb

        }).on('fileuploadadd', function(e, data) {
            /*Momento de carga*/

        }).on('fileuploadprocessalways', function(e, data) {
        }).on('fileuploadprogressall', function(e, data) {

            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('.progress .progress-bar').css(
                    'width',
                    progress + '%'
            );

        }).on('fileuploaddone', function(e, data) {
            // al terminar de cargar imagen exitosamente:
            if (data.result.files.length > 0) {

                var file = data.result.files[0];
                console.log("URL "+file.url);
                $('.bg-avatar').css({
                        "background-image": "url(" + file.url + ")"});
                // tenemos que primero cargar la imagen en un elemento "ficticio" para poder leer su w / h y despues mostrarlo en la página con dimensiones adaptadas


            }

        }).on('fileuploadfail', function(e, data) {
        }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');

};
 var selected = function (value,xmlValue){
    if(value == xmlValue)
        return true;
    
    return false;
};
function createId(){
            var id = "textarea-";
            $(".text-area").each(function(){
               // console.log($(this).attr('id'));
                if(typeof $(this).attr('id') == "undefined"){
                    $(this).attr('id',id+''+count);
                    createEditorField(id+''+count);  
            }
            count++;
            });

        }

function createEditorField(id){
    
        $('#'+id).summernote($.fn.summernoteConfig('picture',id));
    
}
function sendFile(file,editor,welEditable,id){
    data = new FormData();
    data.append("file", file);
    $.ajax({
        url:'uploadImage.php',
        data:data,
        cache:false,
        contentType:false,
        processData:false,
        type:'POST',
        success:function(data){
            data = JSON.parse(data);
            basePath = data.basePath;
            $('#'+id).summernote("insertImage", data.path, data.filename);
            data.path = data.path.replace('../../../../',"");
            addInclude(data.path);
        },
        error:function (jqXHR, textStatus, errorThrown){
            console.log(textStatus+" "+errorThrown);
        }
    });
}
function updateSample() {
    
    var hueBase = jQuery.Color( $('#base-color').val() );
 //    var bgBase = jQuery.Color( { 
    //  hue: hueBase.hue(),
    //  saturation: 1,
    //  lightness: ((2 - 1)*0.4) / 2,
    //  alpha: 1
    // } );
    bgBase = hueBase;
        
    var bgDesaturated = bgBase.saturation( bgBase.saturation() * 0.8 );
    
    $('#sample').css({
        'background-color': bgBase.toHexString(),
        'background-image': "linear-gradient("+bgDesaturated.lightness( bgBase.lightness() + (1 - bgBase.lightness()) * 0.3 ).toHexString()+", "+bgDesaturated.lightness( bgBase.lightness() + (1 - bgBase.lightness()) * 0.1 ).toHexString()+")",
        'color': $('#text-color').val()
    });
}

//Santiago Peñuela A. - 11/02/2015
function findCssValue(cssText, property)
{
    var a = cssText.substr(cssText.indexOf(property));
    var ab = a.indexOf(":");
    var c = a.indexOf(";");

    return a.substring((ab+1),c).trim();
}

// Añadir contenido del XML a la página
function addContent() {

    var xml = $(window.xmlContent),
        xmlBody = xml.find('content').text(),
        xmlBodyReplace = "",
        i = 0;
        console.log(xmlBody);
    // Entorno test solo:
    if (window.srcPath === "")
        window.srcPath = "/"+appfolder+"/plugins_html/_config/glosario";

//    $('h2').html(xml.find("maintitle"));

    console.log(xml.find('styles textcolor').text());

    $('#header-styles').val( xml.find('styles header').text() );
    $('#body-styles').val( xml.find('styles body').text() );    
    $('#base-color').val( (xml.find('userdata > styles bgcolor').text() !== "") ? xml.find('userdata > styles bgcolor').text() : "#000066" );
    $('#text-color').val( (xml.find('userdata > styles textcolor').text() !== "") ? xml.find('userdata > styles textcolor').text() : "#ffffff" );
    $('.fondo textarea').val( xml.find('intro content').text() );
    // Leer plantillas Mustache, guardar en variable global
    $.get('templates.mustache', function(templatesFile) {
        templates = templatesFile;        
        renderPage(xml);
        contentsLoaded();
    });
}

function setType(aType) {
    var title = "";
        var bgcolor = "#000000";
        switch(aType) {
            case "punto_reflexion":
                title = JSLang.getTranslation("LUDIC_HTML_CONTEMPLATE");
                bgcolor = '#edc20f';
            break;
            case "recuerda_que":
                title = JSLang.getTranslation("LUDIC_HTML_REMEMBER");
                bgcolor = '#4b5db2';
            break;
            case "muy_importante":
                title = JSLang.getTranslation("LUDIC_HTML_VERY_IMPORTANT");
                bgcolor = '#109eb5';
            break;
            case "sabias_que":
                title = JSLang.getTranslation("LUDIC_HTML_DID_YOU_KNOW");
                bgcolor = '#d34620';
            break;
        }

        $('#logo').val(aType+".png");
        $('#title').val(title);
        $('#background').val(bgcolor);
}

// para ejecutar una vez esté agregados los contenidos: agregar eventos etc 
function contentsLoaded() {
    updateSample();
    
    $('#base-color, #text-color').change( function() {
        updateSample();
    });

    //setType( $('#tipoLudica').val() );
    $('#tipoLudica').change( function(e) {
        setType($(e.target).val());
    });

    // LISTENER borrar término
    $('#hbar').on("click", ".page a.cerrar", function(e) {
        e.preventDefault();
        if ($(e.target).closest(".page").length > 0) {
            // término
            var page = $(e.target).closest(".page");

            page.animate(
                    {
                        opacity: 0,
                        height: 0
                    },
            200,
                    function() {
                        $('#imagen').find('#spot' + page.attr('id').replace('page', '')).remove();
                        page.remove();
                    });
        }
    });

}

function validateConfig() {
    var valid = true;

    return valid;
}

function readParamsToXml() {
    var xmlStr = '<?xml version="1.0" encoding="UTF-8"?><root><userdata>';
    xmlStr += '<ludic>'+$('#tipoLudica').val()+'</ludic>';
    xmlStr += '<ludicTitle><![CDATA['+$('#title').val()+']]></ludicTitle>';
    xmlStr += '<transition>'+$('#transition').val()+'</transition>';
    var imgSrc = $('.bg-avatar').css("background-image").replace(/(")|(')/g,'');    
    imgSrc = imgSrc.substring(imgSrc.lastIndexOf("/") + 1, imgSrc.indexOf(")"));
    xmlStr += '<avatar><src>files/'+imgSrc+'</src></avatar>';
    var logo = ($('#logo').val() != "")?$('#logo').val():"muy_importante.png";
    xmlStr += '<logo>files/'+logo+'</logo>';
    xmlStr += '<basecolor>#f2f4f3</basecolor>';
    xmlStr += '<instancecolor>'+$('#background').val()+'</instancecolor>';
    var body = $('.text-area').code();
        if(basePath){
            body = body.replace(basePath,"files/");
        }
    body = body.replace(window.srcPath+"/","");
    xmlStr += '<content><![CDATA[' + body + ']]></content>';
    xmlStr += '</userdata></root>';
    return xmlStr;
}

function readIncludes() {
    var imgSrc = $('.bg-avatar').css("background-image").replace(/(")|(')/g,'');
    imgSrc = imgSrc.substring(imgSrc.indexOf(datafolder), imgSrc.indexOf(")"));
    imgSrc = imgSrc.substr(imgSrc.indexOf(datafolder)) + ",";
    return imgSrc.substr(0, imgSrc.length - 1);
}

$(document).ready(function(){

});