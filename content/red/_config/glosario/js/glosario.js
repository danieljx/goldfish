var backgrounds = new Array();      // librería de fondos
var characters = new Array();       // librería de carácteres
var mySound;                        // global para cargar y reproducir sonido
var templates;                      // plantillas HTML, formato Mustache 
var idToLoader = new Array();
var loadersSave = new Array();
var dlSounds = new Array();
var imgW = 0;
var imgH = 0;
var imgPos;
var spotR = 0;
var tts;
var spots = new Array();
var count = 0;
var alphabet = new Array("a","b","c","d","e","f","g","h","i","j","k","l","m","n","ñ","o","p","q","r","s","t","u","v","w","x","y","z");

// var CKEDITOR = parent.CKEDITOR;
function renderPage(xmlNode, i, last) {
    // Preparamos un modelo Javascript de los datos de una escena obtenidos de XML
    var newPage = {
        translation_image: JSLang.getTranslation("IMAGE"),
        translation_audio: JSLang.getTranslation("AUDIO"),
        translation_content: JSLang.getTranslation("CONTENT"),
        translation_title: JSLang.getTranslation("TITLE"),
        translation_image_above: JSLang.getTranslation("IMAGE_ABOVE"),
        translation_image_below: JSLang.getTranslation("IMAGE_BELOW"),
        translation_title: JSLang.getTranslation("TITLE"),
        translation_type: JSLang.getTranslation("TYPE"),
        translation_font_color: JSLang.getTranslation("TEXT_COLOR"),
        translation_font_size: JSLang.getTranslation("GAME_CONFIG_TEXT_SIZE"),
        translation_background_color: JSLang.getTranslation("GAME_CONFIG_BACKGROUND"),
        robotic_voice:JSLang.getTranslation("TEXT_TO_SPEECH"),

           /* fonts:[
                {"name":"Arial, 'Helvetica Neue', Helvetica, sans-serif","shortname":"Arial"},
                {"name":"'Arial Black', 'Arial Bold', Gadget, sans-serif","shortname":"Arial Black"},
                {"name":"'Arial Narrow', Arial, sans-serif","shortname":"Arial Narrow"},
                {"name":"'Arial Rounded MT Bold', 'Helvetica Rounded', Arial, sans-serif","shortname":"Arial Rounded"},
                {"name":"'Avant Garde', Avantgarde, 'Century Gothic', CenturyGothic, 'AppleGothic', sans-serif","shortname":"Avant Garde"},
                {"name":"Calibri, Candara, Segoe, 'Segoe UI', Optima, Arial, sans-serif","shortname":"Calibri"},
                {"name":"'Century Gothic', CenturyGothic, AppleGothic, sans-serif","shortname":"Century Gothic"},
                {"name":"'Franklin Gothic Medium', 'Franklin Gothic', 'ITC Franklin Gothic', Arial, sans-serif","shortname":"Franklin Gothic"},
                {"name":"Futura, 'Trebuchet MS', Arial, sans-serif","shortname":"Futura"},
                {"name":"Geneva, Tahoma, Verdana, sans-serif","shortname":"Geneva"},
                {"name":"'Gill Sans', 'Gill Sans MT', Calibri, sans-serif","shortname":"Gill Sans"},
                {"name":"'Helvetica Neue', Helvetica, Arial, sans-serif","shortname":"Helvetica"},
                {"name":"Impact, Haettenschweiler, 'Franklin Gothic Bold', Charcoal, 'Helvetica Inserat', 'Bitstream Vera Sans Bold', 'Arial Black', sans serif","shortname":"Impact"},
                {"name":"'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Geneva, Verdana, sans-serif","shortname":"Lucida"},
                {"name":"Optima, Segoe, 'Segoe UI', Candara, Calibri, Arial, sans-serif","shortname":"Optima"},
                {"name":"'Segoe UI', Frutiger, 'Frutiger Linotype', 'Dejavu Sans', 'Helvetica Neue', Arial, sans-serif","shortname":"Segoe"},
                {"name":"Tahoma, Verdana, Segoe, sans-serif","shortname":"Tahoma"},
                {"name":"'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif","shortname":"Trebuchet"},
                {"name":"TimesNewRoman, 'Times New Roman', Times, Baskerville, Georgia, serif","shortname":"TimesNewRoman"},
                {"name":"Georgia, Times, 'Times New Roman', serif","shortname":"Georgia"},
                {"name":"Cambria, Georgia, serif","shortname":"Cambria"},
                {"name":"'Lucida Console', 'Lucida Sans Typewriter', Monaco, 'Bitstream Vera Sans Mono', monospace","shortname":"Lucida"},
                {"name":"Monaco, Consolas, 'Lucida Console', monospace","shortname":"Monaco"},
                {"name":"'Lucida Sans Typewriter', 'Lucida Console', Monaco, 'Bitstream Vera Sans Mono', monospace","shortname":"Lucida Sans"},

                {"name":"Consolas, monaco, monospace","shortname":"Consolas"},
                {"name":"Verdana","shortname":"Verdana", "select":"selected"},
                {"name":"PT Sans","shortname":"PT Sans"}],*/
            fsize:[{"number":"16"},{"number":"12"},{"number":"13"},{"number":"14"},{"number":"15"},{"number":"11"},
            {"number":"17"},{"number":"18"},{"number":"19"},{"number":"20"},{"number":"21"},{"number":"22"},{"number":"23"},
            {"number":"24"},{"number":"25"},{"number":"26"},{"number":"27"},{"number":"28"}]
        };


    newPage.id = "page" + i;
    newPage.npage = i;
    newPage.title = $(xmlNode).find("title").text();
    newPage.img = $(xmlNode).find("image src").text() != "" ? (window.srcPath + "/" + $(xmlNode).find("image src").text()) : "";
    newPage.imgPos = $(xmlNode).find("image position").text();
    newPage.src = $(xmlNode).find("sound src").text() != "" ? (window.srcPath + "/" + $(xmlNode).find("sound src").text()) : "";
    newPage.texto = $(xmlNode).find("texto").text();

    // Combinamos el modelo Javascript con un template, para crear un bloque de HTML y agregarlo al DOM.
    $('#add-page').before(Mustache.render($(templates).filter('#pageTpl').html(), newPage));
    createId();
    //Santiago Peñuela A - 11/02/2015
    $("#page"+i+" .ffamily option").each( function() { 
        if(findCssValue($(xmlNode).find("css").text(),"font-family") == $(this).val())
        {            
            $(this).attr("selected", "selected");
        }
    });

    $("#page"+i+" .fsize option").each( function() { 
        if(findCssValue($(xmlNode).find("css").text(),"font-size") == $(this).val()+"px")
        {            
            $(this).attr("selected", "selected");
        }
    });

    $("#page"+i+" .fcolor").val(findCssValue($(xmlNode).find("css").text(),"color"));
    $("#page"+i+" .bcolor").val(findCssValue($(xmlNode).find("css").text(),"background-color"));

    /////////////////////////////////////////////////////////////////////////////////////////////////

    if (newPage.src != "") {
        $('#glosario .page').last().find('.current-sound a').addClass("loaded");
    }

    if (newPage.imgPos != "") {
        $('#imagepos-' + i + '-' + newPage.imgPos).attr('checked', true);
        $('#glosario .page').last().find('.imgpos-container').show();
    }

    // Si no hay mas escenas, seguimos
    if (last)
        contentsLoaded();
}
// Michael Sanchez 14/07/2015
function createId(){
            var id = "textarea-";
            $(".text-area").each(function(){
               // console.log($(this).attr('id'));
                if(typeof $(this).attr('id') == "undefined"){
                    $(this).attr('id',id+''+count);
                    createEditorField(id+''+count);  
            }
            count++;
            })

        }
function createEditorField(id){
    
        $('#'+id).summernote({
                fontNames: [ 
                            'Arial',
                            'Arial Black',
                            'Comic Sans MS',
                            'Courier New',
                            'Century Gothic',
                            "Helvetica",
                            "Impact",
                            "Times New Roman",
                            "Verdana"
                            ]
                ,
                fontNamesIgnoreCheck: ['Century Gothic']
            }, 
            $.fn.summernoteConfig());    
}

//Santiago Peñuela A. - 17/02/2015
function findCssValue(cssText, property)
{
    //console.log(cssText);
    var a = cssText.substr(cssText.indexOf(property));
    var ab = a.indexOf(":");
    var c = a.indexOf(";");
    var d = a.substring((ab+1),c).trim();
    return a.substring((ab+1),c).trim();;
}

// Añadir contenido del XML a la página
function addContent() {

    var xml = $(window.xmlContent);
    var i = 0;

    // Entorno test solo:
    if (window.srcPath == "")
        window.srcPath = "/"+appfolder+"/plugins_html/_config/glosario";    

    $('#bbcolor').val( findCssValue(xml.find('styles body').text(),'background-color'));
    $('#tcolor').val( findCssValue(xml.find('styles header').text(),'color'));

    $('#letters').val("");
    xml.find('letters letter').each( function() {
        $('#letters').val( $('#letters').val() + ( $('#letters').val() != "" ? "," : "") + $(this).text() );
    });

    var pages = xml.find('term');

    // Leer plantillas Mustache, guardar en variable global
    $.get('templates.mustache', function(templatesFile) {
        templates = templatesFile;
        pages.each(function() {
            i++;
            renderPage($(this), i, (i === pages.length));
        });
        if (pages.length == 0)
            contentsLoaded();
    });
}

// para ejecutar una vez esté agregados los contenidos: agregar eventos etc 
function contentsLoaded() {

    createjs.Sound.registerPlugins([createjs.HTMLAudioPlugin]);

    createjs.Sound.addEventListener("fileload", function(event) {
        soundInstance = createjs.Sound.play(event.src);
        soundInstance.addEventListener("complete", function(e) {
            $('source[src$="'+e.target.src+'"]').closest("audio").siblings("a").removeClass("playing");
            createjs.Sound.removeSound(e.target.src);
            soundInstance = 'undefined';
        });
    });
    
    function sameNameTrunk(str1, str2) {
        return (str1.substring(0, str1.lastIndexOf(".")) === str2.substring(0, str2.lastIndexOf(".")));
    }

    // LISTENER play/pause sonido
    $('#glosario').on("click", '.current-sound a', function(e) {
        e.preventDefault();

        if ($(e.target).hasClass("loaded")) {
            if ($(e.target).hasClass("playing")) {
                soundInstance.stop();
                createjs.Sound.removeSound(soundInstance.src);
                soundInstance = 'undefined';
                $(e.target).removeClass("playing");
            } else {
                var audioTag = $(e.target).siblings("audio");
                if (audioTag.length > 0) 
                {
                    var oggSrc = audioTag.find("source").eq(0).attr("src");
                    
                    if (typeof soundInstance === 'undefined' || typeof soundInstance.src === 'undefined' || !sameNameTrunk(soundInstance.src, oggSrc)) {
                        if (typeof soundInstance !== 'undefined' && typeof soundInstance.src !== 'undefined') {
                            soundInstance.stop();
                            $('source[src$="'+soundInstance.src+'"]').closest("audio").siblings("a").removeClass("playing");
                            createjs.Sound.removeSound(soundInstance.src);
                            soundInstance = 'undefined';   
                        }
                        createjs.Sound.alternateExtensions = ["mp3"];
                        createjs.Sound.registerSound({id: "currentSound", src: oggSrc});
                    } else {
                        soundInstance.play();
                    }
                    $(e.target).addClass("playing"); 
                }
            }
        } 
    });

    // LISTENER borrar término
    $('#glosario').on("click", ".page a.cerrar", function(e) {
        e.preventDefault();
        if ($(e.target).closest(".page").length > 0) {
            // término
            var page = $(e.target).closest(".page");

            page.animate(
                    {
                        opacity: 0,
                        height: 0
                    }, 200,
                    function() {
                        $('#imagen').find('#spot' + page.attr('id').replace('page', '')).remove();
                        page.remove();
                    });
        }
    });
}

function readParamsToXml() {
    var xmlStr = '<?xml version="1.0" encoding="UTF-8"?><root><userdata>';

    xmlStr += "<styles><body><![CDATA[background-color: "+$('#bbcolor').val()+"; ";
    xmlStr += "]]></body><header><![CDATA[color: " + $('#tcolor').val()+"; background-color: #2e4045; ";
    xmlStr += "]]></header></styles>";
    
    xmlStr += "<letters>";
    var letters = $('#letters').val().split(",");
    $(letters).each( function(i, val) {
            xmlStr += "<letter>"+val+"</letter>";
        });
    xmlStr += "</letters>"; 
    xmlStr += "<autoplay>"+$('#check-main').is(':checked')+"</autoplay>"; 

    var pageStr = "";
    dlSounds = new Array();
    sounds = tts.getSounds();
    //console.log(sounds);
    $('.page').each(function() {

        $(this).find('audio source').not(".template audio src").each(function() {
            dlSounds.push($(this).attr("src"));
        });

        pageStr += '<term>';
        pageStr += '<title>' + $(this).find('h3 p').text() + '</title>';
        pageStr += '<content>';
        id = $(this).attr('id');

        var pageSound = $(this).find('.tts[data-file]').attr('data-file');
        if (pageSound) {
            pageStr += '<sound><src>files/' + getFileName(pageSound) + '</src></sound>';
        }
        // if (sounds[id]) {
        //     var src = sounds[id];
        //     //console.log(getFileName(src));
        //     pageStr += '<sound><src>files/' + getFileName(src) + '</src></sound>';
        // }

        if ($(this).find('img.thumb').length > 0) {
            var src = $(this).find('img.thumb').attr('src');

            if (getFileName(src) != "") {
                pageStr += '<image>';
                pageStr += '<src>files/' + getFileName(src) + '</src>';
                pageStr += '<position>' + $(this).find('.imgpos-container input:checked').val() + '</position>';
                pageStr += '</image>';
            }
        }
        var id = $(this).find('textarea').attr('id');
        pageStr += '<texto><![CDATA[' + $('#'+id).code() + ']]></texto>';
        //Santiago Peñuela A - 17/02/2015
        pageStr += '<css><![CDATA[';
        // font-family: ' + $(this).find('.cssaddpage .ffamily').val() + '; ';
        // pageStr += ' font-size: ' + $(this).find('.cssaddpage .fsize').val() + 'px;';
        pageStr += ' color: ' + $(this).find('.cssaddpage .fcolor').val() + ';';
        pageStr += ' background-color: ' + $(this).find('.cssaddpage .bcolor').val() + ';]]></css>';
        ////////////////////////////////////////////////////////////////////////////////
        pageStr += '</content>';
        pageStr += '</term>';
    });

    xmlStr += '<terms>' + pageStr + '</terms>';
    xmlStr += '</userdata></root>';

    return xmlStr;
}
$(function() {

    // Definimos funcionamiento de botones para subir imagen..
    setTimeout(function() {

        var url = '../../jquery-file-upload-handler.php',
                uploadButton = $('<button/>')
                .addClass('btn btn-primary')
                .prop('disabled', true)
                .text('Processing...')
                .on('click', function() {
                    
                    var $this = $(this),
                            data = $this.data();
                    $this
                            .off('click')
                            .text('Abort')
                            .on('click', function() {

                                $this.remove();
                                data.abort();
                            });
                    data.submit().always(function() {
                        $this.remove();
                    });
                });

// UPLOAD imagen interna a término
        $('body').on('created', '.uploads .imagen .fileupload', function() {
            $(this).fileupload({
                url: url,
                dataType: 'json',
                autoUpload: true,
                acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
                maxFileSize: 5 * 1000000 // 5 Mb

            }).on('fileuploadadd', function(e, data) {
//            $(e.target).closest('span').hide();
                $(e.target).closest('div').find('.progress').show();

            }).on('fileuploadprocessalways', function( e, data ) {
            }).on('fileuploadprogressall', function( e, data ) {

                var progress = parseInt(data.loaded / data.total * 100, 10);
                $(e.target).closest('div').find('.progress .progress-bar').css(
                        'width',
                        progress + '%'
                        );

            }).on('fileuploaddone', function(e, data) {
                // al terminar de cargar imagen exitosamente:
                 $(e.target).closest('div').find('.progress').fadeOut();
                if (data.result.files.length > 0) {

                    var file = data.result.files[0];

                    // tenemos que primero cargar la imagen en un elemento "ficticio" para poder leer su w / h y despues mostrarlo en la página con dimensiones adaptadas
                    $('<img>').load(function() {
                        var div = $(e.target).closest('div');

                        div.find('img.thumb').remove();
                        div.append('<img src="' + $(this).attr("src") + '" class="thumb" />');
                        $(e.target).closest('.page').find('.imgpos-container').show();

                    }).attr('src', file.url).each(function() {
                        if (this.complete)
                            $(this).load();
                    });
                }

            }).on('fileuploadfail', function(e, data) {
            }).prop('disabled', !$.support.fileInput)
                    .parent().addClass($.support.fileInput ? undefined : 'disabled');
        });


        // UPLOAD sonido interno a término
        $('body').on('created', '.uploads .audio .fileupload', function() {
            $(this).fileupload({
                url: url,
                dataType: 'json',
                autoUpload: true,
                acceptFileTypes: /(\.|\/)(mp3|ogg|wav|wma)$/i,
                maxFileSize: 50 * 1000000 // 50 Mb

            }).on('fileuploadadd', function(e, data) {
//            $(e.target).closest('span').hide();
                $(e.target).closest('div').find('.progress').show();

            }).on('fileuploadprocessalways', function(e, data) {
            }).on('fileuploadprogressall', function(e, data) {

                var progress = parseInt(data.loaded / data.total * 100, 10);
                $(e.target).closest('div').find('.progress .progress-bar').css(
                        'width',
                        progress + '%'
                        );

            }).on('fileuploaddone', function(e, data) {

                $(e.target).closest('div').find('.progress-bar').html("Convirtiendo...");
                if (data.result.files.length > 0) {
                    var file = data.result.files[0];

                    $.ajax({
                        type: "POST",
                        url: "process_upload.php",
                        data: {file_name: file.name, file_path: data.result.physPath},
                        dataType: 'json',
                        success: function(result) {

                            if (result.error == "") {
                                var soundPath = "../../../../" + datafolder + "/uploads/" + result.path_html;
                                var soundPath = "/" + datafolder + "/uploads/" + result.path_html;

                                var mp3Path = soundPath //".mp3";
                                var oggPath = soundPath //".ogg";


                                $(e.target).closest('div').find('.progress').slideUp(200);
                               var container = ".uploads";
                                $(e.target).closest(container).find('.current-sound audio').remove();
                                $(e.target).closest(container).find('.current-sound a').removeClass("loaded").addClass("loaded");
                                $(e.target).closest(container).find('.current-sound').append('<audio><source src="' + oggPath + '" type="audio/ogg"><source src="' + mp3Path + '" type="audio/mp3"></audio>');
                            }
                        }
                    });

                }

            }).on('fileuploadfail', function(e, data) {
            }).prop('disabled', !$.support.fileInput)
                    .parent().addClass($.support.fileInput ? undefined : 'disabled');
        });

        $('.uploads .fileupload').each(function() {
            $(this).trigger('created');
        });


    }, 500);

    $('#add-page').click(function(e) {
        e.preventDefault();
        var i = $('.page').length + 1;
        var newPage = {
            id: "page" + i,
            npage: i,
            title: "",
            src: "",
            texto: "",
            img: "",
            translation_image: JSLang.getTranslation("IMAGE"),
            translation_audio: JSLang.getTranslation("AUDIO"),
            translation_content: JSLang.getTranslation("CONTENT"),
            translation_title: JSLang.getTranslation("TITLE"),
            translation_image_above: JSLang.getTranslation("IMAGE_ABOVE"),
            translation_image_below: JSLang.getTranslation("IMAGE_BELOW"),
            translation_title: JSLang.getTranslation("TITLE"),
            translation_type: JSLang.getTranslation("TYPE"),
            translation_font_color: JSLang.getTranslation("TEXT_COLOR"),
            translation_font_size: JSLang.getTranslation("GAME_CONFIG_TEXT_SIZE"),
            translation_background_color: JSLang.getTranslation("GAME_CONFIG_BACKGROUND"),
            robotic_voice:JSLang.getTranslation("TEXT_TO_SPEECH"),

            /*fonts:[
                {"name":"Arial, 'Helvetica Neue', Helvetica, sans-serif","shortname":"Arial"},
                {"name":"'Arial Black', 'Arial Bold', Gadget, sans-serif","shortname":"Arial Black"},
                {"name":"'Arial Narrow', Arial, sans-serif","shortname":"Arial Narrow"},
                {"name":"'Arial Rounded MT Bold', 'Helvetica Rounded', Arial, sans-serif","shortname":"Arial Rounded"},
                {"name":"'Avant Garde', Avantgarde, 'Century Gothic', CenturyGothic, 'AppleGothic', sans-serif","shortname":"Avant Garde"},
                {"name":"Calibri, Candara, Segoe, 'Segoe UI', Optima, Arial, sans-serif","shortname":"Calibri"},
                {"name":"'Century Gothic', CenturyGothic, AppleGothic, sans-serif","shortname":"Century Gothic"},
                {"name":"'Franklin Gothic Medium', 'Franklin Gothic', 'ITC Franklin Gothic', Arial, sans-serif","shortname":"Franklin Gothic"},
                {"name":"Futura, 'Trebuchet MS', Arial, sans-serif","shortname":"Futura"},
                {"name":"Geneva, Tahoma, Verdana, sans-serif","shortname":"Geneva"},
                {"name":"'Gill Sans', 'Gill Sans MT', Calibri, sans-serif","shortname":"Gill Sans"},
                {"name":"'Helvetica Neue', Helvetica, Arial, sans-serif","shortname":"Helvetica"},
                {"name":"Impact, Haettenschweiler, 'Franklin Gothic Bold', Charcoal, 'Helvetica Inserat', 'Bitstream Vera Sans Bold', 'Arial Black', sans serif","shortname":"Impact"},
                {"name":"'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Geneva, Verdana, sans-serif","shortname":"Lucida"},
                {"name":"Optima, Segoe, 'Segoe UI', Candara, Calibri, Arial, sans-serif","shortname":"Optima"},
                {"name":"'Segoe UI', Frutiger, 'Frutiger Linotype', 'Dejavu Sans', 'Helvetica Neue', Arial, sans-serif","shortname":"Segoe"},
                {"name":"Tahoma, Verdana, Segoe, sans-serif","shortname":"Tahoma"},
                {"name":"'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif","shortname":"Trebuchet"},
                {"name":"TimesNewRoman, 'Times New Roman', Times, Baskerville, Georgia, serif","shortname":"TimesNewRoman"},
                {"name":"Georgia, Times, 'Times New Roman', serif","shortname":"Georgia"},
                {"name":"Cambria, Georgia, serif","shortname":"Cambria"},
                {"name":"'Lucida Console', 'Lucida Sans Typewriter', Monaco, 'Bitstream Vera Sans Mono', monospace","shortname":"Lucida"},
                {"name":"Monaco, Consolas, 'Lucida Console', monospace","shortname":"Monaco"},
                {"name":"'Lucida Sans Typewriter', 'Lucida Console', Monaco, 'Bitstream Vera Sans Mono', monospace","shortname":"Lucida Sans"},
                {"name":"Consolas, monaco, monospace","shortname":"Consolas"},
                {"name":"Verdana","shortname":"Verdana", "select":"selected"},
                {"name":"PT Sans","shortname":"PT Sans"}],
                fsize:[{"number":"16"},{"number":"12"},{"number":"13"},{"number":"14"},{"number":"15"},{"number":"11"},
                {"number":"17"},{"number":"18"},{"number":"19"},{"number":"20"},{"number":"21"},{"number":"22"},{"number":"23"},
                {"number":"24"},{"number":"25"},{"number":"26"},{"number":"27"},{"number":"28"}]*/
                };

        // Combinamos el modelo Javascript con un template, para crear un bloque de HTML y agregarlo al DOM.
        newPage = Mustache.render($(templates).filter('#pageTpl').html(), newPage);
        $('#add-page').before(newPage);

        $('.page').last().find('.fileupload').trigger('created');
        createId();

    });

//      TEMP: funciones de test
//    $('body').append('<a href="#" id="send">dd</a>');
//    $('#send').click(function(e) {
//        e.preventDefault();
//        console.log(getXml());
//        console.log("********************************************");
//        console.log(getIncludes());
//
//    });

    $(document).ready(function(){
       tts = new tts('../../../tts/index.php',"../../../../"+datafolder+"/tts/");
       tts.init();    
    });

});

function readIncludes() {

    var returnStr = "";
    
    $('.page img.thumb').each( function() {
        
        imgSrc = $(this).attr('src');
        
        if (imgSrc.indexOf(datafolder) > -1) {
           // console.log("++ "+imgSrc);
            if (getFileName(imgSrc) != "") {
                returnStr += imgSrc.substr(imgSrc.indexOf(datafolder) ) + ",";
            }
        }
    });
    var sounds = tts.getSounds();

    $.each(sounds,function(key,value){
        if(getFileName(value)){
          //  console.log("SRC 1 = "+value.substr(value.indexOf(datafolder)));
            //returnStr += value.substr(value.indexOf(datafolder))+ ".mp3,";
            //returnStr += value.substr(value.indexOf(datafolder))+ ",.ogg";
            returnStr += value.substr(value.indexOf(datafolder))+ ",";
            returnStr += value.substr(value.indexOf(datafolder))+ ",";
        }
    });
    console.warn(returnStr.substr(0, returnStr.length - 1));
    return  returnStr.substr(0, returnStr.length - 1);
}
