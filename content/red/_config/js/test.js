xmlFile = 'userdata.xml';

if (window.XMLHttpRequest) {

    // Navegadores civilizados
    $.support.cors = true;
    $.ajax({
        type: 'GET',
        url: xmlFile,
        dataType: ($.browser.msie) ? "text" : "xml",
        async: false,
        success: function(data) { init(data, "") },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log('Error al leer XML - ' + errorThrown);
        }
    });
} else {
    if (typeof window.ActiveXObject != 'undefined') {
        // Internet Explorer
        xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
        xmlDoc.async = false;
        while (xmlDoc.readyState != 6) {
        }
        xmlDoc.load(xmlFile);
        init(xmlDoc,"");
    }
}