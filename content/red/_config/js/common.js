var metadata;
window.includes = '';
var datafolder;

if (typeof JSLang !== "object") {
    var JSLang = parent.JSLang;
}

// Clase para crear animación "cargando" - necesitamos JS para animarla

function waitObj() {
    var self = this;
    var intervalId;
    var size = 100;
    var num = 12;
    var duration = 50;
    var i = 0;
    
    this.start = function() {
       if ($('#waiting').length === 0) {
           $('body').append('<div id="waiting"></div>');   
       }
       this.el = $('#waiting');
       this.intervalId = setInterval(this.animate, duration);
       this.el.fadeIn(300);
    };
    
    this.animate = function() {
        //console.log("animating.."+i);
        i = (i+size)%(size*num);
        self.el.css({'background-position': -i+'px'});
    };
    
    this.stop = function() {
       this.el.fadeOut(300, function() {
           clearInterval(self.intervalId);
           self.el.remove();
       }); 
    };
}

function init(data, srcPath, config){
    
    window.xmlContent = '';
    
    if (data !== null && typeof data === 'object') {
        window.xmlContent = $.parseXML(data.xml);
    } else {}

    
    // console.log(config);
    console.log(window.xmlContent);
    window.srcPath = '/' + srcPath;
    console.log(srcPath);
    console.log(window.srcPath);
    
    datafolder = 'data/tmp/';
    $(function() { 
        
        $('[data-translate]').each( function() {
            var txt = JSLang.getTranslation( $(this).attr("data-translate") );
            $(this).html( txt );
        });
        
        $('[placeholder^="{{"]').each( function() {
            var placeholder = $(this).attr("placeholder");
            $(this).attr( "placeholder", JSLang.getTranslation( placeholder.substr(2, placeholder.length - 4) ) );
        });
        
        var wait = new waitObj();
        wait.start();
        
        id_unidad = parent.id_unidad;
        
        console.time("load dependencies");
        $('head').append('<script type="text/javascript" src="../../../lib/javascript/underscore-min.js"></script>');
        $('head').append('<script type="text/javascript" src="../../../lib/javascript/backbone-min.js"></script>');
        $('head').append('<script type="text/javascript" src="../../../lib/javascript/jquery.tokeninput.js"></script>');
        $('head').append('<script type="text/javascript" src="../../../lib/javascript/add_skills_indicators.js"></script>');
        console.timeEnd("load dependencies");
        
        $.get('../js/metadata.mustache', function(templatesFile) {
            
            var metadataModel = Backbone.Model.extend({
                defaults: {
                    competencias: [],
                    indicadores: []
                },
                asXml : function() {
                    var anXml = "<metadata>\n";
                    anXml = anXml + "<created>"+this.attributes.created+"</created>\n";
                    anXml = anXml + "<lastmodified>"+this.attributes.lastmodified+"</lastmodified>\n";
                    anXml = anXml + "<id>"+this.attributes.id+"</id>\n";
                    anXml = anXml + "<description>"+this.attributes.description+"</description>\n"; // OK
                    anXml = anXml + "<autor>"+this.attributes.autor+"</autor>\n";
                    anXml = anXml + "<title>"+this.attributes.title+"</title>\n"; // OK
                    anXml = anXml + "<indicadores>\n";

                    if (this.attributes.indicadores.length > 0) {
                        for (var i = 0; i < this.attributes.indicadores.length; i++) {
                            anXml = anXml + "<indicador>\n";
                            anXml = anXml + "<id>"+this.attributes.indicadores[i].id+"</id>\n";
                            anXml = anXml + "<nombre>"+this.attributes.indicadores[i].name+"</nombre>\n";
                            anXml = anXml + "</indicador>\n";
                        }
                    }

                    anXml = anXml + "</indicadores>\n";
                    anXml = anXml + "<w>"+this.attributes.w+"</w>\n";
                    anXml = anXml + "<wunit>"+this.attributes.wunit+"</wunit>\n";
                    anXml = anXml + "</metadata>\n";
                    return anXml;
                }
            });
            
            var metadataView = Backbone.View.extend({
                
                initialize: function(options) {
                    this.template = options.template;
                    this.render(); 
                },
                events: {
                    "click h2":             "toggleOpen",
                    //"click button.test":    "testOutput",
                    "change":               "change",
                    "change #metadata-description": "descriptionChanged",
                    "change #metadata-title": "titleChanged",
//                    "change .competencias": "skillsChanged",
                    "change .indicadores": "indicatorsChanged",
                    "click .indicador-add": "indicatorAdmin",
                    "change #metadata-w": "wChanged",
                    "change #metadata-wunit": "wUnitChanged",
                    "click svg": "refreshInstance"
                },
                render: function () {
                    var view = this, rendered;

                    rendered = Mustache.to_html(view.template, view.model.toJSON());

                    $(this.el).html(rendered);
                    $(this.el).addClass("closed");


                    
                    if ($(this.el).parents().length < 1) {
                        // Si no está conectado con el DOM, conéctalo
                        $(this.el).prependTo($('body'));
                    }

                    $(this.el).find('#metadata-wunit option[value="'+view.model.get('wunit')+'"]').prop("selected", true);
                    
                    console.log('metadataView rendered');
//                    this.addAutoComplete($(view.el).find('.competencias'), canAddSkills);
                    this.addAutoComplete($(view.el).find('.indicadores'), canAddIndicators, this.model.get('indicadores'));
//                    this.indicatorAdmin($(view.el).find('.indicadores'));
                    
//                    $('a.add-skill').click( function(e) {
//        e.preventDefault();
//        skillAdmin($(this).prevAll('#oa-skills'));
//    }
//    );
                    
                    return view;
                },
                
                toggleOpen: function(e) {
                        e.preventDefault();
                        
                        this.$el.children('ul').slideToggle(300);
                        this.$el.toggleClass('closed');
                },
                /*testOutput: function(e) {
                    e.preventDefault();
                    console.log(this.model.asXml());
                },*/
                indicatorAdmin: function(e) {
                    e.preventDefault();
                    indicatorAdmin(this.$('.indicadores'), false);
                },
                change: function(e) {
                },
                descriptionChanged: function(e) {
                    this.model.set("description", this.$('#metadata-description').val());
                },
                wChanged: function(e) {
                    this.model.set("w", this.$('#metadata-w').val());
                },
                wUnitChanged: function(e) {
                    this.model.set("wunit", this.$('#metadata-wunit').val());
                },
                titleChanged: function(e) {
                    this.model.set("title", this.$('#metadata-title').val());
                },
                indicatorsChanged: function(e) {
                    var idList = this.$('.indicadores').tokenInput("get");
                    this.model.set("indicadores", idList);
                },
                addAutoComplete: function(inputEl, canAdd, prePopObj) {
                    var view = this;
                    inputEl.tokenInput("../../../app/ajax/getIndicators.php?id="+id_unidad, {hintText: "", searchingText: "......", noResultsText: "", minChars: 2, permitCustom: canAdd, zindex: 99999, prePopulate: prePopObj, onAdd: function() { view.indicatorsChanged(); } });
                },
                refreshInstance: function(e) {
                    if (!this.$('p').hasClass("refreshed")) {
                        if (window.confirm("Estás seguro? Esta funcionalidad te permite actualizar a la última versión disponible, pero no se garantiza que sea compatible con tu versión.")) {
                            this.$('p').removeClass("refresh-error");
                            this.$('p').addClass("waiting");
                            $.ajax({
                                url: '../../refresh_content.php',
                                method: 'post',
                                context: this,
                                data: {
                                    refresh: 'true',
                                    src: window.getSrc()
                                },
                                success: function(data) {
                                    this.$('p').removeClass("waiting");
                                    if (data.error !== '') {
                                        this.$('p').addClass("refresh-error");
                                    } else {
                                        this.$('p').addClass("refreshed");
                                    }
                                }
                            });     
                        }
                           
                    }
                    
                }

            });
            
            metadata = new metadataModel();
            
            var metadataXml = $(window.xmlContent).find("metadata");
            
            // Poblamos el modelo con los datos del XML
            metadataXml.children().each( function(i) {
                var tmp = {};
                if (this.tagName !== 'indicadores') {
                    tmp[this.tagName] = $(this).text();
                    metadata.set(tmp);
                }
            });

            // Retrocompatibilidad: para no tener problemas con infografías creadas antes de que
            // fueran agregadas estas dos propiedades nuevas.
            if ( !$.isNumeric( metadata.get("w") ) ) {
                metadata.set("w", 100);
                metadata.set("wunit", "%");
            }

            metadata.set("skills-add", canAddSkills);
            metadata.set("indicators-add", canAddIndicators);
            
            metadataXml.find("indicador").each(function(i, el) {
                metadata.get("indicadores").push( {
                    id: $(this).find('id').text(), 
                    name: $(this).find('nombre').text()
                });
            });
            
            metadata.set("translation_metadata", JSLang.getTranslation("METADATA"));
            metadata.set("translation_title", JSLang.getTranslation("TITLE"));
            metadata.set("translation_description", JSLang.getTranslation("DESCRIPTION"));
            metadata.set("translation_creation_date", JSLang.getTranslation("CREATION_DATE"));
            metadata.set("translation_last_modified", JSLang.getTranslation("LAST_MODIFIED"));
            metadata.set("translation_author", JSLang.getTranslation("AUTHOR"));
            metadata.set("translation_skill_indicators", JSLang.getTranslation("SKILL_INDICATORS"));

            $('head').append('<link rel="stylesheet" href="../css/metadata.css">');
            
            var viewOptions = {
                model: metadata,
                template: templatesFile
            };
            
            if ($('#metadata').length > 0) {
                viewOptions.el = $('#metadata');
            } else {
                viewOptions.tagName = 'section';
                viewOptions.id = 'metadata';
            }
            
            var metadataVista = new metadataView(viewOptions);

//            contentsLoaded();
            
        });
        
        addContent(); 
        if (typeof wait === "object") { wait.stop(); }
    
    });
}

function getXml() {
    var userdata = readParamsToXml();
    var userdataIndex = userdata.indexOf("<root>") + 6;
    window.xmlContent = userdata.substr(0, userdataIndex) + metadata.asXml() + userdata.substr(userdataIndex);
    
    return window.xmlContent;
}

function addInclude(path) {
    window.includes += (window.includes !== "" ? "," : "")+path;
}

function getIncludes() {

    if (typeof readIncludes == "function") {
        window.includes += (window.includes !== "" ? "," : "") +readIncludes();
    }
    return window.includes;
}

function getSrc() {
    return window.srcPath;
}

function contentValid() {
    if (typeof validateConfig === 'function') {
        return validateConfig();
    } else {
        return true;
    }
}

function invalidMsg() {
    if (typeof customInvalidMsg === 'function') {
        return customInvalidMsg();
    } else {
        return JSLang.getTranslation("INVALID_DIALOG");
    }
}

