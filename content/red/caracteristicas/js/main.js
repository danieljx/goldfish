// Constants

var APP_MIN_WIDTH = 768;    // App minimum width for PC
var APP_MAX_WIDTH = 960;    // App maximum width

var XML_FILE = 'userdata.xml';
var JS_XML_FILE = 'userdata.js';

var FONT_URL = "<link href='fonts/{font}.css' rel='stylesheet' type='text/css'>";

// Global vars

var queue,
    animBucket = [],
    fonts = [],
    sounds = [],
    files = [],
    body = $('body'),
    wrapper = $('.wrapper'),
    content = $('.content'),
    loader = $('.loader'),
    cover = $('.cover'),
    mainTitle = $('.main-title'),
    nav = $('nav'),
    cssRules,
    autoplay;
initCssRules();
$(document).on('load ready',function () {
    
    initFlowtype();
    initWindowResize();
    checkWindowResize();
    preload();
});

function isMobile()
{
    return ($(window).width() < APP_MIN_WIDTH);
}

function preload()
{
    // Preload events

    queue = new createjs.LoadQueue(true);
    createjs.Sound.alternateExtensions = ["mp3"];
    queue.installPlugin(createjs.Sound);
    queue.on("progress", onLoadProgress);
    queue.on("complete", onLoadComplete);

    // File loading
    
    if (files.length) {
        queue.loadManifest(files);
    } else {
        init();
    }
}

function onLoadProgress(e)
{
    //loadingBar.value = e.loaded * 100;
}

function onLoadComplete()
{
    loader.addClass('fadeOut');
    
    setTimeout(function () {
        init();
    }, 500);
}

function hideLoader()
{
    loader
        .addClass('complete')
        .removeClass('fadeOut');

    content.addClass('available');
}

function init()
{
    hideLoader();
    initWrapper();
    initAnimations();
    initNavButtons();
    initMobileNavigation();
    checkWindowResize();
}

function initWindowResize()
{
    $(window).resize(checkWindowResize);
}

function checkWindowResize()
{
    var width = $(window).width(),
        container = $('.slides > ul'),
        slides = container.find('> li'),
        activeSlide = slides.filter('.active'),
        i = slides.index(activeSlide),
        containerWidth = (slides.length * 100) + '%',
        slideWidth = (100 / slides.length) + '%',
        btParent = $($('nav > ul > li').eq(i)),
        button;
    
    // Fix parent iframe height
    
    if (cat_plugin) {
        if ($.isFunction(cat_plugin.growFrameH)) {
            cat_plugin.growFrameH(wrapper.height());
        }
    }
    
    if (width < APP_MIN_WIDTH) {
        body.removeAttr('style');
        container.width(containerWidth);
        slides.width(slideWidth);
        goToSlide(i);
    } else {
        container.removeAttr('style');
        slides.removeAttr('style');
        
        if (!btParent.hasClass('cover-slide')) {
            button = btParent.find('> a').click();
        } else {
            if (!nav.hasClass('keep-visible')) {
                mainTitle
                    .addClass('slideInRight')
                    .removeClass('slideOutRight');
            }
        }
    }
}

function initCssRules()
{
    cssRules =  "<style type='text/css'>";
    cssRules += '.slides .cover-slide h2, .main-title {' + '{MAIN_TITLE}' + '}';
    cssRules += '.slides .cover-slide .pic {' + '{COVER}' + '}';
    cssRules += '@media (min-width: 768px) {';
    cssRules += '.cover {' + '{COVER}' + '}';
    cssRules += '.main-title > h1 {' + '{MAIN_TITLE_DESKTOP}' + '}';
    cssRules += '}';
    cssRules += '{SLIDES}';
    cssRules += "</style>";
    initXmlData();
}

function getCssProperty(prop, value, quotes)
{
    if (value != '') {
        value = (quotes) ? "'" + value + "'" : value;
        return prop + ':' + value + ';';
    }
    
    return '';
}

function getSlideCssRules()
{
    var slideCss;
    
    slideCss  = '.slides > ul > {SLIDE_ID} > h2 {' + '{SLIDE_TITLE}' + '}';
    slideCss += '.slides > ul > {SLIDE_ID} > p {' + '{SLIDE_CONTENT}' + '}';
    slideCss += '@media (max-width: 767px) {';
    slideCss += '.slides > ul > {SLIDE_ID} > .pic {' + '{MOB_SLIDE_IMAGE}' + '}';
    slideCss += '}';
    slideCss += '@media (min-width: 768px) {';
    slideCss += '.slides > ul > {SLIDE_ID} > h2 {' + '{SLIDE_TITLE_DESKTOP}' + '}';
    slideCss += '.slides > ul > {SLIDE_ID} > p {' + '{SLIDE_CONTENT_DESKTOP}' + '}';
    slideCss += '.slides > ul > {SLIDE_ID} > a.close {' + '{SLIDE_CLOSE}' + '}';
    slideCss += 'nav > ul > {NAV_ID} > a {' + '{NAV_ANCHOR}' + '}';
    slideCss += '}';
    
    return slideCss;
}

function initXmlData()
{
    if (window.XMLHttpRequest) {
        $.support.cors = true;
        $.ajax({
            url: XML_FILE,
            dataType: "xml",
            async: false
        }).done(function (response) {
            addContent(response);
        }).fail(function (request, status, error) {
            document.write('<script type="text/javascript" src="'+JS_XML_FILE+'"></script>');
        });
    } else {
        if (typeof window.ActiveXObject != 'undefined') {
            var xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
            xmlDoc.async = false;
            
            while (xmlDoc.readyState != 4) {}
            
            xmlDoc.load(XML_FILE);
            addContent(xmlDoc);
        } 
    }
}

function addContent(xmlContents)
{
    var data = $(xmlContents),
        titleNode = data.find('userdata > title'),
        coverNode = data.find('userdata > bg'),
        slideNodes = data.find('userdata > slides > slide'),
        coverSlide = $('.slides li.cover-slide'),
        slidesCss = [],
        cssRule;

    // Title setup
    autoplay = (data.find('userdata > autoplay').text() == 'true');
    cssRule  = getCssProperty('font-family', titleNode.attr('font').replace('+', ' '), true);
    cssRule += getCssProperty('color', titleNode.attr('fontColor'));
    cssRule += getCssProperty('background-color', titleNode.attr('bgColor'));
    cssRules = cssRules.replace('{MAIN_TITLE}', cssRule);
    
    cssRule  = getCssProperty('font-size', titleNode.attr('fontSize'));
    cssRules = cssRules.replace('{MAIN_TITLE_DESKTOP}', cssRule);
    
    if (titleNode.text() != '') {
        mainTitle
            .find('h1')
            .html(titleNode.text());

        coverSlide
            .find('h2')
            .html(titleNode.text());
    }
    
    if (titleNode.attr('font') != '') {
        fonts.push(titleNode.attr('font'));
    }
    
    // Cover setup
    
    cssRule = '';
    
    if (parseInt(coverNode.attr('type')) == 1) {
        if (coverNode.attr('url') != '') {
            coverSlide.find('img').attr('src', coverNode.attr('url'));
            files.push({src: coverNode.attr('url')});
            cssRule = getCssProperty('background-image', "url('" + coverNode.attr('url') + "')");
        } else {
            coverSlide.addClass('no-pic');
        }
    } else if (parseInt(coverNode.attr('type')) == 2) {
        console.log(coverNode.attr('color'));
        cssRule = getCssProperty('background-color', coverNode.attr('color'));
        coverSlide.addClass('no-pic');
    }
    
    cssRules = cssRules.replace(/\{COVER\}/g, cssRule);
    
    // Slides setup
    
    slideNodes.each(function (i) {
        var slideNode = $(this),
            titleNode = slideNode.find('> title'),
            contentNode = slideNode.find('> content'),
            closeNode = slideNode.find('> close'),
            imageNode = slideNode.find('> media > image'),
            soundNode = slideNode.find('> media > sound'),
            iconNode = slideNode.find('> icon'),
            slideContainer = $('.slides > ul'),
            _slide = slideContainer.find('li.animated:first'),
            slide = (i == 0) ? _slide : _slide.clone(),
            slideId = 'slide' + i,
            title = slide.find('> h2'),
            content = slide.find('> p'),
            img = slide.find('> .pic'),
            soundButton = slide.find('> .sound > a'),
            soundData,
            navContainer = nav.find('> ul'),
            _navItem = navContainer.find('li:not(.cover-slide):first'),
            navItem = (i == 0) ? _navItem : _navItem.clone(),
            navItemId = 'nav' + i,
            iconAnchor = navItem.find('a'),
            icon = iconAnchor.find('span > i'),
            iconImage = iconAnchor.find('span > img'),
            slideCss = getSlideCssRules();
        
        // Set slide and nav id
        
        slide.attr('id', slideId);
        navItem.attr('id', navItemId);
        slideCss = slideCss.replace(/\{SLIDE_ID\}/gi, '#'+slideId);
        slideCss = slideCss.replace(/\{NAV_ID\}/gi, '#'+navItemId);
        
        // Title setup
        
        cssRule  = getCssProperty('font-family', titleNode.attr('font').replace('+', ' '), true);
        cssRule += getCssProperty('color', titleNode.attr('fontColor'));
        cssRule += getCssProperty('background-color', titleNode.attr('bgColor'));
        slideCss = slideCss.replace('{SLIDE_TITLE}', cssRule);
        
        cssRule  = getCssProperty('font-size', titleNode.attr('fontSize'));
        slideCss = slideCss.replace('{SLIDE_TITLE_DESKTOP}', cssRule);
        
        cssRule  = getCssProperty('background-color', titleNode.attr('bgColor'));
        slideCss = slideCss.replace('{MOB_SLIDE_IMAGE}', cssRule);
        
        if (titleNode.text() != '') {
            title
                .find('span')
                .html(titleNode.text());
        }
        
        if (titleNode.attr('font') != '') {
            fonts.push(titleNode.attr('font'));
        }
        
        // Content setup
        
        cssRule  = getCssProperty('font-family', contentNode.attr('font').replace('+', ' '), true);
        cssRule += getCssProperty('color', contentNode.attr('fontColor'));
        cssRule += getCssProperty('background-color', contentNode.attr('bgColor'));
        slideCss = slideCss.replace('{SLIDE_CONTENT}', cssRule);
        
        cssRule  = getCssProperty('font-size', contentNode.attr('fontSize'));
        slideCss = slideCss.replace('{SLIDE_CONTENT_DESKTOP}', cssRule);
        
        if (contentNode.text() != '') {
            content.html(contentNode.text());
        }
        
        if (contentNode.attr('font') != '') {
            fonts.push(contentNode.attr('font'));
        }
        
        // Close button setup
        
        cssRule  = getCssProperty('color', closeNode.attr('color'));
        slideCss = slideCss.replace('{SLIDE_CLOSE}', cssRule);
        
        // Image setup
        
        slide.removeClass('no-pic');
        
        if (imageNode.attr('url') != '') {
            files.push({src: imageNode.attr('url')});
            img.find('img').attr('src', imageNode.attr('url'));
            img.find('img').on("click", function(e) {
                cat_plugin.imgFull(this.src);
            });
        } else {
            slide.addClass('no-pic');
        }
        
        // Sound setup
        
        slide.removeClass('no-sound');
        
        if (soundNode.attr('ogg') != '' && soundNode.attr('mp3') != '') {
            soundData = {ogg: soundNode.attr('ogg'), mp3: soundNode.attr('mp3')};
            sounds.push({src: soundData, id: soundNode.attr('id')});
            soundButton.data('sound', soundNode.attr('id'));
        } else {
            slide.addClass('no-sound');
        }
        
        // Menu icon setup
        
        cssRule = getCssProperty('background-color', iconNode.attr('bgColor'));
        
        if (titleNode.text() != '') {
            iconAnchor.data('title', titleNode.text());
        }
        
        navItem.removeClass('image');
        
        if (iconNode.attr('type') == 1) {
            cssRule += getCssProperty('color', iconNode.attr('color'));
            icon.attr('class', iconNode.attr('code'));
        } else if (iconNode.attr('type') == 2 && iconNode.attr('url') != '') {
            files.push({src: iconNode.attr('url')});
            iconImage.attr('src', iconNode.attr('url'));
            navItem.addClass('image');
        }
        
        slideCss = slideCss.replace('{NAV_ANCHOR}', cssRule);
        
        // Add slide to container
        
        if (i > 0) {
            slideContainer.append(slide);
            nav.find('> ul').append(navItem);
        }
        
        // Add css rule to slides css
        
        slidesCss.push(slideCss);
    });
    
    // Register sounds
    
    if (sounds.length) {
        createjs.Sound.registerSounds(sounds, '');
    }
    
    // Add fonts to page
    
    fonts = removeDuplicates(fonts);
    
    $.each(fonts, function (i, font) {
        $('head').append(FONT_URL.replace('{font}', font));
    });
    
    // Add custom css rules to page
    
    cssRules = cssRules.replace('{SLIDES}', slidesCss.join(''));
    $('head').append($(cssRules));
    cat_plugin.waitForHeight();
}

function initFlowtype()
{
    $('body').flowtype({
        minimum: APP_MIN_WIDTH,
        fontRatio: 60
    });
}

function initWrapper()
{
    wrapper.mouseenter(function (e) {
        if (!isMobile()) {
            
            // Show navigation menu with animation

            if (!nav.hasClass('keep-visible')) {
                nav .addClass('slideInUp')
                    .removeClass('slideOutDown');
            }
        }
        
        e.stopPropagation();
        return;
    }).mouseleave(function (e) {
        if (!isMobile()) {
            
            // Hide navigation menu with animation

            if (!nav.hasClass('keep-visible')) {
                nav .addClass('slideOutDown')
                    .removeClass('slideInUp');
            }
        }
        
        e.stopPropagation();
        return;
    });
}

function initAnimations()
{
    // Show cover with animation
    
    cover.addClass('fadeIn');
    
    // Show main title with animation
    
    setTimeout(function () {
        mainTitle.addClass('slideInRight');
    }, 500);
    
    // Show navigation menu with animation
    
    setTimeout(function () {
        nav.addClass('slideInUp');
    }, 1000);
}

function initNavButtons()
{
    var listItems = $('nav > ul > li'),
        buttons = listItems.find('> a'),
        tip = $('.slide-title-tip'),
        slides = $('.slides > ul > li');
    
    // Navigation button events
    
    buttons.click(function () {
        if (!isMobile()) {
            var anchor = $(this),
                activeAnchor = buttons.filter('.active'),
                i = listItems.index(anchor.parent()),
                slide = $(slides.eq(i)),
                slideTitle = slide.find('> h2'),
                slideClose = slide.find('> a.close'),
                slideCont = slide.find('> p'),
                slideImage = slide.find('> .pic'),
                slideSound = slide.find('> .sound'),
                slideSoundButton = slide.find('> .sound > a'),
                activeSlide = slides.filter('.active');

            activeAnchor.removeClass('active');
            anchor.addClass('active');

            // Clean animation bucket

            cleanAnimationBucket();

            // Stop audio (if any)

            createjs.Sound.stop();

            // Remove nav protection (if any)

            if (nav.hasClass('keep-visible')) {
                nav.removeClass('keep-visible');
            }

            // Hide active slide

            activeSlide
                .removeClass('active')
                .find('> .animated').removeClass('slideInLeft slideInRight slideInUp slideInDown');

            // Hide main title (if visible) with animation

            mainTitle.addClass('slideOutRight').removeClass('slideInRight');

            // Hide title tip with animation

            tip .addClass('fadeOut')
                .removeClass('fadeIn');

            // Enable new active slide

            slide.addClass('active');

            // Show slide title with animation

            animBucket.push(setTimeout(function () {
                slideTitle.addClass('slideInLeft');
            }, 500));

            // Show slide content with animation

            animBucket.push(setTimeout(function () {
                slideCont.addClass('slideInDown');
            }, 1000));

            // Show slide image with animation

            animBucket.push(setTimeout(function () {
                slideImage.addClass('slideInUp');
            }, 1500));
            
            // Show slide close button with animation

            animBucket.push(setTimeout(function () {
                slideClose.addClass('slideInRight');
            }, 2000));
            
            // Show slide sound button with animation
            animBucket.push(setTimeout(function () {
                slideSound.addClass('slideInDown');
                if (autoplay) {
                    slideSound.removeClass('active');
                    slideSoundButton.trigger('click');
                } 
            }, 2500));


        } 
    }).mouseenter(function (e) {
        var anchor = $(this);
        
        if (!anchor.hasClass('active')) {
            
            // Show tip with animation and set the title text

            tip .addClass('fadeIn')
                .removeClass('fadeOut')
                .find('span')
                .html(anchor.data('title'));
        }
        
        e.stopPropagation();
        return;
    }).mouseleave(function (e) {
        var anchor = $(this);
        
        if (!anchor.hasClass('active')) {
        
            // Hide tip with animation

            tip .addClass('fadeOut')
                .removeClass('fadeIn');
        }
        
        e.stopPropagation();
        return;
    });
    
    // Slide inner events
    
    slides.each(function () {
        var slide = $(this),
            closeButton = slide.find('> a.close'),
            soundButton = slide.find('> .sound > a');
        
        // Slide close button event
        
        closeButton.click(function () {
            buttons
                .filter('.active')
                .removeClass('active');
            
            // Set cover slide as active button
            
            listItems
                .filter('.cover-slide')
                .find('> a')
                .addClass('active');
            
            // Stop audio (if any)
        
            createjs.Sound.stop();
            
            // Hide slide with animation
            
            slide.addClass('fadeOut');
            
            setTimeout(function () {
                
                // Show main title with animation
                
                slide.removeClass('active fadeOut')
                     .find('> .animated').removeClass('slideInLeft slideInRight slideInUp slideInDown');
                
                // Set cover slide as active slide
                
                slides
                    .filter('.cover-slide')
                    .addClass('active');
                
                // Show main title with animation
                
                mainTitle
                    .addClass('slideInRight')
                    .removeClass('slideOutRight');
            }, 500);
        });
        
        // Play/Stop slide sound
        
        soundButton.click(function () {
            var instance;
            soundButton.toggleClass('active');
            
            if (soundButton.hasClass('active')) {
                instance = createjs.Sound.play(soundButton.data('sound'));
                instance.on('complete', onSoundComplete);
            } else {
                createjs.Sound.stop();
            }
        }); 
    });
}

//

function onSoundComplete()
{
    $('.slides .active .sound > a').toggleClass('active');
}

function cleanAnimationBucket()
{
    for (var i in animBucket) {
        clearTimeout(animBucket[i]);
    }
}

function initMobileNavigation()
{
    // Button events
    
    $('.nav-mobile .nav-prev, .nav-mobile .nav-next').click(function () {
        moveSlide(($(this).hasClass('nav-prev')) ? -1 : 1);
    });
    
    // Swipe events
    
    $('.wrapper').hammer()
        .bind('swipeleft', function () {
            moveSlide(1);
        })
        .bind('swiperight', function () {
            moveSlide(-1);
        });
}

function moveSlide(direction)
{
    var slides = $('.slides > ul > li'),
        activeSlide = slides.filter('.active'),
        i = slides.index(activeSlide) + direction;
    
    // New slide index correction
    
    if (i < 0) {
        i = slides.length - 1;
    } else if (i == slides.length) {
        i = 0;
    }
    
    // Go to new slide
    
    goToSlide(i);
}

function goToSlide(i)
{
    var container = $('.slides > ul'),
        slides = container.find('> li'),
        slideWidth = (100 / slides.length),
        activeSlide = slides.filter('.active'),
        bullets = nav.find('> ul > li'),
        activeBullet = bullets.find('> a.active'),
        slide, bullet, posX;
    
    slide = $(slides.eq(i));
    slideSoundButton = slide.find('> .sound > a');
    bullet = $(bullets.eq(i)).find('> a');
    posX = 'translateX(-' + (i * slideWidth) + '%)';
    
    activeSlide.removeClass('active');
    slide.addClass('active');
    activeBullet.removeClass('active');
    bullet.addClass('active');
    
    // Setting up new position for slide container
    
    container.css({
        transform: posX,
        MozTransform: posX,
        WebkitTransform: posX,
        msTransform: posX
    });
    
    // Stop audio (if any)

    createjs.Sound.stop();
    if (autoplay) {
    	slideSoundButton.trigger('click');
    }
}
