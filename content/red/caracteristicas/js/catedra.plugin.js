function catedra_plugin() {
    var that = this;
    var timeStamp = new Date().getTime();
    this.id = "";
    
    this.aspectRatio = 1;
    this.limitByHeight = false;
    this.plugin = false;
    this.ready = false;
    
    this.setReady = function() {
        this.ready = true;
        console.log("#### PLUGIN READY ####");
        this.play(this.limitByHeight);
    };
    
    this.loadedCallback = function() {};
    
    this.setParameters = function(plugin, idPrefix, aspectRatio, limitByHeight) {
        this.aspectRatio = aspectRatio;
        this.plugin = plugin;
        this.limitByHeight = limitByHeight;
        this.id = idPrefix + "_" + timeStamp + "_" + Math.floor(Math.random()*1000);
        this.plugin.setLimitByHeight(this.limitByHeight);
        console.log("Limit by "+(this.limitByHeight ? "height" : "width"));
        try {
            if (window.frameElement) {
                console.log(window.parent.oa);
                this.waitForHeight();
                $(window).resize( that.waitForHeight );
            }
        } catch(e) {

        }
    };
    
    this.changeAspectRatio = function(z) {
        this.aspectRatio = z;
        this.plugin.redraw(this.aspectRatio, this.limitByHeight);
    };
    
    this.play = function() {
        console.log("*** catplugin play");
        if (this.plugin && this.ready)
            this.plugin.play(this.limitByHeight);
    };

    this.growFrameH = function(newH) {
        try {
            console.log(that.frameId);
            if (window.parent && that.iframeId) {
                window.parent.postMessage(JSON.stringify({msg:'growFrameH',id: that.iframeId, height: newH}),'*'); 
                return true;
            } else {
                return false;
            }
        } catch(e) {
        }
    };

    this.imgFull = function(imgUrl) {
        try {
            if (window.parent && that.iframeId) {
                window.parent.postMessage(JSON.stringify({msg:'showImgFull',id: that.iframeId, src:imgUrl}),'*'); 
                return true;
            } else {
                return false;
            }
        } catch(e) {
        }
    };

    this.waitForHeight = function() {
        var h = $('body').height(); 
        console.log(that.id+" h:"+h);
        if ((h > 0) && that.growFrameH(h)) {
            
        } else {
            setTimeout(that.waitForHeight, 500);
        }
    };

    this.receiveMessage = function (event) {
        var id = event.data.split("I am your father,")[1];
        that.iframeId = id;
    };

}

var cat_plugin = new catedra_plugin();

window.addEventListener("message", cat_plugin.receiveMessage, false);