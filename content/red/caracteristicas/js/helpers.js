function removeDuplicates(source)
{
    var unique = [];
    
    if ($.isArray(source)) {
        $.each(source, function(i, el){
            if ($.inArray(el, unique) === -1) {
                unique.push(el);
            }
        });
    }
    
    return unique;
}