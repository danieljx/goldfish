function removeDuplicates(source)
{
    var unique = [];
    
    if ($.isArray(source)) {
        $.each(source, function(i, el){
            if ($.inArray(el, unique) === -1) {
                unique.push(el);
            }
        });
    }
    
    return unique;
}

function changeUrl(url, reverse)
{
    if (url != "") {
        if (!reverse) {
            url = url.split("/");
            url = url.pop();
            url = "files/"+url;
        } else {
            url = window.srcPath + "/" +url;
        }
        
        return url;
    }
    
    return '';

}