function catedra_plugin() {
    var that = this;
    var timeStamp = new Date().getTime();
    this.id = "";
    
    this.aspectRatio = 1;
    this.limitByHeight = false;
    this.plugin = false;
    this.ready = false;
    
    this.setReady = function() {
        this.ready = true;
        console.log("#### PLUGIN READY ####");
        this.play(this.limitByHeight);
    };
    
    this.loadedCallback = function() {};
    
    this.setParameters = function(plugin, idPrefix, aspectRatio, limitByHeight) {
        this.aspectRatio = aspectRatio;
        this.plugin = plugin;
        this.limitByHeight = limitByHeight;
        this.id = idPrefix + "_" + timeStamp + "_" + Math.floor(Math.random()*1000);
        this.plugin.setLimitByHeight(this.limitByHeight);
        console.log("Limit by "+(this.limitByHeight ? "height" : "width"));
        try {
            if (window.frameElement) {
                console.log(window.parent.oa);
                this.waitForHeight();
                $(window).resize( that.waitForHeight );
            }
        } catch(e) {

        }
    };
    
    this.changeAspectRatio = function(z) {
        this.aspectRatio = z;
        this.plugin.redraw(this.aspectRatio, this.limitByHeight);
    };
    
    this.play = function() {
        console.log("*** catplugin play");
        if (this.plugin && this.ready)
            this.plugin.play(this.limitByHeight);
    };

    this.growFrameH = function(newH) {
        try {
            if (window.frameElement) {
                // Si estamos dentro de un IFRAME, hazlo crecer en sentido Y para que tenga la misma altura que la infografÌa
                $('#'+window.frameElement.getAttribute('id'), window.parent.document).height(newH);
            }
        } catch(e) {

        }
    };

    this.waitForHeight = function() {
        var h = $('body').height(); 
        console.log(that.id+" h:"+h);
        if (h > 0) {
            that.growFrameH(h);
        } else {
            setTimeout(that.waitForHeight, 500);
        }
    };

}

var cat_plugin = new catedra_plugin();