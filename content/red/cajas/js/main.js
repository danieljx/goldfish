// Constants

var APP_MIN_WIDTH = 768;    // App minimum width for PC
var APP_MAX_WIDTH = 960;    // App maximum width

var XML_FILE = 'userdata.xml';
var JS_XML_FILE = 'userdata.js';

var FONT_URL = "<link href='fonts/{font}.css' rel='stylesheet' type='text/css'>";

// Global vars

var queue,
    animBucket = [],
    fonts = [],
    sounds = [],
    files = [],
    body = $('body'),
    wrapper = $('.wrapper'),
    content = $('.content'),
    loader = $('.loader'),
    cover = $('.cover'),
    coverTitle = cover.find('h1'),
    coverImage = cover.find('.image'),
    btnMenu = $('.menu-button'),
    nav = $('nav'),
    cssRules,
    autoplay;

//

initCssRules();
initXmlData();

$(document).on('load ready',function () {
    initFlowtype();
    initWindowResize();
    checkWindowResize();
    preload();
});

//

function isMobile()
{
    return ($(window).width() < APP_MIN_WIDTH);
}

//

function delay(t)
{
    return isMobile() ? 0 : t;set
}

//

function preload()
{
    // Preload events

    queue = new createjs.LoadQueue(true);
    createjs.Sound.alternateExtensions = ["mp3"];
    queue.installPlugin(createjs.Sound);
    queue.on("progress", onLoadProgress);
    queue.on("complete", onLoadComplete);

    // File loading
    
    if (files.length) {
        queue.loadManifest(files);
    } else {
        //init();
        onLoadComplete();
    }
}

//

function onLoadProgress(e)
{
    //loadingBar.value = e.loaded * 100;
}

//

function onLoadComplete()
{
    loader.addClass('fadeOut');
    
    setTimeout(function () {
        init();
    }, delay(500));
}

//

function hideLoader()
{
    loader
        .addClass('complete')
        .removeClass('fadeOut');

    content.addClass('available');
}

//

function cleanAnimationBucket()
{
    for (var i in animBucket) {
        clearTimeout(animBucket[i]);
    }
}

//

function init()
{
    hideLoader();
    initAnimations();
    createMobileNav();
    initNavButtons();
    checkWindowResize();
}

//

function initAnimations()
{
    // Show cover with animation
    
    cover.addClass('fadeIn');
    
    // Show cover title with animation
    
    setTimeout(function () {
        coverTitle.addClass('fadeInDown');
    }, delay(500));
    
    // Show cover image with animation
    
    setTimeout(function () {
        coverImage.addClass('fadeIn');
    }, delay(1000));
    
    // Show navigation menu with animation
    
    setTimeout(function () {
        nav.addClass('fadeInUp');
    }, delay(1500));
}

//

function createMobileNav()
{
    btnMenu.sidr({
        name: 'mobile-menu',
        source: '.menu'
    });
}

//

function initNavButtons()
{
    var listItems = $('nav > ul > li'),
        _listItems = $('nav:visible > ul > li'),
        buttons = listItems.find('> a'),
        tip = $('.slide-title-tip'),
        slides = $('.slides > ul > li');
    
    // Navigation button events
    
    buttons
        .click(function () {

            var anchor = $(this),
                activeAnchor = listItems.filter('.active'),
                i = _listItems.index(anchor.parent()),
                slide = $(slides.eq(i)),
                slideText = slide.find('.text'),
                slideImage = slide.find('.image'),
                slideClose = slide.find('a.close'),
                slideSound = slide.find('.sound'),
                activeSlide = slides.filter('.active');
                slideSoundButton = slide.find('> .sound');

            activeAnchor.removeClass('active');
            anchor.parent().addClass('active');

            // Hide cover (mobile only)

            cover.addClass('mobile-hidden');

            // Hide menu button (mobile only)

            btnMenu.addClass('mobile-hidden');

            // Close sidebar (mobile only)

            $.sidr('close', 'mobile-menu');

            // Clean animation bucket

            cleanAnimationBucket();

            // Stop audio (if any)

            createjs.Sound.stop();

            // Hide nav bar (if visible) with animation

            nav.toggleClass('fadeOutDown fadeInUp');

            // Hide active slide

            activeSlide
                .removeClass('active')
                .find('> .animated').removeClass('slideInLeft slideInRight slideInUp slideInDown');

            // Hide cover title (if visible) with animation

            coverTitle.toggleClass('fadeOutUp fadeInDown');

            // Hide cover image (if visible) with animation

            coverImage.toggleClass('fadeIn fadeOut');

            // Hide title tip with animation

            tip .addClass('fadeOut')
                .removeClass('fadeIn');

            // Enable new active slide

            slide.addClass('active');

            // Show slide text with animation

            animBucket.push(setTimeout(function () {
                slideText.addClass('fadeInLeft');
            }, delay(500)));

            // Show slide image with animation

            animBucket.push(setTimeout(function () {
                slideImage.addClass('fadeInRight');
            }, delay(1000)));

            // Show slide close button with animation

            animBucket.push(setTimeout(function () {
                slideClose.addClass('fadeInDown');
            }, delay(1500)));

            // Show slide sound button with animation

            animBucket.push(setTimeout(function () {
                slideSound.addClass('fadeInUp');
                checkWindowResize();
            }, delay(2000)));

            if (autoplay && !(slide.hasClass('no-sound'))) {
                console.log("autoplay!");
                console.log(slideSoundButton);
                slideSoundButton.trigger('click');
            } else {
                console.log("no autoplay..");
            }
        })
        .mouseenter(function (e) {
            var anchor = $(this);

            if (!anchor.parent().hasClass('active')) {

                // Show tip with animation and set the title text

                tip .addClass('fadeIn')
                    .removeClass('fadeOut')
                    .find('span')
                    .html(anchor.data('title'));
            }

            e.stopPropagation();
            return;
        })
        .mouseleave(function (e) {
            var anchor = $(this);

            if (!anchor.parent().hasClass('active')) {

                // Hide tip with animation

                tip .addClass('fadeOut')
                    .removeClass('fadeIn');
            }

            e.stopPropagation();
            return;
        });
    
    // Slide inner events
    
    slides.each(function () {
        var slide = $(this),
            closeButton = slide.find('> a.close'),
            soundButton = slide.find('> .sound');
        
        // Slide close button event
        
        closeButton.click(function () {
            listItems
                .filter('.active')
                .removeClass('active');
            
            // Stop audio (if any)
        
            createjs.Sound.stop();
            onSoundComplete();
            // Hide slide with animation
            
            slide.addClass('fadeOut');
            
            setTimeout(function () {
                
                // Show cover (mobile only)

                cover.removeClass('mobile-hidden');
                
                // Show menu button (mobile only)

                btnMenu.removeClass('mobile-hidden');
                
                // Show main title with animation
                
                slide.removeClass('active fadeOut')
                     .find('.animated').removeClass('fadeInLeft fadeInRight fadeInUp fadeInDown');
                
                // Show cover title with animation
                
                coverTitle
                    .addClass('fadeInDown')
                    .removeClass('fadeOutUp');
                
                // Show cover image with animation

                coverImage.toggleClass('fadeIn fadeOut');
                
                // Show nav bar with animation

                nav.addClass('fadeInUp')
                    .removeClass('fadeOutDown');
                
                checkWindowResize();
            }, delay(500));
        });
        
        // Play/Stop slide sound
        
        soundButton.click(function () {
            var instance;
            soundButton.toggleClass('active');
            
            if (soundButton.hasClass('active')) {
                instance = createjs.Sound.play(soundButton.data('sound'));
                instance.on('complete', onSoundComplete);
            } else {
                createjs.Sound.stop();
            }
        }); 
    });
}

//

function onSoundComplete()
{
    $('.slides .active .sound').removeClass('active');
}

//

function initCssRules()
{
    cssRules =  "<style type='text/css'>";
    cssRules += '.cover {' + '{COVER}' + '}';
    cssRules += '.cover h1 {' + '{COVER_TITLE}' + '}';
    cssRules += '@media (min-width: 768px) {';
    cssRules += '.cover h1 {' + '{COVER_TITLE_DESKTOP}' + '}';
    cssRules += '.slide-title-tip {' + '{COVER_TOOLTIP_DESKTOP}' + '}';
    cssRules += '}';
    cssRules += '{SLIDES}';
    cssRules += "</style>";
}

//

function getCssProperty(prop, value, quotes)
{
    if (value != '') {
        value = (quotes) ? "'" + value + "'" : value;
        return prop + ':' + value + ';';
    }
    
    return '';
}

//

function getSlideCssRules()
{
    var slideCss;
    
    slideCss  = '.slides > ul > {SLIDE_ID} .text {' + '{SLIDE_CONTENT_BORDER}' + '}';
    slideCss += '.slides > ul > {SLIDE_ID} .text .container {' + '{SLIDE_CONTENT_BG}' + '}';
    slideCss += '.slides > ul > {SLIDE_ID} h2 {' + '{SLIDE_TITLE}' + '}';
    slideCss += '.slides > ul > {SLIDE_ID} p {' + '{SLIDE_CONTENT}' + '}';
    slideCss += '.slides > ul > {SLIDE_ID} a.close {' + '{SLIDE_CLOSE}' + '}';
    slideCss += '@media (min-width: 768px) {';
    slideCss += '.slides > ul > {SLIDE_ID} h2 {' + '{SLIDE_TITLE_DESKTOP}' + '}';
    slideCss += '.slides > ul > {SLIDE_ID} p {' + '{SLIDE_CONTENT_DESKTOP}' + '}';
    slideCss += 'nav > ul > {NAV_ID} > a {' + '{NAV_ANCHOR}' + '}';
    slideCss += '}';
    
    return slideCss;
}

//

function initXmlData()
{
    if (window.XMLHttpRequest) {
        $.support.cors = true;
        $.ajax({
            url: XML_FILE,
            dataType: "xml",
            async: false
        }).done(function (response) {
            addContent(response);
        }).fail(function (request, status, error) {
            document.write('<script type="text/javascript" src="'+JS_XML_FILE+'"></script>');
        });
    } else {
        if (typeof window.ActiveXObject != 'undefined') {
            var xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
            xmlDoc.async = false;
            
            while (xmlDoc.readyState != 4) {}
            
            xmlDoc.load(XML_FILE);
            addContent(xmlDoc);
        } 
    }
}

//

function initFlowtype()
{
    $('body').flowtype({
        minimum: APP_MIN_WIDTH,
        fontRatio: 60
    });
}

//

function initWindowResize()
{
    $(window).resize(checkWindowResize);
}

//

function checkWindowResize()
{
    // Fix parent iframe height
    
    if (cat_plugin) {
        if ($.isFunction(cat_plugin.growFrameH)) {
            cat_plugin.growFrameH($('body').outerHeight(true));
        }
    }
}

//

function addContent(xmlContents)
{
    var data             = $(xmlContents),
        coverNode        = data.find('userdata > cover'),
        coverTitleNode   = coverNode.find('> title'),
        coverImageNode   = coverNode.find('> image'),
        coverTooltipNode = coverNode.find('> tooltip'),
        slideNodes = data.find('userdata > slides > slide'),
        slidesCss = [],
        cssRule;

    autoplay = (data.find('userdata > autoplay').text() == 'true');

    // Cover setup
    
    cssRule  = getCssProperty('background-color', coverNode.attr('bgColor'));
    cssRules = cssRules.replace('{COVER}', cssRule);
    
    cssRule  = getCssProperty('font-family', coverTitleNode.attr('font').replace('+', ' '), true);
    cssRule += getCssProperty('color', coverTitleNode.attr('fontColor'));

    cssRules = cssRules.replace('{COVER_TITLE}', cssRule);
    
    cssRule  = getCssProperty('font-size', coverTitleNode.attr('fontSize'));
    cssRules = cssRules.replace('{COVER_TITLE_DESKTOP}', cssRule);
    
    if (coverTitleNode.text() != '') {
        coverTitle.find('span')
            .html(coverTitleNode.text());
    }
    
    if (coverTitleNode.attr('font') != '') {
        fonts.push(coverTitleNode.attr('font'));
    }
    
    if (coverImageNode.attr('url') != '') {
        files.push({src: coverImageNode.attr('url')});
        coverImage.find('img')
            .attr('src', coverImageNode.attr('url'));
    } else {
        cover.addClass('no-img');
    }
    
    cssRule  = getCssProperty('background-color', coverTooltipNode.attr('bgColor'));
    cssRule += getCssProperty('color', coverTooltipNode.attr('fontColor'));
    cssRules = cssRules.replace('{COVER_TOOLTIP_DESKTOP}', cssRule);
    
    // Slides setup
    
    slideNodes.each(function (i) {
        var slideNode = $(this),
            titleNode = slideNode.find('> title'),
            contentNode = slideNode.find('> content'),
            closeNode = slideNode.find('> close'),
            imageNode = slideNode.find('> media > image'),
            soundNode = slideNode.find('> media > sound'),
            iconNode = slideNode.find('> icon'),
            slideContainer = $('.slides > ul'),
            _slide = slideContainer.find('li.animated:first'),
            slide = (i == 0) ? _slide : _slide.clone(),
            slideId = 'slide' + i,
            title = slide.find('.text h2'),
            content = slide.find('.text p'),
            img = slide.find('.image'),
            soundButton = slide.find('.sound'),
            soundData,
            navContainer = nav.find('> ul'),
            _navItem = navContainer.find('li:first'),
            navItem = (i == 0) ? _navItem : _navItem.clone(),
            navItemId = 'nav' + i,
            iconAnchor = navItem.find('a'),
            icon = iconAnchor.find('i'),
            iconImage = iconAnchor.find('img'),
            slideCss = getSlideCssRules();

  
        
        // Set slide and nav id
        
        slide.attr('id', slideId);
        navItem.attr('id', navItemId);
        slideCss = slideCss.replace(/\{SLIDE_ID\}/gi, '#'+slideId);
        slideCss = slideCss.replace(/\{NAV_ID\}/gi, '#'+navItemId);
        
        //
        
        cssRule  = getCssProperty('background-color', contentNode.attr('borderColor'));
        slideCss = slideCss.replace('{SLIDE_CONTENT_BORDER}', cssRule);
        
        cssRule  = getCssProperty('background-color', contentNode.attr('bgColor'));
        slideCss = slideCss.replace('{SLIDE_CONTENT_BG}', cssRule);
        
        // Title setup
        
        cssRule  = getCssProperty('font-family', titleNode.attr('font').replace('+', ' '), true);
        cssRule += getCssProperty('color', titleNode.attr('fontColor'));
        cssRule += getCssProperty('border-color', titleNode.attr('fontColor'));
        slideCss = slideCss.replace('{SLIDE_TITLE}', cssRule);
        
        cssRule  = getCssProperty('font-size', titleNode.attr('fontSize'));
        slideCss = slideCss.replace('{SLIDE_TITLE_DESKTOP}', cssRule);
        
        if (titleNode.text() != '') {
            title.html(titleNode.text());
        }
        
        if (titleNode.attr('font') != '') {
            fonts.push(titleNode.attr('font'));
        }
        
        // Content setup
        
        cssRule  = getCssProperty('font-family', contentNode.attr('font').replace('+', ' '), true);
        cssRule += getCssProperty('color', contentNode.attr('fontColor'));
        slideCss = slideCss.replace('{SLIDE_CONTENT}', cssRule);
        
        cssRule  = getCssProperty('font-size', contentNode.attr('fontSize'));
        slideCss = slideCss.replace('{SLIDE_CONTENT_DESKTOP}', cssRule);
        
        if (contentNode.text() != '') {
            content.html(contentNode.text());
        }
        
        if (contentNode.attr('font') != '') {
            fonts.push(contentNode.attr('font'));
        }
        
        // Close button setup
        
        cssRule  = getCssProperty('background-color', closeNode.attr('color'));
        slideCss = slideCss.replace('{SLIDE_CLOSE}', cssRule);
        
        // Image setup
        
        slide.removeClass('no-img');
        
        if (imageNode.attr('url') != '') {
            files.push({src: imageNode.attr('url')});
            img.find('img').attr('src', imageNode.attr('url'));
        } else {
            slide.addClass('no-img');
        }
        
        // Sound setup
        
        slide.removeClass('no-sound');
        
        if (soundNode.attr('ogg') != '' && soundNode.attr('mp3') != '') {
            soundData = {ogg: soundNode.attr('ogg'), mp3: soundNode.attr('mp3')};
            sounds.push({src: soundData, id: soundNode.attr('id')});
            soundButton.data('sound', soundNode.attr('id'));
        } else {
            slide.addClass('no-sound');
        }
        
        // Menu icon setup
        
        cssRule = getCssProperty('background-color', iconNode.attr('bgColor'));
        
        if (titleNode.text() != '') {
            iconAnchor.data('title', titleNode.text())
                .find('span')
                .text(titleNode.text());
        }
        
        navItem.removeClass('image');
        
        if (iconNode.attr('type') == 1) {
            cssRule += getCssProperty('color', iconNode.attr('color'));
            icon.attr('class', iconNode.attr('code'));
        } else if (iconNode.attr('type') == 2 && iconNode.attr('url') != '') {
            files.push({src: iconNode.attr('url')});
            iconImage.attr('src', iconNode.attr('url'));
            navItem.addClass('image');
        }
        
        slideCss = slideCss.replace('{NAV_ANCHOR}', cssRule);
        
        // Add slide to container
        
        if (i > 0) {
            slideContainer.append(slide);
            nav.find('> ul').append(navItem);
        }

        // 20170810 Mats Fjellner - los siguientes bloques se comentan por causar errores
        // var images_ = []; 
        
        // data.find("image_slide").each(function(){
        //        images_.push({"id": $(this).attr('data-id'), "elem": $(this).text()});
        // });

        //  $(".slides ul").find("li.animated").each(function(index){
            
        //   $(this).find('.image').css({"display":"none"});
        //     console.log(images_)
        //        if( images_[index] != '' ){
        //          if( images_[index].elem != '' &&  images_[index].id == index ){
                  
        //             var wts = $("body").height();
                        
        //             $(this)
        //             .find('.container > p')
        //             .css({  
        //                 "width":"55%", "height": parseInt(wts - 250)+"px", 
        //                 "overflow": "auto", "overflow-y": "hidden"
        //                  });

        //             $(this)
        //             .find('.image')
        //             .css({"display":"block"})
        //             .find(".inner")
        //             .find('img').attr("src", images_[index].elem);
        //          }
        //     }
        //     else{
        //         console.info("ocultado!!!");
        //             $(this)
        //             .find('.image')
        //             .css({"display":"none"})
        //             .find(".inner")
        //             .find('img').attr("src",'');
        //     }

          
                   
        //     })
        
        // Add css rule to slides css
        
        slidesCss.push(slideCss);
    });
    
    // Register sounds
    
    if (sounds.length) {
        createjs.Sound.registerSounds(sounds, '');
    }
    
    // Add fonts to page
    
    fonts = removeDuplicates(fonts);
    
    $.each(fonts, function (i, font) {

        $('head').append(FONT_URL.replace('{font}', font));
    });
    
    // Add custom css rules to page
    
    cssRules = cssRules.replace('{SLIDES}', slidesCss.join(''));
    $('head').append($(cssRules));
}