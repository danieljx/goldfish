<!DOCTYPE html>
<html>
<head>
	<script src="../dist/lib/jquery.min.js"></script>
	<script src="../dist/lib/underscore.min.js"></script>
	<script src="../node_modules/d3/build/d3.min.js"></script>
	<style>
	html {
		height: 100%;
	}

	body {
		height: 100%;
		background: linear-gradient(to bottom,#2d2d2d 0,#252525 100%);
    padding: 0;
    margin: 0;
    overflow: hidden;
	}

  svg {
    border: 1px solid #f07814;
    width: 100vw;
    height: 100vh;
    margin: 0;
    box-sizing: border-box;
  }

	.node {
    cursor: pointer;
  }

  .overlay{
      background-color:#EEE;
  }
   
  .node circle {
    fill: #252525;
    /*fill: none;*/
    fill: #101010;
    stroke: #f07814;
    stroke-width: 0.3vw;
    /*fill-rule: evenodd;*/
  }

  .node.selected circle {
    fill: #f07814;
    stroke: #101010;
  }
   
  .node text {
    font-size:10px; 
    font-family:sans-serif;
    fill: #ffffff;
  }
   
  .link {
    fill: none;
    stroke: #f07814;
    /*stroke: #101010;*/
    stroke-width: 0.3vw;
    stroke-dasharray: 1vw,1vw;
    opacity: 0.5;
  }

  .templink {
    fill: none;
    stroke: #f07814;
    /*stroke: #101010;*/
    stroke-width: 0.3vw;
    stroke-dasharray: 1vw,1vw;
    opacity: 0.5;
  }

  .node circle.ghostCircle.show{
      display:block;
  }

  .node.activeDrag circle {
    opacity: 0.2;
  }

  .node circle.ghostCircle, .node.activeDrag circle.ghostCircle{
       display: none;
       fill: #f07814;
       opacity: 0.5;
       stroke: none;
       stroke-width: 0;
  }

  nav {
  	position: absolute;
  	top: 1vw;
    left: 1vw;
  }

  nav button {
  	width: 2vw;
  	height: 2vw;
  	box-sizing: border-box;
  	border-radius: 2vw;
  	border: 0;
  	line-height: 2vw;
    outline: 0 none;
    padding: 0;
    background: #101010;;
    color: #f07814;
    border: 0.15vw solid #f07814;
    font-weight: bold;
  }

  .tooltip {
    position: absolute;
    z-index: 10;
    opacity: 0;
    transition: opacity 0.3s;
    background: transparent;
    color: #ffffff;
    font-family: sans-serif;
    text-shadow: 0px 0px 5px #000;
    font-size: 1.5vw;
    margin-top: -0.75vw;
  }

	</style>
</head>
<body>
	<nav>
    <button id="add-child" disabled="disabled">+</button> 
	<button id="remove" disabled="disabled">-</button>
  <button id="save-tree">S</button>
  <button id="move-left">&lt;</button>
  <button id="move-right">&gt;</button>
	</nav>
    <script>
    	// ### DATA MODEL START

// var data;

function returnSanitized(node){
  // console.log(node);
      var sanNode = {};
      sanNode.name = node.data.name;
      sanNode.order = node.data.order;
      sanNode.children = [];
      if(node.children){
      node.children.forEach(function(child, index, array){
      sanNode.children.push(returnSanitized(child));
      });
      }
      return sanNode;
    }

// ### DATA MODEL END
d3.json("tree.json", function(error, data) {
  if (error) throw error;

  //  assigns the data to a hierarchy using parent-child relationships

// Set the dimensions and margins of the diagram
var margin,
  width,
  height,
  maxDim = Math.min(width, height),
  circleR = maxDim / 40,
  panBoundary = 20,
  selectedNode = null,
  draggingNode = null;

var svg = d3.select("body").
	append("svg").
  append("g");

  var diagonal = d3.linkHorizontal()
    .x(function(d) { return d.y; })
    .y(function(d) { return d.x; });

var i = 0, duration = 300;
var treemap = d3.tree();

root = d3.hierarchy(data, function(d) {
    return d.children;
    });

update(root);

window.addEventListener("resize", function() { update(root); });

var selected = null;

var tooltip = d3.select("body")
    .append("div")
    .classed("tooltip", true);

function update(source) {

  margin = $('body').height()*0.05;
  height = $('body').height() - 2*margin;
  width = $('body').width() - 2*margin;
  maxDim = Math.max(width, height);
  circleR = height / 40;


  console.log(JSON.stringify(returnSanitized(source), null, "    "));

  root.sort(function(a, b) {
            return b.data.order < a.data.order ? 1 : -1;
        });

  root.x0 = circleR;
  root.y0 = width / 2;

  treemap.size([width, height]);
  
  svg.attr("transform", "translate(" + margin + "," + margin + ")");

  var maxDepth = 0;
  var treeData = treemap(root.sort());
  
  var nodes = treeData.descendants(),
  	links = treeData.descendants().slice(1);

  nodes.forEach(function(d){
    maxDepth = Math.max(maxDepth, d.depth);
  });

  nodes.forEach(function(d){
  	d.y = d.depth * height / maxDepth;
  });

  var link = svg.selectAll('path.link').
  	data(links, function(d) {
    	return d.id;
    });

    var linkEnter = link.enter().insert("path", "g")
            .attr("class", "link")
            .attr("d", function(d) {
       return "M" + d.x + "," + d.y
         + "C" + d.x + "," + (d.y + d.parent.y) / 2
         + " " + (d.x + d.parent.x) / 2 + "," + d.parent.y
         + " " + d.parent.x + "," + d.parent.y;
       });
    
  var linkUpdate = linkEnter.merge(link);
  
  linkUpdate.transition().
  	duration(duration).
    attr("d", function(d) {
       return "M" + d.x + "," + d.y
         + "C" + d.x + "," + (d.y + d.parent.y) / 2
         + " " + (d.x + d.parent.x) / 2 + "," + d.parent.y
         + " " + d.parent.x + "," + d.parent.y;
       });

  var linkExit = link.exit().
    remove();

  var node = svg.selectAll('g.node')
  	.data(nodes, function(d) {
    	return d.id || (d.id = ++i);
    });

  var nodeEnter = node.enter().
    append('g').
    attr('class', 'node').
    attr('id', function(d) { return 'node-'+d.id }).
    attr("transform", function(d) {
      return "translate(" + source.x0 + "," + source.y0 + ")";
    }).
    on('click', click);

    nodeEnter.on("mouseover", function(d){
      tooltip.
      text(d.data.name).
      style("top", d.y+margin+"px").
      style("left",d.x+margin+circleR*2+"px"); 

      return tooltip.style("opacity", 1);})
      .on("mouseout", function(){return tooltip.style("opacity", 0);})

  nodeEnter.append('circle').
    attr('r', circleR);

  nodeEnter.append("circle")
            .attr('class', 'ghostCircle')
            .attr("r", circleR*2)
            .attr("opacity", 0.2) // change this to zero to hide the target area
            .attr('pointer-events', 'mouseover')
            .on("mouseover", function(node) {
                overCircle(node);
            })
            .on("mouseout", function(node) {
                outCircle(node);
            });

  var nodeUpdate = nodeEnter.merge(node);

  nodeUpdate
  .transition()
  .duration(duration)
    .attr("transform", function(d) {
    	return "translate(" + d.x + "," + d.y + ")";
  	});

  nodeUpdate.select('circle.node').
  	attr('r', circleR).
    attr('cursor', 'pointer');

  var nodeExit = node.exit().
  	transition().
    duration(duration).
    attr("transform", function(d) {
    	return "translate(" + source.y + "," + source.x + ")";
    }).
    remove();

  nodeExit.select('circle').attr('r', 0);
  
  nodes.forEach(function(d){
    d.x0 = d.x;
    d.y0 = d.y;
  });

  function click(d) {
    selected = d;
    d3.selectAll('.node:not(#'+this.id+')').classed('selected', false);
    d3.select(this).classed('selected', true);
    document.getElementById('add-child').disabled = false;
    document.getElementById('remove').disabled = false;
    update(d);
  }

  function setNodeDepth(d) {
    d.depth = d == root ? 0 : d.parent.depth + 1;
    if (d.children) {
        d.children.forEach(setNodeDepth);
    } else if (d._children) {
        d._children.forEach(setNodeDepth);
    }
}


    function initiateDrag(d, domNode) {
        draggingNode = d;
        d3.select(domNode).select('.ghostCircle').attr('pointer-events', 'none');
        d3.selectAll('.ghostCircle').attr('class', 'ghostCircle show');
        d3.select(domNode).attr('class', 'node activeDrag');
        console.log(d3.select(domNode));

        // svg.selectAll(".node").sort(function(a, b) { // select the parent and sort the path's
        //     if (a.id != draggingNode.id) return 1; // a is not the hovered element, send "a" to the back
        //     else return -1; // a is the hovered element, bring "a" to the front
        // });
        console.log(dragNodes);
        console.log(dragNodes.length);
        // if nodes has children, remove the links and nodes
        if (dragNodes.length > 0) {

            // remove link paths
            links = dragNodes.slice(1);
            nodePaths = svg.selectAll("path.link")
                .data(links, function(d) {
                    return d.id;
                }).remove();
            // remove child nodes
            nodesExit = svg.selectAll("g.node")
                .data(dragNodes, function(d) {
                  console.log(d.id);
                    return d.id;
                })
                .filter(function(d, i) {
                    if (d.id == draggingNode.id) {
                        return false;
                    }
                    return true;
                })
                .remove();
        }

        parentLink = treemap(draggingNode.parent).descendants().slice(1);
        svg.selectAll('path.link').filter(function(d, i) {
            console.log(d);
            if (d.id == draggingNode.id) {
                return true;
            }
            return false;
        }).remove();

        dragStarted = null;
    }

  // Define the drag listeners for drag/drop behaviour of nodes.
    dragListener = d3.drag()
        .on("start", function(d) {
          console.log("dragstart");
          console.log(d);
            if (d == root) {
                return;
            }
            dragStarted = true;
            dragNodes = treemap(d).descendants();
            console.log(dragNodes);
            d3.event.sourceEvent.stopPropagation();
            // it's important that we suppress the mouseover event on the node being dragged. Otherwise it will absorb the mouseover event and the underlying node will not detect it d3.select(this).attr('pointer-events', 'none');
        })
        .on("drag", function(d) {
          console.log("drag");
            if (d == root) {
                return;
            }
            if (dragStarted) {
                domNode = this;
                initiateDrag(d, domNode);
            }


            d.x0 += d3.event.dx;
            d.y0 += d3.event.dy;
            var node = d3.select(this);
            // node.attr("transform", "translate(" + d.x0 + "," + d.y0 + ")");
            updateTempConnector();
        }).on("end", function(d) {
            if (d == root) {
                return;
            }
            domNode = this;
            if (selectedNode) {
                // now remove the element from the parent, and insert it into the new elements children
                var index = draggingNode.parent.children.indexOf(draggingNode);
                if (index > -1) {
                    draggingNode.parent.children.splice(index, 1);
                    // had to update the "draggingNode" parent "children" array if it's 
                    // empty otherwise the "firstWalk()" core function throws an error when
                    // trying to call "children[0]".
                    if (draggingNode.parent.children.length == 0) {
                        draggingNode.parent.children = null;
                    }
                }

                // update the "draggingNode" parent as well as update all its children 
                // so that their properties are properly adjusted to their new depths.
                draggingNode.parent = selectedNode;
                draggingNode.descendants().forEach(setNodeDepth);

                var maxOrder = 0;


                // check for visible children.
                if (selectedNode.children) {
                    selectedNode.children.push(draggingNode);
                    selectedNode.children.forEach( function(d) {
                      maxOrder = Math.max(maxOrder, d.data.order+1);
                      
                    });
                } else {
                    selectedNode.children = [];
                    selectedNode.children.push(draggingNode);
                }
                console.log(maxOrder);
                draggingNode.data.order = maxOrder;
                // Make sure that the node being added to is expanded so user can see added node is correctly moved
                // expand(selectedNode);
                // sortTree();
                endDrag();
            } else {
                endDrag();
            }
        });

    dragListener( d3.selectAll('.node') );

    function endDrag() {
        // console.log(selectedNode.children);
        selectedNode = null;
        d3.selectAll('.ghostCircle').attr('class', 'ghostCircle');
        d3.select(domNode).attr('class', 'node');
        // now restore the mouseover event or we won't be able to drag a 2nd time
        d3.select(domNode).select('.ghostCircle').attr('pointer-events', '');

        updateTempConnector();
        if (draggingNode !== null) {
            update(root);
            // centerNode(draggingNode);
            draggingNode = null;
        }
    }

    var updateTempConnector = function() {
        var data = [];
        if (draggingNode !== null && selectedNode !== null) {
            // have to flip the source coordinates since we did this for the existing connectors on the original tree
            // data = [{
            //     source: {
            //         x: selectedNode.x0,
            //         y: selectedNode.y0
            //     },
            //     target: {
            //         x: draggingNode.x0,
            //         y: draggingNode.y0
            //     }
            // }];
        }
       //  var link = svg.selectAll(".templink").data(data);

       //  link.enter().append("path")
       //      .attr("class", "templink")
       //      .attr("d", function(d) {
       //        console.log(d);
       // return "M" + d.source.x + "," + d.source.y
       //   + "C" + (d.source.x + d.target.x) / 2 + "," + d.source.y
       //   + " " + (d.source.x + d.target.x) / 2 + "," + d.target.y
       //   + " " + d.target.x + "," + d.target.y;
       // })
       //      .attr('pointer-events', 'none');

       //  link.attr("d", function(d) {
       // return "M" + d.source.x + "," + d.source.y
       //   + "C" + (d.source.x + d.target.x) / 2 + "," + d.source.y
       //   + " " + (d.source.x + d.target.x) / 2 + "," + d.target.y
       //   + " " + d.target.x + "," + d.target.y;
       // });

       //  link.exit().remove();
    };

    var overCircle = function(d) {
        selectedNode = d;
        updateTempConnector();
    };
    var outCircle = function(d) {
        selectedNode = null;
        updateTempConnector();
    };

    function collapse(d) {
        if (d.children) {
            d._children = d.children;
            d._children.forEach(collapse);
            d.children = null;
        }
    }

    function expand(d) {
        if (d._children) {
            d.children = d._children;
            d.children.forEach(expand);
            d._children = null;
        }
    }

    function sortTree() {
        tree.sort(function(a, b) {
            return b.data.order < a.data.order ? 1 : -1;
        });
    }

    
}

document.getElementById('add-child').onclick = function() {
  console.log(selected);
  //creates New OBJECT
  var newNodeObj = {
    name: new Date().getTime(),
    order: 0,
    children: []
  };
  //Creates new Node 
  var newNode = d3.hierarchy(newNodeObj);
  newNode.depth = selected.depth + 1; 
  newNode.height = selected.height - 1;
  newNode.parent = selected; 
  newNode.id = Date.now();
  
  if(!selected.children){
    selected.children = [];
    selected.data.children = [];
  }
  selected.children.push(newNode);
  selected.data.children.push(newNode.data);
  
  update(selected);
};

document.getElementById('remove').onclick = function() {
  console.log(selected);
  var children = [];
  var parent = selected.parent;
     //iterate through the children 
     selected.parent.children.forEach(function(child){
       if (child.id != selected.id){
         //add to the child list if target id is not same 
         //so that the node target is removed.
         children.push(child);
       }
     });
     console.log(children);
     //set the target parent with new set of children sans the one which is removed
    selected.parent.children = children.length > 0 ? children : null;
  update(selected.parent);
};

document.getElementById('move-left').onclick = function() {
  console.log(selected);
  var children = [];
  var parent = selected.parent;
     //iterate through the children 
     var leftSibling = false;
     selected.parent.children.forEach(function(child){
       if (leftSibling && (child.id == selected.id)){
         leftOrder = leftSibling.data.order;
         leftSibling.data.order = child.data.order;
         child.data.order = leftOrder;
       }
       leftSibling = child;
       // children.push(child);
     });
     console.log(children);
     //set the target parent with new set of children sans the one which is removed
    // selected.parent.children = children.length > 0 ? children : null;
  update(selected.parent);
};

document.getElementById('move-right').onclick = function() {
  console.log(selected);
  var children = [];
  var parent = selected.parent;
     //iterate through the children 
     var selectedChild = false;
     selected.parent.children.forEach(function(child){
      if (selectedChild) {
        leftOrder = selectedChild.data.order;
        selectedChild.data.order = child.data.order;
        child.data.order = leftOrder;
        selectedChild = false;
       }
       if (child.id == selected.id){
        selectedChild = child;
       }

       // children.push(child);
     });
     console.log(children);
     //set the target parent with new set of children sans the one which is removed
    // selected.parent.children = children.length > 0 ? children : null;
  update(selected.parent);
};

document.getElementById('save-tree').onclick = function() {
  console.log(JSON.stringify(returnSanitized(root)));
};
});


    </script>
</body>
</html>