<!DOCTYPE html>
<html>
<head>
	<script src="../dist/lib/jquery.min.js"></script>
	<script src="../dist/lib/underscore.min.js"></script>
	<script src="../node_modules/d3/build/d3.min.js"></script>
	<style>
	html {
		height: 100%;
	}

	body {
		height: 100%;
		background: linear-gradient(to bottom,#2d2d2d 0,#252525 100%);
	}

	.node {
    cursor: pointer;
  }

  .overlay{
      background-color:#EEE;
  }
   
  .node circle {
    fill: #252525;
    stroke: #f07814;
    stroke-width: 0.5vw;
  }
   
  .node text {
    font-size:10px; 
    font-family:sans-serif;
  }
   
  .link {
    fill: none;
    stroke: #f07814;
    stroke-width: 0.5vw;
  }

  .ghostCircle.show{
      display:block;
  }

  .ghostCircle, .activeDrag .ghostCircle{
       display: none;
  }

  nav {
  	position: absolute;
  	top: 10px;
  }

  nav button {
  	width: 20px;
  	height: 20px;
  	box-sizing: border-box;
  	border-radius: 20px;
  	border: 0;
  	line-height: 20px;
  }

	</style>
</head>
<body>
	<nav>
    <button id="add-child" disabled="disabled">+</button> 
	<button id="remove" style="line-height: 0px" disabled="disabled">-</button>
	</nav>
    <script>
    	// ### DATA MODEL START

var data = {
  name: '1',
  children: [{
  	name: '2',
    children: [{
  		name: '3',
      children: [{
  			name: '4',
        children: []
      }]
    }, {
  		name: '5',
      children: []
    }]
  }]
};

// ### DATA MODEL END

// Set the dimensions and margins of the diagram
var margin = {top: 20, right: 90, bottom: 30, left: 90},
	width = $('body').width() - margin.left - margin.right,
  height = $('body').height() - margin.top - margin.bottom,
  maxDim = Math.min(width, height);

// append the svg object to the body of the page
// appends a 'group' element to 'svg'
// moves the 'group' element to the top left margin
var svg = d3.select("body").
	append("svg").
  attr("width", width + margin.right + margin.left).
  attr("height", height + margin.top + margin.bottom).
  append("g").
  attr("transform", "translate(" + margin.left + "," + margin.top + ")");

  var diagonal = d3.linkHorizontal()
    .x(function(d) { return d.y; })
    .y(function(d) { return d.x; });

var i = 0, duration = 300, root;

// declares a tree layout and assigns the size
var treemap = d3.tree().size([height, width]);

// Assigns parent, children, height, depth
root = d3.hierarchy(data, function(d) {
	return d.children;
});
root.x0 = height / 2;
root.y0 = 0;

update(root);

var selected = null;

function update(source) {

  // Assigns the x and y position for the nodes
  var treeData = treemap(root);

  // Compute the new tree layout.
  var nodes = treeData.descendants(),
  	links = treeData.descendants().slice(1);
    console.log(links);

  // Normalize for fixed-depth.
  nodes.forEach(function(d){
  	d.y = d.depth * 180
  });

  // ### LINKS

  // Update the links...
  var link = svg.selectAll('path.link').
  	data(links, function(d) {
    	return d.id;
    });

    // Enter any new links at the parent's previous position.
        // link.enter().insert("path", "g")
        //     .attr("class", "link")
        //     .attr("d", function(d) {
        //         var o = {
        //             x: source.x0,
        //             y: source.y0
        //         };
        //         return diagonal({
        //             source: o,
        //             target: o
        //         });
        //     });

        // // Transition links to their new position.
        // link.transition()
        //     .duration(duration)
        //     .attr("d", diagonal);

  // Enter any new links at the parent's previous position.
  // var linkEnter = link.enter().
  // 	append('line').
  //   attr("class", "link").
  //   attr("stroke-width", 2).
  //   attr("stroke", 'black').
  //   attr('x1', function(d) {
  //   	return source.y0;
  //   }).
  //   attr('y1', function(d) {
  //   	return source.x0;
  //   }).
  //   attr('x2', function(d) {
  //   	return source.y0;
  //   }).
  //   attr('y2', function(d) {
  //   	return source.x0;
  //   });

    // link.transition()
    //         .duration(duration)
    //         .attr("d", diagonal);

    var linkEnter = link.enter().insert("path", "g")
            .attr("class", "link")
            .attr("d", function(d) {
       return "M" + d.y + "," + d.x
         + "C" + (d.y + d.parent.y) / 2 + "," + d.x
         + " " + (d.y + d.parent.y) / 2 + "," + d.parent.x
         + " " + d.parent.y + "," + d.parent.x;
       });


    
  var linkUpdate = linkEnter.merge(link);
  
  linkUpdate.transition().
  	duration(duration).
    attr("d", function(d) {
       return "M" + d.y + "," + d.x
         + "C" + (d.y + d.parent.y) / 2 + "," + d.x
         + " " + (d.y + d.parent.y) / 2 + "," + d.parent.x
         + " " + d.parent.y + "," + d.parent.x;
       });

  // Transition back to the parent element position
  // linkUpdate.transition().
  // 	duration(duration).
  //   attr('x1', function(d) {
  //   	return d.parent.y;
  //   }).
  //   attr('y1', function(d) {
  //   	return d.parent.x;
  //   }).
  //   attr('x2', function(d) {
  //   	return d.y;
  //   }).
  //   attr('y2', function(d) {
  //   	return d.x;
  //   });

  // Remove any exiting links
  var linkExit = link.exit().
    remove();

	// ### CIRCLES

  // Update the nodes...
  var node = svg.selectAll('g.node')
  	.data(nodes, function(d) {
    	return d.id || (d.id = ++i);
    });

  // Enter any new modes at the parent's previous position.
  var nodeEnter = node.enter().
    append('g').
    attr('class', 'node').
    attr("transform", function(d) {
      return "translate(" + source.y0 + "," + source.x0 + ")";
    }).
    on('click', click);

  // Add Circle for the nodes
  nodeEnter.append('circle').
  	attr('class', 'node').
    attr('r', maxDim / 20);

  // Update
  var nodeUpdate = nodeEnter.merge(node);

  // Transition to the proper position for the node
  nodeUpdate.transition().
  	duration(duration).
    attr("transform", function(d) {
    	return "translate(" + d.y + "," + d.x + ")";
  	});

  // Update the node attributes and style
  nodeUpdate.select('circle.node').
  	attr('r', maxDim / 20).
    attr('cursor', 'pointer');

  // Remove any exiting nodes
  var nodeExit = node.exit().
  	transition().
    duration(duration).
    attr("transform", function(d) {
    	return "translate(" + source.y + "," + source.x + ")";
    }).
    remove();

  // On exit reduce the node circles size to 0
  nodeExit.select('circle').attr('r', 0);
  
  // Store the old positions for transition.
  nodes.forEach(function(d){
    d.x0 = d.x;
    d.y0 = d.y;
  });

  // Toggle children on click.
  function click(d) {
    selected = d;
    document.getElementById('add-child').disabled = false;
    document.getElementById('remove').disabled = false;
    update(d);
  }
}

document.getElementById('add-child').onclick = function() {
  console.log(selected);
  //creates New OBJECT
  var newNodeObj = {
    name: new Date().getTime(),
    children: []
  };
  //Creates new Node 
  var newNode = d3.hierarchy(newNodeObj);
  newNode.depth = selected.depth + 1; 
  newNode.height = selected.height - 1;
  newNode.parent = selected; 
  newNode.id = Date.now();
  
  if(!selected.children){
    selected.children = [];
  	selected.data.children = [];
  }
  selected.children.push(newNode);
  selected.data.children.push(newNode.data);
  
  update(selected);
};

document.getElementById('remove').onclick = function() {
	console.log(selected);
	var children = [];
	var parent = selected.parent;
     //iterate through the children 
     selected.parent.children.forEach(function(child){
       if (child.id != selected.id){
         //add to the child list if target id is not same 
         //so that the node target is removed.
         children.push(child);
       }
     });
     console.log(children);
     //set the target parent with new set of children sans the one which is removed
    selected.parent.children = children.length > 0 ? children : null;
	update(selected.parent);
};
    </script>
</body>
</html>