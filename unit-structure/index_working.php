<!DOCTYPE html>
<html>
<head>
	<script src="../dist/lib/jquery.min.js"></script>
	<script src="../dist/lib/underscore.min.js"></script>
	<script src="../node_modules/d3/build/d3.min.js"></script>
	<style>
	html {
		height: 100%;
	}

	body {
		height: 100%;
		background: linear-gradient(to bottom,#2d2d2d 0,#252525 100%);
    padding: 0;
    margin: 0;
    overflow: hidden;
	}

  svg {
    border: 1px solid #f07814;
    width: 100vw;
    height: 100vh;
    margin: 0;
    box-sizing: border-box;
  }

	.node {
    cursor: pointer;
  }

  .overlay{
      background-color:#EEE;
  }
   
  .node circle {
    fill: #252525;
    /*fill: none;*/
    fill: #101010;
    stroke: #f07814;
    stroke-width: 0.3vw;
    /*fill-rule: evenodd;*/
  }

  .node.selected circle {
    fill: #f07814;
    stroke: #101010;
  }
   
  .node text {
    font-size:10px; 
    font-family:sans-serif;
    fill: #ffffff;
  }
   
  .link {
    fill: none;
    stroke: #f07814;
    /*stroke: #101010;*/
    stroke-width: 0.3vw;
    stroke-dasharray: 1vw,1vw;
    opacity: 0.5;
  }

  .templink {
    fill: none;
    stroke: #f07814;
    /*stroke: #101010;*/
    stroke-width: 0.3vw;
    stroke-dasharray: 1vw,1vw;
    opacity: 0.5;
  }

  .node circle.ghostCircle.show{
      display:block;
  }

  .node.activeDrag circle {
    opacity: 0.2;
  }

  .node circle.ghostCircle, .node.activeDrag circle.ghostCircle{
       display: none;
       fill: #f07814;
       opacity: 0.5;
       stroke: none;
       stroke-width: 0;
  }

  nav {
  	position: absolute;
  	top: 10px;
  }

  nav button {
  	width: 20px;
  	height: 20px;
  	box-sizing: border-box;
  	border-radius: 20px;
  	border: 0;
  	line-height: 20px;
    outline: 0 none;
  }

  .tooltip {
    position: absolute;
    z-index: 10;
    opacity: 0;
    transition: opacity 0.3s;
    background: transparent;
    color: #ffffff;
    font-family: sans-serif;
    text-shadow: 0px 0px 5px #000;
    font-size: 1.5vw;
    margin-top: -0.75vw;
  }

	</style>
</head>
<body>
	<nav>
    <button id="add-child" disabled="disabled">+</button> 
	<button id="remove" style="line-height: 0px" disabled="disabled">-</button>
  <button id="save-tree">S</button>
	</nav>
    <script>
    	// ### DATA MODEL START

var data = {
  name: 'Oden',
  children: [{
  	name: 'Njord',
    children: [{
  		name: 'Heimdall',
      children: [{
  			name: 'Tor',
        children: []
      }]
    }, {
  		name: 'Tyr',
      children: []
    }]
  }]
};

// ### DATA MODEL END

// Set the dimensions and margins of the diagram
var margin = Math.min($('body').width(), $('body').height())*0.05,
	width = $('body').width() - 2*margin,
  height = $('body').height() - 2*margin,
  maxDim = Math.min(width, height),
  circleR = maxDim / 40,
  panBoundary = 20,
  selectedNode = null,
  draggingNode = null;

// append the svg object to the body of the page
// appends a 'group' element to 'svg'
// moves the 'group' element to the top left margin
var svg = d3.select("body").
	append("svg").
  // attr("width", $('body').width()).
  // attr("height", $('body').height()).
  append("g").
  attr("transform", "translate(" + margin + "," + margin + ")");

  var diagonal = d3.linkHorizontal()
    .x(function(d) { return d.y; })
    .y(function(d) { return d.x; });

var i = 0, duration = 300, root;

// declares a tree layout and assigns the size
var treemap = d3.tree().size([width, height]);

// Assigns parent, children, height, depth
root = d3.hierarchy(data, function(d) {
	return d.children;
});

root.x0 = circleR;
root.y0 = width / 2;

update(root);

window.addEventListener("resize", update);

var selected = null;

var tooltip = d3.select("body")
    .append("div")
    .classed("tooltip", true);

function returnSanitized(node){
      var sanNode = {};
      sanNode.name = node.data.name;
      sanNode.children = [];
      if(node.children){
      node.children.forEach(function(child, index, array){
      sanNode.children.push(returnSanitized(child));
      });
      }
      return sanNode;
    }

function update(source) {

  margin = $('body').height()*0.05;
  height = $('body').height() - 2*margin;
  width = $('body').width() - 2*margin;
  
  
  maxDim = Math.max(width, height);
  circleR = height / 40;

  svg.attr("transform", "translate(" + margin + "," + margin + ")");

  

  var maxDepth = 0;

  

  // Assigns the x and y position for the nodes
  var treeData = treemap(root);
  treemap.size([width, height]);

  // Compute the new tree layout.
  var nodes = treeData.descendants(),
  	links = treeData.descendants().slice(1);
    // console.log(links);

  // Normalize for fixed-depth.
  nodes.forEach(function(d){
    maxDepth = Math.max(maxDepth, d.depth);
  });

  nodes.forEach(function(d){
  	d.y = d.depth * height / maxDepth;
  });

  // ### LINKS

  // Update the links...
  var link = svg.selectAll('path.link').
  	data(links, function(d) {
    	return d.id;
    });

    var linkEnter = link.enter().insert("path", "g")
            .attr("class", "link")
            .attr("d", function(d) {
       return "M" + d.x + "," + d.y
         + "C" + (d.x + d.parent.x) / 2 + "," + d.y
         + " " + (d.x + d.parent.x) / 2 + "," + d.parent.y
         + " " + d.parent.x + "," + d.parent.y;
       });


    
  var linkUpdate = linkEnter.merge(link);
  
  linkUpdate.transition().
  	duration(duration).
    // attr("d", function(d) {
    //    return "M" + d.x + "," + d.y
    //      + "C" + (d.x + d.parent.x) / 2 + "," + d.y
    //      + " " + (d.x + d.parent.x) / 2 + "," + d.parent.y
    //      + " " + d.parent.x + "," + d.parent.y;
    //    });
    attr("d", function(d) {
       return "M" + d.x + "," + d.y
         + "C" + d.x + "," + (d.y + d.parent.y) / 2
         + " " + (d.x + d.parent.x) / 2 + "," + d.parent.y
         + " " + d.parent.x + "," + d.parent.y;
       });

  // Remove any exiting links
  var linkExit = link.exit().
    remove();

	// ### CIRCLES

  // Update the nodes...
  var node = svg.selectAll('g.node')
  	.data(nodes, function(d) {
    	return d.id || (d.id = ++i);
    });

  // Enter any new modes at the parent's previous position.
  var nodeEnter = node.enter().
    append('g').
    attr('class', 'node').
    attr('id', function(d) { return 'node-'+d.id }).
    // attr("transform", function(d) {
    //   return "translate(" + source.x0 + "," + source.y0 + ")";
    // }).
    on('click', click);

    nodeEnter.on("mouseover", function(d){
      tooltip.
      text(d.data.name).
      style("top", d.y+margin+"px").
      style("left",d.x+margin+circleR*2+"px"); 

      return tooltip.style("opacity", 1);})
      // .on("mousemove", function(){return tooltip.style("top", (d3.event.pageY-10)+"px").style("left",(d3.event.pageX+10)+"px");})
      .on("mouseout", function(){return tooltip.style("opacity", 0);})

  // Add Circle for the nodes
  nodeEnter.append('circle').
  	// attr('class', 'node').
    attr('r', circleR);

  nodeEnter.append("circle")
            .attr('class', 'ghostCircle')
            .attr("r", circleR*2)
            .attr("opacity", 0.2) // change this to zero to hide the target area
        // .style("fill", "red")
            .attr('pointer-events', 'mouseover')
            .on("mouseover", function(node) {
                overCircle(node);
            })
            .on("mouseout", function(node) {
                outCircle(node);
            });

  // nodeEnter.append("text")
  //     // .attr("text-anchor", "middle")
  //     .attr("x", 12)
  //     .attr("y", ".35em")
  //     .text(function(d) { console.log(d.data); return d.data.name });

  // Update
  var nodeUpdate = nodeEnter.merge(node);

  // Transition to the proper position for the node
  nodeUpdate
  // .transition()
  // .duration(duration)
    .attr("transform", function(d) {
    	return "translate(" + d.x + "," + d.y + ")";
  	});

  // Update the node attributes and style
  nodeUpdate.select('circle.node').
  	attr('r', circleR).
    attr('cursor', 'pointer');

  // Remove any exiting nodes
  var nodeExit = node.exit().
  	transition().
    duration(duration).
    attr("transform", function(d) {
    	return "translate(" + source.y + "," + source.x + ")";
    }).
    remove();

  // On exit reduce the node circles size to 0
  nodeExit.select('circle').attr('r', 0);
  
  // Store the old positions for transition.
  nodes.forEach(function(d){
    d.x0 = d.x;
    d.y0 = d.y;
  });

  function click(d) {
    selected = d;
    console.log(d);
    console.log(this);
    d3.selectAll('.node:not(#'+this.id+')').classed('selected', false);
    d3.select(this).classed('selected', true);
    document.getElementById('add-child').disabled = false;
    document.getElementById('remove').disabled = false;
    update(d);
  }

  function setNodeDepth(d) {
    d.depth = d == root ? 0 : d.parent.depth + 1;
    if (d.children) {
        d.children.forEach(setNodeDepth);
    } else if (d._children) {
        d._children.forEach(setNodeDepth);
    }
}


    function initiateDrag(d, domNode) {
        draggingNode = d;
        d3.select(domNode).select('.ghostCircle').attr('pointer-events', 'none');
        d3.selectAll('.ghostCircle').attr('class', 'ghostCircle show');
        d3.select(domNode).attr('class', 'node activeDrag');
        console.log(d3.select(domNode));

        // svg.selectAll(".node").sort(function(a, b) { // select the parent and sort the path's
        //     if (a.id != draggingNode.id) return 1; // a is not the hovered element, send "a" to the back
        //     else return -1; // a is the hovered element, bring "a" to the front
        // });
        console.log(dragNodes);
        console.log(dragNodes.length);
        // if nodes has children, remove the links and nodes
        if (dragNodes.length > 0) {

            // remove link paths
            links = dragNodes.slice(1);
            nodePaths = svg.selectAll("path.link")
                .data(links, function(d) {
                    return d.id;
                }).remove();
            // remove child nodes
            nodesExit = svg.selectAll("g.node")
                .data(dragNodes, function(d) {
                  console.log(d.id);
                    return d.id;
                })
                .filter(function(d, i) {
                    if (d.id == draggingNode.id) {
                        return false;
                    }
                    return true;
                })
                .remove();
        }

        // remove parent link
        // parentLink = tree.links(tree.nodes(draggingNode.parent));
        // var treeData = treemap(root);
        parentLink = treemap(draggingNode.parent).descendants().slice(1);
        svg.selectAll('path.link').filter(function(d, i) {
            console.log(d);
            if (d.id == draggingNode.id) {
                return true;
            }
            return false;
        }).remove();

        dragStarted = null;
    }

  // Define the drag listeners for drag/drop behaviour of nodes.
    dragListener = d3.drag()
        .on("start", function(d) {
          console.log("dragstart");
          console.log(d);
            if (d == root) {
                return;
            }
            dragStarted = true;
            dragNodes = treemap(d).descendants();
            console.log(dragNodes);
            d3.event.sourceEvent.stopPropagation();
            // it's important that we suppress the mouseover event on the node being dragged. Otherwise it will absorb the mouseover event and the underlying node will not detect it d3.select(this).attr('pointer-events', 'none');
        })
        .on("drag", function(d) {
          console.log("drag");
            if (d == root) {
                return;
            }
            if (dragStarted) {
                domNode = this;
                initiateDrag(d, domNode);
            }


            d.x0 += d3.event.dx;
            d.y0 += d3.event.dy;
            var node = d3.select(this);
            // node.attr("transform", "translate(" + d.x0 + "," + d.y0 + ")");
            updateTempConnector();
        }).on("end", function(d) {
            if (d == root) {
                return;
            }
            domNode = this;
            if (selectedNode) {
                // now remove the element from the parent, and insert it into the new elements children
                var index = draggingNode.parent.children.indexOf(draggingNode);
                if (index > -1) {
                    draggingNode.parent.children.splice(index, 1);
                    // had to update the "draggingNode" parent "children" array if it's 
                    // empty otherwise the "firstWalk()" core function throws an error when
                    // trying to call "children[0]".
                    if (draggingNode.parent.children.length == 0) {
                        draggingNode.parent.children = null;
                    }
                }

                // update the "draggingNode" parent as well as update all its children 
                // so that their properties are properly adjusted to their new depths.
                draggingNode.parent = selectedNode;
                draggingNode.descendants().forEach(setNodeDepth);

                // check for visible children.
                if (selectedNode.children) {
                    selectedNode.children.push(draggingNode);
                // check for hidden children.
                } else if (selectedNode._children) {
                    selectedNode._children.push(draggingNode);
                // no children exist, create new children array and add "draggingNode".
                } else {
                    selectedNode.children = [];
                    selectedNode.children.push(draggingNode);
                }
                // Make sure that the node being added to is expanded so user can see added node is correctly moved
                // expand(selectedNode);
                // sortTree();
                endDrag();
            } else {
                endDrag();
            }
        });

    dragListener( d3.selectAll('.node') );

    function endDrag() {
        selectedNode = null;
        d3.selectAll('.ghostCircle').attr('class', 'ghostCircle');
        d3.select(domNode).attr('class', 'node');
        // now restore the mouseover event or we won't be able to drag a 2nd time
        d3.select(domNode).select('.ghostCircle').attr('pointer-events', '');
        updateTempConnector();
        if (draggingNode !== null) {
            update(root);
            // centerNode(draggingNode);
            draggingNode = null;
        }
    }

    var updateTempConnector = function() {
        var data = [];
        if (draggingNode !== null && selectedNode !== null) {
            // have to flip the source coordinates since we did this for the existing connectors on the original tree
            // data = [{
            //     source: {
            //         x: selectedNode.x0,
            //         y: selectedNode.y0
            //     },
            //     target: {
            //         x: draggingNode.x0,
            //         y: draggingNode.y0
            //     }
            // }];
        }
       //  var link = svg.selectAll(".templink").data(data);

       //  link.enter().append("path")
       //      .attr("class", "templink")
       //      .attr("d", function(d) {
       //        console.log(d);
       // return "M" + d.source.x + "," + d.source.y
       //   + "C" + (d.source.x + d.target.x) / 2 + "," + d.source.y
       //   + " " + (d.source.x + d.target.x) / 2 + "," + d.target.y
       //   + " " + d.target.x + "," + d.target.y;
       // })
       //      .attr('pointer-events', 'none');

       //  link.attr("d", function(d) {
       // return "M" + d.source.x + "," + d.source.y
       //   + "C" + (d.source.x + d.target.x) / 2 + "," + d.source.y
       //   + " " + (d.source.x + d.target.x) / 2 + "," + d.target.y
       //   + " " + d.target.x + "," + d.target.y;
       // });

       //  link.exit().remove();
    };

    var overCircle = function(d) {
        selectedNode = d;
        updateTempConnector();
    };
    var outCircle = function(d) {
        selectedNode = null;
        updateTempConnector();
    };

    function collapse(d) {
        if (d.children) {
            d._children = d.children;
            d._children.forEach(collapse);
            d.children = null;
        }
    }

    function expand(d) {
        if (d._children) {
            d.children = d._children;
            d.children.forEach(expand);
            d._children = null;
        }
    }

    function sortTree() {
        tree.sort(function(a, b) {
            return b.name.toLowerCase() < a.name.toLowerCase() ? 1 : -1;
        });
    }

    
}

document.getElementById('add-child').onclick = function() {
  console.log(selected);
  //creates New OBJECT
  var newNodeObj = {
    name: new Date().getTime(),
    children: []
  };
  //Creates new Node 
  var newNode = d3.hierarchy(newNodeObj);
  newNode.depth = selected.depth + 1; 
  newNode.height = selected.height - 1;
  newNode.parent = selected; 
  newNode.id = Date.now();
  
  if(!selected.children){
    selected.children = [];
  	selected.data.children = [];
  }
  selected.children.push(newNode);
  selected.data.children.push(newNode.data);
  
  update(selected);
};

document.getElementById('remove').onclick = function() {
	console.log(selected);
	var children = [];
	var parent = selected.parent;
     //iterate through the children 
     selected.parent.children.forEach(function(child){
       if (child.id != selected.id){
         //add to the child list if target id is not same 
         //so that the node target is removed.
         children.push(child);
       }
     });
     console.log(children);
     //set the target parent with new set of children sans the one which is removed
    selected.parent.children = children.length > 0 ? children : null;
	update(selected.parent);
};

document.getElementById('save-tree').onclick = function() {
  console.log(JSON.stringify(returnSanitized(root)));
  
};
    </script>
</body>
</html>