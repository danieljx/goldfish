<?php
// echo('http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'].'?'.$_SERVER['QUERY_STRING']);
// die();

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';

/*
/user/:id 		GET 	Get user info + unit ids 
/unit/:id 		GET 	Get unit info + page ids
/page/:id 		GET 	Get page info + block ids 
*/
session_start();

// <HACK> Mats Fjellner 2016.11.04 Slim usa REQUEST_URI que contiene URL pre-rewrite
$_SERVER['REQUEST_URI'] = str_replace('/api/', '/slim-api/public/', $_SERVER['REQUEST_URI']);
// </HACK>


// Instantiate the app
$settings = require __DIR__ . '/../src/settings.php';
$app = new \Slim\App($settings);

// $app['notFoundHandler'] = function ($app) {
//     return function ($request, $response) use ($app) {
//         return $app['response']->withStatus(404)->withHeader('Content-Type', 'text/html')->write('Page not found');
//     };
// };

// Set up dependencies
require __DIR__ . '/../src/dependencies.php';



$app->get('/auth/logout', function (Request $request, Response $response) {
	session_unset();
	$json = ['result' => 'ok'];
	$newResponse = $response->withJson($json);
    return $newResponse;
});

$app->get('/user/{user-id:[0-9]+}/project/{id:[0-9]+}', function (Request $request, Response $response) {
    $id = $request->getAttribute('id');
    // $json = array('id' => $id, 'name' => 'Mats Fjellner');

    $sth = $this->db->prepare("SELECT project.id, project.title, project.remote_auth, fk_role AS role, project.description, unit.id AS unit_id, unit.title AS unit_title FROM gf_projects AS project LEFT JOIN gf_project_users AS rel ON project.id = rel.fk_project_id LEFT JOIN gf_user_units AS rel2 ON rel.fk_user_id = rel2.fk_user_id LEFT JOIN gf_units AS unit ON rel2.fk_unit_id = unit.id WHERE project.id=:id AND rel.fk_user_id =:user_id");
    $sth->bindParam("id", $id);
    $sth->bindParam("user_id", $id);

	$sth->execute();
    $items = $sth->fetchALL(PDO::FETCH_ASSOC);

    $json = array();

    foreach($items as $item) {
    	if (!isset($json[$item['id']])) {

	      $json[$item['id']] = array(
	          'id' => $item['id'],
	          'title' => $item['title'],
	          'remote_auth' => $item['remote_auth'],
	          'description' => $item['description'],
	          'roles' => array($item['role']),
	          'units' => array()
	      );
	  }

	   if (!in_array($item['role'], $json[$item['id']]['roles'])) {
	   		$json[$item['id']]['roles'][] = $item['role'];
	   }

	   unset($item['id']);
	   unset($item['title']);
	   unset($item['remote_auth']);
	   unset($item['description']);

	   

	   unset($item['role']);

	   $item['id'] = $item['unit_id'];
	   unset($item['unit_id']);

	   if (!isset($json[$id]['units'][$item['id']])) {
	   		$item['units'] = array($item['id']);
	   		$json[$id]['units'][$item['id']] = $item['unit_title'];
	   } 
	   
	}

	if (count($json[$id]['units']) > 0) {
		$units = [];
		foreach ($json[$id]['units'] as $key => $value) {
			$units[] = array('id' => $key, 'title' => $value);
		}
		$json[$id]['units'] = $units;
	}
    $newResponse = $response->withJson($json[$id]);
    return $newResponse;
});

$app->put('/user/config/{id:[0-9]+}', function(Request $request, Response $response) {
	$id = $request->getAttribute('id');

	$request = (array) json_decode($request->getBody());

	$sth = $this->db->prepare("UPDATE gf_user_config SET editor=:editor, theme=:theme, effects=:effects WHERE id=:id");
	$sth->bindParam("id", $id);
	$sth->bindParam("editor", $request['editor']);
	$sth->bindParam("theme", $request['theme']);
	$sth->bindParam("effects", $request['effects']);
	$sth->execute();
	// $request = (array) json_decode(request()->getBody());

	$json = array('method' => 'put', 'editor' => $request['editor']);
	$newResponse = $response->withJson($request);
    return $newResponse;
	
	
});

$app->get('/user/{id:[0-9]+}', function (Request $request, Response $response) {
    $id = $request->getAttribute('id');
    // $json = array('id' => $id, 'name' => 'Mats Fjellner');

    $sth = $this->db->prepare("SELECT user.id, user.names, user.surnames, user.profile, project.id AS project_id, title, remote_auth, fk_role AS role, description FROM gf_users AS user LEFT JOIN gf_project_users AS rel ON user.id = rel.fk_user_id LEFT JOIN gf_projects AS project ON rel.fk_project_id = project.id WHERE user.id=:id");
    $sth->bindParam("id", $id);
	$sth->execute();
    $items = $sth->fetchALL(PDO::FETCH_ASSOC);

    $sth = $this->db->prepare("SELECT theme, effects, id FROM gf_user_config WHERE fk_user_id = :id");
    $sth->bindParam("id", $id);
	$sth->execute();
	$config = $sth->fetchALL(PDO::FETCH_ASSOC);	

    $json = array();

    foreach($items as $item) {
    	if (!isset($json[$item['id']])) {

		      $json[$item['id']] = array(
		          'id' => $item['id'],
		          'names' => $item['names'],
		          'surnames' => $item['surnames'],
		          'profile' => $item['profile'],
		          'projects' => array(),
		          'config' => array()
		      );

		      if (count($config) > 0) {
		      	$json[$item['id']]['config'] = $config[0];
		      }

	      }
	   unset($item['id']);
	   unset($item['names']);
	   unset($item['surnames']);
	   unset($item['profile']);
	   // if (!isset($json[$item['id']])) {

	   $item['id'] = $item['project_id'];

	   unset($item['project_id']);
	   if (!isset($json[$id]['projects'][$item['id']])) {
	   		$item['roles'] = array($item['role']);
	   		unset($item['role']);
	   		$json[$id]['projects'][$item['id']] = $item;
	   } else {
	   		$json[$id]['projects'][$item['id']]['roles'][] = $item['role'];
	   		unset($item['role']);
	   }
	   
	}
	$projects = [];
	foreach ($json[$id]['projects'] as $key => $value) {
		$value['id'] = $key;
		$projects[] = $value;
	}
	$json[$id]['projects'] = $projects;

    $newResponse = $response->withJson($json[$id]);
    return $newResponse;
});



$app->get('/auth/{email}/{password}', function (Request $request, Response $response) {
    $email = $request->getAttribute('email');
    $password = $request->getAttribute('password');

     $sth = $this->db->prepare("SELECT user.id FROM gf_users AS user WHERE user.email=:email AND user.password=:password LIMIT 1");
    $sth->bindParam("email", $email);
    $sth->bindParam("password", $password);
	$sth->execute();
    $items = $sth->fetchALL(PDO::FETCH_ASSOC);

    $json = array();

    $json['email'] = $email;
    $json['password'] = $password;
    $json['result'] = 0;
    if (count($items) > 0) {
    	$_SESSION['user_id'] = $json['result'] = $items[0]['id'];
    }
 //    foreach($items as $item) {
 //    	if (!isset($json[$item['id']])) {


	//       $json[$item['id']] = array(
	//           'id' => $item['id'],
	//           'names' => $item['names'],
	//           'surnames' => $item['surnames'],
	//           'profile' => $item['profile'],
	//           'units' => array()
	//       );
	//       }
	//    unset($item['id']);
	//    unset($item['names']);
	//    unset($item['surnames']);
	//    unset($item['profile']);
	//    $item['id'] = $item['unit_id'];
	//    unset($item['unit_id']);
	//    $json[$id]['units'][] = $item;
	// }

    $newResponse = $response->withJson($json);
    return $newResponse;
});

$app->get('/user/{user-id:[0-9]+}/project/{project-id:[0-9]+}/unit/{id:[0-9]+}', function (Request $request, Response $response) {
	$id = $request->getAttribute('id');
    // $json = array('id' => $id, 'name' => 'Mats Fjellner');

    $sth = $this->db->prepare("SELECT unit.id, unit.title, unit.saved, rel.fk_user_id AS author, page.title AS page_title, page.id AS page_id FROM gf_units AS unit INNER JOIN gf_user_units AS rel ON unit.id = rel.fk_unit_id LEFT JOIN gf_unit_pages AS rel2 ON unit.id = rel2.fk_unit_id LEFT JOIN gf_pages AS page ON rel2.fk_page_id = page.id WHERE unit.id=:id");
    $sth->bindParam("id", $id);
	$sth->execute();
	$items = $sth->fetchALL(PDO::FETCH_ASSOC);
	
	$json = array();
	foreach($items as $item) {
    	if (!isset($json[$item['id']])) {

			$json[$item['id']] = array(
				'id' => $item['id'],
				'title' => $item['title'],
				'saved' => $item['saved'],
				'author' => $item['author'],
				'pages' => array()
			);
		}
		unset($item['id']);
		unset($item['title']);
		unset($item['saved']);
		unset($item['author']);
		$item['id'] = $item['page_id'];
		unset($item['page_id']);
		$item['title'] = $item['page_title'];
		unset($item['page_title']);
		$json[$id]['pages'][] = $item;
	}

    $newResponse = $response->withJson($json[$id]);
    return $newResponse;
});

$app->get('full/user/{id}', function (Request $request, Response $response) {
	
});
$app->run();