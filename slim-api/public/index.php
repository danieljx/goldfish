<?php
// echo('http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'].'?'.$_SERVER['QUERY_STRING']);
// die();

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';
require '../../vendor/leafo/scssphp/scss.inc.php';
require '../../vendor/Underscore.php-master/underscore.php';

/*
/user/:id 		GET 	Get user info + unit ids 
/unit/:id 		GET 	Get unit info + page ids
/page/:id 		GET 	Get page info + block ids 
*/
session_start();


function add_log($param) {
    file_put_contents('log.txt', $param . PHP_EOL, FILE_APPEND);
}



// <HACK> Mats Fjellner 2016.11.04 Slim usa REQUEST_URI que contiene URL pre-rewrite
$_SERVER['REQUEST_URI'] = str_replace('/api/', '/slim-api/public/', $_SERVER['REQUEST_URI']);
// </HACK>

const ROLE_SUPERADMIN = 1;
const ROLE_ADMIN = 2;
const ROLE_EDITOR = 3;


// Instantiate the app
$settings = require __DIR__ . '/../src/settings.php';
$app = new \Slim\App($settings);

// $app['notFoundHandler'] = function ($app) {
//     return function ($request, $response) use ($app) {
//         return $app['response']->withStatus(404)->withHeader('Content-Type', 'text/html')->write('Page not found');
//     };
// };

// Set up dependencies
require __DIR__ . '/../src/dependencies.php';

// Register middleware
require __DIR__ . '/../src/middleware.php';

// Set up Language
require __DIR__ . '/../src/lang.php';

// Set up Manager database for Eloquent
require __DIR__ . '/../src/capsule.php';

// Register routes
require __DIR__ . '/../src/models.php';

// Register routes
require __DIR__ . '/../src/controllers.php';

// Register routes
require __DIR__ . '/../src/routes.php';
$app->run();