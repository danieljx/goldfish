<?php

use App\Langs\Lang;

$container['lang'] =  function ($c) {
    $availableLanguages = $c->get('settings')['availableLanguages'];
    $defaultLanguage    = $c->get('settings')['defaultLanguage'];

    $langPath = __DIR__ . "/Langs/" . $defaultLanguage . ".php";
    
    if(!is_file($langPath)) {
        throw new Error("Language not found => " . $availableLanguages);
    }
    $langArray = require $langPath;
    return new Lang($langArray);
};