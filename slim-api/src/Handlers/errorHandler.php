<?php

$container['errorHandler'] = function ($c) {
    return function ($request, $response, $exception) use ($c) {
        return $c['response']->withStatus(417)
                             ->withHeader('Content-Type', 'application/json')
                             ->write(json_encode(["error" => TRUE, "msg" => $c['lang']->getError($exception->getMessage())]));
    };
};