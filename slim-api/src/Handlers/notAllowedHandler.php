<?php

$container['notAllowedHandler'] = function ($c) {
    return function ($request, $response, $methods) use ($c) {
        return $c['response']
            ->withStatus(405)
            ->withHeader('Allow', implode(', ', $methods))
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(["error" => TRUE, "msg" => 'Method must be one of: ' . implode(', ', $methods)]));
    };
};