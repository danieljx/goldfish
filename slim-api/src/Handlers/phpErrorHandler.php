<?php

$container['phpErrorHandler'] = function ($c) {
    return function ($request, $response, $error) use ($c) {
        return $c['response']
            ->withStatus(500)
            ->withHeader('Content-Type', 'application/json')
            ->write(json_encode(["error" => TRUE, "msg" => $error->getMessage(), "line" => $error->getLine(), "code" => $error->getCode(), "file" => $error->getFile()]));
    };
};