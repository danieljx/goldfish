<?php
// Models

use App\Models\FileModel;
use App\Models\StorageModel;
use App\Models\MimeTypeModel;
use App\Models\FileShareModel;
use App\Models\UserConfigModel;
use App\Models\UserModel;
use App\Models\ProjectModel;
use App\Models\MountModel;
use App\Models\ComponentBlueprintModel;
use App\Models\ThemeBlueprintModel;
use App\Models\MediaqueryBlueprintModel;
use App\Models\FontModel;

$container['FileModel'] = function ($c) {
    return new FileModel();
};

$container['StorageModel'] = function ($c) {
    return new StorageModel();
};

$container['MimeTypeModel'] = function ($c) {
    return new MimeTypeModel();
};

$container['UserConfigModel'] = function ($c) {
    return new UserConfigModel();
};

$container['UserModel'] = function ($c) {
    return new UserModel();
};

$container['FileShareModel'] = function ($c) {
    return new FileShareModel();
};

$container['ProjectModel'] = function ($c) {
    return new ProjectModel();
};

$container['MountModel'] = function ($c) {
    return new MountModel();
};

$container['TagModel'] = function ($c) {
    return new TagModel();
};
$container['ComponentBlueprintModel'] = function ($c) {
    return new ComponentBlueprintModel();
};

$container['ThemeBlueprintModel'] = function ($c) {
    return new ThemeBlueprintModel();
};

$container['MediaqueryBlueprintModel'] = function ($c) {
    return new MediaqueryBlueprintModel();
};

$container['FontModel'] = function ($c) {
    return new FontModel();
};
