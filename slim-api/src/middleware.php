<?php
// Application middleware
namespace App\Middlewares;
// e.g: $app->add(new \Slim\Csrf\Guard);
require __DIR__ . '/Middlewares/TimerMiddleware.php';

$app->add( new TimerMiddleware() );
