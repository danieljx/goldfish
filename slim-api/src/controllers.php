<?php

use App\Controllers\FileController;
use App\Controllers\StorageController;
use App\Controllers\UserController;
use App\Controllers\ProjectController;
use App\Controllers\TagController;

$container['FileController'] = function ($c) {
    return new FileController($c);
};

$container['StorageController'] = function ($c) {
    return new StorageController($c);
};

$container['UserController'] = function ($c) {
    return new UserController($c);
};

$container['ProjectController'] = function ($c) {
    return new ProjectController($c);
};

$container['TagController'] = function ($c) {
    return new TagController($c);
};
