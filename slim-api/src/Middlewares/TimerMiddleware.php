<?php

namespace App\Middlewares;

class TimerMiddleware {

    public function __invoke($request, $response, $next) {
        $execStart = microtime(true);
        $response = $next($request, $response);

        $logger = new \Monolog\Logger('Access log');
        $formatter = new \Monolog\Formatter\LineFormatter(null, null, false, true);
        $file_handler = new \Monolog\Handler\StreamHandler("../logs/access.log");
        $file_handler->setFormatter($formatter);
        $logger->pushHandler($file_handler);
        $logger->info($request->getMethod()." ".$request->getUri()->getPath().":" ); 
        $logger->info(((microtime(true) - $execStart)*1000)." ms" ); 

        return $response;
    }

    


}