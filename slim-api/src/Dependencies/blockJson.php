<?php
namespace App\Dependencies;

use App\Controllers\FileController;

$container['blockJson'] = function ($c) { 
    return function($db, $json, $ancestry, $themeId) use ($c) {
        $blockCSSJson   = $c->get('blockCSSJson');
        $blockJson      = $c->get('blockJson');

        // Need to save red to filesystem before saving to bbdd, with hash
        // If update/hasId, folder exists
        
        add_log("NEW BLOCK ".$json['instanceOf']." at depth ".count($ancestry));
        // add_log( print_r($json, true) );

        if ($json['data'] && array_key_exists('plugin_name', $json['data']) ) {
            add_log("PLUGIN");
            add_log($json['data']['plugin_name'] !== '');
            add_log( "*".$json['data']['plugin_name']."*" );
            
            $dir = $_SERVER['DOCUMENT_ROOT'].'/content/red/'.$json['data']['plugin_name'];

            $FileController = new FileController($c);
            // TODO: en vez de 1 será el usuario real
            $FileController->setUserStorage($c->userId);

            if (!array_key_exists('id',$json)) { // Copia carpeta base
                // Format ludica-20180116-1224-45345
                $time = microtime();
                $split = explode(" ",$time);
                $secs = $split[1];
                $microsecs = $split[0]*1000000;
                $date = date_create_from_format('U', $secs);
                $postfix = date_format($date, 'Ymd-His')."-".$microsecs;
                $dataFolder     = array();
                $nameFolderv    = "reds";

                if ($FileController->hasFileDataStorage("files/".$nameFolderv))  {
                    $dataF = $FileController->getFileDataStorage("files/".$nameFolderv);
                    $dataFolder = $dataF['data'];
                } else {
                    $dataFolder = $FileController->createDirStorage($nameFolderv);
                }
                $saveFolder = $FileController->setFolderStorage($dir, $dataFolder['path_hash']);
                $path = $saveFolder['data']['path'];
                $finalPath = $json['data']['plugin_name'].'-'.$postfix;
                $saveFolder = $FileController->reNameFileStorage($finalPath, $saveFolder['data']['path_hash']);
                $userdata = $FileController->getFileDataStorage("files/reds/".$finalPath.'/userdata.xml');
                $json['url'] = $saveFolder['path_hash'];
                
                $xmlHash = $userdata['data']['path_hash'];
            } else {
                $dataF = $FileController->getFileDataStorage($json['url']);

                foreach ($dataF['data']['children'] as $file) {
                    if ($file['name'] == 'userdata.xml') {
                        $xmlHash = $file['path_hash'];
                        break;
                    }
                }
            }

            // Save XML
            $FileController->upFileStorage($json['data']['xml'], $xmlHash, true);
            // TODO: Save user files (remove unused?)
        }

        $parentId = count($ancestry) > 0 ? $ancestry[count($ancestry) - 1] : 0;
        $hasId = array_key_exists('id',$json);

        if ($hasId) {
            $sth = $db->prepare("INSERT INTO gf_components (id, saved, body, data, repeatable, parent_id, type, url, gf_component_blueprints_id) VALUES (:id, NOW(), :body, :data, :repeatable,:parent_id, :type, :url, :instanceOf)");
        } else {
            $sth = $db->prepare("INSERT INTO gf_components (saved, body, data, repeatable, parent_id, type, url, gf_component_blueprints_id) VALUES (NOW(), :body, :data, :repeatable, :parent_id, :type, :url, :instanceOf)");
        }

        $sth->bindParam("body", $json['body']);
        // add_log(print_r($json['data'], true));
        $sth->bindParam("data", json_encode($json['data']));
        $sth->bindParam("repeatable", $json['repeatable']);
        $sth->bindParam("instanceOf", $json['instanceOf']);
        $sth->bindParam("type", $json['type']);
        $sth->bindParam("url", $json['url']);
        $sth->bindParam("parent_id", $parentId);

        if ($hasId) {
            $sth->bindParam("id", $json['id']);
            $sth->execute();		
        } else {
            $sth->execute();
            $json['id'] = $db->lastInsertId();	
        }

        foreach ($json['cssQueries'] as $key => $query) {
            $json['cssQueries'][$key] = $blockCSSJson($db,$query,$json['id'], $themeId, $json['instanceOf']);
        }

        $depth = count($ancestry);
        $ancestry[] = $json['id'];
        
        $sth = $db->prepare("INSERT INTO gf_component_relations (gf_ascendant,gf_descendant,depth,position,block_group) VALUES (:ascendant,:descendant,:depth,:position,:group)");
        $sth->bindParam('descendant',$json['id']);
        // $sth->bindParam('depth',$depth);
        for ($i = 0; $i<count($ancestry); $i++) {
            $sth->bindParam('ascendant',$ancestry[$i]);
            $sth->bindValue('depth',$depth-$i);
            $sth->bindValue('position',$json['position']);
            $sth->bindValue('group',$json['group']);
            $sth->execute();
        }

        foreach($json['blocks'] as $key => $block) {
            $json['blocks'][$key] = $blockJson($db, $block, $ancestry, $themeId);
        }
        return $json;
    };
};