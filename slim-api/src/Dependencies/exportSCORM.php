<?php

use App\Controllers\FileController;
use App\Dependencies\SCORM\Scorm;

$container['exportSCORM'] = function ($c) {
	return function (&$zip, $unitData, $resources, $css, $nav) use ($c) {
		$scorm = new Scorm($zip, $unitData, $resources, '../src/Dependencies/SCORM/tpl/');
		$scorm->addFile('adlcp_rootv1p2.xsd', file_get_contents('../../js/scorm12/adlcp_rootv1p2.xsd'), false);
		$scorm->addFile('ims_xml.xsd', file_get_contents('../../js/scorm12/ims_xml.xsd'), false);
		$scorm->addFile('imscp_rootv1p1p2.xsd', file_get_contents('../../js/scorm12/imscp_rootv1p1p2.xsd'), false);
		$scorm->addFile('imsmd_rootv1p2p1.xsd', file_get_contents('../../js/scorm12/imsmd_rootv1p2p1.xsd'), false);

		$scorm->addFile('content/all.css', $css.' '.$nav['css']);
		$scorm->addFile('dist/css/semantic.min.css', file_get_contents('../../dist/lib/semantic/dist/semantic.min.css'));
		$scorm->addFile('dist/unit.json.js', 'var unit = '.json_encode($unitData).';' );
		$scorm->addFile('dist/bundle.export.js', file_get_contents('../../dist/js/bundle.export.js') );
		$scorm->addFile('dist/fishunit.min.js', file_get_contents('../../dist/js/fishunit.min.js') );
		$scorm->addFile('maps/bundle.export.js.map', file_get_contents('../../dist/maps/bundle.export.js.map') );
		$scorm->addFile('maps/fishunit.min.js.map', file_get_contents('../../dist/maps/fishunit.min.js.map') );
		$scorm->addFile('dist/bundle.export.scorm12.js', file_get_contents('../../dist/js/bundle.export.scorm12.js'));
		$scorm->addFile('maps/bundle.export.scorm12.js.map', file_get_contents('../../dist/maps/bundle.export.scorm12.js.map'));
		$scorm->addFile($unitData['title'].'.html', __::template( file_get_contents('../../dist/tpl/export.tpl'), array('title' => $unitData['title'], 'cacheKiller' => time(), 'cmi5' => '0', 'scorm12' => '1' ) ) );
		$scorm->addFile('imsmanifest.xml', __::template( file_get_contents('../src/Dependencies/SCORM/tpl/imsmanifest.tpl'), $scorm->getData()), false);
	};
};