<?php
$container['pageJson'] = function ($c) { 
    return function($db, $json, $ancestry, $unitId, $themeId) use ($c) {
        $blockCSSJson   = $c->get('blockCSSJson');
        $blockJson      = $c->get('blockJson');
        $pageJson      = $c->get('pageJson');

        // add_log("pageJson");
        // add_log(print_r($json, true));

        $parentId = count($ancestry) > 0 ? $ancestry[count($ancestry) - 1] : 0;

        if (isset($json['id']) && $json['id']) {

            $sth = $db->prepare("SELECT gf_descendant AS id FROM gf_component_relations WHERE gf_ascendant = :component_id"); 
            $sth->bindParam("component_id", $json['block']['id']); 
            $sth->execute(); 
            $items = $sth->fetchALL(PDO::FETCH_ASSOC); 
            $ids = []; 
 
            foreach($items as $item) {
                $ids[] = $item['id']; 
            }

            $sth = $db->prepare("DELETE c FROM gf_components as c WHERE id IN (".implode(',', $ids).")"); 
            $sth->execute(); 
        }

        $json['block'] = $blockJson($db, $json['block'],[], $themeId);

        if (isset($json['id']) && $json['id']) {
            $sth = $db->prepare("INSERT INTO gf_pages (id,title,saved,gf_unit_id,gf_components_id) VALUES(:id, :title, NOW(), :unit_id, :component_id)");
            $sth->bindParam("id", $json['id']);
            $sth->bindParam("title", $json['title']);
            $sth->bindParam("unit_id", $unitId);
            $sth->bindParam("component_id", $json['block']['id']);
            $sth->execute();
        } else {
            $sth = $db->prepare("INSERT INTO gf_pages (title,saved,gf_unit_id,gf_components_id) VALUES(:title, NOW(), :unit_id, :component_id)");
            $sth->bindParam("title", $json['title']);
            $sth->bindParam("unit_id", $unitId);
            $sth->bindParam("component_id", $json['block']['id']);
            $sth->execute();
            $json['id'] = $db->lastInsertId();
        }

        $depth = count($ancestry);
        $ancestry[] = $json['id'];

        $sth = $db->prepare("INSERT INTO gf_page_relations (gf_ascendant,gf_descendant,depth,position,root) VALUES (:ascendant,:descendant,:depth,:position,:root)");
        $sth->bindParam('descendant',$json['id']);

        for ($i = 0; $i<count($ancestry); $i++) {
            $sth->bindParam('ascendant',$ancestry[$i]);
            $sth->bindValue('depth',$depth-$i);
            $sth->bindValue('position',$json['position']);
            $sth->bindValue('root', ($depth == 0) ? 1 : 0 );
            $sth->execute();
        }

        foreach($json['pages'] as $key => $page) {
            $json['pages'][$key] = $pageJson($db, $page, $ancestry, $unitId, $themeId);
        }

        return $json;
    };
};