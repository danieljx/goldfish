<?php

$container['getUnit'] = function ($c) {
    return function ($id) use ($c) {
        
        $sth = $c->db->prepare("SELECT 
            unit.id, 
            unit.title, 
            unit.saved, 
            unit.gf_theme_instance_id AS user_theme,
            unit.data,
            unit.gf_output_formats_id AS output_format,
            theme.folder AS theme_folder, 
            theme_instance.gf_theme_blueprints_id AS theme,
            rel.gf_users_id AS author, 
            page.title AS page_title, 
            page.id AS page_id,
            page.gf_components_id
            FROM gf_units AS unit 
            INNER JOIN gf_unit_has_users AS rel 
            ON unit.id = rel.gf_unit_id 
            LEFT JOIN gf_pages AS page 
            ON unit.id = page.gf_unit_id 
            LEFT JOIN gf_theme_instances AS theme_instance
            ON unit.gf_theme_instance_id = theme_instance.id
            LEFT JOIN gf_theme_blueprints AS theme
            ON theme_instance.gf_theme_blueprints_id = theme.id
            WHERE unit.id=:id");
        $sth->bindParam("id", $id);
        $sth->execute();
        $items = $sth->fetchALL(PDO::FETCH_ASSOC);

        $pageIds    = array();
        $pages      = array();
        $rootId     = false;

        foreach($items as $item) {
            if (is_numeric($item['page_id']) && !in_array($item['page_id'], $pageIds)) {
                $pageIds[] = $item['page_id'];
            }
        }

        if (count($pageIds) > 0) {

            $sth = $c->db->prepare("SELECT 
                page.title AS title, 
                page.id AS id,
                rel.depth, 
                rel.position,
                rel.gf_ascendant,
                rel.root,
                page.gf_components_id
                FROM gf_pages AS page 
                JOIN gf_page_relations AS rel 
                ON rel.gf_descendant = page.id AND 
                (rel.depth = 1 OR rel.root = 1)
                WHERE page.id IN (".implode(',', $pageIds).")
                ORDER BY depth,position");
            $sth->bindParam("id", $id);
            $sth->execute();
            $pages = $sth->fetchALL(PDO::FETCH_ASSOC);
            $rootId = false;

            foreach ($pages as $page) {
                if ($page['root'] == '1') {
                    $rootId = $page['id'];
                }
            }

        }

        $buildPageTree = $c->get('buildPageTree');
        $pages = $buildPageTree($pages, $item['theme'], $rootId);

        $json = array();
        foreach($items as $item) {
            if (!isset($json[$item['id']])) {

                $json[$item['id']] = array(
                    'id' => $item['id'],
                    'title' => $item['title'],
                    'saved' => $item['saved'],
                    'data' => json_decode($item['data']),
                    'author' => $item['author'],
                    'theme' => $item['theme'],
                    'user_theme' => $item['user_theme'],
                    'output_format' => $item['output_format'],
                    'pages' => $pages
                );
            }
            unset($item['id']);
            unset($item['title']);
            unset($item['saved']);
            unset($item['data']);
            unset($item['author']);
            unset($item['theme']);
            unset($item['user_theme']);     
            unset($item['page_id']);
            unset($item['page_title']);
            unset($item['output_format']);
            unset($item['pages']);
            
        }
        return $json;

    };
};