<?php

class Helpers {


	public static function flyToZip(&$zip, &$storage, $zipFolder, $subFolder, $data, $userId) {
		if ($data['mimetype'] == 2) {
			foreach ($data['children'] as $subData) {
				if ($subData['mimetype'] == 2) {
					$storage2 = $storage->getFileDataStorage( $subData['path_hash'] );
					self::flyToZip($zip, $storage, $zipFolder, ($subFolder !== '' ? $subFolder.DIRECTORY_SEPARATOR : '').substr($subData['path'], strlen($data['path']) + 1 ), $storage2['data'], $userId );
				} else {
					self::flyToZip($zip, $storage, $zipFolder, $subFolder, $subData, $userId);
				}

			}

		} else {
			$path = $zipFolder.($subFolder !== '' ? DIRECTORY_SEPARATOR.$subFolder : '').DIRECTORY_SEPARATOR.$data['name'];
			$contents = $storage->getStorageFileSystem()->read($data['path']);
			$zip->addFromString($path, $contents);
		}
	}

	public static function prepareCSSQueries($cssQueries, $prefix) {
		$str = '';
		foreach($cssQueries as $cssQuery) {
			if ($cssQuery['mediaQuery'] !== '') {
				$str .= $cssQuery['mediaQuery'].'{';
			}
			$cssSelectors = [];
			foreach ( $cssQuery['body_css'] as $body_css ) {
				$selector = implode($body_css['selectors']);
				if ( !array_key_exists( $selector, $cssSelectors )) {
					$cssSelectors[ $selector."" ] = [];
				}
				foreach ( $body_css['properties'] as $propName => $propValue) {
					$cssSelectors[ $selector ][ $propName ] = $propValue;
				}
			}
			foreach ( $cssQuery['body_css_custom'] as $body_css ) {
				$selector = $body_css['selectors'];
				if ( !array_key_exists( $selector, $cssSelectors )) {
					$cssSelectors[ $selector."" ] = [];
				}
				foreach ( $body_css['properties'] as $propName => $propValue) {
					$cssSelectors[ $selector ][ $propName ] = $propValue;
					// $cssSelectors[ $selector ][ $property ] = $cssQuery['body_css_custom'][$body_css]['properties'][$property];
				}
			}

			foreach ($cssSelectors as $selector => $attrList) {
				$attributes = '{';
				foreach ($attrList as $propName => $propValue) {
					$attributes .= ' '.$propName.':'.$propValue.'; ';
				}
				$attributes .= '}';
				$str .= $prefix .' '.$selector.' '.$attributes;
			}

			// foreach ($cssQuery['rules'] as $rule) {
			// 	$res['css'] .= ' #block-'.implode('-',$depthArray).'-g'.$subBlock['group'].'-p'.$subBlock['position'].' '.$rule['parsed'];
			// }

			if ($cssQuery['mediaQuery'] !== '') {
				$str .= '}';
			}
		}
		// add_log($str);
		return $str;
	}
	/*
	* prepareBlock
	* Procesa un componente y sus subcomponentes para devolver un array [html, css]
	*/
	public static function prepareBlock($block,$depthArray, $forExport = false) {
		add_log("*** PREPAREBLOCK ***");
		if ($forExport) {
			add_log("EXPORT");
		}
		// add_log( print_r($block, true));
		$blockTag = '<blocks />';
		$bodyBlocks = explode($blockTag, $block['body']);
		$orderedBlocks = array( substr_count($block['body'], $blockTag) );
		$j = 0;
		$res = ['html'=>$block['body'], 'css'=>''];
		if ($forExport) {
			$res['resources'] = [];
		}
		foreach($block['blocks'] as $subBlock) {

			if (!is_array($orderedBlocks[$subBlock['group']*1])) {
				$orderedBlocks[$subBlock['group']*1] = array();
			}

			if (count($subBlock['blocks']) > 0) {

				$res['css'] = self::prepareCSSQueries($subBlock['cssQueries'], ' #block-'.implode('-',$depthArray).'-g'.$subBlock['group'].'-p'.$subBlock['position']);

				$depthArray[] = $j + 1;
				$recurse = self::prepareBlock($subBlock,$depthArray, $forExport);
				$orderedBlocks[$subBlock['group']*1][$subBlock['position']*1] = '<div class="block" id="block-'.implode('-',array_slice($depthArray, 0, -1)).'-g'.$subBlock['group'].'-p'.$subBlock['position'].'">'.$recurse['html'].'</div>';
				if ($forExport) {
					$res['resources'] = array_merge($res['resources'], $recurse['resources']);
				}
				$res['css']  .= $recurse['css'];

			} else {
				if ($subBlock['type']*1 == 2) {
					add_log("FOUND RED");
					$user_id = 1; // TODO: buscar el id real y luego incluir caso de archivo compartido
					$body = str_replace(
						[
							'{{url}}',
							'{{id}}'
						],
						[
							$forExport ? 'resources/'.$subBlock['url'].'/index.html' : '/api/user/'.$user_id.'/file/reds/'.$subBlock['url'].'/view/index.html',
							$subBlock['url']
						],
						$subBlock['data']['output']);
						if ($forExport) {
							$res['resources'] = array_merge($res['resources'], array("red" => array("user_id" => $user_id, "hash" => $subBlock['url'] ) ) );
						}
				} else if ($subBlock['type']*1 == 3) {
					$body = $subBlock['body'];
				} else {
					$body = $subBlock['body'];
				}

				$orderedBlocks[$subBlock['group']*1][$subBlock['position']*1] = '<div class="block" id="block-'.implode('-',$depthArray).'-g'.$subBlock['group'].'-p'.$subBlock['position'].'">'.$body.'</div>';

				$res['css'] .= self::prepareCSSQueries($subBlock['cssQueries'], ' #block-'.implode('-',$depthArray).'-g'.$subBlock['group'].'-p'.$subBlock['position']);

				// foreach($subBlock['cssQueries'] as $cssQuery) {
				// 	if ($cssQuery['mediaQuery'] !== '') {
				// 		$res['css'] .= $cssQuery['mediaQuery'].'{';
				// 	}
				// 	foreach ($cssQuery['rules'] as $rule) {
				// 		$res['css'] .= .' '.$rule['parsed'];
				// 	}
				// 	if ($cssQuery['mediaQuery'] !== '') {
				// 		$res['css'] .= '}';
				// 	}
				// }
			}
			$j++;
		}

		for ($k = 0; $k < count($orderedBlocks); $k++) {
			$bodyPart = '';
			if ( count($orderedBlocks[$k]) > 0) {
				$bodyPart = implode($orderedBlocks[$k]);
			}
			array_splice($bodyBlocks, (2*$k) +1, 0, $bodyPart);
		}

		$res['html'] = implode($bodyBlocks);
		// return ['html'=>$res['html'],'css'=>$res['css']];
		return $res;
	}

	public static function addDirToZip($source, &$zip, $base, &$resources = '') {
		$source = str_replace('\\', '/', realpath($source));
		while (strrpos($source, '/') == strlen($source) - 1) {
			$source = substr($source, 0, strrpos($source, '/'));
		}
		if (strrpos($source, 'sessiondata')) {
			$ignore = $source . '/';
			$ignore = substr($source, 0, strrpos($source, '/') + 1);
		} else {
			$ignore = substr($source, 0, strrpos($source, '/') + 1);
		}
		if (is_dir($source) === true) {
			$files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);
			foreach ($files as $file) {
				$file = str_replace('\\', '/', $file);
				//Ignore "." and ".." folders
				if (in_array(substr($file, strrpos($file, '/') + 1), array('.', '..'))) {
					continue;
				}
				$file = str_replace('\\', '/', realpath($file));
				if (is_dir($file) === true) {
					$file = $base . str_replace($ignore, '', $file);
					$zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
				} else if (is_file($file) === true) {
					$file2 = $base . str_replace($ignore, '', $file);
					$zip->addFromString(str_replace($source . '/', '', $file2), file_get_contents($file));
					$resources.='<file href="' . $file2 . '"/>';
				}
			}
		}
	}

	public static function getRuleSet($rule) {
		$selectors = [];
		foreach($rule->getSelectors() as $selector) {
			$selectors[] = $selector->getValue();
		}
	
		$declarations = [];
	
		foreach($rule->getDeclarations() as $declaration) {
			$declarations[$declaration->getProperty()] = $declaration->getValue();
		}

		return array(
			"selectors" => $selectors,
			"properties" => $declarations
		);
	}

}
