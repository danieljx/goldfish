<?php

use App\Controllers\FileController;

$container['exportUnit'] = function ($c) {
	return function ($user_id, $project_id, $unit_id) use ($c) {
	// Obtenemos la unidad
		error_reporting(E_ERROR);

		function processPage(&$zip, $page, &$i) {
			$body = '<h2>'.$page['title'].'</h2>';
			$blocks = ['html'=>'', 'css'=>'', 'resources'=>[]];
			$navObj = array(
                'css' => '',
				'title' => $page['title'],
				'id' => $page['id'],
				'i' => $i,
				'visited' => false,
				'position' => $page['position'],
				'children' => [],
				'resources' => []
			);

			if (count($page['block']['blocks']) > 0) {
				$blocks = Helpers::prepareBlock($page['block'],[$i+1,1], true);
            }
			$navObj['css'] = $blocks['css'];
			$navObj['resources'] = $blocks['resources'];

			// $zip->addFromString('content/page_'.$i.'.css', $blocks['css']);
			$zip->addFromString('content/page_'.$i.'.htm', $body.$blocks['html']);

			$i++;
			if (array_key_exists('pages', $page)) {
				foreach ($page['pages'] as $subpage) {
                    $fnord = processPage($zip, $subpage, $i);
                    $navObj['children'][] = $fnord;
					$navObj['css'] .= ' '.$fnord['css'];
					$navObj['resources'] = array_merge($navObj['resources'], $fnord['resources']);
				}	
            }

			return $navObj;
		}

		$getUnit = $c->get('getUnit');

		$unit = json_decode( json_encode($getUnit($unit_id)[$unit_id]), true);
		$cmi5 = $unit['output_format']*1 == 2;
		$scorm12 = $unit['output_format']*1 == 1;

	// ..y su plantilla
		$userTheme = $c->get('userTheme');
		$theme = $userTheme($unit["user_theme"]);

	// Definimos ruta de archivo zip a generar
		$config = json_decode(file_get_contents('../../config.json'), true);
		$tempDir = '/preview/'.$user_id.'-'.$project_id.'-'.$unit_id;
		$tempDirFull = $config['baseDir'].'/'.$config['dataFolder'].$tempDir;

		if (!file_exists($tempDirFull) && !mkdir($tempDirFull)) {
			add_log("Couldn't create directory ".$tempDirFull);
			die();
		}

		$zip = new ZipArchive;

        $zipName = $tempDirFull.'/'.rand(100000,999999).'.zip';
        
        $css = '';

        $scss = new scssc();
		$scss->addImportPath('../../components/_common');
		$scss->addImportPath('../../units');
		$scss->setFormatter('scss_formatter_compressed');
		
		$css .= $scss->compile('$assetPath: \'theme/assets/\'; '.$theme['sass'].' $fontPath: \'../dist/fonts/\';'.' @import "basics.scss";   @import "scss/nav_accordeon.scss";   @import "scss/nav_d3.scss"; ');
        if ($theme['unitsass']) {
            $css .= $scss->compile('$assetPath: \'theme/assets/\'; '.$theme['sass'].$theme['unitsass']).' ';
		}

		if ($zip->open($zipName, ZIPARCHIVE::CREATE)) {
			$i = 0;
			$nav = processPage($zip, $unit['pages'], $i);
            // $navJson = json_encode( $nav );
			$resources = '<resources>';
			
			if ($cmi5) {
				$cmi5Settings = array(
					'courseTitle' => $unit['title'],
					'auTitle' => $unit['title'],
					'courseDescription' => 'Cátedra cmi5 descripción por defecto',
					'auDescription' => '',
					'courseId' => $c->get('settings')['defaultPublisher'] . 'courses/' . md5($unit['id']),
					'auId' => ''
				);
	
				$cmi5Settings['auId'] = $cmi5Settings['courseId'].'/aus/'.md5($unit['id']+1);
				$cmi5Settings['auDescription'] = $cmi5Settings['courseDescription'];
			}
			
            foreach($nav['resources'] as $key => $resource) {
				if ($key == "red") {
					$FileController = new FileController($c);
					$FileController->setUserStorage($c->userId);
					$storage = $FileController->getFileDataStorage( $resource['hash'] );
					//Helpers::flyToZip($zip, $FileController, 'resources', $resource['hash'], $storage['data'], 1);
				}
			}

            Helpers::addDirToZip('../../components/themes/'.$theme['name'].'/assets', $zip, 'theme/', $resources);
			Helpers::addDirToZip('../../dist/css/fonts', $zip, 'dist/', $resources);
			
			if ($cmi5) {
				$zip->addFromString('content/all.css', $css.' '.$nav['css']);
				$zip->addFromString('dist/css/semantic.min.css', file_get_contents('../../dist/lib/semantic/dist/semantic.min.css'));
				$zip->addFromString('dist/unit.json.js', 'var unit = '.json_encode($unit).';' );
				
				$zip->addFromString('dist/bundle.export.js', file_get_contents('../../dist/js/bundle.export.js') );
				$zip->addFromString('dist/fishunit.min.js', file_get_contents('../../dist/js/fishunit.min.js') );

				$zip->addFromString('maps/bundle.export.js.map', file_get_contents('../../dist/maps/bundle.export.js.map') );
				$zip->addFromString('maps/fishunit.min.js.map', file_get_contents('../../dist/maps/fishunit.min.js.map') );				
				$zip->addFromString($unit['title'].'.html', __::template( file_get_contents('../../dist/tpl/export.tpl'), array('title' => $unit['title'], 'cacheKiller' => time(), 'cmi5' => ( $cmi5 ? '1' : '0' ), 'scorm12' => '0' ) ) );
				$zip->addFromString('dist/bundle.export.cmi5.js', file_get_contents('../../dist/js/bundle.export.cmi5.js') );	
				$zip->addFromString('cmi5.xml', __::template( file_get_contents('../../dist/tpl/cmi5.tpl'), $cmi5Settings ) );
			}

			$resources .= '</resources>';
			
			if ($scorm12) {
				$exportSCORM = $c->get('exportSCORM');
				$exportSCORM($zip, $unit, $resources, $css, $nav);
			}
			
			$zip->close();
            
            // $scss = new scssc();
            // $scss->addImportPath('../components/_common');
            // $scss->setFormatter('scss_formatter_compressed');
    
            // $css = $scss->compile($theme['sass'].' @import "basics.scss";');
            // if ($theme['unitsass']) {
            //     $css .= $scss->compile($theme['sass'].$theme['unitsass']);
            // }
		}	

		return [
            'realName' => $zipName, 
            'publicName' => $unit['title'].'.zip'
        ];
	};
};