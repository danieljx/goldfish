<?php

$container['buildTree'] = function ($c) {
    return function ($elements, $parentId) use ($c) {
        $branch = array();
        $children = array();
        $buildTree  = $c->get('buildTree');
        // add_log("buildTree");
        // add_log( print_r($elements, true));
        foreach ($elements as $el) {
            if ($el['id'] == $parentId) {
                if (!isset($branch['id'])) {
                    $branch['id'] = $el['id'];
                    $branch['body'] = $el['body'];
                    $branch['data'] = json_decode( $el['data'] );
                    $branch['repeatable'] = $el['repeatable'];
                    $branch['type'] = $el['type'];
                    $branch['url'] = $el['url'];
                    $branch['position'] = $el['position'];
                    $branch['group'] = $el['block_group'];
                    $branch['instanceOf'] = $el['instanceOf'];
                    $branch['blocks'] =  [];
                    $branch['cssQueries'] = [];
                }
                    
                if ($el['css_id']) {
                    $sass = ($el['body_css_custom'] !== '');
                    $branch['cssQueries'][] = array(
                        'id' => $el['css_id'],
                        'instanceOf' => $el['css_instanceOf'],
                        'mediaQuery' => $el['media_query'],
                        'body_css_custom' => json_decode($el['body_css_custom']),
                        'body_css' =>  json_decode($el['body_css'])
                        );
                    
                }
            } else if ($el['parent_id'] == $parentId && !in_array($el['id'], $children)) { // Resolver duda de esta parte
                $branch['blocks'][] = $buildTree($elements, $el['id']);
                $children[] = $el['id'];
            }
        }

        return $branch;

    };
};