<?php

$container['userTheme'] = function ($c) {
    return function ($id) use ($c) {
        $sth = $c->db->prepare("SELECT 
    	blueprint.id AS blueprint_id,
    	blueprint.name,
    	blueprint.thumbnail, 
    	blueprint.folder, 
    	blueprint.description, 
    	blueprint.includes,
    	blueprint.sass,
    	blueprint.unitsass,
    	changed,
    	body_css,
    	instance.id
    	FROM gf_theme_instances AS instance
    	INNER JOIN gf_theme_blueprints AS blueprint
    	ON instance.gf_theme_blueprints_id = blueprint.id
    	WHERE instance.id=:id");
    $sth->bindParam("id", $id);
	$sth->execute();
    $items = $sth->fetch(PDO::FETCH_ASSOC);
    return $items;

    };
};