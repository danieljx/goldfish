<?php
$container['blockCSSJson'] = function ($c) {
    return function ($db,$json,$componentId, $themeId, $instanceOf) use ($c) {
        // $css = "'".implode("','",$json['rules'][0])."'";
        $css = json_encode($json['body_css_custom']);
        // $dboSQL = array_key_exists('id',$json) ? 
        // "UPDATE gf_mediaqueries 
        // SET body_css=:css, media_query=:query, gf_components_id=:component_id, gf_mediaquery_blueprints_id=:instanceOf 
        // WHERE id = :id" : 
        // "INSERT INTO gf_mediaqueries 
        // (body_css,media_query,gf_components_id,gf_mediaquery_blueprints_id) VALUES (:css,:query,:component_id,:instanceOf)";
        $dboSQL = "INSERT INTO gf_mediaqueries 
        (body_css,media_query,gf_components_id,gf_mediaquery_blueprints_id) VALUES (:css,:query,:component_id,:instanceOf)";

        $sth = $db->prepare($dboSQL);
        // $sth->bindValue("id", $json['id'] ? $json['id'] : 0);
        $sth->bindParam("css", $css); 
        $sth->bindParam("query", $json['mediaQuery']);
        $sth->bindParam("component_id", $componentId);
        $sth->bindParam("instanceOf", $json['instanceOf']);

        

        
        
        $sth->execute();	
        if (isset($json['id']) && !$json['id']) {
            $json['id'] = $db->lastInsertId();	
        }

        // add_log("Theme: ".$themeId);
        // add_log("Blueprint id: ".$instanceOf);
        // add_log("Mediaquery: ".$json['mediaQuery']);

        $dboSQL = "SELECT body_css FROM gf_mediaquery_blueprints
        WHERE gf_theme_blueprints_id = :theme_id AND gf_component_blueprints_id = :blueprint_id AND media_query = :media_query";

        $sth = $db->prepare($dboSQL);
        $sth->bindParam("theme_id", $themeId); 
        $sth->bindParam("blueprint_id", $instanceOf); 
        $sth->bindParam("media_query", $json['mediaQuery']); 

        $sth->execute();
        $mediaQuery = $sth->fetch(PDO::FETCH_ASSOC);

        if (isset($mediaQuery['body_css'])) {
            $json['body_css'] = json_decode($mediaQuery['body_css']);
        } else {
            $json['body_css'] = [];
        }


        return $json;
    };
};