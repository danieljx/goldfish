<resource identifier="<%=$identifier%>" type="webcontent" adlcp:scormtype="sco" href="<%=$href%>.html">
    <% __::each($files, function($file, $index) { %>
        <file href="<%=$file['href']%>" />
    <% }); %>
</resource>