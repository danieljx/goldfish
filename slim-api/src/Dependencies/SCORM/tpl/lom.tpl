<lom xmlns="http://www.imsglobal.org/xsd/imsmd_rootv1p2p1"
			  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
			  xsi:schemaLocation="http://www.imsglobal.org/xsd/imsmd_rootv1p2p1 imsmd_rootv1p2p1.xsd">
			<general>
				<title>
					<langstring xml:lang="es-ES"><%=$title%></langstring>
				</title>
				<language>es-ES</language>
				<description>
					<langstring xml:lang="es-ES"><%=$title%></langstring>
				</description>
				<aggregationlevel>
					<source>
						<langstring xml:lang="x-none">LOMv1.0</langstring>
					</source>
					<value>
						<langstring xml:lang="x-none">3</langstring>
					</value>
				</aggregationlevel>
			</general>
			<lifecycle>
				<version>
					<langstring xml:lang="x-none">1</langstring>
				</version>
				<status>
					<source>
						<langstring xml:lang="x-none">LOMv1.0</langstring>
					</source>
					<value>
						<langstring xml:lang="x-none">draft</langstring>
					</value>
				</status>
			</lifecycle>
			<metametadata>
				<metadatascheme>ADL SCORM 1.2</metadatascheme>
				<language>es-ES</language>
			</metametadata>
			<technical>
				<format>text/html</format>
                <format>application/x-shockwave-flash</format>
			</technical>
			<educational>
			</educational>
			<rights>
				<cost>
					<source>
						<langstring xml:lang="x-none">LOMv1.0</langstring>
					</source>
					<value>
						<langstring xml:lang="x-none">no</langstring>
					</value>
				</cost>
				<copyrightandotherrestrictions>
					<source>
						<langstring xml:lang="x-none">LOMv1.0</langstring>
					</source>
					<value>
						<langstring xml:lang="x-none">yes</langstring>
					</value>
				</copyrightandotherrestrictions>
			</rights>
			<classification>
				<purpose>
					<source>
						<langstring xml:lang="x-none">LOMv1.0</langstring>
					</source>
					<value>
						<langstring xml:lang="x-none">Skill Level</langstring>
					</value>
				</purpose>
				<description>
					<langstring xml:lang="es-ES"><%=$title%></langstring>
				</description>
				<keyword>
					<langstring xml:lang="es-ES"><%=$keywords%></langstring>
				</keyword>
				<keyword>
	<langstring xml:lang="es-ES"><%=$title%></langstring>
</keyword>

			</classification>
		</lom>