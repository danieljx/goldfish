<?php
namespace App\Dependencies\SCORM;

use \Error;
use \Exception;

use __;

class Scorm {
    protected $path;
    protected $version = "1.2";
    protected $zip;
    protected $data;
    protected $resources;
    protected $identifier;
    public function __construct(&$zip, $data, &$resources, $path = "") {
        $this->zip          = $zip;
        $this->data         = $data;
        $this->resources    = $resources;
        $this->path         = $path;
        $this->identifier   = 'resource_' . time() . '_' . $this->data['title'];
    }
    public function addFile($name, $stream, $rfile = true) {
        $this->zip->addFromString($name, $stream);
        if($rfile) {
            $xml	= simplexml_load_string($this->resources);
            $toDom	= dom_import_simplexml($xml);
            $fx = new \SimpleXMLElement('<file />');
            $fx->addAttribute('href', $name);
            $fromFx	= dom_import_simplexml($fx);
            $toDom->appendChild($toDom->ownerDocument->importNode($fromFx, true));
            $this->resources = $xml->asXML();
        }
    }
    public function setPathTpl($path) {
        $this->path = $path;
    }
    public function getData() {
        return array(
            'title'     => $this->data['title'],
            'lom'       => $this->getLom(),
            'items'     => $this->getItem(),
            'resources' => $this->getResourse()
        );
    }
    private function getLom() {
        $data = array(
            'title' => $this->data['title'],
            'keywords' => ''
        );
        return __::template( file_get_contents($this->path . 'lom.tpl'), $data );
    }
    private function getItem() {
        $data = array(
            'title'         => $this->data['title'],
            'identifierref' => $this->identifier,
            'identifier'    => 'item_' . time() . '_' . $this->data['title']
        );
        return __::template( file_get_contents($this->path . 'item.tpl'), $data );
    }
    private function getResourse() {
        $data = array(
            'identifier'    => $this->identifier,
            'href'          => $this->data['title'],
            'files'         => $this->getFiles()
        );
        return __::template( file_get_contents($this->path . 'resource.tpl'), $data );
    }
    private function getFiles() {
        $files = array();
        $xml = simplexml_load_string($this->resources);
        foreach ($xml->file as $key => $value) {
                $data = get_object_vars($value);
                $files[] = $data['@attributes'];
        }
        return $files;
    }
}