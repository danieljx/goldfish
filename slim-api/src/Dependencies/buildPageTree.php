<?php

$container['buildPageTree'] = function ($c) {
    return function ($elements, $themeId, $parentId) use ($c) {
        $branch = array();
        $children = array();
        $buildPageTree  = $c->get('buildPageTree');
        foreach ($elements as $el) {
            if ($el['id'] == $parentId) {
                if (!isset($branch['id'])) {
                    $branch['id'] = $el['id'];
                    $branch['title'] = $el['title'];
                    $branch['depth'] = $el['depth'];
                    $branch['position'] = $el['position'];
                    $branch['gf_components_id'] = $el['gf_components_id'];

                    $sth = $c->db->prepare("SELECT 
                    c.id, 
                    c.saved, 
                    c.body, 
                    c.data,
                    c.repeatable,
                    c.type,
                    c.url,
                    c.gf_component_blueprints_id AS instanceOf, 
                    rel.depth, 
                    rel.block_group,
                    rel.position,
                    rel.gf_ascendant, 
                    c.parent_id, 
                    css.id AS css_id, 
                    css.media_query, 
                    cssbp.gf_theme_blueprints_id,
                    cssbp.gf_component_blueprints_id,
                    css.body_css AS body_css_custom, 
                    css.gf_mediaquery_blueprints_id AS css_instanceOf,
                    cssbp.body_css AS body_css
                    FROM 
                    gf_components AS c
                    INNER JOIN gf_component_relations AS rel 
                    ON c.id = rel.gf_descendant 
                    INNER JOIN gf_mediaqueries AS css 
                    ON c.id = css.gf_components_id 
                    INNER JOIN gf_mediaquery_blueprints AS cssbp_old
                    ON css.gf_mediaquery_blueprints_id = cssbp_old.id
                    INNER JOIN gf_mediaquery_blueprints AS cssbp
                    ON (cssbp.gf_component_blueprints_id = cssbp_old.gf_component_blueprints_id AND
                    cssbp.gf_theme_blueprints_id = :theme_id AND
                    cssbp.media_query = cssbp_old.media_query)
                    WHERE rel.gf_ascendant=:base_component
                    ORDER BY depth,block_group,position");

                    $sth->bindParam("theme_id", $themeId);
                    $sth->bindParam("base_component", $el['gf_components_id']);

                    $sth->execute();
                    $components = $sth->fetchALL(PDO::FETCH_ASSOC);
                    $buildTree = $c->get('buildTree');
                    $branch['block'] = $buildTree($components,$el['gf_components_id']);
                    
                }
                    
                
            } else if ($el['gf_ascendant'] == $parentId && !in_array($el['id'], $children)) {
                    $branch['pages'][] = $buildPageTree($elements, $themeId, $el['id']);
                    $children[] = $el['id'];
            }
        }

    return $branch;

    };
};