<?php
return [
    'TITLE'     => 'Api GoldFish',
    'TITLE_MSG' => 'Welcome',
    'DEFAULT_FILES' => [
        'FILES'       => 'Home',
        'DOCUMENTS'   => 'Documents',
        'PHOTOS'      => 'Photos',
        'SHARED'      => 'Shared with me',
        'ME'          => 'My Files',
        'PUBLIC'      => 'Public'
    ],
    'ERROR'     => [
        'RESOURCE_NOT_FOUND'    => 'Resource not found',
        'STORAGE_NOT_FOUND'     => 'Storage not found',
        'CREATE_FOLDER'         => 'Error create folder',
        'FOLDER_EXIST'          => 'Folder exists',
        'NOT_DELETE_FOLDER'     => 'Folder not deleted',
        'NOT_DELETE_FILE'       => 'File not deleted'
    ]
];
