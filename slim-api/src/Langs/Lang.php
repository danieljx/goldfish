<?php
namespace App\Langs;

class Lang {
	protected $local;

	public function __construct($langArray)  {
		$this->setLangData($langArray);
	}
	public function setLangData($langArray) {
		if (!is_array($langArray)) {
			throw new Exception('Not a Array.');
		}
		$this->local = $langArray;
	}
	public function getLangData() {
		return $this->local;
	}
	public function get($key = "")  {
		if(!is_array($key)) {
			if(isset($this->local[$key])) {
				return $this->local[$key];
			} else {
				return $key;
			}
		} else {
			return $key;
		}
	}
	public function getDefault($key = "")  {
		if(isset($this->local["DEFAULT_FILES"][$key])) {
			return $this->local["DEFAULT_FILES"][$key];
		} else {
			return $key;
		}
	}
	public function getError($key = "")  {
		if(isset($this->local["ERROR"][$key])) {
			return $this->local["ERROR"][$key];
		} else {
			return $key;
		}
	}
	public function __toString() {
		return $this->get();
	}
}
