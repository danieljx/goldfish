<?php
return [
    'TITLE'         => 'Api GoldFish',
    'TITLE_MSG'     => 'Bienvenido',
    'DEFAULT_FILES' => [
        'FILES'       => 'Home',
        'DOCUMENTS'   => 'Documentos',
        'PHOTOS'      => 'Fotos',
        'SHARED'      => 'Compartidos conmigo',
        'ME'          => 'Mis Archivos',
        'PUBLIC'      => 'Público'
    ],
    'ERROR'         => [
        'RESOURCE_NOT_FOUND'    => 'Recurso no encontrado',
        'STORAGE_NOT_FOUND'     => 'Storage no encontrado',
        'CREATE_FOLDER'         => 'Error al crear carpeta',
        'FOLDER_EXIST'          => 'Carpeta ya existe',
        'NOT_DELETE_FOLDER'     => 'Carpeta no borrada',
        'NOT_DELETE_FILE'       => 'Archivo no borrado'
    ]
];
