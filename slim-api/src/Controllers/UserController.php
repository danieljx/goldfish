<?php
namespace App\Controllers;

use \Error;
use \Exception;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Slim\Interfaces\RouterInterface as Router;
use Slim\Http\UploadedFile as UploadedFile;
use Slim\Http\Stream;
use App\Models\UserModel;
use App\Models\UserConfigModel;

class UserController extends Controller {

    public function __construct($c) {
        parent::__construct($c);
        $this->pathRoot         = $this->getContainer()->get('settings')['pathRoot'];
        $this->pathApp          = $this->getContainer()->get('settings')['pathApp'];
        $this->UserModel        = $this->getContainer()->get('UserModel');
        $this->UserConfigModel  = $this->getContainer()->get('UserConfigModel');
    }
    public function getUsers($request, $response, $args) {
        $data = $this->UserModel->getUsers();
        return $response->getBody()->write(json_encode($data));
    }
    public function getUsersList($request, $response, $args) {
        $userList = array(
            'page' => (isset($args['page'])?(int)$args['page']:1),
            'perPage' => (isset($args['perPage'])?(int)$args['perPage']:5),
            'total' => 0,
            'models' => array()
        );
        $start = ($userList['page']-1)*$userList['perPage'];
        $start = (($start < 0)?0:$start);
        $userList['models'] = $this->UserModel->getUsersList($start,$userList['perPage']);
        $userList['total']  = count($this->UserModel->getUsers());
        return $response->getBody()->write(json_encode($userList));
    }
    public function getUserSearch($request, $response, $args) {
        $this->q    = $request->getParam('q');
        $data       = $this->UserModel->getSearchUser($this->q);
        return $response->getBody()->write(json_encode($data));
    }
    public function setUser($request, $response, $args) {
        $data = $request->getParsedBody();
        if(!isset($data['config'])) {
            $data['config'] = array();
            $data['config']['editor']   = $data['editor'];
            $data['config']['theme']    = $data['theme'];
            $data['config']['effects']  = $data['effects'];
            if($data['action'] == 'up') {
                $data['config']['gf_users_id'] = $data['id'];
            }
            if(isset($data['config_id']) && !empty($data['config_id'])) {
                $data['config']['id'] = $data['config_id'];
            }
            unset($data['config_id']);
            unset($data['editor']);
            unset($data['theme']);
            unset($data['effects']);
        }
        if($data['action'] == 'add') {
            $this->UserModel->setUser($data);
            $dataUser = $this->UserModel->toArray();
            $data['config']['gf_users_id'] = $dataUser['id'];
            $this->UserConfigModel->setConfig($data['config']);
            $dataConfig = $this->UserConfigModel->toArray();
            $data['action'] = 'up';
        } else if($data['action'] == 'up') {
            $this->UserModel->updateUser($data);
            if(isset($data['config']['id'])) {
                $this->UserConfigModel->updateConfig($data['config']);
            } else {
                $data['config']['gf_users_id'] = $data['id'];
                $this->UserConfigModel->setConfig($data['config']);
            }
            $dataUser   = $this->UserModel->toArray();
            $dataConfig = $this->UserConfigModel->toArray();
        } else if($data['action'] == 'del') {
            $this->UserModel->delUser($data);
            $this->UserConfigModel->delConfig($data['config']);
            $dataUser   = $this->UserModel->toArray();
            $dataConfig = $this->UserConfigModel->toArray();
        }
        $uploadedFiles = $request->getUploadedFiles();
        if($data['action'] != 'del' && isset($uploadedFiles['file']) && count($uploadedFiles['file'])) {
            $pathProfile = $this->pathRoot . DIRECTORY_SEPARATOR . $this->pathApp . DIRECTORY_SEPARATOR . 'user' . DIRECTORY_SEPARATOR . 'profile' . DIRECTORY_SEPARATOR . $dataUser['id'];
            if(!is_dir($pathProfile)) {
                mkdir($pathProfile);
            }
            if(is_dir($pathProfile)) {
                $dataUser['profile'] = $uploadedFiles['file']->getClientFilename();
                $uploadedFiles['file']->moveTo($pathProfile . DIRECTORY_SEPARATOR . $dataUser['profile']);
                $this->UserModel->updateUser($dataUser);
            }
        }
        $dataUser = $this->UserModel->getUser($dataUser['id']);
        $dataUser['config'] = $this->UserConfigModel->getConfig($dataConfig['id']);
        $dataUser['action'] = $data['action'];
        return $response->getBody()->write(json_encode($dataUser));
    }
    public function delUser($request, $response, $args) {
        $this->UserConfigModel->delUserById($args['user']);
        $this->UserModel->delUserById($args['user']);
        return $response->getBody()->write(json_encode($args));
    }
}