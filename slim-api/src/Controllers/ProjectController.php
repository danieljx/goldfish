<?php
namespace App\Controllers;

use \Error;
use \Exception;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Slim\Interfaces\RouterInterface as Router;
use Slim\Http\UploadedFile as UploadedFile;
use Slim\Http\Stream;
use App\Models\ProjectModel;

class ProjectController extends Controller {

    public function __construct($c) {
        parent::__construct($c);
        $this->ProjectModel    = $this->getContainer()->get('ProjectModel');
    }
    public function getProjectSearch($request, $response, $args) {
        $this->q    = $request->getParam('q');
        $data       = $this->ProjectModel->getSearchProject($this->q);
        return $response->getBody()->write(json_encode($data));
    }
}