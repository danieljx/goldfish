<?php

namespace App\Controllers;

use League\Flysystem\Filesystem;
use League\Flysystem\MountManager;
use League\Flysystem\Adapter;
use LogicException;

class FilesSystem {
    protected $settings = [
        'local' => [
            'path' => null,
        ],
    ];
    protected $fs = null;
    protected $mounts;

    public function __construct(array $settings) {
        $this->settings = array_merge($this->settings, $settings);
        $mounts = [];
        if (!is_null($this->settings['local']['path'])) {
            $mounts['local'] = new Filesystem(new Adapter\Local($this->settings['local']['path']));
        }
        $this->mounts = new MountManager($mounts);
    }
    
    public function __get($prefix) {
        $this->fs = $this->mounts->getFilesystem($prefix);
        return $this;
    }

    public function getFlysystem() {
        return $this->mounts->getFilesystem('local');
    }

    public function mountLocal($path) {
        if (isset($this->settings[$path]['path'])) {
            $this->fs = $this->mounts->getFilesystem($path);
            return $this;
        }
        $this->fs = new Filesystem(new Adapter\Local($path));
        return $this;
    }
    
    public function mountFtp($host, $username = '', $password = '', array $opt = []) {
        if (isset($this->settings[$host]['host'])) {
            $this->fs = $this->mounts->getFilesystem($host);
            return $this;
        }
        $opts = array_merge($opt, [
            'host' => $host,
            'username' => $username,
            'password' => $password,
        ]);
        $this->fs = new Filesystem(new Adapter\Ftp($opts));
        return $this;
    }
    
    public function mountArchive($path) {
        if (!class_exists('League\Flysystem\ZipArchive\ZipArchiveAdapter')) {
            throw new LogicException('No adapter found to mount Zip Archive.');
        }
        $this->fs = new Filesystem(new \League\Flysystem\ZipArchive\ZipArchiveAdapter($path));
        return $this;
    }
    
    public function write($path, $contents, array $config = []) {
        $filesystem = $this->fs ?: $this->mounts;
        return $filesystem->write($path, $contents, $config);
    }
    
    public function writeStream($path, $contents, array $config = []) {
        $filesystem = $this->fs ?: $this->mounts;
        return $filesystem->writeStream($path, $contents, $config);
    }
    
    public function put($path, $contents, array $config = []) {
        $filesystem = $this->fs ?: $this->mounts;
        return $filesystem->put($path, $contents, $config);
    }
    
    public function putStream($path, $contents, array $config = []) {
        $filesystem = $this->fs ?: $this->mounts;
        return $filesystem->putStream($path, $contents, $config);
    }
    
    public function update($path, $contents, array $config = []) {
        $filesystem = $this->fs ?: $this->mounts;
        return $filesystem->update($path, $contents, $config);
    }
    
    public function updateStream($path, $contents, array $config = []) {
        $filesystem = $this->fs ?: $this->mounts;
        return $filesystem->updateStream($path, $contents, $config);
    }
    
    public function read($path) {
        $filesystem = $this->fs ?: $this->mounts;
        return $filesystem->read($path);
    }
    
    public function readStream($path) {
        $filesystem = $this->fs ?: $this->mounts;
        return $filesystem->readStream($path);
    }
    
    public function rename($path, $newpath) {
        $filesystem = $this->fs ?: $this->mounts;
        return $filesystem->rename($path, $newpath);
    }
    
    public function copy($path, $newpath) {
        $filesystem = $this->fs ?: $this->mounts;
        return $filesystem->copy($path, $newpath);
    }
    
    public function delete($path) {
        $filesystem = $this->fs ?: $this->mounts;
        return $filesystem->delete($path);
    }
    
    public function deleteDir($dirname) {
        $filesystem = $this->fs ?: $this->mounts;
        return $filesystem->deleteDir($dirname);
    }
    
    public function createDir($dirname, array $config = []) {
        $filesystem = $this->fs ?: $this->mounts;
        return $filesystem->createDir($dirname, $config);
    }
    
    public function listContents($directory = '', $recursive = false) {
        $filesystem = $this->fs ?: $this->mounts;
        return $filesystem->createDir($directory, $recursive);
    }
    
    public function getMimetype($path) {
        $filesystem = $this->fs ?: $this->mounts;
        return $filesystem->getMimetype($path);
    }
    
    public function getTimestamp($path) {
        $filesystem = $this->fs ?: $this->mounts;
        return $filesystem->getTimestamp($path);
    }
    
    public function getVisibility($path) {
        $filesystem = $this->fs ?: $this->mounts;
        return $filesystem->getVisibility($path);
    }
    
    public function getSize($path) {
        $filesystem = $this->fs ?: $this->mounts;
        return $filesystem->getSize($path);
    }

    public function setVisibility($path, $visibility) {
        $filesystem = $this->fs ?: $this->mounts;
        return $filesystem->setVisibility($path, $visibility);
    }

    public function getMetadata($path) {
        $filesystem = $this->fs ?: $this->mounts;
        return $filesystem->getMetadata($path);
    }
}