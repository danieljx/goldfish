<?php
namespace App\Controllers;

use \Error;
use \Exception;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Slim\Interfaces\RouterInterface as Router;
use Slim\Http\Stream;
use App\Models\FileModel;
use App\Models\FileTagsModel;
use App\Models\TagModel;

class TagController extends Controller {
  public function __construct($c) {
      parent::__construct($c);
      $this->lang             = $this->getContainer()->get('lang');
      $this->UserModel        = $this->getContainer()->get('UserModel');
      $this->fs               = $this->getContainer()->get('fs');
  }

  public function getSearchTags($request, $response, $args) {
        $TagsModel  = new TagModel();
        $name       = preg_replace('/[^0-9a-z-a-zA-ZÀ-ÿ]/', '', strtolower(trim($args['tag'])));
        $key        = preg_replace('~&([a-z]{1,2})(acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($name, ENT_QUOTES, 'UTF-8'));
        $tagData    = $TagsModel->searchTag($name, $key);
        $responseData = [
            "success" => true,
            "results" => []
        ];
        foreach ($tagData as $i => $tag) {
            $tag["value"]   = $tag["id"];
            $tag["text"]    = $tag["key"];
            $responseData["results"][] = $tag;
        }
        return $response->getBody()->write(json_encode($responseData));
  }
  public function getTags($request, $response, $args) {
      $TagsModel  = new TagModel();
      $name       = preg_replace('/[^0-9a-z-a-zA-ZÀ-ÿ]/', '', strtolower(trim(isset($args['tag'])?$args['tag']:'')));
      $key        = preg_replace('~&([a-z]{1,2})(acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($name, ENT_QUOTES, 'UTF-8'));
      $tagData    = $TagsModel->searchTag($name, $key);
      return $response->getBody()->write(json_encode($tagData));
  }
}
