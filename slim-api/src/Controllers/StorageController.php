<?php
namespace App\Controllers;

use \Error;
use \Exception;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class StorageController extends Controller {

    protected $fs;
    protected $StorageFileSystem;
    protected $StorageModel;

    public function __construct($c) {
        parent::__construct($c);
        $this->fs               = $this->getContainer()->get('fs');
        $this->StorageModel     = $this->getContainer()->get('StorageModel');    
    }

    public function setStorageMount($path) {
        $this->StorageFileSystem = $this->fs->mountLocal($path);
    }

    public function getStorageMount($path) {
        return $this->StorageFileSystem;
    }

    public function getStorageModel($path) {
        return $this->StorageModel;
    }
}