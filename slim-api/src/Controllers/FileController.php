<?php
namespace App\Controllers;

use \Error;
use \Exception;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Slim\Interfaces\RouterInterface as Router;
use Slim\Http\UploadedFile as UploadedFile;
use Slim\Http\Stream;
use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Local;
use League\Flysystem\MountManager;
use App\Models\UserModel;
use App\Models\ProjectModel;
use App\Models\StorageModel;
use App\Models\MountModel;
use App\Models\FileModel;
use App\Models\FileShareModel;
use App\Models\FileTagsModel;
use App\Models\TagModel;

class FileController extends Controller {

    protected $fs;
    protected $MimeTypeModel;
    protected $FileModel;
    protected $SharedModel;
    protected $UserModel;
    protected $pathHash;
    protected $pathRoot;
    protected $pathApp;
    protected $folderOrigin = '';
    protected $pathBase     = 'data';
    protected $pathFolder   = 'data';
    protected $partial      = [];
    protected $dataStorage  = [];
    protected $StorageModel;
    protected $StorageAdapter;
    protected $StorageFileSystem;
    protected $StorageManager;

    public function __construct($c) {
        parent::__construct($c);
        $this->lang             = $this->getContainer()->get('lang');
        $this->StorageModel     = $this->getContainer()->get('StorageModel');
        $this->MountModel       = $this->getContainer()->get('MountModel');
        $this->FileModel        = $this->getContainer()->get('FileModel');
        $this->MimeTypeModel    = $this->getContainer()->get('MimeTypeModel');
        $this->SharedModel      = $this->getContainer()->get('FileShareModel');
        $this->UserModel        = $this->getContainer()->get('UserModel');
        $this->pathRoot         = $this->getContainer()->get('settings')['pathRoot'];
        $this->pathApp          = $this->getContainer()->get('settings')['pathApp'];
        $this->pathData         = $this->getContainer()->get('settings')['pathData'];
        $this->pathDataDefault  = $this->getContainer()->get('settings')['pathDataDefault'];
        $this->fs               = $this->getContainer()->get('fs');
    }
    public function getFileNotification($request, $response, $args) {
      while (true) {
        $data = time();
        return $response
            ->withHeader("Content-Type", "text/event-stream")
            ->withHeader("Cache-Control", "no-cache")
            ->write("data: {$data}\n\n");
        sleep();
      }
    }
    private function checkFolderExist($userId) {
      $dataUser   = $this->UserModel->getUser($userId);
      $folderUser = $this->pathRoot . DIRECTORY_SEPARATOR . $this->pathApp . DIRECTORY_SEPARATOR . $this->pathData . DIRECTORY_SEPARATOR . $dataUser['email'];
      return file_exists($folderUser);
    }
    private function setupUserFiles($userId) {
      $dataUser       = $this->UserModel->getUser($userId);

      $folderUser     = $this->pathRoot . DIRECTORY_SEPARATOR . $this->pathApp . DIRECTORY_SEPARATOR . $this->pathData . DIRECTORY_SEPARATOR . $dataUser['email'];
      $folderDefault  = $this->pathRoot . DIRECTORY_SEPARATOR . $this->pathApp . DIRECTORY_SEPARATOR . $this->pathData . DIRECTORY_SEPARATOR . $this->pathDataDefault;
      if(!mkdir($folderUser)) {
          throw new Exception('CREATE_FOLDER');
      }

      $dataR = array();
      $dataR['user_id']      = $userId;
      $dataR['user_path']    = "home::" . $dataUser['email'];
      $dataR['available']    = 1;
      $dataR['last_checked'] = 1;
      $this->StorageModel->setStorage($dataR);
      $dataS = $this->StorageModel->toArray();

      $dataR = array();
      $dataR['storage_id']   = $dataS['id'];
      $dataR['root_id']      = 1;
      $dataR['user_id']      = $userId;
      $dataR['mount_point']  = DIRECTORY_SEPARATOR . $dataUser['email'] . DIRECTORY_SEPARATOR;
      $this->MountModel->setMount($dataR);
      $dataM = $this->StorageModel->toArray();

      $dataR = array();
      $dataR['name']             = '';
      $dataR['path']             = '';
      $dataR['path_hash']        = $this->getPathHash($dataUser['email']);
      $dataR['parent']           = -1;
      $dataR['size']             = 0;
      $dataR['mtime']            = 1;
      $dataR['storage_mtime']    = 0;
      $dataR['encrypted']        = 0;
      $dataR['storage_id']       = $dataS['id'];
      if($dataR['storage_mtime'] == 0) {
          $dataR['storage_mtime']  = 1;
      }
      $dataR['permissions']      = (int)65;
      $dataR['mimetype']         = (int)2;
      $FileModel = new FileModel();
      $FileModel->setFile($dataR);
      $dataBase = $FileModel->toArray();

      $this->prepareAllData($userId);

      $filesDir = scandir($folderDefault);
      foreach($filesDir as $keyDir => $valueDir) {
        if($valueDir != "." && $valueDir != "..") {
          $setPath = realpath($folderDefault . DIRECTORY_SEPARATOR . $valueDir);
          if(!is_dir($setPath)) {
            $this->setFileStorage($setPath, $dataBase['path_hash']);
          } else {
            $this->setFolderStorage($setPath, $dataBase['path_hash']);
          }
        }
      }
    }
    private function dataShareFile($file) {
        $FileModel  = new FileModel();
        $dataFile   = $FileModel->getFileByHash($file, true);
        $data       = $dataFile->toArray();
        if(count($data) < 1) {
          $dataFile = $FileModel->getFileByPath($file, true);
          $data     = $dataFile->toArray();
        }
        if(count($data) < 1) {
          $dataFile = $FileModel->getFileById($file, true);
          $data     = $dataFile->toArray();
        }
        if(!(count($data))) {
            throw new Exception('RESOURCE_NOT_FOUND');
        } else if(count($data)) {
          $StorageModel = new StorageModel();
          $dataStorage  = $StorageModel->getStorageDataById($data[0]['storage_id']);
          $FileModel = new FileModel();
          $FileModel->setStorage($dataStorage);
          $FileModel->setSharedModel($this->SharedModel);
          $FileModel->setUserModel($this->UserModel);
          $FileModel->setMimeTypeModel($this->MimeTypeModel);
          $data = $FileModel->getFile($file);
          $this->getFileMimeType($data);
          return $data;
        }
    }
    private function getFatherPermisionShared($dataFile, $idUser) {
        $FileModel  = new FileModel();
        $dataShare  = array();
        $whereArr   = array();
        $whereArr[] = ['id', '=', $dataFile['parent']];
        $data       = $FileModel->where($whereArr)->get();
        if(count($data)) {
          foreach($data as $x => $file) {
            $dataItem = $file->toArray();
            $FileShareModel = new FileShareModel();
            $dataShare = $FileShareModel->getFileShareByFile($dataItem['id'], $idUser);
            if(!(count($dataShare) > 0)) {
              $this->getFatherPermisionShared($dataItem, $idUser);
            }
          }
        }
        return $dataShare;
    }
    private function checkFatherShared($dataFile, $idUser) {
        $BoK = false;
        $FileModel  = new FileModel();
        $whereArr   = array();
        $whereArr[] = ['id', '=', $dataFile['parent']];
        $data       = $FileModel->where($whereArr)->get();
        if(count($data)) {
          foreach($data as $x => $file) {
            $dataItem = $file->toArray();
            $FileShareModel = new FileShareModel();
            $dataShare = $FileShareModel->getFileShareByFile($dataItem['id'], $idUser);
            $BoK = (count($dataShare) > 0);
            if(!$BoK) {
              $this->checkFatherShared($dataItem, $idUser);
            }
          }
        }
        return $BoK;
    }
    private function setShareBreadcrumb(&$breadcrumb) {
      $dataBreadcrumb       = array();
      $dataBreadcrumb[]     = array("id" => -1, "name" => $this->lang->getDefault('SHARED'), "path_hash" => '');
      array_splice($breadcrumb, 1, 0, $dataBreadcrumb);
    }
    public function getSharedFileDownload($request, $response, $args) {
        $this->prepareAllData($args['user-id']);
        $this->FileShareModel   = new FileShareModel();
        $shareHash = $args['path_hash'];
        if(!empty($shareHash)) {
            $data      = $this->dataShareFile($shareHash);
            if(!(count($data))) {
                throw new Exception('RESOURCE_NOT_FOUND');
            }
            $shareData = $this->FileShareModel->getFileShareByFile($data['id'], $args['user-id']);
            if(!(count($shareData))) {
                if(!$this->checkFatherShared($data, $args['user-id'])) {
                throw new Exception('RESOURCE_NOT_FOUND');
                }
            }
            $this->setShareBreadcrumb($data['breadcrumb']);
            $pathBase   = $this->getPathBase($this->getStoragePath($this->dataStorage["user_path"]));
            $realFile   = $pathBase . DIRECTORY_SEPARATOR . $data['path'];
            if(!file_exists($realFile)) {
                throw new Exception('RESOURCE_NOT_FOUND');
            }
            if($data["mimetype"] == 2) {
                return $response->getBody()->write(json_encode($data));
            } else {
                return $this->fileResponseDownload($response, $realFile, $data);
            }
        } else {
            throw new Exception('RESOURCE_NOT_FOUND');
        }
    }
    public function getSharedFileView($request, $response, $args) {
        $this->prepareAllData($args['user-id']);
        $this->FileShareModel   = new FileShareModel();
        $shareHash = $args['path_hash'];
        if(!empty($shareHash)) {
            $data      = $this->dataShareFile($shareHash);
            if(!(count($data))) {
                throw new Exception('RESOURCE_NOT_FOUND');
            }
            $shareData = $this->FileShareModel->getFileShareByFile($data['id'], $args['user-id']);
            if(!(count($shareData))) {
                if(!$this->checkFatherShared($data, $args['user-id'])) {
                throw new Exception('RESOURCE_NOT_FOUND');
                }
            }
            $this->setShareBreadcrumb($data['breadcrumb']);
            $pathBase   = $this->getPathBase($this->getStoragePath($this->dataStorage["user_path"]));
            $realFile   = $pathBase . DIRECTORY_SEPARATOR . $data['path'];
            if(!file_exists($realFile)) {
                throw new Exception('RESOURCE_NOT_FOUND');
            }
            if($data["mimetype"] == 2) {
                return $response->getBody()->write(json_encode($data));
            } else {
                return $this->fileResponseView($response, $realFile, $data);
            }
        } else {
            throw new Exception('RESOURCE_NOT_FOUND');
        }
    }
    public function getSharedFile($request, $response, $args) {
        $this->prepareAllData($args['user-id']);
        $data = array();
        $this->FileShareModel   = new FileShareModel();
        $shareHash = $request->getParam('share_hash');
        if(!empty($shareHash)) {
          $data      = $this->dataShareFile($shareHash);
          if(!(count($data))) {
              throw new Exception('RESOURCE_NOT_FOUND');
          }
          $shareData = $this->FileShareModel->getFileShareByFile($data['id'], $args['user-id']);
          if(!(count($shareData))) {
            if(!$this->checkFatherShared($data, $args['user-id'])) {
              throw new Exception('RESOURCE_NOT_FOUND');
            }
          }
          $this->setShareBreadcrumb($data['breadcrumb']);
        } else {
          $shareData = $this->FileShareModel->getFileShareByUser($args['user-id']);
          $data['id']               = -1;
          $data['name']             = 'shared';
          $data['path']             = 'shared';
          $data['path_hash']        = 'shared/';
          $data['parent']           = -1;
          $data['size']             = 0;
          $data['mtime']            = 0;
          $data['storage_mtime']    = 0;
          $data['encrypted']        = false;
          $data['storage_id']       = $this->dataStorage["id"];
          if($data['storage_mtime'] == 0) {
            $data['storage_mtime']  = 1;
          }
          $data['permissions']      = 65;
          $data['mimetype']         = 2;
          $data['date_changed']     = date("Y-m-d H:i:s");
          $data['date_created']     = date("Y-m-d H:i:s");
          $data['breadcrumb']       = array();
          $data['breadcrumb'][]     = array("id" => -1, "name" => 'Home', "path_hash" => '');
          $data['breadcrumb'][]     = array("id" => -1, "name" => $this->lang->getDefault('SHARED'), "path_hash" => '');
          $data['children']         = array();
          $data['shared_user']      = array();
          foreach($shareData as $share) {
            $dataf  = $this->dataShareFile($share['file_id']);
            $dataf['data_share'] = $share;
            $this->getFileMimeType($dataf);
            $data['children'][] = $dataf;
          }
        }
        if(count($data['children'])) {
          foreach($data['children'] as $x => $child) {
            if(count($data['children'][$x]['shared_user'])) {
              foreach($data['children'][$x]['shared_user'] as $y => $share) {
                if($data['children'][$x]['shared_user'][$y]['shared_id'] == $args['user-id']) {
                  $data['children'][$x]['shared'] = $data['children'][$x]['shared_user'][$y];
                  unset($data['children'][$x]['shared_user'][$y]);
                }
              }
            } else {
              $data['children'][$x]['shared'] = $this->getFatherPermisionShared($data['children'][$x], $args['user-id']);
            }
          }
        }
        if(count($data['shared_user'])) {
          foreach($data['shared_user'] as $z => $share) {
            if($data['shared_user'][$z]['shared_id'] == $args['user-id']) {
              $data['shared'] = $data['shared_user'][$z];
              unset($data['shared_user'][$z]);
            }
          }
        }
        return $response->getBody()->write(json_encode($data));
    }
    private function setPublicBreadcrumb(&$breadcrumb) {
      $dataBreadcrumb       = array();
      $dataBreadcrumb[]     = array("id" => -2, "name" => $this->lang->getDefault('PUBLIC'), "path_hash" => '');
      array_splice($breadcrumb, 1, 0, $dataBreadcrumb);
    }
    public function getPublicFileDownload($request, $response, $args) {
        $this->prepareAllData(1);
        $data = array();
        $data = $this->FileModel->getFile($args['path_hash']);
        if(!(count($data))) {
            throw new Exception('RESOURCE_NOT_FOUND');
        }
        $pathBase   = $this->getPathBase($this->getStoragePath($this->dataStorage["user_path"]));
        $realFile   = $pathBase . DIRECTORY_SEPARATOR . $data['path'];
        if(!file_exists($realFile)) {
            throw new Exception('RESOURCE_NOT_FOUND');
        }
        if($data["mimetype"] == 2) {
            return $response->getBody()->write(json_encode($data));
        } else {
            return $this->fileResponseView($response, $realFile, $data);
        }
    }
    public function getPublicFileView($request, $response, $args) {
        $this->prepareAllData(1);
        $data = array();
        $data = $this->FileModel->getFile($args['path_hash']);
        if(!(count($data))) {
            throw new Exception('RESOURCE_NOT_FOUND');
        }
        $pathBase   = $this->getPathBase($this->getStoragePath($this->dataStorage["user_path"]));
        $realFile   = $pathBase . DIRECTORY_SEPARATOR . $data['path'];
        if(!file_exists($realFile)) {
            throw new Exception('RESOURCE_NOT_FOUND');
        }
        if($data["mimetype"] == 2) {
            return $response->getBody()->write(json_encode($data));
        } else {
            return $this->fileResponseDownload($response, $realFile, $data);
        }
    }
    public function getPublicFile($request, $response, $args) {
        $this->prepareAllData(1);
        $data = array();
        $publicHash   = $request->getParam('public_hash');
        if(!empty($publicHash)) {
            $data = $this->FileModel->getFile($publicHash);
            $this->getFileMimeType($data);
            $this->setPublicBreadcrumb($data['breadcrumb']);
            if(!(count($data))) {
                throw new Exception('RESOURCE_NOT_FOUND');
            }
        } else {
            $data['id']               = -2;
            $data['name']             = 'public';
            $data['path']             = 'public';
            $data['path_hash']        = 'public/';
            $data['parent']           = -2;
            $data['size']             = 0;
            $data['mtime']            = 0;
            $data['storage_mtime']    = 0;
            $data['encrypted']        = false;
            $data['storage_id']       = $this->dataStorage["id"];
            if($data['storage_mtime'] == 0) {
                $data['storage_mtime']  = 1;
            }
            $data['permissions']      = 65;
            $data['mimetype']         = 2;
            $data['date_changed']     = date("Y-m-d H:i:s");
            $data['date_created']     = date("Y-m-d H:i:s");
            $data['breadcrumb']       = array();
            $data['breadcrumb'][]     = array("id" => -2, "name" => 'Home', "path_hash" => '');
            $data['breadcrumb'][]     = array("id" => -2, "name" => $this->lang->getDefault('PUBLIC'), "path_hash" => '');
            $data['children']         = array();
            $data['shared_user']      = array();
            $dataPublic               = $this->FileModel->getFile("files");
            if(count($dataPublic['children'])) {
                foreach($dataPublic['children'] as $x => $child) {
                    if(($dataPublic['children'][$x]['name'] == 'public' || $dataPublic['children'][$x]['name'] == 'shared')) {
                        unset($dataPublic['children'][$x]);
                    }
                }
            }
            $data['children'] = array_values($dataPublic['children']);
            $this->getFileMimeType($data);
        }
        return $response->getBody()->write(json_encode($data, true));
    }
    public function setTagsFile($request, $response, $args) {
        $this->prepareAllData($args['user-id']);
        $dataPost     = $request->getParsedBody();
        $tagsPost     = $dataPost['tags'];
        $tagsResponse = array();
        foreach($tagsPost as $tag) {
          $tagData = array();
          $this->TagsModel  = new TagModel();
          $tag['name']      = preg_replace('/[^0-9a-z-a-zA-ZÀ-ÿ]/', '', strtolower(trim($tag['name'])));
          $tag['key']       = preg_replace('~&([a-z]{1,2})(acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($tag['name'], ENT_QUOTES, 'UTF-8'));
          if(isset($tag['tag_id'])) {
            $tagData = $this->TagsModel->getTag($tag['tag_id']);
          }
          if(!(count($tagData) > 0)) {
            $tagData = $this->TagsModel->getTagName($tag['name']);
            if(!(count($tagData) > 0)) {
              $tagData = $this->TagsModel->setTag($tag);
            }
          }
          $fileTag = array();
          $this->FileTagsModel   = new FileTagsModel();
          if($tag['tag_action'] == 'add') {
              $fileTag['file_id'] = $dataPost['id'];
              $fileTag['tag_id']  = $tagData['id'];
              $tagData = $this->FileTagsModel->setTags($fileTag);
          } else if($tag['tag_action'] == 'del') {
              $fileTag['id']      = $tag['id'];
              $fileTag['file_id'] = $tag['file_id'];
              $fileTag['tag_id']  = $tag['tag_id'];
              $this->FileTagsModel->delTags($fileTag);
          }
          //if(count($tagData)) {
            if($tag['tag_action'] == 'add') {
              $tag['id']         = $tagData['id'];
              $tag['file_id']    = $tag['file_id'];
              $tag['tag_id']     = $tagData['tag_id'];
              $tag['tag_action'] = 'up';
              $tagsResponse[] = $tag;
            } else if($tag['tag_action'] == 'up') {
              $tagsResponse[] = $tag;
            }
          //}
        }
        $dataPost['tags'] = $tagsResponse;
        return $response->getBody()->write(json_encode($dataPost));
    }
    public function setSharedFile($request, $response, $args) {
        $this->prepareAllData($args['user-id']);
        $dataPost       = $request->getParsedBody();
        $sharedPost     = $dataPost['shared_user'];
        $shareResponse  = array();
        foreach($sharedPost as $share) {
            $shareData              = array();
            $share['owner_id']      = (isset($share['owner_id'])?$share['owner_id']:$args['user-id']);
            $share['initiator_id']  = $args['user-id'];
            $share['share_hash']    = $this->getPathHash($this->pathBase . DIRECTORY_SEPARATOR . 'shared' . DIRECTORY_SEPARATOR . $dataPost['path']);
            $this->FileShareModel   = new FileShareModel();
            if($share['share_action'] == 'add') {
                $shareData = $this->FileShareModel->setShare($share);
            } else if($share['share_action'] == 'up') {
                $shareData = $this->FileShareModel->updateShare($share);
            } else if($share['share_action'] == 'del') {
                $this->FileShareModel->delShare($share);
            }
            if(count($shareData)) {
                $this->UserModel    = new UserModel();
                $this->ProjectModel = new ProjectModel();
                if(!isset($shareData["shared_id"])) {
                    die(var_dump($shareData));
                }
                if($shareData["share_type"] == '1') {
                    $shareUser  = $this->UserModel->where([['id', '=', $shareData["shared_id"]]])->get();
                    $shareData['share_user']     = (count($shareUser)?$shareUser[0]:[]);
                    $shareData['share_project']  = array();
                } else {
                    $shareProject  = $this->ProjectModel->where([['id', '=', $shareData["shared_id"]]])->get();
                    $shareData['share_project']  = (count($shareProject)?$shareProject[0]:[]);
                    $shareData['share_user']     = array();
                }
                $shareOwner = $this->UserModel->where([['id', '=', $shareData["owner_id"]]])->get();
                $shareData['share_owner']     = (count($shareOwner)?$shareOwner[0]:[]);
                $shareInit  = $this->UserModel->where([['id', '=', $shareData["initiator_id"]]])->get();
                $shareData['share_initiator'] = (count($shareInit)?$shareInit[0]:[]);
                $shareData['share_action']  = 'up';
                $shareResponse[] = $shareData;
            }
        }
        $dataPost['shared_user'] = $shareResponse;
        return $response->getBody()->write(json_encode($dataPost));
    }
    public function getGalleryFile($request, $response, $args) {
        $this->prepareAllData($args['user-id']);
        $this->q        = rtrim(ltrim((isset($args['file'])?$args['file']:"")));
        $dataMe = $this->FileModel->getSearchFileGallery($this->q);
        $dataResp = [];
        foreach($dataMe as $fileMe) {
            $fileMe['color'] = 'blue';
            $fileMe['view']  = '';
            $this->getFileMimeType($fileMe);
            $dataResp[] = $fileMe;
        }
        $dataShared = $this->FileModel->getSearchSharedFileGallery($args['user-id'], $this->q);
        foreach($dataShared as $fileShared) {
            $fileShared['color'] = 'yellow';
            $fileShared['view']  = 'shared';
            $dataResp[] = $fileShared;
        }
        $this->prepareAllData(1);
        $dataPublic = $this->FileModel->getSearchFileGallery($this->q);
        foreach($dataPublic as $filePublic) {
            $filePublic['color'] = 'green';
            $filePublic['view']  = 'public';
            $dataResp[] = $filePublic;
        }
        return $response->getBody()->write(json_encode($dataResp));
    }
    public function getFileSearch($request, $response, $args) {
        $this->prepareAllData($args['user-id']);
        $this->q        = rtrim(ltrim($args['file']));
        $dataMe = $this->FileModel->getSearchFile('files', $this->q);
        $dataResp = [
            "ME"        => ["name" => $this->lang->getDefault('ME'), "results" => []],
            "SHARED"    => ["name" => $this->lang->getDefault('SHARED'), "results" => []],
            "PUBLIC"    => ["name" => $this->lang->getDefault('PUBLIC'), "results" => []]
        ];
        foreach($dataMe as $fileMe) {
            $this->getFileMimeType($fileMe);
            $dataResp["ME"]["results"][] = [
                "title"     => str_replace('/',' / ',str_replace('files/', $this->lang->getDefault('FILES') . '/', $fileMe["path"])),
                "icon"      => $fileMe["icon"],
                "mimetype"  => $fileMe["mimetype"],
                "path"      => $fileMe["path"],
                "path_hash" => $fileMe["path_hash"],
                "breadcrumb" => $fileMe["breadcrumb"],
                "sharedView"=> false,
                "publicView"=> false
            ];
        }
        $dataShared = $this->FileModel->getSearchSharedFile($args['user-id'], $this->q);
        foreach($dataShared as $fileShared) {
            $this->getFileMimeType($fileShared);
            $dataResp["SHARED"]["results"][] = [
                "title"     => str_replace('/',' / ',str_replace('files/', $this->lang->getDefault('FILES') . '/' . $this->lang->getDefault('SHARED') . '/', $fileShared["path"])),
                "icon"      => $fileShared["icon"],
                "mimetype"  => $fileShared["mimetype"],
                "path"      => $fileShared["path"],
                "path_hash" => $fileShared["path_hash"],
                "breadcrumb" => $fileShared["breadcrumb"],
                "sharedView"=> true,
                "publicView"=> false
            ];
        }
        $this->prepareAllData(1);
        $dataPublic = $this->FileModel->getSearchPublicFile($this->q);
        foreach($dataPublic as $filePublic) {
            $this->getFileMimeType($filePublic);
            $dataResp["PUBLIC"]["results"][] = [
                "title"     => str_replace('/',' / ',str_replace('files/', $this->lang->getDefault('FILES') . '/' . $this->lang->getDefault('PUBLIC') . '/', $filePublic["path"])),
                "icon"      => $filePublic["icon"],
                "mimetype"  => $filePublic["mimetype"],
                "path"      => $filePublic["path"],
                "path_hash" => $filePublic["path_hash"],
                "breadcrumb" => $filePublic["breadcrumb"],
                "sharedView"=> false,
                "publicView"=> true
            ];
        }
        $dataResp = ["results" => $dataResp];
        return $response->getBody()->write(json_encode($dataResp));
    }
    public function getFileReds($request, $response, $args) {
        $data               = array();
        $this->prepareAllData($args['user-id']);
        $data = $this->FileModel->getFile($args['path_hash']);
        if(!(count($data))) {
            throw new Exception('RESOURCE_NOT_FOUND');
        }
        $this->path = $data['path'] . DIRECTORY_SEPARATOR . $args['path'];
        $data = $this->FileModel->getFile($this->path);
        if(!(count($data))) {
            throw new Exception('RESOURCE_NOT_FOUND');
        }
        return $response->getBody()->write(json_encode($data));
    }
    public function getFile($request, $response, $args) {
        if(!$this->checkFolderExist($args['user-id'])) {
          $this->setupUserFiles($args['user-id']);
        }
        $this->prepareAllData($args['user-id']);
        $userPathBase   = $this->storageName . DIRECTORY_SEPARATOR . 'files';
        $this->pathHash = (!empty($request->getParam('path_hash'))?$request->getParam('path_hash'):'files');
        $data = $this->FileModel->getFile($this->pathHash);
        $this->getFileMimeType($data);
        if(count($data['children'])) {
            foreach ($data['children'] as $k => $fileChild) {
              if($data['children'][$k]['name'] == 'public') {
                $data['children'][$k]['name'] = $this->lang->getDefault('PUBLIC');
                $data['children'][$k]['icon'] = 'folder-public';
              }
              if($data['children'][$k]['name'] == 'shared') {
                $data['children'][$k]['name'] = $this->lang->getDefault('SHARED');
                $data['children'][$k]['icon'] = 'folder-shared';
              }
            }
        }
        if(!(count($data))) {
            throw new Exception('RESOURCE_NOT_FOUND');
        }
        return $response->getBody()->write(json_encode($data));
    }
    public function setFolder($request, $response, $args) {
        $data               = array();
        $this->prepareAllData($args['user-id']);
        $dataPost           = $request->getParsedBody();
        $dataPost["storage_id"] = $this->dataStorage["id"];
        $dataParent = $this->FileModel->getFile($dataPost["parent"]);
        if(is_dir($this->pathBase)) {
            $dataFile = $this->createFolder($this->pathBase, $dataPost['name'], $dataParent);
            $dataFile = $dataPost + $dataFile;
            $dataFile['path_hash']  = $this->getPathHash($this->pathBase . DIRECTORY_SEPARATOR . $dataParent['path'] . DIRECTORY_SEPARATOR . $dataPost['name']);
            $this->FileModel->setFile($dataFile);
            $data = $this->FileModel->toArray();
            $data = $this->FileModel->getFile($data['path_hash']);
            $this->getFileMimeType($data);
        } else {
            throw new Exception('RESOURCE_NOT_FOUND');
        }
        return $response->getBody()->write(json_encode($data));
    }
    public function delFile($request, $response, $args) {
        $data = array();
        $this->prepareAllData($args['user-id']);
        $this->pathHash = $request->getParam('path_hash');
        if(empty($this->pathHash)) {
            throw new Exception('RESOURCE_NOT_FOUND');
        }
        $data = $this->FileModel->getFile($this->pathHash);
        if(!(count($data)) || !$data) {
            throw new Exception('RESOURCE_NOT_FOUND');
        }
        $delFile = $this->pathBase . DIRECTORY_SEPARATOR . $data['path'];
        if(file_exists($delFile)) {
            if($this->deleteFile($delFile)) {
                $this->FileModel->delFile($data);
            }
        } else {
            throw new Exception('RESOURCE_NOT_FOUND');
        }
        return $response->getBody()->write(json_encode($data));
    }
    public function remFile($request, $response, $args) {
        $data = array();
        $this->prepareAllData($args['user-id']);
        $this->pathHash = $request->getParam('path_hash');
        $this->path     = $request->getParam('path');
        $this->name     = $request->getParam('name');
        if(empty($this->pathHash)) {
            throw new Exception('RESOURCE_NOT_FOUND');
        }
        $data       = $this->FileModel->getFile($this->pathHash);
        if(!(count($data)) || !$data) {
            throw new Exception('RESOURCE_NOT_FOUND');
        }
        $oldNameFile = $this->pathBase . DIRECTORY_SEPARATOR . $data['path'];
        $newNameFile = $this->pathBase . DIRECTORY_SEPARATOR . $this->path;
        if(file_exists($oldNameFile)) {
            if($this->renameFile($oldNameFile, $newNameFile)) {
                $data['path']       = $this->path;
                $data['name']       = $this->name;
                $data['path_hash']  = $this->getPathHash($this->pathBase . DIRECTORY_SEPARATOR . $this->path);
                if($data['mimetype'] == 2) {
                    $this->FileModel->updateFolder($data);
                } else {
                    $this->getFileMimeTypeExt($data);
                    $this->FileModel->updateFile($data);
                }
                $this->prepareAllData($args['user-id']);
                $data = $this->FileModel->getFile($data['path_hash']);
                $this->getFileMimeType($data);
            }
        } else {
            throw new Exception('RESOURCE_NOT_FOUND');
        }
        return $response->getBody()->write(json_encode($data));
    }
    public function setUploadFile($request, $response, $args) {
        $data = array();
        $this->prepareAllData($args['user-id']);
        $uploadedFiles 	= $request->getUploadedFiles();
        $this->pathHash = $request->getParam('path_hash');
        $data           = $this->FileModel->getFile($this->pathHash);
        if(!(count($data)) || !$data) {
            throw new Exception('RESOURCE_NOT_FOUND');
        }
        $dataFile   = $this->moveUploadedFile($this->pathBase, $data['path'],  $data, $uploadedFiles['file']);
        $this->FileModel->setFile($dataFile);
        $data = $this->FileModel->toArray();
        $data = $this->FileModel->getFile($data['path_hash']);
        $this->getFileMimeTypeExt($data);
        return $response->getBody()->write(json_encode($data));
    }
    public function getFileViewReds($request, $response, $args) {
        $data = array();
        $this->prepareAllData($args['user-id']);
        $data = $this->FileModel->getFile($args['path_hash']);
        if(!(count($data))) {
            throw new Exception('RESOURCE_NOT_FOUND');
        }
        $this->path = $data['path'] . DIRECTORY_SEPARATOR . $args['path'];
        $data = $this->FileModel->getFile($this->path);
        if(!(count($data))) {
            throw new Exception('RESOURCE_NOT_FOUND');
        }
        $pathBase   = $this->getPathBase($this->getStoragePath($this->dataStorage["user_path"]));
        $realFile   = $pathBase . DIRECTORY_SEPARATOR . $data['path'];
        if(!file_exists($realFile)) {
            throw new Exception('RESOURCE_NOT_FOUND');
        }
        if($data["mimetype"] == 2) {
            return $response->getBody()->write(json_encode($data));
        } else {
            return $this->fileResponseView($response, $realFile, $data);
        }
    }
    public function getFileDownloadReds($request, $response, $args) {
        $data = array();
        $this->prepareAllData($args['user-id']);
        $data = $this->FileModel->getFile($args['path_hash']);
        if(!(count($data))) {
            throw new Exception('RESOURCE_NOT_FOUND');
        }
        $this->path = $data['path'] . DIRECTORY_SEPARATOR . $args['path'];
        $data = $this->FileModel->getFile($this->path);
        if(!(count($data))) {
            throw new Exception('RESOURCE_NOT_FOUND');
        }
        $pathBase   = $this->getPathBase($this->getStoragePath($this->dataStorage["user_path"]));
        $realFile   = $pathBase . DIRECTORY_SEPARATOR . $data['path'];
        if(!file_exists($realFile)) {
            throw new Exception('RESOURCE_NOT_FOUND');
        }
        if($data["mimetype"] == 2) {
            return $response->getBody()->write(json_encode($data));
        } else {
            return $this->fileResponseDownload($response, $realFile, $data);
        }
    }
    public function getFileView($request, $response, $args) {
        $this->pathHash     = $args["path_hash"];
        $this->dataStorage 	= $this->getStorageData($args['user-id']);
        if(count($this->dataStorage)) {
            $this->FileModel->setStorage($this->dataStorage);
            $data = $this->FileModel->getFile($this->pathHash);
            $this->getFileMimeType($data);
            if(!(count($data))) {
                throw new Exception('RESOURCE_NOT_FOUND');
            }
            $pathBase   = $this->getPathBase($this->getStoragePath($this->dataStorage["user_path"]));
            $realFile = $pathBase . DIRECTORY_SEPARATOR . $data['path'];
            if(!file_exists($realFile)) {
                throw new Exception('RESOURCE_NOT_FOUND');
            }
        } else {
            throw new Exception('STORAGE_NOT_FOUND');
        }
        if($data["mimetype"] == 2) {
            return $response->getBody()->write(json_encode($data));
        } else {
            return $this->fileResponseView($response, $realFile, $data);
        }
    }
    public function getFilesDownload($request, $response, $args) {
        $this->arrayPathHash     = $request->getParam('array_path_hash');
        die(var_dump($this->arrayPathHash));
    }
    public function getFileDownload($request, $response, $args) {
        $this->pathHash     = $args["path_hash"];
        $this->dataStorage 	= $this->getStorageData($args['user-id']);
        if(count($this->dataStorage)) {
            $this->FileModel->setStorage($this->dataStorage);
            $data = $this->FileModel->getFile($this->pathHash);
            $this->getFileMimeType($data);
            if(!(count($data))) {
                throw new Exception('RESOURCE_NOT_FOUND');
            }
            $pathBase   = $this->getPathBase($this->getStoragePath($this->dataStorage["user_path"]));
            $realFile = $pathBase . DIRECTORY_SEPARATOR . $data['path'];
            if(!file_exists($realFile)) {
                throw new Exception('RESOURCE_NOT_FOUND');
            }
        } else {
            throw new Exception('STORAGE_NOT_FOUND');
        }
        if($data["mimetype"] == 2) {
            return $response->getBody()->write(json_encode($data));
        } else {
            return $this->fileResponseDownload($response, $realFile, $data);
        }
    }
    private function uploadFile($oldNameFile = "", $newNameFile = "") {
        if(!rename($oldNameFile, $newNameFile)) {
            throw new Exception('RENAME_FILE');
        }
        return true;
    }
    private function fileResponseView(Response $response, $fileName, $data = array()) {
        if ($fd = fopen ($fileName, "r")) {
            $ext      = pathinfo($data["name"], PATHINFO_EXTENSION);
            $response = $response->withHeader("Content-type", $this->MimeTypeModel->getContentTypeExt("." . $ext));
            $response = $response->withHeader("Content-Disposition", 'filename="' . $data['name'] . '"');
            $response = $response->withHeader("Cache-control", "private");
            $response = $response->withHeader("Content-length", $this->getFileSize($fileName));
            $stream   = new Stream($fd);
            $response = $response->withBody($stream);
            return $response;
        } else {
            throw new Exception('RESOURCE_NOT_OPEN');
        }
    }
    private function fileResponseDownload(Response $response, $fileName, $data = array()) {
        if ($fd = fopen ($fileName, "r")) {
            $ext      = pathinfo($data["name"], PATHINFO_EXTENSION);
            $response = $response->withHeader("Pragma","public");
            $response = $response->withHeader("Expires", "0");
            $response = $response->withHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
            $response = $response->withHeader("Content-Type", "application/force-download");
            $response = $response->withHeader("Content-Type", "application/octet-stream");
            $response = $response->withHeader("Content-Type", "application/download");
            $response = $response->withHeader("Content-Disposition", "attachment;filename=" . $data['name']);
            $response = $response->withHeader("Content-Transfer-Encoding", "binary");
            $response = $response->withHeader("Content-length", $this->getFileSize($fileName));
            $stream   = new Stream($fd);
            $response = $response->withBody($stream);
            return $response;
        } else {
            throw new Exception('RESOURCE_NOT_OPEN');
        }
    }
    private function moveUploadedFile($pathBase, $pathFolder, $dataParent, UploadedFile $uploadedFile) {
        $filePath = $pathBase . DIRECTORY_SEPARATOR . $pathFolder . DIRECTORY_SEPARATOR . $uploadedFile->getClientFilename();
        $uploadedFile->moveTo($filePath);
        $dataR = array();
        $dataR['name']             = $uploadedFile->getClientFilename();
        $dataR['path']             = $pathFolder . DIRECTORY_SEPARATOR . $uploadedFile->getClientFilename();
        $dataR['path_hash']        = $this->getPathHash($filePath);
        $dataR['parent']           = (int)$dataParent['id'];
        $dataR['size']             = $this->getFileSize($filePath);
        $dataR['storage_id']       = $this->dataStorage["id"];
        $this->getFileMimeTypeExt($dataR);
        $dataR['mtime']            = 1;
        $dataR['storage_mtime']    = 0;
        $dataR['encrypted']        = 0;
        if($dataR['storage_mtime'] == 0) {
            $dataR['storage_mtime']  = 1;
        }
        $dataR['permissions']      = (int)65;
        return $dataR;
    }
    private function reNameFile($oldNameFile = "", $newNameFile = "") {
        if(!rename($oldNameFile, $newNameFile)) {
            throw new Exception('RENAME_FILE');
        }
        return true;
    }
    private function createFolder($pathBase = "", $folder = "", $dataParent = array()) {
        $newFolder = $pathBase . DIRECTORY_SEPARATOR . $dataParent['path'] . DIRECTORY_SEPARATOR . $folder;
        if(!file_exists($newFolder)) {
            if(!mkdir($newFolder)) {
                throw new Exception('CREATE_FOLDER');
            } else {
                return $this->getInfoFile($newFolder, $pathBase, $folder, $dataParent, true);
            }
        } else {
            throw new Exception('FOLDER_EXIST');
        }
    }
    private function getInfoFile($realPath = "", $pathFolder = "", $file = "", $dataParent = array(), $folder = false) {
        $dataR = array();
        $dataR['path']             = $file;
        $dataR['path_hash']        = $this->getPathHash($pathFolder . DIRECTORY_SEPARATOR . $file);
        $dataR['parent']           = (int)$dataParent['id'];
        $dataR['size']             = ($folder?$this->getDirSize($realPath):$this->getFileSize($realPath));
        $dataR['mtime']            = 1;
        $dataR['storage_mtime']    = 0;
        $dataR['encrypted']        = 0;
        $dataR['storage_id']       = $this->dataStorage["id"];
        if($dataR['storage_mtime'] == 0) {
            $dataR['storage_mtime']  = 1;
        }
        $dataR['permissions']      = (int)65;
        $dataR['mimetype']         = (int)1;
        return $dataR;
    }
    private function getPathHash($path = "") {
        return md5($path);
    }
    private function getStorageData($userId = "") {
      return $this->StorageModel->getStorageData($userId);
    }
    private function getStoragePath($pathStorage = "") {
        $path = explode("::",$pathStorage);
        return (count($path)?$path[1]:$pathStorage);
    }
    private function getPathBase($pathStorage = "") {
        return $this->pathRoot . DIRECTORY_SEPARATOR . $this->pathApp . DIRECTORY_SEPARATOR . $this->pathFolder . DIRECTORY_SEPARATOR . $pathStorage;
    }
    private function getFileSize($filePath = "") {
        return filesize($filePath);
    }
    private function getDirSize($dirPath) {
        @$dh = opendir($dirPath);
        $size = 0;
        while ($file = @readdir($dh)) {
            if ($file != "." and $file != "..") {
                $path = $dirPath . DIRECTORY_SEPARATOR . $file;
                if (is_dir($path)) {
                    $size += $this->getDirSize($path);
                } else if (is_file($path)) {
                    $size += filesize($path);
                }
            }
        }
        @closedir($dh);
        return $size;
    }
    private function deleteFile($dir) {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (is_dir($dir . DIRECTORY_SEPARATOR . $object)) {
                        $this->deleteFile($dir . DIRECTORY_SEPARATOR . $object);
                    } else {
                        if(!unlink($dir  . DIRECTORY_SEPARATOR . $object)) {
                            throw new Exception('NOT_DELETE_FILE');
                        }
                    }
                }
            }
            if(!rmdir($dir)) {
                throw new Exception('NOT_DELETE_FOLDER');
            }
        } else if(is_file($dir)) {
            if(!unlink($dir)) {
                throw new Exception('NOT_DELETE_FILE');
            }
        }
        return true;
    }
    public function getFileMimeType(&$data = array()) {
        $dataF = $this->MimeTypeModel->getMimeTypeData($data["mimetype"]);
        $data['icon'] = (count($dataF)?$dataF['icon']:"file");
        if(isset($data['children']) && count($data['children'])) {
            foreach($data['children'] as $k => $file) {
                $dataM = $this->MimeTypeModel->getMimeTypeData($file["mimetype"]);
                $data['children'][$k]['icon'] = (count($dataM)?$dataM['icon']:"file");
            }
        } else {
            $data['children'] = array();
        }
    }
    public function getFileMimeTypeExt(&$data = array()) {
        $extension = pathinfo($data["name"], PATHINFO_EXTENSION);
        if(!empty($extension)) {
            $ext = "." . $extension;
            $dataE = $this->MimeTypeModel->getMimeTypeExtData($ext);
            $data['mimetype'] = (count($dataE)?$dataE['id']:3);
            $data['icon'] = (count($dataE)?$dataE['icon']:"file");
        }
    }
    private function prepareAllData($userId = "") {
        if(!empty($userId)) {
            $this->userId       = $userId;
            $this->dataStorage  = $this->getStorageData($userId);
            if(count($this->dataStorage)) {
                $this->storageName      = $this->getStoragePath($this->dataStorage["user_path"]);
                $this->pathBase         = $this->getPathBase($this->storageName);
                $this->StorageAdapter   = new Local($this->pathBase);
                $this->StorageFileSystem= new Filesystem($this->StorageAdapter);
                $this->StorageManager   = new MountManager();
                $this->FileModel        = new FileModel();
                $this->StorageManager->mountFilesystem($this->storageName, $this->StorageFileSystem);
                $this->FileModel->setStorage($this->dataStorage);
                $this->FileModel->setSharedModel($this->SharedModel);
                $this->FileModel->setUserModel($this->UserModel);
                $this->FileModel->setMimeTypeModel($this->MimeTypeModel);
            } else {
                throw new Exception('STORAGE_NOT_FOUND');
            }
        } else {
            throw new Exception('USER_NOT_FOUND');
        }
    }
    public function setUserStorage($userId = "") {
        $this->prepareAllData($userId);
    }
    public function getStorageManager() {
        return $this->StorageManager;
    }
    public function getStorageFileSystem() {
        return $this->StorageFileSystem;
    }
    public function hasFileDataStorage($path) {
        $this->prepareAllData($this->userId);
        $data = $this->FileModel->getFile($path);
        return (count($data) > 0 && $this->StorageFileSystem->has($data['path']));
    }
    public function getFileDataStorage($path = "files") {
        $this->prepareAllData($this->userId);
        $data = $this->FileModel->getFile($path);
        return [
            'data'    => $data,
            'storage' => $this->StorageFileSystem->listContents($data['path'], true)
        ];
    }
    private function getRenameExistFile($path, $file, $start = "_", $end = "") {
        $realPath = $this->pathBase . DIRECTORY_SEPARATOR . $path . DIRECTORY_SEPARATOR . $file;
        if(file_exists($realPath)) {
            $dataFile   = pathinfo($realPath);
            $files = $this->StorageFileSystem->listContents($path, true);
            $i = 0;
            foreach ($files as $object) {
                if(strpos($object['filename'], $dataFile['filename']) !== false) {
                    $i++;
                }
            }
            $file = $dataFile['filename'] . $start . $i . $end . (is_dir($realPath)?"":"." . $dataFile['extension']);
        }
        return $file;
    }
    public function setFileStorage($pathFile, $pathHashDes = "files", $rename = false, $start = "_", $end = "") {
        $this->prepareAllData($this->userId);
        $pathParts  = pathinfo($pathFile);
        $stream     = fopen($pathFile, 'r+');
        $dataDes    = $this->FileModel->getFile($pathHashDes);
        if((!(count($dataDes)) || !$dataDes)) {
            throw new Exception('RESOURCE_NOT_FOUND');
        }
        $this->path  = (empty($dataDes['path'])?$pathParts['basename']:$dataDes['path'] . DIRECTORY_SEPARATOR . $pathParts['basename']);
        if($rename && $this->hasFileDataStorage($this->path)) {
            $pathParts['basename'] = $this->getRenameExistFile($dataDes['path'], $pathParts['basename'], $start, $end);
            $this->path  = (empty($dataDes['path'])?$pathParts['basename']:$dataDes['path'] . DIRECTORY_SEPARATOR . $pathParts['basename']);
        }
        $this->StorageFileSystem->writeStream($this->path, $stream);
        $data = $this->getDataFile($this->path, $dataDes);
        $this->getFileMimeTypeExt($data);
        $FileModel = new FileModel();
        $FileModel->setFile($data);
        $data = $FileModel->toArray();
        $this->getFileMimeType($data);
        return $data;
    }
    public function setFolderStorage($pathFile, $pathHashDes = "files", $rename = false, $start = "_", $end = "", $last = false) {
        if(is_dir($pathFile)) {
            $this->prepareAllData($this->userId);
            $pathParts  = pathinfo($pathFile);
            $dataDes    = $this->FileModel->getFile($pathHashDes);
            if((!(count($dataDes)) || !$dataDes)) {
                throw new Exception('RESOURCE_NOT_FOUND');
            }
            $this->path  = (empty($dataDes['path'])?$pathParts['basename']:$dataDes['path'] . DIRECTORY_SEPARATOR . $pathParts['basename']);
            if($rename && $this->hasFileDataStorage($this->path)) {
                $pathParts['basename'] = $this->getRenameExistFile($dataDes['path'], $pathParts['basename'], $start, $end);
                $this->path  = (empty($dataDes['path'])?$pathParts['basename']:$dataDes['path'] . DIRECTORY_SEPARATOR . $pathParts['basename']);
            }
            if(!$this->hasFileDataStorage($this->path)) {
                $dataFolder = $this->createDirStorage($pathParts['basename'], $pathHashDes, $rename, $start, $end);
            } else {
                $dataFolder = $this->FileModel->getFile($this->path);
                if((!(count($dataDes)) || !$dataDes)) {
                    throw new Exception('RESOURCE_NOT_FOUND');
                }
            }
            if(!$last && empty($this->folderOrigin)) {
                $this->folderOrigin = $dataFolder['path_hash'];
            }
            $filesDir = scandir($pathFile);
            foreach($filesDir as $keyDir => $valueDir) {
                if($valueDir != "." && $valueDir != "..") {
                    $setPath = realpath($pathFile . DIRECTORY_SEPARATOR . $valueDir);
                    if(!is_dir($setPath)) {
                        $this->setFileStorage($setPath, $dataFolder['path_hash'], $rename, $start, $end);
                    } else {
                        $this->setFolderStorage($setPath, $dataFolder['path_hash'], $rename, $start, $end, true);
                    }
                }
            }
            if(!$last && !empty($this->folderOrigin)) {
                return $this->getFileDataStorage($this->folderOrigin);
            }
        } else {
            throw new Exception('NOT_FOLDER');
        }
    }
    public function upFileStorage($pathFile, $pathHash = "files", $stream = false) {
        $this->prepareAllData($this->userId);
        if(!$stream) {
            $stream = fopen($pathFile, 'r+');
        } else {
            $stream = $pathFile;
        }
        $dataDes    = $this->FileModel->getFile($pathHash);
        if((!(count($dataDes)) || !$dataDes)) {
            throw new Exception('RESOURCE_NOT_FOUND');
        }
        if(!$stream) {
            $this->StorageFileSystem->updateStream($dataDes['path'], $stream);
        } else {
            $this->StorageFileSystem->update($dataDes['path'], $stream);
        }
        $this->FileModel->updateFile($dataDes);
        $data = $this->FileModel->toArray();
        return $data;
    }
    public function moveFileStorage($pathHashOrigin, $pathHashDes = "files", $rename = false, $start = "_", $end = "") {
        $this->prepareAllData($this->userId);
        $dataOrigin = $this->FileModel->getFile($pathHashOrigin);
        $dataDes    = $this->FileModel->getFile($pathHashDes);
        if((!(count($dataOrigin)) || !$dataOrigin) && (!(count($dataDes)) || !$dataDes)) {
            throw new Exception('RESOURCE_NOT_FOUND');
        }
        $this->path  = (empty($dataDes['path'])?$dataOrigin['name']:$dataDes['path'] . DIRECTORY_SEPARATOR . $dataOrigin['name']);
        $contents    = $this->StorageFileSystem->readAndDelete($dataOrigin['path']);
        if($rename && $this->hasFileDataStorage($this->path)) {
            $dataOrigin['name'] = $this->getRenameExistFile($dataDes['path'], $dataOrigin['name'], $start, $end);
            $this->path  = (empty($dataDes['path'])?$dataOrigin['name']:$dataDes['path'] . DIRECTORY_SEPARATOR . $dataOrigin['name']);
        }
        $this->StorageFileSystem->write($this->path, $contents);
        $dataOrigin['path']       = $this->path;
        $dataOrigin['parent']     = $dataDes['id'];
        $dataOrigin['path_hash']  = $this->getPathHash($this->pathBase . DIRECTORY_SEPARATOR . $this->path);
        $this->FileModel->updateFile($dataOrigin);
        $data = $this->FileModel->toArray();
        $this->getFileMimeType($data);
        return $data;
    }
    public function copyFileStorage($pathHashOrigin, $pathHashDes = "files", $rename = false, $start = "_", $end = "") {
        $this->prepareAllData($this->userId);
        $dataOrigin = $this->FileModel->getFile($pathHashOrigin);
        $dataDes    = $this->FileModel->getFile($pathHashDes);
        if((!(count($dataOrigin)) || !$dataOrigin) && (!(count($dataDes)) || !$dataDes)) {
            throw new Exception('RESOURCE_NOT_FOUND');
        }

        $this->path  = (empty($dataDes['path'])?$dataOrigin['name']:$dataDes['path'] . DIRECTORY_SEPARATOR . $dataOrigin['name']);
        if($rename && $this->hasFileDataStorage($this->path)) {
            $dataOrigin['name'] = $this->getRenameExistFile($dataDes['path'], $dataOrigin['name'], $start, $end);
            $this->path  = (empty($dataDes['path'])?$dataOrigin['name']:$dataDes['path'] . DIRECTORY_SEPARATOR . $dataOrigin['name']);
        }
        $this->StorageFileSystem->copy($dataOrigin['path'], $this->path);
        unset($dataOrigin["id"]);
        $dataOrigin['path']       = $this->path;
        $dataOrigin['parent']     = $dataDes['id'];
        $dataOrigin['path_hash']  = $this->getPathHash($this->pathBase . DIRECTORY_SEPARATOR . $this->path);
        $this->FileModel->setFile($dataOrigin);
        $data = $this->FileModel->toArray();
        $this->getFileMimeType($data);
        return $data;
    }
    public function reNameFileStorage($name = "", $pathHash = "") {
        $this->prepareAllData($this->userId);
        $data = $this->FileModel->getFile($pathHash);
        if(!(count($data)) || !$data) {
            throw new Exception('RESOURCE_NOT_FOUND');
        }
        $this->name  = $name;
        $oldNameFile = $data['path'];
        $this->path  = str_replace($data['name'],$name,$data['path']);
        $newNameFile = $this->path;
        if($this->StorageFileSystem->has($oldNameFile)) {
            if($this->StorageFileSystem->rename($oldNameFile, $newNameFile)) {
                $data['path']       = $this->path;
                $data['name']       = $this->name;
                $data['path_hash']  = $this->getPathHash($this->pathBase . DIRECTORY_SEPARATOR . $this->path);
                if($data['mimetype'] == 2) {
                    $data = $this->FileModel->updateFolder($data, $oldNameFile);
                } else {
                    $this->getFileMimeTypeExt($data);
                    $this->FileModel->updateFile($data);
                    $data = $this->FileModel->toArray();
                }
                $this->getFileMimeType($data);
            }
        } else {
            throw new Exception('RESOURCE_NOT_FOUND');
        }
        return $data;
    }
    public function createDirStorage($name = "", $pathHash = "files", $rename = false, $start = "_", $end = "") {
        $this->prepareAllData($this->userId);
        $data = $this->FileModel->getFile($pathHash);
        if(!(count($data)) || !$data) {
            throw new Exception('RESOURCE_NOT_FOUND');
        }
        $this->name = $name;
        $this->path = (empty($data['path'])?$this->name:$data['path'] . DIRECTORY_SEPARATOR . $this->name);
        if($rename && $this->hasFileDataStorage($this->path)) {
            $this->name = $this->getRenameExistFile($data['path'], $this->name, $start, $end);
            $this->path = (empty($data['path'])?$this->name:$data['path'] . DIRECTORY_SEPARATOR . $this->name);
        }
        if($this->StorageFileSystem->createDir($this->path)) {
            $dataN                     = array();
            $dataN['path']             = $this->path;
            $dataN['name']             = $this->name;
            $dataN['path_hash']        = $this->getPathHash($this->pathBase . DIRECTORY_SEPARATOR . $this->path);
            $dataN['parent']           = (int)$data['id'];
            $dataN['size']             = $this->StorageFileSystem->getSize($this->path);
            $dataN['mtime']            = 1;
            $dataN['storage_mtime']    = 0;
            $dataN['encrypted']        = 0;
            $dataN['storage_id']       = $this->dataStorage["id"];
            if($dataN['storage_mtime'] == 0) {
                $dataN['storage_mtime']  = 1;
            }
            $dataN['permissions']      = (int)65;
            $dataN['mimetype']         = (int)2;
            $this->FileModel->setFile($dataN);
            $data = $this->FileModel->toArray();
            $this->getFileMimeType($data);
            return $data;
        } else {
            throw new Exception('CREATE_FOLDER');
        }
    }
    public function deleteDirStorage($pathHash = "") {
        $this->prepareAllData($this->userId);
        $data = $this->FileModel->getFile($pathHash);
        if(!(count($data)) || !$data) {
            throw new Exception('RESOURCE_NOT_FOUND');
        }
        if($this->StorageFileSystem->deleteDir($data['path'])) {
            $this->FileModel->delFile($data);
        }
    }
    public function deleteFileStorage($pathHash = "") {
        $data = $this->FileModel->getFile($pathHash);
        if(!(count($data)) || !$data) {
            throw new Exception('RESOURCE_NOT_FOUND');
        }
        if($this->StorageFileSystem->delete($data['path'])) {
            $this->FileModel->delFile($data);
        }
    }
    private function getDataFile($pathFile, $dataParent) {
        $pathParts = pathinfo($this->pathBase . DIRECTORY_SEPARATOR . $pathFile);
        $pathReal  = realpath($this->pathBase . DIRECTORY_SEPARATOR . $pathFile);
        $dataR = array();
        $dataR['name']             = $pathParts['basename'];
        $dataR['path']             = $pathFile;
        $dataR['path_hash']        = $this->getPathHash($pathReal);
        $dataR['parent']           = (int)$dataParent['id'];
        $dataR['size']             = (is_dir($pathReal)?$this->getDirSize($pathReal):$this->getFileSize($pathReal));
        $dataR['mtime']            = 1;
        $dataR['storage_mtime']    = 0;
        $dataR['encrypted']        = 0;
        $dataR['storage_id']       = $this->dataStorage["id"];
        if($dataR['storage_mtime'] == 0) {
            $dataR['storage_mtime']  = 1;
        }
        $dataR['permissions']      = (int)65;
        $dataR['mimetype']         = (int)1;
        return $dataR;
    }
}
