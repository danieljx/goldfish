<?php
namespace App\Controllers;

use Interop\Container\ContainerInterface;

class Controller
{
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->lang      = $this->getContainer()->get('lang');
    }

    public function __get($property)
    {
        return $this->container->get($property);
        if ($this->container->{$property}) {
            return $this->container->{$property};
        }
    }

    public function getContainer()
    {
        return $this->container;
    }
}