<?php
// Routes

require __DIR__ . '/Routes/AuthRoute.php';
require __DIR__ . '/Routes/UserRoute.php';
require __DIR__ . '/Routes/UserThemeRoute.php';
require __DIR__ . '/Routes/FileRoute.php';
require __DIR__ . '/Routes/ProjectRoute.php';
require __DIR__ . '/Routes/TagRoute.php';
