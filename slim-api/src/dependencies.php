<?php
// DIC configuration

$container = $app->getContainer();

require __DIR__ . '/Handlers/errorHandler.php';
require __DIR__ . '/Handlers/phpErrorHandler.php';
require __DIR__ . '/Handlers/notFoundHandler.php';
require __DIR__ . '/Handlers/notAllowedHandler.php';

require __DIR__ . '/Dependencies/getUnit.php';
require __DIR__ . '/Dependencies/userTheme.php';
require __DIR__ . '/Dependencies/exportUnit.php';
require __DIR__ . '/Dependencies/exportSCORM.php';
require __DIR__ . '/Dependencies/blockCSSJson.php';
require __DIR__ . '/Dependencies/blockJson.php';
require __DIR__ . '/Dependencies/buildTree.php';
require __DIR__ . '/Dependencies/buildPageTree.php';
require __DIR__ . '/Dependencies/pageJson.php';

require_once __DIR__ . '/Dependencies/helpers.php';

$container['userId'] = 0;
// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

// PDO database library 
$container['db'] = function ($c) {
    $settings = $c->get('settings')['db'];
    $pdo = new PDO("mysql:host=" . $settings['host'] . ";dbname=" . $settings['dbname'] . ";charset=utf8",
        $settings['user'], $settings['pass']);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    return $pdo;
};

use App\Controllers\FilesSystem;

$container['fs'] = function ($c) {
    $fs = new FilesSystem([
        'local' => [
            'path' => $c->get('settings')['pathRoot'] . DIRECTORY_SEPARATOR . $c->get('settings')['pathApp'] . DIRECTORY_SEPARATOR . $c->get('settings')['pathData'],
        ]
    ]);
    return $fs;
};