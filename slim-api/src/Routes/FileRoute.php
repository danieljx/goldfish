<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use App\Middlewares\FileMiddleware;
use App\Middlewares\FileUpMiddleware;
use App\Controllers\FileController;

$app->group('/user/{user-id:[0-9]+}', function () {
	$this->get('/file/notification/', 'FileController:getFileNotification')->setName('user-file-notification');
	$this->get('/file/[{path_hash}]', 'FileController:getFile')->setName('user-file');
	$this->post('/file[/]', 'FileController:setFolder')->setName('user-file-folder');
	$this->delete('/file/{id}', 'FileController:delFile')->setName('user-file-delete');
	$this->put('/file/{id}', 'FileController:remFile')->setName('user-file-rename');
	$this->post('/file/move[/]', 'FileController:moveFile')->setName('user-file-move');
	$this->post('/file/upload[/]', 'FileController:setUploadFile')->add(new FileUpMiddleware())->setName('user-file-upload');
	$this->delete('/file/trash[/]', 'FileController:delTrashFile')->setName('user-file-delete-trash');
	$this->get('/file/download/{path_hash}', 'FileController:getFileDownload')->setName('user-file-download');
	$this->get('/file/view/{path_hash}', 'FileController:getFileView')->setName('user-file-view');
	$this->post('/file/download[/]', 'FileController:getFilesDownload')->setName('user-file-downloads');

	$this->get('/file/gallery/[{file}]', 'FileController:getGalleryFile')->setName('user-file-gallery');

	$this->get('/file/search/[{file}]', 'FileController:getFileSearch')->setName('user-file-search');

	$this->get('/file/tags/[{path_hash}]', 'FileController:getTagsFile')->setName('user-file-tags');
	$this->post('/file/tags[/]', 'FileController:setTagsFile')->setName('user-file-tags-post');

	$this->post('/file/shared[/]', 'FileController:setSharedFile')->setName('user-file-shared-post');
	$this->post('/file/shared/folder[/]', 'FileController:setSharedFolder')->setName('user-file-shared-post');
	$this->get('/file/shared/[{path_hash}]', 'FileController:getSharedFile')->setName('user-file-shared');
	$this->get('/file/shared/view/{path_hash}', 'FileController:getSharedFileView')->setName('user-file-view-shared');
	$this->get('/file/shared/download/{path_hash}', 'FileController:getSharedFileDownload')->setName('user-file-shared-download');
	$this->get('/file/shared/search[/]', 'FileController:getSharedFileSearch')->setName('user-file-shared-search');
	$this->put('/file/shared/{id}', 'FileController:remSharedFile')->setName('user-file-shared-rename');
	$this->post('/file/shared/upload[/]', 'FileController:setSharedUploadFile')->add(new FileUpMiddleware())->setName('user-file-shared-upload');

	$this->get('/file/public/[{path_hash}]', 'FileController:getPublicFile')->setName('user-file-public');
	$this->get('/file/public/view/{path_hash}', 'FileController:getPublicFileView')->setName('user-file-public-view');
	$this->get('/file/public/download/{path_hash}', 'FileController:getPublicFileDownload')->setName('user-file-public-download');


		/*
	$this->post('/file/shared/download[/]', 'FileController:getFilesDownload')->setName('user-file-shared-downloads');

	$this->post('/file/shared[/]', 'FileController:setSharedFile')->setName('user-file-shared-post');
	$this->delete('/file/shared/{id}', 'FileController:cancelSharedFile')->setName('user-file-shared-delete');
	$this->put('/file/shared/{id}', 'FileController:remSharedFile')->setName('user-file-shared-rename');
	$this->post('/file/shared/upload[/]', 'FileController:setUploadSharedFile')->add(new FileUpMiddleware())->setName('user-file-shared-upload');
	$this->get('/file/shared/download/{path_hash}', 'FileController:getFileDownload')->setName('user-file-shared-download');
*/
	/*
	$this->group('/file/shared/[{path_hash}]', function () {
			$this->get('/view/{path_hash}', 'FileController:getFileViewReds')->setName('user-file-reds-view');
			$this->get('/download/{path_hash}', 'FileController:getFileDownloadReds')->setName('user-file-reds-downloads');
			$this->put('/rename/', 'FileController:remFile')->setName('user-file-rename');
			$this->get('/search[/]', 'FileController:getFileSearch')->setName('user-file-search');
	});
*/
	$this->group('/file/reds/{path_hash}', function () {
		$this->get('/data[/{path:.*}]', 'FileController:getFileReds')->setName('user-file-reds');
		$this->get('/view[/{path:.*}]', 'FileController:getFileViewReds')->setName('user-file-reds-view');
		$this->get('/download[/{path:.*}]', 'FileController:getFileDownloadReds')->setName('user-file-reds-downloads');
	});
})->add(new FileMiddleware());
