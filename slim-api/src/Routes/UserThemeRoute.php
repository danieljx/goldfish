<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/usertheme/{theme-id:[0-9]+}', function (Request $request, Response $response) {
	$id = $request->getAttribute('theme-id');
	$userTheme = $this->get('userTheme');
	$theme = $userTheme($id);
	
    $newResponse = $response->withJson($theme);
    return $theme;
});