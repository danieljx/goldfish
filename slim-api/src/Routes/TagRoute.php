<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use App\Middlewares\FileMiddleware;
use App\Middlewares\FileUpMiddleware;
use App\Controllers\FileController;

$app->group('/user/{user-id:[0-9]+}', function () {
	$this->get('/tag/[{tag}]', 'TagController:getTags')->setName('user-tags');
	$this->get('/tag/search/[{tag}]', 'TagController:getSearchTags')->setName('user-tags-search');
	$this->post('/tag[/]', 'TagController:setTags')->setName('user-tags-set');
});
