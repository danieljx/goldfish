<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

// OK v2
$app->get('/auth/logout', function (Request $request, Response $response) {
	session_unset();
	$json = ['result' => 'ok'];
	$newResponse = $response->withJson($json);
    return $newResponse;
});

// OK v2
$app->get('/auth/{email}/{password}', function (Request $request, Response $response) {
    $email = $request->getAttribute('email');
    $password = $request->getAttribute('password');

     $sth = $this->db->prepare("SELECT user.id FROM gf_users AS user WHERE user.email=:email AND user.password=:password LIMIT 1");
    $sth->bindParam("email", $email);
    $sth->bindParam("password", $password);
	$sth->execute();
    $items = $sth->fetchALL(PDO::FETCH_ASSOC);

    $json = array();

    $json['email'] = $email;
    $json['password'] = $password;
    $json['result'] = 0;
    $json['auth'] = bin2hex(openssl_random_pseudo_bytes(16));
    if (count($items) > 0) {
    	$_SESSION['user_id'] = $json['result'] = $items[0]['id'];
    }
 //    foreach($items as $item) {
 //    	if (!isset($json[$item['id']])) {


	//       $json[$item['id']] = array(
	//           'id' => $item['id'],
	//           'names' => $item['names'],
	//           'surnames' => $item['surnames'],
	//           'profile' => $item['profile'],
	//           'units' => array()
	//       );
	//       }
	//    unset($item['id']);
	//    unset($item['names']);
	//    unset($item['surnames']);
	//    unset($item['profile']);
	//    $item['id'] = $item['unit_id'];
	//    unset($item['unit_id']);
	//    $json[$id]['units'][] = $item;
	// }

    $newResponse = $response->withJson($json);
    return $newResponse;
});