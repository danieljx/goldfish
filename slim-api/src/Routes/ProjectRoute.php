<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use App\Middlewares\FileMiddleware;
use App\Controllers\ProjectController;

$app->group('/user/{user-id:[0-9]+}', function () {
    $this->get('/project/search[/]', 'ProjectController:getProjectSearch')->setName('user-project-search');
});