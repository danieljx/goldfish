<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use App\Models\ComponentBlueprintModel;
use App\Models\ThemeBlueprintModel;
use App\Models\MediaqueryBlueprintModel;
use App\Models\FontModel;

$app->get('/user/{user-id:[0-9]+}/fonts/', function (Request $request, Response $response) {
	$userId = $request->getAttribute('user-id');
	$fontDirPhys = '../../dist/css/fonts';

	$avaliable = array();
	$dir = array_diff(scandir($fontDirPhys), array('..', '.','.gitignore'));
	foreach ($dir as $file) {
		$obj = array();
		$obj['file_name'] = $file;
		$obj['title'] = str_replace(['_','.woff'], [' ',''], $file);

		$available[] = $obj;
	}
	
	$installed = FontModel::all();
    
    // $sth = $this->db->prepare("SELECT 
	// 		id,
	// 		title,
	// 		file_name,
	// 		font_type,
	// 		css_stack,
	// 		weight
	// 		FROM gf_fonts");
	// $sth->execute();
	// $installed = $sth->fetchALL(PDO::FETCH_ASSOC);

	$all = array();

	foreach($installed as $dbFont) {
		$found = false;
		foreach($available as $dirKey => $dirFont) {
			if (!$found && ($dbFont['title'] == $dirFont['title'])) {
				$all[] = $dbFont;
				unset($available[$dirKey]);
				$found = true;
			} 
		}
	}

	foreach ($available as $dirFont) {
		$all[] = $dirFont;
	}

    $newResponse = $response->withJson($all);
    return $newResponse;
});

$app->post('/user/{user-id:[0-9]+}/font/', function (Request $request, Response $response) {
	$userId = $request->getAttribute('user-id');
	$fontScssFile = '../../components/_common/fonts.scss';

	$request = (array) json_decode($request->getBody());

	$font = new FontModel;

	$font->fill($request);
	$font->save();
	$request['id'] = $font->id;

	if (!is_file($fontScssFile)) {
		file_put_contents($fontScssFile, '');
		$fontScss = '';
	} else {
		$fontScss = file_get_contents($fontScssFile);
	}

	$newScss = "@font-face {\nfont-family: '".$font->title."';\nfont-style: normal;\nfont-weight: ".$font->weight.";\nsrc: local('".$font->title."'), local('".$font->title."'), url(#{\$fontPath}".$font->file_name.") format('woff');\n}\n";

	preg_match("/font-family(\s*):(.*?)'".$font->title."'".'("|;|$)/', $fontScss, $matches);
	if (empty($matches[2])) {
		file_put_contents($fontScssFile, $fontScss."\n".$newScss);
	}

	// add_log($font->toJson());

	

    $newResponse = $response->withJson($request);
    return $newResponse;
});

$app->get('/user/{user-id:[0-9]+}/components/', function (Request $request, Response $response) {
	$userId = $request->getAttribute('user-id');
	$items = ComponentBlueprintModel::all();
    $newResponse = $response->withJson($items);
    return $newResponse;
});

$app->post('/user/{user-id:[0-9]+}/component/', function (Request $request, Response $response) {
	$userId = $request->getAttribute('user-id');
	$request = (array) json_decode($request->getBody());

	$component = new ComponentBlueprintModel;

	$component->fill($request);
	$component->version = 1;
	$component->save();

	$request['id'] = $component->id;

	$themes = ThemeBlueprintModel::all();

	foreach ($themes as $theme) {
		$scss = new scssc();

		$scss->setFormatter('scss_formatter_compressed');
		$css = $scss->compile($theme->sass.' '.$component->sass);
		$cssParsed = new \Crossjoin\Css\Reader\CssString($css);
		$mediaQueries = ['' => []];

		foreach ($cssParsed->getStyleSheet()->getRules() as $rule) {
			if (get_class( $rule ) == 'Crossjoin\Css\Format\Rule\AtMedia\MediaRule') {
				$mediaFeatures = [];
				foreach( $rule->getQueries() as $query ) {
					foreach($query->getConditions() as $condition) {
						$mediaFeatures[] = '('.$condition->getValue().')';	
					}
				}
				$mediaQuery = '@media '.implode(' and ',$mediaFeatures);

				foreach ($rule->getRules() as $mediaRule) {
					$mediaQueries[$mediaQuery][] = Helpers::getRuleSet($mediaRule);	
				}

			} else {
				$mediaQueries[''][] = Helpers::getRuleSet($rule);
			}
		}

		// MediaqueryBlueprintModel::where(['theme_id' => $theme->id, 'block_id' => $component_id])->delete();
		add_log( MediaqueryBlueprintModel::where('theme_id', $theme->id)->where('block_id', $component->id)->toSql() );

		foreach ($mediaQueries as $query => $queryRules) {
			
			$mediaQuery = MediaqueryBlueprintModel::where('gf_theme_blueprints_id', $theme->id)
				->where('gf_component_blueprints_id', $component->id)
				->where('media_query', $query)
				->first();
			if (!$mediaQuery) {
				$mediaQuery = new MediaqueryBlueprintModel();
				$mediaQuery->gf_component_blueprints_id = $component->id;
				$mediaQuery->gf_theme_blueprints_id = $theme->id;
				$mediaQuery->media_query = $query;
			}
			
			$mediaQuery->body_css = $queryRules;

			add_log( print_r($mediaQuery->toJson(), true) );
			$mediaQuery->save();
		}
	}

    $newResponse = $response->withJson($request);
    return $newResponse;
});

$app->put('/user/{user-id:[0-9]+}/component/{component-id:[0-9]+}', function (Request $request, Response $response) {
	$userId = $request->getAttribute('user-id');
	$componentId = $request->getAttribute('component-id');
	$request = (array) json_decode($request->getBody());

	$component = ComponentBlueprintModel::find($componentId*1);

	$component->fill($request);
	$component->version += 1;
	$component->save();

	$themes = ThemeBlueprintModel::all();

	foreach ($themes as $theme) {
		$scss = new scssc();

		$scss->setFormatter('scss_formatter_compressed');
		$css = $scss->compile($theme->sass.' '.$component->sass);
		$cssParsed = new \Crossjoin\Css\Reader\CssString($css);
		$mediaQueries = ['' => []];

		foreach ($cssParsed->getStyleSheet()->getRules() as $rule) {
			if (get_class( $rule ) == 'Crossjoin\Css\Format\Rule\AtMedia\MediaRule') {
				$mediaFeatures = [];
				foreach( $rule->getQueries() as $query ) {
					foreach($query->getConditions() as $condition) {
						$mediaFeatures[] = '('.$condition->getValue().')';	
					}
				}
				$mediaQuery = '@media '.implode(' and ',$mediaFeatures);

				foreach ($rule->getRules() as $mediaRule) {
					$mediaQueries[$mediaQuery][] = Helpers::getRuleSet($mediaRule);	
				}

			} else {
				$mediaQueries[''][] = Helpers::getRuleSet($rule);
			}
		}

		// MediaqueryBlueprintModel::where(['theme_id' => $theme->id, 'block_id' => $component_id])->delete();
		add_log( MediaqueryBlueprintModel::where('theme_id', $theme->id)->where('block_id', $component->id)->toSql() );

		foreach ($mediaQueries as $query => $queryRules) {
			
			$mediaQuery = MediaqueryBlueprintModel::where('gf_theme_blueprints_id', $theme->id)
				->where('gf_component_blueprints_id', $component->id)
				->where('media_query', $query)
				->first();
			if (!$mediaQuery) {
				$mediaQuery = new MediaqueryBlueprintModel();
				$mediaQuery->gf_component_blueprints_id = $component->id;
				$mediaQuery->gf_theme_blueprints_id = $theme->id;
				$mediaQuery->media_query = $query;
			}
			
			$mediaQuery->body_css = $queryRules;

			add_log( print_r($mediaQuery->toJson(), true) );
			$mediaQuery->save();
		}
	}

    $newResponse = $response->withJson($request);
    return $newResponse;
});

$app->get('/user/{user-id:[0-9]+}/themes/', function (Request $request, Response $response) {
	$userId = $request->getAttribute('user-id');
	$items = ThemeBlueprintModel::all();
    $newResponse = $response->withJson($items);
    return $newResponse;
});

$app->put('/user/{user-id:[0-9]+}/theme/{theme-id:[0-9]+}', function (Request $request, Response $response) {
	$userId = $request->getAttribute('user-id');
	$themeId = $request->getAttribute('theme-id');
	$request = (array) json_decode($request->getBody());

	$theme = ThemeBlueprintModel::find($themeId*1);

	$theme->fill($request);
	$theme->version += 1;
	$theme->save();

	$components = ComponentBlueprintModel::all();

	foreach ($components as $component) {
		$scss = new scssc();

		$scss->setFormatter('scss_formatter_compressed');
		$css = $scss->compile($theme->sass.' '.$component->sass);
		$cssParsed = new \Crossjoin\Css\Reader\CssString($css);
		$mediaQueries = ['' => []];

		foreach ($cssParsed->getStyleSheet()->getRules() as $rule) {
			if (get_class( $rule ) == 'Crossjoin\Css\Format\Rule\AtMedia\MediaRule') {
				$mediaFeatures = [];
				foreach( $rule->getQueries() as $query ) {
					foreach($query->getConditions() as $condition) {
						$mediaFeatures[] = '('.$condition->getValue().')';	
					}
				}
				$mediaQuery = '@media '.implode(' and ',$mediaFeatures);

				foreach ($rule->getRules() as $mediaRule) {
					$mediaQueries[$mediaQuery][] = Helpers::getRuleSet($mediaRule);	
				}

			} else {
				$mediaQueries[''][] = Helpers::getRuleSet($rule);
			}
		}

		foreach ($mediaQueries as $query => $queryRules) {
			$mediaQuery = MediaqueryBlueprintModel::where('gf_theme_blueprints_id', $theme->id)
				->where('gf_component_blueprints_id', $component->id)
				->where('media_query', $query)
				->first();
			if (!$mediaQuery) {
				$mediaQuery = new MediaqueryBlueprintModel();
				$mediaQuery->gf_component_blueprints_id = $component->id;
				$mediaQuery->gf_theme_blueprints_id = $theme->id;
				$mediaQuery->media_query = $query;
			}
			
			$mediaQuery->body_css = $queryRules;

			add_log( print_r($mediaQuery->toJson(), true) );
			$mediaQuery->save();
		}
	}

    $newResponse = $response->withJson($request);
    return $newResponse;
});

// OK v2
$app->get('/user/{user-id:[0-9]+}/project/{id:[0-9]+}', function (Request $request, Response $response) {
    $id = $request->getAttribute('id');
    $user_id = $request->getAttribute('user-id');

    $sth = $this->db->prepare("SELECT
    	project.id,
    	project.title,
    	project.remote_auth,
		project.gf_output_formats_id AS format_id,
    	rel.gf_roles_id AS role,
    	project.description,
    	unit.id AS unit_id,
    	unit.title AS unit_title,
		gf_theme_instances.gf_theme_blueprints_id AS unit_theme
    	FROM gf_projects AS project LEFT JOIN gf_user_has_projects_roles AS rel
    		ON project.id = rel.gf_projects_id
    	LEFT JOIN gf_unit_has_users AS rel2
    		ON rel.gf_users_id = rel2.gf_users_id
    	LEFT JOIN gf_units AS unit
    		ON rel2.gf_unit_id = unit.id AND unit.gf_projects_id=:id
		LEFT JOIN gf_theme_instances
			ON unit.gf_theme_instance_id = gf_theme_instances.id
    	WHERE project.id=:id AND rel.gf_users_id =:user_id");
    $sth->bindParam("id", $id);
    $sth->bindParam("user_id", $user_id);

	$sth->execute();
    $items = $sth->fetchALL(PDO::FETCH_ASSOC);

    $json = array();

    foreach($items as $item) {
    	if (!isset($json[$item['id']])) {

	      $json[$item['id']] = array(
	          'id' => $item['id'],
	          'title' => $item['title'],
	          'remote_auth' => $item['remote_auth'],
			  'description' => $item['description'],
			  'format_id' => $item['format_id'],
	          'roles' => array($item['role']),
	          'units' => array()
	      );
	  }

	   if (!in_array($item['role'], $json[$item['id']]['roles'])) {
	   		$json[$item['id']]['roles'][] = $item['role'];
	   }

	   unset($item['id']);
	   unset($item['title']);
	   unset($item['remote_auth']);
	   unset($item['description']);
	   unset($item['format_id']);
	   unset($item['role']);

	   $item['id'] = $item['unit_id'];
	   unset($item['unit_id']);

	   if ($item['id']*1 > 0 && !isset($json[$id]['units'][$item['id']])) {
	   		$item['units'] = array($item['id']);
	   		$json[$id]['units'][$item['id']] = array('title' => $item['unit_title'], 'theme' => $item['unit_theme'] );
	   }

	}

	if (count($json[$id]['units']) > 0) {
		$units = [];
		foreach ($json[$id]['units'] as $key => $value) {
			$units[] = array('id' => $key.'', 'title' => $value['title'], 'theme' => $value['theme']);
		}
		$json[$id]['units'] = $units;
	}
    $newResponse = $response->withJson($json[$id]);
    return $newResponse;
});

// OK v2
$app->put('/user/{user_id:[0-9]+}/config/{id:[0-9]+}', function(Request $request, Response $response) {
	$id = $request->getAttribute('id');

	$request = (array) json_decode($request->getBody());

	$sth = $this->db->prepare("UPDATE gf_user_configs SET editor=:editor, theme=:theme, effects=:effects WHERE id=:id");
	$sth->bindParam("id", $id);
	$sth->bindParam("editor", $request['editor']);
	$sth->bindParam("theme", $request['theme']);
	$sth->bindParam("effects", $request['effects']);
	$sth->execute();
	// $request = (array) json_decode(request()->getBody());

	$json = array('method' => 'put', 'editor' => $request['editor']);
	$newResponse = $response->withJson($request);
    return $newResponse;


});

// OK v2
$app->post('/user/{user_id:[0-9]+}/config/', function(Request $request, Response $response) {
	$user_id = $request->getAttribute('user_id');
	$request = (array) json_decode($request->getBody());

	$sth = $this->db->prepare("INSERT INTO gf_user_configs (editor,theme,effects,gf_users_id) VALUES (:editor,:theme,:effects,:gf_users_id)");
	$sth->bindParam("editor", $request['editor']);
	$sth->bindParam("theme", $request['theme']);
	$sth->bindParam("effects", $request['effects']);
	$sth->bindParam("gf_users_id", $user_id);
	$sth->execute();

	// $request = (array) json_decode(request()->getBody());
	$request['id'] = $this->db->lastInsertId();

	$json = $request;

	$newResponse = $response->withJson($json);
    return $newResponse;


});

$app->post('/user/', function (Request $request, Response $response) {
	$request = (array) json_decode($request->getBody());

	$request['password'] = password_hash($request['password'], PASSWORD_BCRYPT);
	$request['password_salt'] = '';
	$request['token'] = '';
	$request['token_exp'] = '';


	$sth = $this->db->prepare("INSERT INTO gf_users (
		names,
		surnames,
		profile,
		email,
		password,
		active,
		password_salt,
		token,
		token_exp
		)
	VALUES
		(
		:names,
		:surnames,
		:profile,
		:email,
		:password,
		:active,
		:password_salt,
		:token,
		:token_exp
		)");

	$sth->bindParam("names", $request['names']);
	$sth->bindParam("surnames", $request['surnames']);
	$sth->bindParam("profile", $request['profile']);
	$sth->bindParam("email", $request['email']);
	$sth->bindParam("password", $request['password']);
	$sth->bindParam("active", $request['active']);
	$sth->bindParam("password_salt", $request['password_salt']);
	$sth->bindParam("token", $request['token']);
	$sth->bindParam("token_exp", $request['token_exp']);
	$sth->execute();

	// $request = (array) json_decode(request()->getBody());
	$request['id'] = $this->db->lastInsertId();

	$json = $request;

	$newResponse = $response->withJson($json);
    return $newResponse;
});

// OK v2
$app->get('/user/{id:[0-9]+}', function (Request $request, Response $response) {
    $id = $request->getAttribute('id');
    // $json = array('id' => $id, 'name' => 'Mats Fjellner');
    $this->db->beginTransaction();
    $sth = $this->db->prepare("SELECT
    	user.id,
    	user.names,
    	user.surnames,
    	user.profile,
    	project.id AS project_id,
    	title,
    	remote_auth,
    	gf_roles_id AS role,
    	project.description
    	FROM gf_users AS user
    	LEFT JOIN gf_user_has_projects_roles AS rel
    	ON user.id = rel.gf_users_id
    	LEFT JOIN gf_projects AS project
    	ON rel.gf_projects_id = project.id
    	WHERE user.id=:id");
    $sth->bindParam("id", $id);
	$sth->execute();
    $items = $sth->fetchALL(PDO::FETCH_ASSOC);

    $sth = $this->db->prepare("SELECT theme, effects, id FROM gf_user_configs WHERE gf_users_id = :id");
    $sth->bindParam("id", $id);
	$sth->execute();
	$config = $sth->fetchALL(PDO::FETCH_ASSOC);

	$sth = $this->db->prepare("SELECT id, name, description, default_format FROM gf_output_formats");
	$sth->execute();
	$formats = $sth->fetchALL(PDO::FETCH_ASSOC);

	$sth = $this->db->prepare("SELECT id, title, file_name AS name, font_type AS type, css_stack AS stack, weight FROM gf_fonts");
	$sth->execute();
	$fonts = $sth->fetchALL(PDO::FETCH_ASSOC);

	$this->db->commit();
    $json = array();

    foreach($items as $item) {
    	if (!isset($json[$item['id']])) {

		    $json[$item['id']] = array(
				'id' => $item['id'],
				'names' => $item['names'],
				'surnames' => $item['surnames'],
				'profile' => $item['profile'],
				'projects' => array(),
				'config' => array(),
				'formats' => array(),
				'fonts' => array()
			);

			if (count($config) > 0) {
				$json[$item['id']]['config'] = $config[0];
			}

			if (count($formats) > 0) {
				$json[$item['id']]['formats'] = $formats;
			}

			if (count($fonts) > 0) {
				$json[$item['id']]['fonts'] = $fonts;
			}

	      }
	   unset($item['id']);
	   unset($item['names']);
	   unset($item['surnames']);
	   unset($item['profile']);
	   // if (!isset($json[$item['id']])) {

	   $item['id'] = $item['project_id'];

	   unset($item['project_id']);
	   if (!isset($json[$id]['projects'][$item['id']])) {
	   		$item['roles'] = array($item['role']);
	   		unset($item['role']);
	   		$json[$id]['projects'][$item['id']] = $item;
	   } else {
	   		$json[$id]['projects'][$item['id']]['roles'][] = $item['role'];
	   		unset($item['role']);
	   }

	}
	$projects = [];
	foreach ($json[$id]['projects'] as $key => $value) {
		$value['id'] = $key;
		$projects[] = $value;
	}
	$json[$id]['projects'] = $projects;

	$sql = "SELECT \t f.id\n";
	$sql.= "\t\t,f.name\n";
	$sql.= "\t\t,f.path\n";
	$sql.= "\t\t,f.path_hash\n";
	$sql.= "\t\t,s.id AS storage_id\n";
	$sql.= "FROM gf_storages as s\n";
	$sql.= "INNER JOIN gf_files AS f on f.storage_id = s.id\n";
	$sql.= "WHERE s.user_id = :id\n";
	$sql.= "AND f.path_hash = md5('files');\n";
	$sth = $this->db->prepare($sql);
    $sth->bindParam("id", $id);
	$sth->execute();
    $files = $sth->fetchALL(PDO::FETCH_ASSOC);
	if(count($files) > 0) {
		foreach ($files as $key => $value) {
			$sqlCh = "\t\t SELECT h.id\n";
			$sqlCh.= "\t\t		,h.name\n";
			$sqlCh.= "\t\t		,h.path_hash\n";
			$sqlCh.= "\t\t		,h.storage_id\n";
			$sqlCh.= "\t\t  FROM gf_files AS h\n";
			$sqlCh.= "\t\t  WHERE h.parent = :id\n";
			$sthCh = $this->db->prepare($sqlCh);
			$sthCh->bindParam("id", $value["id"]);
			$sthCh->execute();
			$files[$key]["children"] = $sthCh->fetchALL(PDO::FETCH_ASSOC);
		}
	}
	$json[$id]['files'] = $files;

    $newResponse = $response->withJson($json[$id]);
    return $newResponse;
});



// Semi OK v2
$app->get('/user/{user-id:[0-9]+}/project/{project-id:[0-9]+}/theme/{theme-id:[0-9]+}/components', function (Request $request, Response $response) {
	$userId = $request->getAttribute('user-id');
	$projectId = $request->getAttribute('project-id');
	$themeId = $request->getAttribute('theme-id');

	/* TODO: adaptar para reflejar compontentes por unidad en vez de proyecto, incluyendo theme */

    $sth = $this->db->prepare("SELECT
    	DISTINCT(mediaquery.id) AS mediaquery_id,
    	component.id,
    	component.name,
    	component.title,
    	component.repeatable,
    	component.body,
		component.data,
		component.type,
		component.url,
    	mediaquery.id AS mediaquery_id,
    	mediaquery.body_css AS body_css,
    	media_query
    	FROM gf_component_blueprints AS component
    	INNER JOIN gf_project_has_components AS project_components
    		ON component.id = project_components.gf_component_blueprints_id OR project_components.gf_projects_id=0
    	LEFT JOIN gf_mediaquery_blueprints AS mediaquery
    		ON component.id = mediaquery.gf_component_blueprints_id
    	WHERE (project_components.gf_projects_id=:projectId OR project_components.gf_projects_id=0)
		AND mediaquery.gf_theme_blueprints_id=:themeId");
    $sth->bindParam("projectId", $projectId);
	$sth->bindParam("themeId", $themeId);
	$sth->execute();
	$items = $sth->fetchALL(PDO::FETCH_ASSOC);

	// $newResponse = $response->withJson($items);
 //    return $newResponse;

	$json = array();
	foreach($items as $item) {
    	if (!isset($json[$item['id']])) {

			$json[$item['id']] = array(
				'id' => $item['id'],
				'name' => $item['name'],
				'title' => $item['title'],
				'body' => $item['body'],
				'data' => json_decode($item['data']),
				'repeatable' => $item['repeatable'],
				'type' => $item['type'],
				'url' => $item['url'],
				'css' => array()
			);
		}
		$id = $item['id'];
		unset($item['id']);
		unset($item['name']);
		unset($item['title']);
		unset($item['body']);
		unset($item['data']);
		unset($item['repeatable']);
		unset($item['type']);
		unset($item['url']);

		if ($item['mediaquery_id']) {
			$item['id'] = $item['mediaquery_id'];
			$item['body_css'] = json_decode($item['body_css'], true);
			unset($item['mediaquery_id']);
			$json[$id]['css'][] = $item;
		}

		unset($item['media_query']);
		unset($item['body_css']);
	}

	$json2 = array();

	foreach($json as $key => $value) {
		$json2[] = $value;
	}


    $newResponse = $response->withJson($json2);
    return $newResponse;
});

$app->get('/user/{user-id:[0-9]+}/project/{project-id:[0-9]+}/themes', function (Request $request, Response $response) {
	$project_id = $request->getAttribute('project-id');
	$json = array();

	$sth = $this->db->prepare("SELECT
    	theme.id,
    	theme.name,
    	theme.title,
    	theme.version,
    	theme.description
    	FROM gf_theme_blueprints AS theme
    	INNER JOIN gf_project_has_themes AS rel
    	ON theme.id = rel.gf_theme_blueprints_id
    	WHERE rel.gf_projects_id=:project_id");
    $sth->bindParam("project_id", $project_id);
	$sth->execute();
    $items = $sth->fetchAll(PDO::FETCH_ASSOC);

 //    foreach($items as $item) {
 //    	if (!isset($json[$item['id']])) {

	// 		$json[$item['id']] = array(
	// 			'id' => $item['id'],
	// 			'name' => $item['name'],
	// 			'title' => $item['title'],
	// 			'version' => $item['version'],
	// 			'description' => $item['description'],
	// 			// 'sass' => $item['sass'],
	// 		);
	// 	}
	// 	unset($item['id']);
	// 	unset($item['name']);
	// 	unset($item['title']);
	// 	unset($item['version']);
	// 	unset($item['description']);
	// 	unset($item['sass']);
	// 	unset($item['url']);
	// }

	// $json2 = array();

	// foreach($json as $key => $value) {
	// 	$json2[] = $value;
	// }


    $newResponse = $response->withJson($items);
    return $newResponse;
});


// OK v2
$app->get('/user/{user-id:[0-9]+}/project/{project-id:[0-9]+}/unit/{id:[0-9]+}[/theme/{theme-id:[0-9]+}]', function (Request $request, Response $response) {
	$theme_id = is_numeric($request->getAttribute('theme-id')) ? $request->getAttribute('theme-id') : false;
	$id = $request->getAttribute('id');
	$project_id = $request->getAttribute('project-id');

	// TODO: Agregar condicional de que la unidad sea del proyecto
	$getUnit = $this->get('getUnit');
	$json = $getUnit($id);

    $newResponse = $response->withJson($json[$id]);
    return $newResponse;
});

$app->get('/user/{user-id:[0-9]+}/project/{project-id:[0-9]+}/unit/{id:[0-9]+}/export[/format/{format-str:[a-z]+}]', function (Request $request, Response $response) {
	// Recoger los parámetros
	$user_id = $request->getAttribute('user-id');
	$this->userId = $user_id;
	$project_id = $request->getAttribute('project-id');
	$unit_id = $request->getAttribute('id');

	$exportUnit = $this->get('exportUnit');
	$zipName = $exportUnit($user_id, $project_id, $unit_id);

	$response   = $response->withHeader('Content-Type', 'application/zip');
	$response   = $response->withHeader('Content-Description', 'File Transfer');
	$response   = $response->withHeader('Content-Disposition', 'attachment; filename="' .basename($zipName['publicName']) . '"');
	$response   = $response->withHeader('Content-Transfer-Encoding', 'binary');
	$response   = $response->withHeader('Expires', '0');
	$response   = $response->withHeader('Cache-Control', 'must-revalidate');
	$response   = $response->withHeader('Pragma', 'public');
	$response   = $response->withHeader('Content-Length', filesize($zipName['realName']));

	readfile($zipName['realName']);

    return $response;
});

$app->get('/pagestructure/{id:[0-9]+}', function (Request $request, Response $response) {
	$id = $request->getAttribute('id');

    // TODO: Agregar condicional de que la unidad sea del proyecto

    $sth = $this->db->prepare("SELECT
    	page.title AS title,
    	page.id AS id,
    	page.gf_components_id,
    	rel.depth,
		rel.position,
		rel2.gf_ascendant
    	FROM gf_pages AS page
    	JOIN gf_page_relations AS rel
		ON page.id = rel.gf_descendant

		JOIN gf_page_relations AS rel2
		ON rel.gf_descendant = rel2.gf_descendant AND
		(rel2.depth = 1 OR rel2.gf_descendant = :id)
		WHERE rel.gf_ascendant=:id
		ORDER BY depth,position");
    $sth->bindParam("id", $id);
	$sth->execute();
	$items = $sth->fetchALL(PDO::FETCH_ASSOC);

	// $newResponse = $response->withJson($items);
 //    return $newResponse;

	$buildPageTree = $this->get('buildPageTree');
	$json = $buildPageTree($items,$id);


    $newResponse = $response->withJson($json);
    // $newResponse = $response->withJson($items);
    return $newResponse;
});

// OK v2
$app->post('/user/{user-id:[0-9]+}/project/{project-id:[0-9]+}/unit', function (Request $request, Response $response) {
	$userId = $request->getAttribute('user-id');
	$projectId = $request->getAttribute('project-id');
	$request = (array) json_decode($request->getBody());
	$this->db->beginTransaction();
	try {
		$sth = $this->db->prepare("INSERT INTO gf_theme_instances (changed, gf_theme_blueprints_id, body_css) VALUES (:changed,:blueprint,:css)");
		$sth->bindValue("changed", 1);
		$sth->bindValue("blueprint", 1);
		$sth->bindValue("css", '');
		$sth->execute();
		$request['theme_instance_id'] = $this->db->lastInsertId();


		$sth = $this->db->prepare("INSERT INTO gf_units (title,saved,gf_theme_instance_id,gf_projects_id,gf_output_formats_id) VALUES (:title,NOW(),:theme_id,:project_id,(SELECT gf_output_formats_id FROM gf_projects WHERE id=:project_id))");
		$sth->bindParam("title", $request['title']);
		$sth->bindParam("theme_id", $request['theme_instance_id']);
		$sth->bindParam("project_id", $projectId);
		$sth->execute();
		$request['id'] = $this->db->lastInsertId();

		$sth = $this->db->prepare("INSERT INTO gf_unit_has_users (gf_unit_id,gf_users_id,gf_roles_id) VALUES (:id,:user_id,:role_id)");
		$sth->bindParam("id", $request['id']);
		$sth->bindParam("user_id", $userId);
		$sth->bindValue("role_id", ROLE_EDITOR);
		$sth->execute();

		$this->db->commit();
	}
	catch(Exception $e) {
		add_log($e->getLine().': '.$e->getMessage());
	    $request['error'] = $e->getLine().': '.$e->getMessage();
	    $this->db->rollBack();
	}
	// Debe manejar páginas también
	return $response->withJson($request);
});

// OK v2
$app->put('/user/{user-id:[0-9]+}/project/{project-id:[0-9]+}/unit/{id:[0-9]+}', function (Request $request, Response $response) {
	$write = true;
	$userId = $request->getAttribute('user-id');
	$this->userId = $userId;
	$projectId = $request->getAttribute('project-id');
	$id = $request->getAttribute('id');

	$request = (array) json_decode($request->getBody(), true);

	$request['gotPages'] = is_array($request['pages']);

	$this->db->beginTransaction();

	try {
		$sth = $this->db->prepare("SELECT gf_theme_instance_id AS theme_instance FROM gf_units WHERE id = :id");
		$sth->bindParam("id", $request['id']);
		$sth->execute();
		$row = $sth->fetch(PDO::FETCH_ASSOC);

		$sth = $this->db->prepare("UPDATE gf_theme_instances SET changed = 1, body_css = '', gf_theme_blueprints_id = :theme  WHERE id=:id");
		$sth->bindParam("theme", $request['theme']);
		$sth->bindParam("id", $row['theme_instance']);
		$sth->execute();


		$sth = $this->db->prepare("UPDATE gf_units SET title = :title, saved = NOW(), data = :data WHERE id=:id");
		$sth->bindParam("title", $request['title']);
		$sth->bindParam("id", $request['id']);
		$sth->bindParam("data", json_encode($request['data']));
		$sth->execute();

		$sth = $this->db->prepare("DELETE FROM gf_pages WHERE gf_unit_id = :id");
		$sth->bindParam("id", $request['id']);
		$sth->execute();

		if (is_array($request['pages'])) {
			$pageJson = $this->get('pageJson');
			$request['pages'] = $pageJson($this->db, $request['pages'], [], $id, $request['theme']);
		}

		$this->db->commit();


	}
	catch(Exception $e) {
		add_log($e->getLine().': '.$e->getMessage());
	    $request['error'] = $e->getFile().':'.$e->getLine().': '.$e->getMessage();
	    $this->db->rollBack();
	}




	// Posibilidad de reubicar en gf_unit_has_users?
	// Guardar páginas si hay
	// if (array_key_exists('themes', $request)) {
	// 	unset($request['themes']);
	// }

	return $response->withJson($request);
});

/*
 * @danieljx
 */
 $app->get('/user/search[/]', 'UserController:getUserSearch')->setName('user-search');
 $app->get('/user/{user-id:[0-9]+}/users[/]', 'UserController:getUsers')->setName('users');
 $app->get('/user/{user-id:[0-9]+}/users/list/[{page:[0-9]+}[/{perPage:[0-9]+}]]', 'UserController:getUsersList')->setName('users-list');
 $app->put('/user/{user-id:[0-9]+}/users/{user:[0-9]+}', 'UserController:setUser')->setName('users-put');
 $app->post('/user/{user-id:[0-9]+}/users/[{user:[0-9]+}]', 'UserController:setUser')->setName('users-post');
 $app->delete('/user/{user-id:[0-9]+}/users/[{user:[0-9]+}]', 'UserController:delUser')->setName('users-del');
