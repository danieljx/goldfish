<?php
namespace App\Models;

use \Illuminate\Database\Eloquent as Eloquent;

class ProjectModel extends Eloquent\Model {
    protected $table = 'gf_projects';

    public function getSearchProject($q = "") {
        $listProjects   = array();
        $whereArr       = array();
        $whereArr[]     = ['title', 'LIKE', "%{$q}%"];
        $data = $this->where($whereArr)->get();
        if(count($data)) {
            foreach($data as $x => $project) {
                $dataProject = $project->toArray();
                $listProjects[] = $dataProject;
            }
        }
        return $listProjects;
    }
}