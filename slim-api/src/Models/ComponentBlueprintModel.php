<?php
namespace App\Models;

use \Illuminate\Database\Eloquent as Eloquent;

class ComponentBlueprintModel extends Eloquent\Model {
  protected $table = 'gf_component_blueprints';
  protected $fillable = ['body','title','repeatable','type','url','name','sass','version','data'];
}
