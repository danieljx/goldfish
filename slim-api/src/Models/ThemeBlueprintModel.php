<?php
namespace App\Models;

use \Illuminate\Database\Eloquent as Eloquent;

class ThemeBlueprintModel extends Eloquent\Model {
  protected $table = 'gf_theme_blueprints';
  protected $fillable = ['name','title','folder','description','includes','sass','unitsass','version'];
}
