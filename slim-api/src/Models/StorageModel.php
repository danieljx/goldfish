<?php
namespace App\Models;

use \Illuminate\Database\Eloquent as Eloquent;

class StorageModel extends Eloquent\Model {
  protected $table = 'gf_storages';

  public function setStorage($data) {
    $this->setValues($data);
    $this->save();
  }

  public function getStorageData($idUser = 0) {
    $data = $this->where([['user_id', '=', $idUser]])->get();
    $data = (count($data)?$data[0]:$data);
    return $data->toArray();
  }

  public function getStorageDataById($id = 0) {
    $data = $this->where([['id', '=', $id]])->get();
    $data = (count($data)?$data[0]:$data);
    return $data->toArray();
  }

  private function setValues($data) {
    if(count($data)) {
      foreach($data as $key => $val) {
        $this->$key = $val;
      }
    }
  }

}
