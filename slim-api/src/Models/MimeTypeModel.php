<?php
namespace App\Models;

use \Illuminate\Database\Eloquent as Eloquent;

class MimeTypeModel extends Eloquent\Model {
  protected $table = 'gf_mimetypes';

  public function getMimeTypeData($idType = 0) {
    $data = $this->where([['id', '=', $idType]])->get();
    $data = (count($data)?$data[0]:$data);
    return $data->toArray();
  }

  public function getMimeTypeExtData($ext = "") {
    $data = $this->where([['ext', '=', $ext]])->get();
    $data = (count($data)?$data[0]:$data);
    return $data->toArray();
  }

  public function getContentTypeExt($ext = "") {
    $data = $this->where([['ext', '=', $ext]])->get();
    $data = (count($data)?$data[0]:$data);
    $data = $data->toArray();
    return $data["mimetype"];
  }
}