<?php
namespace App\Models;

use \Illuminate\Database\Eloquent as Eloquent;

class FileModel extends Eloquent\Model {
  protected $table      = 'gf_files';
  protected $pathBase   = 'data';
  protected $pathFolder = 'data';
  protected $partial    = [];
  protected $storage    = [];
  protected $children   = [];
  protected $shared     = [];
  protected $breadcrumb = [];
  protected $MimeTypeModel;
  protected $SharedModel;
  protected $UserModel;
  protected $ProjectModel;

  public function tagsFile() {
      return $this->hasMany('App\Models\FileTagsModel', 'file_id');
  }
  private function getHash($str) {
    return md5($str);
  }
  public function setStorage($storage = array()) {
      $this->storage      = $storage;
      $this->pathBase     = $this->getStoragePath($this->storage["user_path"]);
  }
  private function getStoragePath($pathStorage = "") {
      $path = explode("::",$pathStorage);
      return (count($path)?$path[1]:$pathStorage);
  }
  public function setMimeTypeModel($MimeTypeModel) {
      $this->MimeTypeModel = $MimeTypeModel;
  }
  public function setSharedModel($SharedModel) {
      $this->SharedModel = $SharedModel;
  }
  public function setUserModel($UserModel) {
      $this->UserModel = $UserModel;
  }
  public function getSearchFileGallery($qName = "") {
    $listFiles = array();
    $qNameArr   = preg_split('/\s+/', $qName);
    $whereArr   = array();
    $whereArr[] = ['gf_files.storage_id', '=', $this->storage["id"]];
    $data = $this->where($whereArr)
                 ->whereIn('gf_files.mimetype', array(7,8,10,30))
                 ->where(function ($query) use ($qNameArr) {
                    if(count($qNameArr)) {
                      foreach($qNameArr as $i => $w) {
                        $query->orWhere('gf_files.name', 'LIKE', '%' . $w . '%');
                      }
                    }
                    $query->orWhereNotNull('gf_tags.id');
                  })
                  ->leftJoin('gf_files_tags', 'gf_files_tags.file_id', '=', 'gf_files.id')
                  ->leftJoin('gf_tags', function($query) use ($qName, $qNameArr) {
                    $query->on('gf_tags.id', '=', 'gf_files_tags.tag_id');
                    $query->where(function ($query) use ($qNameArr) {
                      if(count($qNameArr)) {
                        foreach($qNameArr as $i => $w) {
                          $query->orWhere('gf_tags.name', 'LIKE', '%' . $w . '%')
                                ->orWhere('gf_tags.key', 'LIKE', '%' . $w . '%');
                        }
                      }
                    });
                  })
                  ->groupBy('gf_files.id')
                  ->get(['gf_files.*']);
                  //->toSql();
    //die($data);
    if(count($data)) {
      foreach($data as $x => $file) {
        $dataItem = $file->toArray();
        $dataItem['breadcrumb']   = array();
        $dataItem['breadcrumb'][] = array("id" => $dataItem["id"], "name" => $dataItem["name"], "path_hash" => $dataItem["path_hash"]);
        $this->getFilesBreadcrumb($dataItem['parent'], $dataItem['breadcrumb']);
        $this->getFilesBreadcrumbClean($dataItem['breadcrumb']);
        $listFiles[] = $dataItem;
      }
    }
    return $listFiles;
  }
  public function getSearchSharedFileGallery($userId, $qName = "") {
    $listFiles  = array();
    $qNameArr   = preg_split('/\s+/', $qName);
    $whereArr   = array();
    $whereArr[] = ['gf_files_share.shared_id', '=', $userId];
    $whereArr[] = ['gf_files_share.share_type', '=', 1];
    $filesData  = $this->where($whereArr)
                      ->whereIn('gf_files.mimetype', array(7,8,10,30))
                      ->where(function ($query) use ($qNameArr) {
                        if(count($qNameArr)) {
                          foreach($qNameArr as $i => $w) {
                            $query->orWhere('gf_files.name', 'LIKE', '%' . $w . '%');
                          }
                        }
                        $query->orWhereNotNull('gf_tags.id');
                      })
                      ->join('gf_files_share', 'gf_files.id', '=', 'gf_files_share.file_id')
                      ->leftJoin('gf_files_tags', 'gf_files_tags.file_id', '=', 'gf_files.id')
                      ->leftJoin('gf_tags', function($query) use ($qName, $qNameArr) {
                        $query->on('gf_tags.id', '=', 'gf_files_tags.tag_id');
                        $query->where(function ($query) use ($qNameArr) {
                          if(count($qNameArr)) {
                            foreach($qNameArr as $i => $w) {
                              $query->orWhere('gf_tags.name', 'LIKE', '%' . $w . '%')
                                    ->orWhere('gf_tags.key', 'LIKE', '%' . $w . '%');
                            }
                          }
                        });
                      })
                      ->groupBy('gf_files.id')
                      ->get(['gf_files.*']);
    if(count($filesData)) {
      foreach($filesData as $x => $file) {
        $dataItem = $file->toArray();
        $dataItem['breadcrumb']   = array();
        $dataItem['breadcrumb'][] = array("id" => $dataItem["id"], "name" => $dataItem["name"], "path_hash" => $dataItem["path_hash"]);
        $this->getFilesBreadcrumb($dataItem['parent'], $dataItem['breadcrumb']);
        $this->getFilesBreadcrumbClean($dataItem['breadcrumb']);
        $listFiles[] = $dataItem;
      }
    }
    return $listFiles;
  }
  public function getSearchPublicFile($qName) {
    $listFiles  = array();
    $qNameArr   = preg_split('/\s+/', $qName);
    $whereArr   = array();
    //$whereArr[] = ['gf_files.name', 'LIKE', "%{$qName}%"];
    $whereArr[] = ['gf_files.storage_id', '=', $this->storage["id"]];
    $filesData  = $this->where($whereArr)
                      ->where(function ($query) use ($qNameArr) {
                        if(count($qNameArr)) {
                          foreach($qNameArr as $i => $w) {
                            $query->orWhere('gf_files.name', 'LIKE', '%' . $w . '%');
                          }
                        }
                        $query->orWhereNotNull('gf_tags.id');
                      })
                      ->leftJoin('gf_files_tags', 'gf_files_tags.file_id', '=', 'gf_files.id')
                      ->leftJoin('gf_tags', function($query) use ($qName, $qNameArr) {
                        $query->on('gf_tags.id', '=', 'gf_files_tags.tag_id');
                        $query->where(function ($query) use ($qNameArr) {
                          if(count($qNameArr)) {
                            foreach($qNameArr as $i => $w) {
                              $query->orWhere('gf_tags.name', 'LIKE', '%' . $w . '%')
                                    ->orWhere('gf_tags.key', 'LIKE', '%' . $w . '%');
                            }
                          }
                        });
                      })
                      ->groupBy('gf_files.id')
                      ->get(['gf_files.*']);
    if(count($filesData)) {
      foreach($filesData as $x => $file) {
        $dataItem = $file->toArray();
        $dataItem['breadcrumb']   = array();
        $dataItem['breadcrumb'][] = array("id" => $dataItem["id"], "name" => $dataItem["name"], "path_hash" => $dataItem["path_hash"]);
        $this->getFilesBreadcrumb($dataItem['parent'], $dataItem['breadcrumb']);
        $this->getFilesBreadcrumbClean($dataItem['breadcrumb']);
        $listFiles[] = $dataItem;
      }
    }
    return $listFiles;
  }
  public function getSearchSharedFile($userId, $qName = "") {
    $listFiles  = array();
    $qNameArr   = preg_split('/\s+/', $qName);
    $whereArr   = array();
    $whereArr[] = ['gf_files_share.shared_id', '=', $userId];
    $whereArr[] = ['gf_files_share.share_type', '=', 1];
    $filesData  = $this->where($whereArr)
                      ->where(function ($query) use ($qNameArr) {
                        if(count($qNameArr)) {
                          foreach($qNameArr as $i => $w) {
                            $query->orWhere('gf_files.name', 'LIKE', '%' . $w . '%');
                          }
                        }
                        $query->orWhereNotNull('gf_tags.id');
                      })
                      ->join('gf_files_share', 'gf_files.id', '=', 'gf_files_share.file_id')
                      ->leftJoin('gf_files_tags', 'gf_files_tags.file_id', '=', 'gf_files.id')
                      ->leftJoin('gf_tags', function($query) use ($qName, $qNameArr) {
                        $query->on('gf_tags.id', '=', 'gf_files_tags.tag_id');
                        $query->where(function ($query) use ($qNameArr) {
                          if(count($qNameArr)) {
                            foreach($qNameArr as $i => $w) {
                              $query->orWhere('gf_tags.name', 'LIKE', '%' . $w . '%')
                                    ->orWhere('gf_tags.key', 'LIKE', '%' . $w . '%');
                            }
                          }
                        });
                      })
                      ->groupBy('gf_files.id')
                      ->get(['gf_files.*']);
    if(count($filesData)) {
      foreach($filesData as $x => $file) {
        $dataItem = $file->toArray();
        $dataItem['breadcrumb']   = array();
        $dataItem['breadcrumb'][] = array("id" => $dataItem["id"], "name" => $dataItem["name"], "path_hash" => $dataItem["path_hash"]);
        $this->getFilesBreadcrumb($dataItem['parent'], $dataItem['breadcrumb']);
        $this->getFilesBreadcrumbClean($dataItem['breadcrumb']);
        $listFiles[] = $dataItem;
      }
    }
    return $listFiles;
  }
  public function getSearchFile($pathHash, $q = "") {
    $listFiles = array();
    if($this->storage["id"]) {
      $dataFile = $this->getFileByHash($pathHash);
      $data     = $dataFile->toArray();
      if(count($data) < 1) {
        $dataFile = $this->getFileByPath($pathHash);
        $data     = $dataFile->toArray();
      }
      if(count($data) < 1) {
        $dataFile = $this->getFileById($pathHash);
        $data     = $dataFile->toArray();
      }
      if(count($data)) {
        $this->getFilesAllChildDataList($data[0], $listFiles, $q);
      }
    }
    return $listFiles;
  }
  private function getFilesAllChildDataList($dataParent, &$children, $qName) {
    $qNameArr   = preg_split('/\s+/', $qName);
    $whereArr   = array();
    $whereArr[] = ['gf_files.parent', '=', $dataParent["id"]];
    $whereArr[] = ['gf_files.storage_id', '=', $this->storage["id"]];
    $data = $this->where($whereArr)
                 ->where(function ($query) use ($qNameArr) {
                    if(count($qNameArr)) {
                      foreach($qNameArr as $i => $w) {
                        $query->orWhere('gf_files.name', 'LIKE', '%' . $w . '%');
                      }
                    }
                    $query->orWhereNotNull('gf_tags.id');
                  })
                  ->leftJoin('gf_files_tags', 'gf_files_tags.file_id', '=', 'gf_files.id')
                  ->leftJoin('gf_tags', function($query) use ($qName, $qNameArr) {
                    $query->on('gf_tags.id', '=', 'gf_files_tags.tag_id');
                    $query->where(function ($query) use ($qNameArr) {
                      if(count($qNameArr)) {
                        foreach($qNameArr as $i => $w) {
                          $query->orWhere('gf_tags.name', 'LIKE', '%' . $w . '%')
                                ->orWhere('gf_tags.key', 'LIKE', '%' . $w . '%');
                        }
                      }
                    });
                  })
                  ->groupBy('gf_files.id')
                  ->get(['gf_files.*']);
    if(count($data)) {
      foreach($data as $x => $file) {
        $dataItem = $file->toArray();
        $dataItem['breadcrumb']   = array();
        $dataItem['breadcrumb'][] = array("id" => $dataItem["id"], "name" => $dataItem["name"], "path_hash" => $dataItem["path_hash"]);
        $this->getFilesBreadcrumb($dataItem['parent'], $dataItem['breadcrumb']);
        $this->getFilesBreadcrumbClean($dataItem['breadcrumb']);
        $children[] = $dataItem;
      }
    }
    $whereArrFolder = array();
    $whereArrFolder[] = ['parent', '=', $dataParent["id"]];
    $whereArrFolder[] = ['mimetype', '=', "2"];
    $whereArrFolder[] = ['storage_id', '=', $this->storage["id"]];
    $dataFolder = $this->where($whereArrFolder)->get();
    if(count($dataFolder)) {
      foreach($dataFolder as $xFolder => $fileFolder) {
        $dataItemFolder = $fileFolder->toArray();
        $this->getFilesAllChildDataList($dataItemFolder, $children, $qName);
      }
    }
  }
  public function getFile($pathHash, $q = "") {
    if($this->storage["id"]) {
      $dataFile = $this->getFileByHash($pathHash);
      $data     = $dataFile->toArray();

      if(count($data) < 1) {
        $dataFile = $this->getFileByPath($pathHash);
        $data     = $dataFile->toArray();
      }
      if(count($data) < 1 && is_numeric($pathHash)) {
        $dataFile = $this->getFileById($pathHash);
        $data     = $dataFile->toArray();
      }
      if(count($data)) {
        $dataR = $this->formatDataResponse($data[0]);
        $dataR['children']    = array();
        $dataR['breadcrumb']  = array();
        $dataR['shared']      = array();
        $dataR['shared_user'] = array();
        $dataR['tags']        = array();
        $dataR['breadcrumb'][] = array("id" => $dataR["id"], "name" => $dataR["name"], "path_hash" => $dataR["path_hash"]);
        if($dataR["name"] == 'files') {
          $this->getFilesPublic($dataR['children']);
          $this->getFilesShered($dataR['children']);
        }
        if($dataR['mimetype'] == 2) {
          $this->getFilesChildren($dataR['id'], $dataR['children'], $q);
        } else {
          $this->getFilesTags($dataR['id'], $dataR['tags']);
        }
        $this->getFilesBreadcrumb($dataR['parent'], $dataR['breadcrumb']);
        $this->getFilesBreadcrumbClean($dataR['breadcrumb']);
        $this->getUserShered($dataR['shared_user'], $this->storage["user_id"], $dataR['id']);
        return $dataR;
      } else if (!$data and is_string($pathHash)) {
        if (isset($this->partial[$pathHash])) {
          $data = $this->partial[$pathHash];
        }
        return $data;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }
  private function getFilesPublic(&$children = array()) {
    $whereArr = array();
    $whereArr[] = ['storage_id', '=', 1];
    $whereArr[] = ['path', '=', 'files'];
    $publicData = $this->where($whereArr)->get();
    $publicData = $publicData->toArray();
    if(count($publicData)) {
      $dataR = array();
      $dataR['id']               = -2;
      $dataR['name']             = 'public';
      $dataR['path']             = 'public';
      $dataR['path_hash']        = 'public/';
      $dataR['parent']           = -2;
      $dataR['size']             = 0;
      $dataR['mtime']            = 0;
      $dataR['storage_mtime']    = 0;
      $dataR['encrypted']        = false;
      $dataR['storage_id']       = 1;
      if($dataR['storage_mtime'] == 0) {
        $dataR['storage_mtime']  = 1;
      }
      $dataR['permissions']      = 65;
      $dataR['mimetype']         = 2;
      $dataR['date_changed']     = date("Y-m-d H:i:s");
      $dataR['date_created']     = date("Y-m-d H:i:s");
      $children[] = $dataR;
    }
  }
  private function getFilesShered(&$children = array()) {
    $whereArr = array();
    $whereArr[] = ['shared_id', '=', $this->storage["user_id"]];
    $whereArr[] = ['share_type', '=', 1];
    $dataShared = $this->SharedModel->where($whereArr)->get();
    $dataShared = $dataShared->toArray();
    if(count($dataShared)) {
      $dataR = array();
      $dataR['id']               = -1;
      $dataR['name']             = 'shared';
      $dataR['path']             = 'shared';
      $dataR['path_hash']        = 'shared/';
      $dataR['parent']           = -1;
      $dataR['size']             = 0;
      $dataR['mtime']            = 0;
      $dataR['storage_mtime']    = 0;
      $dataR['encrypted']        = false;
      $dataR['storage_id']       = $this->storage["id"];
      if($dataR['storage_mtime'] == 0) {
        $dataR['storage_mtime']  = 1;
      }
      $dataR['permissions']      = 65;
      $dataR['mimetype']         = 2;
      $dataR['date_changed']     = date("Y-m-d H:i:s");
      $dataR['date_created']     = date("Y-m-d H:i:s");
      $children[] = $dataR;
    }
  }
  public function getFileByHash($pathHash = "", $out = false) {
    $whereArr = array();
    $whereArr[] = ['path_hash', '=', (String)$pathHash];
    if(!$out) {
      $whereArr[] = ['storage_id', '=', $this->storage["id"]];
    }
    return $this->where($whereArr)->get();
  }
  public function getFileByPath($path = "", $out = false) {
    $whereArr = array();
    $whereArr[] = ['path', '=', (String)$path];
    if(!$out) {
      $whereArr[] = ['storage_id', '=', $this->storage["id"]];
    }
    return $this->where($whereArr)->get();
  }
  public function getFileByRealPath($path = "", $out = false) {
    $whereArr = array();
    $whereArr[] = ['path_hash', '=', $this->getHash($path)];
    if(!$out) {
      $whereArr[] = ['storage_id', '=', $this->storage["id"]];
    }
    return $this->where($whereArr)->get();
  }
  public function getFileById($idFile = "") {
    return $this->where([['id', '=', $idFile]])->get();
  }
  private function getFilesTags($idFile = "", &$tags = array()) {
    //$tagsData = $this->tagsFile()->with('file')->with('tags')->get(['gf_tags.*']);
    $tagsData = $this->where([['gf_files.id', '=', $idFile]])->join('gf_files_tags', 'gf_files.id', '=', 'gf_files_tags.file_id')->join('gf_tags', 'gf_tags.id', '=', 'gf_files_tags.tag_id')->get(['gf_files_tags.*', 'gf_tags.name', 'gf_tags.key', 'gf_tags.status', 'gf_tags.type']);
    // ['gf_files_tags.*, gf_tags.name, gf_tags.key, gf_tags.status, gf_tags.type']
    //$tagsData = $this->tagsFile()->with('file')->get();
    //die(var_dump($tagsData->toArray()));
    if(count($tagsData)) {
      foreach($tagsData as $x => $tag) {
        $tags[] = $tag->toArray();
      }
    }
    return $tags;
  }
  private function getFilesChildren($idParent = "", &$children = array(), $qName = "") {
    $whereArr = array();
    $whereArr[] = ['parent', '=', $idParent];
    if(!empty($qName)) {
      $whereArr[] = ['name', 'LIKE', "%{$qName}%"];
    }
    $data = $this->where($whereArr)->get();
    if(count($data)) {
      foreach($data as $x => $file) {
        $dataItem = $file->toArray();
        $dataItem = $this->formatDataResponse($dataItem);
        $dataItem['shared_user'] = array();
        $this->getUserShered($dataItem['shared_user'], $this->storage["user_id"], $dataItem['id']);
        $dataItem['tags']        = array();
        if($dataItem['mimetype'] != 2) {
          $this->getFilesTags($dataItem['id'], $dataItem['tags']);
        }
        $children[] = $dataItem;
      }
    }
  }
  private function getFilesBreadcrumb($idParent = "", &$breadcrumb = array()) {
      $data = $this->where([['id', '=', $idParent]])->get();
      if(count($data)) {
        foreach($data as $x => $file) {
          $dataItem = $file->toArray();
          $breadcrumb[] = array("id" => $dataItem["id"], "name" => $dataItem["name"], "path_hash" => $dataItem["path_hash"]);
          $this->getFilesBreadcrumb($dataItem["parent"], $breadcrumb);
        }
      }
  }
  private function getFilesBreadcrumbClean(&$breadcrumb = array()) {
    if(count($breadcrumb)) {
      foreach($breadcrumb as $x => $item) {
        if(empty($item["name"])) {
          unset($breadcrumb[$x]);
        }
        if($item["name"] == "files") {
          $breadcrumb[$x]["name"]      = "Home";
          $breadcrumb[$x]["path_hash"] = "";
        }
      }
      $breadcrumb = array_reverse($breadcrumb);
    }
  }
  private function formatDataResponse($data) {
    $dataR = array();
    $dataR['id']               = (int)$data['id'];
    $dataR['name']             = $data['name'];
    $dataR['path']             = $data['path'];
    $dataR['path_hash']        = $data['path_hash'];
    $dataR['parent']           = (int)$data['parent'];
    $dataR['size']             = 0 + $data['size'];
    $dataR['mtime']            = (int)$data['mtime'];
    $dataR['storage_mtime']    = (int)$data['storage_mtime'];
    $dataR['encrypted']        = (bool)$data['encrypted'];
    $dataR['storage_id']       = $this->storage["id"];
    if($data['storage_mtime'] == 0) {
      $dataR['storage_mtime']  = $data['mtime'];
    }
    $dataR['permissions']      = (int)$data['permissions'];
    $dataR['mimetype']         = (int)$data['mimetype'];
    $dataR['date_changed']     = $data['updated_at'];
    $dataR['date_created']     = $data['created_at'];
    return $dataR;
  }
  public function setFile($dataFile = array()) {
    $this->setValues($dataFile);
    $this->save();
  }
  public function updateFile($dataFile = array()) {
    $this->setValues($dataFile);
    $this->exists = true;
    $this->save();
  }
  public function updateFolder($dataFile, $pathOld, $firts = true) {
    unset($dataFile["date_created"]);
    unset($dataFile["date_changed"]);
    unset($dataFile["fileBlob"]);
    unset($dataFile["breadcrumb"]);
    unset($dataFile["icon"]);
    unset($dataFile["edit"]);
    unset($dataFile["checked"]);
    unset($dataFile["children"]);
    unset($dataFile["shared_user"]);
    unset($dataFile["shared"]);
    unset($dataFile["share"]);
    unset($dataFile["tags"]);
    $files = $this->where([['parent', '=', $dataFile["id"]]])->get();
    if(count($files)) {
      foreach($files as $x => $file) {
        $dataChild          = $file->toArray();
        $dataChild['path']       = str_replace($pathOld, $dataFile['path'], $dataChild['path']);
        $dataChild['path_hash']  = md5($this->pathBase . DIRECTORY_SEPARATOR . $dataChild['path']);
        $file->path              = $dataChild['path'];
        $file->path_hash         = $dataChild['path_hash'];
        $file->exists = true;
        $file->save();
        if($file->mimetype == 2) {
          $this->updateFolder($file->toArray(), $pathOld . DIRECTORY_SEPARATOR . $dataChild['name'], false);
        }
      }
    }
    if($firts && $this->where([['id', '=', $dataFile["id"]]])->update($dataFile)) {
        return $this->getFile($dataFile['path_hash']);
    }
  }
  public function delFile($dataFile = array()) {
    $files = $this->where([['parent', '=', $dataFile["id"]]])->get();
    if(count($files)) {
      foreach($files as $x => $file) {
        if($file->mimetype == 2) {
          $this->delFile($file->toArray());
        } else {
          $file->delete();
        }
      }
    }
    $this->where([['id', '=', $dataFile["id"]]])->delete();
  }
  private function setValues($data) {
    if(count($data)) {
      foreach($data as $key => $val) {
        if($key != 'tags' && $key != 'share' && $key != 'shared' && $key != 'share_others' && $key != 'share_details' && $key != 'shared_user' && $key != 'date_created' && $key != 'date_changed' && $key != 'fileBlob' && $key != 'breadcrumb' && $key != 'icon' && $key != 'edit' && $key != 'checked') {
          $this->$key = $val;
        }
      }
    }
  }
  private function getUserShered(&$dataShared = array(), $owner = null, $fileId = null) {
    $this->SharedModel  = new FileShareModel();
    $shared = $this->SharedModel->where([['file_id', '=', $fileId]])->get();
    if(count($shared)) {
      foreach($shared as $x => $sharedItem) {
        $this->UserModel    = new UserModel();
        $this->ProjectModel = new ProjectModel();
        $dataSharedItem = $sharedItem->toArray();
        if($dataSharedItem["share_type"] == '1') {
          $shareUser  = $this->UserModel->where([['id', '=', $dataSharedItem["shared_id"]]])->get();
          $dataSharedItem['share_user']     = (count($shareUser)?$shareUser[0]->toArray():[]);
          $dataSharedItem['share_project']  = array();
        } else {
          $shareProject  = $this->ProjectModel->where([['id', '=', $dataSharedItem["shared_id"]]])->get();
          $dataSharedItem['share_project']  = (count($shareProject)?$shareProject[0]->toArray():[]);
          $dataSharedItem['share_user']     = array();
        }
        $shareOwner = $this->UserModel->where([['id', '=', $dataSharedItem["owner_id"]]])->get();
        $dataSharedItem['share_owner']     = (count($shareOwner)?$shareOwner[0]->toArray():[]);
        $shareInit  = $this->UserModel->where([['id', '=', $dataSharedItem["initiator_id"]]])->get();
        $dataSharedItem['share_initiator'] = (count($shareInit)?$shareInit[0]->toArray():[]);
        $dataShared[] = $dataSharedItem;
      }
    }
  }
}
