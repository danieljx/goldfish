<?php
namespace App\Models;

use \Illuminate\Database\Eloquent as Eloquent;

class UserModel extends Eloquent\Model {
  protected $table = 'gf_users';
  
  private function setValues($data) {
    if(count($data)) {
      foreach($data as $key => $val) {
        if($key != 'config' && $key != 'action' && $key != 'profileBlob' && $key != 'storage') {
          $this->$key = $val;
        }
      }
    }
  }

  public function getUser($userId) {
    $listUsers  = array();
    $dataUser = $this->where([['id', '=', $userId]])->get();
    $dataUser = $dataUser->toArray();
    return (count($dataUser)?$dataUser[0]:[]);
  }

  public function getUsers() {
    $listUsers  = array();
    $data = $this->selectRaw('gf_users.*, gf_user_configs.id AS config_id, gf_user_configs.editor, gf_user_configs.theme, gf_user_configs.effects')
                 ->leftJoin('gf_user_configs', 'gf_user_configs.gf_users_id', '=', 'gf_users.id')
                 ->orderBy('gf_users.id', 'DESC')
                 ->get();
    if(count($data)) {
      foreach($data as $x => $user) {
        $dataUser = $user->toArray();
        $dataUser['config'] = array( 'id' => $dataUser['config_id']
                                    ,'editor' => $dataUser['editor']
                                    ,'theme' => $dataUser['theme']
                                    ,'effects' => $dataUser['effects'] );
        unset($dataUser['password']);
        unset($dataUser['password_salt']);
        unset($dataUser['token']);
        unset($dataUser['token_exp']);
        unset($dataUser['config_id']);
        unset($dataUser['editor']);
        unset($dataUser['theme']);
        unset($dataUser['effects']);
        $listUsers[] = $dataUser;
      }
    }
    return $listUsers;
  }

  public function getUsersList($start = 0, $end = 0) {
    $listUsers  = array();
    $data = $this->selectRaw('gf_users.*, gf_user_configs.id AS config_id, gf_user_configs.editor, gf_user_configs.theme, gf_user_configs.effects')
                 ->leftJoin('gf_user_configs', 'gf_user_configs.gf_users_id', '=', 'gf_users.id')
                 ->orderBy('gf_users.id', 'DESC')
                 ->skip($start)
                 ->take($end)
                 ->get();
    if(count($data)) {
      foreach($data as $x => $user) {
        $dataUser = $user->toArray();
        $dataUser['config'] = array( 'id' => $dataUser['config_id']
                                    ,'editor' => $dataUser['editor']
                                    ,'theme' => $dataUser['theme']
                                    ,'effects' => $dataUser['effects'] );
        unset($dataUser['password']);
        unset($dataUser['password_salt']);
        unset($dataUser['token']);
        unset($dataUser['token_exp']);
        unset($dataUser['config_id']);
        unset($dataUser['editor']);
        unset($dataUser['theme']);
        unset($dataUser['effects']);
        $listUsers[] = $dataUser;
      }
    }
    return $listUsers;
  }

  public function getSearchUser($q = "") {
    $listUsers  = array();
    $whereArr   = array();
    $whereArr[] = ['names', 'LIKE', "%{$q}%"];
    $data = $this->where($whereArr)->get();
    if(count($data)) {
      foreach($data as $x => $user) {
        $dataUser = $user->toArray();
        unset($dataUser['password']);
        unset($dataUser['password_salt']);
        unset($dataUser['token']);
        unset($dataUser['token_exp']);
        $listUsers[] = $dataUser;
      }
    }
    return $listUsers;
  }

  public function delUserById($userId) {
    $dataUser = $this->where([['id', '=', $userId]])->delete();
    return $dataUser;
  }

  public function setUser($dataUser = array()) {
    $this->setValues($dataUser);
    $this->timestamps = false;
    $this->save();
  }

  public function updateUser($dataUser = array()) {
    $this->setValues($dataUser);
    $this->exists     = true;
    $this->timestamps = false;
    $this->save();
  }
}
