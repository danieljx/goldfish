<?php
namespace App\Models;

use \Illuminate\Database\Eloquent as Eloquent;
use \Illuminate\Database\Capsule\Manager as DB;

class UserConfigModel extends Eloquent\Model {
  protected $table = 'gf_user_configs';

  private function setValues($data) {
    if(count($data)) {
      foreach($data as $key => $val) {
        //if($key != 'tag_action' && $key != 'tag_id' && $key != 'file_id') {
          $this->$key = $val;
        //}
      }
    }
  }
  public function getConfig($configId) {
    $dataConfig = $this->where([['id', '=', $configId]])->get();
    $dataConfig = $dataConfig->toArray();
    return (count($dataConfig)?$dataConfig[0]:[]);
  }
  public function setConfig($dataConfig = array()) {
    $this->setValues($dataConfig);
    $this->timestamps = false;
    $this->save();
  }
  public function updateConfig($dataConfig = array()) {
    $this->setValues($dataConfig);
    $this->exists = true;
    $this->timestamps = false;
    $this->save();
  }
  public function delUserById($userId) {
    $dataUser = $this->where([['gf_users_id', '=', $userId]])->delete();
    return $dataUser;
  }
}
