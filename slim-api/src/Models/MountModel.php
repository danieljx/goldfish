<?php
namespace App\Models;

use \Illuminate\Database\Eloquent as Eloquent;

class MountModel extends Eloquent\Model {
  protected $table = 'gf_mounts';

  public function setMount($data) {
    $this->setValues($data);
    $this->save();
  }

  public function getMountData($idStorage = 0) {
    $data = $this->where([['storage_id', '=', $idStorage]])->get();
    $data = (count($data)?$data[0]:$data);
    return $data->toArray();
  }

  private function setValues($data) {
    if(count($data)) {
      foreach($data as $key => $val) {
        $this->$key = $val;
      }
    }
  }

}
