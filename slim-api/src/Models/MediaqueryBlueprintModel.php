<?php
namespace App\Models;

use \Illuminate\Database\Eloquent as Eloquent;

class MediaqueryBlueprintModel extends Eloquent\Model {
  protected $table = 'gf_mediaquery_blueprints';
  protected $fillable = ['media_query','body_css','gf_component_blueprints_id','gf_theme_blueprints_id'];

  protected $casts = [
    'body_css' => 'array',
];
}
