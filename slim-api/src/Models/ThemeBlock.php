<?php
namespace App\Models;

use \Illuminate\Database\Eloquent as Eloquent;

class ThemeBlockModel extends Eloquent\Model {
	public $id;
	public $name;
	public $body = '';
	public $title = '';
	public $repeatable = '';
	public $type = 1;
	public $url = '';
	public $sass = '';
	public $version = 0;
	public $data = array();
	private $folder;
	private $db;
	// private $baseDir = PHYSDIR;

	public function __construct() {
		// $this->physDir = $this->baseDir . '/components/blocks';
		$this->db = getDb();
	}

	// public function getFromDir($folder) {
	// 	$this->folder = $folder;
	// 	$dir = array_diff(scandir($this->physDir.'/'.$this->folder), array('..', '.','.gitignore'));
		
	// 	if (in_array('fnord.htm',$dir) && in_array('fnord.scss',$dir) && in_array('metadata.json',$dir)) {
	// 		$this->metadata = file_get_contents($this->physDir.'/'.$this->folder.'/metadata.json');
	// 		$this->html = file_get_contents($this->physDir.'/'.$this->folder.'/fnord.htm');
	// 		$this->sass = file_get_contents($this->physDir.'/'.$this->folder.'/fnord.scss');
	// 		$metadataJson = json_decode($this->metadata, true);
	// 		$this->name = $metadataJson['name'];
	// 		$this->title = $metadataJson['title'];
	// 		return true;
	// 	} else {
	// 		return false;
	// 	}
	// }

	public function getFromDB() {
		$sth = $this->db->prepare("SELECT 
				id,
				name,
				title,
				type,
				url,
				sass,
				data,
				version,
				repeatable
				FROM gf_component_blueprints
				WHERE id = :id");
    		$sth->bindParam("id", $this->id, PDO::PARAM_INT);

			$sth->execute();
    		$items = $sth->fetchALL(PDO::FETCH_ASSOC);
    		if (count($items) > 0) {
    			$this->populate($items[0]);
    			// $this->id = $items[0]["id"];
    			return true;
    		} else {
    			return false;	
    		}
    		
	}

	public function toJSON() {
		return json_encode([
			'id' => $this->id, 
			'name' => $this->name, 
			'title' => $this->title, 
			'type' => $this->type,
			'url' => $this->url,
			'sass' => $this->sass,
			'data' => $this->data,
			'body' => $this->body,
			'version' => $this->version,
			'repeatable' => $this->repeatable
			]);
	}

	public function populate($arr) {
		$this->id = $arr['id']*1;
		$this->name = $arr['name'];
		$this->title = $arr['title'];
		$this->type = $arr['type'];
		$this->url = $arr['url'] ? $arr['url'] : '';
		$this->sass = $arr['sass'] ? $arr['sass'] : '';
		$this->data = $arr['data'] ? $arr['data'] : '';
		$this->body = $arr['body'];
		$this->version = $arr['version'];
		$this->repeatable = $arr['repeatable'] ? $arr['repeatable'] : '';
	}

	public function saveDB() {
		$res = ['result' => '0', 'error' => ''];
		$update = "UPDATE gf_component_blueprints SET 
			body = :body, 
			title=:title, 
			repeatable=:repeatable,
			type=:type,
			url=:url,
			name=:name,
			sass=:sass,
			data=:data,
			version=version + 1 
			WHERE id=:id";
		$insert = "INSERT INTO gf_component_blueprints (
			body,
			title,
			repeatable,
			type,
			url,
			name,
			sass,
			data,
			version
		) VALUES (
			:body,
			:title,
			:repeatable,
			:type,
			:url,
			:name,
			:sass,
			:data,
			1
		)";
		$sth = $this->db->prepare($this->id > 0 ? $update : $insert);
		$sth->bindParam("body", $this->body);
		$sth->bindParam("title", $this->title);
		$sth->bindParam("repeatable", $this->repeatable);
		$sth->bindParam("type", $this->type);
		$sth->bindParam("url", $this->url);
		$sth->bindParam("name", $this->name);
		$sth->bindParam("sass", $this->sass);
		$sth->bindParam("data", $this->data);
		
		if ($this->id > 0) {
			$sth->bindParam("id", $this->id, PDO::PARAM_INT);
		}

		$res["result"] = $sth->execute();

		if ($res["result"]) {
			if (!$this->id) {
				$this->id = $this->db->lastInsertId();
			}
		} else {
			$res["error"] = $sth->errorInfo();
		}

		return $res;
	}

	public function getByIdAndName() {
    		$sth = $this->db->prepare("SELECT 
    			id,
				name,
				version
				FROM gf_component_blueprints
				WHERE id = :id AND name = :name");
    		$sth->bindParam("id", $this->id, PDO::PARAM_INT);
    		$sth->bindParam("name", $this->name);

			$sth->execute();
    		$items = $sth->fetchALL(PDO::FETCH_ASSOC);
    		if (count($items) > 0) {
    			return true;
    		} else {
    			return false;	
    		}
	}

	public static function getAll() {
		$db = getDb();
        $sth = $db->prepare("SELECT 
				id,
				name,
				title,
				type,
				url,
				sass,
				data,
				body,
				version,
				repeatable
				FROM gf_component_blueprints");
        $sth->execute();
    	$items = $sth->fetchALL(PDO::FETCH_ASSOC);
        $list = array();
        foreach($items as $item) {
            $obj = new self();
            $obj->populate($item);
            $list[$obj->id] = $obj; // Associative array or not... 
        }
        return $list;
    }

}
