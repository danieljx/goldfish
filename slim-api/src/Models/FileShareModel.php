<?php
namespace App\Models;

use \Illuminate\Database\Eloquent as Eloquent;

class FileShareModel extends Eloquent\Model {
  protected $table = 'gf_files_share';

  private function setValues($data) {
    if(count($data)) {
      foreach($data as $key => $val) {
        if($key != 'share_action' && $key != 'share_user' && $key != 'share_project' && $key != 'share_owner' && $key != 'share_initiator') {
          $this->$key = $val;
        }
      }
    }
  }
  public function getFileShareByFile($fileId, $userId) {
    $whereArr   = array();
    $whereArr[] = ['file_id', '=', $fileId];
    $whereArr[] = ['shared_id', '=', $userId];
    $whereArr[] = ['share_type', '=', 1];
    $dataShare  = $this->where($whereArr)->get();
    $dataShare  = $dataShare->toArray();
    return (count($dataShare)?$dataShare[0]:[]);
  }
  public function getFileShare($shareHash) {
    $dataShare = $this->where([['share_hash', '=', $shareHash]])->get();
    return $dataShare->toArray();
  }
  public function getFileShareByUser($userId) {
    $whereArr   = array();
    $whereArr[] = ['shared_id', '=', $userId];
    $whereArr[] = ['share_type', '=', 1];
    $dataShare  = $this->where($whereArr)->get();
    return $dataShare->toArray();
  }
  public function delShare($dataShare = array()) {
    $this->where([['id', '=', $dataShare["id"]]])->delete();
  }
  public function setShare($dataShare = array()) {
    $this->setValues($dataShare);
    $this->save();
    return $this->toArray();
  }
  public function updateShare($dataShare = array()) {
    $this->setValues($dataShare);
    $this->exists = true;
    $this->save();
    return $this->toArray();
  }
}
