<?php
namespace App\Models;

use \Illuminate\Database\Eloquent as Eloquent;
use \Illuminate\Database\Capsule\Manager as DB;

class TagModel extends Eloquent\Model {
  protected $table = 'gf_tags';

  public function fileTags() {
      return $this->belongsTo('App\Models\FilesTagsModel');
  }
  private function setValues($data) {
    if(count($data)) {
      foreach($data as $key => $val) {
        if($key != 'tag_action' && $key != 'tag_id' && $key != 'file_id') {
          $this->$key = $val;
        }
      }
    }
  }
  public function searchTag($name, $key = "") {
    $dataTags = $this->select('gf_tags.*', DB::raw('count(gf_files_tags.file_id) as countFile'))
                     ->where(function ($query) use ($name, $key) {
                       $query->where('name', 'like', '%' . $name . '%')
                             ->orWhere('key', 'like', '%' . $key . '%');
                     })
                     ->leftJoin('gf_files_tags', 'gf_files_tags.tag_id', '=', 'gf_tags.id')
                     ->groupBy('gf_tags.id')
                     ->orderBy('countFile', 'DESC')
                     ->get();
    return $dataTags->toArray();
  }
  public function getTagName($tagName) {
    $dataTags = $this->where([['name', '=', $tagName]])->get();
    $dataTags = $dataTags->toArray();
    return (count($dataTags)?$dataTags[0]:[]);
  }
  public function getTag($tagId) {
    $dataTags = $this->where([['id', '=', $tagId]])->get();
    $dataTags = $dataTags->toArray();
    return (count($dataTags)?$dataTags[0]:[]);
  }
  public function setTag($dataTag = array()) {
    $this->setValues($dataTag);
    $this->save();
    return $this->toArray();
  }
  public function updateShare($dataTag = array()) {
    $this->setValues($dataTag);
    $this->exists = true;
    $this->save();
    return $this->toArray();
  }
}
