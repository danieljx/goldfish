<?php
namespace App\Models;

use \Illuminate\Database\Eloquent as Eloquent;

class FileTagsModel extends Eloquent\Model {
  protected $table = 'gf_files_tags';

  public function file() {
      return $this->belongsTo('App\Models\FileModel');
  }
  public function tags() {
      return $this->hasMany('App\Models\TagsModel', 'tag_id');
  }
  private function setValues($data) {
    if(count($data)) {
      foreach($data as $key => $val) {
        if($key != 'tag_action') {
          $this->$key = $val;
        }
      }
    }
  }
  public function delTags($dataTag = array()) {
    $this->where([['id', '=', $dataTag["id"]]])->delete();
  }
  public function setTags($dataTag = array()) {
    $this->setValues($dataTag);
    $this->save();
    return $this->toArray();
  }
  public function updateTags($dataTag = array()) {
    $this->setValues($dataTag);
    $this->exists = true;
    $this->save();
    return $this->toArray();
  }
}
