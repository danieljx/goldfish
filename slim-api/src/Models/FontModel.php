<?php
namespace App\Models;

use \Illuminate\Database\Eloquent as Eloquent;

class FontModel extends Eloquent\Model {
  protected $table = 'gf_fonts';
  protected $fillable = ['title','file_name','font_type','css_stack','weight'];
  public $timestamps = false;
}
