/* File: gulpfile.js */

const versionConfig = {
    'value': '%MDS%',
    'append': {
      'key': 'v',
      'to': ['css', 'js', 'image'],
    },
  };

// grab our gulp packages
var gulp  = require('gulp'),
    gutil = require('gulp-util'),
    uglify = require('gulp-uglify'),
    sloc = require('gulp-sloc'),
    version = require('gulp-version-number'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    sourcemaps = require('gulp-sourcemaps'),
    htmlmin = require('gulp-htmlmin'),
    cleancss = require('gulp-clean-css'),
    sass = require('gulp-sass'),
    sloc = require('gulp-sloc'),
    notify = require('gulp-notify'),
    jshint = require('gulp-jshint'),
    livereload = require('gulp-livereload'),
    gulpDocumentation = require('gulp-documentation'),
    pump = require('pump');

var jsFiles = [
    'js/views/view.goldfish.js',
    'js/helpers.js',
    'js/models/base/*.js',
    'js/collections/base/*.js',
    'js/models/*.js',
    'js/collections/*.js',
    'js/views/*.js',
    'js/routers/*.js',
    'js/goldfish.js'
];

gulp.task('admin-version', function() {
            return gulp.src('admin/build.php')
              .pipe(version(versionConfig))
              .pipe(rename('index.php'))
              .pipe(gulp.dest('admin'));
});

gulp.task('main-version', function() {
    return gulp.src('build.php')
      .pipe(version(versionConfig))
      .pipe(rename('index.php'))
      .pipe(gulp.dest('.'));
});

gulp.task('tpl-prepare', function() {
    var files = ['templates/*.tpl'];

    return gulp.src(files)
        .pipe(concat('templates.html'))
        .pipe(gulp.dest('dist'));
        // .pipe(notify({message: 'TPL OK', icon: __dirname + '/notify.ico'}));
  return gutil.log('TPL OK!');
});

gulp.task('unit-tpl-prepare', function() {
    var files = ['units/templates/*.tpl'];

    return gulp.src(files)
        .pipe(concat('templates.html'))
        .pipe(gulp.dest('dist/tpl'));
        // .pipe(notify({message: 'TPL OK', icon: __dirname + '/notify.ico'}));
  return gutil.log('TPL OK!');
});

gulp.task('admin-tpl-prepare', function() {
    var files = ['admin/templates/*.tpl'];

    return gulp.src(files)
        .pipe(concat('admin-templates.html'))
        .pipe(htmlmin({
            processScripts: ['text/template'],
            removeComments: true,
            collapseWhitespace: true
        }))
        .pipe(gulp.dest('dist'));
        // .pipe(notify({message: 'TPL OK', icon: __dirname + '/notify.ico'}));
  return gutil.log('TPL OK!');
});

gulp.task('documentation', function () {

  gulp.src('./js/**/*.js')
    .pipe(gulpDocumentation('html'))
    .pipe(gulp.dest('js-doc'));

});

gulp.task('admin-js-lib-bundle', function() {
    var files = [
        'dist/lib/jquery.min.js',
        'dist/lib/semantic/dist/semantic.min.js',
        'dist/lib/codemirror/lib/codemirror.js',
        'dist/lib/codemirror/mode/xml/xml.js',
        'dist/lib/codemirror/mode/javascript/javascript.js',
        'dist/lib/codemirror/mode/css/css.js',
        'dist/lib/codemirror/mode/htmlmixed/htmlmixed.js',
        'dist/lib/codemirror/addon/format/formatting.js',
        'dist/lib/jshint.min.js',
        'dist/lib/csslint.js',
        'dist/lib/htmlhint.js',
        'dist/lib/scsslint.js',
        'dist/lib/jsonlint.js',
        'dist/lib/codemirror/addon/lint/lint.js',
        'dist/lib/codemirror/addon/lint/javascript-lint.js',
        'dist/lib/codemirror/addon/lint/html-lint.js',
        'dist/lib/codemirror/addon/lint/json-lint.js',
        'dist/lib/scss-lint.js',
        'dist/lib/codemirror/addon/lint/css-lint.js',
        'dist/lib/underscore.min.js',
        'dist/lib/backbone.min.js'
    ];
    return gulp.src(files)
    // .pipe(jshint())
	// 	.pipe(jshint.reporter('jshint-stylish'))
    .pipe(sourcemaps.init({debug: true}))
    .pipe(concat('bundle.admin-lib.js'))
    // .pipe(sloc())
    .pipe(gulp.dest('dist/js'))
    .pipe(uglify())
    // .pipe(sourcemaps.write())
    .pipe(sourcemaps.write('../maps'))
    .pipe(gulp.dest('dist/js'));
    // .pipe(notify({message: 'JS OK', icon: __dirname + '/notify.ico'}));
return gutil.log('Export bundle OK!');
});

gulp.task('js-export-bundle', function() {
    var files = [
        'dist/lib/jquery.min.js',
        'dist/lib/underscore.min.js',
        'dist/lib/i18next.min.js',
        'dist/lib/semantic/dist/semantic.min.js',
        'dist/lib/backbone.js',
        'node_modules/d3/build/d3.min.js',
    ]
	return gulp.src(files)
    .pipe(sourcemaps.init({debug: true}))
    .pipe(concat('bundle.export.js'))
    // .pipe(sloc())
    .pipe(gulp.dest('dist/js'))
    .pipe(uglify())
    // .pipe(sourcemaps.write())
    .pipe(sourcemaps.write('../maps'))
    .pipe(gulp.dest('dist/js'));
    // .pipe(notify({message: 'JS OK', icon: __dirname + '/notify.ico'}));
return gutil.log('Export bundle OK!');
});

gulp.task('js-export-cmi5-bundle', function() {
    var files = [
        'js/cmi5/cmi5_init.js',
        'js/cmi5/xapiwrapper.js',
        'js/cmi5/verbs.js',
        'js/cmi5/xapistatement.js',
        'js/cmi5/cmi5Controller.js',
        'js/cmi5/cmi5AUSimulator_JSVersion.js'
    ]
	return gulp.src(files)
    .pipe(sourcemaps.init({debug: true}))
    .pipe(concat('bundle.export.cmi5.js'))
    // .pipe(sloc())
    .pipe(gulp.dest('dist/js'))
    .pipe(uglify())
    // .pipe(sourcemaps.write())
    .pipe(sourcemaps.write('../maps'))
    .pipe(gulp.dest('dist/js'));
    // .pipe(notify({message: 'JS OK', icon: __dirname + '/notify.ico'}));
return gutil.log('CMI-5 export bundle OK!');
});

gulp.task('js-export-scorm12-bundle', function () {
    var files = [
        'js/scorm12/APIWrapper.js',
        'js/scorm12/SCORMGoldfish.js'
    ]
    return gulp.src(files)
        .pipe(sourcemaps.init({ debug: true }))
        .pipe(concat('bundle.export.scorm12.js'))
        .pipe(gulp.dest('dist/js'))
        .pipe(uglify())
        .pipe(sourcemaps.write('../maps'))
        .pipe(gulp.dest('dist/js'));
    return gutil.log('SCORM-1.2 export bundle OK!');
});

gulp.task('sloc', function(){
    gulp.src(jsFiles)
      .pipe(sloc());
  });

gulp.task('js-prepare', function() {
	return gulp.src(jsFiles)
		.pipe(jshint())
		.pipe(jshint.reporter('jshint-stylish', { evil: false }))
		.pipe(sourcemaps.init({debug: true}))
		.pipe(concat('zorbidor.min.js'))
        // .pipe(sloc())
		.pipe(gulp.dest('dist/js'))
		.pipe(uglify())
		// .pipe(sourcemaps.write())
		.pipe(sourcemaps.write('../maps'))
		.pipe(gulp.dest('dist/js'));
		// .pipe(notify({message: 'JS OK', icon: __dirname + '/notify.ico'}));
  return gutil.log('JS OK!');
});

gulp.task('admin-js-prepare', function() {
    var files = ['admin/js/models/base/*.js','admin/js/models/*.js','admin/js/collections/*.js', 'admin/js/views/*.js', 'admin/js/routers/*.js', 'admin/js/adminfish.js'];
	return gulp.src(files)
		.pipe(jshint())
		.pipe(jshint.reporter('jshint-stylish'))
		.pipe(sourcemaps.init({debug: true}))
		.pipe(concat('cthulhu.min.js'))
        // .pipe(sloc())
		.pipe(gulp.dest('dist/js'))
		.pipe(uglify())
		// .pipe(sourcemaps.write())
		.pipe(sourcemaps.write('../maps'))
		.pipe(gulp.dest('dist/js'));
		// .pipe(notify({message: 'JS OK', icon: __dirname + '/notify.ico'}));
  return gutil.log('JS OK!');
});

gulp.task('unit-js-prepare', function() {
    var files = ['units/js/src/helpers.js', 'units/js/src/models/*.js','units/js/src/collections/*.js', 'units/js/src/views/*.js', 'units/js/src/fishunit.js'];
	return gulp.src(files)
		.pipe(jshint())
		.pipe(jshint.reporter('jshint-stylish'))
		.pipe(sourcemaps.init({debug: true}))
		.pipe(concat('fishunit.min.js'))
        // .pipe(sloc())
		.pipe(gulp.dest('dist/js'))
		.pipe(uglify())
		// .pipe(sourcemaps.write())
		.pipe(sourcemaps.write('../maps'))
		.pipe(gulp.dest('dist/js'));
		// .pipe(notify({message: 'JS OK', icon: __dirname + '/notify.ico'}));
  return gutil.log('JS OK!');
});

gulp.task('sass-compile', function() {
	var files = ['scss/*.scss'];

	return gulp.src(files)
		.pipe(sass({includePaths: ['scss/components','components/_common']}))
		// .pipe(rename('app.css'))
		.pipe(gulp.dest('scss'));
	return gutil.log('SASS -> CSS OK!')
});

gulp.task('admin-sass-compile', function() {
	var files = ['admin/scss/admin.scss'];

	return gulp.src(files)
		.pipe(sass({includePaths: ['scss/components','components/_common']}))
		// .pipe(rename('app.css'))
		.pipe(gulp.dest('scss'));
	return gutil.log('SASS -> CSS OK!')
});

gulp.task('reload', function() {
	files = ['index.php'];
	gutil.log("reload function");
	return gulp.src(files)
		.pipe(livereload());
});

gulp.task('admin-reload', function() {
	files = ['admin-index.php'];
	return gulp.src(files)
		.pipe(livereload());
});

gulp.task('css-prepare',['sass-compile'], function() {
	var files = ['scss/*.css'];

	return gulp.src(files)
		// .pipe(concat('app.css'))
		.pipe(gulp.dest('dist/css'))
		.pipe(cleancss({compatibility: 'ie8'}))
		// .pipe(sourcemaps.write())
		.pipe(gulp.dest('dist/css'));
		// .pipe(notify({title: 'Hmm..', message: 'CSS ready', icon: __dirname + '/notify.ico'}));
  // return gutil.log('CSS OK!');
});

gulp.task('admin-css-prepare',['admin-sass-compile'], function() {
	var files = ['scss/admin.css'];

	return gulp.src(files)
		// .pipe(concat('app.css'))
		.pipe(gulp.dest('dist/css'))
		.pipe(cleancss({compatibility: 'ie8'}))
		// .pipe(sourcemaps.write())
		.pipe(gulp.dest('dist/css'));
		// .pipe(notify({title: 'Hmm..', message: 'CSS ready', icon: __dirname + '/notify.ico'}));
  // return gutil.log('CSS OK!');
});


gulp.task('watch', function() {
	livereload.listen();
	gulp.watch(['scss/**/*.scss'], ['css-prepare','main-version','reload']);
	gulp.watch(['js/**/*.js'], ['js-prepare','documentation', 'main-version','reload']);
    gulp.watch(['templates/*.tpl'], ['tpl-prepare','main-version','reload']);
    gulp.watch(['build.php'], ['main-version','reload']);
});

gulp.task('admin-watch', function() {
	livereload.listen();
	gulp.watch(['admin/scss/*.scss'], ['admin-css-prepare','admin-version','reload']);
	gulp.watch(['admin/js/**/*.js'], ['admin-js-prepare','admin-version','reload']);
    gulp.watch(['admin/templates/*.tpl'], ['admin-tpl-prepare','admin-version','reload']);
	gulp.watch(['admin/build.php'], ['admin-version','reload']);
});

gulp.task('unit-watch', function() {
	livereload.listen();
    gulp.watch(['units/js/**/*.js'], ['unit-js-prepare', 'reload']);
    gulp.watch(['units/templates/*.tpl'], ['unit-tpl-prepare','reload']);
    gulp.watch(['units/index.php'], ['reload']);
    gulp.watch(['units/scss/*.scss'], ['reload']);
});

