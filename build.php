<?php
require('backend/general.php');
require('vendor/leafo/scssphp/scss.inc.php');
// Importante: build.php es la fuente de index.php, que se genera desde el anterior a traves de gulp
if (!isset($_SESSION['user_id'])) {
  include('start.php');
  die();
}

$user_id = $_SESSION['user_id'];
$user = file_get_contents(getServer().'api/user/'.$user_id);

if (!isJson($user) || !isJson($config)) {
  die("Error getting initial models.");
}

/* Compilar CSS de webfonts "live" */
$scss = new scssc();
$scss->addImportPath('components/_common');
$scss->setFormatter('scss_formatter_compressed');
$fonts = $scss->compile('$fontPath: \'dist/css/fonts/\';'.' @import "fonts.scss";');
?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>Goldfish</title>
  <!-- <link href="https://fonts.googleapis.com/css?family=Oswald:300,400,700" rel="stylesheet"> -->
  <link rel="stylesheet" href="dist/lib/spectrum/spectrum.css">
  <link rel="stylesheet" id="css-void" href="dist/css/void.css">
  <style id="css-fonts"><?php echo($fonts); ?></style>
  <style id="css-theme">
  </style>
  <link rel="stylesheet" href="dist/lib/semantic/dist/semantic.min.css">
  <link rel="stylesheet" href="node_modules/toastr/build/toastr.min.css">

</head>
<body>
  <div id="bg"></div>

  <section id="unit"></section>

  <section id="config"></section>

  <section id="projects"></section>

  <section id="files"></section>

  <section id="project"></section>

  <section id="notifications"></section>

  <!-- <svg viewBox="0 0 340.274 340.274" class="menu-toggle"><use xlink:href="img/menu.svg#icon-1"></use></svg> -->

  <aside id="dashboard">

  </aside>

  <div class="screen">

    <svg viewBox="0 0 32 32" class="waiting"><use xlink:href="img/icons/spinner2.svg#icon-1"></use></svg>
    <p></p>
  </div>

  <div id="content-holder"></div>

  <aside id="secondary-menu">
  </aside>

  <div id="preview">
  </div>

  <aside id="debug-info"></aside>

<?php include('dist/templates.html'); ?>
  <script src="dist/lib/jquery.min.js"></script>
  <script src="dist/lib/semantic/dist/semantic.min.js"></script>
  <script src="dist/lib/jquery-ui.min.js"></script>
  <!-- <script src="dist/lib/zoidable.min.js"></script> -->
  <script src="dist/lib/underscore.min.js"></script>
  <script src="dist/lib/backbone.js"></script>
  <script src="dist/lib/i18next.min.js"></script>
  <!-- <script src="dist/lib/backbone.debug.js"></script> -->
  <!-- <script src="dist/lib/backbone.localStorage.js"></script> -->
  <script src="dist/lib/ckeditor/ckeditor.js"></script>
  <script src="dist/lib/spectrum/spectrum.js"></script>
  <script src="node_modules/d3/build/d3.min.js"></script>
  <script src="node_modules/toastr/build/toastr.min.js"></script>
  <script src="dist/js/zorbidor.min.js"></script>
  <script>
    var user = <?php echo($user); ?>;
    var config = <?php echo($config); ?>;
    user.config.themes = ['dark','light','goldfish'];
    var i18n = {
      "sequence": "Mostrar en secuencia",
      "blockNavDuring": "Bloquear navegación por la duración del cuestionario",
      "blockNav": "Bloquear navegación hasta aprobar",
      "timeLimit": "Límite de tiempo (0 = sin límite)",
      "umbral": "Porcentaje mínimo para pasar",
      "transitionIn": "Transición de entrada",
      "transitionOut": "Transición de salida",
      "outputNav": "Sistema de navegación",
      "d3": "Arból D3",
      "accordeon": "Acordeón",
    };
    goldfish.start({user: user, config: config, formats: user.formats, fonts: user.fonts, i18n: i18n });

  </script>
</body>
</html>
