<?php
require('backend/general.php');
$name = $_GET['value'];

function getFileType($param) {
    $url  = strtolower($param);
    $type = 'none';
    if (strpos($url, '.jpeg') || strpos($url, '.jpg'))
        $type = 'jpg';
    elseif (strpos($url, '.gif'))
        $type = 'gif';
    elseif (strpos($url, '.png') || strpos($url, 'maps/api/staticmap'))
        $type = 'png';
    elseif (strpos($url, '.swf'))
        $type = 'swf';
    elseif (strpos($url, '.doc'))
        $type = 'doc';
    elseif (strpos($url, '.xls'))
        $type = 'xls';
    elseif (strpos($url, '.ppt'))
        $type = 'ppt';
    elseif (strpos($url, '.pdf'))
        $type = 'pdf';
    elseif (strpos($url, '.zip'))
        $type = 'zip';
    elseif (strpos($url, '.mp3'))
        $type = 'mp3';
    elseif (strpos($url, '.mp4'))
        $type = 'mp4';
    elseif (strpos($url, '.gl'))
        $type = 'none';
    elseif (strpos($url, 'texdebug'))
        $type = 'png';
    elseif (strpos($url, '.html')) {
        $type = 'none';
    } elseif (strpos($url, '.org')) {
        $type = 'none';
    } elseif (strpos($url, '.com')) {
        $type = 'none';
    } elseif (strpos($url, '.asp')) {
        $type = 'none';
    } elseif (strpos($url, '.php')) {
        $type = 'none';
    } elseif (strpos($url, '.co')) {
        $type = 'none';
    } elseif (strpos($url, '.mx')) {
        $type = 'none';
    } elseif (strpos($url, '.ve')) {
        $type = 'none';
    } elseif (strpos($url, '.ar')) {
        $type = 'none';
    } elseif (strpos($url, '.us')) {
        $type = 'none';
    } elseif (strpos($url, '.be')) {
        $type = 'none';
    } elseif (strpos($url, 'latexrender'))
        $type = 'png';
    else {
        $type = $url;
    }
    return $type;
}

$attach = isset($_GET['attach']) ? true : false;
switch (getFileType($name)) {
    case 'jpg':
        header('Content-type: image/jpeg');
        break;
    case 'gif':
        header('Content-type: image/gif');
        break;
    case 'png':
        header('Content-type: image/png');
        break;
    case 'mp4':
        header('Content-type: video/mp4');
        break;
    case 'zip':
        header('Content-type: application/zip');
        //header('Content-type: force-download');
        
        $attach = true;
        break;
    case 'swf':
        header("Content-Type: application/x-shockwave-flash");
        break;
    case 'pdf':
        header("Content-type: application/pdf");
        break;
    default:
        break;
}
if ($attach)
    header('Content-Disposition: attachment; filename="' . basename($name) . '"');
echo file_get_contents(PHYSDIR.$name);
?>