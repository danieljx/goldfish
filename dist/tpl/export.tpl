<!doctype html>
<html lang="en">
	<head>
	  <meta charset="utf-8">
	  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	  <meta http-equiv="Pragma" content="no-cache">
	  <title><%=$title%></title>
	  <link rel="stylesheet" id="theme-styles" href="content/all.css?v=<%=$cacheKiller%>">
	  <link rel="stylesheet" href="dist/css/semantic.min.css?v=<%=$cacheKiller%>">
	  <style>
	  	body {  }

		#main > nav {
			position: relative;
		}

		#main .red > iframe {
		width: 100%;
		border: 0 none;
		}

		#main > #content > .page {
		display: none;
		}
	  </style>
	</head>
	<body>
	<script type="text/template" id="tpl-nav-d3">
	<a href="" class="nav-open">
		<svg viewBox="0 0 32 32">
		<use xlink:href="/units/img/menu.svg#icon-1"></use>
		</svg>
	</a>
	<section class="main">
		<!-- <a href="" class="nav-close">Cerrar</a> 
		<svg>-->
	</section>
	</script>
	<script type="text/template" id="tpl-unit">
		<h1><%= title %></h1>
		<section id="main">
		  <nav></nav>
		  <section id="content"></section>
		</section>
	</script>
	<script type="text/javascript" src="dist/unit.json.js?v=<%=$cacheKiller%>"></script>
	<script type="text/javascript" src="dist/bundle.export.js?v=<%=$cacheKiller%>"></script>
	<script type="text/javascript" src="dist/fishunit.min.js?v=<%=$cacheKiller%>"></script>
	<% if ($cmi5) { %>
	<script type="text/javascript" src="dist/bundle.export.cmi5.js?v=<%=$cacheKiller%>"></script>
	<% } else if ($scorm12) { %>
	<script type="text/javascript" src="dist/bundle.export.scorm12.js?v=<%=$cacheKiller%>"></script>
	<% }%>
	<script>
	var i18n = {
      "sequence": "Mostrar en secuencia",
      "blockNavDuring": "Bloquear navegación por la duración del cuestionario",
      "blockNav": "Bloquear navegación hasta aprobar",
      "timeLimit": "Límite de tiempo (0 = sin límite)"
	};
	
	fishunit.start({unit: unit, i18n: i18n });
	</script>
	<% if ($scorm12) { %>
	<script>
		fishunit.scorm = new fishunit.Scorm({
			listenLocation: fishunit.unit,
			fnLocation: 'setCurrentPage',
			keyLocation: 'currentPage',
			attrLocation: 'cid'
		});
	</script>
	<% }%>
	</body>
</html>