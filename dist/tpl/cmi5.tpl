<?xml version="1.0" encoding="utf-8"?>
<courseStructure xmlns="https://w3id.org/xapi/profiles/cmi5/v1/CourseStructure.xsd">
  <course id="<%=$courseId%>">
    <title>
      <langstring lang="en-US"><%=$courseTitle%></langstring>
    </title>
    <description>
      <langstring lang="en-US"><%=$courseDescription%></langstring>
    </description>
  </course>
  <au id="<%=$auId%>" launchMethod="AnyWindow">
    <title>
      <langstring lang="en-US"><%=$auTitle%></langstring>
    </title>
    <description>
      <langstring lang="en-US"><%=$auDescription%></langstring>
    </description>
    <url><%=$auTitle%>.html</url>
  </au>
</courseStructure>
