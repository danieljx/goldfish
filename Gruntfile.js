

module.exports = function(grunt) {

    // 1. All configuration goes here 

    grunt.loadNpmTasks('grunt-contrib-watch');    

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        watch: {
          scripts: {
            files: ['js/views/*.js','js/models/*.js','js/collections/*.js','js/app.js','css/app.scss'],
            tasks: ['default']
          },
        },
        sass: {
            dist: {
                files: {
                    'css/app.css': 'css/app.scss'
                }
            }
        },
        concat: {   
            dist: {
                src: [
                    'js/models/page.js',
                    'js/models/unit.js',
                    'js/models/app.js',
                    'js/collections/pages.js',
                    'js/views/page.js',
                    'js/views/unit.js',
                    'js/views/app.js',
                    'js/routers/router.js',
                    'js/app.js'

                ],
                // dest: 'js/zorbidor.js',
                dest: 'js/zorbidor.min.js',
            },
            style: {
                src: [
                    'css/app.css'
                ],
                dest: 'css/app.css',
            }
        },
        uglify: {
            options: {
                // beautify: true,
                mangle: {
                   except: ['Backbone', 'goldfish']
                },
                compress: {
                    // drop_console: true,
                }
            },
            build: {
                src: 'js/zorbidor.js',
                dest: 'js/zorbidor.min.js'
            }
        },
        cssmin: {
          options: {
            shorthandCompacting: false,
            roundingPrecision: -1
          },
          target: {
            files: {
              'css/app.min.css': ['css/app.css']
            }
          }
        },
        notify: {
            standard: {
                options: {
                    title: 'Grunt complete',
                    message: 'The owls are not what they seem.',
                    duration: 1,
                    success: true,
                    enabled: true
                }
            }
        }
        // imagemin: {                          // Task
        //     dynamic: {                         // Another target
        //       files: [{
        //         expand: true,                  // Enable dynamic expansion
        //         cwd: 'theme/images/',                   // Src matches are relative to this path
        //         src: ['**/*.{png,jpg,gif}'],   // Actual patterns to match
        //         dest: 'theme/images_ok/'                  // Destination path prefix
        //       }]
        //     }
        //   }

    });

    // 3. Where we tell Grunt we plan to use this plug-in.
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-svgmin');
    grunt.loadNpmTasks('grunt-notify');
    // 4. Where we tell Grunt what to do when we type "grunt" into the terminal.
    // grunt.registerTask('default', ['concat', 'uglify', 'cssmin']);
    // grunt.registerTask('default', ['concat', 'uglify', 'sass', 'cssmin', 'notify:standard']);
    grunt.registerTask('default', ['concat', 'sass', 'cssmin', 'notify:standard']);

    grunt.event.on('watch', function(action, filepath, target) {
      grunt.log.writeln(target + ': ' + filepath + ' has ' + action);
    });

};

